# National Virtual Library of India

The purpose of National Virtual Library of India is to facilitate a comprehensive database on digital resources on information about India and on information generated in India, in an open access environment.

This component is main portal which is a maven based web application with primary modules being Spring & Hibernate.

## Getting Started

1. Pull the latest source code of the project
2. Open project in Netbeans, Clean & Build (by running maven will download all the necessary dependencies for the source code )
3. Make sure all configurations are properly set See  [Configurations] (#code-configurations) for details.

### Prerequisites
1. Oracle JDK 1.8 installed [Get it from here](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
2. Application server (Glassfish 4.1.2 recommended )
3. Build Tool - [Maven](http://www-us.apache.org/dist/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.zip) installed & configured
4. Latest MariaDB installed and configured either in network or in device

### Installation
Building the source using `maven` build tool is quite easy.

### Environment Setup
The following configurations need to be changed before deployment.
1. Make sure that maven binaries are properly installed / extracted and its `/maven-intallation-dir/bin` is added into systems `PATH` variable.
2.  `.nvli` folder created in local system usually under user's home directory. Make sure this directory is available. If not create it using ```$ mkdir -p ~/.nvli/conf```
3.  Create `settings.properties` in `~/.nvli/conf` directory, and copy the contents of `settings.properties` file available in projects root directory.
4.  Make sure the values in the `settings.properties` file are properly reconfigured as per the system environment and dependent
5.  Mysql Database Settings under jndi-dao-nvliPortal-config.xml file or nvli-Pool created in glassfish (navigate to Resources-> JDBC-> Connection Pools under glassfish admin)
6. Logging settings for log file location and logger level in Log4j.xml available at `/src/main/webapp/WEB-INF/config` folder
7. MongoDB (NOSQL) Database Settings under spring-data-mongodb-config.xml file at `/src/main/webapp/WEB-INF/config` folder




### Building the project
Go to projects root directory and clean and build the source using maven
```bash
# Nove to the directory where the project has been cloned
$ cd /path/to/project-nvli

# Following command will genereate the .war file of the project in target directory in the root of the project
$ mvn clean install
```

### Deploying the project
* Start the Glassfish Web Server
* Deploy the generate `war` file into Glassfish Web Server

To know more about Glassfish Webserver [visit this document &#x1f517;](https://javaee.github.io/glassfish/doc/4.0/reference-manual.pdf)

## Contributing
Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.


## License
