# Release Note ~ v1.17.0 

### Features 
* Added : Module to allow user to add heritage sites, places and temples related information
* Added : Feature in rewards and certificate module to validate awarded certificates to user via certificate id and other necessary fields

### Changes 
* Changed : Layout of view page to show the user defined virtual galleries in grid ([f0b42ccd7](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/f0b42ccd7aff9e08576deac595083ed12fd55936))
* Changed : Variable sized thumbnails to in Gallery List page to square sized thumbnails ([4d0e67ffd](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/4d0e67ffd4445948cbeab1f6ac1797cd7c804815))
* Changed : Interface and style to display the metadata of virtual galleries more effective manner ([fd55d2589](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/fd55d2589776f52f710bee1375c4e1eda6b849f5))
* Changed : RabbitMQ Queues for subscription and delete resources were merged for logging activities into MongoDB ([2576254b7](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/2576254b7ce58c08e9ffe8846b5d9afb35486ff9))

### Bug Fixes 
* Fixed : User was able to create and publish virtual gallery without any artefacts being added into it ([3faddadec](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/3faddadec08af9fd64c41804aa000eedb980fe1b))
* Fixed : Users were not able to delete `Event` created by them ([8581f44f7](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/8581f44f72ff0edde32236d5ded82044514e8a4f))
* Fixed : Exceptions occuring while savnig the activity logs ([8e3e8931e](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/8e3e8931eec9e40ef570c380da394669c14379c1))
* Fixed : Awarded star was not being displayed properly on profile page ([8c1ca8fc2](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/8c1ca8fc20bdf447a404fa904732b53c6c5f6edf))

# Release Note ~ v1.16.5 

### Changes 
* Changed : Checkbox UI in Event Page ([d6c0403a9](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/d6c0403a9dbacd1e47021a809b928cef29b055c3))
* Changed : Design changes for Event module ([e25543cf1](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/e25543cf14454beae8773426ee63d9af616198d2))
* Changed : UI of gallery sliders items ([71d4fd709](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/71d4fd7098955c9f4f907ee21c74de8dca72882b))
* Changed : Rare Book thematic type page as per book shelf design issue as per rare_books_1column.htm. ([338c06af3](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/338c06af3bdc252a94f6f28455ae38152038e186))

### Bug Fixes 
* Fixed : Titles of various views ([7d760d0fd](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/7d760d0fd5c655f69da4d3df738f7fa8ff5e6f17))
* Fixed : 'Resource Not found' message was appearing for certain resource types in analytics page ([d3da389e4](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/d3da389e401f30a91f8e3c9e72e16b2c76baea38))

# Release Note ~ v1.16.4 

### Features
* Added : RabbitMQ queuing in between NVLI Portal and some mongo web service calls ([bed442bf5](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/bed442bf5bc3f619f4f34570e58879c45b2822e7))

### Changes 
* Changed : Activity Log payload's item's datatype for sending it to rabbitmq ([3f1798b47](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/3f1798b47cc066890a1e522fbb7a880198c5e7a4))
* Changed : Form fields and attributes to improve accessibilities. ([2f12bcaf9](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/2f12bcaf9d39dd9e018ba70f5c0523b31929ada1))

### Bug Fixes
* Fixed : Certificate PDF file rendering properly with required validation from cdn ([36a84047d](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/36a84047d948930d84dda700fd8d3a5658959cfd))

# Release Note ~ v1.16.3

### Changes
* Added : With-content and without-content filters are applied on resource-browser page ([c1ef6ac96](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/c1ef6ac960638584453bd92b88c8f3c3aed64474))

### Bug Fixes
* Fixed : Process to show certificate pdf ([e13c75bdf](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/e13c75bdf8203d8b98a18e109faf566f47112b4e))

# Release Note ~ v1.16.2

### Changes
* Changed : Entrance effect of resource type bubble to fadeInUp [6793db371](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/6793db371490ef79008a35e59440649ee5e8d575)

### Bug Fixes
* Fixed : URLs of sly plugin's script and css [5200cbaa3](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/5200cbaa37ce353d5f063d150a7eff6d566e03ab) 


# Release Note ~ v1.16.1

### Features

### Changes
* Preview not available PNG image has been replaced with SVG. ([89427a4d3](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/89427a4d344d58b34964c4450a2ad67f0b94a544))
* Added translation for Virtual Gallery views. ([872aed6d7](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/872aed6d7f2992fa5e7ba7be0896f1d1a9e6ba8c))
* Modified : Design related to resource-browser page ([cc064fdb6](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/cc064fdb6d82e9ce9e743f1e9ef7a61b38ddffcd))
### Bug Fixes
* Fixed : Websocket error occuring on anonymously accessible pages ([f2ed0c622](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/f2ed0c622e159ffd6e3b98f3bc90f92238836761))
* Fixed : Home screen background image flickering issue ([286a39571](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/286a3957175daa8308a8415e3e32dfa947f052e4))
* Fixed : Updated record count function of rare book ([fd7bf2735](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/fd7bf27354588b5d75e3f613575c5f7c29d74d68))
* Fixed : Pagination of the resource browser page ([e2624311b](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/e2624311b0fc2cccb0a382462925016e796725a1))

# Release Note ~ v1.16.0

### Features
* Added : functionality to update approved points crowdsourcing contribution via udc tags and custom tag ([4b72d399d](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/4b72d399d7e21255b1395a9054a516390065985c))
* Added: Feature of User Defined Virtual Gallery ([bc80da322](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/bc80da322dc0cf26a65f376e2396f3bea3db2688), [d3c72c97c](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/d3c72c97c360a615ed38bba41bd70e27449f4ec7), [97921503b](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/97921503bf83b758d85e85d725a4dcaefed53ec0), [e1ab30aaf](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/e1ab30aaf9fd5537e9cadefd785b5b0b3923818f),
[45c019d0e](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/45c019d0ea133c617e1932f9976a2772d183c010), [431db0bfc](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/431db0bfcfc39eb16987bb2f828d027a5533beb4) )
* Added: Component sly for creating horizontal scroller ([1c09e9e0b](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/1c09e9e0b18ed1d06f048e1ce8d6b80a026e6ee6))
* Added: methods for serving gallery lists and save updated gallery ([32d97511d](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/32d97511d3dc606100729ccd4bf3874aeae10804) , [dde500fae](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/dde500faecc231720a96ba95ae1a398397914e14))
* Added: methods to delete added media from gallery ([d7af67f98](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/d7af67f9889fad5cb6ef32c0a4bd46a7ccd66dcd))
* Added: API to serve user's public information ([9d3eeed7a](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/9d3eeed7a54386a03e5587b5b80a416ae52abde2))
* Added: YouTube Live Streaming broadcasting feature ([04f2327c5](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/04f2327c570b3e36fe9395ea59794188d02b0a6b), [48c4bd9c7](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/48c4bd9c75df136e2d7eb2422505fb6dc98f66ac),  [609a65ce0](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/609a65ce02a5eebb7cbc9a0d6696c27bf9557854), [5f0e7116f](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/5f0e7116f92678d8a06f88fec36bbcd0f2fa10eb),  [5e39cb470](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/5e39cb470c016ffbef9ddcf7980a53fbe104e08e), [14b41bb30](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/14b41bb30d8c49fb0516f68d241326470ebd8c0d), [65820c8e5](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/65820c8e5f24fd62efb84cf1ded96cd2a4de0191), [89053ad34](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/89053ad34f73e34c41f4abf7d84f40bca3995d8c), [824accedb](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/824accedb744ac51b4e52deb20705d9c7b531d83), [0f924f828](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/0f924f82819c919bff2a8519db347554e4f7c1ca), [35e0be9d6](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/35e0be9d6c766c6e76ff94585e9c3c3d33e3b4de), [29c3cad75](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/29c3cad750c26d374a6f064d796b170765dae336), [83333ddc3](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/83333ddc3bee3a0bc804e5d0d720c9f071e18c47), [58dd8768b](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/58dd8768be53d7ce61eea9f905537c40c6548cbd)     )
* Added: Functionality to flash message on login screen if there exists an event today ([4a279f0e7](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/4a279f0e741bafbd58b92cd9a7941acda5134126)) 
* Added: Basic statistics for events and feature to remove events by creator ([66601511a](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/66601511a51aaa6196d46b5f838167661eec78d3) )
* Added: Functionality to identify valid youtube video ID from the Feed URL automatically ([f81153796](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/f811537965734d4d5890fb88b0b61fb0db4c957e))
* 
### Changes
* Removed : Stale code related to email funcionality and optimized code ([3c255302d](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/3c255302dcef97f291a33ce596f3cc39b0b36aa8))
* Changed : HTTP Intercept Access to generate captcha image for all types of users ([57205971c](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/57205971cf5d1eb42801cd5aed65ccece63f8571))
* Changed: Virtual Gallery List controllers to serve paginated data ([5dae1b0ac](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/5dae1b0ace56732e885c04bace61f38dd5a1b6f3))
* Changed : AnalyticsQueryParam bean to make it common ([0e7130ae1](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/0e7130ae1fd47c6bcdf9e0c20b71f7e98d405232))
* Updated : Question Answering System Page. ([ecc80fbe6](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/ecc80fbe6d3867e3cb397f68413dcb37d1b72306))
* Changed : View of rarebook resource category to mimic book shelf ([b6854bdaa](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/b6854bdaa7e27259448ec178f54b12625a140384))
### Bug Fixes
* Fixed : Accessibility issues in all layouts, footer, home page and submit button of common search ([e44c2b4fd](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/e44c2b4fdc4b1e4a9fc55d1e4ee6c9bb72ad88c4), [ef1f4b26c](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/ef1f4b26c79c91848f2bd17c9adc3beb498e19d0), [50c0a99ee](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/50c0a99ee712026db53545a0672847708ba6f9f6), [ac52944e0](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/ac52944e010237df7d193fb816ba6d00d73c6a0c) )
* Fixed : Page loading performance Issues in some pages ([88bff55ba](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/88bff55baed5ad1207b52ef9c2df96bc76d9ef21), [6f78e6495](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/6f78e64959b642155e5de5124d9b722711e4f14b), [cba459403](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/cba4594036ee6365b046e27912dbe8c8356e336d))
* Fixed : Modified user image path while updating user profile ([98f7bfa1c](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/98f7bfa1c204d58d2f4e6fe51b74b9806c021239))
* Fixed : json chaining issueby adding `@JsonIgnore` annotations to pojo properties  ([f924e4abf](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/f924e4abfde0f51dc810a8ad1ae8a84529fe8fc5) )
* Fixed : issues related to Reward and Certificates feature ([50a18774e](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/50a18774eee01adf6ddfc689f3a5001443523c08) , [40c76ae0f](http://gitlab.cse.iitb.ac.in/nvli/project-nvli/commit/40c76ae0f9055451f0473ec96eaede5f9d1d41b8),  )