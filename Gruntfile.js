'use strict';
module.exports = function (grunt) {

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Project settings
        config: {
            app: 'src/main/webapp/WEB-INF/static',
            dist: 'src/main/webapp/WEB-INF/static'
        },
        pkg: grunt.file.readJSON('package.json'),
        // Compiles less files to generate CSS files
        less: {
//            development: {
//                options: {
//                    // paths: ['<%= config.app %>/styles/']
//                },
//                files: {
//                    "<%= config.app %>/styles/css/main.css": "<%= config.app %>/styles/less/main.less"
//                }
//            },
            production: {
                options: {},
                files: {
                    "<%= config.app %>/styles/css/production.css": "<%= config.app %>/styles/less/main.less",
                    "<%= config.app %>/styles/css/profile.css": "<%= config.app %>/styles/less/profile.less"
                }
            }
        },
        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                        dot: true,
                        src: [
                            '.tmp',
                            '<%= config.dist %>/*',
                            '!<%= config.dist %>/.git*'
                        ]
                    }]
            },
            server: '.tmp'
        },
        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            options: {
                dest: '<%= config.dist %>'
            },
            html: '<%= config.app %>/index.html'
        },
        // By default, your `index.html`'s <!-- Usemin block --> will take care
        // of minification. These next options are pre-configured if you do not
        // wish to use the Usemin blocks.
        cssmin: {
            dist: {
                files: {
                    '<%= config.dist %>/styles/css/production.min.css': [
                        '<%= config.app %>/styles/css/production.css'
                    ]
                }
            }
        },
        uglify: {
            build: {
                src: '<%= config.app %>/scripts/production.js',
                dest: '<%= config.dist %>/scripts/production.min.js'
            }
        },
        concat: {
            dist: {
                src: [
                    '<%= config.app %>/scripts/main.js'
                ],
                dest: '<%= config.dist %>/scripts/production.js'
            }
        },
        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                        expand: true,
                        dot: true,
                        cwd: '<%= config.app %>',
                        dest: '<%= config.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            'images/{,*/}*.webp',
                            '{,*/}*.html',
                            'fonts/{,*/}*.*'
                        ]
                    }, {
                        src: 'node_modules/apache-server-configs/dist/.htaccess',
                        dest: '<%= config.dist %>/.htaccess'
                    }, {
                        expand: true,
                        dot: true,
                        cwd: 'assets/bower_components/bootstrap/dist',
                        src: 'fonts/*',
                        dest: '<%= config.dist %>'
                    }]
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%= config.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        }
    });


    // Load grunt tasks automatically
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Register grunt build task
    grunt.registerTask('build', [
        'less',
        'concat',
        'cssmin',
        'uglify'
    ]);

    // Register default grunt task
    grunt.registerTask('default', [
        'build'
    ]);
};
