<%-- 
    Author     : Priya Bhalerao
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />


<ol class="breadcrumb">
    <li>
        <a href="${pageContext.request.contextPath}/home"><span class="fa fa-home"></span></a>
    </li>
    <c:if test="${not empty breadcrumbMap}">
        <c:forEach items="${breadcrumbMap.keySet()}" var="key">
            <c:choose>
                <c:when test="${breadcrumbMap.get(key) eq '#'}">
                    <li>${key}</li>
                    </c:when>
                    <c:otherwise>
                    <li><a href="${pageContext.request.contextPath}/${breadcrumbMap.get(key)}">${key}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </c:if>
    <li  class="active">${title}</a></li>
</ol>