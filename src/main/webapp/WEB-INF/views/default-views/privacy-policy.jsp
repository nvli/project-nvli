<%--
    Document   : privacy-policy
    Created on : 14 Feb, 2018, 12:03:06 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>.h-heading{font-weight: bold; color:#444; } h4{ margin:20px 0 15px; font-weight: bold; color: #333;}</style>
<section class="container" style="text-align: justify;">
    <br>
    <h2 class="text-center h-heading">Privacy Policy</h2>
    <br>
    <h4>What information does “NVLI” collect?</h4>
    <p>We have provided a facility through which you can upload and manage your personal information. “Users” are required to ensure that they take all precautions to guard their privacy and security. We may collect the information you provide such as name, affiliation, designation, phone number, email address, correspondence address, etc. to store with your account; log in and other related information and  record the information about downloads, and your pattern of access of our “Site”. We may collect your location information through systems such as Global Positioning System (GPS) and other sensors, may store the information on local storage devices using mechanisms such as browser web storage and application data caches; related to update of any service provided on the “Site”. We may record the discussion on online or offline fora, if any. During online and offline discussions if any, you may share some information voluntarily. “NVLI” is unable to control any use of such information. NVLI is not responsible for any data or information not stored on our servers.</p>
    <h4>How does “NVLI” share the information collected?</h4>
    <p>We may use the information for academic research purposes to develop new technologies to provide better service to our “Users”.  We may share personal information at an aggregated level for academic purposes such as research, data analysis, statistical analysis, annual reports and like, on conditions of anonymity; with the funding agency, Ministry of Culture (MoC); to third parties in case of any legal activity. </p>
    <p>We follow security practices and procedures to protect your personal information. In their own interest, “Users” are required to maintain the confidentiality of their login ID and password, and not share it with others. Your personal and related information will be accessed only by concerned “NVLI” employees, and authorized administrative staff. The contact and other relevant information you share, may be used to protect the rights, property, and safety of “NVLI”, our “Users” and other Stakeholders. </p>
    <h4>Dispute resolution</h4>
    <p>NVLI is committed to resolving complaints, if any, as early as possible through our self-regulatory systems, but if in any case we are not able to resolve any complaint related to personal data or privacy of the “Users”, it will be referred to the local competent data protection authority, for resolution. </p>
</section>