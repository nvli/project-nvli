<%--
    Document   : about
    Created on : 14 Feb, 2018, 10:29:10 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    .logo-container-about{text-align: center;}
    .list-group {
        padding-left: 20px;
    }
    .list-group li{
        /*color: #333333;*/
        margin-bottom: 5px;
    }
</style>
<ol class="breadcrumb">
    <li><a href="${context}/home"><span class="fa fa-home"></span></a></li>
    <li>About</li>
</ol>

<section class="container">
    <br>
    <article style="margin: 20px 0 30px; color: #333; text-align: justify;">
        <div class="logo-container-about"><img src="${context}/images/nvli-logo-128.png"></div><br>
        <h2 style="margin-bottom: 15px; text-align: center;font-weight: bold;color:#444;">
            NVLI Web Portal (Software)
        </h2>
        <!--<h4 style="margin-bottom: 15px; text-align: center;font-weight: bold;">National Virtual Library of India (NVLI)</h4>-->
        <br>
        <p style='text-align: justify'>The Web Portal for National Virtual Library of India (Software) is designed and developed by Centre for Development of Advanced Computing (C-DAC), Pune. The NVLI Web Portal is comprising of many innovative software applications, modules and functionalities, which are briefly summarized as under &mdash;
        </p>
        <br>
        <ul class="list-group">
            <li>Single window access to diverse Indian knowledge resources</li>
            <li>Federated and unified search</li>
            <li>Multilingual search with phonetic text input in Indian languages</li>
            <li>Cross-lingual search</li>
            <li>Localization of user interface in Indian languages</li>
            <li>Advanced configurable search</li>
            <li>Web scale discovery</li>
            <li>Crowdsourcing of metadata in DC and MARC21 formats</li>
            <li>Certificate of appreciation for contribution in crowdsourcing</li>
            <li>Multilingual UDC tagging for structured browsing</li>
            <li>Ontology based structured browsing and visualization support for knowledge structure</li>
            <li>Generic and domain specific facets for filtering of information</li>
            <li>Harvestor for open respositories</li>
            <li>eNews and Website crawling</li>
            <li>Folksonomy and social media sharing</li>
            <li>Trend analytics for most searched keywords, popular links, most shared resources</li>
            <li>Personalization of NVLI as per user requirements and extensive user profiling</li>
            <li>Event based thematic library</li>
            <li>Online payment for paid access to contents</li>
            <li>QA system</li>
            <li>User experience design</li>
        </ul><br>
    </article>
</section>