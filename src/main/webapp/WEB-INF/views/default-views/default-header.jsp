<%--
    Document   : default-header
    Created on : Apr 4, 2016, 11:36:53 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"  %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" href="${context}/components/toastr/toastr.min.css">
<script src="${context}/components/toastr/toastr.min.js"></script>
<script src="${context}/components/websocket/sockjs-0.3.4.js"></script>
<script src="${context}/components/websocket/stomp.js"></script>
<div id="header" class="clearfix bg1">
    <a href="${context}/home"><div class="header_logo vcenter"> <img src="${context}/themes/images/logo/NVLI-LOGO-header.png"  alt="National Virtual Library of India (NVLI)"/></div></a>
    <div class="pull-right vcenter">
        <!-- Single button -->
        <div class="btn-group">
            <button id="lanSelect" type="button" class="btn btn-secondary btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
            <div class="dropdown-menu">
                <a class="dropdown-item localisation" href="#" id="en">English</a>
                <a class="dropdown-item localisation" href="#" id="hi">हिंदी</a>
                <a class="dropdown-item localisation" href="#" id="mr">मराठी</a>
                <a class="dropdown-item localisation" href="#" id="bn">বাংলা</a>
                <a class="dropdown-item localisation" href="#" id="te">తెలుగు</a>                
                <a class="dropdown-item localisation" href="#" id="ta">தமிழ்<</a>
                <a class="dropdown-item localisation" href="#" id="gu">ગુજરાતી</a>
                <a class="dropdown-item localisation" href="#" id="kn">ಕನ್ನಡ</a>
                <a class="dropdown-item localisation" href="#" id="ml">മലയാളം</a>
                <a class="dropdown-item localisation" href="#" id="pa">ਪੰਜਾਬੀ</a>
            </div>
        </div>
        <security:authorize access="isAuthenticated()">
            <!-- Single button -->
            <div class="btn-group">
                <button id="selected-theme" type="button" class="btn btn-secondary btn-sm  dropdown-toggle" 
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${user.theme.themeName}</button>
                <div class="dropdown-menu" id="drop_down_menu"></div>
            </div>
        </security:authorize>
        <!-- Single button -->
        <div class="btn-group m-r-1">
            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group btn-group-sm " role="group" aria-label="First group">
                    <a href="#" class="btn btn-secondary font13 fontSizeMinus">&#8211; </a>
                    <a href="#" class="btn btn-secondary font13 fontReset"> A</a>
                    <a href="#" class="btn font13 btn-secondary fontSizePlus">+ </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top_nav_bg bg-inverse clearfix  animated fadeIn">
    <nav class="navbar navbar-dark navbar-light m-l-2 m-r-3 ">
        <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header"> &#9776; </button>
        <div class="collapse navbar-toggleable-xs" id="navbar-header">
            <div class="form-inline pull-xs-right">
                <ul class="nav-left">
<!--                    <li><a href="${context}/special-features" > <span class="fa fa-list" aria-hidden="true"></span> <spring:message code="label.special.features"/></a></li>-->
                    <security:authorize access="isAuthenticated()">

                        <li><a href="${context}/home"><span class="fa fa-home" aria-hidden="true"> </span> <spring:message code="label.home"/></a></li>
                        <li>
                            <a href="${context}/user/dashboard">
                                <span class="fa fa-dashboard" aria-hidden="true"> </span> <spring:message code="sb.dashboard"/>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle  header-notif-btn" href="#" aria-expanded="false">
                                <span class="fa fa-bell" aria-hidden="true"> </span> <spring:message code="notif.heading"/>
                                <span class="notification-count" style="display: none;"></span> <b class="caret"></b>
                            </a>
                            <div class="dropdown-menu notification dropdwnshdw dropdown-menu-right" role="menu" style="display: none; top: 135px; left: 1505.2px;">
                                <div class="noti_hide" id="notif-items">

                                </div>
                                <div class="media-footer">
                                    <a id="notif-view-all" class=" btn btn-link marginauto" href="${context}/user/notifications"><spring:message code="label.view.all"/></a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="${context}/user/profile"><span class="fa fa-user"></span> ${user.firstName} ${user.lastName}</a>
                        </li>

                    </security:authorize>
                    <security:authorize access="!isAuthenticated()">
                        <li>
                            <a href="${context}/auth/login">  <span class="fa fa-sign-in" aria-hidden="true"></span> <spring:message code="label.login"/></a>
                        </li>
                        <li>
                            <a href="${context}/auth/registration"><span class="fa fa-user-plus" aria-hidden="true"></span> <spring:message code="label.register"/></a>
                        </li>
                    </security:authorize>
                    <security:authorize access="isAuthenticated()">
                        <li><a href="${context}/auth/logout"><span class="fa fa-sign-out"></span><spring:message code="label.logout"/> </a></li>
                    </security:authorize>
                </ul>
            </div>
        </div>
    </nav>
</div>
