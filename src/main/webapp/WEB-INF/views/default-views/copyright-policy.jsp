<%--
    Document   : copyright-policy
    Created on : 14 Feb, 2018, 11:56:23 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>.h-heading{font-weight: bold; color:#444; } h4{ margin:20px 0 15px; font-weight: bold; color: #333;}</style>
<section class="container" style="text-align: justify;">
    <br>
    <h2 class="text-center h-heading">Copyright Policy</h2>
    <br>
    <p>“NVLI” respects the intellectual property rights and other proprietary rights of the Content Owners. In case you believe you are the creator or owner of any content hosted or displayed by NVLI, and have Intellectual Property (IP) rights over such content, you are requested to immediately contact us. Please note it is not the intent of NVLI to infringe on any copyright or IP right.
        <br>
        <br>
    <p>To enable us to take the required action, the copyright owner or legal representative of the copyright owner should provide material documented proof of their rights and also specify exactly which content they are referring to, as well as the location on NVLI “Site” where they believe it is displayed. The copyright owner or legal representative of the copyright owner is also requested to provide a statement, stating the authorization, address, telephone number, and email address for correspondence. We would like to state that any communication without authorization, will not be acted on. Unless notified, we are not responsible for any infringement, and it is the sole responsibility of the copyright owner.</p>
</section>
