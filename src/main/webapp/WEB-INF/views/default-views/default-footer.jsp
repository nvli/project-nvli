<%--
    Document   : default-footer
    Created on : Apr 4, 2016, 11:37:14 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<footer class="footer clearfix" id="bg_footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5><span class="localisation-label" data-key="label.about.footer"><tags:message code="label.about.footer"/></span></h5>
                <a href="${context}/about-us" class="link1"><span class="localisation-label" data-key="label.about"><tags:message code="label.about"/></span></a>
                <a href="${context}/nvli-logo" class="link1"><span class="localisation-label" data-key="label.nvli.logo"><tags:message code="label.nvli.logo"/></span></a>
                <a href="http://www.nmlindia.nic.in/" target="_blank" class="link1"><span class="localisation-label" data-key="label.nml"><tags:message code="label.nml"/></span></a>
                <a href="${context}/partner-institutions" class="link1"><span class="localisation-label" data-key="label.about.partners"><tags:message code="label.about.partners"/></span></a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5><span class="localisation-label" data-key="label.quick.links"><tags:message code="label.quick.links"/></span></h5>
                <a href="${context}/home"  class="link1">
                    <span class="localisation-label" data-key="label.home">
                        <tags:message code="label.home"/> </span>
                </a>
                <a onclick="goToLink(this);" href="${context}/analytics/" target="_blank">
                    <span class="lacalisation-label" data-key="label.nvli.analytics">
                        <tags:message code="label.nvli.analytics"/>
                    </span>
                </a>
                <a onclick="goToLink(this);" href="${context}/search/show/BLOG" target="_blank">
                    <span class="lacalisation-label" data-key="label.nvli.blog">
                        <tags:message code="label.nvli.blog"/>
                    </span>
                </a>
                <a onclick="goToLink(this);" href="${context}/nac/news" target="_blank">
                    <span class="lacalisation-label" data-key="label.nvli.enews">
                        <tags:message code="label.nvli.enews"/>
                    </span>
                </a>
                <a onclick="goToLink(this);" href="${context}/visualize/owl" target="_blank">
                    <span class="lacalisation-label" data-key="label.nvli.ontology">
                        <tags:message code="label.nvli.ontology"/>
                    </span>
                </a>
                <a onclick="goToLink(this);" href="${context}/cs/udc-tags-crowdsource" target="_blank">
                    <span class="lacalisation-label" data-key="label.udcTitle">
                        <tags:message code="label.udcTitle"/>
                    </span>
                </a>
                <a onclick="goToLink(this);" href="${exposedProps.wikiBaseURL}" target="_blank">
                    <span class="lacalisation-label" data-key="label.nvli.wiki">
                        <tags:message code="label.nvli.wiki"/>
                    </span>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h5><span class="localisation-label" data-key="label.policies"><tags:message code="label.policies"/></span> </h5>
                <a href="${context}/copyright-policy" class="link1"> 
                    <span class="localisation-label" data-key="label.legal.copyright"><tags:message code="label.legal.copyright"/> </span>
                </a>
                <a href="${context}/hyperlink-policy" class="link1"> 
                    <span class="localisation-label" data-key="label.legal.hyperlink"><tags:message code="label.legal.hyperlink"/> </span>
                </a>
                <a href="${context}/privacy-policies" class="link1"> 
                    <span class="localisation-label" data-key="label.legal.privacy"><tags:message code="label.legal.privacy"/> </span>
                </a>
                <a href="${context}/terms-of-service" class="link1">
                    <span class="localisation-label" data-key="label.legal.information"><tags:message code="label.legal.information"/> </span>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-left">
                <h5><span class="localisation-label" data-key="label.policies"><tags:message code="label.feedback"/></span> </h5>
                <a href="${context}/fc/feedback" class="link1"> <span class="localisation-label" data-key="label.feedback"><tags:message code="label.feedback"/></span></a> 
            </div>
            <div style="margin-bottom:5px;" class="clearfix"></div>
        </div>
    </div>
    <div class="">
        <div class="text-center footer_bottom">
            <p><span class="localisation-label" data-key="nvli.logo"><tags:message code="nvli.logo"/> </span> � <span class="localisation-label" data-key="label.copyright"><tags:message code="label.copyright"/> </span> 
                <%= new java.text.SimpleDateFormat("yyyy").format(new java.util.Date())%>
            </p>
        </div>
    </div>
</footer>
