<%-- 
    Document   : underscore-templates.jsp
    Created on : Apr 19, 2017, 1:16:15 PM
    Author     : Vivek Bugale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>

<script type="text/template" id="tpl-alert">

    <div class="modal" id="{{=modalId}}">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title" bgColor="green">{{=title}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
    <p>{{=message}}</p>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-primary" id="okButton">Save</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
</script>

<!--alert for resources without LIBRARY USER-->
<script type="text/template" id="tpl-resources-without-lib-user">         
    <div class="overlay" id="resourcesWithoutLibUser" style="position: fixed; z-index: 2000;  height: 100%; width: 100%; top: 0px; background-color:rgba(0,0,0,.8)">
    <div class="pc-info-block boxed-layout" style="max-width: 800px; min-width: 800px; min-height: 800px; max-height: 800px; width: 100%; height: 100%; margin: 100px auto;overflow-y: auto;">
    <div class="card">
    <div class="card-header">
    Resources without Library User
    </div>
    <div class="" style="background-color: #fff;">
    <table class="table table-bordered">
    <thead>
    <tr>
    <th width="5%"><tags:message code="label.serial.no"/></th>
    <th width="55%"><tags:message code="label.resource"/></th>
    <th width="40%"><tags:message code="label.resourcetype"/></th>
    </tr>
    </thead>
    <tbody class="scroll_list">
    {{ _.each(resourcesWithoutLibUsersJSONArray, function (resource,i) { }}                                    
    <tr>
    <td>{{=i+1}}</td>
    <td>{{=resource.resourceName}}</td>
    <td>{{=resource.resourceTypeName}}</td>
    </tr>                              
    {{ }); }}                        
    </tbody>
    </table>
    </div>
    <div class="card-footer">
    <div class="text-xs-right small">
    <button id="gotoRegisterBtn" class="btn btn-sm btn-success">Goto Library User Register</button>&nbsp;&nbsp;&nbsp;
    <button id="remindMeLaterBtn" class="btn btn-sm btn-primary">Remind me later</button>
    </div>
    </div>
    </div>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-header-theme-list">
    <a class="dropdown-item theme-item-link" href="#" data-theme-id="{{=id}}" id="{{=themeCode}}" >{{=themeName}}</a>
</script>

<script type="text/template" id="tpl-notif-item">
    <div class="notif-item media {{ if(is_read){ }} noti_read {{ } }}" data-notif-id="{{=notif_id}}" data-target-url="{{=target_url}}">
    <a class="pull-left" href="#">
    <img class="media-object" src="#" alt="Media Object" onerror="this.src='${context}/images/profile-picture.png'">
    </a>

    <div>
    <p> {{=notif_message}} </p>
    <span class="text-muted"><i class="fa fa-clock-o"></i> {{=notif_time}} </span>
    </div>
    <!--<a href="#" class="delete_block"> <span class="fa fa-close"></span> </a>-->
    </div>
</script>


<script type="text/template" id="tpl-profile-check">
    <div class="overlay" id="profileCheckOverlay" style="position: fixed; z-index: 2000;  height: 100%; width: 100%; top: 0px; background-color:rgba(0,0,0,.8)">

    <div class="pc-info-block boxed-layout" style="max-width: 560px; min-width: 500px; min-height: 400px; max-height: 400px; width: 100%; height: 100%; margin: 100px auto;">

    <div class="card">
    <div class="card-header">
    Complete your profile to proceed
    </div>
    <div class="card-block" style="background-color: #fff;">
    <div>
    Your profile information as per your role does not seems to be complete. Please follow the following links to complete.
    </div>
    </div>
    <div class="card-footer">
    <div class="text-xs-center small">
    {{ if(!hasLanguageExpertise){  }}
    <a href="${context}/user/languages?ref=user-profile-check&progress=1&hasLanguageExpertise=0" class="btn btn-sm btn-secondary">Select Language Expertise</a>
    {{ }  }}
    {{ if(!hasResourceExpertise){  }}
    <a href="${context}/user/resource_specialization?ref=user-profile-check&progress=1&hasResourceExpertise=0" class="btn btn-sm btn-secondary">Select Resource Expertise</a>
    {{ }  }}
    </div>
    </div>
    </div>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-image-browser">
    <div class="modal" id="{{=id}}">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{=title}}
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </h5>
                </div>
                <div class="modal-toolbar" style="background:#efefef;padding:15px">
                    <p class="help-text mute">Upload your desired images and crop them and instert the cropped image into your article. Images should be in <em>.jpeg, .jpg, .png or .gif</em> format of maximum size 1MB.</p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div id="modal-img-br-container">
                                <div id="uploaded-image-grid" style="margin:0 0 -6px;padding:20px;max-height:600px;overflow-y:auto;background: #efefef;"></div>
                                <input id="input-new-image" name="uploadfile" type="file" style="visibility:hidden; width:0px;" accept="image/jpeg,image/png,image/gif,image/jpg"/>
                                <button class="btn btn-lg btn-primary btn-block" onclick="$('#input-new-image').click();"><i class="fa fa-picture-o"></i><sup>+</sup> Upload New Image</button>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div id="image-crop-container" style="height:600px; margin-bottom:15px;">
                                <img id="image" style="width:100%;">
                            </div>
                            <button id="btn-crop" class="btn btn-block btn-lg btn-primary"><i class="fa fa-crop"></i> Crop & Insert</button>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="docs-preview clearfix">
                                <div class="img-preview preview-lg"></div>
                                <div class="img-preview preview-md"></div>
                                <div class="img-preview preview-sm"></div>
                                <div class="img-preview preview-xs"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align:right;">
                    <!--<button type="button" class="btn btn-primary" id="okButton">Save</button>-->
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/template" id="tpl-uploaded-image">
    <div class="uploaded-image" style="margin:0 15px 15px 0;width:100%;" data-img-url="{{=imgUrl}}" data-img-id="{{=id}}" data-img-name="{{=fileName}}">
        <img src="{{=imgUrl}}" class="" style="width:100%; min-width:100%;">
        <div class="overlay">
            <button class="button-insert btn btn-facebook btn-sm" title="Insert" data-img-url="{{=imgUrl}}" data-img-id="{{=id}}" data-img-name="{{=fileName}}"><i class="fa fa-plus"></i> Insert</button>
            <button class="button-crop btn btn-primary btn-sm" title="Crop" data-img-url="{{=imgUrl}}" data-img-id="{{=id}}" data-img-name="{{=fileName}}"><i class="fa fa-crop"></i> Crop</button>
        </div>
    </div>
</script>

<script type="text/template" id="tpl-modal-alert">
    <div class="modal" id="modal-alert">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title" bgColor="green">{{=title}}</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
    <p>{{=message}}</p>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-primary" id="okButton" data-dismiss="modal">Okay</button>
    <button type="button" class="btn btn-secondary" id="cancelButton" data-dismiss="modal">Close</button>
    </div>
    </div>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-role-l10n-label-GENEREAL_USER">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.GenerealUser"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-LIB_USER">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.LibUser"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-MOC_USER">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.MocUser"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-TRANSLATOR_USER">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.TranslatorUser"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-ADMIN_USER">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.AdminUser"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-METADATA_ADMIN">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.MetadataAdmin"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-MARKETING_MGR">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.MarketingManger"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-ORG_ADMIN">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.OrgAdmin"/></a></li>
</script>

<script type="text/template" id="tpl-role-l10n-label-CURATOR_USER">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.CuratorUser"/></a></li>
</script>
<script type="text/template" id="tpl-role-l10n-label-1">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.privacyPolicy"/></a></li>
</script>
<script type="text/template" id="tpl-role-l10n-label-2">
    <li class="panel-headli" style="padding-left:15px;"><a href="{{=url}}"  class="menu_link"><tags:message code="label.termsAndServices"/></a></li>
</script>