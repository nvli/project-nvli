<%--
    Document   : sidebar
    Created on : Apr 1, 2016, 12:49:08 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    a.menu_link{
        padding: 6px 12px 6px 20px !important;
        width: 100%;
    }
    a.menu_link:hover{
        cursor: pointer !important;
        background: #efefef;
    }
    .panel-sidebar{
        border: 1px solid #ddd;
        margin-bottom: 15px;
    }
    .profile-info{
        /*border:1px solid #ddd;*/
        /*padding-top:15px;*/
        padding-bottom:15px;
        /*margin-bottom: 20px;*/
    }
    .profile-image, .profile-info > p{
        margin-bottom: 10px;
    }
    .panel-heading a{
        padding-left: 20px !important;
    }
    .panel-headli.active{
        /*background: #efefef;*/

    }
    .panel-headli.active>a{
        color: #8c2d10;
    }
    .role-underlined{
        border-bottom: 1px solid #aaa;
    }
    .role-underlined:hover{
        color: #005983 !important;
        border-bottom: 1px solid #0275d8;
    }
    .rotate {

        /* Safari */
        -webkit-transform: rotate(-90deg);

        /* Firefox */
        -moz-transform: rotate(-90deg);

        /* IE */
        -ms-transform: rotate(-90deg);

        /* Opera */
        -o-transform: rotate(-90deg);

        /* Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

    }
</style>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2" id="sidebar-menus">
    <div id="accordion1" class="">
        <div class="clear22"></div>
        <div class="card bg-grey clearfix" id="user-info">
            <div class="profile-info media marginauto p-a-1">
                <div class="text-xs-center profile-image">
                    <a href="${context}/user/profile">
                        <img src="${exposedProps.userImageBase}/${user.profilePicPath}"

                             class="media-object img-thumbnail"
                             alt="${user.firstName} ${user.lastName}"
                             onerror="this.src='${context}/images/profile-picture.png'"/>
                    </a>
                </div>
                <p class="text-xs-center">
                    <a href="${context}/user/profile"><strong class="font-bold">${user.firstName} ${user.lastName}</strong></a>
                </p>
                <p class="text-xs-center"> <i class="fa fa-key rotate"></i> 
                    <c:forEach items="${user.getRoles()}" var="role">
                        <a class="role-underlined">${role.getName()}</a> &nbsp;
                    </c:forEach>
                </p>
            </div>
        </div>
        <div>
            <ul style="margin-top: 0px; border: 1px solid #ddd;" class="panel-headli_ul">
                <li class="panel-headli"><a href="${context}/user/profile" class="menu_link"><strong><span class="fa fa-user"></span> <tags:message code="sb.profile"/></strong></a></li>
                <li class="panel-headli"><a href="${context}/user/dashboard" class="menu_link"><strong><span class="fa fa-dashboard"></span> <tags:message code="sb.dashboard"/></strong></a></li>
                <li class="panel-headli"><a href="${context}/user/notifications" class="menu_link"><strong><span class="fa fa-bell"></span> <tags:message code="sb.notifications"/></strong></a></li>
                <li class="panel-headli"><a href="${context}/user/redirect/publicLists" class="menu_link"><strong><span class="fa fa-list"></span> <tags:message code="sb.public.lists"/> </strong></a></li>
                <li class="panel-headli"><a href="${context}/user/activity/log" class="menu_link"><strong><span class="fa fa-book"></span> <tags:message code="sb.activity.log"/></strong></a></li>
            </ul>
            <div class="panel card m-t-1">
                <div class="card-header-s accordion-toggle collapsed"
                     data-toggle="collapse"
                     data-parent="#user-info"
                     aria-expanded="false"
                     href="#sidebar-user-activity">
                    <tags:message code="sb.general.settings"/>
                </div>


                <div id="sidebar-user-activity" class="panel-collapse collapse" data-level="0">
                    <div class="g-body">
                        <ul style="margin-top: 0px;" class="panel-headli_ul">
                            <li class="panel-headli"><a href="${context}/user/interests" class="menu_link"><strong><span class="fa fa-soccer-ball-o"></span> <tags:message code="profile.setting.interest"/></strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/profession" class="menu_link"><strong><span class="fa fa-gears"></span> <tags:message code="profile.setting.profession"/> </strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/languages" class="menu_link"><strong><span class="fa fa-language"></span> <tags:message code="profile.setting.language"/></strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/subscribe-news" class="menu_link"><strong><span class="fa fa-newspaper-o "></span> <tags:message code="profile.setting.news.papers"/></strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/subscribe-news-channels" class="menu_link"><strong><span class="fa fa-tv"></span> <tags:message code="profile.setting.news.channels"/></strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/theme-preference" class="menu_link"><strong><span class="fa fa-photo"></span> <tags:message code="sb.theme.settings"/></strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/saved/resource/interest" class="menu_link"><strong><span class="fa fa-heart"></span> <tags:message code="sb.favorite.resources"/> </strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/social/settings" class="menu_link"><strong><span class="fa fa-wrench"></span> <tags:message code="sb.social.settings"/></strong></a></li>
                            <li class="panel-headli"><a href="${context}/user/account/settings" class="menu_link"><strong><span class="fa fa-gears"></span> <tags:message code="sb.account.settings"/> </strong></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="panel card m-t-1">
                <div class="card-header-s accordion-toggle collapsed"
                     data-toggle="collapse"
                     data-parent="#user-info"
                     aria-expanded="false"
                     href="#sidebar-user-mystuff">
                    <tags:message code="sb.my.stuff"/>
                </div>
                <div id="sidebar-user-mystuff" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul style="margin-top: 0px;" class="panel-headli_ul">
                            <li class="panel-headli">
                                <a href="${context}/cs/show-articles" class="menu_link"><strong><span class="fa fa-edit"></span> <tags:message code="dashboard.article.heading"/></strong></a>
                            </li>
                            <li class="panel-headli">
                                <a href="${context}/user/suggestedebooks" class="menu_link"><strong><span class="fa fa-book"></span> <tags:message code="sb.free.ebooks"/> </strong></a>
                            </li>
                            <li class="panel-headli">
                                <a href="${context}/user/ebooks" class="menu_link"><strong><span class="fa fa-book"></span>  <tags:message code="sb.eBooks"/> </strong></a>
                            </li>

                            <!--                            <li class="panel-headli">
                                                            <a href="#" class="menu_link"><strong>My e-NewsPaper</strong></a>
                                                        </li>-->
                            <!--                            <li class="panel-headli">
                                                            <a href="#" class="menu_link"><strong>My WatchList</strong></a>
                                                        </li>-->
                            <!--                            <li class="panel-headli">
                                                            <a href="#" class="menu_link"><strong>My Themes</strong></a>
                                                        </li>-->
                            <li class="panel-headli">
                                <a href="${context}/user/lists" class="menu_link"><strong><span class="fa fa-list"></span> <tags:message code="dashboard.saved.list"/> </strong></a>
                            </li>                            
                            <li class="panel-headli">
                                <a href="${context}/user/watch-lists" class="menu_link"><strong><span class="fa fa-eye-slash"></span> <tags:message code="sb.watch-lists"/> </strong></a>
                            </li>
                            <!--                            <li class="panel-headli">
                                                            <a href="${context}/user/specialization" class="menu_link"><strong><span class="fa fa-suitcase"></span> <tags:message code="sb.specializations"/> </strong></a>
                                                        </li>-->
                            <li class="panel-headli">
                                <a href="${context}/user/news-feed" class="menu_link"><strong><span class="fa fa-newspaper-o"></span>  <tags:message code="sb.news.papers"/> </strong></a>
                            <li class="panel-headli">
                                <a href="${context}/user/news-channel" class="menu_link"><strong><span class="fa fa-tv"></span>  <tags:message code="sb.news.channels"/> </strong></a>
                            </li>
                            <li class="panel-headli">
                                <a href="${context}/user/transaction" class="menu_link"><strong><span class="fa fa-history"></span>  <tags:message code="sb.transactions"/> </strong></a>
                            </li>
                            <li class="panel-headli">
                                <a href="${context}/rewards/showRewards" class="menu_link"><strong><span class="fa fa-gift"></span>  <tags:message code="label.myRewards"/> </strong></a>
                            </li>
                            <!--<li class="panel-headli">
                                <a href="${context}/user/download/history" class="menu_link"><strong> My Downloads</strong></a>
                            </li>
                            <li class="panel-headli">
                                <a href="${context}/user/edits" class="menu_link"><strong> My Edits </strong></a>
                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>

            <security:authorize access="hasAnyRole('ADMIN_USER','METADATA_ADMIN')">
                <div class="panel card m-t-1">
                    <div class="card-header-s accordion-toggle collapsed"
                         data-toggle="collapse"
                         data-parent="#user-info"
                         aria-expanded="false"
                         href="#sidebar-admin-activity"><tags:message code="sb.admin.activity"/>
                    </div>
                    <div id="sidebar-admin-activity" class="panel-collapse collapse" data-level="0">
                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli">
                                        <a href="${context}/analytics/admin-dashboard" class="menu_link">
                                            <strong><tags:message code="label.admin.dashboard.title"/> </strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </security:authorize>
                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli">
                                        <a href="${context}/admin" class="menu_link">
                                            <strong><tags:message code="label.admin.dashboard.title"/> 2</strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </security:authorize>
                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli">
                                        <a href="${context}/admin/application-config" class="menu_link">
                                            <strong><tags:message code="sb.admin.application.configuration"/> </strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </security:authorize>



                        <security:authorize access="hasAnyRole('ADMIN_USER', 'METADATA_ADMIN')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli"><a href="${context}/user/manage/invitations"  class="menu_link"> <tags:message code="sb.invited.users"/></a></li>
                                </ul>
                            </div>
                        </security:authorize>

                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli">
                                        <a href="${context}/cs/manage/articles" class="menu_link">
                                            <strong><tags:message code="sb.manage.articles"/> </strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </security:authorize>

                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli">
                                        <a href="${context}/user/manage-roles" class="menu_link">
                                            <strong><tags:message code="sb.manage.roles"/> </strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </security:authorize>
                        <security:authorize access="hasAnyRole('ADMIN_USER', 'METADATA_ADMIN')">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <a href="#manage-user" class="collapsed"
                                       aria-controls="collapseOne"
                                       aria-expanded="false"
                                       data-target="#manage-user"
                                       data-toggle="collapse">
                                        <tags:message code="sb.manage.users"/>
                                    </a>
                                </div>
                                <div id="manage-user" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" data-level="1">
                                    <ul style="margin-top: 0px;" class="left_menu_ul collapse" id="sidebar-usermanagement">

                                        <!--<li class="panel-headli"><a href="${context}/user/manage"  class="menu_link">Manage Users</a></li>-->
                                        <%--<li class="panel-headli"><a href="${context}/user/manage-permissions"  class="menu_link">Manage Permissions</a></li>--%>
                                        <%--<li class="panel-headli"><a href="${context}/user/manage/professions"  class="menu_link">Manage Profession Data</a></li>--%>
                                    </ul>
                                </div>
                            </div>
                        </security:authorize>
                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <a href="#manage-policy" class="collapsed"
                                       aria-controls="collapseOne"
                                       aria-expanded="false"
                                       data-target="#manage-policy"
                                       data-toggle="collapse">
                                        <tags:message code="sb.manage.policies"/>
                                    </a>
                                </div>
                                <div id="manage-policy" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" data-level="1">
                                    <ul style="margin-top: 0px;" class="left_menu_ul collapse" id="sidebar-policy">
<!--                                        <li class="panel-headli"><a href="${context}/policy/manage/privacy-policy"  class="menu_link">Privacy Policy</a></li>
                                        <li class="panel-headli"><a href="${context}/policy/manage/terms-of-use"  class="menu_link">Terms and Conditions Policy</a></li>-->

                                        <%--<li class="panel-headli"><a href="${context}/user/manage-permissions"  class="menu_link">Manage Permissions</a></li>--%>
                                        <%--<li class="panel-headli"><a href="${context}/user/manage/professions"  class="menu_link">Manage Profession Data</a></li>--%>
                                    </ul>
                                </div>
                            </div>
                        </security:authorize>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <a href="#manage-rewards" class="collapsed"
                                   aria-controls="collapseOne"
                                   aria-expanded="false"
                                   data-target="#manage-rewards"
                                   data-toggle="collapse">
                                    <tags:message code="sb.manage.rewards"/>
                                </a>
                            </div>
                            <div id="manage-rewards" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" data-level="1">
                                <ul style="margin-top: 0px;" class="left_menu_ul collapse" id="sidebar-rewards">
                                    <li class="panel-headli"><a href="${context}/rewards/manage/points-details"  class="menu_link"><tags:message code="label.reward.points"/></a></li>
<!--                                    <li class="panel-headli"><a href="${context}/rewards/manage/level-details"  class="menu_link">Level Details</a></li>-->
                                </ul>
                            </div>
                        </div>
                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <a href="#resource_management" class="collapsed"
                                       aria-controls="collapseOne"
                                       aria-expanded="false"
                                       data-target="#resource_management"
                                       data-toggle="collapse">
                                        <tags:message code="sb.resource.management"/>
                                    </a>
                                </div>
                                <div id="resource_management" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour"  data-level="1">
                                    <ul class="panel-headli_ul collapse" id="resource-list">
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/create"  class="menu_link"><tags:message code="res.create"/></a></li>
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/contenttype/list"  class="menu_link"><tags:message code="sb.content.type.list"/></a></li>
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/group/list" class="menu_link"><tags:message code="sb.group.list"/></a></li>
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/thematic/list"  class="menu_link"><tags:message code="sb.theme.list"/></a></li>
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/language/list"  class="menu_link"><tags:message code="sb.language.list"/></a></li>
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/organization/list"  class="menu_link"><tags:message code="label.organization.list"/></a></li>
                                        <li class="panel-headli" style="padding-left: 15px;"><a href="${context}/resource/resourceType/list"  class="menu_link"><tags:message code="sb.resource.register"/></a></li>
                                    </ul>
                                </div>
                            </div>
                        </security:authorize>

                        <security:authorize access="hasAnyRole('ADMIN_USER')">
                            <div class="panel-body">
                                <ul style="margin-top: 0px;" class="left_menu_ul">
                                    <li class="panel-headli">
                                        <a href="${context}/admin/manage/themes" class="menu_link">
                                            <strong><tags:message code="sb.theme.management"/> </strong>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </security:authorize>
                    </div>
                </div>
            </security:authorize>
            <security:authorize access="hasAnyRole('METADATA_ADMIN','CURATOR_USER','LIB_USER')">
                <div class="panel card m-t-1">
                    <div class="card-header-s accordion-toggle collapsed"
                         data-toggle="collapse"
                         data-parent="#user-info"
                         aria-expanded="false"
                         href="#sidebar-curation-activity">
                        <tags:message code="sb.curation.activity"/>
                    </div>
                    <div id="sidebar-curation-activity" class="panel-collapse collapse" data-level="0">
                        <div class="g-body">
                            <ul style="margin-top: 0px;" class="panel-headli_ul">
                                <security:authorize access="hasAnyRole('METADATA_ADMIN')">
                                    <li class="panel-headli"><a href="${context}/curation-activity/show-record-distribution" class="menu_link"><strong> <tags:message code="sb.record.distribution.register"/></strong></a></li>
                                    </security:authorize>
                                    <security:authorize access="hasAnyRole('LIB_USER')">
                                    <li class="panel-headli"><a href="${context}/cs/metadata-curated-record-register/1/15" class="menu_link"><strong><tags:message code="sb.curated.records"/></strong></a></li>
                                    <li class="panel-headli"><a href="${context}/cs/udc-tagged-record-register/1/15" class="menu_link"><strong><tags:message code="sb.udc.tagged.records"/></strong></a></li>
                                            </security:authorize>
                                            <security:authorize access="hasAnyRole('CURATOR_USER')">
                                    <li class="panel-headli"><a href="${context}/curation-activity/curationRegister" class="menu_link"><strong> <tags:message code="sb.record.register"/> </strong></a></li>
                                    </security:authorize>
                            </ul>
                        </div>
                    </div>
                </div>
            </security:authorize>
            <security:authorize access="hasAnyRole('LIB_USER')">
                <div class="panel card m-t-1">
                    <div class="card-header-s accordion-toggle collapsed"
                         data-toggle="collapse"
                         data-parent="#translation-info"
                         aria-expanded="false"
                         href="#sidebar-translation-activity">
                        <tags:message code="sb.translation.activity"/>
                    </div>
                    <div id="sidebar-translation-activity" class="panel-collapse collapse" data-level="0">
                        <div class="g-body">
                            <ul style="margin-top: 0px;" class="panel-headli_ul">
                                <li class="panel-headli"><a href="${context}/cs/udc-translated-tag" class="menu_link"><strong><tags:message code="sb.udc.translated.tags"/></strong></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </security:authorize>
            <!-- ad analytics start-->
            <div class="panel card m-t-1">
                <div class="card-header-s accordion-toggle collapsed"
                     data-toggle="collapse"
                     data-parent="#user-info"
                     aria-expanded="false"
                     href="#sidebar-ad-analytics"> <tags:message code="sb.ad.manager"/>
                </div>
                <div id="sidebar-ad-analytics" class="panel-collapse collapse" data-level="0">
                    <div class="g-body">
                        <ul style="margin-top: 0px;" class="panel-headli_ul">
                            <li class="panel-headli"><a href="${context}/ad/userad" class="menu_link"><span class="fa fa-list-alt"></span><strong> <tags:message code="sb.my.ad"/></strong></a></li>

                            <security:authorize access="hasAnyRole('MARKETING_MGR')">
                                <li class="panel-headli"><a href="${context}/ad/view-approved-rejected-ad" class="menu_link"><span class="fa fa-wrench"></span> <strong> <tags:message code="sb.ad.setting"/></strong></a></li>
                                </security:authorize>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- ad analytics -->
            <%--
            <!-- widgets -->
            <div class="panel card m-t-1">
                <div class="card-header-s accordion-toggle collapsed"
                     data-toggle="collapse"
                     data-parent="#user-info"
                     aria-expanded="false"
                     href="#sidebar-cal-widgets"> <tags:message code="sb.widgets"/>
                </div>
                <div id="sidebar-cal-widgets" class="panel-collapse collapse" data-level="0">
                    <div class="g-body">
                        <ul style="margin-top: 0px;" class="panel-headli_ul">
                            <li class="panel-headli"><a href="${context}/widgets/themeCalWidget" class="menu_link"><strong><tags:message code="sb.calender.widgets"/></strong></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            --%>
        </div>
    </div>
</div>
