<%-- 
    Document   : about-partners
    Created on : 16 Feb, 2018, 11:11:43 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    td.partner-logo{text-align: center;}
    .h-heading{font-weight: bold; color:#444; } 
    h2, h4{ margin-top:15px; font-weight: bold; color: #333;}
    .partner-name{font-size: 18px; color: #333;}
</style>
<section class="container">
    <article style="margin: 30px 0 30px">
        <h2 class="text-center">Partner Institutions</h3>
        <br>
        <h4>Funding</h4>
        <table class="table  table-hover table-striped">
            <tbody>
                <tr>
                    <td class="partner-logo"><img class="logo-partners" style="height:50%;" src="${context}/images/moc-logo128.png"/></td>
                    <td style="vertical-align: middle;" class="partner-name">Ministry of Culture, Government of India, New Delhi</td>
                </tr>
                <tr>
                    <td  class="partner-logo" style="min-width: 110px;">
                        <img class="logo-partners" style="height:50%;" src="${context}/images/nml-logo-128.png"/>
                        <img class="logo-partners" style="height:50%;" src="${context}/images/RRRLF-logo-128.png"/>
                    </td>
                    <td style="vertical-align: middle;" class="partner-name">
                        <p>National Mission on Libraries</p>
                        <p>Raja Rammohan Roy Library Foundation (RRRLF), Kolkata</p>
                    </td>
                </tr>
                <tr><td colspan="2"></td></tr>
            </tbody>
        </table>
        <h4>Project Implementation</h4>
        <table class="table  table-hover table-striped">
            <tbody>
                <tr>
                    <td  class="partner-logo"><img class="logo-partners" style="height:50%;" src="${context}/images/IIT_Bombay_Logo-128.png"/></td>
                    <td style="vertical-align: middle;" class="partner-name">Indian Institute of Technology (IIT), Mumbai</td>
                </tr>
                <tr>
                    <td  class="partner-logo"><img class="logo-partners" style="height:50%;" src="${context}/images/cdac-logo-128.png"/></td>
                    <td style="vertical-align: middle;" class="partner-name">Centre for Development of Advanced Computing (C-DAC), Pune</td>
                </tr>
                <tr>
                    <td  class="partner-logo"><img class="logo-partners" style="height:50%;" src="${context}/images/IGNOU-logo-128.png"/></td>
                    <td style="vertical-align: middle;" class="partner-name">The Indira Gandhi National Open University (IGNOU), New Delhi</td>
                </tr>
                <tr><td colspan="2"></td></tr>
            </tbody>
        </table>
    </article>
</section>
