<%--
    Document   : hyperlink-policy
    Created on : 14 Feb, 2018, 11:56:04 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>.h-heading{font-weight: bold; color:#444; } h4{ margin:20px 0 15px; font-weight: bold; color: #333;}</style>
<section class="container" style="text-align: justify;">
    <br>
    <h2 class="text-center h-heading">Hyperlinking Policy</h2>
    <br>
    <p>This hyperlinking Policy is a part of “Terms of Service and Policies”.  NVLI does not have any objection if “Users” of our “Site” hyperlink the content or information that is hosted on our “Site”. NVLI is not responsible for your usage of the hyperlinked site. NVLI is not endorsing the hyperlinked site or text. We have no control over the hyperlinked site and hence do not give guarantee of availability of the content or information. If you have any queries or suggestions, please contact us.</p>
</section>