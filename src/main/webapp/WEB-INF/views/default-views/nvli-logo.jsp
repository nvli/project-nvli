<%-- 
    Document   : nvli-logo
    Created on : 19 Feb, 2018, 12:02:19 PM
    Author     : Gulafsha Khan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    .logo-container-logo{
        text-align: center;
    }
    .h-heading{
        font-weight: bold; 
        color:#444;
    } 
    h4{ 
        margin:20px 0 15px; 
        font-weight: bold;
        color: #333;
    }
</style>
<ol class="breadcrumb">
    <li><a href="${context}/home"><span class="fa fa-home"></span></a></li>
    <li>NVLI Logo</li>
</ol>
<section class="container">
    <br>
    <article style="margin: 20px 0 30px; color: #333;">
        <h4>NVLI Logo</h4>
        <p style='text-align: justify'>
            The logo of NVLI represents diverse knowledge resources
            in the form of various petals of a flower. The magnifying 
            glass combined with the Sanskrit letter ज्ञ (jña) conveys search, seeking and discovery of knowledge. 
            Representation of various alphabet in Indian languages on every petal represents the multilingual data 
            repositories and multilingual search & retrieval.
        </p>
        <div class="logo-container-logo"><img src="${context}/images/nvli-logo-128.png"></div><br>
        <h4>Motto</h4>
        <p style='text-align: justify; '>The motto of "Rāshtriya Virtual Jnānbhuvan:
            The National Virtual Library of India" encapsulates the vision and the aspiration of Indians which is aptly expressed in Sanskrit as under.</p>
        <p style='text-align: justify;text-align: center;'><b>भारती भा भुवनेषु भातू</b><br> Bhārati bhā bhuvaneshu bhātu<br> Indian knowledge illuminating the universe<br></p><br>
        <p style='text-align: justify;'> NVLI aims to represent the true cultural ethos embodied in the name of our great nation "Bharat" <b>भारत</b>. In Sanskrit language "Bharat" means the land of seekers, the people who are devoted to pursuit of knowledge .
            <br>
        <p style='text-align: justify;text-align: center;'> ( <b>भा</b> = Knowledge, Light, Illumination <b>रत</b> = Devoted).</p>
        </p>
        NVLI aims to provide "single window access" to diverse digital knowledge resources created and maintained by different institutions.
    </article>
</section>