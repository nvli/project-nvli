<%-- 
    Document   : analytics-admin-dashboard
    Created on : Mar 23, 2017, 11:52:54 AM
    Author     : Gulafsha
--%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><tags:message code="label.admin.dashboard.title"/></div>
        <div class="card-block">
            <iframe src="https://static1.nvli.in/piwik/index.php?&token_auth=30660ef66aad219dc4998bdc15460e47&module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=1&period=week&date=yesterday" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="1200px"></iframe>
        </div>
    </div>
</div>
