<%--
    Document   : widget-analytics-view
    Created on : Mar 31, 2017, 5:56:17 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<script type="text/javascript" src="${context}/themes/js/tagcloud/jqxtagcloud.js"></script>
<style>
    .jqx-widget {
        -moz-text-size-adjust: none;
        background-clip: padding-box;
        box-sizing: content-box;
        color: #000000;
        direction: ltr !important;
        font-family: Verdana,Arial,sans-serif;
        font-size: 13px;
        font-style: normal;
    }
    .jqx-widget, .jqx-widget-content, .jqx-widget-header, .jqx-fill-state-normal {
        line-height: 1.231;
    }
    .jqx-tag-cloud ul {
        padding-left: 5px;
    }
    .jqx-tag-cloud-item {
        display: inline-block;
        padding: 3px;
        margin-left: 8px;
    }
    .jqx-tag-cloud-item a {
        text-decoration: initial;
    }
    .jqx-tag-cloud-item a:hover {
        text-decoration: underline;
    }
</style>
<script>
    var __NVLI = __NVLI || {};
    __NVLI['locale'] = "${pageContext.response.locale}";
    __NVLI['analyticsEndpoint'] = "${analyticsEndpoint}";
</script>
<style>
    .trend-item{
        border-bottom: 1px solid #efefef;
        padding:10px 15px;
        text-transform: capitalize;
    }
    .trend-item:hover{
        cursor: pointer;
        background: #eee;
    }
    .loader{
        padding: 20px;
        text-align: center;
    }
</style>

<div class="container80">
    <div style="margin-top:15px;">
        <a class="btn btn-primary btn-sm" href="${context}/analytics/named/entities">Named Entities & records</a>
    </div>
    <hr>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 m-t-1">
            <div class="card card-header">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><tags:message code="label.most.shared.links"/></div>
                </div>
            </div>
            <div class="card card-block">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                    <button type="button" class="btn btn-primary-outline chart-type-btn active" id="yearly"><tags:message code="button.last.year"/></button>
                    <button type="button" class="btn btn-primary-outline chart-type-btn" id="monthly"><tags:message code="button.last.month"/></button>
                    <button type="button" class="btn btn-primary-outline chart-type-btn" id="weekly"><tags:message code="button.last.days"/></button>
                    <button type="button" class="btn btn-primary-outline chart-type-btn" id="daily"><tags:message code="button.last.hour"/></button>
                </div>
                <div style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <ul id="top-resource-items" class="list-group"></ul>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                            <div id="graph-container" style="margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 m-t-1">
            <div class="card card-header">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><tags:message code="label.most.viewedlinks"/></div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <select id="resource-type-select" class="form-control"></select>
                    </div>
                </div>
            </div>


            <div class="card card-block">
                <div style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <div id="resource-items-widget2" style="overflow-x: hidden;">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <div id="graph-container-widget2" style="height: 300px; margin: 0 auto"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 m-t-1">
            <div class="card card-header">
                <div class="row">
                    <div class="col-lg-12 col-md-8 col-sm-8 col-xs-6"><tags:message code="label.most.searched.keyword"/></div>
                </div>
            </div>


            <div class="card card-block">
                <div style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="tagCloud"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
            <div class="col-lg-12 col-md-12 col-sm-12  col-xs-12 m-t-1">
                <div class="card card-header">
                    <span class="fa fa-line-chart"></span> <tags:message code="label.top.trending"/> <span style="color:#444;"><tags:message code="label.last24.hour"/></span>
                </div>
                <div class="card ">
                    <div id="trend-search-container">
                        <div class="loader"><img src="${context}/themes/images/processing.gif"></div>
                    </div>
                </div>
            </div>
            <%--
            <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 m-t-1">
                <div class="card card-header">
                    <span class="fa fa-line-chart"></span> Top Trending - Shares
                </div>
                <div class="card card-block">
                    <div id="trend-share-container"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 m-t-1">
                <div class="card card-header">
                    <span class="fa fa-line-chart"></span> Top Trending - Likes
                </div>
                <div class="card card-block">
                    <div id="trend-likes-container"></div>
                </div>
            </div>
            --%>
        </div>

        <%--
        <div class="col-lg-12 col-md-12 col-sm-12 m-t-1">
            <div class="card card-header">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">You May Like To See</div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <!--                                    <select id="widgetSel3" class="form-control" onchange="return loadWidgetGraph3(this);">
                        <c:forEach items="${listOFResourceType}" var="resourceType">
                            <option id="${resourceType.resourceTypeCode}" value="${resourceType.resourceTypeCode}">${resourceType.resourceType}</option>
                        </c:forEach>
                    </select>-->
                    </div>
                </div>
            </div>

            <div class="card card-block">
                <div style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <ul id="youMayLike">
                            </ul>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <div id="graph-container-widget2" style="overflow-wrap:normal; height: 300px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        --%>

        <%--
        <div class="clearfix"></div>
        <div class="col-lg-12 col-md-12 col-sm-12 m-t-1">
            <div class="card card-header">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">Open Journals</div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                        <select id="widgetSel3" class="form-control" onchange="return loadWidgetGraph3(this);">
                            <c:forEach items="${listOFResourceType}" var="resourceType">
                                <option id="${resourceType.resourceTypeCode}" value="${resourceType.resourceTypeCode}">${resourceType.resourceType}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card card-block">
                <div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="btn-group pull-right">
                                <a id="selected" type="button" class="btn btn-secondary btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Theme</a>
                                <div class="dropdown-menu" id="chart-drop-down-menu">
                                    <a class="dropdown-item chart-type-item-link" href="#" data-theme-id="10" id="mahavir_jayanti" >Resource Type 1</a>
                                    <a class="dropdown-item chart-type-item-link" href="#" data-theme-id="10" id="mahavir_jayanti" >Resource Type 2</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="h4 text-center">The graphics below show year wise counts over time for Biology and Medicine.</div>
                            <br>
                            <div id="bubble-chart" style="text-align: center;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        --%>
        <div class="clearfix" style="margin-bottom:20px;"></div>
    </div>


</div>
<script src="${context}/scripts/widgets/most-searched-keyword.js"></script>
<script type="text/template" id="tpl-top-reviewed-item">
    <li class="list-group-item" style="padding: 10px;">{{=resourceName}}</li>
</script>
<script type="text/template" id="tpl-top-resource-item">
    <div>
    <a style="" href="{{=link}}" title="{{=resourceName}}"><div class="trend-item"><h6 style="margin : 0;display: block;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">{{=i}}. {{=resourceName}}</h6></div></a>
    </div>
</script>

<script type="text/template" id="tpl-top-viewd-resource">
    <a href="{{=url}}" style="" title="{{=resourceName}}"><div class="trend-item"><h6 style="margin : 0;display: block;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">{{=serial}}. {{=resourceName}}</h6></div></a>
</script>

<script type="text/template" id="tpl-search-trend-item">
    <div>
    <a href="{{=permaLink}}"><div class="trend-item"><h5 style="margin : 0;">{{=searchPhrase}}<div id="sparkline-data-{{=i}}" class="pull-right"></div></h5></div></a>
    </div>
</script>

<script type="text/template" id="tpl-resource-type">
    <option id="{{=resourceTypeCode}}" value="{{=resourceTypeCode}}" {{ if(resourceTypeCode === "VIRM"){ }} selected {{ } }}>{{=resourceType}}</option>
</script>
<script src="${context}/components/sparkline/jquery.sparkline.min.js"></script>
<script src="${context}/components/highcharts/highcharts.js"></script>
<script src="${context}/components/highcharts/modules/exporting.js"></script>
<script src="${context}/scripts/widgets/most-visited-resources.js"></script>
<script src="${context}/scripts/widgets/most-shared-resources.js"></script>
<!--<script src="${context}/scripts/widgets/bubble-d3-scripts.js"></script>-->
<!--<script src="${context}/scripts/widgets/bubble-d3-publishing-count.js"></script>-->
<script>
    function renderSearchTrends() {
        $("#trend-search-container").empty();
        $.ajax({
            url: __NVLI.context + "/analytics/ws/trending/search",
            data: {},
            type: 'GET',
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                _.each(response.data, function (log, i, res) {
                    var tpl = _.template($("#tpl-search-trend-item").html());

                    log.permaLink = __NVLI.context + log.permaLink;
                    log['i'] = i;
                    var el = tpl(log);
                    $("#trend-search-container").append(el);
                    $("#sparkline-data-" + i).sparkline(response.trends[log.searchPhrase], {type: 'line', disableTooltips: true, barColor: '#33a1ff', width: "100px", fillColor: "#EEE", lineColor: "#25f", highlightLineColor: null});
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    }

    $(document).ready(function (e) {
        renderSearchTrends();
    });
</script>