<style>

    #chart-container {
        background: whitesmoke;
    }

    #record-container {
        height: 940px; 
        overflow-y: auto;
        width: 100%;
    }

    #button {
        width: 1000em; 
    }

</style>
<script src="//d3js.org/d3.v4.js"></script>

<div class="row">
    <section  class="col-lg-6" style="padding:0;">
        <div class="card-header records-title" id="chart-title">Entity : Persons </div>
        <div id="chart-container"></div>
    </section>
    <section id="record-outer-container" class="col-lg-6" style="padding:0;">
        <div class="card-header records-title" id="records-title">Records</div>
        <div id="record-container" class="list-group" style=""></div>
    </section>
</div>
<script type="text/template" id="tpl_record_data">
    <button type="button" id="{{=record}}" class="list-group-item list-group-item-action" onclick="openLink('{{=record}}')" style="cursor:pointer;"><i class="fa fa-link"></i> {{=name}}</button>
</script>

<script>
    function processData(data) {
        var obj = data.countries_msg_vol;
        var newDataSet = [];
        for (var prop in obj) {
            newDataSet.push({name: prop, className: prop.toLowerCase(), size: obj[prop]});
        }
        return {children: newDataSet};
    }

    function plot(data) {
        var diameter = 960,
                format = d3.format(",d"),
                color = d3.scaleOrdinal(d3.schemeCategory20c);

        var bubble = d3.pack()
                .size([diameter, diameter])
                .padding(1.5);

        var svg = d3.select("#chart-container").append("svg")
                .attr("width", diameter)
                .attr("height", diameter)
                .attr("class", "bubble");

//        d3.json(__NVLI.context + "/json/sample-data.json", function (error, data) {
//        d3.json(jsondata, function (error, data) {
//            if (error) {
//                throw error;
//            }

            var pdata = {"name": "Named Entities", "children": []};
            for (var i = 0; i < data.children.length; i++) {
                var idata = {"name": data.children[i].entityName, "size": data.children[i].frequency, "records": data.children[i].records};
                // console.log(data.children[i].records[2]);
                pdata.children.push(idata);
            }


            var root = d3.hierarchy(classes(pdata))
                    .sum(function (d) {
                        return d.value;
                    })
                    .sort(function (a, b) {
                        return b.value - a.value;
                    });

            bubble(root);
            var node = svg.selectAll(".node")
                    .data(root.children)
                    .enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function (d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    });

            node.append("title")
                    .text(function (d) {

                        return d.data.className + ": " + format(d.value);
                    });

            node.append("circle")
                    .attr("r", function (d) {
                        return d.r;
                    })
                    .style("fill", function (d) {
                        return color(d.value);

                    });

            d3.selectAll(".node").on('click', function (d) {
                renderRecord(d);
                //console.log("bub",d.data.records);
            });

            node.append("text")
                    .attr("dy", ".3em")
                    .style("text-anchor", "middle")
                    .text(function (d) {
                        return d.data.className.substring(0, d.r / 3);
                    });
//        });

        d3.select(self.frameElement).style("height", diameter + "px");
    }

// Returns a flattened hierarchy containing all leaf nodes under the root.
    function classes(root) {
        var classes = [];

        function recurse(name, node) {
            if (node.children)
                node.children.forEach(function (child) {
                    recurse(node.name, child);
                });
            else
                classes.push({packageName: name, className: node.name, value: node.size, records: node.records});
        }

        recurse(null, root);
        return {children: classes};
    }

    function renderRecord(dataObject) {
        var Count = 0;
        $("#record-container").empty();
        $("#records-title").html(dataObject.data.className);
        console.log(dataObject.data.records);
        $.ajax({
            url : __NVLI.context + "/analytics/ws/get/recordInfo",
            data : { 'records' : dataObject.data.records },
            success: function (res, textStatus, jqXHR) {
                
                _.each(res.recordinfo, function (obj) {
                    var template = _.template($("#tpl_record_data").html());
                    Count++;
                    var opts = {
                        sno: Count,
                        name: obj.nameToView,
                        record:obj.recordIdentifier
                };
                var el = template(opts);
                $("#record-container").append(el);
                });
            }
        });

    }

    function openLink(rName) {

        var url = __NVLI.context + "/search/preview/" + rName;
        console.log(url);
        var win = window.open(url, '_blank');

    }

    var getEntiies = function (page) {
        console.log("Fetching DATA");
        $.ajax({
            url: __NVLI.context + "/analytics/ws/get/named/entities/" + page,
            type: 'GET',
            dataType: 'json',
            success: function (res, textStatus, jqXHR) {
                plot(res);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error");
            }
        });
    };

    $(document).ready(function () {
        getEntiies(1);
    });
</script>

