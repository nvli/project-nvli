<%-- 
    Document   : question-answer
    Created on : Sep 5, 2017, 2:48:28 PM
    Author     : vrushali
    Author     : Vivek Bugale<bvivek@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="tags"  %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<ol class="breadcrumb">
    <li>
        <a href="${context}/home">
            <tags:message code="label.home"/>
        </a>
    </li>
    <li class="active">
        <tags:message code="label.question.answering.system"/>
    </li>
</ol>
<div class="m-l-3 m-r-3 m-t-3" id="main">
    <div class="row">
        <div class="col-lg-12">
            <div class="paper clearfix">       
                <div class="col-lg-3 col-sm-12 text-right-cust m-a-0 queAnsIcon">
                    <img src="${context}/themes/images/icon.png"
                         alt="Question Answering System" width="120">
                </div>
                <div class="col-lg-6 p-t-1 p-b-1">
                    <h2 class="text-center">
                        <tags:message code="label.question.answering.system"/>
                        <sup>
                            <tags:message code="label.QA.version"/>
                        </sup>
                    </h2>
                    <div class="input-group asnwer_inputbtn">
                        <input id="question" name="question"
                               onkeypress=""
                               class="form-control p-a-4" 
                               placeholder="Type your question here..."
                               autofocus="true" autocomplete="off" type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-success p-a-4 p-l-1 p-r-1"
                                    id="question-search" type="button">
                                <tags:message code="label.ask.question"/>
                            </button>
                        </span> 
                    </div>
                </div>
              </div>    
        </div>
        <div class="clearfix"></div>            
        <div class="col-lg-12">
            <div id="loading-answers" style="display: none;text-align: center;margin-top: 100px;">
                <span>
                    <img src="${context}/themes/images/processing.gif" alt="Loading..." width="40px"/>
                </span>
            </div>
            <div class="alert alert-warning no-question-asked" style="display:none;text-align: center;margin-top: 20px;">
                Enter your question.
            </div>
            <div class="alert alert-warning no-answer-found" style="display:none;text-align: center;margin-top: 20px;">
                Expected answer is not found !
            </div>
            <div class="alert alert-danger service-down" style="display:none;text-align: center;margin-top: 20px;">
                Service is currently unavailable, please try after sometime.
            </div>
            <div id="container-wrapper">  
                <!--Answer start here-->
                <div id="answer-container"></div>
                <!--Answer end here-->
                <!--Related answer start here-->
                <div id="related-container"></div>
                <!--Related answer start here-->
            </div>
        </div>
        </div><!-- row end here -->
    </div>
</div> 
<script type="text/template" id="tpl-questions-answer">
    <div class="clearfix card m-t-2">
        <div class="card-header clearfix">{{=type}}
            <div class="pull-right"
                <small>
                    <tags:message code="label.total.results"/> : {{=totalResults}}
                </small>
            </div>
        </div>
        <section class="card-block clearfix">
            <div class="col-md-12 answers_links">
                {{ _.each(answerArray,function(answer,i){ if(!_.isEmpty(answer)){ }}        
                    {{ if(i<3){ }}
                        <div class="row">
                    {{ }else{ }}
                        <div class="row answer-row" style="display : none;">
                    {{ } }}
                        <div class="col-xl-2 col-lg-2 col-md-2 col-xs-12 text-right-cust m-a-0">
                            <label class="bg-success answer_block m-r-2">
                                Ans.{{=++i}}
                            </label>
                        </div>
                        <div class="col-xl-9 col-lg-10 col-md-10 col-xs-12 asnwer_content">
                            <p class="comment moreqa">
                                {{=answer}}
                            </p>
                        </div>
                    </div>
                {{ } }); }}
                {{ if(totalResults>3){ }}
                    <div class="row m-b-0">
                        <div class="col-md-11 col-md-offset-1 text-center">
                            <a href="javascript:;" class="btn btn-secondary bg-faded read-more">
                                <b>Read More</b>
                            </a>
                        </div>
                    </div>
                {{ } }}
            </div>        
        </section>
        <div class="clear22"></div>
    </div>
</script>
<script type="text/javascript">                      
    var _langCode = '${pageContext.response.locale}';
    $(document).ready(function () {       
        bindAskQuestion();        
    }); 
    
    $("#question").on("keypress",function (e){
        if (e.keyCode === 13) {
            fetchAnswers();
        }
    });
    
    function bindAskQuestion(){
        $("#question-search").on("click",function(){
           fetchAnswers();
        });
    }  
    
    function fetchAnswers(){
        if($("#question").val()){
            $.ajax({
                   url: "${context}/nac/fetch-answers",
                   type: 'POST',
                   data :{
                       question : $("#question").val()
                   },
                   beforeSend: function () {
                      $("#container-wrapper").hide();
                      $("#answer-container").empty();
                      $("#related-container").empty();
                      $(".no-answer-found").hide();
                      $(".service-down").hide();     
                      $(".no-question-asked").hide();
                      $("#loading-answers").show();
                   },
                   complete: function () {
                      $("#loading-answers").hide();
                   }
               }) 
                   .done(function (response) {
                       if (!_.isEmpty(response)) {
                           $("#container-wrapper").show();
                           if(!_.isEmpty(response.answer)){
                               answerTemplate("answer",response);
                           }
                           if(!_.isEmpty(response.related)){
                               answerTemplate("related",response);
                           }
                           bindReadMoreAnswer();
                           bindReadMore();  
                       }else{
                           $(".no-answer-found").show();                        
                       }
                   })
                   .fail(function (xhr) {
                       $(".service-down").show();
                       console.log('error ::', xhr);
                   });
        }else{
            $(".no-question-asked").show();
        }
    }    
    
    function answerTemplate(key,response){
        $('#'+key+'-container').empty();
        var template_answers = _.template($("#tpl-questions-answer").html());
        var opts={
           type : key==="answer" ? "Answers" : "Related Results",
           totalResults : response[key].length,
           answerArray : response[key]
        };
        var el_answers=template_answers(opts);
        $('#'+key+'-container').append(el_answers);        
    }
    
    function bindReadMore(){
        $(".read-more").on("click", function(){            
            $(".answer-row").show();            
            $(this).hide();
        });        
    }
    
    function bindReadMoreAnswer(){
        var showChar = 500;
	var ellipsestext = "...";
	var moretext = "Read more";
	var lesstext = "Read less";
	$('.moreqa').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {

                    var c = content.substr(0, showChar);
                    var h = content.substr(showChar, content.length - showChar);

                    var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                    $(this).html(html);
            }
	});

	$(".morelink").click(function(){
            if($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(moretext);
            } else {
                    $(this).addClass("less");
                    $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
	});
    }
 </script> 