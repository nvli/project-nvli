<%--
    Document   : features
    Created on : May 31, 2017, 3:02:55 PM
    Author     : Hemant Anjana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>

<div class="container-fluid p-a-2">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-lg-offset-3" style="box-shadow: 0 0 2px 2px #888;padding: 0">
            <div class="card-header"><tags:message code="label.special.feature"/></div>
            <div class="list-group features_text">
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.one"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.two"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.three"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.web.cross"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.four"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.five"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp;<tags:message code="label.web.scale"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.six"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.seven"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.eight"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.nine"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.ten"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.eleven"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.twelve"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.thirteen"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.fourteen"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.fifteen"/></a>
                <a href="#" class="list-group-item"><i class="fa fa-circle" aria-hidden="true"></i> &nbsp; <tags:message code="label.special.sixteen"/></a>
            </div>
        </div>
    </div>
</div>