<%--
    Document   : view-approved-reject-ad
    Created on : May 17, 2017, 3:46:26 PM
    Author     : Nitin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@include file="../../static/components/pagination/pagination.html" %>
<link   href="${context}/components/semantic/transition.min.css" rel="stylesheet" type="text/css" />
<script src="${context}/components/semantic/transition.min.js"></script>

<!--Record Distribution Register starts here-->
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title"><tags:message code="label.advertise.register"/></strong>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <input id="searchString" class="form-control" placeholder="Search">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <select id="statusTypeSel" class="form-control">
                    <option value="0" selected> <tags:message code="label.All"/></option>
                    <option value="4"><tags:message code="label.Approved"/></option>
                    <option value="5"><tags:message code="label.Rejected"/></option>
                    <option value="2"><tags:message code="label.Disabled"/></option>
                    <option value="1"><tags:message code="label.Enabled"/></option>
                    <option value="3"><tags:message code="label.Pending"/></option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><tags:message code="label.sno"/></th>
                    <th><tags:message code="label.Advertisement"/></th>
                    <th><tags:message code="label.lbl.status"/></th>
                    <th><tags:message code="label.Action"/></th>
                    <th><tags:message code="label.View"/></th>
                </tr>
            </thead>
            <tbody id="rd-tbl-body"></tbody>
        </table>
        <div id="advertise-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>

<script type="text/javascript">

    var _dataLimit = 10;

    $(document).ready(function () {
        populateAdTable(1);

        $("#searchString").unbind().bind("keyup", function () {
            populateAdTable(1);
        });
    });

    function populateAdTable(pageNo) {

        $('#loadMore').remove();
        $.ajax({
            url: __NVLI.context + "/ad/getAdByFiltersWithLimit",
            data: {
                searchString: $('#searchString').val(),
                dataLimit: _dataLimit,
                pageNo: pageNo,
                statusId: $('#statusTypeSel').val()
            },
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                var rowCount = ((pageNo - 1) * _dataLimit) + 1;
                var rd_tbl_row_template = _.template($("#tpl-rd-tbl-row").html());
                var opts;
                var rd_tbl_row_el;

                $("#rd-tbl-body").empty();
                _.each(data.adDetailListArr, function (adList) {
                    opts = {
                        "adId": adList.advertiseId,
                        "rowCount": rowCount,
                        "header": adList.header,
                        "status": adList.Status
                    };
                    rd_tbl_row_el = rd_tbl_row_template(opts);
                    $("#rd-tbl-body").append(rd_tbl_row_el);
                    rowCount++;
                });

                renderPagination(populateAdTable, "advertise-tbl-footer", "page-btn", pageNo, data.totalAds, 10, true, 5, true);

                $(".adId .btn-group a").unbind().bind("click", function (e) {

                    var adId = $(this).parent().parent().attr('id');
                    var currentStatus = $(this).attr('id')
                    var statusId;
                    var statusMsg;

                    $("#status" + adId).html()

                    if (currentStatus === "approve") {
                        statusId = 4;
                        statusMsg = "<span class=\"label label-success\">Approved</span>";
                    } else if (currentStatus === "reject") {
                        statusId = 5;
                        statusMsg = "<span class=\"label label-danger\">Rejected</span>";
                    } else if (currentStatus === "enable") {
                        statusId = 1;
                        statusMsg = "<span class=\"label label-info\">Enabled</span>";
                    } else if (currentStatus === "disable") {
                        statusId = 2;
                        statusMsg = "<span class=\"label label-warning\">Disabled</span>";
                    }

                    $.ajax({
                        url: __NVLI.context + "/ad/setAdStatus",
                        data: {
                            statusId: statusId,
                            adId: adId
                        },
                        dataType: 'json',
                        type: 'POST',
                        success: function (data) {
                            if (data === true) {
                                $("#status" + adId).html(statusMsg);

                                $("#alert-box").html('<div class="alert alert-success">Status successfully changed.</div>').fadeIn();
                                $("#alert-box").fadeOut(3000);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            // alert(xhr.status);
                            //  alert(xhr.responseText);
                        }
                    });
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status);
                //alert(xhr.responseText);
                $("div.col-lg-10").replaceWith("<h2>Some thing wrong. Please try after some time...</h2>");
            }
        });
    }

    $("#statusTypeSel").change(function () {

        /*var optionSelected = $("option:selected", this);
         var valueSelected = this.value;
         
         alert("Option Sel:="+optionSelected);
         alert("Value Sele:= "+valueSelected);*/
        populateAdTable(1, true);
    });

</script>

<!--template for record distribution register start here-->
<script type="text/template" id="tpl-rd-tbl-row">
    <tr>
    <td>{{=rowCount}}</td>
    <td><a href="${context}/user/search/history" class="menu_link">{{=header}}</a></td>
    <td>
    <div id="status{{=adId}}">
    {{ if(status === "approved") { }}
    <span class="label label-success">Approved</span>
    {{ } else if(status === "rejected") { }}
    <span class="label label-danger">Rejected</span>
    {{ } else if(status === "enabled") { }}
    <span class="label label-info">Enabled</span>
    {{ } else if(status === "disabled") { }}
    <span class="label label-info">Disabled</span>
    {{ }  else if(status === "pending") { }}
    <span class="label label-warning">Pending</span>
    {{ } else if(status === "deleted") { }}
    <span class="label label-warning">Deleted</span>
    {{ } }}
    </div>
    </td>
    <td>
    <div class="adId" id="{{=adId}}">
    <div class="btn-group" title="Approve">
    <a class="btn-secondary btn" href="javascript:;" id="approve" data-toggle="tooltip" title="Approve"><i class="fa fa-check"> </i></a>
    </div>
    <div class="btn-group" title="Reject">
    <a class="btn-secondary btn" href="javascript:;" id="reject" data-toggle="tooltip" title="Reject"><i class="fa fa-times"></i></a>
    </div>    
    <div class="btn-group" title="Enable">
    <a class="btn-secondary btn" href="javascript:;" id="enable" data-toggle="tooltip" title="Enable"><i class="fa fa-check-circle-o"></i></a>
    </div>
    <div class="btn-group" title="Disable">
    <a class="btn-secondary btn" href="javascript:;" id="disable" data-toggle="tooltip" title="Disable"><i class="fa fa-ban"> </i></a>
    </div>
    </div>
    </td>            
    <td><a class="btn-secondary btn" href="${context}/ad/get-ad-details/{{=adId}}/detail" id="approve" title="View"><i class="fa fa-eye"> </i></a></td>
    </tr>
</script>


