<%--
    Document   : ad-details.jsp
    Created on : May 19, 2017, 4:56:15 PM
    Author     : Nitin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:choose>
        <c:when test="${resultStatus=='error'}">
            <h2> <tags:message code="label.something.wrong"/></h2>
        </c:when>
        <c:otherwise>
            <div class="card">
                <div id="alert-box" style="display: none;"></div>
                <div class="card-header row" style="display: block; margin: 0">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                        <strong class="header-title"> <tags:message code="label.addetail"/></strong>
                    </div>
                </div>
                <div id="success-alert" class="alert alert-success col-md-3 col-md-offset-5 text-center" ><strong> <tags:message code="label.success"/></strong> <tags:message code="label.status"/></div>
                <div class="container-fliud">
                    <div class="wrapper row">
                        <div class="preview col-md-6">

                            <div class="preview-pic tab-content">
                                <div class="tab-pane active" id="pic-1"><img src="${context}/adBaseLocation/${adDetails.relativePath}/${adDetails.mediaName}" width="80%" height="80%"></div>
                            </div>
                        </div>
                        <br>
                        <div class="details col-md-6">
                            <h3 class="product-title">${adDetails.header}</h3>

                            <p class="product-description">${adDetails.description}</p>
                            <br>
                            <h5 class="vote"> <tags:message code="label.Advertiser"/>
                                <small><span class="size">${advertiseUser.firstName} ${advertiseUser.lastName}</span></small>
                            </h5>
                            <h5 class="vote"> <tags:message code="label.MimeType"/> ${adDetails.mimeTypeName}</h5>

                            <h5 class="sizes"> <tags:message code="label.tags"/>
                                <small><span class="size">${adDetails.tags}</span></small>
                            </h5>

                            <h5 class="sizes"> <tags:message code="label.Dimesion"/>
                                <small><span class="size">${adDetails.width}  X ${adDetails.height}</span></small>
                            </h5>

                            <h5 class="colors"> <tags:message code="label.Creation"/>
                                <span> <small>${adDetails.creationDate}</small></span>
                            </h5>
                            <h5 class="colors"> <tags:message code="label.Category"/>
                                <span> <small>${adDetails.categoryName}</small></span>
                            </h5>
                            <h5 class="colors"> <tags:message code="label.url"/>
                                <span class="color green"> <small><a style="color: #2a5cea;" href="${adDetails.destinationUrl}" target="_blank">${adDetails.destinationUrl} </a></small></span>
                            </h5>
                            <c:if test="${view=='detail'}">
                                <div class="action">
                                    <h5> <tags:message code="label.status"/></h5>
                                    <div class="col-lg-3">
                                        <select id="statusTypeSel" class="combobox form-control">
                                            <option value="4"> <tags:message code="label.Approved"/></option>
                                            <option value="5"> <tags:message code="label.Rejected"/></option>
                                            <option value="2"> <tags:message code="label.Disabled"/></option>
                                            <option value="1"> <tags:message code="label.Enabled"/></option>
                                            <option value="3"> <tags:message code="label.Pending"/></option>
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-success" onclick="return updateStatus();"> <tags:message code="label.submit"/></button>
                                    <a href="${context}/ad/view-approved-rejected-ad"><button type="button" class="btn btn-primary"> <tags:message code="label.Cancel"/></button></a>
                                </div>
                            </c:if>
                            <br>
                        </div>

                    </div>
                </div>
            </div>

        </c:otherwise>
    </c:choose>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#statusTypeSel").val('${adDetails.statusId}');
        $("#success-alert").hide()();
    });

    function updateStatus() {

        var statusId = $("#statusTypeSel").val();
        var adId = ${adDetails.advertiseId};
        $.ajax({
            url: __NVLI.context + "/ad/setAdStatus",
            data: {
                statusId: statusId,
                adId: adId
            },
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                if (data === true) {
                    $("#success-alert").slideDown();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert(xhr.status);
                //  alert(xhr.responseText);
            }
        }).done(function () {
            window.setTimeout(function () {

                $("#success-alert").slideUp(500, function () {
                    $("#success-alert").hide();
                    window.location.href = '${context}/ad/view-approved-rejected-ad';
                });
            }, 2000);
        });
    }
</script>
