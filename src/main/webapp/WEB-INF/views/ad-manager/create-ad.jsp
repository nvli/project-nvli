<%--
    Document   : create-ad
    Created on : May 5, 2017, 12:09:55 PM
    Author     : Doppa Srinivas
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />

      <!-- Dashboard Main Contents -->
      <style>
    #interest-tab-content .tab-content>.active{
        padding-right: 0px;
        padding-left: 0px;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" style="display: none;"></div>
    <div class="card">
        <div class="card-header"> <tags:message code="label.Advertisement"/> </div>
        <div class="tabs m-a-0 p-a-0">
            <form:form name="adForm" id="adForm" enctype="multipart/form-data" action="${context}/ad/saveAd" modelAttribute="adForm" method="post" autocomplete="off">
                <div class="tab-content">              
                    <div id="tab1" class="tab active">
                        <div class="row"><!-- inserted new row here -->
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="card">
                                    <div class="card-header"><tags:message code="label.Basic.Information"/></div>
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <fieldset class="form-group row"><!-- added row to each form-group -->
                                                    <label for="head" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong> <tags:message code="label.title"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                        <input id="head" name="header" placeholder="Header" class="form-control" type="text">
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group row">
                                                    <label for="description" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong> <tags:message code="label.description"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                      <input id="description" name="description" placeholder="description" class="form-control" type="text">
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group row">
                                                    <label for="tags" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong><tags:message code="label.Tag.keywords"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                      <input id="tags" name="tags" placeholder="Tag keywords" class="form-control" type="text">
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group row">
                                                    <label for="destinationURL" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong><tags:message code="label.Destination.URL"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                      <input id="destinationURL" name="destinationUrl" placeholder="URL" class="form-control" type="text">
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header"><tags:message code="label.Media"/></div>
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <fieldset class="form-group row">
                                                    <label for="adMediaType" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong><strong> <tags:message code="label.Content"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                        <select class="form-control" id="adMediaType">
                                                            <option value="0" selected="selected"> <tags:message code="label.Select"/></option>
                                                            <c:forEach items="${contentTypes}" var="contentType">
                                                                <option value="${contentType.key}" >${contentType.value}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group row">
                                                    <label for="mimeType" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong><tags:message code="label1.Mime.Type"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                        <form:select  path="mimeType" class="form-control" id="mimeType"> </form:select>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group row">
                                                    <label for="dimension" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><strong> <tags:message code="label.Dimension"/></strong></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                        <form:select path="dimension" class="form-control" id="dimension" onchange="setDimension();">
                                                        <%--<form:option value="0">Original</form:option>--%>
                                                            <c:forEach items="${dimensions}" var="dimension">
                                                                <form:option value="${dimension.key}" >${dimension.value}</form:option>
                                                            </c:forEach>
                                                        </form:select>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group row">
                                                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"></label>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                                                        <input id="fileUpload" name="file" type="file">
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div> 



                                                     <!--
                                <div class="card">
                                <div class="card-header">Targeted User</div>
                                <div class="">
                                    <div class="tabs" style="margin-top: 0;padding:0 !important;">
                                        <ul id="myTab" class="nav nav-tabs font-weight-bold">
                                            <li class="nav-item"> <a class="nav-link active" >Basic</a> </li>
                                            <li class="nav-item"> <a class="nav-link" ><tags:message code="profile.setting.interest"/></a> </li>
                                            <li class="nav-item"> <a class="nav-link" href="${context}/ad/user/profession"><tags:message code="profile.setting.profession"/></a> </li>
                                            <li class="nav-item"> <a class="nav-link" href="${context}/ad/user/languages"><tags:message code="profile.setting.language"/></a> </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="interest-tab-content" class="tab active">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-8 col-lg-8">
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <div class="row">
                                                                    <div class="col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">
                                                                        <fieldset class="form-group">
                                                                            <label for="gender"><strong>Gender</strong></label>
                                                                            <form:select path="gender" class="form-control" id="gender-id">
                                                                                <form:option value="all">All</form:option>
                                                                                <form:option value="male">Male</form:option>
                                                                                <form:option value="Female">Female</form:option>
                                                                            </form:select>
                                                                        </fieldset>
                                                                        <fieldset class="form-group">
                                                                            <label for="age-from"><strong>Age From</strong></label>
                                                                            <input  type="text" class="form-control" id="age-from" onchange="">
                                                                            <label for="age-to"><strong>To</strong></label>
                                                                            <input  type="text" class="form-control" id="age-to" onchange="">
                                                                        </fieldset>
                                                                        <fieldset class="form-group" hidden>
                                                                            <label for="country"><strong>Country</strong></label>
                                                                            <input type="hidden" value="INDIA" id="user-country">
                                                                            <form:select class="form-control" required="required" path="country" id="countries">
                                                                            </form:select>
                                                                            <form:errors path="country" element="div" cssClass="text-danger"/>
                                                                        </fieldset> 
                                                                        <fieldset class="form-group">
                                                                            <label for="states"><strong>States</strong></label>
                                                                            <form:select class="form-control"  path="state" id="states">
                                                                            </form:select>
                                                                            <form:errors path="state" element="div" cssClass="text-danger"/>
                                                                        </fieldset>
                                                                         <fieldset class="form-group">
                                                                            <label for="district"><strong>Districts</strong></label>
                                                                            <form:select class="form-control" path="district" id="district">
                                                                            </form:select>
                                                                            <form:errors path="city" element="div" cssClass="text-danger"/>
                                                                        </fieldset>
                                                                         <fieldset class="form-group">
                                                                            <label for="city"><strong>Cities</strong></label>
                                                                            <form:select class="form-control"  path="city" id="city">
                                                                            </form:select>
                                                                            <form:errors path="city" element="div" cssClass="text-danger"/>
                                                                        </fieldset>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                                                        -->
                            </div>  

                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card">
                                    <div class="card-header"><tags:message code="label.Ad.Preview"/></div>
                                    <div class="card-block">
                                        <div class="form-group row prof_delet ad_imgPreview text-center">
                                            <a href="javascript:;" id="data" class="col-lg-12"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>       
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-success btn-sm"> <tags:message code="label.save"/></button>
                    <a href="${context}/ad/userad">
                        <button type="button" class="btn btn-primary btn-sm"> Cancel</button>
                    </a> 
                </div>
            </form:form>        
        </div>
    </div>
</div>
<script>
    var contentTypeId;
    var dimensionId = '0';
    var mimeType;
    var errorMsg = '';
    var urlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    var isFormValid = true;
    $(document).ready(function () {
        populateCountries();
        $('#countries').change(function (e) {
            populateStates(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("data-id"));
        });
        $('#states').change(function (e) {
            populateDistrict(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("state-id"));
        });
        $('#district').change(function (e) {
            populateCity(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("district-id"));
        });
        $("#adMediaType").change(function (e) {
            document.getElementById('data').innerHTML = '';
            $('#mimeType').empty();
            $('#alert-box').empty();
            $("#fileUpload").replaceWith($("#fileUpload").val('').clone(true));
            contentTypeId = document.getElementById("adMediaType").value;
            if (contentTypeId === '0') {
                errorMsg = 'please select valid content type';
                error();
                return;
            }
            $.ajax({
                url: '${context}/ad/getMimeTypes',
                type: "get",
                data: {
                    'contentTypeId': contentTypeId
                },
                success: function (response)
                {
                    document.getElementById('mimeType').innerHTML = '';
                    if (Object.keys(response).length !== 0) {
                        for (var key in  response) {
                            $('#mimeType').append($('<option>',
                                    {
                                        value: key,
                                        text: response[key]
                                    }));
                        }
                    } else {
                        errorMsg = 'No mime type found..please create one mime type';
                        error();
                    }
                },
                error: function ()
                {

                }
            });
        });
        $('input[type=file]').change(function (e)
        {
            $('#alert-box').empty();
            document.getElementById('data').innerHTML = '';
            contentTypeId = document.getElementById("adMediaType").value;
            mimeType = document.getElementById("mimeType").value;
            if (contentTypeId === '0') {
                errorMsg = 'please select valid content type';
                error();
                return;
            }
            if (mimeType.trim() === '' || $('#mimeType > option').length === '0') {
                errorMsg = 'No mime type found..please create one mime type';
                error();
                return;
            }
            if (checkFile())
            {
                var reader = new FileReader(),
                        files = e.dataTransfer ? e.dataTransfer.files : e.target.files,
                        i = 0;
                reader.onload = onFileLoad;
                while (files[i])
                    reader.readAsDataURL(files[i++]);
            }
        });
        $("#adForm").submit(function () {
            isFormValid = true;
            validateForm();
            return isFormValid;
        });
        //clicking the content this will be called
        $('#data').click(function (e) {
            $('#alert-box').empty();
            var destinationURL = document.getElementById('destinationURL').value.trim();
            if (destinationURL === '' || destinationURL.length === 0) {
                errorMsg = 'Please give a destination url before you click';
                error();
            } else if (!urlRegex.test(destinationURL)) {
                errorMsg = 'Please give a valid destination url';
                error();
            } else {
                window.open(destinationURL);
            }
        });
    });
    function onFileLoad(e)
    {
        contentTypeId = document.getElementById("adMediaType").value;
        dimensionId = document.getElementById('dimension').value;
        var dimension = $("#dimension option[value='" + dimensionId + "']").text().split('*');
        var data = e.target.result;
        if (contentTypeId === '1') {
            document.getElementById('data').innerHTML = '<img src="" alt="" id="imageId">';
            $('#imageId').attr("src", data);
            if (dimensionId !== '0') {
                $('#imageId').attr("width", dimension[1]);
                $('#imageId').attr("height", dimension[0]);
            }
            //to get original height and width
//            var image = new Image();
//            image.src = $('#imageId').attr("src");
//            image.onload = function() {
//            console.log('height: ' + this.height+" width.. "+this.width);
//            };
        } else if (contentTypeId === '2') {
            document.getElementById('data').innerHTML = '<video controls="" controlsList="nodownload" class="avcontrol" id="videoTagId" ><source src="" id="videoId">Your browser does not support the video tag.</video>';
            $('#videoId').attr("src", data);
            if (dimensionId !== '0') {
                $('#videoTagId').attr("width", dimension[1]);
                $('#videoTagId').attr("height", dimension[0]);
            }
            //to get original height and width
//            var videoTagRef=document.getElementById('videoTagId')
//            videoTagRef.addEventListener('loadedmetadata', function(e){
//    console.log(videoTagRef.videoWidth, videoTagRef.videoHeight);
//});
        }
    }
    function setDimension() {
        contentTypeId = document.getElementById("adMediaType").value;
        dimensionId = document.getElementById('dimension').value;
        var dimension = $("#dimension option[value='" + dimensionId + "']").text().split('*');
        if (contentTypeId === '1') {
            if (dimensionId !== '0') {
                $('#imageId').attr("width", dimension[1]);
                $('#imageId').attr("height", dimension[0]);
            } else {
                $('#imageId').removeAttr("width");
                $('#imageId').removeAttr("height");
            }
        } else if (contentTypeId === '2') {
            if (dimensionId !== '0') {
                $('#videoTagId').attr("width", dimension[1]);
                $('#videoTagId').attr("height", dimension[0]);
            } else {
                $('#videoTagId').removeAttr("width");
                $('#videoTagId').removeAttr("height");
            }
        }
    }
    function checkFile() {
        $('#alert-box').empty();
        //var iSize = 0;
        // iSize = ($("#fileUpload")[0].files[0].size / 1024);
        // document.getElementById("imageErrMSG").innerHTML="";

        //if file is less than 100KB allowed to process
//        if (iSize < 100)
//        {
        mimeType = $("#mimeType option[value='" + document.getElementById("mimeType").value + "']").text().trim();
        var name = $("#fileUpload")[0].value;
        //console.log($("#fileUpload")[0].files[0].size / 1024);
        var ext = name.substring(name.lastIndexOf(".") + 1, name.length);
        if (mimeType.toUpperCase() !== ext.toUpperCase())
        {
            $("#fileUpload")[0].value = "";
            errorMsg = 'you have selected ' + mimeType + ' mimetype but uploaded ' + ext + ' mimetype';
            error();
            return false;
        }
        //}
        return true;
    }
    function error() {
        $("#alert-box").html('<div class="alert alert-danger">' + errorMsg + '</div>').fadeIn();
        $("#alert-box").fadeOut(3000);
    }
    function validateForm() {
        $('#alert-box').empty();
        var header = document.getElementById('head').value.trim();
        var description = document.getElementById('description').value.trim();
        var tags = document.getElementById('tags').value.trim();
        var destinationURL = document.getElementById('destinationURL').value.trim();
        contentTypeId = document.getElementById('adMediaType').value.trim();
        mimeType = document.getElementById('mimeType').value.trim();
        dimensionId = document.getElementById('dimension').value.trim();
        if (header === '' || header.length === 0) {
            errorMsg = 'Please give a title to the advertisement';
            error();
            isFormValid = false;
            return;
        }
        if (description === '' || description.length === 0) {
            errorMsg = 'Please about describe the advertisement';
            error();
            isFormValid = false;
            return;
        }
        if (tags === '' || tags.length === 0) {
            errorMsg = 'Please tag at least one keyword';
            error();
            isFormValid = false;
            return;
        }
        if (destinationURL === '' || destinationURL.length === 0) {
            errorMsg = 'Please give a destination url';
            error();
            isFormValid = false;
            return;
        }
        if (!urlRegex.test(destinationURL)) {
            errorMsg = 'Please give a valid destination url';
            error();
            isFormValid = false;
            return;
        }
        if (contentTypeId === '' || contentTypeId.length === '0' || contentTypeId === '0') {
            errorMsg = 'Please seelect a valid content type';
            error();
            isFormValid = false;
            return;
        }
        if (mimeType === '' || mimeType.length === '0' || $('#mimeType > option').length === '0') {
            errorMsg = 'No mime type found..please create one mime type';
            error();
            isFormValid = false;
            return;
        }
        if (dimensionId === '' || dimensionId.length === '0') {
            errorMsg = 'Please select dimension of advertisement';
            error();
            isFormValid = false;
            return;
        }
        var files = document.getElementById('fileUpload').files;
        if (files.length === 0) {
            errorMsg = 'Please upload a file';
            error();
            isFormValid = false;
            return;
        }
    }
    function  populateCity(district) {
        $("#city").empty();
        $("#city").append("<option value='' selected>Select</option>");
        $.ajax({
            url: __NVLI.context + "/api/fetch/city/" + parseInt(district),
            dataType: 'json',
            type: 'GET',
            success: function (city) {
                $.each(city, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.cityname);
                    $(opt).attr("value", option.cityname);
                    $(opt).attr("district-id", option.cityid);
                    $('#city').append(opt);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function  populateDistrict(state) {
        $("#district").empty();
        $("#city").empty();
        $("#district").append("<option value='' selected>Select</option>");
        $.ajax({
            url: __NVLI.context + "/api/fetch/district/" + parseInt(state),
            dataType: 'json',
            type: 'GET',
            success: function (district) {
                $.each(district, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.districtname);
                    $(opt).attr("value", option.districtname);
                    $(opt).attr("district-id", option.districtid);
                    $('#district').append(opt);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function  populateStates(country) {
        $("#states").empty();
        $("#district").empty();
        $("#city").empty();
        $("#states").append("<option value='' selected>Select</option>");
        $.ajax({
            url: __NVLI.context + "/api/fetch/states/" + parseInt(country),
            dataType: 'json',
            type: 'GET',
            success: function (states) {
                $.each(states, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.statename);
                    $(opt).attr("value", option.statename);
                    $(opt).attr("state-id", option.stateid);
                    $('#states').append(opt);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {console.log("error");
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function  populateCountries() {
        /*$("#countries").empty();
        $("#states").empty();
        $("#district").empty();
        $("#city").empty();
        $.ajax({
            url: __NVLI.context + "/api/fetch/countries",
            dataType: 'json',
            type: 'GET',
            success: function (countries) {
                $.each(countries, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.countryname);
                    $(opt).attr("value", option.countryname);
                    $(opt).attr("data-id", option.countryid);
                    $('#countries').append(opt);
                });
                var userCountry = $("#user-country").val();
                $('#countries').val(userCountry);
                var country = document.getElementById('countries');
                var id = country.options[country.selectedIndex].getAttribute('data-id');
                console.log(id);
                if (!_.isEmpty(id)) {
                    populateStates(id);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });*/
    }
</script>