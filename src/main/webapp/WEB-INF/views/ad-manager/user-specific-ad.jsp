<%--
    Document   : user-specific-ad
    Created on : July 11, 2017, 3:46:26 PM
    Author     : Nitin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@include file="../../static/components/pagination/pagination.html" %>
<link   href="${context}/components/semantic/transition.min.css" rel="stylesheet" type="text/css" />
<script src="${context}/components/semantic/transition.min.js"></script>

<!--Record Distribution Register starts here-->
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" style="display: none;"></div><!-- inserted alert before card -->
    <div class="card">          
        <div class="card-header cardHeaderInputMargin">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-8"> <strong class="header-title"><tags:message code="sb.my.ad"/></strong>&nbsp;&nbsp;
                    <a href="${context}/ad/createNewAd" class="btn btn-success btn-sm"><tags:message code="sb.create.new.ad"/></a> 
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-2">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-2">
                    <select id="statusTypeSel" class="form-control form-control-sm">
                        <option value="0" selected="selected"> <tags:message code="label.All"/></option>
                        <option value="4"><tags:message code="label.Approved"/></option>
                        <option value="5"><tags:message code="label.Rejected"/></option>
                        <option value="2"><tags:message code="label.Disabled"/></option>
                        <option value="1"><tags:message code="label.Enabled"/></option>
                        <option value="3"><tags:message code="label.Pending"/></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="8%"><tags:message code="label.sno"/></th>
                        <th><tags:message code="label.Advertisement"/></th>
                        <th><tags:message code="label.lbl.status"/></th><!-- removed view coulumn -->
                        <th width="10%"><tags:message code="label.View"/></th>
                        <th width="10%"><tags:message code="label.action"/></th>
                    </tr>
                </thead>
                <tbody id="rd-tbl-body"></tbody>
          `</table>
        </div>
        <div id="advertise-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
                    
 <script type="text/javascript">
    var _dataLimit = 10;
    $(document).ready(function () {
        var status = "${status}";
        if (status === "success") {
            $("#alert-box").html('<div class="alert alert-success">Advertise successfully saved.</div>').fadeIn();
            $("#alert-box").fadeOut(4000);
        } else if (status === "error") {
            $("#alert-box").html('<div class="alert alert-success">Something went wrong. please try after some time.</div>').fadeIn();
            $("#alert-box").fadeOut(4000);
        }
        populateAdTable(1);

        $("#searchString").unbind().bind("keyup", function () {
            populateAdTable(1);
        });
    });

    function populateAdTable(pageNo) {
        $.ajax({
            url: __NVLI.context + "/ad/getUserAdByFiltersWithLimit",
            data: {
                searchString: $('#searchString').val(),
                dataLimit: _dataLimit,
                pageNo: pageNo,
                statusId: $('#statusTypeSel').val()
            },
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                if (data !== null) {

                    if (_.isEmpty(data.adDetailListArr)) {
                        $("#rd-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var rowCount = ((pageNo - 1) * _dataLimit) + 1;
                        var rd_tbl_row_template = _.template($("#tpl-rd-tbl-row").html());
                        var opts;
                        var rd_tbl_row_el;

                        $("#rd-tbl-body").empty();

                        _.each(data.adDetailListArr, function (adList) {
                            opts = {
                                "adId": adList.advertiseId,
                                "rowCount": rowCount,
                                "header": adList.header,
                                "status": adList.Status
                            };
                            rd_tbl_row_el = rd_tbl_row_template(opts);
                            $("#rd-tbl-body").append(rd_tbl_row_el);
                            rowCount++;
                        });

                        renderPagination(populateAdTable, "advertise-tbl-footer", "page-btn", pageNo, data.totalAds, 10, true, 5, true);

                        $("a span").unbind().bind("click", function (e) {

                            var adId = $(this).closest('div').attr('id');
                            var statusId;
                            var statusMsg;

                            if ($(this).parent().attr('id') === "approve") {
                                statusId = 4;
                                statusMsg = "<div class=\"alert-success col-md-4 col-md-offset-4\" align=\"center\"><strong>Approved</strong></div>";
                            } else if ($(this).parent().attr('id') === "reject") {
                                statusId = 5;
                                statusMsg = "<div class=\"alert-info col-md-4 col-md-offset-4\" align=\"center\"><strong>Rejected</strong></div>";
                            } else if ($(this).parent().attr('id') === "enable") {
                                statusId = 1;
                                statusMsg = "<div class=\"alert-warning col-md-4 col-md-offset-4\" align=\"center\"><strong>Enabled</strong></div>";
                            } else if ($(this).parent().attr('id') === "disable") {
                                statusId = 2;
                                statusMsg = "<div class=\"alert-danger col-md-4 col-md-offset-4\" align=\"center\"><strong>Disabled</div>";
                            }

                            $.ajax({
                                url: __NVLI.context + "/ad/setAdStatus",
                                data: {
                                    statusId: statusId,
                                    adId: adId
                                },
                                dataType: 'json',
                                type: 'POST',
                                success: function (data) {
                                    if (data === true) {
                                        $("#status" + adId).html(statusMsg);
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    // alert(xhr.status);
                                    //  alert(xhr.responseText);
                                }
                            });
                        });
                    }
                } else {
                    $("div.col-lg-10").replaceWith("<h2>Some thing wrong. Please try after some time...</h2>");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(xhr.status);
                //alert(xhr.responseText);
                $("div.col-lg-10").replaceWith("<h2>Some thing wrong. Please try after some time...</h2>");
            }
        });
    }

    $("#statusTypeSel").change(function () {
        populateAdTable(1);
    });

    function deleteAd(adId) {
        if (confirm("Are you sure to delete?")) {
            $.ajax({
                url: __NVLI.context + "/ad/ad-delete",
                data: {
                    adId: adId
                },
                type: 'POST',
                success: function (response) {
                    $("#alert-box").html('<div class="alert alert-success">Advertise successfully deleted.</div>').fadeIn();
                    $("#alert-box").fadeOut(3000);

                    window.setTimeout(function () {
                        location.href = __NVLI.context + "/ad/userad";
                    }, 3000);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    location.href = __NVLI.context + "/ad/userad";
                }
            });
        }
    }

</script>

<!--template for record distribution register start here-->
<script type="text/template" id="tpl-rd-tbl-row">
    <tr>
        <td>{{=rowCount}}</td>
        <td><a href="${context}/user/search/history" class="menu_link">{{=header}}</a></td>
        <td>
            <div id="status{{=adId}}">
                {{ if(status === "approved") { }}
                    <span class="label label-success">Success</span>
                {{  } else if(status === "rejected") { }}
                    <span class="label label-danger">Rejected</span>
                {{ } else if(status === "enabled") { }}
                    <span class="label label-danger">Rejected</span>
                {{ } else if(status === "disabled") { }}
                    <span class="label label-danger">Rejected</span>
                {{ } else if(status === "unpublish") { }}
                    <a href="javascript:;"> <button type="button" class="btn btn-primary col-md-4 col-md-offset-4"> Publish</button></a>
                {{ } else if(status === "pending") { }}
                    <span class="label label-warning">Pending</span>
                {{ }  }}                    
            </div>
        </td>
        <td>
            <div class="btn-group" title="View">
            <a class="btn-secondary btn btn-sm" href="${context}/ad/get-ad-details/{{=adId}}/view" id="ad_view" id="approve" title="View"><i class="fa fa-eye"></i></a>
            </div>
        </td>
        <td>
            <div class="btn-group" title="Edit">
                <a class="btn-secondary btn btn-sm" href="${context}/ad/get-ad-details/{{=adId}}/edit" id="approve" title="Edit"><i class="fa fa-pencil"> </i></a>
            </div>
            <div class="btn-group" title="Delete">
                <a class="btn-secondary btn btn-sm" href="javascript:;" id="ad_delete" onClick="return deleteAd({{=adId}});" title="Delete"><i class="fa fa-trash"> </i></a>                               
            </div
        </td>
    </tr>
</script>