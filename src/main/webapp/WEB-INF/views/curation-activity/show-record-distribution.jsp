<%--
    Document   : show-record-distribution.jsp
    Created on : Oct 18, 2016, 5:59:29 PM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />

<!--Record Distribution Register starts here-->
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title"><tags:message code="sb.record.distribution.register"/></strong>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <input id="searchString" class="form-control" placeholder="Search Resource">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <select id="resourceTypeFilter" class="form-control">
                    <option value="0" selected><tags:message code="label.All"/></option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="3%"><tags:message code="label.sno"/></th>
                    <th><tags:message code="label.resource"/></th>
                    <th width="8%"><tags:message code="label.total.records"/></th>
                    <th width="12%"><tags:message code="label.undistributed.records"/></th>
                    <th width="11%"><tags:message code="label.distributed.records"/></th>
                    <th width="10%"><tags:message code="label.assigned.curators"/></th>
                    <th width="13%"><tags:message code="label.assigned.library.expert"/></th>
                    <th width="10%" style="text-align:center"><tags:message code="label.action"/></th>
                </tr>
            </thead>
            <tbody id="rd-tbl-body"></tbody>
        </table>
    </div>
</div>
<!--Record Distribution Register ends here-->

<!--Modal for Curator Statistics start here-->
<div id="show-resource-statistics-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <tags:message code="label.statistics"/> of <span id="resourceNameSpan" style="text-transform: capitalize; color: #1200ff;"></span></h4>
            </div>
            <div class="" id="show-resource-statistics-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!--Modal for Curator Statistics ends here-->

<!--template for curator statistics starts here-->
<script type="text/template" id="tpl-resource-statistics">
    <table class="table table-bordered table-striped">
    <thead>
    <tr>
    <th> SR. No.</th>
    <th> <tags:message code="label.username"/></th>
    <th> <tags:message code="label.assigned.record"/></th>
    <th> <tags:message code="label.curated.records"/></th>
    <th> <tags:message code="label.uncurated.records"/></th>
    </tr>
    </thead>
    <tbody>
    {{ _.each(userList, function (user,i) { }}
    <tr>
    <td> {{=i+1}}</td>
    <td style="text-transform: capitalize;"> {{=user.name}}</td>
    {{ if(typeof statisticsMap[user.id]==='undefined') { }}
    <td>0</td>
    <td>0</td>
    <td>0</td>
    {{ } else {  }}
    <td> {{=statisticsMap[user.id].assignedCount}}</td>
    <td> {{=statisticsMap[user.id].curatedCount}}</td>
    <td> {{=(parseInt(statisticsMap[user.id].assignedCount)-parseInt(statisticsMap[user.id].curatedCount))}}</td>
    {{ } }}
    </tr>
    {{ }); }}
    </tbody>
    </table>
</script>
<!--template for curator statistics ends here-->

<!--Modal for Library Expert Statistics starts here-->
<div id="show-libExpert-statistics-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <tags:message code="label.statistics"/> of <span id="libExpertResourceNameSpan" style="text-transform: capitalize; color: #1200ff;"></span></h4>
            </div>
            <div class="" id="show-libExpert-statistics-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!--Modal for Library Expert Statistics ends here-->

<!--template for Library Expert statistics starts here-->
<script type="text/template" id="tpl-libExpert-statistics">
    <table class="table table-bordered table-striped">
    <thead>
    <tr>
    <th> SR. No.</th>
    <th> <tags:message code="label.username"/></th>
    <th> <tags:message code="label.assigned.record"/></th>
    <th> <tags:message code="label.approved.records"/></th>
    <th> <tags:message code="label.unapproved.records"/></th>
    </tr>
    </thead>
    <tbody>
    {{ if ( !_.isEmpty(userList) && _.isEmpty(statisticsArr)) { }}
    {{ _.each(userList, function (user,i) { }}
    <tr>
    <td> {{=i+1}}</td>
    <td style="text-transform: capitalize;"> {{=user.name}}</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    </tr>
    {{ }); }}
    {{ } else if (!_.isEmpty(userList) && !_.isEmpty(statisticsArr)) {  }}
    {{ _.each(userList, function (user,i) { }}
    {{ _.each(statisticsArr, function (stat) { }}
    {{ if ( user.id === stat.id) { }}
    <tr>
    <td> {{=i+1}}</td>
    <td  style="text-transform: capitalize;"> {{=user.name}}</td>
    <td> {{=stat.assignedRecords}}</td>
    <td> {{=stat.approvedRecords}}</td>
    <td> {{=stat.unApprovedRecords}}</td>
    </tr>
    {{ } }}
    {{ }); }}
    {{ }); }}
    {{ } else { }}
    <tr><td colspan="5" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>
    {{ } }}


    </tbody>
    </table>
</script>
<!--template for Library Expert statistics ends here-->

<!--Record Distribution Register Action template starts here-->
<script type="text/template" id="tpl-rd-dist-rg-action">
    <div class="btn-group"><a id="{{=resourceId}}-link" class="btn btn-sm btn-info start-distribution-btn disabled"><i class="fa fa-users"></i></a></div>
    <div class="btn-group"><a id="{{=resourceId}}-show-resource-statistics" class="btn btn-sm btn-info show-resource-statistics" data-resource-name="{{=resourceName}}" data-resource-code="{{=resourceCode}}" data-resource-type-code="{{=resourceTypeCode}}"><i class="fa fa-eye"></i></a></div>
    <div class="btn-group"><a id="{{=resourceId}}-show-libExpert-statistics" class="btn btn-sm btn-info show-libExpert-statistics" data-resource-name="{{=resourceName}}" data-resource-code="{{=resourceCode}}" data-resource-type-code="{{=resourceTypeCode}}"><i class="fa fa-eye"></i></a></div>
</script>
<!--Record Distribution Register Action template ends here-->

<!--template for record distribution register start here-->
<script type="text/template" id="tpl-rd-tbl-row">
    <tr>
    <td>{{=rowCount}}</td>
    <td>{{=resourceName}}</td>
    <td id="{{=resourceId}}-totalCount" class="text-right">
    <img title="Fetch count..." style="-webkit-user-select: none" src="${context}/themes/images/processing.gif">
    </td>
    <td id="{{=resourceId}}-undistributedCount" class="text-right">
    <img title="Fetch count..." style="-webkit-user-select: none" src="${context}/themes/images/processing.gif">
    </td>
    <td id="{{=resourceId}}-distributedCount" class="text-right">
    <img title="Fetch count..." style="-webkit-user-select: none" src="${context}/themes/images/processing.gif">
    </td>
    <td id="{{=resourceId}}-userCount" class="text-right">
    <img title="Fetch count..." style="-webkit-user-select: none" src="${context}/themes/images/processing.gif">
    </td>
    <td id="{{=resourceId}}-libExpertCount" class="text-right">
    <img title="Fetch count..." style="-webkit-user-select: none" src="${context}/themes/images/processing.gif">
    </td>
    <td id="{{=resourceId}}-action" class="text-right" style="text-align:center">
    <div class="btn-group" title="Start Distribution">
    <a id="{{=resourceId}}-link" class="btn btn-sm btn-info start-distribution-btn disabled" data-resource-id="{{=resourceId}}" data-resource-name="{{=resourceName}}" data-resource-code="{{=resourceCode}}" data-resource-type-code="{{=resourceTypeCode}}"><img src="${context}/images/icons/distribute-rec.png" alt=""/></i></a>
    </div>
    <div class="btn-group" title="Curator Statistics">
    <a id="{{=resourceId}}-show-resource-statistics" class="btn btn-sm btn-info show-resource-statistics disabled" data-resource-name="{{=resourceName}}" data-resource-code="{{=resourceCode}}" data-resource-type-code="{{=resourceTypeCode}}"><i class="fa fa-eye"></i></a>
    </div>
    <div class="btn-group" title="Library Expert Statistics">
    <a id="{{=resourceId}}-show-libExpert-statistics" class="btn btn-sm btn-info show-libExpert-statistics disabled" data-resource-name="{{=resourceName}}" data-resource-code="{{=resourceCode}}" data-resource-type-code="{{=resourceTypeCode}}"><i class="fa fa-eye"></i></a>
    </div>
    </td>
    </tr>
</script>
<!--template for record distribution register ends here-->

<script type="text/javascript">
    var _dataLimit = 10;

    $(document).ready(function () {
        populateResourceTable(1, true);
        $("#searchString").unbind().bind("keyup", function () {
            populateResourceTable(1, false);
        });

        $("#resourceTypeFilter").unbind().bind("change", function () {
            populateResourceTable(1, false);
        });
    });

    function populateResourceTable(pageNo, isFirstTimeCall) {
        $('#loadMore').remove();
        $.ajax({
            url: __NVLI.context + "/curation-activity/getResourcesByFiltersWithLimit",
            data: {
                searchString: $('#searchString').val(),
                resourceTypeFilter: $('#resourceTypeFilter').val(),
                dataLimit: _dataLimit,
                pageNo: pageNo
            },
            dataType: 'json',
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (pageNo === 1) {
                        $("#rd-tbl-body").empty();
                    }

                    var rowCount = ((pageNo - 1) * _dataLimit) + 1;
                    var rd_tbl_row_template = _.template($("#tpl-rd-tbl-row").html());
                    var opts;
                    var rd_tbl_row_el;

                    var isMoreDataPresent = false;
                    _.each(jsonObject.resourceTypeArray, function (resourceType) {

                        if (isFirstTimeCall) {
                            $("#resourceTypeFilter").append('<option value="' + resourceType.id + '">' + resourceType.resourceTypeName + '</option>');
                        }

                        if (!_.isEmpty(jsonObject.resourceMap[resourceType.id]) && $("#resource-type-" + resourceType.id).length === 0) {
                            $("#rd-tbl-body").append('<tr id="resource-type-' + resourceType.id + '"><td colspan="8" class="alert-warning" style="text-align:center"><b>' + resourceType.resourceTypeName + '</b></td></tr>');
                        }
                        _.each(jsonObject.resourceMap[resourceType.id], function (resource) {
                            isMoreDataPresent = true;
                            opts = {
                                "rowCount": rowCount,
                                "resourceId": resource.id,
                                "resourceName": resource.resourceName,
                                "resourceCode": resource.resourceCode,
                                "resourceTypeCode": resourceType.resourceTypeCode
                            };
                            rd_tbl_row_el = rd_tbl_row_template(opts);
                            $("#rd-tbl-body").append(rd_tbl_row_el);
                            rowCount++;
                            getRecordAndUserCountByResource(resource.id, resource.resourceCode, resourceType.resourceTypeCode);
                        });
                    });

                    if (isMoreDataPresent) {
                        pageNo++;
                        $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-info" style="text-align:center"><b><a href="javascript:;" class="btn-primary btn" onclick="populateResourceTable(' + pageNo + ',false);">Load More</a></b></td></tr>');
                    } else {
                        if (pageNo === 1) {
                            $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                        } else {
                            $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-success" style="text-align:center"><b>No more records</b></td></tr>');
                            setTimeout(function () {
                                $('#loadMore').remove();
                            }, 3000);
                        }
                    }

                    $(".start-distribution-btn").unbind().bind("click", function (e) {
                        var resourceId = e.currentTarget.getAttribute("data-resource-id");
                        var resourceCode = e.currentTarget.getAttribute("data-resource-code");
                        var resourceTypeCode = e.currentTarget.getAttribute("data-resource-type-code");
                        var resourceName = e.currentTarget.getAttribute("data-resource-name");
                        document.getElementById(resourceId + "-action").innerHTML = '<img title="In process..." style="-webkit-user-select: none" src="${context}/themes/images/processing.gif">';
                        $.ajax({
                            url: '${context}/curation-activity/startDistribution',
                            data: {
                                resourceCode: resourceCode,
                                resourceTypeCode: resourceTypeCode
                            },
                            type: 'POST',
                            success: function (response) {
                                var action_template = _.template($("#tpl-rd-dist-rg-action").html());
                                var opts = {
                                    resourceId: resourceId,
                                    resourceName: resourceName,
                                    resourceCode: resourceCode,
                                    resourceTypeCode: resourceTypeCode
                                };
                                var action_el = action_template(opts);
                                $('#' + resourceId + "-action").html(action_el);
                                bindShowCuratorStatisticsBtn();
                                bindShowLibExpertStatisticsBtn();

                                getRecordAndUserCountByResource(resourceId, resourceCode, resourceTypeCode);
                                if (response) {
                                    $("#alert-box").html('<div class="alert alert-success">Record <b>successfully</b> distributed for resource ' + resourceName + '.</div>').fadeIn();
                                    $("#alert-box").fadeOut(5000);
                                } else {
                                    $("#alert-box").html('<div class="alert alert-danger">Record distribution <b>unsuccessful</b> for resource ' + resourceName + '.<br>Something went wrong,try after some time.</div>').fadeIn();
                                    $("#alert-box").fadeOut(5000);
                                }
                            },
                            error: function () {
                                $("#alert-box").html('<div class="alert alert-danger">Record distribution <b>unsuccessful</b> for resource ' + resourceName + '.<br>Something went wrong,try after some time.</div>').fadeIn();
                                $("#alert-box").fadeOut(5000);
                            }
                        });
                    });
                    bindShowCuratorStatisticsBtn();
                    bindShowLibExpertStatisticsBtn();
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function bindShowCuratorStatisticsBtn() {
        $(".show-resource-statistics").unbind().bind("click", function (e) {
            var resourceCode = e.currentTarget.getAttribute("data-resource-code");
            var resourceTypeCode = e.currentTarget.getAttribute("data-resource-type-code");
            var resourceName = e.currentTarget.getAttribute("data-resource-name");
            $.ajax({
                url: '${context}/curation-activity/showResourceStatistics',
                data: {
                    resourceCode: resourceCode,
                    resourceTypeCode: resourceTypeCode
                },
                type: 'POST',
                success: function (response) {
                    if (response !== null) {
                        var resourceStatisticsTpl = _.template($("#tpl-resource-statistics").html());
                        var Opts = {
                            "userList": response.associatedUsers,
                            "statisticsMap": response.statistics
                        };

                        var resourceStatisticsModalBody = resourceStatisticsTpl(Opts);
                        $("#show-resource-statistics-body").empty();
                        $("#show-resource-statistics-body").append(resourceStatisticsModalBody);
                        $("#resourceNameSpan").html(resourceName);
                        $("#show-resource-statistics-modal").modal("show");

                    } else {
                        alertErrorMsg();
                    }
                },
                error: function () {
                    alertErrorMsg();
                }
            });
        });
    }

    function bindShowLibExpertStatisticsBtn() {
        $(".show-libExpert-statistics").unbind().bind("click", function (e) {
            var resourceCode = e.currentTarget.getAttribute("data-resource-code");
            var resourceTypeCode = e.currentTarget.getAttribute("data-resource-type-code");
            var resourceName = e.currentTarget.getAttribute("data-resource-name");
            $.ajax({
                url: '${context}/curation-activity/showLibExpertStatistics',
                data: {
                    resourceCode: resourceCode,
                    resourceTypeCode: resourceTypeCode
                },
                type: 'POST',
                success: function (response) {
                    if (response !== null) {
                        var resourceStatisticsTpl = _.template($("#tpl-libExpert-statistics").html());
                        var Opts = {
                            "userList": response.associatedUsers,
                            "statisticsArr": response.statistics
                        };
                        var resourceStatisticsModalBody = resourceStatisticsTpl(Opts);
                        $("#show-libExpert-statistics-body").empty();
                        $("#show-libExpert-statistics-body").append(resourceStatisticsModalBody);
                        $("#libExpertResourceNameSpan").html(resourceName);
                        $("#show-libExpert-statistics-modal").modal("show");
                    } else {
                        alertErrorMsg();
                    }
                },
                error: function () {
                    alertErrorMsg();
                }
            });
        });
    }

    function getRecordAndUserCountByResource(resourceId, resourceCode, resourceTypeCode)
    {
        $.ajax({
            url: '${context}/curation-activity/getRecordAndUserCountByResource',
            data: {resourceCode: resourceCode, resourceTypeCode: resourceTypeCode},
            type: 'POST',
            success: function (response) {
                if (response !== null) {
                    var countJSON = jQuery.parseJSON(response.recordCount);
                    document.getElementById(resourceId + "-totalCount").innerHTML = countJSON.totalcount;
                    document.getElementById(resourceId + "-undistributedCount").innerHTML = countJSON.undistributedcount;
                    document.getElementById(resourceId + "-distributedCount").innerHTML = parseInt(countJSON.totalcount) - parseInt(countJSON.undistributedcount);
                    document.getElementById(resourceId + "-userCount").innerHTML = response.userCount;
                    document.getElementById(resourceId + "-libExpertCount").innerHTML = response.libExpertCount;
                    if (parseInt(countJSON.undistributedcount) > 0 && parseInt(response.userCount) > 0)
                    {
                        $("#" + resourceId + "-link").removeClass("disabled");
                    }
                    if (parseInt(response.userCount) > 0)
                    {
                        $("#" + resourceId + "-show-resource-statistics").removeClass("disabled");
                    }
                    if (parseInt(response.libExpertCount) > 0)
                    {
                        $("#" + resourceId + "-show-libExpert-statistics").removeClass("disabled");
                    }

                } else {
                    document.getElementById(resourceId + "-totalCount").innerHTML = "NA";
                    document.getElementById(resourceId + "-undistributedCount").innerHTML = "NA";
                    document.getElementById(resourceId + "-distributedCount").innerHTML = "NA";
                    document.getElementById(resourceId + "-userCount").innerHTML = "NA";
                    document.getElementById(resourceId + "-libExpertCount").innerHTML = "NA";
                }
            },
            error: function () {
                document.getElementById(resourceId + "-totalCount").innerHTML = "NA";
                document.getElementById(resourceId + "-undistributedCount").innerHTML = "NA";
                document.getElementById(resourceId + "-distributedCount").innerHTML = "NA";
                document.getElementById(resourceId + "-userCount").innerHTML = "NA";
                document.getElementById(resourceId + "-userCount").innerHTML = "NA";
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>