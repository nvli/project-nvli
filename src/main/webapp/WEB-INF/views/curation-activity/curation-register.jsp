<%--
    Document   : curation-register
    Created on : Oct 18, 2016, 12:31:45 PM
    Author     : Doppa Srinivas
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!doctype html>
<form:form name="curationForm" id="curationForm" action="${context}/curation-activity/curationRegister" modelAttribute="curationForm" method="get">
    <div class="container-fluid">
        <div class="m-t-3 ">
            <!-- search result content starts here -->
            <div class="m-l-3 m-r-3 m-t-2" id="main">
                <div class="clear"></div>
                <!-- starts -->
                <div class="">
                    <div class="row clearfix">
                        <!-- sidebar
                        -->
                        <div class="col-lg-2 col-md-4 col-sm-12" >
                            <div class="clearfix" id="accordion1"  role="tablist" aria-multiselectable="true" >
                                <h5 class="card-header"> <tags:message code="label.Filters"/> </h5>
                                <div class="m-t-2  card">
                                    <div class="card-header-s"><tags:message code="label.resource-type"/> </div>
                                    <div class="overflow330">
                                        <c:forEach items="${resourceTypeCodeList}" var="resourceTypeCode" varStatus="count">
                                            <div id="accordion${count.index}" role="tablist" aria-multiselectable="true">
                                                <div class="panel">
                                                    <div class="panel-heading withcount" role="tab" id="resourceTypeHeading${count.index}">
                                                        <c:if test="${curationForm.resourceCode eq resourceTypeCode}">
                                                            <a data-toggle="collapse" data-parent="#accordion${count.index}" href="#${resourceTypeCode}_collapse${count.index}" aria-expanded="true" aria-controls="${resourceTypeCode}_collapse${count.index}">
                                                                ${resourceTypes[resourceTypeCode].resourceType}
                                                            </a>
                                                        </c:if>
                                                        <c:if test="${curationForm.resourceCode ne resourceTypeCode}">
                                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion${count.index}" href="#${resourceTypeCode}_collapse${count.index}" aria-expanded="false" aria-controls="${resourceTypeCode}_collapse${count.index}">
                                                                ${resourceTypes[resourceTypeCode].resourceType}
                                                            </a>
                                                        </c:if>
                                                    </div>
                                                    <c:if test="${curationForm.resourceCode ne resourceTypeCode}">
                                                        <div id="${resourceTypeCode}_collapse${count.index}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="resourceTypeHeading${count.index}">
                                                        </c:if>
                                                        <c:if test="${curationForm.resourceCode eq resourceTypeCode}">
                                                            <div id="${resourceTypeCode}_collapse${count.index}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="resourceTypeHeading${count.index}">
                                                                <c:if test="${resources eq null or resources.isEmpty()}">
                                                                    <ul class="list-group sideb clearfix">
                                                                        <li class="list-group-item nobr" title="${resource.resourceName}">
                                                                            <span class="truncate">
                                                                                <tags:message code="label.no.resources.avaliable"/>
                                                                            </span>
                                                                        </li>
                                                                    </ul>
                                                                </c:if>
                                                                <c:if test="${!(resources eq null or resources.isEmpty())}">
                                                                    <c:forEach items="${resources}" var="resource">
                                                                        <c:if test="${curationForm.allSubResourceIdentifiers ne null and curationForm.allSubResourceIdentifiers.contains(resource.resourceCode)}">
                                                                            <ul class="list-group sideb clearfix">
                                                                                <li class="list-group-item nobr" title="${resource.resourceName}">
                                                                                    <span class="truncate">
                                                                                        <label class="checkbox-inline" style="margin-top:-20px">
                                                                                            <form:checkbox path="subResourceIdentifiers"  value="${resource.resourceCode}" onchange="formSubmit();"/>
                                                                                        </label>
                                                                                        ${resource.resourceName}
                                                                                    </span>
                                                                                </li>
                                                                            </ul>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                    <c:forEach items="${resources}" var="resource">
                                                                        <c:if test="${curationForm.allSubResourceIdentifiers ne null and !curationForm.allSubResourceIdentifiers.contains(resource.resourceCode)}">
                                                                            <ul class="list-group sideb clearfix">
                                                                                <li class="list-group-item nobr" title="${resource.resourceName}">
                                                                                    <span class="truncate">
                                                                                        <label class="checkbox-inline" style="margin-top:-20px">
                                                                                            <input type="checkbox" disabled/>
                                                                                        </label>
                                                                                        ${resource.resourceName}
                                                                                    </span>
                                                                                </li>
                                                                            </ul>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:if>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                            <c:forEach items="${resourceTypes}" var="resourceType" varStatus="count">
                                                <c:if test="${!resourceTypeCodeList.contains(resourceType.value.resourceTypeCode)}">
                                                    <div id="otherResourceTypeAccordion${count.index}" role="tablist" aria-multiselectable="true">
                                                        <div class="panel">
                                                            <div class="panel-heading"  role="tab" id="otherResourceTypeHeading${count.index}">
                                                                <c:if test="${curationForm.resourceCode eq resourceType.value.resourceTypeCode}">
                                                                    <a data-toggle="collapse" data-parent="#otherResourceTypeAccordion${count.index}" href="#${resourceType.value.resourceTypeCode}_collapse${count.index}" aria-expanded="true" aria-controls="${resourceType.value.resourceTypeCode}_collapse${count.index}">
                                                                        ${resourceType.value.resourceType}
                                                                    </a>
                                                                </c:if>
                                                                <c:if test="${curationForm.resourceCode ne resourceType.value.resourceTypeCode}">
                                                                    <a class="collapsed" data-toggle="collapse" data-parent="#otherResourceTypeAccordion${count.index}" href="#${resourceType.value.resourceTypeCode}_collapse${count.index}" aria-expanded="false" aria-controls="${resourceType.value.resourceTypeCode}_collapse${count.index}">
                                                                        ${resourceType.value.resourceType}
                                                                    </a>
                                                                </c:if>
                                                            </div>
                                                            <c:if test="${curationForm.resourceCode ne resourceType.value.resourceTypeCode}">
                                                                <div id="${resourceType.value.resourceTypeCode}_collapse${count.index}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="otherResourceTypeHeading${count.index}">
                                                                </c:if>
                                                                <c:if test="${curationForm.resourceCode eq resourceType.value.resourceTypeCode}">
                                                                    <div id="${resourceType.value.resourceTypeCode}_collapse${count.index}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="otherResourceTypeHeading${count.index}">
                                                                        <c:if test="${resources eq null or resources.isEmpty()}">
                                                                            <ul class="list-group sideb clearfix">
                                                                                <li class="list-group-item nobr" title="${resource.resourceName}">
                                                                                    <span class="truncate">
                                                                                        <tags:message code="label.no.resources.avaliable"/>
                                                                                    </span>
                                                                                </li>
                                                                            </ul>
                                                                        </c:if>
                                                                        <c:if test="${!(resources eq null or resources.isEmpty())}">
                                                                            <c:forEach items="${resources}" var="resource">
                                                                                <ul class="list-group sideb clearfix">
                                                                                    <li class="list-group-item nobr" title="${resource.resourceName}">
                                                                                        <span class="truncate">
                                                                                            <label class="checkbox-inline" style="margin-top:-20px">
                                                                                                <input type="checkbox" disabled/>
                                                                                            </label>
                                                                                            ${resource.resourceName}
                                                                                        </span>
                                                                                    </li>
                                                                                </ul>
                                                                            </c:forEach>
                                                                        </c:if>
                                                                    </c:if>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="m-t-1  card">
                                            <div class="card-header-s"><tags:message code="label.choose.language"/> </div>
                                            <div class="overflow330 ">
                                                <div class="m-ga-1">
                                                    <div class="panel">
                                                        <div class="in_b2ox">
                                                            <ul class="list-group sideb clearfix">
                                                                <c:forEach items="${languageList}" var="language">
                                                                    <li class="list-group-item nobr">
                                                                        <span class="truncate">
                                                                            <label class="checkbox-inline" style="margin-top:-20px">
                                                                                <form:checkbox path="languages" value="${language.languageCode}" onchange="formSubmit();"/>
                                                                            </label>
                                                                            ${language.languageName}
                                                                        </span>
                                                                    </li>
                                                                </c:forEach>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / acc1-->
                                    </div>
                                </div>
                                <input name="language" id="language" value="en" type="hidden">
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                                    <div class="card">
                                        <div class="card-header cardHeaderInputMargin">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-7">
                                                    <strong  class="header-title">
                                                        <tags:message code="sb.record.register"/>
                                                    </strong>
                                                </div>                                                
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-2" onchange="formSubmit();">
                                                    <form:input path="searchTerm" id="searchTerm" placeholder="Search in title" class="form-control form-control-md"/>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 col-xl-2">                                                    
                                                    <form:select path="status" onchange="formSubmit();" class="form-control form-control-md">
                                                        <form:option value="UNCURATED"><a href="#" class="label label-danger"> <tags:message code="label.Uncurrated"/> </a></form:option>
                                                        <form:option value="CURATED"><span class="label label-success"> <tags:message code="label.Currated"/> </span></form:option>
                                                    </form:select>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-1 col-xl-1" onchange="formSubmit();">
                                                    <form:select class="form-control form-control-md" path="pageWin" id="limit">
                                                        <form:option   value="20">20</form:option>
                                                        <form:option  value="30">30</form:option>
                                                        <form:option   value="40">40</form:option>
                                                        <form:option   value="50">50</form:option>
                                                    </form:select>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <table  class="table table-bordered tr_hand" cellpadding="0" cellspacing="0">
                                                <thead>
                                                <th width="5%"><tags:message code="label.sno"/></th>
                                                <th width="30%"><tags:message code="label.Record.Identifier"/></th>
                                                <th width="50%"> <tags:message code="label.Record.Title"/></th>
                                                <th width="15%"><tags:message code="user.status"/></th>
                                                </thead>
                                                <tbody>
                                                    <c:if test="${not empty curationForm.listOfResult}">
                                                        <c:forEach items="${curationForm.listOfResult}" var="result" varStatus="count">
                                                            <tr class="col_head">
                                                                <td> ${((curationForm.pageWin * (curationForm.pageNo-1)) + count.index)+1} </td>
                                                                <td>${result.recordIdentifier}</td>
                                                                <td> ${result.nameToView} </td>
                                                                <td class="text-right">
                                                                    <c:if test="${curationForm.status eq 'UNCURATED'}">
                                                                        <a class="btn btn-sm btn-info start-distribution-btn" href="${context}/cs/${result.recordIdentifier}"><tags:message code="label.Curate"/></a>
                                                                    </c:if>
                                                                    <c:if test="${curationForm.status eq 'CURATED'}">
                                                                        <a class="btn btn-sm btn-info start-distribution-btn" href="${context}/cs/${result.recordIdentifier}"><tags:message code="label.Curated"/></a>
                                                                    </c:if>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </c:if>
                                                    <c:if test="${ empty curationForm.listOfResult}">
                                                        <tr class="col_head">
                                                            <td colspan="4" class="alert-warning" style="text-align:center;">
                                                                <b><tags:message code="label.no.records.found"/></b>
                                                            </td>
                                                        </tr>
                                                    </c:if>
                                                </tbody>
                                            </table>
                                            <!-- PAGINATION -->
                                            <div class="clear22"></div>
                                            <div class="card-footer clearfix">
                                                <c:if test="${not empty curationForm.listOfResult}">
                                                    <span class="pull-left">
                                                        <c:if test="${not empty curationForm.listOfResult}">
                                                            <tags:message code="label.total.records.found"/> (${curationForm.resultSize})
                                                        </c:if>
                                                    </span>
                                                    <span class="pull-right">
                                                        <ul class="pagination pagination-sm">
                                                            <%--For displaying Page numbers. --%>
                                                            <c:choose>
                                                                <c:when test="${totalPages eq 0}">
                                                                    <li class="page-item active"> <a class="page-link" > 1 <span class="sr-only">(current)</span></a> </li>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <c:choose>
                                                                            <c:when test="${totalPages lt 10}">
                                                                                <c:if test="${curationForm.pageNo ne 1}">
                                                                                <li class="page-item"> <a class="page-link pl"  data-page-num="1"  href="#">[1] </a> </li>
                                                                                <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${curationForm.pageNo-1}"  href="#"> <span aria-hidden="true">&laquo;</span> <span class="sr-only"><tags:message code="label.previous"/></span> </a> </li>
                                                                                </c:if>
                                                                                <c:forEach begin="1" end="${totalPages}" var="i">
                                                                                    <c:set var="lastCount" value="${i}"/>
                                                                                    <c:choose>
                                                                                        <c:when test="${curationForm.pageNo eq i}">
                                                                                        <li class="active page-item"> <a class="page-link" >${i} <span class="sr-only">(current)</span></a> </li>
                                                                                        </c:when >
                                                                                        <c:otherwise>
                                                                                        <li class="page-item">
                                                                                            <a class="page-link pl" data-page-num="${i}"  href="#">${i}</a>
                                                                                        </li>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:forEach>
                                                                            <c:if test="${lastCount ne curationForm.pageNo}">
                                                                                <li class="page-item"> <a class="page-link pl" href="#" aria-label="Next" data-page-num="${curationForm.pageNo+1}" > <span aria-hidden="true">�</span> <span class="sr-only"><tags:message code="label.next"/></span> </a></li>
                                                                                <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${totalPages}"  href="#">[${totalPages}] </a> </li>
                                                                                </c:if>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <c:if test="${curationForm.pageNo ne 1}">
                                                                                <li class="page-item"> <a class="page-link pl"  data-page-num="1"  href="#">[1] </a> </li>
                                                                                <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${curationForm.pageNo-1}"  href="#"> <span aria-hidden="true">&laquo;</span> <span class="sr-only"><tags:message code="label.previous"/></span> </a> </li>
                                                                                </c:if>
                                                                                <c:set var="begin" value="${curationForm.pageNo-4}"/>
                                                                                <c:set var="end" value="${curationForm.pageNo+4}"/>
                                                                                <c:if test="${(curationForm.pageNo-4) lt 1}">
                                                                                    <c:set var="begin" value="1"/>
                                                                                </c:if>
                                                                                <c:if test="${(totalPages-4) lt curationForm.pageNo}">
                                                                                    <c:set var="end" value="${totalPages}"/>
                                                                                </c:if>
                                                                                <c:forEach begin="${begin}" end="${end}" var="i">
                                                                                    <c:set var="lastCount" value="${i}"/>
                                                                                    <c:choose>
                                                                                        <c:when test="${curationForm.pageNo eq i}">
                                                                                        <li class="active page-item"> <a class="page-link" >${i} <span class="sr-only">(current)</span></a> </li>
                                                                                        </c:when >
                                                                                        <c:otherwise>
                                                                                        <li class="page-item">
                                                                                            <a class="page-link pl" data-page-num="${i}"  href="#">${i}</a>
                                                                                        </li>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:forEach>
                                                                            <c:if test="${lastCount ne curationForm.pageNo}">
                                                                                <li class="page-item"> <a class="page-link pl" href="#" aria-label="Next" data-page-num="${curationForm.pageNo+1}" > <span aria-hidden="true">�</span> <span class="sr-only"><tags:message code="label.next"/></span> </a></li>
                                                                                <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${totalPages}"  href="#">[${totalPages}] </a> </li>
                                                                                </c:if>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                        </ul>
                                                    </span>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form:hidden path="pageNo" id="pageNo"/>
            <form:hidden path="resourceCode" id="resourceCode"/>
        </form:form>
        <!-- ends -->
        <div class="clear22"></div>
        <!-- s result content container -->
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div style="margin-bottom:0px;" class="clearfix"></div>
        <!-- ends -->
        <div class="clear22"></div>
        <!-- s result content container -->
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <script>
            var resourceCode = '${curationForm.resourceCode}';
            $(document).ready(function () {
                $(".pl").unbind().bind("click", function (e) {
                    document.getElementById('pageNo').value = e.currentTarget.getAttribute("data-page-num");
                    document.getElementById('resourceCode').value = resourceCode;
                    document.getElementById('curationForm').submit();
                });
                //when collapse event is executed
                $(function () {
                    $(".collapse").on('show.bs.collapse', function (e) {
                        if ($(this).is(e.target)) {
                            if (resourceCode != this.id.trim().split('_')[0]) {
                                $('input:checkbox[name=subResourceIdentifiers]').attr('checked', false);
                                resourceCode = this.id.trim().split('_')[0];
                                formSubmit();
                            }
                        }
                    });
                });
            });
            function formSubmit() {
                document.getElementById('pageNo').value = 1;
                document.getElementById('resourceCode').value = resourceCode;
                document.getElementById('curationForm').submit();
            }
        </script>