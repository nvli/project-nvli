<%--
    Document   : show-curators.jsp
    Created on : Oct 17, 2016, 5:59:29 PM
    Author     : Gulafsha
    Author     : Sanjay Rabidas

--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="col-md-12">
        <div class="card">
            <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
            <div id="alert-box1" class="alert alert-success" style="display: none;"></div>
            <div class="card-header"><tags:message code="label.Curators"/></div>
            <table class="table table-bordered" data-page-size="15">
                <thead>
                    <tr>
                        <th><tags:message code="label.serial.no"/></th>
                        <th></th>
                        <th><i class="fa fa-user"></i> <tags:message code="label.user.name"/></th>
                        <th><i class="fa fa-envelope-o"></i> <tags:message code="user.email"/></th>
                        <th><i class="fa fa-info-circle"></i> <tags:message code="user.status"/></th>
                        <th><i class="fa fa-gear"></i> <tags:message code="user.action"/></th>
                        <th><tags:message code="label.work.stats"/></th>
                        <th><tags:message code="label.Reports"/></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${allUsers}" var="user" varStatus="counter">
                        <tr>
                            <td><c:out value="${counter.count}"/></td>
                            <td>
                                <a href="${context}/user/profile/view?userId=${user.id}">
                                    <img src="${exposedProps.userImageBase}/${user.profilePicPath}"
                                         width="50"
                                         class="media-object img-thumbnail"
                                         alt="${user.firstName} ${user.lastName}"
                                         onerror="this.src='${context}/images/profile-picture.png'"/>
                                </a>
                            </td>
                            <td>
                                <a href="${context}/user/profile/view?userId=${user.id}">
                                    <strong><c:out value="${user.firstName} ${user.middleName} ${user.lastName}"/></strong>
                                </a>
                            </td>
                            <td><c:out value="${user.email}"/></td>
                            <td><c:choose>
                                    <c:when test="${user.deleted==1}">
                                        <span class="label label-primary"><tags:message code="user.action.deleted"/></span>
                                    </c:when>
                                    <c:otherwise>
                                        <c:if test="${user.enabled==1}"><span class="label label-success"><tags:message code="user.action.active"/></span></c:if><c:if test="${user.enabled==0}"><span class="label label-warning"><tags:message code="user.action.inactive"/></span></c:if>
                                        </c:otherwise>
                                    </c:choose>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a class="btn-secondary btn" href="${context}/user/profile/view?userId=${user.id}"  title="view"><i class="fa fa-eye"></i></a>
                                    <a class="btn-secondary btn" href="${context}/user/profile/update?userId=${user.id}" title="edit"><i class="fa fa-pencil"></i></a>

                                </div>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-warning" onclick="showCuratorWorkStats('${user.id}');"><tags:message code="label.View"/></a>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-info btn-show-report" data-user-id="${user.id}"><tags:message code="label.View"/></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="show-period" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.select.period"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <label class="col-xs-3 control-label">From </label>
                        <div class="input-group input-group-sm input-append date" id="datePicker">
                            <input readonly id="fromDate" class="form-control" type="text" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div><br>
                        <label class="col-xs-3 control-label">To </label>
                        <div class="input-group input-group-sm  input-append date" id="datePicker1">
                            <input readonly id="toDate" class="form-control" type="text" required>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                    <div id="show-period-error-msg"></div>
                </div><br>
            </div>
            <div class="modal-footer" align="center">
                <a class="btn btn-secondary" id="view-report-btn"  onclick="return generateCuratorReport();" target="_blank"><tags:message code="label.show.report"/></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="show-work-stats" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.curation.work.stats"/></h4>
            </div>
            <div class="modal-body overflow685">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><tags:message code="label.serial.no"/></th>
                                    <th><tags:message code="label.resName"/></th>
                                    <th><tags:message code="label.curated.count"/></th>
                                    <th><tags:message code="label.assigned.count"/></th>
                                </tr>
                            </thead>
                            <tbody id="tbl-body-show-work-stats">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link   href="${context}/themes/css/datepicker3.min.css" rel="stylesheet" type="text/css" />
<script src="${context}/themes/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
                    var _userId;
                    $(document).ready(function () {
                        var toEndDate = new Date();
                        toEndDate.setDate(toEndDate.getDate() - 1);

                        $('#datePicker').datepicker({
                            startView: 2,
                            format: 'yyyy-mm-dd',
                            endDate: toEndDate,
                            autoclose: true
                        });

                        $('#datePicker1').datepicker({
                            startView: 2,
                            format: 'yyyy-mm-dd',
                            endDate: new Date(),
                            autoclose: true
                        });
                        $(".btn-show-report").click(function (e) {
                            _userId = e.currentTarget.getAttribute("data-user-id");
                            $('#show-period').modal('show');
                            $(".modal").css("z-index", 5);
                            $(".modal-backdrop").css("z-index", 4);
                        });
                    });
                    function generateCuratorReport() {
                        if ($('#fromDate').val().length !== 0 && $('#toDate').val().length !== 0) {
                            var d1 = new Date($('#fromDate').val());
                            var d2 = new Date($('#toDate').val());
                            var months;
                            months = (d2.getFullYear() - d1.getFullYear()) * 12;
                            months -= d1.getMonth() + 1;
                            months += d2.getMonth();
                            //            months= months <= 0 ? 0 : months;
                            if (months < 0) {
                                $('#show-period-error-msg').html('To date must be greater than from date.');
                                return false;
                            } else if (months >= 0 && months <= 2) {
                                $('#show-period').modal('hide');
                                document.getElementById("view-report-btn").href = "${context}/curation-activity/curator-report/" + _userId + "?startDate=" + document.getElementById("fromDate").value + "&endDate=" + document.getElementById("toDate").value;
                                return true;
                            } else {
                                $('#show-period-error-msg').html('<div class="form-control-feedback">Period difference must be with in 2 months.</div>');
                                return false;
                            }
                        } else {
                            $('#show-period-error-msg').html('<div class="form-control-feedback">Please fill out the fields.</div>');
                            return false;
                        }
                    }
                    function showCuratorWorkStats(usreId) {

                        $('#show-work-stats').modal('show');
                        $(".modal").css("z-index", 5);
                        $(".modal-backdrop").css("z-index", 4);
                        $("#tbl-body-show-work-stats").empty();
                        $.ajax({
                            url: '${context}/curation-activity/workstats/' + usreId,
                            type: 'POST',
                            success: function (response) {
                                if (!$.isEmptyObject(response)) {
                                    var count = 0;
                                    var rd_tbl_row_template = _.template($("#tpl-user-resource-stats").html());
                                    var opts;
                                    var rd_tbl_row_el;
                                    _.each(response.resourceWiseCount, function (resourceWiseCount) {
                                        count++;
                                        opts = {
                                            "rowCount": count,
                                            "resourceName": resourceWiseCount.resourceName,
                                            "curatedCount": resourceWiseCount.processedCount,
                                            "assignedCount": resourceWiseCount.assignedCount
                                        };
                                        rd_tbl_row_el = rd_tbl_row_template(opts);
                                        $("#tbl-body-show-work-stats").append(rd_tbl_row_el);
                                    });
                                } else {
                                    $("#tbl-body-show-work-stats").append("<td colspan='4'>No data found.</td>");
                                }
                            },
                            error: function () {
                                $("#tbl-body-show-work-stats").append("<td colspan='4'>No data found.</td>");
                            }
                        });
                    }
</script>
<script type="text/template" id="tpl-user-resource-stats">
    <tr>
    <td>{{=rowCount}}</td>
    <td>{{=resourceName}}</td>
    <td>{{=curatedCount}}</td>
    <td>{{=assignedCount}}</td>
    </tr>
</script>
