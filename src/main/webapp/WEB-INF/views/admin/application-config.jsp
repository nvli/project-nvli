<%-- 
    Document   : application-config
    Created on : Oct 5, 2017, 11:22:47 AM
    Author     : Gulafsha Khan <gulafsha@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-md-12">
            <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
            <div id="alert-box1" class="alert alert-success" style="display: none;"></div>
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-gear"> Application Settings</i> 
                </div>
                <div class="card-block">
                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                        <thead>
                            <tr>
                                <th>Feature Id</th>
                                <th>Feature Name</th>
                                <th>Enable/Disable</th>
                            </tr>
                        </thead>
                        <tbody id="feature-list-container"></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--        <div class="col-md-4">
                    <form>
                        <div class="card">
                            <div class="card-header"><i> Create New feature for configuration</i>
                            </div>
                            <div class="card-block">
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-3 col-md-4 col-sm-12 text-right">Feature Name</label>
                                    <div class="col-lg-8 col-md-7 col-sm-12">
                                        <input type="text" class="form-control" name="feature-name" id="featureName"  placeholder="Enter Feature name">
                                        <p id="feature-status" style="color:red" ><strong></strong></p>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer clearfix">
                                <div class="col-xs-offset-3 col-lg-10 col-sm-12">
                                    <button id="btnCreateFeature" class="btn btn-sm btn-primary" type="button">Add</button>
                                    <input type="reset" class="btn btn-secondary" value="Reset">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>-->
    </div>
</div>
<script type="text/template" id="app-config_tpl">
    <tr>
    <td>{{=featureId}}</td>
    <td>{{=featureName}}</td>
    <td><div class="form-check">
    <label class="form-check-label">
    <input class="form-check-input" type="checkbox" id="checkbox_{{=featureId}}">
    </label>
    </div>
    </td>
    </tr>
</script>
<script>
    $(document).ready(function () {
        fetchFeatures();
        $("#btnCreateFeature").click(function (e) {
            var featureName = $("#featureName").val();
            var data = {};
            data["featureName"] = featureName;
            $.get(window.__NVLI.context + "/admin/check/feature", data, function (exist) {
                if (!exist)
                {
                    var strconfirm = confirm('Are you sure you want to add invitation?');
                    if (strconfirm) {
                        $.post(window.__NVLI.context + "/admin/create/feature", data, function (response) {
                            if (response.status === "success") {
                                showSuccessMessage('New Feature added Successfully');
                                fetchFeatures();
                            }
                            else if (response.status === 'error')
                            {
                                showMessage('Some error came while adding feature!');
                            }
                            else if (response.status === "failed")
                            {
                                showMessage('feature could not be added!!');
                            }
                        });
                    }
                }
                else
                {
                    $("#feature-status").html('<div class="alert alert-danger">feature already Exist ...</div>').fadeIn();
                    $("#feature-status").fadeOut(10000);
                }

            });
        });
    });
    function populateFeatures(features)
    {
        // Empty feature list
        $("#feature-list-container").empty();
        _.each(features, function (feature) {
            var feature_template = _.template($("#app-config_tpl").html());
            var opts = {
                "featureId": feature.id,
                "featureName": feature.featureName,
                "isEnabled": feature.featureEnabledStatus,
            };
            console.log(feature.featureEnabledStatus);
            var feature_el = feature_template(opts);
            $("#feature-list-container").append(feature_el);
            if (feature.featureEnabledStatus)
            {
                $('#checkbox_' + feature.id).attr("checked", true);
            }
            else
            {
                $('#checkbox_' + feature.id).attr("checked", false);
            }
            $('#checkbox_' + feature.id).click(function () {
                var data = {};
                if (this.checked)
                    data["isEnabled"] = true;
                else
                    data["isEnabled"] = false;
                data["featureId"] = feature.id;
                updateFeature(data);
            });
        });
    }
    function fetchFeatures() {
        $.ajax({
            url: __NVLI.context + "/admin/fetch/features",
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                populateFeatures(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
    function updateFeature(data)
    {
        $.post(window.__NVLI.context + "/admin/update/feature", data, function (response) {
            if (response.status === "success") {
                if (response.value === 1)
                {
                    $("#alert-box1").hide();
                    showSuccessMessage("Feature is Successfully Enabled");
                }
                else if (response.value === 0)
                {
                    showSuccessMessage("Feature is Successfully Disabled");
                }
            }
            else if (response.status === 'error')
            {
                console.log("not updated")
            }
            fetchFeatures();
        });
    }
    function showMessage(message) {
        $("#alert-box").html(message);
        $("#alert-box").show();
    }
    function showSuccessMessage(message) {
        $("#alert-box1").html(message);
        $("#alert-box1").show();
        $('#alert-box1').fadeIn(500).delay(2000).fadeOut(800);
    }

</script>

