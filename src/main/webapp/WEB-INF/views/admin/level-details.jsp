<%-- 
    Document   : level-details
    Created on : 26 Feb, 2018, 2:29:42 PM
    Author     : Gulafsha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title policy-tittle"></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-success" id="create-level-button"> Create Level</button>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%">S.NO</th>
                    <th width="20%">Level name</th>
                    <th width="10%">Approved Points</th>
                    <th width="10%">Edit Points</th>
                    <th width ="25"> Badge</th>
                    <th width ="30"> Stars</th>
                </tr>
            </thead>
            <tbody id="levels-tbl-body"></tbody>
        </table>
        <div id="levels-tbl-footer" class="card-footer clearfix"></div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="levelName"></h4>
                    </div>
                    <div class="modal-body edit-body" style="font-weight: normal" id="levels">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="tpl-level-tbl-row">
    {{ _.each(levelsList, function (level,i) { }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>{{=level.levelName}}</td>
    <td>{{=level.approvedPoints}}</td>
    <td>{{=level.editPoints}}</td>
    <td>{{=level.badgeAwarded.badgeCode}}</td>
    <td>{{=level.starsAwarded.starsName}}</td>
    </tr>
    {{ }); }}

</script>
<script type="text/template" id="tpl-level-edit">
    <form id="form-edit-level-details">
    <div class = "form-group">
    <label class = "control-label" > Edit Points </label>
    <input type = "text" class = "form-control form-control-sm" id = "editPoints" name="editPoints" placeholder = "" required = "required" value="{{=editPoints}}" />
    <label class = "control-label" > Approved Points </label>
    <input type = "text" class = "form-control form-control-sm" id = "approvedPoints" name="approvedPoints" placeholder = "" required = "required" value="{{=approvedPoints}}" />
    <label class = "control-label" >Badge Awarded </label>
    <select class="form-control" name="badgeAwarded" id="badgeAwarded">
    {{ _.each(badges, function (badge,i) { }}
    <option value="{{=badge.badgeCode}}" id="{{=badge.badgeId}}" {{ if (badge.badgeCode === badgeAwarded){ }} selected {{ } }}>{{=badge.badgeCode}}</option>
    {{ }); }}
    </select>
    <label class = "control-label" >Stars Awarded </label>
    <select class="form-control" name="starsAwarded" id="starsAwarded">
    {{ _.each(stars, function (star,i) { }}
    <option value="{{=star.starsCode}}" id="{{=star.starsId}}" {{ if (star.starsCode === starsAwarded){ }} selected {{ } }}>{{=star.starsCode}}</option>
    {{ }); }}
    </select>
    </br>
    <button type="submit" class="btn btn-primary" id="edit-level-details">Submit</button>
    </div>
    </form>
</script>
<script>
    function editLevel(levelId) {
        var data = {};
        data['levelId'] = levelId;
        $.ajax({
            url: __NVLI.context + "/rewards/edit-level",
            type: 'POST',
            data: data,
            success: function (jsonObject) {
                if (jsonObject !== null)
                {
                    var levelName = jsonObject.level.levelName;
                    $("#levelName").text(jsonObject.level.levelName);
                    var levelsTpl = _.template($("#tpl-level-edit").html());
                    var Opts = {
                        "editPoints": jsonObject.level.editPoints,
                        "approvedPoints": jsonObject.level.approvedPoints,
                        "badgeAwarded": jsonObject.level.badgeAwarded.badgeCode,
                        "badges": jsonObject.badges,
                        "starsAwarded": jsonObject.level.starsAwarded.starsCode,
                        "stars": jsonObject.starsDetails
                    };
                    var body = levelsTpl(Opts);
                    $(".edit-body").empty();
                    $(".edit-body").html(body);
                    $("#form-edit-level-details").submit(function (e) {
                        e.preventDefault();
                        var x = $("#form-edit-level-details").serializeArray();
                        var jsonObj = {};
                        jsonObj['levelId'] = levelId;
                        jsonObj['activity'] = "Edit";
                        jsonObj['levelName'] = levelName;
                        $.each(x, function (i, field) {
                            console.log(field.name + ":" + field.value + " ");
                            jsonObj[field.name] = field.value;
                        });
                        $.post(window.__NVLI.context + "/rewards/update/level-details", jsonObj, function (response)
                        {
                            if (response.status === "update-success") {
                                console.log("success");
                                $('.edit-body').empty();
                                $("#myModal").modal("hide");

                                fetchAndRenderTableData();
                            } else if (response.status === "error") {
                                alertErrorMsg();
                            }

                        });
                        // on success reload data to render
                        return false;
                    });
                } else
                {
                    alertErrorMsg();
                }
            }
        });
    }
    function fetchAndRenderTableData() {
        $.ajax({
            url: __NVLI.context + "/rewards/getLevels",
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.LevelsList)) {
                        $("#levels-tbl-body").html('<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var levelsTpl = _.template($("#tpl-level-tbl-row").html());
                        var Opts = {
                            "levelsList": jsonObject.LevelsList,
                            "recordCount": 0
                        };
                        var body = levelsTpl(Opts);
                        $("#levels-tbl-body").empty();
                        $("#levels-tbl-body").html(body);
                        $(".btn-edit-level").unbind().bind("click", function (e) {
                            e.preventDefault();
                            $("#myModal").modal("show");
                            editLevel(e.currentTarget.getAttribute("data-level-id"));
                        });
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }
    function createLevel()
    {
        console.log("under create");
        $.post(window.__NVLI.context + "/rewards/find/last-level", function (jsonObj) {
            if (jsonObj.LastLevel !== null) {
                var levelName = jsonObj.newLevel;
                console.log(jsonObj.newLevel);
                $("#myModal").modal("show");
                $("#levelName").text(jsonObj.newLevel);
                var levelsTpl = _.template($("#tpl-level-edit").html());
                var Opts = {
                    "editPoints": "",
                    "approvedPoints": "",
                    "badgeAwarded": jsonObj.LastLevel.badgeAwarded.badgeCode,
                    "badges": jsonObj.badges,
                    "starsAwarded": jsonObj.LastLevel.starsAwarded.starsCode,
                    "stars": jsonObj.starsDetails
                };
                var body = levelsTpl(Opts);
                $(".edit-body").empty();
                $(".edit-body").html(body);
                $("#form-edit-level-details").submit(function (e) {
                    e.preventDefault();
                    var x = $("#form-edit-level-details").serializeArray();
                    var jsonObj = {};
                    console.log("hhhh", levelName);
                    jsonObj['levelName'] = levelName;
                    jsonObj['levelId'] = '';
                    jsonObj['activity'] = "Create";
                    $.each(x, function (i, field) {
                        console.log(field.name + ":" + field.value + " ");
                        jsonObj[field.name] = field.value;
                    });
                    $.post(window.__NVLI.context + "/rewards/update/level-details", jsonObj, function (response)
                    {
                        if (response.status === "create-success") {
                            console.log("success");
                            $('.edit-body').empty();
                            $("#myModal").modal("hide");
                            fetchAndRenderTableData();
                        } else if (response.status === "error") {
                            alertErrorMsg();
                        }

                    });
                    // on success reload data to render
                    return false;
                });
            } else
            {
            }
        }
        );
    }
    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
    $(document).ready(function () {
        $("#create-level-button").click(function (e) {
            createLevel();
        });
        fetchAndRenderTableData();
    });
</script>



