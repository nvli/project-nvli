<%-- 
    Document   : point-details
    Created on : 26 Feb, 2018, 11:58:31 AM
    Author     : Gulafsha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title points-tittle"><tags:message code="label.reward.points"/></strong>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%">S.NO</th>
                    <th width="20%">Activity Name</th>
                    <th width="15%">Earned Points</th>
                    <th width="15%">Lower Bound Accuracy</th>
                    <th width="15%">Upper Bound Accuracy</th>
                    <!--                    <th width="15%">Last Modified Date</th>-->
                    <th style="text-align:center">Edit Points</th>
                </tr>
            </thead>
            <tbody id="points-tbl-body"></tbody>
        </table>
        <div id="points-tbl-footer" class="card-footer clearfix"></div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="activity"></h4>
                </div>
                <div class="modal-body edit-body" style="font-weight: normal" id="points-details">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="tpl-points-tbl-row">
    {{ _.each(pointsDetailList, function (point,i) { }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>{{=point.activityDesc}}</td>
    <td>{{=point.earnedPoints}}</td>
    <td>{{=point.lowerBoundAccuracy}}</td>
    <td>{{=point.upperBoundAccuracy}}</td>
    <td class="text-right" style="text-align:center">
    <div class="btn-group" title="Edit Points">
    <button class="btn-secondary btn btn-edit-points" data-point-id="{{=point.pointsId}}"><i class="fa fa-edit"></i></button>
    </div>
    </td>
    </tr>
    {{ }); }}
</script>
<script type="text/template" id="tpl-points-edit">
    <form id="form-edit-points-details">
    <div class = "form-group">
    <label class = "control-label" > Earned Points </label>
    <input type = "text" class = "form-control form-control-sm" id = "earnedPoints" name="earnedPoints" placeholder = "" required = "required" value="{{=earnedPoints}}" />
    </br>
    <button type="submit" class="btn btn-primary" id="edit-points-details">Submit</button>
    </div>
    </form>
</script>
<script>
    $(document).ready(function () {
        fetchAndRenderTableData();
    });
    function editEarnedPoints(pointsId) {
        var data = {};
        data['pointsId'] = pointsId;
        console.log(pointsId);
        $.ajax({
            url: __NVLI.context + "/rewards/edit-points-details",
            type: 'POST',
            data: data,
            success: function (jsonObject) {
                if (jsonObject !== null)
                {
                    $("#activity").text(jsonObject.pointDetails.activityDesc);
                    var pointsTpl = _.template($("#tpl-points-edit").html());
                    var Opts = {
                        "earnedPoints": jsonObject.pointDetails.earnedPoints
                    };
                    var body = pointsTpl(Opts);
                    $(".edit-body").empty();
                    $(".edit-body").html(body);
                    $("#form-edit-points-details").submit(function (e) {
                        e.preventDefault();
                        var x = $("#form-edit-points-details").serializeArray();
                        var jsonObj = {};
                        jsonObj['pointsId'] = pointsId;
                        $.each(x, function (i, field) {
                            console.log(field.name + ":" + field.value + " ");
                            jsonObj[field.name] = field.value;
                        });
                        $.post(window.__NVLI.context + "/rewards/update/points-details", jsonObj, function (response)
                        {
                            if (response.status === "update-success") {
                                console.log("success");
                                $('.edit-body').empty();
                                $("#myModal").modal("hide");
                                fetchAndRenderTableData();
                            } else if (response.status === "error") {
                                alertErrorMsg();
                            }

                        });
                        // on success reload data to render
                        return false;
                    });
                } else
                {
                    alertErrorMsg();
                }
            }
        });
    }
    function fetchAndRenderTableData() {
        $.ajax({
            url: __NVLI.context + "/rewards/getPointsDetails",
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.pointsDetailList)) {
                        $("#levels-tbl-body").html('<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var pointsTpl = _.template($("#tpl-points-tbl-row").html());
                        var Opts = {
                            "pointsDetailList": jsonObject.pointsDetailList,
                            "recordCount": 0
                        };
                        var body = pointsTpl(Opts);
                        $("#points-tbl-body").empty();
                        $("#points-tbl-body").html(body);
                        $(".btn-edit-points").unbind().bind("click", function (e) {
                            e.preventDefault();
                            $("#myModal").modal("show");
                            editEarnedPoints(e.currentTarget.getAttribute("data-point-id"));
                        });
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }
</script>