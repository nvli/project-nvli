<%-- 
    Document   : user-transction.jsp
    Created on : May 15, 2017, 11:05:14 AM
    Author     : Nitin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><tags:message code="dashboard.transaction.heading"/></div>
        <div class="card-block">
            <div id="dashboard-fav-libs">
                <div class="project-list">
                    <table class="table table-hover">
                        <tbody id="rd-tbl-body">                           
                            <c:forEach items="${userTransactionList}" varStatus="index" var="userTransaction">
                                <tr>
                                    <td class="lib-title">
                                        <i class="fa fa-history"></i>
                                        <c:choose>
                                            <c:when test="${userTransaction.resource eq 'false'}">
                                                <a href="${context}/search/preview/${userTransaction.recordId}?language=${pageContext.response.locale}">
                                                <strong>${userTransaction.itemName}</strong></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${context}/search/across/resource/${userTransaction.resourceTypeCode}/1/50?searchElement=&searchingIn=${userTransaction.resourceTypeCode}&searchComplexity=BASIC&subResourceIdentifiers[1]=${userTransaction.resourceCode}">
                                                <strong>${userTransaction.itemName}</strong></a>
                                            </c:otherwise>
                                        </c:choose>
                                        <br>
                                        <small title="Price"><strong><tags:message code="utrans.price"/> </strong><span class="label label-info"><i class="fa fa-rupee" style="color:#3333ff;"></i> ${userTransaction.totalAmount}</span> </small>
                                        &nbsp;
                                        <small title="Order Date"><strong> <tags:message code="utrans.order.date"/> </strong><span class="label label-warning">${userTransaction.orderDate}</span></small>
                                        &nbsp;
                                        <small title="Order Status"><strong><tags:message code="utrans.status"/> </strong><span class="label label-success">${userTransaction.orderStatus}</span> </small>
                                        &nbsp;
                                        <small title="Expiry Date"><strong><tags:message code="utrans.expiry.date"/> </strong><span class="label label-danger">${userTransaction.expiryDate}</span> </small>
                                    </td>
                                    <td class="lib-actions text-right"> 
                                        <strong><tags:message code="utrans.invoice"/> </strong><br/><a href="#" class=""> ${userTransaction.invoiceNo} </a>
                                    </td>
                                </tr>
                            </c:forEach>
                           
                            <c:choose>
                                 <c:when test="${hasError}">
                                    <tr id="loadMore"><td colspan="8" class="alert-danger" style="text-align:center"><b><tags:message code="utrans.alert.danger"/></b></td></tr>
                                </c:when>
                                <c:when test="${fn:length(userTransactionList) == 0}">
                                    <tr id="loadMore"><td colspan="8" class="alert-warning" style="text-align:center"><b><tags:message code="utrans.alert.warning"/></b></td></tr>
                                </c:when>
                                <c:when test="${fn:length(userTransactionList) > 9}">
                                    <tr id="loadMore"><td colspan="8" class="alert-info" style="text-align:center"><b><a href="javascript:;" class="btn-primary btn" onclick="populateTransctionTable(2,false);"><tags:message code="utrans.load"/></a></b></td></tr>                                                                    
                                </c:when>
                            </c:choose>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
        
<script type="text/javascript">
    var _dataLimit=10;
    
    /*$(document).ready(function () {
        populateTransctionTable(1,true);
    });*/
        
    function populateTransctionTable(pageNo,isFirstTimeCall) {
        $('#loadMore').remove();
        $.ajax({
            url: __NVLI.context+"/user/getTransctionByFiltersWithLimit",
            data: {
                dataLimit: _dataLimit, 
                pageNo: pageNo
            },
            type: 'POST',
            success: function (data) {                
                if(pageNo===1) {
                    $("#rd-tbl-body").empty();
                }
                var rowCount=((pageNo-1)*_dataLimit)+1;
                var rd_tbl_row_template = _.template($("#tpl-rd-tbl-row").html());
                var opts;
                var rd_tbl_row_el;

                var isMoreDataPresent=false;                
                _.each(data, function (transactionData) { 
                    isMoreDataPresent=true;
                    var link;                            
                    if(transactionData.resource === false) {

                        link = "${context}/search/preview/"+transactionData.recordId+"?language=${pageContext.response.locale}";
                    } else {
                        link = "${context}/search/across/resource/"+transactionData.resourceTypeCode+"/1/50?searchElement=&searchingIn="+transactionData.resourceTypeCode+"&searchComplexity=BASIC&subResourceIdentifiers[1]="+transactionData.resourceCode;                                
                    }                            
                    opts = {    
                        "link" :link,
                        "itemName":transactionData.itemName,
                        "totalAmount": transactionData.totalAmount,
                        "orderDate": transactionData.orderDate,
                        "orderStatus": transactionData.orderStatus,
                        "expiryDate": transactionData.expiryDate,
                        "invoiceNo": transactionData.invoiceNo
                    };
                    rd_tbl_row_el = rd_tbl_row_template(opts);
                    $("#rd-tbl-body").append(rd_tbl_row_el);
                    rowCount++;
                });
                if(isMoreDataPresent) {
                    pageNo++;
                    $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-info" style="text-align:center"><b><a href="javascript:;" class="btn-primary btn" onclick="populateTransctionTable('+pageNo+',false);">Load More</a></b></td></tr>');
                } else {
                    if(pageNo===1) {
                        $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-warning" style="text-align:center"><b>No transactions Found</b></td></tr>');
                    } else {
                        $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-success" style="text-align:center"><b>No more transactions</b></td></tr>');
                        setTimeout(function(){
                            $('#loadMore').remove();
                        }, 3000);
                    }
                }                
            },
            error: function () {           
                $("#rd-tbl-body").append('<tr id="loadMore"><td colspan="8" class="alert-danger" style="text-align:center"><b>Some error occurred. Please try after some time.</b></td></tr>');
                setTimeout(function(){
                    $('#loadMore').remove();
                }, 5000);
            }
        });
    }
    
</script>    
    
<!--template for record distribution register start here-->
<script type="text/template" id="tpl-rd-tbl-row">
    <tr>
        <td class="lib-title">
            <i class="fa fa-history"></i>                        
            <a href="{{=link}}"><strong>{{=itemName}}</strong></a>
            <br>
            <small title="Price"><strong>Price: </strong><span class="label label-info"><i class="fa fa-rupee" style="color:#3333ff;"></i> {{=totalAmount}}</span> </small>
            &nbsp;
            <small title="Order Date"><strong> Order Date: </strong><span class="label label-warning">{{=orderDate}}</span></small>
            &nbsp;
            <small title="Order Status"><strong>Status: </strong><span class="label label-success">{{=orderStatus}}</span> </small>
            &nbsp;
            <small title="Expiry Date"><strong>Expiry Date: </strong><span class="label label-danger">{{=expiryDate}}</span> </small>
        </td>
        <td class="lib-actions text-right">
            <strong>Invoice No. </strong><br/> <a href="#" class=""> {{=invoiceNo}} </a>
        </td>
    </tr>
</script>