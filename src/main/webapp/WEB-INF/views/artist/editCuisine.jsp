<%--
    Document   : editProfile
    Created on : 7 Jun, 2016, 5:53:30 PM
    Author     : richa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div style="margin-bottom:0px;" class="clearfix"></div>
<!-- search result content starts here -->
<div class="container-fluid" id="main">
    <div class="clear"></div>
    <div class="col-lg-12">
        <!--Breadcrumb-->

        <!--show hide panel-->
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div class="container">
            <form:form method="post" modelAttribute="cuisines" action="${pageContext.request.contextPath}/editresource/editrecipe" enctype="multipart/form-data" onsubmit="return Validate(this);" role="form">
                <div class="card" id="image_change">
                    <div class="card-header"><tags:message code="label.Edit.Recipe"/></div>

                    <form:hidden path="recordIdentifier"/>
                    <form:hidden path="cuisineDetail.imageName"/>
                    <form:hidden path="cuisineDetail.imageUploadPath"/>

                    <div class="card-block">
                        <div class="container">
                            <div class="form-group row">
                                <label class="form-control-label col-lg-2 col-md-5 col-sm-4 text-right text-sm-left"><tags:message code="label.Select.Picture"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <input type="hidden" class="form-control-file" id="file" name="recipeImage">
                                    <img src="${cuisines.path}" width="250" height="250" alt="No image available" id="recipeImage"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineName" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Recipe.Name"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="cuisineName" placeholder="Cuisine Name" path="cuisineDetail.cuisineName"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="state" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.state"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" placeholder="State" path="cuisineDetail.state"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineDescription" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.description"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:textarea type="text" class="form-control ltextarea" id="cuisineDescription" placeholder="Cuisine Description" path="cuisineDetail.description"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineIngredients" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Ingredients"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:textarea type="text" class="form-control ltextarea"  placeholder="Ingredients" path="cuisineDetail.ingredients"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineMethod" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Method.preparation"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:textarea type="text" class="form-control ltextarea"  placeholder="Method of preparation" path="cuisineDetail.method"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineType" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Recipe.type"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" placeholder="Recipe type" path="cuisineDetail.cuisinetype"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineCourtesy" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Courtesy"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" placeholder="Courtesy" path="cuisineDetail.courtesy"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="siteURL" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Site.url"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" placeholder="Site URL" path="cuisineDetail.siteURL"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cuisineYoutubeURL" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Recipe.Youtube.URL"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" placeholder="Recipe Youtube URL" path="cuisineDetail.cuisineYoutubeURL"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cuisineYoutubeURL" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Recipe.Youtube.Video"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <c:choose>
                                        <c:when test="${cuisines.cuisineDetail.cuisineYoutubeURL!='' && cuisines.cuisineDetail.cuisineYoutubeURL!=null}">
                                            <iframe width="560" height="315" src="${fn:replace(cuisines.cuisineDetail.cuisineYoutubeURL,'watch?v=', 'embed/')}" frameborder="0" allowfullscreen></iframe>
                                            </c:when>
                                            <c:otherwise>
                                                <tags:message code="label.No.video.available"/>
                                            </c:otherwise>
                                        </c:choose>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <label for="artistUploadPortfolio" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"> </label>
                        <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                            <button type="submit" class="btn btn-primary"><tags:message code="label.update"/></button>
                        </div>
                    </div>

                </div>
            </form:form>
        </div>
    </div>
</div>
<div class="clear22"></div>

<!-- s result content container -->
<div style="margin-bottom:20px;" class="clearfix"></div>
<script type="text/javascript" src="${pageContext.request.contextPath}/themes/js/ssi-uploader.js"></script>
<script>
    $(document).ready(function () {
        $('#file').awesomeCropper(
                {width: 200, height: 200, debug: true}
        );

        $('.yes').click(function () {
            var images = $("#file").val();
//
            if (images != "") {
                $("#recipeImage").hide();

            }
        });

    });</script>
<script type="text/javascript">

    var _validFileExtensions = [".jpg"];
    var _validSize = 1024;
    function Validate(oForm) {
        var arrInputs = oForm.getElementsByTagName("input");
        for (var i = 0; i < arrInputs.length; i++) {
            var oInput = arrInputs[i];
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    var sizeValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            var size = parseFloat(oInput.files[0].size / 1024).toFixed(2);
                            if (_validSize >= size)
                                sizeValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                    if (!sizeValid) {
                        alert("Sorry, permitted file size is " + _validSize + " KB");
                        oInput.value = "";
                        return false;
                    }
                }
            }
        }

        return true;
    }


    $(document).ready(function () {
        $('#leftTab a:first').tab('show');
    });

</script>