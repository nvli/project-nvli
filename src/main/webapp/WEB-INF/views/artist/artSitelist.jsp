<%--
    Document   : artist
    Created on : May 20, 2016, 10:11:47 AM
    Author     : Gajraj Tanwar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div id="wrapper" class="container">
    <table class="artistSites table table-striped" style="height:400px;overflow:auto;" >
        <thead>
            <tr class="text-center">
                <th><tags:message code="label.Site.id"/></th>
                <th><tags:message code="label.Artist.Site.Name"/></th>
                <th><tags:message code="label.Artists.gallery"/> </th>
                <th><tags:message code="label.Site.url"/></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${artisticResourceList}" var="artistSite">
                <tr>
                    <td><c:out value="${artistSite.artisticResourseId}"/></td>
                    <td><c:out value="${artistSite.artistSite}"/></td>
                    <td> <a href="/artist/ingallery/${artistSite.artisticResourseId}"> <tags:message code="label.View.artists"/></a> </td>
                    <td> <a href="<c:out value="${artistSite.siteName}"/>" >${artistSite.siteName}</a></td>
                    <td> <a class="btn btn-xs btn-primary update-button" id="updateButton" data-artist-id="${artistSite.artisticResourseId}"><tags:message code="label.Update.Gallery"/></a></td>
                    <td> <a class="btn btn-xs btn-primary extract-button" id="extractButton" data-artist-id="${artistSite.artisticResourseId}"><tags:message code="label.Extract.Artists.Gallery"/></a>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<script>
    function update(id) {
        $.get(window.__NVLI.context + "/artist/update/" + id, {}, function (response) {
            console.log(response);
        });
    }

    function extract(id) {
        $.get(window.__NVLI.context + "/artist/extract/" + id, {}, function (response) {
            console.log(response);
        });
    }


    $(document).ready(function () {
        $(".update-button").unbind().bind("click", function (e) {
            var artistId = e.currentTarget.getAttribute("data-artist-id");
            console.log(artistId);
            update(artistId);
        });


        $(".extract-button").unbind().bind("click", function (e) {
            alert(' extraction begun')
            var artistId = e.currentTarget.getAttribute("data-artist-id");
            console.log(artistId);
            extract(artistId);
        });
    });

</script>