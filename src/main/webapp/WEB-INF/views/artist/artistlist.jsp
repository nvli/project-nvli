<%--
    Document   : artist
    Created on : May 20, 2016, 10:11:47 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<style>
    .artistProfilestable{
        max-height: 500px;
        height:500px;
        overflow-y:scroll;
    }
    #my-table{
        border: 1px solid #ddd;
        padding: 15px;
    }
</style>
<div id="wrapper">
    <!-- Search Block -->
    <h2><tags:message code="label.artist"/> <small class="text-muted"><tags:message code="label.Gallery"/> ${artistSiteID}</small></h2>
    <div id="my-table" >
        <c:forEach items="${artistList}" var="artistProfile">

            <div class="ibox-content" style="border-bottom: 1px solid #ddd; padding: 20px 0;">
                <h3 class="text-capitalize">${artistProfile.artistName}</h3>
                <pre class=" pre-scrollable text-justify" style="white-space: pre-wrap; font-family: sans-serif;
                     word-break: normal;">${artistProfile.artistDescription}</pre>
                <a class="btn btn-xs btn-primary" href="${artistProfile.artistProfileLink}"><tags:message code="label.Profile.Link"/></a>
                <a class="btn btn-xs btn-primary update-button" id="updateButton" data-artist-id="${artistProfile.id}"><tags:message code="label.Update.Profile"/></a>

            </div>
        </c:forEach>
    </div>
</div>


<script>
    $("#my-table").slimScroll({
        height: '600px',
        size: '8px',
        position: 'right',
        color: 'black',
        alwaysVisible: false,
        disilVisible: false,
        ratance: '0px',
        railColor: '#efefef',
        railOpacity: 0.3,
        wheelStep: 10,
        allowPageScroll: false,
        disableFadeOut: false
    });

    function update(id) {
        $.get(window.__NVLI.context + "/artist/existing/update/" + id, {}, function (response) {
            console.log(response);
        });
    }


    $(document).ready(function () {
        $(".update-button").unbind().bind("click", function (e) {
            var artistId = e.currentTarget.getAttribute("data-artist-id");
            console.log(artistId);
            update(artistId);
        });

    });
</script>
