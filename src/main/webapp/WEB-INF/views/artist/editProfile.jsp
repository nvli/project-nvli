<%--
    Document   : editProfile
    Created on : 7 Jun, 2016, 5:53:30 PM
    Author     : richa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div style="margin-bottom:0px;" class="clearfix"></div>
<!-- search result content starts here -->
<div class="container-fluid" id="main">
    <div class="clear"></div>
    <div class="col-lg-12">
        <!--Breadcrumb-->

        <!--show hide panel-->
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div class="container">
            <form:form method="post" modelAttribute="artist" action="${pageContext.request.contextPath}/editresource/editartist" enctype="multipart/form-data" onsubmit="return Validate(this);" role="form">
                <form:hidden path="recordIdentifier" id="recordIdentifier" />
                <form:hidden path="artistProfile.imageUploadPath" id="imageUploadPath"/>
                <div class="card" id="image_change">
                    <div class="card-header"><tags:message code="profile.edit.profile"/></div>
                    <input type="hidden" name="portfolioUpload" id="portfolioUpload"/>
                    <div class="card-block">
                        <div class="container">
                            <div class="form-group row">
                                <label class="form-control-label col-lg-2 col-md-5 col-sm-4 text-right text-sm-left"><tags:message code="label.Select.Picture"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <input type="hidden" class="form-control-file" id="file" name="profilePic">
                                    <!--<input type="file" name="fileUpload" >-->
                                    <c:forEach items="${artist.artistProfile.artistImages}" var="portFolio" varStatus="imageCount">
                                        <c:if test="${portFolio.imageType=='Profile Pic'}">
                                            <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageName" />
                                            <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageTitle" />
                                            <form:hidden path="artistProfile.artistImages[${imageCount.index}].imagePath" />
                                            <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageDescription" />
                                            <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageType" />
                                            <img src="${portFolio.imagePath}" width="250" height="250" alt="No image available" id="profileImage"/>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistName" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.artistname"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistName" placeholder="Artist Name" path="artistProfile.artistName"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistProgileLink" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-smf-left"><tags:message code="label.Artist.profile.link"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistProgileLink" placeholder="Artist profile link" path="artistProfile.artistProfileLink"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistImageUlr" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right texft-sm-left"><tags:message code="label.Artist.image.url"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistImageUlr" placeholder="Artist image url" path="artistProfile.artistImageURL"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistDescription" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Artist.Description"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:textarea type="text" class="form-control ltextarea" id="artistDescription" placeholder="Artist Description" path="artistProfile.artistDescription"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="addressLine1" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Address.Line1"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="addressLine1" placeholder="Address Line 1" path="artistProfile.addressLine1"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="addressLine2" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Address.Line2"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="addressLine2" placeholder="Address Line 2" path="artistProfile.addressLine2"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistCity" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.city"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistCity" placeholder="City" path="artistProfile.city"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistState" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.state"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistState" placeholder="State" path="artistProfile.state"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistPincode" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.pincode"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistPincode" placeholder="Pincode" path="artistProfile.pincode"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistContact" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.contact"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistContact" placeholder="Contact" path="artistProfile.contact"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistSocialcontact" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.socialcontact"/> </label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistSocialcontact" placeholder="Social contact" path="artistProfile.socialContact"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistOther" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.other"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <form:input type="text" class="form-control" id="artistOther" placeholder="Other" path="artistProfile.other"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="artistUploadPortfolio" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"><tags:message code="label.Portfolioupload"/></label>
                                <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                                    <!-- <input type="file" class="form-control-file" id="artistUploadPortfolio" name="portfolioUpload" multiple="">-->
                                    <input type="file" multiple id="ssi-upload"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <label for="artistUploadPortfolio" class=" col-lg-2 col-md-5 col-sm-4 form-control-label text-right text-sm-left"> </label>
                        <div class="col-sm-8 col-md-7 col-lg-6 col-xs-12">
                            <button type="submit" class="btn btn-primary"><tags:message code="label.update"/></button>
                        </div>
                    </div>

                </div>
                <c:set var="count" value="0" scope="page" />
                <c:forEach items="${artist.artistProfile.artistImages}" var="portFolio" varStatus="imageCount">
                    <c:if test="${portFolio.imageType=='Portfolio Image'}">
                        <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageName" />
                        <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageTitle" />
                        <form:hidden path="artistProfile.artistImages[${imageCount.index}].imagePath" />
                        <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageDescription" />
                        <form:hidden path="artistProfile.artistImages[${imageCount.index}].imageType" />
                        <c:set var="count" value="${count + 1}" scope="page"/>
                    </c:if>
                </c:forEach>
                <input type="hidden" id="portFolioCnt" value="${count}"/>
            </form:form>
            <!-- portfolio container starts here -->
            <div class="card m-t-3">
                <div class="card-header"><tags:message code="label.Portfolio"/> </div>
                <div class="documentList clearfix" >
                    <div class="scroll_list row">
                        <c:forEach items="${artist.artistProfile.artistImages}" var="portFolio" varStatus="imageCount">
                            <c:if test="${portFolio.imageType=='Portfolio Image'}">

                                <c:set var="imageId" value="${fn:split(portFolio.imageName, '.')}" />

                                <div class="col-xs-12 col-lg-3 col-md-4 col-sm-6 documentItem" id="${imageId[0]}">
                                    <div class="card">
                                        <div class="card-img-top"> <span class="vimg_c"> <img src="${portFolio.imagePath}"  class="center-block"/></span></div>
                                        <div id="${imageId[0]}_div">
                                            <h4 class="card-title">${portFolio.imageTitle}</h4>
                                            <p class="hidden60">${portFolio.imageDescription}</p>
                                        </div>
                                        <div id="${imageId[0]}_div_edit" style="display: none">
                                            <form action="${pageContext.request.contextPath}/editresource/editporfolio" method="post">
                                                <input type="hidden" name="recordIdentifier" value="${artist.recordIdentifier}"/>
                                                <input type="hidden" name="imageName" value="${portFolio.imageName}"/>
                                                <input type="hidden" name="imagePath" value="${portFolio.imagePath}"/>
                                                <input type="hidden" name="imageType" value="${portFolio.imageType}"/>
                                                <input type="text" class="form-control" name="imageTitle" value="${portFolio.imageTitle}" placeholder="Title"/>
                                                <textarea class="form-control" name="imageDescription" style="margin-bottom: 5px;" placeholder="Description">${portFolio.imageDescription}</textarea>
                                                <input type="submit" value="save" class="btn btn-primary"/>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm1 btn-secondary"
                                                onclick="return removeImage('${portFolio.imageName}', '${portFolio.imageTitle}', '${portFolio.imageDescription}', '${portFolio.imageType}', '${portFolio.imagePath}');"><i class="fa fa-trash"></i></button> <button class="btn btn-sm1 btn-secondary" onclick="makeEditablePortfolio('${imageId[0]}');"><i class="fa fa-pencil"></i> </button>  </div>
                                </div>
                            </c:if>
                        </c:forEach>

                        <!-- ends here scroll -->

                    </div>
                </div>
            </div>
            <!-- portfolio container ends here -->
        </div>
    </div>
</div>
<div class="clear22"></div>

<!-- s result content container -->
<div style="margin-bottom:20px;" class="clearfix"></div>

<script>
    $(document).ready(function () {
        $('#file').awesomeCropper(
                {width: 200, height: 200, debug: true}
        );

        $('.yes').click(function () {
            var images = $("#file").val();
            //
            if (images != "") {
                $("#profileImage").hide();
            }
        });

        $('#ssi-upload').ssi_uploader({dropZone: false, maxFileSize: 6, allowed: ['jpg', 'gif', 'png', 'pdf']});
    });
</script>
<script type="text/javascript">
    var portFolioImgCnt = 0;
    $('#ssi-upload').bind('change', function (e) {

        //t h is. f iles[0].size gets the size of your file.
        portFolioImgCnt += this.files.length;
        if (parseInt($("#portFolioCnt").val()) + portFolioImgCnt > 10) {
            alert("Max 10 portfolio images can be uploaded");
            $('#ssi-upload').val("");
            portFolioImgCnt = 0;
        }

    });

    var _validFileExtensions = [".jpg"];
    var _validSize = 1024;
    function Validate(oForm) {
        var arrInputs = oForm.getElementsByTagName("input");
        for (var i = 0; i < arrInputs.length; i++) {
            var oInput = arrInputs[i];
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    var sizeValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            var size = parseFloat(oInput.files[0].size / 1024).toFixed(2);
                            if (_validSize >= size)
                                sizeValid = true;
                            break;
                        }
                    }
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                    if (!sizeValid) {
                        alert("Sorry, permitted file size is " + _validSize + " KB");
                        oInput.value = "";
                        return false;
                    }
                }
            }
        }

        var result = createJsonString();
        $("#portfolioUpload").val(result);
        return true;
    }

    function createJsonString() {
        var tables = [];
        _.each($(".ssi-imgToUploadTable"), function (table) {
            var imgToUploadSRC = $(table).find(".ssi-imgToUpload").attr('src');
            var name = $(table).find("input[type='text']")[0].value;
            var desc = $(table).find("textarea")[0].value;

            tables.push({
                img: imgToUploadSRC,
                name: name,
                desc: desc
            });
        });
        return JSON.stringify({data: tables});

    }

    $(document).ready(function () {
        $('#leftTab a:first').tab('show');
    });
    function removeImage(imageName, imageTitle, imageDescription, imageType, imagePath)
    {

        var artistImage = {
            imageType: imageType,
            imagePath: imagePath,
            imageTitle: imageTitle,
            imageDescription: imageDescription,
            imageName: imageName
        };
        var identifier = $("#recordIdentifier").val();
        var jsonString = JSON.stringify(artistImage);
        $.ajax({
            url: '${pageContext.request.contextPath}/editresource/removeImage/' + identifier,
            data: {'artistImage': jsonString, 'imageUploadPath': $("#imageUploadPath").val()},
            type: 'post',
            success: function (response) {
                var imgDivID = imageName.split(".");
                $("#" + imgDivID[0]).remove();
                alert(response);
                window.location = '${pageContext.request.contextPath}/editresource/showartist/' + identifier
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Some error occured");
            }
        });
    }

    function makeEditablePortfolio(id) {
        $("#" + id + "_div").hide();
        $("#" + id + "_div_edit").show();
    }
</script>