<%--
    Document   : viewProfile
    Created on : 8 Jun, 2016, 2:32:27 PM
    Author     : richa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<div id="wrapper">


    <a href="${pageContext.request.contextPath}/artist/showartist/${artist.id}"><font color="red" size="15"><tags:message code="label.edit"/></font></a> <br>
    <tags:message code="label.Profilephoto"/> <img src="${pageContext.request.contextPath}artist/getartistimage/${imageName}" width="200" height="200" alt="No image available"/> <br>

    <table>
        <tr>
            <td>

                <tags:message code="label.ResourceSite"/>
            </td>
            <td>
                ${artist.artistResourceID.artistSite}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.Sitename"/>
            </td>
            <td>
                ${artist.artistResourceID.siteName}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.artistname"/>
            </td>
            <td>
                ${artist.artistName}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.Artist.profilelink"/>
            </td>
            <td>
                ${artist.artistProfileLink}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.Artist.imageurl"/>
            </td>
            <td>
                ${artist.artistImageURL}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.Artist.Description"/>
            </td>
            <td>
                ${artist.artistDescription}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.Address.Line1"/>
            </td>
            <td>
                ${artist.addressLine1}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.Address.Line2"/>
            </td>
            <td>
                ${artist.addressLine2}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.city"/>
            </td>
            <td>
                ${artist.city}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.state"/>
            </td>
            <td>
                ${artist.state}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.pincode"/>
            </td>
            <td>
                ${artist.pincode}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.contact"/>
            </td>
            <td>
                ${artist.contact}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.socialcontact"/>
            </td>
            <td>
                ${artist.socialContact}
            </td>
        </tr>
        <tr>
            <td>
                <tags:message code="label.other"/>
            </td>
            <td>
                ${artist.other}
            </td>
        </tr>
    </table>
</div>
