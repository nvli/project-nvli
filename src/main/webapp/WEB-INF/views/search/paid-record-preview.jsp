<%-- 
    Document   : paid-rcord-preview
    Created on : May 24, 2017, 5:31:43 PM
    Author     : Nitin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-lg-3 col-md-4 col-sm-12">
    <div class="card clearfix">
        <h5 class="card-header"> Content List </h5>
        <ul class="list_sbar clearfix click-first-anchor">
            <c:if test="${not empty record_file_list}">
                <c:forEach items="${record_file_list}" var="fileName">
                    <li>
                        <a href="javascript:;" class="record-list-item" data-record-identifier="${recordIdentifier}" data-file-name="${fileName}">
                            <span title="${fileName}" class="l_txt">
                                <i class="fa fa-file-pdf-o text-danger m-r-1" aria-hidden="true"></i>
                                ${fileName}
                            </span>
                        </a>
                    </li>
                </c:forEach>
            </c:if>
        </ul>
    </div>
</div>
<!-- right side content starts here -->
<div class="col-lg-9 col-md-8 col-sm-12 ">
    <div class="clearfix card bg-greyf6 thin_brdcr">
        <!-- Page Content -->
        <h5 class="card-header"></h5>
        <div class="m-ua-1">
            <div style="margin-bottom:0px;" class="clearfix"></div>
            <div id="viewall"></div>
        </div>
    </div>
</div>
<!-- right side content ends here -->
<script type="text/javascript">
    function renderFile(e){
        var fileName=e.currentTarget.getAttribute("data-file-name");
        var recordIdentifier=e.currentTarget.getAttribute("data-record-identifier");
        if (fileName.indexOf(".mp4") != -1) {
            var displayHTML = "<div class='videop marginauto'><div class='video_innerbox1'> <video controls controlsList='nodownload'><source src='${context}/paymentws/record-file-path/" + recordIdentifier + "/"+fileName+"' type='video/mp4'>Your browser does not support the video tag.</video></div></div>";
            $('#viewall').html(displayHTML);
        } else if (fileName.indexOf(".mp3") != -1) {
            var displayHTML = " <div class='audiop marginauto'><div class='audio_innerbox'><audio controls><source src='${context}/paymentws/record-file-path/" + recordIdentifier + "/"+fileName+"' type='audio/mpeg'></audio></div></div>";
            $('#viewall').html(displayHTML);
        }
    }

    $(".record-list-item").ready(function (){
        $(".record-list-item").unbind().bind("click",renderFile);
        $('.click-first-anchor a:first').click();
    });
</script>