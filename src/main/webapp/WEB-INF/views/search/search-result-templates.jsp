<%-- 
    Document   : search-result-templates
    Created on : Jul 6, 2017, 5:39:18 PM
    Author     : Vivek Bugale
--%>
<!--templates start here-->
<!-- Web Discovery li template start ------------------------------------------>
<script type="text/template" id="tpl-web-discovery-li">
<li class="list-group-item nobr clearfix" title={{=result}}">
    <span class="truncate">
        <label class="checkbox-inline" style="margin-top:-8px"> 
            <a href="javascript:;" class="viewAll" data-view="{{=resourceType}}">
                {{=result}}
            </a>
        </label>
    </span>
</li>
</script>
<!-- Web Discovery li template end -------------------------------------------->
<script type="text/template" id="tpl-ads">
<div class="panel m-b-1">
<a href="{{=destinationUrl}}" target="_blank">
<img class="img-responsive" src="{{=imageUrl}}" title="ad" width="100%">
</a>
</div>
</script>
<!-- Resource Type Filters start ---------------------------------------------->
<script type="text/template" id="all-resources-ul-tpl">
<div class="card">
    <div class="card-header-s">
        Resource Types
        <span class="all-resources-ul-loader" style="display: none">
            <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
        </span>
    </div>
    <div class="overflow410 ">
        <div class="m-ga-1">
            <div class="panel">
                <div class="in_2box">
                    <ul class="list-group sideb clearfix" id="all-resources-ul">
                        <div class="no-result-found alert-warning" style="text-align: center;padding: .75rem;display: none;">
                            <b><tags:message code="label.noResultsFound"/></b>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<script type="text/template" id="all-resources-li-tpl">
{{ if(resultCount!==0){ }}
    <li class="list-group-item nobr" title="{{=resourceTypeName}}">
        <span class="truncate">
            <label class="checkbox-inline resource-type-filter-label">
                <input type="checkbox" id="rResource-type_{{=parentIndex}}" class="chkboxfacet resource-type-filter-chk" value="{{=resourceTypeCode}}" name="allResourcesFilters"  {{=rchecked}}>
                {{=resourceTypeName}}
            </label>
        </span>
        <span class="count">({{=resultCount}})</span>
    </li>
{{ } }}
<input type="hidden" id="allResources[{{=resourceTypeCode}}]" name="allResources[{{=resourceTypeCode}}]" value="{{=resultCount}}">
</script>
<!-- Resource Type Filters end ------------------------------------------------>
<!-- Filters start ------------------------------------------------------------>
<script type="text/template" id="filters-tpl">
<div class="card">
    <div class="card-header-s">
        {{=headerLabel}}
        <span class="web-discovery-loader" style="display: none">
                <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
        </span> 
    </div>
    <div class="overflow330">
        <div class="m-ga-1">
            <div id="accordion1" class='{{=filterName}}' role="tablist" aria-multiselectable="true" style="text-transform: capitalize;">
            
            </div>
        </div>
    </div>
</div>
</script>
<script type="text/template" id="filters-ul-tpl">    
<div class="panel">
    <div class="panel-heading" role="tab" id="heading{{=diffKey}}{{=parentIndex}}"> 
        <a data-toggle="collapse" data-parent=".{{=filterName}}" href="#collapse{{=diffKey}}{{=parentIndex}}" aria-expanded="true" aria-controls="collapse{{=diffKey}}{{=parentIndex}}" {{ if(fchecked===''){ }}class="collapsed"{{ } }}> 
            {{=facetCategory}} 
        </a> 
    </div>
    <div id="collapse{{=diffKey}}{{=parentIndex}}" {{ if(fchecked===''){ }}class="panel-collapse collapse" aria-expanded="true"{{ } else { }} class="panel-collapse collapse in" {{ } }} role="tabpanel" aria-labelledby="heading{{=diffKey}}{{=parentIndex}}">
        <div class="in_2box">
            <ul class="list-group sideb clearfix" id="{{=filterName}}-{{=facetCategory}}-ul">
                {{ var facetIndex=0;}}
                {{_.each(facetValueList,function(facetValue){ var fcheckedvalue=''; if(fchecked!=='' && _.contains(filters,facetValue.name)){fcheckedvalue=fchecked;} }}
                        <li class="list-group-item nobr clearfix" title="{{=facetValue.name}}">
                            <span class="truncate">
                                <label class="checkbox-inline" style="margin-top:-20px">
                                    <input type="checkbox" id="{{=diffKey}}{{=separateStringByhyphen(facetCategory)}}_{{=facetIndex}}" class="chkboxfacet" value="{{=facetValue.name}}" data-facet-name="{{=facetCategory}}" {{ if(diffKey==='f'){ }} name="filters[{{=facetCategory.toLowerCase()}}][{{=facetIndex++}}]" {{ } else { }} name="specificFilters[{{=facetCategory}}][{{=facetIndex++}}]" {{ } }} {{=fcheckedvalue}}>
                                </label>
                                {{=facetValue.name}}
                            </span>
                            <span class="count">
                                {{ if(isWebDiscovery(_viewAllResourceType)){ }}
                                    {{if (_viewAllResourceType === "WIKI" || _viewAllResourceType === "OCLC") { }}
                                        ({{=facetValue.count}})
                                    {{ } }}                            
                                {{ }else{ }}
                                        ({{=facetValue.count}})
                                {{ } }}
                            </span>
                        </li>    
                {{ });}}                    
            </ul>
        </div>
    </div>
</div>
</script>
<!-- Filters end -------------------------------------------------------------->
<!-- Web Scale Discovery start ------------------------------------------------>
<script type="text/template" id="web-scale-discovery-ul-tpl"> 
<div class="m-t-2 card" id="webDiscovery">
    <div class="card-header-s">
        Web Scale Discovery
        <span class="web-discovery-loader" style="display: none">
                <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
        </span> 
    </div>
    <div class="overflow410 ">
        <div class="m-ga-1">
            <div class="panel">
                <div class="in_2box">
                    <ul class="list-group sideb clearfix" id="web-discovery-ul">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</script>
<!-- Web Scale Discovery end--------------------------------------------------->
<!-- OCLC start---------------------------------------------------------------->
<script type="text/template" id="oclc-resource-header-list">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><div class="masonry-item" id="masonary-item-oclc">
    <div id="header-{{=resourceType}}-resource"></div>
    </div>
    </div>
</script>
<script type="text/template" id="oclc-resource-list">

    <div class="card-header clearfix card-hdr1"><img src="${rTIconLocation}/{{=resourceType}}.png" width="32" height="32" alt="NVLI" class="card_icon">{{=resourceType}}
    <div class="pull-rights font12"><a id="view" class="link3 view"  href="{{=link}}"> <tags:message code="label.view.all"/> </a></div>
    </div>

</script>
<script type="text/template" id="oclc-resource-content-header-list">
    <h3>
    <a href="{{=recordIdentifier}}">
    {{=nameToView}}
    </a>
    </h3>
</script>
<!-- OCLC end ----------------------------------------------------------------->
<!-- Google Book start -------------------------------------------------------->
<script type="text/template" id="gb-resource-header-list">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> <div class="masonry-item" id="masonary-item-gb">
    <div id="header-{{=resourceType}}-resource"></div>
    </div>
    </div>
</script>
<script type="text/template" id="gb-resource-list">
    <div class="card-header clearfix card-hdr1"><img src="${rTIconLocation}/${resourceType.resourceTypeIcon}"/{{=resourceType}}.png" width="32" height="32" alt="NVLI" class="card_icon">{{=resourceType}}
    <div class="pull-rights font12"><a id="view" class="link3 view"  href="{{=link}}"> <tags:message code="label.view.all"/> </a> </div>
    </div>
</script>
<script type="text/template" id="gb-resource-content-header-list">
    <h3>
    <a href="{{=recordIdentifier}}">
    {{=nameToView}}
    </a>
    </h3>
</script>
<!-- Google Book end ---------------------------------------------------------->
<!-- Open Library start ------------------------------------------------------->
<script type="text/template" id="ol-resource-header-list">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">   <div class="masonry-item" id="masonary-item-ol">
    <div id="header-{{=resourceType}}-resource"></div>
    </div>
    </div>
</script>
<script type="text/template" id="ol-resource-list">
    <div class="card-header clearfix card-hdr1"><img src="${rTIconLocation}/{{=resourceType}}.png" width="32" height="32" alt="NVLI" class="card_icon">{{=resourceType}}
    <div class="pull-rights font12"><a id="view" class="link3 view"  href="{{=link}}"> <tags:message code="label.view.all"/> </a></div>
    </div>
</script>
<script type="text/template" id="ol-resource-content-header-list">
    <h3>
    <a href="{{=recordIdentifier}}">
    {{=nameToView}}
    </a>
    </h3>
</script>
<!-- Open Library end --------------------------------------------------------->
<!-- Wiki start --------------------------------------------------------------->
<script type="text/template" id="wk-resource-header-list">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">   <div class="masonry-item" id="masonary-item-wk">
    <div id="header-{{=resourceType}}-resource"></div>
    </div>
    </div>
</script>
<script type="text/template" id="wk-resource-list">
    <div class="card-header clearfix card-hdr1"><img src="${rTIconLocation}/{{=resourceType}}.png" width="32" height="32" alt="NVLI" class="card_icon">Wikipedia
    <div class="pull-rights font12"><a id="view" class="link3 view"  href="{{=link}}"> <tags:message code="label.view.all"/> </a></div>
    </div>
</script>
<script type="text/template" id="wk-resource-content-header-list">
    <h3>
    <a href="{{=recordIdentifier}}">
    {{=nameToView}}
    </a>
    </h3>
</script>
<!-- Wiki end ----------------------------------------------------------------->
<!-- Trove mode non multimedia template start --------------------------------->
<script type="text/template" id="resource-result-tpl">
    <div class="masonry-item">
        <div>
            <div class="card-header clearfix">               
                <img src="${rTIconLocation}/{{=resourceTypeIcon}}" width="32" height="32" alt="NVLI" class="card_icon">
                {{=resourceTypeName}}
                <small>({{=searchResultResponse.resultSize}} results)</small>
                <div class="pull-rights font12">
                    <a href="javascript:;" class="link3 viewAll" data-view="{{=resourceTypeCode}}"><tags:message code="label.view.all"/> </a> 
                </div>
            </div>
            <section class="card-block clearfix">
                {{ _.each(searchResultResponse.listOfResult, function (searchResult){ }}    
                    <article class="post clearfix">
                        <div>
                            <h3>   
                                {{ if( checkTitleResourceTypeCode(searchResult) && searchResult.source != null){ }}  
                                        <a href="{{=searchResult.source[0]}}" class="card-link pull-left" target="_blank">
                                {{ }else{ }}       
                                        <a href="${context}/search/preview/{{=searchResult.recordIdentifier}}?language=${pageContext.response.locale}">   
                                {{ } }}  
                                            {{=searchResult.nameToView}}  

                                        </a>
                                            {{ if(!_.isEmpty(searchResult.imagePath)){ }}
                                                    <img src="${context}/themes/images/{{= getExtension(searchResult.imagePath) }}.png" alt="{{=searchResult.resourceType}}" height="20px" width="20px" style="margin-top: -15px;"/>
                                            {{ } }}
                                
                                {{ if(fullText){ }}
                                    <div style="float: right;font-size: medium;color:#F16C20;font-weight: initial;">
                                        <a style="font-size: small;max-width: 100% !important;color: #F16C20;" title="Match found in document" href="${context}/search/preview/all/jpeg/{{=searchResult.recordIdentifier}}?searchElement={{=searchElement}}&language=${pageContext.response.locale}&fullTextFlag=true">
                                            View Book ({{=searchResult.termCount}})
                                        </a>
                                    <div/> 
                                {{ } }} 
                            </h3>
                            <table cellpadding="0" cellspacing="0" class="table table-bordered1 text-left table-sm">
                                <tbody> 
                                    {{ if(!_.isEmpty(searchResult.subject)){ }}    
                                        <tr class="col_head">
                                            <td class="text-right" width="140"><tags:message code="label.subject"/> :</td>
                                            <td>
                                                <div class="overflow41hdn">
                                                    {{= separatedByComma(searchResult.subject)}}                                                                      
                                                </div>                                                           
                                            </td>
                                        </tr>  
                                    {{ } }}  
                                    {{ if(!_.isEmpty(searchResult.description)){ }}    
                                        <tr class="col_head">
                                            <td class="text-right" width="140"><tags:message code="label.description"/> :</td>
                                            <td>
                                                <div class="overflow41hdn">
                                                    {{ _.each(searchResult.description, function (description){ }}
                                                            {{=description}}
                                                    {{  }); }}     
                                                </div>                                                           
                                            </td>
                                        </tr>  
                                    {{ } }}    
                                    {{ if(!_.isEmpty(searchResult.creator)){ }}    
                                        <tr class="col_head">
                                            {{ if(searchResult.resourceType === 'VALUE') { }}
                                               <td class="text-right" width="140"><tags:message code="label.recepient"/> :</td>
                                            {{ }else{ }}
                                               <td class="text-right" width="140"><tags:message code="label.creator"/> :</td>
                                            {{ } }}
                                            <td>
                                                {{=separatedByComma(searchResult.creator)}}                      
                                            </td>
                                        </tr>  
                                    {{ } }}  
                                    {{ if(!_.isEmpty(searchResult.publisher)){ }}    
                                        <tr class="col_head">
                                            <td class="text-right" width="140"><tags:message code="label.publisher"/> :</td>                                                            
                                            <td>
                                                {{=separatedByComma(searchResult.publisher)}}                    
                                            </td>
                                        </tr>  
                                    {{ } }}   
                                    {{ if(checkTitleResourceTypeCode(searchResult)){ }}    
                                        {{ if(!_.isEmpty(searchResult.data)){ }}
                                        <tr class="col_head word_wrap_1">
                                            <td class="text-right" width="140"><tags:message code="label.date"/> :</td>
                                            <td>
                                                {{= separatedByComma(searchResult.data)}}                            
                                            </td>
                                        </tr>
                                        {{ } }}
                                    {{ } }}    
                                    {{ if(searchResult.resourceType == 'WIKI' || searchResult.resourceType == 'ENEW' || searchResult.resourceType == 'GOVW'){ }}    
                                        {{if(!_.isEmpty(searchResult.language)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.language"/> :</td>
                                                <td>
                                                    {{= separatedByComma(searchResult.language)}}                                                                                                                            
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }} 
                                    {{ if(searchResult.resourceType == 'ENEW' || searchResult.resourceType == 'GOVW' || searchResult.resourceType == 'MANS'){ }}    
                                        {{if(!_.isEmpty(searchResult.source)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.source"/> :</td>
                                                <td>
                                                    <a href="{{=searchResult.source[0]}}" style="color:#2a5cea;" target="_blank">{{=searchResult.source[0]}}</a>                                                                                                                          
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }}     
                                    {{ if(searchResult.resourceType == 'VALUE'){ }}    
                                        {{if(!_.isEmpty(searchResult.type)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.type"/> :</td>
                                                <td>
                                                    {{= separatedByComma(searchResult.type)}}                                                                                                                            
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }}                                                      
                                </tbody>
                            </table>
                            {{ if(!isExternalResource(searchResult.resourceType)){  }}
                                <div class="clearfix m-t-1 postfooter">
                                    <ul class="nav nav-pills  font85 marginauto pull-left">
                                        <li class="nav-item">
                                                <span  class="rateit m-r-1" id="rateit5"
                                                data-rateit-min="0" data-rateit-max="5" data-rateit-value="{{=searchResult.ratingScore}}" 
                                                data-rateit-ispreset="true" data-rateit-readonly="true">
                                                </span>
                                        </li>
                                        <li class="nav-item "> 
                                            <span class="m-r-1">
                                                <i class="fa fa-eye"></i> 
                                                <tags:message code="label.views"/>
                                                    ({{=searchResult.recordViewCount}})
                                            </span>
                                        </li>
                                        <li class="nav-item ">
                                                <span class="m-r-1"> 
                                                    <i class="fa fa-thumbs-up"></i> 
                                                    <tags:message code="label.likes"/> 
                                                    ({{=searchResult.recordLikeCount}})
                                                </span>
                                        </li>
                                        <li class="nav-item ">
                                            <span class="m-r-1">
                                                <i class="fa fa-share-alt"></i> 
                                                <tags:message code="label.shares"/>
                                                ({{=searchResult.recordNoOfShares}})
                                            </span>
                                        </li>
                                        <li class="nav-item "> 
                                            <span class="m-r-1"> 
                                                <i class="fa fa-files-o"></i> 
                                                <tags:message code="label.reviews"/> 
                                                ({{=searchResult.recordNoOfReview}})
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            {{ } }}
                        </div>
                        <div class="clear"></div>
                    </article>
                {{ }); }}
            </section>
        </div>
    </div>   
</script>  
<!-- Trove mode non multimedia template end ----------------------------------->
<!-- Trove mode multimedia template start ------------------------------------->
<script type="text/template" id="resource-result-multimedia-tpl">
    <div class="masonry-item">
        <div>
            <div class="card-header clearfix">               
                <img src="${rTIconLocation}/{{=resourceTypeIcon}}" width="32" height="32" alt="NVLI" class="card_icon">
                {{=resourceTypeName}}
                <small>({{=searchResultResponse.resultSize}} results)</small>
                <div class="pull-rights font12">
                    <a href="javascript:;" class="link3 viewAll" data-view="{{=resourceTypeCode}}"><tags:message code="label.view.all"/> </a> 
                </div>
            </div>
            <section class="card-block clearfix">
                {{ _.each(searchResultResponse.listOfResult, function (searchResult){ }}    
                    <article class="post adv_imageThumb clearfix col-xl-4 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div>
                            <div class="column">
                                {{= insertTag(searchResult.imagePath,resourceTypeCode) }}
                            </div>
                            
                            {{ if( checkTitleResourceTypeCode(searchResult) && searchResult.source != null){ }}  
                                <a href="{{=searchResult.source[0]}}" class="card-link pull-left" target="_blank">
                            {{ }else{ }}       
                                <a href="${context}/search/preview/{{=searchResult.recordIdentifier}}?language=${pageContext.response.locale}">   
                            {{ } }} 
                                    <span class="imagealt">
                                        {{=searchResult.nameToView}}
                                    </span>
                                </a>
                                               
                            {{ if(!isExternalResource(searchResult.resourceType)){  }}
                                <div class="clearfix m-t-1 postfooter text-center">                                    
                                    <ul class="nav nav-pills  font85 marginauto">                                           
                                        <li class="nav-item "> 
                                            <span class="m-r-1">
                                                <i class="fa fa-eye"></i> 
                                                ({{=searchResult.recordViewCount}})
                                            </span>
                                        </li>
                                        <li class="nav-item ">
                                            <span class="m-r-1"> 
                                                <i class="fa fa-thumbs-up"></i> 
                                                ({{=searchResult.recordLikeCount}})
                                            </span>
                                        </li>
                                        <li class="nav-item ">
                                            <span class="m-r-1">
                                                <i class="fa fa-share-alt"></i> 
                                                ({{=searchResult.recordNoOfShares}})
                                            </span>
                                        </li>
<%--                                        <li class="nav-item "> 
                                            <span class="m-r-1"> 
                                                <i class="fa fa-files-o"></i> 
                                                ({{=searchResult.recordNoOfReview}})
                                            </span>
                                        </li>--%>
                                    </ul>
                                    <div class="nav-item">
                                        <span  class="rateit" id="rateit5"
                                                data-rateit-min="0" data-rateit-max="5" data-rateit-value="{{=searchResult.ratingScore}}" 
                                                data-rateit-ispreset="true" data-rateit-readonly="true">
                                        </span>
                                    </div>
                                </div>
                            {{ } }}   
                        </div>
                        <div class="clear"></div>
                    </article>
                {{ }); }}
            </section>
        </div>
    </div>   
</script>
<!-- Trove mode multimedia template end --------------------------------------->
<!-- Mixed mode non multimedia template start --------------------------------->
<script type="text/template" id="mixed-resource-result-tpl">
    <article class="post">
        <div class="">
            <div class="">
                <div class="clearfix">
                    <div class="bg-thumb">
                        <div class="input-group col-lg-1 col-md-3 col-sm-12 input-group-lg">
                            <span class="input-group-addon"></span>     
                            <img src="{{= getImageSrc(searchResult.imagePath,imageURL,resourceTypeName) }}" onerror="this.src='{{=imageURL}}'" alt="{{=resourceTypeName}}" >                           
                        </div>
                    </div>
                    <div class="col-lg-11 col-md-9 col-sm-12  post-content">                        
                        <h3 class="card-header1 clearfix">
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                {{ if(checkTitleResourceTypeCode(searchResult) && !_.isEmpty(searchResult.source)){ }}
                                        <a href="{{=searchResult.source[0]}}" class="card-link pull-left" target="_blank">                      
                                {{ }else if(isWebDiscovery(resourceTypeName)) {  }}       
                                        {{ if(resourceTypeName === "WIKI"){ }} 
                                            <a href="{{=searchResult.source[0]}}" class="card-link pull-left" target="_blank">
                                        {{ } else { }}  
                                            <a href="{{=searchResult.recordIdentifier}}" class="card-link pull-left" target="_blank">
                                        {{ } }}  
                                {{ }else { }}       
                                        <a href="${context}/search/preview/{{=searchResult.recordIdentifier}}?language=${pageContext.response.locale}" class="card-link pull-left">
                                {{ } }}  
                                                {{=searchResult.nameToView}}                                               
                                            </a> 
                                            {{ if(!_.isEmpty(searchResult.imagePath)){ }} 
                                                <img src="${context}/themes/images/{{= getExtension(searchResult.imagePath) }}.png" alt="{{=searchResult.resourceType}}" height="20px" width="20px" style="margin-left: 10px;"/> 
                                            {{ } }}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 searchiconleft"> 
                                <span class="rtype"><img src="${rTIconSearchLocation}/{{=resourceTypeSearchIcon}}" alt="{{=resourceTypeSearchIcon}}" height="32" class="pull-left" style="margin-top:-4px;" title="{{=resourceTypeName}}"/></span>
                                {{if(fullText){ }}
                                    <div style="float: right;font-size: medium;color:#F16C20;font-weight: initial;">
                                        <a style="margin-right: 20px;font-size: small;max-width: 100% !important;color: #F16C20;" title="Match found in document" href="${context}/search/preview/all/jpeg/{{=searchResult.recordIdentifier}}?searchElement={{=searchElement}}&language=${pageContext.response.locale}&fullTextFlag=true">
                                            View Book ({{=searchResult.termCount}})
                                        </a>
                                    <div/> 
                                {{ } }} 
                            </div>
                        </h3>
                        <table class="table table-bordered1 text-left table-sm">
                            <tbody>     
                                    {{ if(!_.isEmpty(searchResult.subject)){ }}    
                                        <tr class="col_head word_wrap_1">
                                            <td class="text-right" width="140"><tags:message code="label.subject"/> :</td>
                                            <td>
                                                <div class="overflow41hdn">
                                                    {{= separatedByComma(searchResult.subject)}}                                                                      
                                                </div>                                                           
                                            </td>
                                        </tr>  
                                    {{ } }}  
                                    {{ if(!_.isEmpty(searchResult.description)){ }}    
                                        <tr class="col_head word_wrap_1">
                                            <td class="text-right" width="140"><tags:message code="label.description"/> :</td>
                                            <td>
                                                <div class="overflow41hdn">
                                                    {{ _.each(searchResult.description, function (description){ }}
                                                            {{=description}}
                                                    {{  }); }}     
                                                </div>                                                           
                                            </td>
                                        </tr>  
                                    {{ } }}    
                                    {{ if(!_.isEmpty(searchResult.creator)){ }}    
                                        <tr class="col_head word_wrap_1">
                                            {{ if(searchResult.resourceType === 'VALUE') { }}
                                               <td class="text-right" width="140"><tags:message code="label.recepient"/> :</td>
                                            {{ }else{ }}
                                               <td class="text-right" width="140"><tags:message code="label.creator"/> :</td>
                                            {{ } }}  
                                            <td>
                                                {{=separatedByComma(searchResult.creator)}}                      
                                            </td>
                                        </tr>  
                                    {{ } }}   
                                    {{ if(!_.isEmpty(searchResult.publisher)){ }}    
                                        <tr class="col_head word_wrap_1">
                                            <td class="text-right" width="140"><tags:message code="label.publisher"/> :</td>                                                            
                                            <td>
                                                {{=separatedByComma(searchResult.publisher)}}                    
                                            </td>
                                        </tr>  
                                    {{ } }}   
                                    {{ if(checkTitleResourceTypeCode(searchResult)){ }}    
                                        {{ if(!_.isEmpty(searchResult.data) && searchResult.resourceType !== 'WIKI'){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.date"/> :</td>
                                                <td>
                                                    {{= separatedByComma(searchResult.data)}}                            
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }}    
                                    {{ if(searchResult.resourceType == 'WIKI' || searchResult.resourceType == 'ENEW' || searchResult.resourceType == 'GOVW'){ }}    
                                        {{if(!_.isEmpty(searchResult.language)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.language"/> :</td>
                                                <td>
                                                    {{= separatedByComma(searchResult.language)}}                                                                                                                            
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }}   
                                    {{ if(searchResult.resourceType == 'ENEW' || searchResult.resourceType == 'GOVW'){ }}    
                                        {{if(!_.isEmpty(searchResult.source)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.source"/> :</td>
                                                <td>
                                                    <a href="{{=searchResult.source[0]}}" style="color:#2a5cea;" target="_blank">{{=searchResult.source[0]}}</a>                                                                                                                          
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ }else if(searchResult.resourceType == 'MANS') {  }} 
                                        {{if(!_.isEmpty(searchResult.source)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.source"/> :</td>
                                                <td>
                                                    {{=searchResult.source[0]}}                                                                                                                        
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }} 
                                    {{ if(searchResult.resourceType == 'VALUE'){ }}    
                                        {{if(!_.isEmpty(searchResult.type)){ }}
                                            <tr class="col_head word_wrap_1">
                                                <td class="text-right" width="140"><tags:message code="label.type"/> :</td>
                                                <td>
                                                    {{= separatedByComma(searchResult.type)}}                                                                                                                            
                                                </td>
                                            </tr>
                                        {{ } }}
                                    {{ } }}                                 
                            </tbody>
                        </table>
                        {{if(!isWebDiscovery(resourceTypeName) && !isExternalResource(searchResult.resourceType)){ }}                            
                            <div class="clearfix m-t-1 postfooter">
                                <ul class="nav nav-pills  font85 marginauto pull-left">
                                    <li class="nav-item">
                                        <span  class="rateit m-r-1" id="rateit5"
                                            data-rateit-min="0" data-rateit-max="5" data-rateit-value="{{=searchResult.ratingScore}}" 
                                            data-rateit-ispreset="true" data-rateit-readonly="true">
                                        </span>
                                    </li>
                                    <li class="nav-item "> 
                                        <span class="m-r-1">
                                            <i class="fa fa-eye"></i> 
                                            <tags:message code="label.views"/>
                                                ({{=searchResult.recordViewCount}})
                                        </span>
                                    </li>
                                    <li class="nav-item ">
                                            <span class="m-r-1"> 
                                                <i class="fa fa-thumbs-up"></i> 
                                                <tags:message code="label.likes"/> 
                                                ({{=searchResult.recordLikeCount}})
                                            </span>
                                    </li>
                                    <li class="nav-item ">
                                        <span class="m-r-1">
                                            <i class="fa fa-share-alt"></i> 
                                            <tags:message code="label.shares"/>
                                            ({{=searchResult.recordNoOfShares}})
                                        </span>
                                    </li>
                                    <li class="nav-item "> 
                                        <span class="m-r-1"> 
                                            <i class="fa fa-files-o"></i> 
                                            <tags:message code="label.reviews"/> 
                                            ({{=searchResult.recordNoOfReview}})
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        {{ } }}
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </article>
</script>
<!-- Mixed mode non multimedia template end ----------------------------------->
<!-- Mixed mode multimedia template start ------------------------------------->
<script type="text/template" id="mixed-resource-result-multimedia-tpl">
    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-xs-12"> 
        <div class="content_imgSearch">
            <div class="column">
                {{= insertTag(searchResult.imagePath,searchResult.resourceType) }}
            </div>            
            {{ if(checkTitleResourceTypeCode(searchResult) && !_.isEmpty(searchResult.source)){ }}
                    <a href="{{=searchResult.source[0]}}" target="_blank">                      
            {{ }else if(isWebDiscovery(resourceTypeName)) {  }}       
                    {{ if(resourceTypeName === "WIKI"){ }} 
                        <a href="{{=searchResult.source[0]}}" target="_blank">
                    {{ } else { }}  
                        <a href="{{=searchResult.recordIdentifier}}" target="_blank">
                    {{ } }}  
            {{ }else { }}       
                    <a href="${context}/search/preview/{{=searchResult.recordIdentifier}}?language=${pageContext.response.locale}" >
            {{ } }}
                        <span class="imagealt">
                            {{=searchResult.nameToView}}
                        </span>
                    </a>            
            {{if(!isWebDiscovery(resourceTypeName) && !isExternalResource(searchResult.resourceType)){ }}                            
                <div class="clearfix m-t-1 text-center">
                    <ul class="nav nav-pills  font85 marginauto">   
                        <li class="nav-item "> 
                            <span class="m-r-1">
                                <i class="fa fa-eye"></i>
                                    ({{=searchResult.recordViewCount}})
                            </span>
                        </li>
                        <li class="nav-item ">
                            <span class="m-r-1"> 
                                <i class="fa fa-thumbs-up"></i> 
                                ({{=searchResult.recordLikeCount}})
                            </span>
                        </li>
                        <li class="nav-item ">
                            <span class="m-r-1">
                                <i class="fa fa-share-alt"></i>
                                ({{=searchResult.recordNoOfShares}})
                            </span>
                        </li>
<%--                        <li class="nav-item "> 
                            <span class="m-r-1"> 
                                <i class="fa fa-files-o"></i>
                                ({{=searchResult.recordNoOfReview}})
                            </span>
                        </li>--%>
                    </ul>
                    <div class="nav-item">
                        <span  class="rateit" id="rateit5"
                            data-rateit-min="0" data-rateit-max="5" data-rateit-value="{{=searchResult.ratingScore}}" 
                            data-rateit-ispreset="true" data-rateit-readonly="true">
                        </span>
                    </div>
                </div>
            {{ } }}
        </div>
    </div>
</script>
<!-- Mixed mode multimedia template end --------------------------------------->
<!-- selected filter label template start ------------------------------------->
<script type="text/template" id ="tpl-selected-filter-label">
    <a href="javascript:;" class="label label-default selectedFilter" data-filter-selected="{{=currentId}}" style="text-transform: capitalize;">
    {{ if(facetName==="Resource Type"){ }}
        {{=currentValue}}
    {{ }else{ }}
        {{=facetName}} : {{=currentValue}}
    {{ } }}
    </a> 
</script>
<!-- selected filter label template end --------------------------------------->
<!-- web discovery service template start ------------------------------------->
<script type="text/template" id="tpl-web-discovery-service">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="masonry-item" id="masonary-item-{{=resourceType}}">
        <div id="header-{{=resourceType}}-resource">            
            <div class="card-header clearfix card-hdr1">
                <img src="${rTIconLocation}/{{=resourceType}}.png" width="32" height="32" alt="{{=resourceType}}" class="card_icon">
                    {{=resourceName}}
                <div class="pull-rights font12">
                    <a href="javascript:;" class="link3 view viewAll" data-view="{{=resourceType}}"> 
                        <tags:message code="label.view.all"/>
                    </a>
                </div>
            </div>
            <div class='card-block clearfix' id='{{=resourceType}}-body'>
                
            </div> 
        </div>
    </div>
</div>
</script>
<!-- web discovery service template end --------------------------------------->
<!-- web discovery service article template start ----------------------------->    
<script type="text/template" id="tpl-web-discovery-service-article">
<article class="post clearfix">
    <div id="{{=resourceType}}-{{=index}}-div">
        <h3>
            {{if(resourceType==='WIKI'){ }}
                <a href="{{=result.source[0]}}" target="_blank">                
            {{ } else{ }}
                <a href="{{=recordIdentifier}}" target="_blank">                
            {{ } }}
                 {{=nameToView}}
                </a>
        </h3>
        <table cellpadding="0" cellspacing="0" class="table table-bordered1 text-left table-sm">
            <tbody id="table-{{=recordIdentifier}}">
                {{if(resourceType!=='WIKI'){}}
                    {{if(!_.isEmpty(result.subject)){ }}                    
                        <tr class="col_head">
                            <td class="text-right" width="140">
                                Subject:
                            </td>
                            <td>  
                                {{=result.subject}}
                            </td>
                        </tr>
                    {{ } }}
                    {{if(!_.isEmpty(result.creator)){ }}                    
                        <tr class="col_head">
                            <td class="text-right" width="140">
                                Creator:
                            </td>
                            <td>  
                                {{=result.creator}}
                            </td>
                        </tr>
                    {{ } }}
                    {{if(!_.isEmpty(result.publisher)){ }}                    
                        <tr class="col_head">
                            <td class="text-right" width="140">
                                Publisher:
                            </td>
                            <td>  
                                {{=result.publisher}}
                            </td>
                        </tr>
                    {{ } }}
                {{ }else{ }}
                    {{if(!_.isEmpty(result.description)){ }}                    
                        <tr class="col_head">
                            <td class="text-right" width="140">
                                Description:
                            </td>
                            <td>  
                                {{=result.description}}
                            </td>
                        </tr>
                    {{ } }}
                    {{if(!_.isEmpty(result.language)){ }}                    
                        <tr class="col_head">
                            <td class="text-right" width="140">
                                Language:
                            </td>
                            <td>  
                                {{=result.language}}
                            </td>
                        </tr>
                    {{ } }}
                {{ } }}
            </tbody>
        </table>
    </div>
</article>
</script>
<!-- web discovery service article template end ------------------------------->    
<!-- span term list template start -------------------------------------------->    
<script type="text/template" id="span-term-list">
    <span>{{=term}}</span>
</script>
<!-- span term list template end ---------------------------------------------->    
<!-- default cross lingual nav bar template start ----------------------------->    
<script type="text/template" id="default-cross-lingual-nav-bar">
    <div id='tabsId'>
        <li class='nav-item cross-lingual-tab-li active'>
            <a onclick='return tabClick(event);' 
               id='translation-tab-{{=languageCode}}' 
               class='nav-link cross-lingual-tab-anchor' 
               data-toggle='tab' 
               data-key='{{=languageCode}}' data-value='{{=searchTerm}}'>
               <b>{{=searchTerm}}</b>
            </a>
        </li>
    </div>
</script>
<!-- default cross lingual nav bar template end ------------------------------->    
<!-- language radio btn template start ---------------------------------------->
<script type="text/template" id="tpl-language-radio-btn">
    {{ _.each(translationMap,function(obj,key){ if(!_.isEmpty(obj) && checkForMoreThanOneTranslations(obj) ) { var arr=[]; }}
        <input type="radio" name="more-translation-radio-btn" id="lang_{{=key}}" value="{{=key}}" data-lang="{{=key}}">
        {{  _.each(obj,function(value){ arr.push(value[0]); }); }}   
        <label for="lang_{{=key}}" style="display: inline;">{{= arr.join('  ') }}</label><br/>
    {{ } }); }}    
</script>
<!-- language radio btn template end ------------------------------------------>
<!-- more-translation btn-group template start -------------------------------->
<script type="text/template" id="tpl-more-translation-btn-group">
    <div class="dec form-inline" id="show_{{=lang}}">
    {{ _.each(translationArr,function(arr,i){ }}   
        <div class="btn-group">          
            <button type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown">Select</button>     
            <div class="dropdown-menu" role="menu">
                {{ _.each(arr,function(word){ }}                         
                    <a class="dropdown-item selected-word dropdown" href="javascript:;" data-index="{{=i}}" data-val="{{=word}}">{{=word}}</a> 
                {{ }); }}
            </div>
        </div>      
        {{=renderNewWordTranslatedSpan(i)}}
    {{ }); }}
    </div>
</script>
<!-- more-translation btn-group template end ---------------------------------->
<!-- Did you mean Modal start ------------------------------------------------->
<div id="search_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.did"/></h4>
            </div>
            <div class="modal-body">
                <p id="termStr"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><tags:message code="label.close"/></button>
            </div>
        </div>
    </div>
</div>
<!-- Did you mean Modal end --------------------------------------------------->
<!-- More Translation Modal start --------------------------------------------->
<div class="modal fade" id="more-translations" tabindex="-1" role="dialog" aria-labelledby="more-translations-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="float: left;">Select Word</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">
                    <p style="text-align: justify;"> 
                        <strong>Disclaimer : </strong>Word Interpretation in cross-lingual search may vary owning to multiple representations of a word in different Indian language families.
                        However, best practices for meaning inheritance has been followed across languages.
                    </p>
                </div>
                <div class="col-lg-12">
                    <div id="more-translation-modal-alert-box" class="alert alert-warning" style="display: none;"></div>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-4" id="language-radio-btn">

                    </div>
                    <div class="col-lg-8">
                        <form class="form-inline">
                            <div class="form-group col-lg-12 translation-options">

                            </div>
                        </form>
                        <div class="clearfix"></div>
                        <div class="p-a-1" id="rendering_word">
                            <span id="new-translation-word"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-success save-new-translation"><tags:message code="label.save.changes"/></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="share.close.button"/></button>                                
            </div>
        </div>
    </div>
</div>
<!-- More Translation Modal start --------------------------------------------->
<!-- templates end here ------------------------------------------------------->