<%--
    Document   : dashboard
    Created on : 26 May, 2016, 5:24:04 PM
    Author     : Priya Bhalerao
    Author     : Madhuri Dhone
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="tags"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message code="label.search.placeholder" var="searchPlaceholder"/> 
<c:set var="context" value="${pageContext.request.contextPath}"/>
<div id="main">                         
    <div class="bg_header_main">
        <div class="bg_header">
            <div class="top_nav_bg bg-inverse clearfix  animated fadeIn custom_menu">
                <nav class="navbar navbar-dark navbar-light  m-l-2 m-r-3 ">
                    <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header"> &#9776; </button>
                    <div class="collapse navbar-toggleable-xs" id="navbar-header">                
                        <div class="form-inline pull-xs-right">
                            <ul class="nav-left">
                                <security:authorize access="!isAuthenticated()">
                                    <li><a onclick="goToLink(this);" href="${context}/auth/login"><span class="localisation-label" data-key="label.login"><tags:message code="label.login"/></span></a></li>
                                    <li><a onclick="goToLink(this);" href="${context}/auth/registration"><span class="localisation-label" data-key="label.register"><tags:message code="label.register"/></span></a></li>
                                        </security:authorize>
                                        <security:authorize access="isAuthenticated()">
                                    <li> 
                                        <a href="${context}/user/profile">
                                            <img src="${exposedProps.userImageBase}/${user.profilePicPath}"
                                                 class="img-small img-circle"
                                                 style="margin-top: -5px"
                                                 height="26"
                                                 alt="${user.firstName} ${user.lastName}"
                                                 onerror="this.src='${context}/images/profile-picture.png'"/> 
                                            ${user.firstName} ${user.lastName}
                                        </a>
                                    </li>
                                    <li class="dropdown responsive_menu"> 
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                            <span class="fa fa-bell" aria-hidden="true"> </span>
                                            <span class="localisation-label" data-key="notif.heading"><tags:message code="notif.heading"/></span>
                                            <span class="notification-count" style="display: none;"></span>
                                            <b class="caret"></b>
                                        </a>
                                        <div class="dropdown-menu notification dropdwnshdw" role="menu" style="display: none; top: 135px; left: 1505.2px;">
                                            <div class="noti_hide" id="notif-items"></div>
                                            <div class="media-footer"> 
                                                <a id="notif-view-all" class=" btn btn-link marginauto" href="${context}/user/notifications"><span class="localisation-label" data-key="label.view.all"><tags:message code="label.view.all"/></span></a> 
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dropdown responsive_menu">
                                        <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="selected-theme">
                                            <span class="fa fa-image" aria-hidden="true"></span> &nbsp;${user.theme.themeName} <b class="caret"></b>
                                        </a>
                                        <div class="dropdown-menu" id="drop_down_menu"></div>
                                    </li>
                                </security:authorize>
                                <li class="responsive_menu">
                                    <ul>
                                        <li><a href="#" class="fontSizeMinus">A<sup>-</sup></a></li>
                                        <li class=""> <a href="#" class="fontReset"> A</a></li>
                                        <li class=""> <a href="#" class="fontSizePlus">A<sup>+</sup> </a></li>  
                                    </ul>     
                                </li>
                                <security:authorize access="isAuthenticated()">
                                    <li class="responsive_menu"><a href="${context}/auth/logout"><span class="fa fa-sign-out"></span> <span class="localisation-label" data-key="label.logout"><tags:message code="label.logout"/></span></a></li>
                                        </security:authorize>
                            </ul>
                        </div>
                    </div>
                </nav>     
            </div>
            <div class="clearfix p-t-3">
                <div class="width_100 shadow_s bg-faded">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-0 "  id="select_text">
                        <div class=" clearfix row">
                            <div class="logo_bgs m-t-6"> <img src="${context}/themes/images/logo/Nvli-logo-home.png"  alt="National Virtual Library of India (NVLI)" class="logoh" />
                                <h1 id="app-name-h1" class="font_by_english"><span class="localisation-label-logo" data-key="nvli.logo"><tags:message code="nvli.logo"/></span></h1>
                                <div class="clear" style="margin-bottom:15px;"></div>
                                <form:form  cssClass="center-block clearfix"  id="searchForm" commandName="searchForm" name="searchForm" role="form">
                                    <div class="col-xl-4 col-lg-8 col-md-10 col-sm-12 col-xl-push-4 col-lg-push-2 col-md-push-1 col-md-push-0">
                                        <div class="input-group custom_search" style="width:100%;">
                                            <%--<span class="ex_check"><input type="checkbox" name="isExactMatch" > Exact Match</span>--%>
                                            <form:input path="searchElement" onKeyPress="return redirecting(event);" placeholder="${searchPlaceholder}" cssClass="form-control p-l-1 langSearch langSearch1" />
                                            <form:input path="searchComplexity"  hidden="true" id="searchComplexity"/>
                                            <form:input path="searchingIn"  hidden="true" id="searchingIn" value="all"  />
                                            <input type="hidden" id="language" name="language" />
                                            <span class="input-group-btn search_button_home">
                                                <button class="btn btn-secondary search_button" type="button" id="submitSearch"><img src="${context}/themes/images/iviewer/search-icon.png" width="18"/></button>
                                            </span>
                                        </div>
                                        <div class="clearfix" style="margin-bottom:15px;"></div>
                                        <div class="center-block homepage_lang_link">
                                            <div class=""> <!--Languages:-->
                                                <span id="langListSpan">
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang=":english" data-id="en">English</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:hindi" data-id="hi">हिंदी</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:bengali" data-id="bn" >বাংলা</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:telugu" data-id="te">తెలుగు</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:marathi" data-id="mr">मराठी</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:tamil" data-id="ta">தமிழ்</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:gujarati" data-id="gu">ગુજરાતી</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:kannada" data-id="kn">ಕನ್ನಡ</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:malayalam" data-id="ml">മലയാളം</a>&nbsp;|&nbsp;
                                                    <a href="javascript:;" class="link1 alangSearch" data-lang="pramukhindic:punjabi" data-id="pa">ਪੰਜਾਬੀ</a>
                                                </span>
                                            </div>
                                            <div style="margin-bottom:10px;" class="clearfix"></div>
                                            <label style="cursor: pointer;" class="webscaledirectory"><input type="checkbox" name="isWebscaleDiscovery" class="font120 font-weight-bold"> <span class="localisation-label" data-key="label.web.scale"><tags:message code="label.web.scale"/></span></label>&nbsp; |&nbsp;
                                            <label style="cursor: pointer;" class="webscaledirectory"><input type="checkbox" name="isCrossLingualSearch" class="font120 font-weight-bold"> <span class="localisation-label" data-key="label.web.cross"><tags:message code="label.web.cross"/></span></label>&nbsp; |&nbsp;
                                            <label style="cursor: pointer;" class="webscaledirectory"><input type="checkbox" name="fullText" class="font120 font-weight-bold"> <span class="localisation-label" data-key="label.fullTextSearch"><tags:message code="label.fullTextSearch"/></span></label>&nbsp; |&nbsp;
                                            <a onclick="goToLink(this);" class="" href="${context}/advsearch/"><span class="localisation-label" data-key="label.advanced.search"><tags:message code="label.advanced.search"/></span></a></div>
                                    </div>
                                </form:form>
                            </div>
                            <div class="col-lg-2" style="position: absolute;bottom: 8%;right: 6%;" id="hoverText"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gray_color">
                <ul class="nvli_homepage_list">
                    <li>
                        <a onclick="goToLink(this);" href='${context}/visualize/owl' target='_blank'>
                            <span class="localisation-label" data-key="label.nvli.ontology">
                                <tags:message code="label.nvli.ontology"/>
                            </span>
                        </a>|
                    </li>
                    <li>
                        <a onclick="goToLink(this);" href='${context}/analytics/' target='_blank'>
                            <span class="localisation-label" data-key="label.nvli.analytics">
                                <tags:message code="label.nvli.analytics"/>
                            </span>
                        </a>|
                    </li>
                    <li>
                        <a onclick="goToLink(this);" href='${context}/cs/udc-tags-crowdsource' target='_blank'>
                            <span class="localisation-label" data-key="label.udcTitle">
                                <tags:message code="label.udcTitle"/>
                            </span>
                        </a>|
                    </li>
                    <li>
                        <a onclick="goToLink(this);" href='${context}/search/show/BLOG' target='_blank'>
                            <span class="localisation-label" data-key="label.nvli.blog">
                                <tags:message code="label.nvli.blog"/>
                            </span></a>|
                    </li>
                    <li>
<!--                        <a onclick="goToLink(this);" href='${exposedProps.wikiBaseURL}' target='_blank'>
                            <span class="lacalisation-label" data-key="label.nvli.wiki">-->
                        <a onclick="goToLink(this);" href='${wikiURL}' target='_blank'>
                            <span class="localisation-label" data-key="label.nvli.wiki">
                                <tags:message code="label.nvli.wiki"/>
                            </span>
                        </a>|
                    </li>
                    <li>
                        <a onclick="goToLink(this);" href='${context}/nac/news' target='_blank'>
                            <span class="localisation-label" data-key="label.nvli.enews">
                                <tags:message code="label.nvli.enews"/>
                            </span>
                        </a>|
                    </li>
                    <li>
                        <a onclick="goToLink(this);" href='${context}/nac/questionanswer' target='_blank'>
                            <span class="localisation-label" data-key="label.nvli.questionanswer">
                                <tags:message code="label.nvli.questionanswer"/>
                            </span>
                        </a>
                    </li>
                </ul>              
            </div>
        </div>
    </div>
    <div class="bg_home">
        <div class="bg_white">
            <div class="clearfix equalheight"> 
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 m-t-2">
                        <div class="col-lg-12 col-md-12 col-sm-12 Oswald" style="text-align: center;color: #686868;">
                            <div style="border: 1px solid #ccc;padding: 12px;"><span class="localisation-home-description" data-key="label.home.description"><tags:message code="label.home.description" arguments="${totalRecordCount}" argumentSeparator=":"/></span></div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 m-t-2 m-b-3 homecategory">
                        <c:forEach items="${resourceTypesAccToTotalRecordCount}" var="resourceType" varStatus="index">
                            <c:forEach items="${resourceType.resourceTypeInfo}" var="resourceInfo">
                                <c:if test="${resourceInfo.language.languageCode eq pageContext.response.locale}">
                                    <c:set var="_selectedLangResourceType" value="${pageContext.response.locale}"/>
                                    <c:set var="resourceInfoDesc" value="${resourceInfo.description}"/>
                                    <c:set var="resourceInfoTitle" value="${resourceInfo.title}"/>
                                </c:if>
                                <c:if test="${_selectedLangResourceType ne 'en' && resourceInfo.language.languageCode eq 'en'}">
                                    <c:set var="defaultLangResourceType" value="en"/>
                                    <c:set var="resourceInfoDesc" value="${resourceInfo.description}"/>
                                    <c:set var="resourceInfoTitle" value="${resourceInfo.title}"/>
                                </c:if>
                            </c:forEach>
                            <c:if test="${not empty resourceInfoTitle && resourceInfoTitle ne null && resourceInfoTitle ne ''}">
                                <c:choose>
                                    <c:when test="${resourceType.resourceTypeCode eq 'BLOG'}">
                                    </c:when>
                                    <c:otherwise>
                                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 m-b-2">
                                            <div class="fix_block hvr-float-shadow"  onclick="return gotToResourceTypePage('${context}/search/show/${resourceType.resourceTypeCode}');">
                                                <div class="m-t-0 card-footer3 heading-concat" style="text-align:center;">
                                                    <span id="title-${resourceType.resourceTypeCode}">${resourceInfoTitle}</span>
                                                </div>
                                                <span class="span_image m-t-2"><img class="hvr-grow" src="${rTIconLocation}/${resourceType.resourceTypeIcon}" alt="${resourceType.resourceType}"></span>
                                                <h4 class="m-t-2 resource-count" data-resource-code="${resourceType.resourceTypeCode}" title="${resourceType.totalRecordCount}">
                                                    <label class="${resourceType.resourceTypeCode}-count-record">${resourceType.totalRecordCount}</label>
                                                    <span class="localisation-label" data-key="label.items"><tags:message code="label.items"/></span>
                                                </h4>
                                                <div class="textsss ellipsis"><p id="description-${resourceType.resourceTypeCode}" class="text-concat" title="${resourceInfoDesc}">${resourceInfoDesc}</p></div>
                                                    <c:set var="_selectedLangResourceType" value=""/>
                                                    <c:set var="defaultLangResourceType" value=""/>
                                                    <c:set var="resourceInfoDesc" value=""/>
                                                    <c:set var="resourceInfoTitle" value=""/>
                                            </div>    
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                        </c:forEach>
                    </div>  
                </div>
            </div>      
        </div>
        <div class="clear22"></div>
    </div>
</div>
<script type="text/template" id="tpl-theme-date-info">
    <time datetime='{{=themeDate}}' class='icon'><p>{{=themeName}}<span>{{=themeDate}}</span></p></time>        
</script>
<script type="text/javascript">
    var _totalRecordCount = '${totalRecordCount}';
    var _selectedLang = '${pageContext.response.locale}';

    function redirectToGenericSearchResult() {
        var genericSearchForm = document.getElementById("searchForm");
        genericSearchForm.setAttribute("action", "${context}/search/across/all");
        $("#language").val(_selectedLang);
        genericSearchForm.setAttribute("method", "get");
        document.getElementById("searchForm").submit();
    }

    function redirecting(event) {
        if (event.keyCode === 13) {
            redirectToGenericSearchResult();
        }
    }

    function gotToResourceTypePage(hrefLink) {
        window.location = hrefLink + "?language=" + _selectedLang;
    }

    function goToLink(e) {
        var url = e.href;
        if (url.indexOf("?language=") === -1) {
            //if not found
            e.href += "?language=" + _selectedLang;
        } else {
            //if found
            url = url.slice(0, -2);
            e.href = url.replace("=", "=" + _selectedLang);
        }
    }

    function changeResourceTypeInfoByLang(lang)
    {
        $.ajax({
            url: '${context}/resourceTypeInfoByLang',
            data: {languageCode: lang, resourceTypeCodeValue: 'all'},
            success: function (jsonObject) {
                _.each(jsonObject, function (object, resourceTypeCode)
                {
                    $("#title-" + resourceTypeCode).text(object.title);
                    $("#description-" + resourceTypeCode).text(object.description);
                    $("#description-" + resourceTypeCode).attr("title", object.description);
                });
            },
            error: function (xhr) {
                console.log(xhr);
            }
        });

    }

    function applyLocalisation(lang)
    {
        $.ajax({
            url: '${context}/getlocalisation',
            data: {languageCode: lang},
            success: function (jsonObject) {
                $(".localisation-label-logo").text(jsonObject[lang]["nvli.logo"]);
                $("#app-name-h1").removeClass().addClass("font_by_" + lang);

                var desc = jsonObject[lang]["label.home.description"];
                $(".localisation-home-description").html(desc.replace('{0}', '<span style="color: #000;">' + _totalRecordCount + '</span>'));

                $(".localisation-label").each(function (i, e) {
                    var text = jsonObject[lang][$(e)[0].attributes[1].value];
//                    if (typeof text === "undefined") {
//                        $(e).text(jsonObject["en"][$(e)[0].attributes[1].value]);
//                    } else {
                        $(e).text(text);
//                    }
                });
                $(".localisation-home-description").html(desc.replace('{0}', '<span style="color: #000;">' + translateNumerals(_totalRecordCount, lang) + '</span>'));
                translateCount(lang);
            },
            error: function (xhr) {
                console.log(xhr);
            }
        });

    }

    function translateCount(lang) {
        $(".resource-count").each(function () {
            var resourceCode = $(this).attr("data-resource-code");
            $("." + resourceCode + "-count-record").text(translateNumerals($(this).attr("title"), lang));
        });
    }
    function translateNumerals(input, target) {
        var systems = {
            ta: 3046, kn: 3302, te: 3174, bn: 2534,
            mr: 2406, ml: 3430, gu: 2790, hi: 2406, pa: 2662
        },
        zero = 48, // char code for Arabic zero
                nine = 57, // char code for Arabic nine
                offset = (systems[target.toLowerCase()] || zero) - zero,
                output = input.toString().split(""),
                i, l = output.length, cc;

        for (i = 0; i < l; i++) {
            cc = output[i].charCodeAt(0);
            if (cc >= zero && cc <= nine) {
                output[i] = String.fromCharCode(cc + offset);
            }
        }
        return output.join("");
    }

    $(document).ready(function () {
        $("#app-name-h1").removeClass().addClass("font_by_" + _selectedLang);
        $(".alangSearch").attr("class", "link1 alangSearch");
        $(".alangSearch[data-id=" + _selectedLang + "]").attr("class", "link1 active alangSearch");
        changeLanguage($(".alangSearch[data-id=" + _selectedLang + "]").attr("data-lang"));
        $('#searchElement').focus();
        $(".alangSearch").click(function () {
            $("#searchElement").val("");
            _selectedLang = $(this).attr("data-id");
            window.history.replaceState(null, null, window.location.pathname + "?language=" + _selectedLang);
            changeResourceTypeInfoByLang(_selectedLang);
            applyLocalisation(_selectedLang);
        });
        $('#searchElement').devbridgeAutocomplete({
            minChars: 3,
            paramName: 'term',
            serviceUrl: '${context}/search/suggestions/all'
        });
        $("#submitSearch").click(function () {
            redirectToGenericSearchResult();
        });
        applyLocalisation(_selectedLang);
    });
</script>
