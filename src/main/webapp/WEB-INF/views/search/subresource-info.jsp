<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script>
    $(document).ready(function () {
        var locatioName = document.getElementById("addr").value;
        //        alert(locatioName);
        showStaticGoogleMapByLocationName(locatioName);
        $(".localisation").click(function () {
            document.getElementById($(this).attr("id")).href = "?language=" + $(this).attr("id");
        });
    });
</script>
<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12 ">
        <div class="card-header"> <img src="${rTIconSearchLocation}/${resourceInfo.resourceType.resourceTypeSearchIcon}" alt="LOGO" height="32" class="pull-left" style="margin-top:-4px;"/>  <span class="txt_highlight m-l-1">${resourceInfo.resourceName}</span></div>
        <div class="clearfix  bg-fadded">
            <!--overflow520-->
            <c:if test="${empty resourceInfo.resourceIcon}">
                <c:set var="resourceicon" value="notavailable" scope="page"/>
            </c:if>
            <c:if test="${not empty resourceInfo.resourceIcon}">
                <c:set var="resourceicon" value="${resourceInfo.resourceIcon}" scope="page"/>
            </c:if>
            <!--<div class="col-lg-2 "><img src={context}/search/get/{resourceicon}/resourceicon" alt="NVLI" width="100" height="100" class="img-fluid" style="margin-top: 15px"/>  </div>-->
            <div class="row marginauto m-t-1 m-b-1 ">
                <img src="${context}/search/get/${resourceicon}/resourceicon" alt="NVLI" width="100" height="75" class="img-fluid"/>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 ">
                <div class="table-responsive">

                    <table class="table text-left">
                        <tbody>

                            <c:if test="${resourceInfo.resourceName ne null && not empty resourceInfo.resourceName && resourceInfo.resourceName ne ' '}">
                                <tr>
                                    <th width="200" class="text-right"><spring:message code="label.subinfo.resourceName"/> </th>
                                    <td>:</td>
                                    <td>${resourceInfo.resourceName}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceTypeObj.resourceType eq 'open repository'}">
                                <c:if test="${resourceInfo.repositoryUrl ne null && not empty resourceInfo.repositoryUrl && resourceInfo.repositoryUrl ne ' '}">
                                    <tr>
                                        <th width="200"  class="text-right"><spring:message code="label.subinfo.repository.url"/></th>
                                        <td>:</td>
                                        <td><a style="color:#2a5cea;" target="_blank" href="${resourceInfo.repositoryUrl}">${resourceInfo.repositoryUrl}</a></td>
                                    </tr>
                                </c:if>
                            </c:if>
                            <c:if test="${resourceInfo.contentTypes ne null && resourceInfo.contentTypes.size() ne 0}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.content.type"/></th>
                                    <td>:</td>
                                    <td>
                                        <c:set var="contentTypes" value="${resourceInfo.contentTypes}"/>
                                        <c:forEach var="ctype" items="${contentTypes}" varStatus="index">
                                            ${fn:trim(ctype.contentType)}<c:if test="${index.count ne resourceInfo.contentTypes.size()}">&#44;</c:if>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.thematicTypes ne null && resourceInfo.thematicTypes.size() ne 0}">                       
                                <tr>
                                    <th  class="text-right"><spring:message code="label.thematic"/></th>
                                    <td>:</td>
                                    <td>
                                        <c:set var="thematicTypes" value="${resourceInfo.thematicTypes}"/>
                                        <c:forEach var="thtype" items="${thematicTypes}" varStatus="index">
                                            ${fn:trim(thtype.thematicType)}<c:if test="${index.count ne resourceInfo.thematicTypes.size()}">&#44;</c:if>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.languages ne null && resourceInfo.languages.size() ne 0}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.language"/></th>
                                    <td>:</td>
                                    <td>
                                        <c:set var="languages" value="${resourceInfo.languages}"/>
                                        <c:forEach var="lang" items="${languages}"  varStatus="index">
                                            ${fn:trim(lang.languageName)}<c:if test="${index.count ne resourceInfo.languages.size()}">&#44;</c:if>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.organization ne null && not empty resourceInfo.organization.name && resourceInfo.organization.name ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.organization"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.organization.name}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.organization ne null && not empty resourceInfo.organization.organisationUrl && resourceInfo.organization.organisationUrl ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.organization.url"/></th>
                                    <td>:</td>
                                    <td>
                                        <c:set var="splittedOrgUrl" value="${fn:split(resourceInfo.organization.organisationUrl,',')}"/>
                                        <c:forEach items="${splittedOrgUrl}" var="orgUrl" varStatus="index">                                            
                                            <a style="color:#2a5cea;" target="_blank" href="${fn:trim(orgUrl)}">${fn:trim(orgUrl)}</a><c:if test="${index.count ne  fn:length(splittedOrgUrl)}">&#44;</c:if>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.description ne null && not empty resourceInfo.description && resourceInfo.description ne ' '}">
                                <tr class="col_head">
                                    <th  class="text-right"><spring:message code="label.subinfo.description"/></th>
                                    <td>:</td>
                                    <td><div class="text-justify">${resourceInfo.description}</div></td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.contactPersonName ne null && not empty resourceInfo.contactPersonName && resourceInfo.contactPersonName ne ' '}">
                                <tr class="col_head">
                                    <th  class="text-right"><spring:message code="label.contact.person"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.contactPersonName}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.designation ne null && not empty resourceInfo.designation && resourceInfo.designation ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.organization.designation"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.designation}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.email ne null && not empty resourceInfo.email &&  resourceInfo.email ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.subinfo.email"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.email}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.phoneNumber ne null && not empty resourceInfo.phoneNumber && resourceInfo.phoneNumber ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.phone"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.phoneNumber}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.organization!=null && resourceInfo.organization.address ne null && not empty resourceInfo.organization.address && resourceInfo.organization.address ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.address"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.organization.address}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.organization!=null && resourceInfo.organization.city ne null && not empty resourceInfo.organization.city && resourceInfo.organization.city ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.city"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.organization.city}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.organization!=null && resourceInfo.organization.stateName ne null  && not empty resourceInfo.organization.stateName && resourceInfo.organization.stateName ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.state.name"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.organization.stateName}</td>
                                </tr>
                            </c:if>
                            <c:if test="${resourceInfo.organization!=null && resourceInfo.organization.country ne null && not empty resourceInfo.organization.country && resourceInfo.organization.country ne ' '}">
                                <tr>
                                    <th  class="text-right"><spring:message code="label.country"/></th>
                                    <td>:</td>
                                    <td>${resourceInfo.organization.country}</td>
                                </tr>
                            </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 ">
        <div class="card-header"> Map </div>
        <div style="margin-bottom:0px;" class="clearfix"></div>
        <div class="md-a-1">
            <c:if test="${resourceInfo.organization!=null}">
                <input type="hidden" value='${resourceInfo.organization.address}' id="addr"/>
            </c:if>
            <iframe id="myMap" class="smap_sm" style="border: 0px none; display: block;" allowfullscreen="" frameborder="0" width="100%" src=""> </iframe>
            <div style="display: none;" id="mapLoading" class="smap"> <img src="${context}/themes/images/mapLoading.gif"> </div>
        </div>
        <div style="margin-bottom:0px;" class="clearfix"></div>
    </div>
    <div class="clear " style="margin-bottom:36px;"></div>    

    <div class="clear22"></div>  
</div>




