<%--
Document   : advanced-search
Created on : Jul 22, 2016, 2:24:54 PM
Author     : Ritesh Malviya
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css" />
<link rel="stylesheet" href="${context}/themes/css/jquery-ui.css">
<div class="m-a-3 container-fluid">
    <form:form commandName="advanceSearchForm" id="advanceSearchForm" name="advanceSearchForm" method='get' action="${context}/advsearch/across/all">
        <div id="main" class="m-t-2" style="z-index: 1;">
            <div class="clear"></div>
            <div class="row clearfix">
                <div class="clear"></div>
                <div class="card bg-greyf6 clearfix">
                    <div class="card-header"><tags:message code="label.advanced.search"/></div>
                    <div class="clearfix" style="margin-bottom:10px;"></div>
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <div class="card clearfix clearfix m-l-2 m-t-2 m-b-2 bg-white">
                            <div class="nested_chk">
                                <div class="nav-tabs p-a-1 font-weight-bold"><tags:message code="label.resource-type"/></div>
                                <div data-toggle="buttons" class="list-type1 p-t-2">
                                    <ol>
                                        <div id="resource-type-container"></div>
                                        <div id="selected-resource-div"></div>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <div class="card clearfix m-a-2  bg-white">
                            <ul id="myTab1" class="nav nav-tabs font-weight-bold" style="margin-top: 0px;">
                                <li class="nav-item">
                                    <a data-toggle="tab" data-target="#t1" class="nav-link active"><tags:message code="search.criteria"/></a>
                                </li>
                                <div class="col-lg-7" style="float: right;">
                                    <div style="width: 100%; display: inline-block;" class=" ">
                                        <div class="search_box1">
                                            <select class="form-control" id="udc-language" onchange="changeUDCLanguage('udc-language');" >
                                                <c:forEach items="${languageList}" var="language">
                                                    <c:if test="${language.id eq 40}">
                                                        <option value="${language.id}" selected="true">${language.languageName}</option>
                                                    </c:if>
                                                    <c:if test="${language.id ne 40}">
                                                        <option value="${language.id}">${language.languageName}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <label for="" class="form-control-label col-lg-4 col-md-4 col-sm-12 text-right text-sm-left" style="float:right;">
                                            <tags:message code="label.select.language"/> :
                                        </label>
                                    </div>
                                </div>
                            </ul>
                            <div class="tab-content">
                                <div id="t1" class="tab-pane nopad active">
                                    <div class="m-a-3 ">
                                        <div id="general-div">

                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-lg-offset-1  ">
                                                <a style="cursor:pointer;color:#0275d8;" class="add-new-row-btn" data-resource-code="general">
                                                    <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>
                                                    <tags:message code="label.add.search.parameter"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="margin-bottom:10px;"></div>
                                        <hr class="style9"/>
                                        <div class="clearfix" style="margin-bottom:10px;"></div>
                                        <div class="form-group row" id="udcTagSearchFilterAdd">
                                            <div class="col-lg-2 col-md-3 col-sm-12 col-lg-offset-1  ">
                                                <a style="cursor:pointer;color:#0275d8;" onclick="addOrRemoveSearchFilterDiv('udcTagSearchFilter','add');" >
                                                    <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>
                                                    <tags:message code="label.Addudc.tags"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="udcTagSearchFilterRemove"  style="display: none;">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-lg-offset-1  ">
                                                <a style="cursor:pointer;color:#0275d8;" onclick="addOrRemoveSearchFilterDiv('udcTagSearchFilter','remove');" >
                                                    <i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i>
                                                    <tags:message code="label.remove.udc.tag"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="margin-bottom:20px;"></div>
                                        <div class="form-group row" id="udcTagSearchFilter" style="display: none;">
                                            <label class="form-control-label col-lg-2 col-md-3 col-sm-12 text-right ">
                                                <tags:message code="label.udc.tags"/>:
                                            </label>
                                            <div class="col-lg-9 col-md-7 col-sm-12">
                                                <div class="input-group" style="float: left;">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#udcTreeViewModel"><tags:message code="label.tree.view.selection"/></button>
                                                    </span>
                                                    <input type="text" placeholder="Enter Description" name="udcTags" id="udcAutocomplete" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="clearfix" style="margin-bottom:30px;"></div>
                                            <div class="col-lg-11 col-md-7 col-sm-12 col-lg-offset-1" id="udc-tag-container"></div>
                                            <form:hidden id="udcNotations" path="udcNotations"/>
                                        </div>
                                        <hr class="style9"/>
                                        <div class="clearfix" style="margin-bottom:10px;"></div>
                                        <div class="form-group row" id="periodSearchFilterAdd">
                                            <div class="col-lg-2 col-md-3 col-sm-12 col-lg-offset-1  ">
                                                <a style="cursor:pointer;color:#0275d8;" onclick="addOrRemoveSearchFilterDiv('periodSearchFilter','add');">
                                                    <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>
                                                    <tags:message code="label.add.period"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="periodSearchFilterRemove" style="display: none;">
                                            <div class="col-lg-2 col-md-3 col-sm-12 col-lg-offset-1  ">
                                                <a style="cursor:pointer;color:#0275d8;" onclick="addOrRemoveSearchFilterDiv('periodSearchFilter','remove');">
                                                    <i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i>
                                                    <tags:message code="label.remove.period"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="margin-bottom:20px;"></div>
                                        <div class="form-group row" id="periodSearchFilter" style="display: none;">
                                            <label for="inputtitle" class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left">
                                                <tags:message code="label.period"/>
                                            </label>
                                            <div class="col-lg-9 col-md-9 col-sm-12 ">
                                                <div class="row">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 ">
                                                        <form:input path="fromDate" cssClass="form-control" id="fromDate" placeholder="Enter start year..." style="text-align: center;"/>
                                                    </div>
                                                    <div class="col-lg-8 col-md-2 col-sm-12" style="margin-top: 10px;">
                                                        <div id="slider-range"></div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-12 ">
                                                        <form:input path="toDate" cssClass="form-control" id="toDate" placeholder="Enter end year..." style="text-align: center;"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="margin-bottom:10px;"></div>
                                        <hr class="style9"/>
                                        <div class="clearfix" style="margin-bottom:20px;"></div>
                                        <div id="resource-div">

                                        </div>
                                    </div>
                                    <div class="card-footer clearfix">
                                        <div class="col-lg-12 col-sm-12" align="center">
                                            <input type="button" value='<tags:message code="label.search"/>' class="btn btn-primary" id="btn_advance_search">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear22"></div>
        </div>
        <div class="clearfix" style="margin-bottom:20px;"></div>
    </form:form>
</div>
<div class="modal fade" id="udcTreeViewModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.udc.tags"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 form-group">
                        <select class="form-control" id="udc-language-model" onchange="changeUDCLanguage('udc-language-model');">
                            <c:forEach items="${languageList}" var="language">
                                <c:if test="${language.id eq 40}">
                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                </c:if>
                                <c:if test="${language.id ne 40}">
                                    <option value="${language.id}">${language.languageName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                </div><br>
                <div id="udcTreeviewCheckable" class=""></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="addTreeviewSelectionList();" data-dismiss="modal"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${context}/themes/js/jquery-ui.js"></script>
<script type="text/javascript" src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src='${context}/themes/js/language/pramukhindic.js'></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>
<script type="text/template" id="tpl-resource-type">
    <li>
        <label id="checkbox-label-{{=resourceTypeCode}}" class="btn resource-type-checkbox" data-checked="false" data-resource-code="{{=resourceTypeCode}}" data-resource-name="{{=resourceTypeName}}">{{=resourceTypeName}}</label>
    </li>
</script>
<script type="text/template" id="tpl-search-row">
    <div class="form-group row" id="{{=resourceCode}}-row-{{=rowCount}}">
        <div class="col-lg-2 col-md-4 col-sm-12 col-lg-offset-1">
            <select class="form-control facet-suggestion-change" id="{{=resourceCode}}-row-{{=rowCount}}-field" data-suggestion-resource-code="{{=resourceCode}}" data-suggestion-box-id="{{=resourceCode}}-row-{{=rowCount}}-text" name="resourceWiseQueryParameterMap[{{=resourceCode}}][{{=rowCount}}].parameterName">
                {{ _.each(facetList, function (facetName) { }}
                    <option value="{{=facetName.name}}">{{=facetName.name}}</option>
                {{ }); }}
            </select>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-12">
            <select class="form-control" name="resourceWiseQueryParameterMap[{{=resourceCode}}][{{=rowCount}}].firstQueryOperator">
                <option value="EQUAL">contains</option>
                <option value="NOT EQUAL">does not contain</option>
            </select>
        </div>
        <span class="col-lg-4 col-md-4 col-sm-12">
            <input type="text" value="" class="{{=resourceCode}}-adv-search-input form-control" id="{{=resourceCode}}-row-{{=rowCount}}-text" name="resourceWiseQueryParameterMap[{{=resourceCode}}][{{=rowCount}}].parameterValue" required>
        </span>
        <div class="col-sm-2 text-muted">
            <select class="form-control" name="resourceWiseQueryParameterMap[{{=resourceCode}}][{{=rowCount}}].secondQueryOperator">
                <option value="AND">AND</option>
                <option value="OR">OR</option>
            </select>
        </div>
        {{ if(rowCount!=0 || resourceCode!='general'){ }}
            <div class="col-sm-1" style="padding: 5px;">
                <a style="cursor:pointer;" class="remove-row-btn" data-row-id="{{=resourceCode}}-row-{{=rowCount}}" data-resource-code="{{=resourceCode}}">
                    <i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i>
                </a>
            </div>
        {{ } }}
    </div>
</script>
<script type="text/template" id="tpl-resource-panel">
    <div class="panel card m-t-1 resources_type" id="{{=resourceCode}}-panel">
        <div href="#"+"{{=resourceCode}}-div" data-toggle="collapse" class="card-header-s accordion-toggle" aria-expanded="true">
            {{=resourceName}}
        </div>
        {{ if(filter1!==""){ }}
            <div class="clearfix" style="margin-bottom:10px;"></div>
        {{ } }}
        {{=filter1}}
        {{ if(isFacetAvailable){ }}
            {{ if(filter1!==""){ }}
                <div class="clearfix" style="margin-bottom:10px;"></div>
                <hr class="style9"/>
                <div class="clearfix" style="margin-bottom:10px;"></div>
            {{ } }}
            <div class="panel-collapse collapse in p-t-1" id="{{=resourceCode}}-div">

            </div>
            <div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-lg-offset-1">
                    <a style="cursor:pointer;color:#0275d8;" class="add-new-row-btn" data-resource-code="{{=resourceCode}}">
                        <i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>
                        Add Search Parameter
                    </a>
                </div>
            </div>
        {{ } }}
        {{ if(filter2!=="" && (filter1!=="" || isFacetAvailable)){ }}
            <div class="clearfix" style="margin-bottom:10px;"></div>
            <hr class="style9"/>
            <div class="clearfix" style="margin-bottom:10px;"></div>
        {{ } }}
        {{=filter2}}
    </div>
</script>
<script type="text/template" id="tpl-filter-row">
    <div class="form-group row">
        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left">
            {{=filterName}}
        </label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <div class="selectBox select-box-{{=resourceCode}}-{{=filterCode}}" data-expanded="false">
                <select class="form-control">
                    <option>-- Select {{=filterName}} --</option>
                </select>
                <div class="overSelect"></div>
            </div>
            <div id="checkboxes-{{=resourceCode}}-{{=filterCode}}" class="overflow200-a checkbox_search">
                {{=filterOption}}
            </div>
        </div>
        <div class="clearfix" style="margin-bottom:20px;"></div>
        <div class="col-lg-11 col-md-7 col-sm-12 col-lg-offset-1" id="container-{{=resourceCode}}-{{=filterCode}}"></div>
    </div>
</script>
<script type="text/template" id="tpl-filter-option">
    <label for="f-{{=resourceCode}}-{{=filterCode}}-{{=optionCount}}" class="checkboxes">
        <input type="checkbox" name="{{=filterCode}}" data-name="{{=optionDisplayName}}" value="{{=optionValue}}" class="chkboxfacet checkbox-click-{{=resourceCode}}-{{=filterCode}}" id="f-{{=resourceCode}}-{{=filterCode}}-{{=optionCount}}"/>
        {{=optionDisplayName}}
    </label>
</script>
<script>

    var nodeIds;
    var notation;
    var notationText;
    var udcJson = {};
    var languageId = '40';
    var _resourcePanel = {};
    var _resourceFacet = {};
    var _selectedResources={};
    var _defaultResourceCode = "general";

    $(document).ready(function () {
        fetchAndRenderResourceType('${pageContext.response.locale}');
        intializePeriodSlider();
        $("#btn_advance_search").click(function (e) {
            e.preventDefault();
            var isOK = true;            
            $(".general-adv-search-input").each(function (){
                if ($.trim($(this).val()) === "") {                   
                    isOK= false;
                    $(this).css('border-color','red');
                } else {
                    $(this).css('border-color','#ccc');
                }
            });
            if(isOK){
                var tags = '';
                $.each(udcJson, function (keyName, value) {
                    if (tags === '') {
                        tags = keyName;
                    } else {
                        tags = tags + '&|&' + keyName;
                    }
                });

                if(tags==='') {
                    $('#udcNotations').remove();
                } else {
                    document.getElementById("udcNotations").value = tags;
                }

                if($('#fromDate').val()==='') {
                    $('#fromDate').remove();
                }

                if($('#toDate').val()==='') {
                    $('#toDate').remove();
                }

                $('#advanceSearchForm').submit();
            }else{
                alert("Please fill mandatory field.");
            }            
            return isOK;
        });

        _resourcePanel[_defaultResourceCode] = parseInt('0');
        getResourceWiseFacetsAndFilters(_defaultResourceCode, "", false);
        $(".add-new-row-btn").unbind().bind("click", function (e) {
            var resourceCode = e.currentTarget.getAttribute("data-resource-code");
            _resourcePanel[resourceCode] ++;
            rowCreation(resourceCode, _resourcePanel[resourceCode]);
        });
        
        setLanguageAdvancedSearch("udc-language");
    });

    function showCheckboxes(resourceCode,filterName) {
        $(".select-box-"+resourceCode+"-"+filterName).attr("data-expanded", "true");
        var checkboxes = document.getElementById("checkboxes-"+resourceCode+"-"+filterName);
        checkboxes.style.display = "block";
    }

    function hideCheckboxes(resourceCode,filterName){
        $(".select-box-"+resourceCode+"-"+filterName).attr("data-expanded", "false");
        var checkboxes = document.getElementById("checkboxes-"+resourceCode+"-"+filterName);
        checkboxes.style.display = "none";
    }

    function getResourceWiseFacetsAndFilters(resourceCode, resourceName, isCallPanelCreation) {
        if (typeof _resourceFacet[resourceCode] === "undefined") {
            $.ajax({
                url: '${context}/advsearch/getResourceWiseFacetsAndFilters/' + resourceCode,
                type: 'get',
                success: function (resourceWiseFacetsAndFiltersMap) {
                    _resourceFacet[resourceCode]=resourceWiseFacetsAndFiltersMap;
                    if(!$.isEmptyObject(resourceWiseFacetsAndFiltersMap)){
                        createResource(resourceCode, resourceName, isCallPanelCreation);
                    }
                },
                error: function () {
                }
            });
        } else {
            if(!$.isEmptyObject(_resourceFacet[resourceCode])){
                createResource(resourceCode, resourceName, isCallPanelCreation);
            }
        }
    }

    function createResource(resourceCode, resourceName, isCallPanelCreation) {
        var isFacetAvailable=false;
        var filter1="";
        var filter2="";
        var filtersName={};
        $.each(_resourceFacet[resourceCode], function (keyName, value) {
            if(keyName==="facetList") {
                isFacetAvailable=true;
            } else {
                var template = _.template($("#tpl-filter-option").html());
                var template_el="";
                var optionCount=0;
                _.each(value, function (optionObject) {
                    opts = {
                        "resourceCode": resourceCode,
                        "filterCode": keyName,
                        "optionCount": optionCount,
                        "optionDisplayName": optionObject.name,
                        "optionValue": optionObject.identifier
                    };
                    template_el += template(opts);
                    optionCount++;
                });
                filtersName[keyName]=keyName;
                if(keyName==="subResourceIdentifiers"){
                    filter1=filterCreation(resourceCode, "Institution", keyName, template_el);
                }else if(keyName==="museumDates"){
                    filter2=filterCreation(resourceCode, "Date:", keyName, template_el);
                }
            }
        });
        if(isCallPanelCreation){
            panelCreation(resourceCode, resourceName, filter1, filter2, isFacetAvailable, filtersName);
        } else {
            rowCreation(resourceCode, _resourcePanel[resourceCode]);
        }
    }

    function filterCreation(resourceCode, filterName, filterCode, filterOption) {
        var template = _.template($("#tpl-filter-row").html());
        opts = {
            "resourceCode": resourceCode,
            "filterName": filterName,
            "filterCode": filterCode,
            "filterOption": filterOption
        };
        var template_el = template(opts);
        return template_el;
    }

    function panelCreation(resourceCode, resourceName, filter1, filter2, isFacetAvailable, filtersName) {
        _resourcePanel[resourceCode] = parseInt('0');

        var template = _.template($("#tpl-resource-panel").html());
        opts = {
            "resourceCode": resourceCode,
            "resourceName": resourceName,
            "filter1": filter1,
            "filter2": filter2,
            "isFacetAvailable": isFacetAvailable
        };
        var template_el = template(opts);
        $("#resource-div").append(template_el);

        if(isFacetAvailable) {
            rowCreation(resourceCode, _resourcePanel[resourceCode]);
            $(".add-new-row-btn").unbind().bind("click", function (e) {
                var resourceCode = e.currentTarget.getAttribute("data-resource-code");
                _resourcePanel[resourceCode] ++;
                rowCreation(resourceCode, _resourcePanel[resourceCode]);
            });
        }

        $.each(filtersName, function (keyName, value) {
            bindEventsForFilter(resourceCode,keyName);
        });
    }

    function bindEventsForFilter(resourceCode,filterName) {
        $(".checkbox-click-"+resourceCode+"-"+filterName).unbind().bind("click", function (e) {
            e.stopPropagation();
            if (this.checked) {
                var displayName=$(this).attr("data-name");
                $("#container-"+resourceCode+"-"+filterName).append('<button type="button" id="'+this.id+'-display" class="btn btn-remove-'+resourceCode+'-'+filterName+'" style="margin:0 0 10px 10px; color:#0275d8;"><span class="fa fa-remove"></span> ' +displayName+ '</button>');
                $(".btn-remove-"+resourceCode+"-"+filterName).unbind().bind("click", function (e) {
                    $("#" + this.id.replace("-display", "")).attr('checked', false);
                    $("#" + this.id).remove();
                });
            } else {
                $("#" + this.id + "-display").remove();
            }
        });

        $(".select-box-"+resourceCode+"-"+filterName).on("click", function (e) {
            e.stopPropagation();
            $(this).attr("data-expanded") === "true"?hideCheckboxes(resourceCode,filterName):showCheckboxes(resourceCode,filterName);
        });
        
        $(".select-box-"+resourceCode+"-"+filterName).on("focusin", function () {
            showCheckboxes(resourceCode,filterName);
        });

        $(".select-box-"+resourceCode+"-"+filterName).on("focusout", function () {
            hideCheckboxes(resourceCode,filterName);
        });

        $(window).click(function(e) {
            if(_.size($(".select-box-"+resourceCode+"-"+filterName))){
                if($(".select-box-"+resourceCode+"-"+filterName).attr("data-expanded")==='true' && (typeof e.target.childNodes[1]==='undefined' || typeof e.target.childNodes[1].className==='undefined' || e.target.childNodes[1].className.indexOf('checkbox-click-'+resourceCode+'-'+filterName)===-1)) {
                    hideCheckboxes(resourceCode,filterName);
                }
            }
        });
    }

    function rowCreation(resourceCode, rowCount) {
        var row_template = _.template($("#tpl-search-row").html());
        opts = {
            "facetList": _resourceFacet[resourceCode]["facetList"],
            "resourceCode": resourceCode,
            "rowCount": rowCount
        };
        var row_template_el = row_template(opts);

        $("#" + resourceCode + "-div").append(row_template_el);

        var field=document.getElementById(resourceCode+"-row-"+rowCount+"-field").value;
        var suggestionBoxId=resourceCode+"-row-"+rowCount+"-text";
        setSuggestionAutocomplete(resourceCode,field,suggestionBoxId);

        $(".remove-row-btn").unbind().bind("click", function (e) {
            var resourceCode = e.currentTarget.getAttribute("data-resource-code");
            $("#" + e.currentTarget.getAttribute("data-row-id")).remove();
            if ($("#" + resourceCode + "-div").html().trim().length === 0) {
                $("#" + resourceCode + "-panel").remove();
                $("#checkbox-label-"+resourceCode).attr("data-checked", "false");
                $("#checkbox-label-"+resourceCode).removeClass("active");
                delete _selectedResources[resourceCode];
                delete _resourcePanel[resourceCode];
                $("#" + resourceCode + "-checkbox").remove();
            }
        });

        $(".facet-suggestion-change").unbind().bind("change", function (e) {
            var suggestionBoxId = e.currentTarget.getAttribute("data-suggestion-box-id");
            var resourceCode = e.currentTarget.getAttribute("data-suggestion-resource-code");
            var field = e.currentTarget.value;
            setSuggestionAutocomplete(resourceCode,field,suggestionBoxId);
        });
    }

    function setSuggestionAutocomplete(resourceCode,field,suggestionBoxId){
        $('#' + suggestionBoxId).devbridgeAutocomplete({
            paramName: 'tagPrefix',
            minChars: 3,
            serviceUrl: '${context}/advsearch/getResourceWiseFieldSuggestions/' + resourceCode + '/' + field + '/' +JSON.stringify(_.map(_selectedResources, function(num, key){return key;}))
        });
    }

    function intializePeriodSlider()
    {
        var currentYear=new Date().getFullYear();
        $("#slider-range").slider({
            range: true,
            min: 1500,
            max: currentYear,
            values: [1500, currentYear],
            slide: function (event, ui) {
                $("#fromDate").val(ui.values[ 0 ]);
                $("#toDate").val(ui.values[ 1 ]);
            }
        });
        $('#fromDate').val('');
        $('#toDate').val('');
    }

    function addOrRemoveSearchFilterDiv(divId,action)
    {
        if(action==='add') {
            $('#'+divId+'Add').hide();
            $('#'+divId).show();
            $('#'+divId+'Remove').show();
        } else if(action==='remove') {
            $('#'+divId+'Add').show();
            $('#'+divId).hide();
            $('#'+divId+'Remove').hide();
        }

        if(divId==='udcTagSearchFilter' && action==='remove') {
            udcJson={};
            document.getElementById('udc-tag-container').innerHTML = '';
        } else if(divId==='periodSearchFilter' && action==='remove') {
            intializePeriodSlider();
        }
    }

    function addTags(tagsJson, div, tagType) {
        document.getElementById(div).innerHTML = '';
        if (!checkJsonIsEmpty(tagsJson)) {
            var divHtml = '';
            $.each(tagsJson, function (keyName, value) {
                if (tagType === 'udc') {
                    divHtml = divHtml + '<button type="button" class="btn" style="margin:0 0 10px 10px; color:#0275d8;" onclick="deleteUDCTag(\'' + keyName + '\',\'' + div + '\',\'' + tagType + '\');"> <span class="fa fa-remove"></span>  ' + keyName + '   ' + value + ' </button>';
                }
            });
            document.getElementById(div).innerHTML = divHtml;
        }
    }

    function deleteUDCTag(key, div, tagType) {
        if (tagType === 'udc') {
            delete udcJson[key];
            addTags(udcJson, div, tagType);
            var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[key]);
            $('#udcTreeviewCheckable').treeview('uncheckNode', [nodeId, {silent: true}]);
        }
    }

    function checkJsonIsEmpty(tagsJson) {
        return (Object.keys(tagsJson).length === 0 && JSON.stringify(tagsJson) === JSON.stringify({}));
    }

    function changeUDCLanguage(languageDiv) {
        languageId = document.getElementById(languageDiv).value;
        document.getElementById('udc-language').value = languageId;
        document.getElementById('udc-language-model').value = languageId;
        if (!checkJsonIsEmpty(udcJson)) {
            var jsonObject = {};
            var udcTagNotations = '';
            $.each(udcJson, function (keyName, value) {
                if (udcTagNotations === '') {
                    udcTagNotations = keyName;
                } else {
                    udcTagNotations = udcTagNotations + "&|&" + keyName;
                }
            });
            $.ajax({
                url: '${context}/cs/getUDCTagDescription',
                dataType: 'json',
                type: "post",
                data: {
                    'udcNotations': udcTagNotations,
                    'languageId': languageId
                },
                success: function (response) {
                    for (var key in response) {
                        jsonObject[key] = response[key];
                    }
                    if (!checkJsonIsEmpty(jsonObject)) {
                        udcJson = jsonObject;
                        addTags(udcJson, 'udc-tag-container', 'udc');
                    }
                },
                error: function () {
                }
            });
        }
        suggestions();
        getUDCTreeviewData();
    }

    function suggestions() {
        $('#udcAutocomplete').devbridgeAutocomplete({
            paramName: 'tagPrefix',
            minChars: 3,
            serviceUrl: '${context}/cs/getUDCTagsSuggestions?languageId=' + languageId,
            onSelect: function (suggestion) {
                if (!checkJsonIsEmpty(udcJson) && (typeof udcJson[suggestion.data] !== "undefined")) {
                    alert('Tag already exist!');
                } else {
                    udcJson[suggestion.data] = suggestion.value;
                    addTags(udcJson, 'udc-tag-container', 'udc');
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[suggestion.data]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                }
                document.getElementById('udcAutocomplete').value = '';
            }
        });
    }

    function getUDCTreeviewData() {
        var tags = '';
        var isDisable = 'false';
        $.ajax({
            url: '${context}/cs/getUDCTreeviewData',
            data: {languageId: languageId, tags: tags, isDisable: isDisable},
            type: 'POST',
            success: function (response) {
                nodeIds = response["nodeIds"];
                $('#udcTreeviewCheckable').treeview({
                    data: response["udcTreeViewData"],
                    showIcon: false,
                    showCheckbox: true,
                    onNodeChecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        notationText = node.text.split("]")[1].trim();
                        udcJson[notation] = notationText;
                    },
                    onNodeUnchecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        delete udcJson[notation];
                    }
                });
                $.each(udcJson, function (keyName, value) {
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[keyName]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                });
            },
            error: function () {
            }
        });
    }

    function addTreeviewSelectionList() {
        addTags(udcJson, 'udc-tag-container', 'udc');
    }
    
    function fetchAndRenderResourceType(lang)
    {
        $.ajax({
            url: '${context}/resourceTypeInfoByLang',
            data: {languageCode: lang,resourceTypeCodeValue: 'all'},
            success: function (jsonObject) {
                _.each(_.sortBy(jsonObject, function(item){return item.title.toLowerCase();}),function(object,resourceTypeCode) {
                    var resource_type_template = _.template($("#tpl-resource-type").html());
                    opts = {
                        "resourceTypeCode": object.resourceTypeCode,
                        "resourceTypeName": object.title
                    };
                    var resource_type_el = resource_type_template(opts);
                    $("#resource-type-container").append(resource_type_el);
                });
                
                $(".resource-type-checkbox").unbind().bind("click", function (e) {
                    resourceTypeCheckboxClick(e);
                });

            },
            error: function (xhr) {
                console.log(xhr);
            }
        });
    }
    
    function resourceTypeCheckboxClick(e) {
        var resourceCode = e.currentTarget.getAttribute("data-resource-code");
        var isChecked = e.currentTarget.getAttribute("data-checked");
        if (isChecked==='false') {
            $(e.currentTarget).attr("data-checked", "true");
            var resourceName = e.currentTarget.getAttribute("data-resource-name");
            _selectedResources[resourceCode]=resourceCode;
            $("#selected-resource-div").append('<input type="hidden" name="searchInResources" value="'+resourceCode+'" id="'+resourceCode+'-checkbox"/>');
            getResourceWiseFacetsAndFilters(resourceCode, resourceName, true);
        } else {
            $(e.currentTarget).attr("data-checked", "false");
            delete _selectedResources[resourceCode];
            delete _resourcePanel[resourceCode];
            $("#" + resourceCode + "-panel").remove();
            $("#" + resourceCode + "-checkbox").remove();
        }

        _.each($("#general-div").children(), function(child){
            var field=document.getElementById(child.id+"-field").value;
            var suggestionBoxId=child.id+"-text";
            setSuggestionAutocomplete("general",field,suggestionBoxId);
        });
    }
</script>