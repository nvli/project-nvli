<%-- 
    Document   : image-tags
    Created on : Sep 20, 2016, 2:03:12 PM
    Author     : Priya More<mpriya@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<noscript>Java Script is off. Please enable Java Script.</noscript>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>

<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src="${context}/themes/js/language/pramukhindic.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>

<!--image tag css and js start-->
<link href="${context}/themes/css/image_tagging/jquery.tag.css" rel="stylesheet"/>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js'></script>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.7/jquery-ui.min.js'></script>
<script type="text/javascript" src="${context}/themes/js/image_tagging/jquery.tag.js"></script>
<link href="${context}/themes/css/image_tagging/jquery-ui.custom.css" rel="stylesheet"/>
<!--image tag css and js end-->

<!-- Horizontal breadcrumbs -->
<ol class="breadcrumb">
    <li><a href="${context}/home"><i class="fa fa-home" aria-hidden="true"> </i><tags:message code="label.home"/> </a></li>
</ol>
<div style="margin-bottom:0px;" class="clearfix"></div>
<div class="clearfix equalheight cardd">
    <div class="card-header"> 
        <img src="${rTIconLocation}/${resourceType.resourceTypeIcon}" alt="National Virtual Library of India (NVLI)" height="32" class="pull-left" style="margin-top:-4px;"/>
        <span class="txt_highlight m-l-1">
            <c:choose>
                <c:when test="${not empty record.title && record.title ne null}">
                    ${record.title[0]}
                </c:when>
                <c:otherwise>
                    ${record.recordIdentifier}
                </c:otherwise>
            </c:choose>
        </span> 
    </div>
    <div class="clearfix"> 
        <div class="m-t-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-lg-offset-2">	
                        <img id="loading" /><div  id="img1"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-lg-offset-2" style="text-align: center; padding-top: 15px;">
                    <button type="button" class="btn btn-primary" id="prev"><tags:message code="label.previous"/></button>
                    <button type="button" class="btn btn-primary" id="next"><tags:message code="label.next"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
<c:set var="location" value="${baseLocation}${record.location}/${record.originalFileName}"/>
<c:set var="backslash" value="\\\\"/>
<c:set var="forwardslash" value="/"/>
<c:set var="path" value="${fn:replace(location,backslash, forwardslash)}" />
<script type="text/javascript">
    var recordIdentifier='${recordIdentifier}';
    var selectedFilePath='${path}';
    var currentPageNo=0;
    var totalPages='${record.paths.size()}';
    $(document).ready(function () {
        $("#img1").tag();

        /*Preview of previous page*/
        $("#prev").click(function() {
            if(totalPages!=1){
                $("#next").removeClass('disabled');
            }
            if(currentPageNo==1){
                $("#prev").addClass('disabled');
            }
            if(currentPageNo<=0){
                alert('Previous page is not available.');
            }else{
                renderJpeg((currentPageNo-1),totalPages);
            }
        });
        /*Preview of next page*/
        $("#next").click(function() {
            if(totalPages!=1){
                $("#prev").removeClass('disabled');
            }
            if(currentPageNo==(totalPages-2)){
                $("#next").addClass('disabled');
            }
            if(currentPageNo>=(totalPages-1)){
                alert('Next page is not available.');
            } else{
                renderJpeg((currentPageNo+1),totalPages);
            }
        });
        if(totalPages==1){
            $("#prev").addClass('disabled');
            $("#next").addClass('disabled');
        }
        
        if(currentPageNo==0){
            $("#prev").addClass('disabled');
        }
    });
    function renderJpeg(no,numPages){
        var recordsPathString= '${record.paths}';
        var recordsPathArr=recordsPathString.split(",");
        //        alert("nn--"+no+"nm=="+numPages+"--"+recordsPathArr[no]);
        selectedFilePath=recordsPathArr[no];
        currentPageNo=no;
        totalPages=numPages;
        src=selectedFilePath;
        splittedImageName=selectedFilePath.split("/");
        imageName=splittedImageName[splittedImageName.length-1];
        if(currentPageNo==0){
            src=src.substring(1, src.length);
        }
        if(currentPageNo=='${record.paths.size()-1}'){
            src=src.substring(0, src.length-1);
            imageName=imageName.substring(0, imageName.length-1);
        }
        
        $('.jTagArea').css('background-image','url('+src+')');

        /*start get tag from db*/
        var urlTagGet='${context}'+'/search/get/image-tags/'+imageName+"/"+recordIdentifier;
        $.ajax(
        {
            type:"POST",
            contentType:"application/json; charset=utf-8",
            dataType: "json",
            url:urlTagGet,
            success:function(data){
                if (!$.trim(data)){
                    $('.inserHere').html('');
                    $('.jTagTag').remove();
                }else{
                    $('.inserHere').html('');
                    $('.jTagTag').remove();
                    for (i = 0; i < data.length; i++) {
                        tag = $('.jTagOverlay').append('<div class="jTagTag" id="tag'+data[i].tagId+'"style="width:'+data[i].width+'px;height:'+data[i].height+'px;top:'+data[i].top_pos+'px;left:'+data[i].left+'px;"><div style="width:100%;height:100%"><div class="jTagDeleteTag" id="tag'+data[i].tagId+'"></div><span>'+data[i].label+'</span></div></div>');
                        if ($.trim(data)){
                            $('.inserHere').append("<tr><td rel='tag"+data[i].tagId+"' class='jlable'>"+data[i].label+"</td><td rel='tag"+data[i].tagId+"' class='jdelete'></td></tr>");
                        }
                    }
                }
                if (data===null||data.length===0){
                    $('.inserHere').append("<tr class='noTags'><td class='jlable' colspan='2'>No Tags</td></tr>");
                }
            },
            error:function(){
                alert(urlTagGet);
            }
        });		
        /*end get tag from db*/
    }
    var recordIdentifier = "${recordIdentifier}";
    var src=selectedFilePath;
    var imageName="${record.originalFileName}";
</script>
