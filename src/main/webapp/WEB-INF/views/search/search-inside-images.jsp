<%-- 
    Document   : search-inside-image
    Created on : 5 Mar, 2018, 11:36:21 AM
    Author     : Vrushali
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${context}/themes/css/bootstrap.css">
        <link rel="stylesheet" href="${context}/themes/css/style.css" >
        <link rel="stylesheet" href="${context}/themes/css/default.css" >
        <link rel="stylesheet" href="${context}/themes/css/font-awesome.css" >
        <link rel="stylesheet" href="${context}/themes/css/full_text_crowdsource_main.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript">
            var con = '${context}';
            var currentPage = parseInt('${currentPage}');
            var totalPages = parseInt('${totalPages}');
            var recordName = '${recordName}';
            var jsonName = '${recordName}-' + currentPage + '.json';
            var recordLocation = '${recordLocation}';
            var originalName = recordLocation.split('/').reverse()[0];
            var imageName = '${imageName}';
            var searchTerm = '${searchTerm}';
            function displayNextPage()
            {
                if (currentPage === totalPages) {
                    alert("You are on last page");
                }
                else {
                    currentPage++;
                    var term=document.getElementById("image-search").value;
                    if(term ===''){
                       term="";
                    }
                    window.location = "${context}/search/search-inside-image/" + recordName + "/" + imageName  + "/" + currentPage + "/" + totalPages + "?recordLocation=" + recordLocation + "&recordTitle=${recordTitle}"+ "&searchTerm="+term;
                }
            }

            function displayPrevPage()
            {
                if (currentPage === 1) {
                    alert("You are on first page");
                }
                else {
                    currentPage--;
                    var term=document.getElementById("image-search").value;
                    if(term===''){
                        term="";
                    }
                    
                    window.location = "${context}/search/search-inside-image/" + recordName + "/" + imageName + "/" + currentPage + "/" + totalPages + "?recordLocation=" + recordLocation + "&recordTitle=${recordTitle}"+"&searchTerm="+term;

                }
            }
        </script>
        <script type="text/javascript">
            var imagePath = '${cdn}/' + '${recordLocation}/' + originalName + "-" + currentPage + ".jpg";
            var jsonPath = '${cdn}/' + '${recordLocation}/' + 'json/' + originalName + "-" + currentPage + ".json";
            ;
        </script>
        <script src="${context}/themes/js/jquery.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/search-inside/all-base.js"></script>
        <title>Search Inside Images</title>
    </head>
    <body class="m-a-0">
        <div class="container-fliud">
        <div id="lheader" class="valign-middle">
            <div class="lheader_logo col-lg-3 col-md-3 col-sm-12 col-xs-12"> <a href="${context}/home"> <img src="${context}/themes/images/logo/NVLI-LOGO-header.png"  alt="National Virtual Library of India (NVLI)" class="img-fluid m-a-0" > </a></div>
            <h2 class="record_title text-center col-lg-6 col-md-6 col-sm-12  col-xs-12 m-t-1">${recordTitle}</h2>
            <div class="col-lg-3 col-md-3 col-sm-12  col-xs-12 m-t-1">
                <div class="pull-right m-r-1"> <span id="page-status" class="font14 font-weight-bold">${currentPage}/${totalPages}</span>
                    <button id="button-prev"  class="btn btn-sm  btn-primary" onClick="displayPrevPage();"><i class="fa fa-2x  fa-caret-left" title="Previous"></i> Prev</button>
                    <button id="button-next"  class="btn btn-sm  btn-primary" onClick="displayNextPage();">Next <i class="fa fa-2x fa-caret-right " title="Next"></i> </button>
                </div>
            </div>
        </div>
        <div class="main_container">
            <div class="card toolbar">
                <div class="">
                    <div class="m-y-2 text-center col-md-4 col-md-offset-4">
                        <input type="text" class="form-control" id="image-search" value="${searchTerm}" placeholder="Search inside the image..." autocomplete='off'>
                    </div>
                    <div class="clearfix"></div>
                    <canvas id="c"></canvas>
                    <script type="text/javascript" src="${context}/themes/js/search-inside/image-search.js"></script>
                    <script type="text/javascript" src="${context}/themes/js/search-inside/fabric.min.js"></script>
                    <script type="text/javascript">
                        $(document).ready(setTimeout(function () {
                            $('#image-search').trigger('keyup');
                        }, 1000));
                    </script>
                    <div class="clearfix"></div>
                </div>
                    <div class="clearfix"></div>
            </div>
        </div>
        </div>
        <script type="text/javascript">
            document.getElementById("page-status").innerHTML = currentPage + "/" + totalPages;
        </script>          
    </body>
</html>
