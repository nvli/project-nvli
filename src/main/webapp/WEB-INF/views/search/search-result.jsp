<%--
    Author     : Priya Bhalerao
    Author     : Madhuri Dhone
    Author     : Vivek Bugale
    Author     : Ritesh Malviya
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@include file="../../static/components/pagination/pagination.html" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message code="label.search.placeholder" var="searchPlaceholder" />
<%@include file="search-result-templates.jsp" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src='${context}/themes/js/language/pramukhindic.js'></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage.js"></script>
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css" />
<script type="text/javascript" src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript">
    var user;
    var _crossLingualObj = {};
    var _resourceTypeMap = JSON.parse('${jsonOfResourceType}');
    var _defaultDataLimit = 10;
    var _dataLimit = parseInt('${dataLimit}');
    var _resultTotalCount;
    var _pageNo = parseInt('${pageNo}');
    var _viewAllResourceType = '${viewAllResourceType}';
    var _sortingMode = '${mode}';
    var _isFirstTime = true;
    var _urlParamObj;
    var _requestCount;
    var _translationMap = {};
    var _resourceLocalisationMap =${resourceLocalisationMap};
    $(document).ready(function () {
        window.onpopstate = function () {
            window.location.reload();
        };
        var urlStr = window.location.pathname + "";
        if (urlStr.indexOf("advsearch") === -1) {
            _urlParamObj = QueryStringToJSON();
            $("#grid").ready(function () {
                //to check filter type
                navigateReqToRelatedFun(true);
            });
        } else {
            var advanceSearchForm = JSON.parse('${advanceSearchForm}');
            _urlParamObj = {};
            if (advanceSearchForm.searchInResources !== null) {
                _urlParamObj.allResourcesFilters = advanceSearchForm.searchInResources;
            }
            if (advanceSearchForm.subResourceIdentifiers !== null) {
                _.each(advanceSearchForm.subResourceIdentifiers, function (subResource, i) {
                    $("#searchElement").parent().append('<input id="sub' + i + '" type="hidden" name="subResourceIdentifiers[' + i + ']" value="' + subResource + '">');
                });
            }
            $("#grid").ready(function () {
                //to check filter type
                $(".res-specific-view").hide();
                $(".cross-lingual-view").show();
                $(".all-view").show();
                $(".all-filters").empty();
                $(".all-filters").append(_.template($("#all-resources-ul-tpl").html()));
                $('select[name^="mode"] option[value="' + _sortingMode + '"]').attr("selected", "selected");
                $("#mixed-mode-div").hide();
                $("#trove-mode-div").show();
                renderTroveModeTemplateForAdvSearch();
            });
        }

        if ('${isCrossLingualSearch}' === "on") {
            $("#isCrossLingualSearch").prop('checked', true);
            $(".select_language").css("display", "flex");
            $('#selectLang').val($("#language").val());
            if ($("#searchElement").val() !== "") {
                $(".select_language").css("display", "flex");
                fetchCrossLingualTranslation();
            } else {
                $(".select_language").css("display", "none");
            }
        }

        if ('${isWebscaleDiscovery}' === "on") {
            $("#isWebscaleDiscovery").prop('checked', true);
        }
        
        $("#fullText").prop('checked', ${searchForm.fullText});        

        changeLanguage(getLanguage('${pageContext.response.locale}'));

        $('#searchElement').devbridgeAutocomplete({
            paramName: 'term',
            minChars: 3,
            serviceUrl: '${context}/search/suggestions/' + $("#searchingIn").val()
        });

        $("#submitSearch").click(function () {
            fetchResultOnSearch();
        });

        _crossLingualObj = {};
        if (localStorage.getItem('savedPhrase') !== null) {
            savePhrase(localStorage.getItem('savedPhrase'));
            localStorage.removeItem('savedPhrase');
        }

        //change mode event
        $("#mode").unbind().bind("change", function () {
            $(".no-result-found").hide();
            _sortingMode = $("#mode").val();
            if (_sortingMode === "nvli_rl_5") {
                $("#mixed-mode-div").show();
                $("#trove-mode-div").hide();
                _pageNo = 1;
                _dataLimit = _defaultDataLimit;
                renderResourceMixedResultContainer(false, "all", false);
            } else {
                $("#mixed-mode-div").hide();
                $("#trove-mode-div").show();
                renderTroveModeTemplate(resourceTypeFilterCheck(false), false, false);
            }
        });
        bindSaveSearch();
        // Invoking check for saved search
        checkIfSearchSaved();
        //for add active class to language tab
        setTimeout(function () {
            var languageVal = "${pageContext.response.locale}";
            $(".nav-link").each(function () {
                var dataKeyVal = $(this).attr('data-key');
                if (languageVal === dataKeyVal) {
                    $(this).addClass("active");
                }
            });
            langChange();
        }, 500);

        fetchAdvertisement();

        //bind multimedia category click
        $(".multimedia-cat").click(function (e) {
            e.preventDefault();
            loadMultimediaResults(e);
        });
    });

    function bindSaveSearch() {
        //to save search
        $("#saveSearch").unbind().bind("click", function () {
            var parent = document.getElementById('save-search-block');
            var child = document.getElementById('saveSearch');
            var inputBox = document.createElement('input');
            inputBox.type = "text";
            inputBox.name = "savedPhrase";
            inputBox.id = "phrase";
            inputBox.placeholder = "Give a name";
            inputBox.style.height = '28px';
            inputBox.style.marginRight = '10px';
            inputBox.className = "form-control pull-left";
            var button = document.createElement('button');
            button.textContent = "Save";
            button.type = "button";
            button.id = "phrasebutton";
            button.className = "btn btn-secondary btn-sm pull-left";
            button.addEventListener('click', getSavedPhrase, false);
            var container = document.getElementById('savedPhrase');
            container.style.display = "flex";
            container.appendChild(inputBox);
            container.appendChild(button);
            parent.removeChild(child);
        });
    }

    function loadCrossLingualNavigationBar(languageCode, searchTerm) {
        $("#myTab1").empty();
        var navigation_bar_template = _.template($("#default-cross-lingual-nav-bar").html());
        var opts = {
            languageCode: languageCode,
            searchTerm: searchTerm.indexOf(":") !== -1 ? searchTerm.split(":").reverse()[0] : searchTerm
        };
        var navigation_bar_el = navigation_bar_template(opts);
        $("#myTab1").append(navigation_bar_el);
    }

    async function fetchCrossLingualTranslation() {
        var lang = $("#language").val();
        var keyword = $("#searchElement").val();
        loadCrossLingualNavigationBar(lang, keyword);
        var languageKeyValue = '${languageKeyValueJson}';
        if (languageKeyValue !== '') {
            var jsonobj = JSON.parse(languageKeyValue);
            var arr = [];
            _.each(jsonobj, function (value, key) {
                _crossLingualObj[key] = value;
                _.each(value, function (val) {
                    _.each(val.translations, function (v, k) {
                        if (!_.isEmpty(v)) {
                            arr.push(v[0]);
                        }
                    });
                });
                if (!_.isEmpty(arr)) {
                    $("#tabsId").append("<li class='nav-item cross-lingual-tab-li'><a onclick='return tabClick(event);' id='translation-tab-" + key + "' class='nav-link cross-lingual-tab-anchor' data-target=" + '#' + key + " data-toggle='tab' data-key='" + key + "' data-value='" + arr.join(' ') + "'><b>" + arr.join(' ') + "</b></a></li>");
                }
                arr = [];
            });
            $("#languageKeyValueJson").val(JSON.stringify(_crossLingualObj));
        } else {
            $.ajax({
                url: __NVLI.context + "/search/crosslingualMap",
                data: {languageId: lang, searchElement: keyword},
                success: function (data) {
                    if (!_.isEmpty(data)) {
                        var arr = [];
                        var tempTranslationArr = [];
                        _.each(data, function (value, key) {
                            tempTranslationArr = [];
                            _crossLingualObj[key] = value;
                            _.each(value, function (val) {
                                _.each(val.translations, function (v, k) {
                                    if (!_.isEmpty(v)) {
                                        tempTranslationArr.push(v);
                                        arr.push(v[0]);
                                    }
                                });
                            });
                            if (!_.isEmpty(arr)) {
                                $("#tabsId").append("<li class='nav-item cross-lingual-tab-li'><a onclick='return tabClick(event);' id='translation-tab-" + key + "' class='nav-link cross-lingual-tab-anchor'  data-toggle='tab' data-key='" + key + "' data-value='" + arr.join(' ') + "'><b>" + arr.join(' ') + "</b></a></li>");
                            }
                            arr = [];
                            _translationMap[key] = tempTranslationArr;
                        });
                        $("#myTab1").append('<li class="nav-item more-translations-li" style="margin-left: 10px;"> <a class="nav-link p-l-1 p-r-1" data-toggle="modal" data-target="#more-translations"><b><tags:message code="label.more.translations"/></b></a> </li>');
                        enableMoreTranslationTab();
                        $("#languageKeyValueJson").val(JSON.stringify(_crossLingualObj));
                    }
                },
                error: function (xhr) {
                    console.log("error in fetch Cross-Lingual Translation::", xhr);
                    $("#alert-box").removeClass().addClass('alert alert-danger').html('Translation service is not availbale').fadeIn().fadeOut(5000);
                }
            });
        }
    }

    function enableMoreTranslationTab() {
        if (!_.isEmpty(_translationMap)) {
            var status = false;
            _.each(_translationMap, function (object) {
                _.each(object, function (el) {
                    if (el.length > 1) {
                        status = true;
                    }
                });
            });
            if (status) {
                $(".more-translations-li").show();
            } else {
                $(".more-translations-li").hide();
            }
        }
    }

    function checkForMoreThanOneTranslations(object) {
        var status;
        _.each(object, function (value, key) {
            status = (value.length > 1);
        });
        return status;
    }

    $('#more-translations').on('show.bs.modal', function () {
        $("#language-radio-btn").empty();
        var language_radio_btn_temp = _.template($("#tpl-language-radio-btn").html());
        var opts = {
            translationMap: _translationMap
        };
        var language_radio_btn_el = language_radio_btn_temp(opts);
        $("#language-radio-btn").append(language_radio_btn_el);
        bindMoreTranslationRadioBtnClick();
    });

    $('#more-translations').on("hide.bs.modal", function () {
        $("#new-translation-word").empty();
        $(".translation-options").empty();
        $(".dropdown-menu").css("display", "none");
        $("#more-translation-modal-alert-box").hide();
    });

    function bindMoreTranslationRadioBtnClick() {
        $('input[type=radio][name=more-translation-radio-btn]').change(function (e) {
            $(".translation-options").empty();
            $("#new-translation-word").empty();
            var translation_options_temp = _.template($("#tpl-more-translation-btn-group").html());
            var opts = {
                lang: e.currentTarget.getAttribute("data-lang"),
                translationArr: _translationMap[e.currentTarget.getAttribute("data-lang")]
            };
            var translation_options_el = translation_options_temp(opts);
            $(".translation-options").append(translation_options_el);
            bindSelectedWord();
        });
    }

    function bindSelectedWord() {
        $(".selected-word").unbind().bind("click", function (e) {
            var index = e.currentTarget.getAttribute("data-index");
            $("#word-" + index).text(e.currentTarget.getAttribute("data-val"));
        });
    }

    $(".save-new-translation").unbind().bind("click", function () {
        if ($("#new-translation-word").text().length > 0) {
            replaceTranslationTab($('input[name=more-translation-radio-btn]:checked').val(), $("#new-translation-word").text());
            $("#more-translations").modal('hide');
        } else {
            //alert
            $("#more-translation-modal-alert-box").show();
            $("#more-translation-modal-alert-box").text("Please select word!");
        }
    });

    function replaceTranslationTab(languageCode, newWord) {
        $("#translation-tab-" + languageCode).attr('data-value', newWord);
        $("#translation-tab-" + languageCode).text(newWord);
    }

    function redirecting(event) {
        if (event.keyCode === 13) {
            fetchResultOnSearch();
        }
    }

    function fetchResultOnSearch() {
        _pageNo = 1;
        $(".no-result-found").hide();
        checkEntireNVLICheckbox();
        if ($("#mode").val() !== "Sorted") {
            $("#mixed-mode-results").empty();
            $(".mixed-mode-multimedia").empty();
            $("#pagination-footer").empty();
        }
        if ($('#isCrossLingualSearch').is(":checked")) {
            //cross-lingual search
            if ($("#searchElement").val() !== "") {
                $(".select_language").css("display", "flex");
                fetchCrossLingualTranslation();
            } else {
                $(".select_language").css("display", "none");
            }
        } else {
            $(".select_language").css("display", "none");
        }
        navigateReqToRelatedFun(false);
        _crossLingualObj = {};
        _translationMap = {};
        $('#selectLang').val($("#language").val());
        // Invoking check for saved search
        checkIfSearchSaved();
    }

    function navigateReqToRelatedFun(isURLObjCheckReq) {
        //populateSimilarTermLists();
        if (_viewAllResourceType === "all") {
            $(".res-specific-view").hide();
            $(".cross-lingual-view").show();
            $(".all-view").show();
            $(".all-filters").empty();
            $(".all-filters").append(_.template($("#all-resources-ul-tpl").html()));
            $('#mode-div').show();
            $('select[name^="mode"] option[value="' + _sortingMode + '"]').attr("selected", "selected");
            if (_sortingMode === "nvli_rl_5") {
                $("#mixed-mode-div").show();
                $("#trove-mode-div").hide();
                renderResourceMixedResultContainer(true, "all", _.has(_urlParamObj, "allResourcesFilters"));
            } else {
                $("#mixed-mode-div").hide();
                $("#trove-mode-div").show();
                renderTroveModeTemplate(_resourceTypeMap, true, _.has(_urlParamObj, "allResourcesFilters"));
            }

            //for web-discovery ul
            if ($("#isWebscaleDiscovery").is(':checked')) {
                var serializedSearchForm = prepareQueryString().slice(0, -1);
                $("#grid2").empty();
                $('#accordion1').append(_.template($('#web-scale-discovery-ul-tpl').html()));
                renderWebDiscoveryServices(serializedSearchForm, "Google-Books");
                renderWebDiscoveryServices(serializedSearchForm, "Open-Library");
                renderWebDiscoveryServices(serializedSearchForm, "WIKI");
                renderWebDiscoveryServices(serializedSearchForm, "OCLC");
                $("#otherResourceType").show();
            }
        } else {
            $('#mode-div').hide();
            $(".res-specific-view").show();
            $(".all-view").hide();
            if (_pageNo === 0) {
                _pageNo = 1;
            }

            if (_dataLimit === 0) {
                _dataLimit = _defaultDataLimit;
            }

            if (isWebDiscovery(_viewAllResourceType)) {
                viewAllWebDiscoveryResults(true);
            } else {
                $(".res-heading-name").text(_resourceLocalisationMap[_viewAllResourceType].title);
                $(".cross-lingual-view").show();
                $("#mixed-mode-div").show();
                renderResourceMixedResultContainer(false, _viewAllResourceType, isURLObjCheckReq);
            }
        }
    }

    function checkEntireNVLICheckbox() {
        if ($('#chkboxEntireNVLI').is(':checked')) {
            window.location = __NVLI.context + "/search/across/all?searchElement=" + $('#searchElement').val() + "&searchingIn=all&searchComplexity=BASIC&language=" + '${pageContext.response.locale}';
        }
    }

    //render web-discovery li and trove mode
    function renderWebDiscoveryServices(serializedSearchForm, resourceType) {
        $.ajax({
            url: __NVLI.context + "/search/fetch-web-discovery/" + resourceType + "/1/3?facetRequired=" + isFacetRequired(),
            type: 'POST',
            data: serializedSearchForm,
            dataType: 'json',
            beforeSend: function () {
                $(".web-discovery-loader").show();
            },
            complete: function () {
                $(".web-discovery-loader").hide();
            }
        })
                .done(function (response) {
                    var grid = document.querySelector("#grid2");
                    salvattore.registerGrid(grid);
                    //web-dicovery-li creation
                    if (response.resultSize !== 0) {
                        _.each(response.results, function (value, key) {
                            var temp_web_discovery_li = _.template($("#tpl-web-discovery-li").html());
                            var opts = {
                                result: resourceNameToView(key),
                                resourceType: resourceType
                            };
                            var el_web_discovery_li = temp_web_discovery_li(opts);
                            $("#web-discovery-ul").append(el_web_discovery_li);
                        });                        
                        //loading web-discovery-results
                        var temp_web_discovery_result = _.template($('#tpl-web-discovery-service').html());
                        var opts = {
                            resourceType: resourceType,
                            resourceName: resourceNameToView(resourceType)
                        };
                        var el_web_discovery_result = temp_web_discovery_result(opts);
                        var div = document.createElement("div");
                        salvattore.appendElements(grid, [div]);
                        div.outerHTML = el_web_discovery_result;
                        //append results to body
                        _.each(response.results, function (value, key) {
                            _.each(value.listOfResult, function (result, i) {
                                if (i < 3) {
                                    var temp_web_discovery_article = _.template($("#tpl-web-discovery-service-article").html());
                                    var opts = {
                                        nameToView: result.nameToView,
                                        recordIdentifier: result.recordIdentifier,
                                        resourceType: resourceType,
                                        index: ++i,
                                        result: result
                                    };
                                    var el_web_discovery_article = temp_web_discovery_article(opts);
                                    $('#' + resourceType + '-body').append(el_web_discovery_article);
                                    //bind web-dicovery-li click
                                    bindViewAll();
                                }
                            });
                        });
                    }
                })
                .fail(function (xhr) {
                    console.log("error in calling " + resourceType + " webservice ::", xhr);
                });
    }
    //depend upon resourceType it will return resource name to view
    function resourceNameToView(resourceName) {
        switch (resourceName) {
            case 'WIKI':
                return "Wikipedia";
            case 'Google-Books':
                return "Google Books";
            case 'Open-Library':
                return "Open Library";
            case 'OCLC':
                return "OCLC";
        }
    }

    //On Language Tab click
    function tabClick(e) {
        var dataKey = e.currentTarget.getAttribute('data-key');
        var dataValue = e.currentTarget.getAttribute('data-value');
        var preValue = $("#searchElement").val();
        $("#searchElement").val(dataValue);
        navigateReqToRelatedFun(false);
        $("#searchElement").val(preValue);
        $(".nav-link").removeClass("active");
        $(this).addClass("active");
    }

    //check saved search phrases
    function checkIfSearchSaved() {
        $.ajax({
            url: __NVLI.context + "/search/check/saved",
            type: 'POST',
            data: encodeURI(window.location.href),
            dataType: 'json',
            success: function (jsonObj) {
                if (jsonObj.isSaved === "notLoggedIn") {
                    user = false;
                } else {
                    user = true;
                }
                // If saved then show different div with saved search name and
                // edit button.
                if (jsonObj.isSaved === "saved") {
                    $('#save-search-block').empty();
                    $('#save-search-block').append("<span style='font-size:14px;font-weight:normal;margin-top:-4px;'>This Search is saved as <b> " + jsonObj.savedPhrase + "</b> | </span>");
                } else {
                    $('#save-search-block').empty();
                    $('#save-search-block').append('<button id="saveSearch" class="pull-right btn btn-success btn-sm"><b><span class="fa fa-save"></span> Save Search</b></button><div id="savedPhrase" class="pull-right"></div>');
                    bindSaveSearch();
                }
            },
            error: function (jqXHR) {
                console.log("error in saved search:: ", jqXHR);
            }
        });
    }

    //get saved search phrases
    function getSavedPhrase() {
        var savedPhrase = document.getElementById("phrase").value;
        if (savedPhrase.length === 0 || savedPhrase.length > 30) {
            showToastMessage("Your phrase should be within 1 to 30 characters", "warning");
            return false;
        }
        if (user === false) {
            localStorage.setItem("savedPhrase", savedPhrase);
        }
        savePhrase(savedPhrase);
    }

    //save phrase
    function savePhrase(savedPhrase) {
        $('#save-search-block').empty();
        $('#save-search-block').append("<span style='font-size:14px;font-weight:normal;margin-top:-4px;'>This Search is saved as <b> " + savedPhrase + "</b> | </span>");
        $.ajax({
            url: window.__NVLI.context + "/search/save/saved/link",
            data: {link: encodeURI(window.location.href), savedPhrase: savedPhrase},
            success: function (jsonObj) {
                if (jsonObj.isSaved === undefined) {
                    document.write(jsonObj);
                } else if (jsonObj.isSaved === "saved") {
                    showToastMessage('Your search is saved as "' + jsonObj.savedPhrase + '".You can acces it from dashboard of your profile."', "success");
                } else if (jsonObj.isSaved === "notLoggedIn")
                    showToastMessage("You must log in to save this search", "warning");
                else {
                    document.getElementById("savedPhrase").innerHTML = '';
                    showToastMessage('Your search is saved as "' + jsonObj.savedPhrase + '.You can acces it from dashboard of your profile."', "info");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ajax Error While Saving search", jqXHR, textStatus, errorThrown);
            }
        });
    }

    function checkTabPressentOrNot() {
        var isAvailable = false;
        $.each($('.cross-lingual-tab-anchor'), function () {
            if ($('#selectLang option:selected').val() === $(this).attr('data-key')) {
                isAvailable = true;
            }
        });
        return isAvailable;
    }

    //lang change event
    function langChange() {
        $('#selectLang').change(function () {
            if (!checkTabPressentOrNot()) {
                var srcLang = document.getElementById("language").value;
                var destLang = $(this).val();
                var keyword = document.getElementById("searchElement").value;
                $.ajax({
                    url: __NVLI.context + "/search/crosslingualMapSingle",
                    data: {
                        srcLang: srcLang,
                        destLang: destLang,
                        searchElement: keyword
                    },
                    success: function (data) {
                        var tempTranslationArr = [];
                        if (!_.isEmpty(data)) {
                            var arr = [];
                            _.each(data, function (value, key) {
                                tempTranslationArr = [];
                                _crossLingualObj[key] = value;
                                _.each(value, function (val) {
                                    _.each(val.translations, function (v, k) {
                                        if (!_.isEmpty(v)) {
                                            tempTranslationArr.push(v);
                                            arr.push(v[0]);
                                        }
                                    });
                                });
                                if (!_.isEmpty(arr)) {
                                    $("#tabsId").append("<li class='nav-item cross-lingual-tab-li'><a onclick='return tabClick(event);' id='translation-tab-" + key + "' class='nav-link cross-lingual-tab-anchor' data-toggle='tab' data-key='" + key + "' data-value='" + arr.join(' ') + "'><b>" + arr.join(' ') + "</b></a></li>");
                                }
                                arr = [];
                                _translationMap[key] = tempTranslationArr;
                                enableMoreTranslationTab();
                            });
                        } else {
                            $("#alert-box").removeClass().addClass('alert alert-danger').html('Translation is not availbale').fadeIn().fadeOut(4000);
                        }
                        $("#languageKeyValueJson").val(JSON.stringify(_crossLingualObj));
                    },
                    error: function (xhr) {
                        $("#alert-box").removeClass().addClass('alert alert-danger').html('Translation service is not availbale').fadeIn().fadeOut(4000);
                        console.log("error in language change ::", xhr);
                    }
                });
                $("#languageKeyValueJson").val(JSON.stringify(_crossLingualObj));
            } else {
                $("#alert-box").removeClass().addClass('alert alert-warning').html('Translation for ' + $('#selectLang option:selected').text() + ' is already availbale').fadeIn().fadeOut(4000);
            }
        });
    }

    function renderNewWordTranslatedSpan(spanIndex) {
        $("#new-translation-word").append('<span id=word-' + spanIndex + '></span>&nbsp;');
    }

    //get image source
    function getImageSrc(imagePath, imageURL, resourceTypeName) {
        if (imagePath === null) {
            return imageURL;
        }
        if (resourceTypeName === "Google-Books" || resourceTypeName === "Open-Library") {
            if (imagePath) {
                return imagePath;
            } else {
                return imageURL;
            }
        }
        var extension = imagePath.substr(imagePath.toLowerCase().lastIndexOf('.') + 1);
        switch (extension.toLowerCase()) {
            case 'png':
            case 'gif':
            case 'jpg':
            case 'jpeg':
            case 'tif':
            case 'tiff':
                return imagePath;
            default:
                return imageURL;
        }
    }

    //resource filter
    function renderResourceFilter(resourceTypeCode, parentIndex, resourceTypeName, resultCount) {
        var all_resources_li_temp = _.template($("#all-resources-li-tpl").html());
        var rchecked = '';
        if (_.has(_urlParamObj, "allResourcesFilters") && _.contains(_urlParamObj.allResourcesFilters, resourceTypeCode)) {
            rchecked = 'checked';
        }
        var opts = {
            resultCount: resultCount,
            resourceTypeCode: resourceTypeCode,
            parentIndex: parentIndex,
            rchecked: rchecked,
            resourceTypeName: resourceTypeName
        };
        var all_resources_li_el = all_resources_li_temp(opts);
        $("#all-resources-ul").append(all_resources_li_el);
    }

    //filter
    function renderFilter(facetMap, filters, headerLabel, filterName, diffKey) {
        var filter_temp = _.template($('#filters-tpl').html());
        var filterOpts = {
            headerLabel: headerLabel,
            filterName: filterName
        };
        var filter_el = filter_temp(filterOpts);
        $('.all-filters').append(filter_el);
        var parentIndex = 0;
        _.each(facetMap, function (facetValueList, facetKey) {
            if (!_.isEmpty(facetValueList)) {
                var fchecked = '';
                var filtersValue;
                var facetCategory = facetKey;
                if (filterName === "general-filter") {
                    facetKey = facetKey.split('_')[1];
                    facetCategory = convertToTitleCase(facetKey);
                }
                if (filters !== null && _.has(filters, facetKey)) {
                    fchecked = 'checked';
                    filtersValue = filters[facetKey];
                }

                var filter_ul_temp = _.template($('#filters-ul-tpl').html());
                var opts = {
                    facetValueList: facetValueList,
                    facetCategory: facetCategory,
                    parentIndex: ++parentIndex,
                    filters: filtersValue,
                    fchecked: fchecked,
                    filterName: filterName,
                    diffKey: diffKey
                };
                var filter_ul_el = filter_ul_temp(opts);
                $('.' + filterName).append(filter_ul_el);
            }
        });
    }

    //convert to title case
    function convertToTitleCase(name) {
        return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
    }

    function renderResourceResultContainerByPagination(pageNo) {
        $('html,body').scrollTop(0);
        _pageNo = pageNo;
        renderResourceMixedResultContainer(false, "all", false);
    }

    function renderViewAllContainerByPagination(pageNo) {
        $("#mixed-mode-results").empty();
        $(".mixed-mode-multimedia").empty();
        $('html,body').scrollTop(0);
        _pageNo = pageNo;
        renderResourceMixedResultContainer(false, _viewAllResourceType, false);
    }

    function renderViewAllWebDiscoveryByPagination(pageNo) {
        $("#mixed-mode-results").empty();
        $(".mixed-mode-multimedia").empty();
        $('html,body').scrollTop(0);
        _pageNo = pageNo;
        if (_viewAllResourceType === "WIKI") {
            viewAllWebDiscoveryResults(true);
        } else {
            viewAllWebDiscoveryResults(false);
        }
    }

    function bindViewAll() {
        $(".viewAll").on("click", function (e) {
            var viewResourceTypeCode = e.currentTarget.getAttribute("data-view");
            var URL = __NVLI.context + "/search/across/resource/" + viewResourceTypeCode;
            if (viewResourceTypeCode !== "Google-Books" && viewResourceTypeCode !== "Open-Library") {
                URL += "/1/" + _defaultDataLimit;
            }
            URL = URL + "?searchElement=" + $('#searchElement').val() + "&searchingIn=" + viewResourceTypeCode + "&searchComplexity=BASIC&language=" + '${pageContext.response.locale}';
            if($('#fullText').is(':checked')){
                URL+="&fullText=on";
            }
            this.href = URL;            
        });
    }

    function resourceTypeFilterCheck(isAllowedEmptyReturn) {
        var resourceTypefilterMap = {};
        var unCheckedResourceTypefilterMap = {};
        $('.resource-type-filter-chk').each(function () {
            if (this.checked) {
                resourceTypefilterMap[this.value] = _resourceTypeMap[this.value];
            } else {
                unCheckedResourceTypefilterMap[this.value] = _resourceTypeMap[this.value];
            }
        });
        if (!isAllowedEmptyReturn && _.isEmpty(resourceTypefilterMap)) {
            resourceTypefilterMap = unCheckedResourceTypefilterMap;
        }
        return resourceTypefilterMap;
    }

    function renderTroveModeTemplateForAdvSearch() {
        var searchFormData = location.search.slice(1);
        $("#grid").empty();
        var grid = document.querySelector("#grid");
        salvattore.registerGrid(grid);
        var parentIndex = 0;
        $.ajax({
            url: "${context}/advsearch/fetch-resource-result/1/3",
            type: 'POST',
            data: searchFormData,
            beforeSend: function () {
                $(".no-result-found").hide();
                $(".all-resources-ul-loader").show();
            },
            complete: function () {
                $(".all-resources-ul-loader").hide();
            }
        })
                .done(function (searchResult) {
                    $(".did_you_mean").hide();
                    $("#total-result-display").text(searchResult.resultSize);
                    $("#searchElement").val(searchResult.searchElement);
                    $("#searchComplexity").val("BASIC");
                    if (searchResult.resultSize !== null && searchResult.resultSize !== 0) {
                        _.each(searchResult.allResources, function (count, resourceTypeCode) {
                            renderResourceFilter(resourceTypeCode, ++parentIndex, _resourceLocalisationMap[resourceTypeCode].title, count);
                            var resource_result_temp = _.template($("#resource-result-tpl").html());
                            var opts = {
                                resourceTypeName: _resourceLocalisationMap[resourceTypeCode].title,
                                resourceTypeCode: resourceTypeCode,
                                resourceTypeIcon: _resourceTypeMap[resourceTypeCode].resourceTypeIcon,
                                searchResultResponse: searchResult.results[resourceTypeCode],
                                fullText : $('#fullText').is(':checked')
                            };
                            var resource_result_el = resource_result_temp(opts);
                            var div = document.createElement("div");
                            salvattore.appendElements(grid, [div]);
                            div.outerHTML = resource_result_el;
                        });
                        // call rateit to display rating
                        $(".rateit").rateit();
                        renderSelectedResourceFilterAll();
                        bindViewAll();
                        bindResourceFilterCheckboxClick("advance");
                    } else {
                        $(".did_you_mean").show();
                        populateSimilarTermLists();
                        $(".no-result-found").show();
                    }
                })
                .fail(function (xhr) {
                    console.log("error in render trove mode for adv search::", xhr);
                });
        changeURL("/search/across/all");
    }

    function isFacetRequired() {
        return _viewAllResourceType === "all" ? false : true;
    }

    function renderTroveModeTemplate(resourceTypeMap, isRenderResourceFilter, isURLObjCheckReq) {
        var searchFormData = prepareQueryString().slice(0, -1);
        var searchTerm = $("#searchElement").val();
        $("#grid").empty();
        var grid = document.querySelector("#grid");
        salvattore.registerGrid(grid);
        _requestCount = 0;
        _resultTotalCount = 0;
        var parentIndex = 0;
        $(".all-resources-ul-loader").show();
        _.each(resourceTypeMap, function (resourceType) {
            _requestCount++;
            $.ajax({
                url: "${context}/search/fetch-resource-result/" + resourceType.resourceTypeCode + "/" + $("#mode").val() + "/1/3?facetRequired=" + isFacetRequired(),
                type: 'POST',
                data: searchFormData
            })
                    .done(function (searchResult) {
                        if (!_.isEmpty(searchResult)) {
                            _resultTotalCount += searchResult.resultSize;
                            _requestCount--;
                            $("#total-result-display").text(_resultTotalCount);
                            if (isRenderResourceFilter) {
                                renderResourceFilter(resourceType.resourceTypeCode, ++parentIndex, _resourceLocalisationMap[resourceType.resourceTypeCode].title, searchResult.resultSize);
                            }
                            allCallCompletion(isURLObjCheckReq);
                            //render non-multimedia template
                            if (searchResult.resultSize !== null && searchResult.resultSize !== 0 && (!isURLObjCheckReq || _.contains(_urlParamObj.allResourcesFilters, resourceType.resourceTypeCode))) {
                                if (!searchResult.multiMedia) {
                                    var resource_result_temp = _.template($("#resource-result-tpl").html());
                                    var opts = {
                                        resourceTypeName: _resourceLocalisationMap[resourceType.resourceTypeCode].title,
                                        resourceTypeCode: resourceType.resourceTypeCode,
                                        resourceTypeIcon: resourceType.resourceTypeIcon,
                                        searchResultResponse: searchResult.results[resourceType.resourceTypeCode],
                                        fullText : $('#fullText').is(':checked'),
                                        searchElement: searchTerm
                                    };
                                    var resource_result_el = resource_result_temp(opts);
                                    var div = document.createElement("div");
                                    salvattore.appendElements(grid, [div]);
                                    div.outerHTML = resource_result_el;
                                    bindViewAll();
                                    loadEllipses();
                                } else {
                                    //render multimedia template
                                    var resource_result_multimedia_temp = _.template($("#resource-result-multimedia-tpl").html());
                                    var opts = {
                                        resourceTypeName: _resourceLocalisationMap[resourceType.resourceTypeCode].title,
                                        resourceTypeCode: resourceType.resourceTypeCode,
                                        resourceTypeIcon: resourceType.resourceTypeIcon,
                                        searchResultResponse: searchResult.results[resourceType.resourceTypeCode]
                                    };
                                    var resource_result_multimedia_el = resource_result_multimedia_temp(opts);
                                    var div = document.createElement("div");
                                    salvattore.appendElements(grid, [div]);
                                    div.outerHTML = resource_result_multimedia_el;
                                    bindViewAll();
                                    loadEllipses();
                                }
                            }
                            // call rateit to display rating
                            $(".rateit").rateit();
                        }
                    })
                    .fail(function (xhr) {
                        _requestCount--;
                        allCallCompletion(isURLObjCheckReq);
                        console.log("error in trove mode template ::", xhr);
                    });
        });
        changeURL("/search/across/all");
        if (searchTerm !== null && searchTerm.trim() !== "") {
            logSearchActivity(searchTerm);
        }
    }
    function allCallCompletion(isURLObjCheckReq) {
        if (_requestCount === 0) {
            $(".all-resources-ul-loader").hide();
            $(".no-result-found").hide();
            if (_resultTotalCount === 0) {
                $(".did_you_mean").show();
                populateSimilarTermLists();
                $(".no-result-found").show();
            } else {
                $(".did_you_mean").hide();
            }
            if (isURLObjCheckReq) {
                renderSelectedResourceFilterAll();
            }
            bindResourceFilterCheckboxClick("trove");
        } else {
            $('.resource-type-filter-chk').unbind().bind('click', function () {
                if (this.checked) {
                    $(this).attr("checked", false);
                } else {
                    $(this).attr("checked", true);
                }
            });
        }
    }
    function renderResourceMixedResultContainer(isRenderResourceFilter, findIn, isURLObjCheckReq) {
        var isRenderSelectedFilter = false;
        var searchTerm = $("#searchElement").val();
        var searchFormData = prepareQueryString().slice(0, -1);
        if (isURLObjCheckReq) {
            searchFormData = location.search.slice(1);
            isRenderSelectedFilter = isURLObjCheckReq;
            isURLObjCheckReq = false;
        }
        var mode = $("#mode").val();
        if (typeof mode === "undefined") {
            mode = "Sorted";
        }
        $.ajax({
            url: "${context}/search/fetch-resource-result/" + findIn + "/" + mode + "/" + _pageNo + "/" + _dataLimit + "?facetRequired=" + isFacetRequired(),
            type: 'POST',
            data: searchFormData,
            beforeSend: function () {
                // setting a timeout
                $(".mixed-result-loader").show();
                $(".all-resources-ul-loader").show();
            },
            complete: function () {
                $(".mixed-result-loader").hide();
                $(".all-resources-ul-loader").hide();
            }
        })
                .done(function (searchResult) {
                    console.log("searchResult::",)
                    if (searchResult.resultSize === 0) {
                        $(".did_you_mean").show();
                        populateSimilarTermLists();
                        $(".no-result-found").show();
                        $("#total-result-display").text(searchResult.resultSize);
                        $(".mixed-result-section").css("display", "none");
                        $(".all-filters").css("display", "none");
                        $("#pagination-footer").empty();
                    } else {
                        $(".did_you_mean").hide();
                        $(".no-result-found").hide();
                        $(".mixed-result-section").css("display", "block");
                        _resultTotalCount = 0;
                        $('#mixed-mode-results').empty();
                        $(".mixed-mode-multimedia").empty();
                        $(".all-filters").css("display", "block");
                        //render non-multimedia template
                        if (!searchResult.multiMedia) {
                            $('.mixed-mode-multimedia').hide();
                            $('#mixed-mode-results').show();
                            _.each(searchResult.mixedSearchResults, function (result) {
                                if (!isURLObjCheckReq || _.contains(_urlParamObj.allResourcesFilters, result.resourceType)) {
                                    var resource_result_temp = _.template($("#mixed-resource-result-tpl").html());
                                    var opts = {
                                        resourceTypeName: _resourceLocalisationMap[result.resourceType].title,
                                        imageURL: "${rTIconDefLocation}/" + _resourceTypeMap[result.resourceType].resourceTypedefaultIcon,
                                        resourceTypedefaultIcon: _resourceTypeMap[result.resourceType].resourceTypedefaultIcon,
                                        resourceTypeSearchIcon: _resourceTypeMap[result.resourceType].resourceTypeSearchIcon,
                                        searchResult: result,
                                        fullText : $('#fullText').is(':checked'),
                                        searchElement: searchTerm
                                    };
                                    var resource_result_el = resource_result_temp(opts);
                                    $('#mixed-mode-results').append(resource_result_el);
                                    loadEllipses();
                                }
                            });
                        } else {
                            //render multimedia template
                            $('.mixed-mode-multimedia').show();
                            $("#grid1").empty();
                            var grid1 = document.querySelector('#grid1');
                            salvattore.registerGrid(grid1);
                            $('#mixed-mode-results').hide();
                            _.each(searchResult.mixedSearchResults, function (result) {
                                if (!isURLObjCheckReq || _.contains(_urlParamObj.allResourcesFilters, result.resourceType)) {
                                    var resource_result_multimedia_temp = _.template($("#mixed-resource-result-multimedia-tpl").html());
                                    var opts = {
                                        resourceTypeName: _resourceLocalisationMap[result.resourceType].title,
                                        imageURL: "${rTIconDefLocation}/" + _resourceTypeMap[result.resourceType].resourceTypedefaultIcon,
                                        resourceTypedefaultIcon: _resourceTypeMap[result.resourceType].resourceTypedefaultIcon,
                                        resourceTypeSearchIcon: _resourceTypeMap[result.resourceType].resourceTypeSearchIcon,
                                        searchResult: result
                                    };
                                    var resource_result_multimedia_el = resource_result_multimedia_temp(opts);

                                    var div = document.createElement("div");
                                    salvattore.appendElements(grid1, [div]);
                                    div.outerHTML = resource_result_multimedia_el;
                                    loadEllipses();
                                }
                            });
                        }
                        // call rateit to display rating
                        $(".rateit").rateit();
                        if (isURLObjCheckReq) {
                            _.each(searchResult.allResources, function (resValue, resKey) {
                                if (_.contains(_urlParamObj.allResourcesFilters, resKey)) {
                                    _resultTotalCount += resValue;
                                }
                            });
                            $("#total-result-display").text(_resultTotalCount);
                        } else {
                            $("#total-result-display").text(searchResult.resultSize);
                        }

                        if (findIn === 'all') {
                            var parentIndex = 0;
                            if (isRenderResourceFilter) {
                                _.each(searchResult.allResources, function (resValue, resKey) {
                                    renderResourceFilter(resKey, ++parentIndex, _resourceLocalisationMap[resKey].title, resValue);
                                });
                            }
                            if (isRenderSelectedFilter) {
                                renderSelectedResourceFilterAll();
                            }
                            bindResourceFilterCheckboxClick("mixed");
                            renderPagination(renderResourceResultContainerByPagination, "pagination-footer", "page-btn", _pageNo, getMaxCount(searchResult.allResources, searchResult.allResourcesFilters), _dataLimit, true, 3, true);
                        } else {
                            $('.all-filters').empty();
                            if (!_.isEmpty(searchResult.facetMap)) {
                                renderFilter(searchResult.facetMap, searchResult.filters, "General", "general-filter", "f");
                            }
                            if (!_.isEmpty(searchResult.specificFacetMap)) {
                                renderFilter(searchResult.specificFacetMap, searchResult.specificFilters, "Specific", "specific-filter", "s");
                            }
                            $("#selectedFilterDiv").empty();
                            $("#selectedFilterDiv").hide();
                            renderSelectedFilterAll();
                            bindFilterCheckboxClick();
                            renderPagination(renderViewAllContainerByPagination, "pagination-footer", "page-btn", _pageNo, searchResult.mixedSearchResultSize, _dataLimit, true, 3, true);
                        }
                    }
                })
                .fail(function (xhr) {
                    console.log("error in render mixed results::", xhr);
                });
        if (findIn === 'all') {
            changeURL("/search/across/" + findIn + "/" + _pageNo + "/" + _dataLimit);
        } else {
            changeURL("/search/across/resource/" + findIn + "/" + _pageNo + "/" + _dataLimit);
        }
        if (searchTerm !== null && searchTerm.trim() !== "") {
            logSearchActivity(searchTerm);
        }
    }

    function loadMultimediaResults(e) {
        var category = e.currentTarget.getAttribute("data-cat");
        var searchQuery = $("#searchElement").val();
        if (searchQuery.indexOf(':') !== -1) {
            searchQuery = searchQuery.split(":").reverse()[0];
        }
        $("#searchElement").val(category + ":" + searchQuery);
        $("#submitSearch").trigger("click");
    }

    function bindRemoveSelectedFilter() {
        $(".selectedFilter").unbind().bind("click", function (e) {
            $(this).remove();
            if ($(".selectedFilter").length === 0) {
                $("#selectedFilterDiv").hide();
            }
            $("input[type=checkbox][id=" + e.currentTarget.getAttribute("data-filter-selected") + "]").prop("checked", false);
            if (_sortingMode === "Sorted" && _viewAllResourceType === "all") {
                renderTroveModeTemplate(resourceTypeFilterCheck(false), false, false);
            } else {
                if (isWebDiscovery(_viewAllResourceType)) {
                    renderViewAllWebDiscoveryByPagination(1);
                } else {
                    renderResourceMixedResultContainer(false, _viewAllResourceType, false);
                }
            }
        });
    }

    function getMaxCount(allResources, allResourcesFilters) {
        if (_.isEmpty(allResourcesFilters)) {
            return _.max(_.values(allResources));
        } else {
            var tempMap = {};
            _.each(allResourcesFilters, function (el) {
                _.each(allResources, function (v, k) {
                    if (k === el) {
                        tempMap[k] = v;
                    }
                });
            });
            return _.max(_.values(tempMap));
        }
    }

    function getExtension(path) {
        if (!_.isEmpty(path)) {
            if (path.includes("www.youtube.com")) {
                return 'mp4';
            } else {
                var extension = path.substr(path.toLowerCase().lastIndexOf('.') + 1);
                switch (extension.toLowerCase()) {
                    case 'pdf':
                        return 'pdf';
                    case 'png':
                    case 'gif':
                    case 'jpg':
                    case 'jpeg':
                        return 'jpg';
                    case 'tif':
                    case 'tiff':
                        return 'tiff';
                    case 'wav':
                    case 'mp3':
                        return 'mp3';
                    case 'mpg':
                    case 'mpeg':
                    case 'mp4':
                        return 'mp4';
                    case 'xml':
                        return 'xml';
                    case 'html':
                        return 'html';
                    case 'doc':
                    case 'docx':
                    case 'odt':
                        return 'docx';
                    case 'ppt':
                    case 'pptx':
                        return 'pptx';
                }
            }
        }
    }

    function insertTag(path, resourceTypeCode) {
        if (!_.isEmpty(path)) {
            return getTag(path, resourceTypeCode);
        } else {
            return getDefaultTag();
        }
    }

    function getTag(path, resourceTypeCode) {
        if (resourceTypeCode === "NPTEL") {
            return getIframeTag(path);
        } else {
            return selectTag(path);
        }
    }

    function selectTag(path) {
        switch (getExtension(path)) {
            case 'jpg':
                return getImageTag(path);
            case 'mp3':
                return getMp3Tag(path);
            case 'mp4':
                return getVideoTag(path);
//uncomment code, if such multimedia formats are available.    
            case 'pdf':
                return getPDFTag();
            case 'tiff':
                return getTiffTag();
            case 'xml':
                return getXmlTag();
            case 'html':
                return getHTMLTag();
            case 'docx':
                return getDocxTag();
            case 'pptx':
                return getPptxTag();
            default:
                return getDefaultTag();
        }
    }

    function getImageTag(path) {
        return '<img src="' + path + '" alt="NVLI" >';
    }

    function getVideoTag(path) {
        return '<video controls controlslist="nodownload"><source src="' + path + '" type="video/mp4">Your browser does not support the video tag.</video>';
    }

    function getIframeTag(path) {
        return '<iframe width="100%" height="100%" src="' + path + '" frameborder="0" allowfullscreen></iframe>';
    }

    function getMp3Tag(path) {
        return '<audio controls style="background:url(${context}/themes/images/icons/multimedia/mp3.jpg);"><source src="' + path + '" type="audio/mpeg">Your browser does not support the audio element.</audio>';
    }

    function getDefaultTag() {
        return '<img src="${context}/themes/images/icons/multimedia/default.jpg" alt="NVLI" >';
    }

    function getPDFTag() {
        return '<img src="${context}/themes/images/icons/multimedia/pdf.jpg" alt="NVLI" >';
    }

    function getTiffTag() {
        return '<img src="${context}/themes/images/icons/multimedia/tiff.jpg" alt="NVLI" >';
    }

    function getXmlTag() {
        return '<img src="${context}/themes/images/icons/multimedia/xml.jpg" alt="NVLI" >';
    }

    function getHTMLTag() {
        return '<img src="${context}/themes/images/icons/multimedia/html.jpg" alt="NVLI" >';
    }

    function getDocxTag() {
        return '<img src="${context}/themes/images/icons/multimedia/docx.jpg" alt="NVLI" >';
    }

    function getPptxTag() {
        return '<img src="${context}/themes/images/icons/multimedia/pptx.jpg" alt="NVLI" >';
    }

    function loadEllipses() {
        $(".imagealt").each(function () {
            var txt = $(this).text().trim();
            if (txt.length > 22) {
                $(this).text(txt.substring(0, 22) + '...');
            }
        });
    }

    function separatedByComma(list) {
        var arr = [];
        _.each(list, function (el) {
            arr.push(el);
        });
        return arr.toString();
    }

    function checkTitleResourceTypeCode(searchResult) {
        switch (searchResult.resourceType) {
            case 'VALUE':
            case 'ENEW':
            case 'GOVW':
                return true;
            default:
                return false;
        }
    }

    function isWebDiscovery(resourceType) {
        switch (resourceType) {
            case 'Google-Books':
            case 'WIKI':
            case 'Open-Library':
            case 'OCLC':
                return true;
            default:
                return false;
        }
    }

    function isExternalResource(resourceType) {
        switch (resourceType) {
            case 'VALUE':
            case 'ENEW':
            case 'GOVW':
                return true;
            default:
                return false;
        }
    }

    function separateStringByhyphen(facetCategory) {
        return facetCategory.indexOf('/') > -1 ? facetCategory.replace(/\//g, '-') : facetCategory.replace(/\s+/g, '-');
    }

    function viewAllWebDiscoveryResults(isRenderFilter) {
        var searchTerm = $("#searchElement").val();
        var searchFormData = prepareQueryString().slice(0, -1);
        $.ajax({
            url: "${context}/search/fetch-web-discovery/" + _viewAllResourceType + "/" + _pageNo + "/" + _dataLimit + "?facetRequired=" + isFacetRequired(),
            data: searchFormData,
            type: 'POST',
            beforeSend: function () {
                // setting a timeout
                $(".web-discovery-loader").show();
                $(".all-resources-ul-loader").show();
                $(".mixed-result-section").css("display", "none");
            },
            complete: function () {
                $(".web-discovery-loader").hide();
                $(".all-resources-ul-loader").hide();
                $(".mixed-result-section").css("display", "block");
            }
        })
                .done(function (searchResult) {
                    if (searchResult.resultSize !== null && searchResult.resultSize !== 0) {
                        $(".did_you_mean").hide();
                        $('#mode-div').hide();
                        $("#total-result-display").text(searchResult.resultSize);
                        $("#trove-mode-div").hide();
                        $("#otherResourceType").hide();
                        $('#mixed-mode-results').empty();
                        $(".mixed-mode-multimedia").empty();
                        $('#mixed-mode-div').show();
                        _.each(searchResult.results, function (value, key) {
                            _.each(value.listOfResult, function (result) {
                                var resource_result_temp = _.template($("#mixed-resource-result-tpl").html());
                                var opts = {
                                    resourceTypeName: _viewAllResourceType,
                                    imageURL: "${rTIconDefLocation}/" + _viewAllResourceType + ".jpg",
                                    resourceTypedefaultIcon: _viewAllResourceType,
                                    resourceTypeSearchIcon: _viewAllResourceType + ".png",
                                    searchResult: result,
                                    fullText : $('#fullText').is(':checked'),
                                    searchElement: searchTerm
                                };
                                var resource_result_el = resource_result_temp(opts);
                                $('#mixed-mode-results').append(resource_result_el);
                            });

                        });
                        if (_viewAllResourceType !== "Open-Library") {
                            if (isRenderFilter) {
                                $('.all-filters').empty();
                                renderFilter(searchResult.facetMap, searchResult.filters, "General", "general-filter", "f");
                                bindFilterCheckboxClick();
                            }
                        } else {
                            $('#searchResultFilter').hide();
                            $(".mixed-result-container").removeClass("col-lg-10").addClass("col-lg-12");
                        }
                        if (_viewAllResourceType === "WIKI" || _viewAllResourceType === "OCLC") {
                            renderPagination(renderViewAllWebDiscoveryByPagination, "pagination-footer", "page-btn", _pageNo, searchResult.resultSize, _dataLimit, true, 3, true);
                        }
                    } else {
                        $(".did_you_mean").show();
                        populateSimilarTermLists();
                        $(".no-result-found").show();
                    }
                })
                .fail(function (xhr) {
                    console.log("error in view all web discovery ::", xhr);
                });

        if (_viewAllResourceType === "WIKI" || _viewAllResourceType === "OCLC") {
            changeURL("/search/across/resource/" + _viewAllResourceType + "/" + _pageNo + "/" + _dataLimit);
        } else {
            changeURL("/search/across/resource/" + _viewAllResourceType);
        }
        if (searchTerm !== null && searchTerm.trim() !== "") {
            logSearchActivity(searchTerm);
        }
    }

    function bindFilterCheckboxClick() {
        $(".chkboxfacet").unbind().bind("click", function (e) {
            $('html,body').scrollTop(0);
            if (this.checked) {
                renderSelectedFilter(e.currentTarget.getAttribute("id"), e.currentTarget.getAttribute("data-facet-name"), e.currentTarget.getAttribute("value"));
            } else {
                var selectedFilterId = e.currentTarget.getAttribute("id");
                $("a[data-filter-selected=" + selectedFilterId + "]").remove();
                if ($(".selectedFilter").length === 0) {
                    $("#selectedFilterDiv").hide();
                }
            }
            if (isWebDiscovery(_viewAllResourceType)) {
                renderViewAllWebDiscoveryByPagination(1);
            } else {
                renderViewAllContainerByPagination(1);
            }
        });
    }

    function bindResourceFilterCheckboxClick(mode) {
        $('.resource-type-filter-chk').unbind().bind('click', function () {
            $('html,body').scrollTop(0);
            if (this.checked) {
                renderSelectedFilter(this.getAttribute("id"), "Resource Type", _resourceLocalisationMap[this.value].title);
            } else {
                var selectedFilterId = this.getAttribute("id");
                $("a[data-filter-selected=" + selectedFilterId + "]").remove();
                if ($(".selectedFilter").length === 0) {
                    $("#selectedFilterDiv").hide();
                }
            }

            if (mode === "trove") {
                renderTroveModeTemplate(resourceTypeFilterCheck(false), false, false);
            } else if (mode === "mixed") {
                renderResourceMixedResultContainer(false, "all", false);
            } else {
                var resObj = resourceTypeFilterCheck(true);
                if (_.isEmpty(resObj)) {
                    resObj = _resourceTypeMap;
                }
                renderTroveModeTemplate(resObj, true, false);
            }
        });
    }

    function renderSelectedFilterAll() {
        $(".chkboxfacet").each(function (i, e) {
            if (this.checked) {
                renderSelectedFilter(this.getAttribute("id"), this.getAttribute("data-facet-name"), this.getAttribute("value"));
            }
        });
    }

    function renderSelectedResourceFilterAll() {
        $(".resource-type-filter-chk").each(function (i, e) {
            if (this.checked) {
                renderSelectedFilter(this.getAttribute("id"), "Resource Type", _resourceLocalisationMap[this.value].title);
            }
        });
    }

    function renderSelectedFilter(currentId, facetName, currentValue) {
        var selected_filter_li_template = _.template($('#tpl-selected-filter-label').html());
        var opts = {
            currentId: currentId,
            facetName: facetName,
            currentValue: currentValue
        };
        var selected_filter_li_el = selected_filter_li_template(opts);
        $('#selectedFilterDiv').append(selected_filter_li_el);
        $('#selectedFilterDiv').show();
        bindRemoveSelectedFilter();
    }

    function changeURL(basePath) {
        if (!_isFirstTime) {
            window.history.pushState(null, null, __NVLI.context + basePath + "?" + prepareQueryString().slice(0, -1));
        } else {
            _isFirstTime = false;
        }
    }

    function prepareQueryString() {
        var searchFormObj = $("#searchForm").serialize();
        var queryStringObj = "";
        _.each(searchFormObj.split("&"), function (paramString) {
            var paramObj = paramString.split("=");
            if (paramObj[1] !== "" && paramObj[0] !== "languageKeyValueJson" && (paramObj[0].includes("allResources") === false || paramObj[0].includes("allResourcesFilters") === true) || paramObj[0] === "searchElement") {
                queryStringObj += paramString + "&";
            }
        });
        return queryStringObj;
    }

    function QueryStringToJSON() {
        var pairs = location.search.slice(1).split("&");
        var result = {};

        pairs.forEach(function (pair) {
            pair = decodeURIComponent(pair).split("=");
            if (pair[1].trim() !== "") {
                if (pair[0].includes("[")) {
                    var response = {};
                    if (_.has(result, pair[0].split("[")[0])) {
                        response = result[pair[0].split("[")[0]];
                    }
                    result[pair[0].split("[")[0]] = recursivelyFindKeys(pair[0], pair[1], response);
                } else if (pair[0] === "allResourcesFilters") {
                    var tempArr = [];
                    if (_.has(result, pair[0])) {
                        tempArr = result[pair[0]];
                    }
                    tempArr.push(pair[1]);
                    result[pair[0]] = tempArr;
                } else {
                    result[pair[0]] = pair[1];
                }
            }
        });

        return JSON.parse(JSON.stringify(result));
    }

    function recursivelyFindKeys(pair, value, response) {
        var key = pair.substring(pair.indexOf("[") + 1, pair.indexOf("]"))
        var nextPair = (pair.split("[" + key + "]")[1]).trim();
        if (nextPair === "") {
            response[key] = value;
        } else {
            response[key] = recursivelyFindKeys(nextPair, value, {});
        }
        return response;
    }

    function submitDYMT(term) {
        $("#searchElement").val(term);
        navigateReqToRelatedFun(false);
    }

    async function populateSimilarTermLists() {
        var resourceType = $("#searchingIn").val();
        var currentValue = document.getElementById("searchElement").value.trim();
        if (currentValue && !isWebDiscovery(resourceType)) {
            $.ajax({
                url: __NVLI.context + "/search/similar/" + resourceType + "/" + currentValue,
                dataType: 'json',
                type: 'GET',
                success: function (lists) {

                    var size = lists.length;
                    $("#similarTerms").empty();
                    $("#termStr").empty();
                    if (size == 0) {
                        $("#similarTerms").append(" No similar terms found !")
                    } else {
                        for (var index = 0; index < lists.length; index++) {
                            var template = _.template($("#span-term-list").html());
                            var opts = {
                                "term": lists[index]
                            };
                            var el = template(opts);
                            if (size > 5)
                            {
                                $("#termStr").append("<a href='javascript:;' id='dymterm" + index + "' data-dismiss='modal' onclick='submitDYMT(\"" + lists[index] + "\");'>" + el + "</a>");
                                if (index != size - 1) {
                                    $("#termStr").append(",");
                                }
                            }
                            if ((index - 1) != size && index != 4)
                            {
                                if (index < 5) {
                                    $("#similarTerms").append("<a href='javascript:;' id='dymterm" + index + "' onclick='submitDYMT(\"" + lists[index] + "\");'>" + el + "</a>");
                                    $("#similarTerms").append(",");
                                }
                            }
                            if (index == 4)
                            {
                                $("#similarTerms").append("<a href='javascript:;' id='dymterm" + index + "' onclick='submitDYMT(\"" + lists[index] + "\");'>" + el + "</a>");
                                if (lists.length > 5)
                                {
                                    $("#similarTerms").append("&nbsp;&nbsp;<a href='javascript:;' data-toggle='modal' data-target='#search_modal'><b>More...</b></a>");
                                }
                            }
                        }
                    }
                },
                error: function (jqXHR) {
                    console.log('error in populate similar terms :: ', jqXHR);
                }
            });
        }
    }

    function logSearchActivity(searchTerm) {
        $.ajax({
            url: __NVLI.context + "/search/log-search-activity",
            type: 'POST',
            data: {
                searchTerm: searchTerm,
                url: (location.pathname + location.search).replace(__NVLI.context, "")
            },
            error: function (xhr) {
                console.log("error in log search activity :: ", xhr);
            }
        });
    }

    //adds related
    function fetchAdvertisement() {
        $.ajax({
            type: "post",
            data: $("#searchForm").serialize(),
            url: __NVLI.context + "/ad/getAds",
            dataType: "json",
            success: function (response) {
                $("#addDiv").empty();
                _.each(response, function (item, i) {
                    var tpl = _.template($("#tpl-ads").html());
                    var imageUrl = '${context}/adBaseLocation/' + item.relativePath + "/" + item.adName;
                    var el = tpl({
                        imageUrl: imageUrl,
                        destinationUrl: item.destinationUrl,
                        displayUrl: item.displayUrl,
                        header: item.header
                    });
                    if (i <= 1) {
                        $("#addDiv").append(el);
                    }
                });
            },
            error: function (e) {
                console.log("error in fetchAdvertisement :: ", e);
            }
        });
    }
</script>
<div class="m-t-3"  style="z-index:-1;">
    <form:form  cssClass="form-group"  id="searchForm" commandName="searchForm" name="searchForm">
        <div class="m-l-3 m-r-3"> <!-- Horizontal curves -->
            <div class="paper clearfix">
                <div class="clearfix row">
                    <div class="p-a-1 clearfix">
                        <div class="clear" style="margin-bottom:0px;"></div>
                        <div class="row">
                            <div class="logos col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                <div class="col-lg-1 col-md-2 col-sm-12 pull-left res-specific-view res-heading-img  m-t-1 text-center" style="display: none;">
                                    <img src="${rTIconLocation}/${searchForm.searchingIn}.png" class="maxw100" alt="National Virtual Library of India (NVLI)" class="m-l-1" />
                                </div>
                                <div class="col-xl-10 col-lg-11 col-md-12 col-sm-12" >
                                    <h3 class="res-specific-view res-heading-name" style="display: none;"></h3>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12" >
                                        <div class="row">
                                            <div class="m-r-1">
                                                <div class="input-group" style="width:100%;">
                                                    <input id="ignrAllRes" name="ignrAllRes" type="hidden"/>
                                                    <input id="languageKeyValueJson"  name="languageKeyValueJson" type="hidden"/>
                                                    <form:input id="searchElement" path="searchElement" onKeyPress="return redirecting(event);" cssClass="form-control p-a-4" placeholder="${searchPlaceholder}" autocomplete="off" autofocus="true"/>
                                                    <form:input path="searchingIn"  hidden="true" id="searchingIn"/>
                                                    <form:input path="searchComplexity"  hidden="true" id="searchComplexity"/>
                                                    <form:input path="advanceSearchQueryString"  hidden="true" id="advanceSearchQueryString"/>
                                                    <form:input path="fromDate" hidden="true"/>
                                                    <form:input path="toDate" hidden="true"/>
                                                    <c:forEach items="${searchForm.advanceSearchQueryMap}" varStatus="index" var="advanceSearchQMap">
                                                        <input type="hidden" name="advanceSearchQueryMap['${advanceSearchQMap.key}']" value='${advanceSearchQMap.value}'>
                                                    </c:forEach>
                                                    <c:forEach items="${searchForm.museumDates}" varStatus="index" var="mdate">
                                                        <input type="hidden" name="museumDates[${index.count-1}]" value="${mdate}">
                                                    </c:forEach>
                                                    <span class="input-group-btn" style="width: 1%;">
                                                        <button class="btn btn-success p-a-4 p-l-1 p-r-1" id="submitSearch" type="button"><i class="fa fa-search" ></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="advc_newLinks">                                        
                                        <a href="#" class="multimedia-cat" data-cat="image">
                                            <i class="fa fa-image" aria-hidden="true"></i> <tags:message code="label.images"/> </a> |
                                        <a href="#" class="multimedia-cat" data-cat="audio">
                                            <i class="fa fa-file-audio-o" aria-hidden="true"></i> <tags:message code="label.audios"/> </a> |
                                        <a href="#" class="multimedia-cat" data-cat="video">
                                            <i class="fa fa-file-video-o" aria-hidden="true"></i> <tags:message code="label.videos"/> </a> |                                        
                                        <a href="#" class="multimedia-cat" data-cat="book">
                                            <i class="fa fa-book" aria-hidden="true"></i> <tags:message code="label.books"/> </a> |
                                        <a href="#" class="multimedia-cat" data-cat="maps">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i> <tags:message code="label.maps"/> </a> |
                                        <a href="#" class="multimedia-cat" data-cat="paper clip">
                                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <tags:message code="label.newspaperclips"/> </a> |
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">
                                                <tags:message code="label.more"/> </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="award">
                                                    <i class="fa fa-trophy" aria-hidden="true"></i> <tags:message code="label.awards"/> </a>                                                    
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="biography">
                                                    <i class="fa fa-book" aria-hidden="true"></i> <tags:message code="label.biography"/> </a>
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="cuisine">
                                                    <i class="fa fa-cutlery" aria-hidden="true"></i> <tags:message code="label.cuisines"/> </a>
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="isbn">
                                                    <i class="fa fa-book" aria-hidden="true"></i> <tags:message code="label.isbn.short"/> </a>                                                    
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="journal">
                                                    <i class="fa fa-book" aria-hidden="true"></i> <tags:message code="label.journals"/> </a>
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="lecture">
                                                    <i class="fa fa-book" aria-hidden="true"></i> <tags:message code="label.lectures"/> </a>
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="personalities">
                                                    <i class="fa fa-users" aria-hidden="true"></i> <tags:message code="label.personalities"/> </a>                                                
                                                <a href="#" class="multimedia-cat dropdown-item" data-cat="report">
                                                    <i class="fa fa-flag" aria-hidden="true"></i> <tags:message code="label.Reports"/> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="d_i_block font14 webscale">
                                        <label style="cursor: pointer;margin-bottom:0px;margin-top:5px; display: none;" class="res-specific-view">
                                            <input type="checkbox" class="font120 font-weight-bold" id="chkboxEntireNVLI" style="vertical-align: text-top;">
                                            <tags:message code="resource.label.all.bubble.search" /> |
                                        </label>
                                        <label style="display: none;" class="all-view">
                                            <input name="isWebscaleDiscovery" id="isWebscaleDiscovery" class="font120 font-weight-bold" type="checkbox">
                                            <tags:message code="label.web.scale"/> |
                                        </label>
                                        <label style="display: none;" class="cross-lingual-view">
                                            <input id="isCrossLingualSearch" name="isCrossLingualSearch" class="font120 font-weight-bold" type="checkbox">
                                            <tags:message code="label.web.cross"/> |
                                        </label>
                                        <label class="full-text-view">
                                            <input id="fullText" name="fullText" class="font120 font-weight-bold" type="checkbox">
                                            <tags:message code="label.fullTextSearch"/> |
                                        </label>
                                        <a href="${context}/advsearch/" class="link1"><tags:message code="label.advanced.search"/></a>
                                        <span class="did_you_mean" style="display: none;">
                                            |
                                            <span class="did_you"><tags:message code="label.did"/></span>
                                            <span id="similarTerms" class="similarTerms"></span>                                            
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-bottom:30px;" class="clearfix"></div>
        <div class="m-l-3 m-r-3" id="main">
            <div class="clear"></div>
            <div class="row clearfix">
                <c:forEach items="${searchForm.subResourceIdentifiers}" varStatus="index" var="subr">
                    <input id="sub${index.count-1}" type="hidden" name="subResourceIdentifiers[${index.count-1}]" value="${subr}">
                </c:forEach>
                <input type="hidden" name="language" id="language" value="${pageContext.response.locale}">
                <div class="col-lg-2 col-md-4 col-sm-12" id="searchResultFilter">
                    <div  class="clearfix all-filters" id="accordion1" role="tablist" aria-multiselectable="true" style="text-transform: capitalize;"></div>
                    <!--add advertisement start here-->
                    <div class="m-t-2">
                        <div class="overflow410 ">
                            <div class="m-ga-1" id="addDiv"></div>
                        </div>
                    </div>
                    <!--add advertisement end here-->
                </div>
                <div class="col-lg-10 col-md-8 col-sm-12 mixed-result-container">
                    <div class="cafgrd clearfix bg-white">
                        <div id="alert-box"></div>
                        <div class="nav-tabs select_language m-b-1" style="display: none;padding:0;">
                            <ul class="nav nav-tabs search_tab" id="myTab1" style="width:90%;"></ul>
                            <div class="cut_tabadd">
                                <select class="form-control" id="selectLang">
                                    <option value="en">English</option>
                                    <option value="hi">हिंदी</option>
                                    <option value="bn">বাংলা</option>
                                    <option value="te">తెలుగు</option>
                                    <option value="mr">मराठी</option>
                                    <option value="ta">தமிழ்</option>
                                    <option value="gu">ગુજરાતી</option>
                                    <option value="kn">ಕನ್ನಡ</option>
                                    <option value="ml">മലയാളം</option>
                                    <option value="pa">ਪੰਜਾਬੀ</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-content">
                            <!-- 1st tab starts  -->
                            <div class="tab-pane nopad active idClass">
                                <div class="clearfix card card bg-greyf6 thin_brdcr" id="searchResultContent">
                                    <div class="card-header clearfix searchPage_results">
                                        <h5 class="pull-left font-weight-bold">
                                            <span id="total-result-display"></span>
                                            <tags:message code="label.search.result"/>
                                            <span class="all-resources-ul-loader" style="display: none">
                                                <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
                                            </span>
                                        </h5>
                                        <div class="pull-right font12">
                                            <!--                                            <div id="save-search-block" class="m-r-1">
                                                                                            <button id="saveSearch" class="btn btn-success btn-sm"><b><span class="fa fa-save"></span>&nbsp;<tags:message code="label.save.search"/></b></button>
                                                                                            <div id="savedPhrase" class="pull-right"></div>
                                                                                        </div>-->
                                            <div id="mode-div">
                                                <select id="mode" class="form-control" name="mode" style="display: initial !important;">
                                                    <option value="Sorted"><tags:message code="label.sorted"/></option>
                                                    <option value="nvli_rl_5"><tags:message code="label.mixed"/></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!------Show selected filter--->
                                    <div class="p-a-1 bg-greye9 m_lab" style="border-bottom:1px solid #ccc; display: none;" id="selectedFilterDiv" ></div>
                                    <!-- search result box  -->
                                    <div class="no-result-found alert-warning" style="text-align: center;padding: .75rem;display: none;">
                                        <b><tags:message code="label.noResultsFound"/></b>
                                    </div>
                                    <div id="mixed-mode-div" class="clearfix bg-white" style="display: none">
                                        <div style="margin-bottom:0px;" class="clearfix"></div>
                                        <section class="mixed-result-section clearfix">
                                            <div class="mixed-result-body">
                                                <div class="mixed-result-loader" style="display: none">
                                                    <center><img src="${context}/themes/images/processing.gif" alt="Loading..." /></center>
                                                </div>
                                                <div class="m-a-1">
                                                    <div class="clearfix cust_clearfix"></div>
                                                    <div class="search_page">
                                                        <div class="row mixed-mode-non-multimedia" id="mixed-mode-results"></div>
                                                        <div class="masonry adv_imageThumb mixed-mode-multimedia" id="grid1" data-columns></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>                                                
                                            </div>
                                        </section>
                                        <div class="clear22"></div>
                                        <div class="card-footer clearfix" id="pagination-footer"></div>
                                    </div>
                                    <div id="trove-mode-div" class="m-a-1" style="display: none">
                                        <div class="clearfix cust_clearfix"></div>
                                        <div class="search_page">
                                            <div class="masonry" id="grid" data-columns="2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="otherResourceType" style="display:none;">
                    <div class="col-lg-2 col-md-4 col-sm-12"></div>
                    <div class="col-lg-10 col-md-8 col-sm-12">
                        <div class="clearfix card card bg-greyf6 thin_brdcr" id="searchResultContent">
                            <h5 class="card-header">
                                <tags:message code="label.web.scale"/>
                                <span class="web-discovery-loader" style="display: none">
                                    <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
                                </span>
                            </h5>
                            <div class="m-a-1">
                                <div style="margin-bottom:15px;" class="clearfix"></div>
                                <div class="grid2 masonry"  id="grid2"></div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
</div>
