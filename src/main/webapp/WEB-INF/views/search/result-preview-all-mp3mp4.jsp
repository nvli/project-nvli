<%--
    Document   : result-preview-all-mp3mp4
    Created on : 9 Jun, 2016, 3:13:41 PM
    Author     : priya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<meta name="referrer" content="origin-when-crossorigin">
<div class="col-lg-3 col-md-4 col-sm-12">
    <div class="card clearfix">
        <h5 class="card-header"> List </h5>
        <ul class="list_sbar clearfix">
            <c:if test="${not empty record.paths && record.paths ne null}">
                <c:set var="firstRecordPath" value="${record.paths.get(0)}"></c:set>
                <c:set var="string" value="${fn:split(firstRecordPath, '/')}" />
                <c:set var="firstRecordFileName" value="${string[fn:length(string)-1]}" />
                <c:forEach items="${record.paths}" var="path">
                    <!--fileName-->
                    <c:set var="string2" value="${fn:split(path, '/')}" />
                    <c:set var="fileName" value="${string2[fn:length(string2)-1]}" />
                    <li>
                        <a href="#" onclick="renderFile('${path}');">
                            <span title="${fileName}" class="l_txt">
                                <c:if test="${fn:containsIgnoreCase(fileName.toLowerCase(), '.mp4')||fn:containsIgnoreCase(fileName.toLowerCase(), '.mpg')||fn:containsIgnoreCase(fileName.toLowerCase(), '.wav')}">
                                    <i class="fa fa-video-camera text-danger m-r-1" aria-hidden="true"></i>
                                </c:if>
                                <c:if test="${fn:containsIgnoreCase(fileName.toLowerCase(), '.mp3')}">
                                    <i class="fa fa-volume-up text-danger m-r-1" aria-hidden="true"></i>
                                </c:if>
                                <c:if test="${fn:containsIgnoreCase(fileName.toLowerCase(), '.pdf')}">
                                    <i class="fa fa-file-pdf-o text-danger m-r-1" aria-hidden="true"></i>
                                </c:if>
                                ${fileName}
                            </span>
                        </a>
                    </li>
                </c:forEach>
            </c:if>
        </ul>
    </div>
</div>
<!-- right side content starts here -->
<div class="col-lg-9 col-md-8 col-sm-12 ">
    <div class="clearfix card bg-greyf6 thin_brdcr">
        <!-- Page Content -->
        <h5 class="card-header">
            <img src="${rTIconSearchLocation}/${resourceType.resourceTypeSearchIcon}"  alt="National Virtual Library of India (NVLI)" height="32" class="pull-left" style="margin-top:-7px;"/>
            <span class="m-l-1">
                <c:choose>
                    <c:when test="${not empty record.title && record.title ne null}">
                        ${record.title[0]}
                    </c:when>
                    <c:otherwise>
                        ${record.recordIdentifier}
                    </c:otherwise>
                </c:choose>
            </span>
            <!--For time code marking-->
            <c:if test="${not empty firstRecordPath && firstRecordPath ne null && crowdSource}">
                <!--firstRecordFileName-->
                <span class="pull-md-right font12" style="margin-top:-4px;" id="timecodeMarkingDiv">
                    <c:if test="${fn:containsIgnoreCase(firstRecordFileName.toLowerCase(), '.mp4')}">
                        <button class="btn btn-primary" type="button" data-toggle="modal"  onclick="openVideoMarking();"><tags:message code="btn.time.code.marking"/></button>
                    </c:if>
                    <c:if test="${fn:containsIgnoreCase(firstRecordFileName.toLowerCase(), '.mp3')}">
                        <button class="btn btn-primary" type="button" data-toggle="modal"  onclick="openVideoMarking();"><tags:message code="btn.time.code.marking"/></button>
                    </c:if>
                </span>
            </c:if>
        </h5>
        <div class="m-ua-1">
            <div style="margin-bottom:0px;" class="clearfix"></div>
            <c:if test="${not empty firstRecordPath && firstRecordPath ne null}">
                <!--firstRecordFileName-->
                <div id="viewall">
                    <c:if test="${fn:containsIgnoreCase(firstRecordFileName.toLowerCase(), '.mp4')}">
                        <div class='videop marginauto'><div class='video_innerbox1'> <video  oncontextmenu="return false;" controls controlsList='nodownload'><source src='${firstRecordPath}' type='video/mp4'><tags:message code="sr.rpa.msg"/></video></div></div>
                                </c:if>
                                <c:if test="${fn:containsIgnoreCase(firstRecordFileName.toLowerCase(), '.mp3')}">
                        <div class='audiop marginauto'><div class='audio_innerbox'><audio controls><source src='${firstRecordPath}' type='audio/mpeg'></audio></div></div>
                                </c:if>
                                <c:if test="${fn:containsIgnoreCase(firstRecordFileName.toLowerCase(), '.pdf')}">
                        <object class='videop object' data='${firstRecordPath}' type='application/pdf' width='100%' height='100%'></object>
                    </c:if>
                </div>
            </c:if>
        </div>
    </div>
</div>
<!-- right side content ends here -->
<script type="text/javascript">
    var fileName = '${record.originalFileName}'.toLowerCase();
    console.log(fileName);
    function renderFile(path) {
        console.log(path);
        if (fileName.indexOf(".mp4") != -1) {
            var displayHTML = "<div class='videop marginauto'><div class='video_innerbox1'> <video  oncontextmenu='return false;' controls controlsList='nodownload'><source src='" + path + "' type='video/mp4'>Your browser does not support the video tag.</video></div></div>";
            $('#viewall').html(displayHTML);
        } else if (fileName.indexOf(".mp3") != -1) {
            var displayHTML = " <div class='audiop marginauto'><div class='audio_innerbox'><audio controls><source src='" + path + "' type='audio/mpeg'></audio></div></div>";
            $('#viewall').html(displayHTML);
        }
        if (fileName.indexOf(".pdf") != -1) {
            var displayHTML = "<object class='videop object' data='" + path + "' type='application/pdf' width='100%' height='100%'></object>";
            $('#viewall').html(displayHTML);
        }
    }
</script>
