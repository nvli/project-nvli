<%-- 
    Document   : result-preview-all
    Created on : 7 Jun, 2016, 4:13:21 PM
    Author     : priya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta name="referrer" content="origin-when-crossorigin">
<div class="clearfix equalheight card">
    <div class="card-header"> 
        <img src="${rTIconSearchLocation}/${resourceType.resourceTypeSearchIcon}" alt="National Virtual Library of India (NVLI)" height="32" class="pull-left" style="margin-top:-4px;"/>
        <span class="txt_highlight m-l-1">
            <c:choose>
                <c:when test="${not empty record.title && record.title ne null}">
                    ${record.title[0]}
                </c:when>
                <c:otherwise>
                    ${record.recordIdentifier}
                </c:otherwise>
            </c:choose>
        </span> 
    </div>
    <div class="clearfix"> 
        <div  id="viewer" class="viewer text-center iviewer_image"></div>
    </div>
    <div class="card-footer">
        <div class="block-center marginauto">
            <a id="prev" href="#">
                <img src="${pageContext.request.contextPath}/themes/images/iviewer/prev.png" alt="Prev" title="Prev"  height="24" hspace="2" width="24" >
            </a>
            <label id="page_Num"></label>
            <label id="total_Pages"></label>
            <a id="next" href="#">
                <img src="${pageContext.request.contextPath}/themes/images/iviewer/next.png" alt="Next" title="Next"  height="24" hspace="2" width="24"> 
            </a>
        </div>
    </div>
</div>
</div>
<script src="${context}/themes/js/iviewer/jqueryui.js"></script>
<script src="${context}/themes/js/iviewer/jquery.iviewer.sh.js"></script>
<script type="text/javascript" charset="utf-8">
    var selectedFilePath,currentPageNo,totalPages,tempTiffFile;
    var recordIdentifier='${record.recordIdentifier}';
    
    $(document).ready(function() {
        /*Preview of previous page*/
        $("#prev").click(function() {
            if(currentPageNo<=1){
                alert('Previous page is not available.');
            }else{
                renderTiff(tempTiffFile,(currentPageNo-1),totalPages);
            }
        });
        /*Preview of next page*/
        $("#next").click(function() {
            if(currentPageNo>=(totalPages-1)){
                alert('Next page is not available.');
            } else{
                renderTiff(tempTiffFile,(currentPageNo+1),totalPages);
            }
        });
    });
    $(function(){
        $.ajax(
        {
            type: "POST", 
            url: "${pageContext.request.contextPath}/search/preview/all/getTiffImagesCount/"+recordIdentifier,
            success: function(response){
                renderTiff(recordIdentifier,1,response);
            },
            error: function(e){
                alert('Error while retreiving number of pages in a tiff file: ' + e+'<br/>Tiff File: '+recordIdentifier);
            }
        });
    });
    function renderTiff(tiffFile,no,numPages){
        selectedFilePath="${pageContext.request.contextPath}/search/preview/all/renderTiff/"+recordIdentifier+"/"+no;
        tempTiffFile=tiffFile;
        currentPageNo=no;
        totalPages=numPages;
        $("#page_Num").text(currentPageNo);
        $("#total_Pages").text("/"+totalPages);
        $(function(){
            $('#viewer').show();
            //            console.log(numPages+" in tif"+tiffFile+" "+no);
            var iv1=$("#viewer").iviewer({
                src: selectedFilePath 
            });
            iv1.iviewer('loadImage', selectedFilePath);
        });
    }
</script>

