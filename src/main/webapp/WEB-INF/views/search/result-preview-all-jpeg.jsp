<%-- 
    Document   : result-preview-all-jpeg
    Created on : 8 Jun, 2016, 3:32:23 PM
    Author     : priya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
    <head>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="referrer" content="origin-when-crossorigin">
        <link rel="stylesheet" type="text/css" href="${context}/themes/BookReader/BookReader.css" id="BRCSS"/>
        <link rel="stylesheet" type="text/css" href="${context}/themes/BookReader/BookReaderDemo.css"/>
        <link rel="stylesheet" type="text/css" href="${context}/themes/BookReader/mmenu/dist/css/jquery.mmenu.css"/>
        <link rel="stylesheet" type="text/css" href="${context}/themes/BookReader/mmenu/dist/addons/navbars/jquery.mmenu.navbars.css"/>

        <script type="text/javascript" src="${context}/themes/BookReader/jquery-1.10.1.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/jquery-ui-1.12.0.min.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/jquery.browser.min.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/dragscrollable-br.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/jquery.bt.min.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/soundmanager/script/soundmanager2-jsmin.js"></script>
        <script>
            soundManager.debugMode = false;
            soundManager.url = '${context}/themes/js/BookReader/soundmanager/swf';
            soundManager.useHTML5Audio = true;
            soundManager.flashVersion = 9; //flash 8 version of swf is buggy when calling play() on a sound that is still loading
        </script>
        <script type="text/javascript" src="${context}/themes/BookReader/mmenu/dist/js/jquery.mmenu.min.js"></script>
        <script type="text/javascript" src="${context}/themes/BookReader/mmenu/dist/addons/navbars/jquery.mmenu.navbars.min.js" ></script>
        <script type="text/javascript" src="${context}/themes/BookReader/BookReader.js"></script>

<!--        <script type="text/javascript" src="${context}/themes/js/BookReader/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/dragscrollable.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/jquery.ui.ipad.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/jquery.bt.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/initBookReader.js"></script>
        <script type="text/javascript" src="${context}/themes/js/BookReader/BookReader.js"></script>-->
        
    </head>

    <!--    <body>
            <div id="BookReader"></div>
            <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
            </script>
            <script>
                $(document).ready(function () {
                   $(window).load(function () {
                wh = $(window).height();
                $("#BookReader").css({height: wh-100px});
            });
                });
            </script>
        </body>-->
    <body style="background-color: #939598;">
        <div id="BookReader" style="width:100%; overflow: hidden;">
            Internet Archive BookReader Demo<br/>
            <noscript>
            <p>
                The BookReader requires JavaScript to be enabled. Please check that your browser supports JavaScript and that it is enabled in the browser settings.  You can also try one of the <a href="http://www.archive.org/details/goodytwoshoes00newyiala"> other formats of the book</a>.
            </p>
            </noscript>
        </div>
        <script type="text/javascript">
             var wh = $(window).height();
            $("#BookReader").css("height",wh);
        </script>
        <script>
            var allpaths = '${pathJson}';
            var bookViewer = '${bookViewer}';
            var recordIdentifier = '${recordIdentifier}';
        </script>
        <script type="text/javascript" src="${context}/themes/BookReader/BookReaderJSSimple.js"></script>
        <script>
            var fullTextFlag ='${fullTextFlag}';
            if(fullTextFlag == 'false'){
                $(".BRtoolbarSectionSearch ").hide();
            }
            if(!$(".textSrch").val()){
               $(".textSrch").val('${searchElement}');
               $("#btnSrch").submit();
            }  
        </script>
    </body>

</html>
