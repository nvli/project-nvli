<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>

<noscript>Java Script is off. Please enable Java Script.</noscript>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"  %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<spring:message code="label.review.placeholder" var="reviewPlaceholder" />
<c:set var="location" value="${baseLocation}${record.location}/${record.originalFileName}"/>
<c:set var="backslash" value="\\\\"/>
<c:set var="forwardslash" value="/"/>
<c:set var="underScore" value="_"/>
<c:set var="path" value="${record.paths[0]}" />
<meta name="referrer" content="origin-when-crossorigin">
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src="${context}/themes/js/language/pramukhindic.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>
<script src="${context}/components/jquery/jquery-dateFormat.min.js"></script>
<script src="${context}/components/underscore/underscore.min.js"></script>
<!--Files for XML Display-->
<link rel="stylesheet" href="${context}/themes/css/XMLDisplay.css">
<script src="${context}/themes/js/XMLDisplay.js"></script>
<!--Files for XML Display-->
<c:if test="${not empty record && record ne null}">
    <div class="row">
        <div class="col-lg-12">
            <div class=" clearfix card search_priview_page m-b-2">
                <div class="clearfix bg1">
                    <div class="col-lg-12 border-b clearfix ">
                        <div class="row">
                            <div class="card-header">
                                <img src="${rTIconSearchLocation}/${resourceType.resourceTypeSearchIcon}" alt="National Virtual Library of India (NVLI)" height="32" class="pull-left" style="margin-top:-4px;"/>&nbsp;
                                ${record.nameToView}                       
                            </div>
                            <nav class="nav_tool card-header5 preference clearfix" id="userActionsDiv">
                                <div style="float: left;margin-top: 7px;">
                                    <c:if test="${record.paths.size() > 1}">
                                        <a id="viewAllJpeg" href="${context}/search/preview/all/jpeg/${record.recordIdentifier}?language=${pageContext.response.locale}&fullTextFlag=${record.fullTextFlag}" class="card-link" target="_blank"><tags:message code="label.view.all"/></a>
                                        <a id="viewAllTif" href="${context}/search/preview/all/tif/${record.recordIdentifier}?language=${pageContext.response.locale}" class="card-link" target="_blank"><tags:message code="label.view.all"/></a>
                                        <a id="viewAllMp3Mp4" href="${context}/search/preview/all/mp3mp4/${record.recordIdentifier}?language=${pageContext.response.locale}" class="card-link" target="_blank"><tags:message code="label.view.all"/></a>
                                    </c:if>
                                </div>
                                <ul class="marginauto">
                                    <li>
                                        <c:choose>
                                            <c:when test="${user==null}">
                                                <a href="${context}/auth/login" class="link3"><i class="fa fa-thumbs-up"></i> <tags:message code="label.like"/></a></li>
                                            </c:when>
                                            <c:otherwise>
                                            <a href="#" class="link3" onclick="likeActivity()" id="likeAnchor"><i class="fa fa-thumbs-o-up"></i> <tags:message code="label.like"/></a></li>
                                        </c:otherwise>
                                    </c:choose>
                                    <!-- social btns -->
                                    <li class="dropdown">
                                        <c:choose>
                                            <c:when test="${user==null}">
                                                <a href="${context}/auth/login" class="link3"><i class="fa fa-share-alt"></i> <tags:message code="label.share"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="dropdown-toggle link3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share-alt"></i> <tags:message code="label.share"/></a>
                                                <div id="share" class="dropdown-menu"></div>                                              
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <!-- /social btns -->
                                    <!-- icon btns fa-external-link-square -->
                                    <%-- <li>
                                         <c:choose>
                                             <c:when test="${user==null}">
                                                 <a href="${context}/auth/login" class="link3"><i class="fa  fa-copy"></i> Export this </a>
                                             </c:when>
                                             <c:otherwise>
                                                 <a href="#" class="link3"><i class="fa  fa-copy"></i> Export this </a>
                                             </c:otherwise>
                                         </c:choose>
                                     </li>
                                     <li>
                                         <c:choose>
                                             <c:when test="${user==null}">
                                                 <a  href="${context}/auth/login" class="link3"><i class="fa fa-mobile"></i> Text this </a>
                                             </c:when>
                                             <c:otherwise>
                                                 <a href="#" class="link3"><i class="fa fa-mobile"></i> Text this </a>
                                             </c:otherwise>
                                         </c:choose>
                                     </li>--%>
                                    <li> <a href="#" class="link3" onclick="copyUrl();" id="copyUrl"><i class="fa fa-external-link"></i> <tags:message code="label.copy.url"/> </a> </li>
                                    <li>
                                        <c:choose>
                                            <c:when test="${user==null}">
                                                <a href="${context}/auth/login" class="link3"><i class="fa fa-envelope-o"></i> <tags:message code="label.e-mail"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <span id="emailshare"></span>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <li>
                                        <c:choose>
                                            <c:when test="${user==null}">
                                                <a href="${context}/auth/login" class="link3"><i class="fa fa-pencil-square-o"></i> <tags:message code="label.review.msg"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="#reviewt" class="link3"><i class="fa fa-pencil-square-o"></i><tags:message code="label.review.msg"/></a>
                                                </c:otherwise>
                                            </c:choose>
                                    </li>
                                    <li>
                                        <c:choose>
                                            <c:when test="${record.crowdSourceStageFlag eq false}">
                                                <a href="${context}/cs/${record.recordIdentifier}" class="link3"><i class="fa fa-users"></i> <tags:message code="label.crowdsource"/> </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="nav-link disabled"><i class="fa fa-link"></i> <tags:message code="label.crowdsource"/> </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <li>
                                        <c:choose>
                                            <c:when test="${user==null}">
                                                <a href="${context}/auth/login" class="link3"><i class="fa fa-link"></i> <tags:message code="label.add.tag"/> </a>
                                            </c:when>
                                            <c:when test="${record.crowdSourceStageFlag eq false}">
                                                <a href="#" data-toggle="modal" data-target="#addTagModal" class="link3"><i class="fa fa-tag"></i><tags:message code="label.add.tag"/> </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="nav-link disabled"><i class="fa fa-link"></i> <tags:message code="label.add.tag"/> </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <li><c:choose>
                                            <c:when test="${user==null}">
                                                <a href="${context}/auth/login" class="link3"><i class="fa fa-floppy-o"></i> <tags:message code="label.save.to.list"/></a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="#" class="link3" id="saveToListAnchor"><i class="fa fa-floppy-o"></i> <tags:message code="label.save.to.list"/></a>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <li>
                                        <c:choose>
                                            <c:when test="${resourceType.resourceTypeCode eq 'BLOG'}">
                                                <a href="javascript:void(0)" class="nav-link disabled">
                                                    <i class="fa fa fa-link"></i> <tags:message code="label.citation"/>
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="javascript:void(0)" class="link3" onclick="citation();">
                                                    <i class="fa fa fa-link"></i> <tags:message code="label.citation"/>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <li>
                                        <a id="imageTagging" class="nav-link disabled">
                                            <i class="fa fa fa-link"></i> <tags:message code="label.image.tagging"/>
                                        </a>
                                    </li>
                                    <li><c:choose>
                                            <c:when test="${record.fullTextFlag eq true}">
                                                <c:choose>
                                                    <c:when test="${user==null}">
                                                        console.log("======",${record.fullTextFlag});
                                                        <a id="fullTextCrowdsource" class="nav-link">
                                                            <a href="${context}/auth/login" class="link3"><i class="fa fa fa-link"></i> <tags:message code="label.full_text_crowdsource"/></a>
                                                        </a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a href="${context}/cs/full-text-crowdsource/${record.recordIdentifier}/1/${record.totalPages}?recordLocation=${record.location}&recordTitle=${record.nameToView}" target="_blank" class="nav-link" id="fullTextCrowdsource"><i class="fa fa fa-link"></i> <tags:message code="label.full_text_crowdsource"/></a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                   <a class="nav-link disabled"><i class="fa fa-link"></i> <tags:message code="label.full_text_crowdsource"/> </a>
                                            </c:otherwise>
                                        </c:choose>
                                    </li>
                                    <%-- <li>
                                         <c:choose>
                                             <c:when test="${user==null}">
                                                 <a href="${context}/auth/login" class="link3"><i class="fa fa-external-link"></i> Print</a>
                                             </c:when>
                                             <c:otherwise>
                                                 <a href="#" class="link3"  onclick="printOb()"><i class="fa fa-external-link"></i> Print</a>
                                             </c:otherwise>
                                         </c:choose>
                                     </li>
                                     <li>
                                         <c:choose>
                                             <c:when test="${user==null}">
                                                 <a href="${context}/auth/login" class="link3"><i class="fa fa-download"></i> Download</a>
                                             </c:when>
                                             <c:otherwise>
                                                 <a href="#" class="link3"  onclick="openDownload();"><i class="fa fa-download"></i> Download</a>
                                                 <div id="openDownload">
                                                     <a href="${context}/search/preview/download/content/${record.recordIdentifier}" >Content</a>
                                                     <a href="${context}/search/preview/download/metadata/${record.recordIdentifier}" >Metadata</a>
                                                     <a href="${context}/search/preview/download/content_metadata/${record.recordIdentifier}" >Content and Metadata</a>
                                                 </div>
                                             </c:otherwise>
                                         </c:choose>
                                     </li>--%>
                                    <span id="btn-buy-div"></span>
                                </ul>
                            </nav>
                        </div>
                        <c:choose>
                            <c:when test="${resourceType.resourceTypeCode eq 'BLOG'}">

                                <div class="article_writing container">
                                    <div class="row">
                                        <div id="name_Article" class="collapse in">
                                            <div class="m-t-1">

                                                <div>
                                                    <c:if test="${not empty record.others && record.others ne null}">
                                                        <c:forEach items="${record.others}" var="otherMetadata">
                                                            <c:if test="${not empty otherMetadata.tagValues && otherMetadata.tagValues ne null}">
                                                                <c:if test="${fn:contains(otherMetadata.tagName, 'description')==true}">
                                                                    ${otherMetadata.tagValues[0]}
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5 col-lg-3">
                                        <c:if test="${not empty record.udcNotation}">
                                            <div class="panel card m-t-1">
                                                <div class="article_writing border_line">
                                                    <div class="accordion-head1 tr_hand">
                                                        <div  type="button" data-toggle="collapse" data-target="#relatedUDCTags" aria-expanded="true" aria-controls="collapseExample">
                                                            <span>
                                                                <strong> <tags:message code="label.udc.tags"/>  </strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div id="relatedUDCTags" class="collapse in">
                                                        <div class="card-block">
                                                            <div class="p-a-1 custom_paddingarticle">
                                                                <c:forEach items="${record.udcNotation}" var="value" varStatus="increment">
                                                                    ${fn:trim(value)}<c:if test="${increment.count ne record.udcNotation.size()}">&#44;</c:if>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${not empty record.customNotation}">
                                            <div class="panel card m-t-1">
                                                <div class="article_writing border_line">
                                                    <div class="accordion-head1 tr_hand">
                                                        <div  type="button" data-toggle="collapse" data-target="#relatedCustomTags" aria-expanded="true" aria-controls="collapseExample">
                                                            <span>
                                                                <strong><tags:message code="label.custom.tags"/> </strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div id="relatedCustomTags" class="collapse in">
                                                        <div class="card-block">
                                                            <div class="p-a-1 custom_paddingarticle">
                                                                <c:forEach items="${record.customNotation}" var="value" varStatus="increment">
                                                                    ${fn:trim(value)}<c:if test="${increment.count ne record.customNotation.size()}">&#44;</c:if>
                                                                </c:forEach>
                                                                <%-- <c:forEach items="${crowdSourceArticleCustomTags}" var="crowdSourceArticleCustomTag">
                                                                     <a href="#" class="label label-default">${crowdSourceArticleCustomTag}</a>
                                                                 </c:forEach>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${not empty crowdSourceAttachedArticleList}">
                                            <div class="panel card m-t-1" id="related-article-div">
                                                <div class="article_writing border_line">
                                                    <div class="accordion-head1 tr_hand">
                                                        <div  type="button" data-toggle="collapse" data-target="#relatedArticles" aria-expanded="true" aria-controls="collapseExample">
                                                            <span>
                                                                <strong> <tags:message code="label.related.article"/> </strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div id="relatedArticles" class="collapse in">
                                                        <div id="bookmarkList_dashboard" class="collapse in">
                                                            <div class="card-block" id='related-article-div-body'>
                                                                <c:forEach items="${crowdSourceAttachedArticleList}" var="crowdSourceAttachedArticle">
                                                                    <c:forEach items="${crowdSourceAttachedArticleIdentifierMap}" var="crowdSourceAttachedArticleIdentifier">
                                                                        <c:if test="${ crowdSourceAttachedArticle.isApproved eq 'true' && crowdSourceAttachedArticle.id eq crowdSourceAttachedArticleIdentifier.key}">
                                                                            <div class="favoriteList">
                                                                                <p>
                                                                                    <span>
                                                                                        <a href="${context}/search/preview/${crowdSourceAttachedArticleIdentifier.value}?language=${pageContext.response.locale}">${crowdSourceAttachedArticle.articleTitle}</a> by <b>${crowdSourceAttachedArticle.user.firstName} ${crowdSourceAttachedArticle.user.lastName}</b>
                                                                                    </span>
                                                                                </p>
                                                                            </div>
                                                                            <hr/>
                                                                            <input type="hidden" name="isAttachedArticleListEmpty" id="isAttachedArticleListEmpty" value="1" />
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${not empty crowdSourceArticle.crowdSourceArticleAttachedResources}">
                                            <div class="panel card m-t-1">
                                                <div class="article_writing border_line">
                                                    <div class="accordion-head1 tr_hand">
                                                        <div  type="button" data-toggle="collapse" data-target="#relatedResources" aria-expanded="true" aria-controls="collapseExample">
                                                            <span>
                                                                <strong><tags:message code="label.related.resources"/> </strong>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div id="relatedResources" class="collapse in">
                                                        <div id="bookmarkList_dashboard" class="collapse in">
                                                            <div class="card-block">
                                                                <c:forEach items="${crowdSourceArticle.crowdSourceArticleAttachedResources}" var="crowdSourceArticleAttachedResource">
                                                                    <div class="favoriteList">
                                                                        <p>
                                                                            <span>
                                                                                <a href="${crowdSourceArticleAttachedResource.resourceURL}">${crowdSourceArticleAttachedResource.resourceLabel}</a>
                                                                            </span>
                                                                        </p>
                                                                    </div>
                                                                    <hr/>
                                                                </c:forEach>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>

                            </c:when>
                            <c:otherwise>
                                <div class="clearfix equalheight row" style="padding:3px 3px 0 3px;">
                                    <!-- sidebar -->
                                    <div class="col-lg-5 col-md-6 col-sm-12 demo bg-white width_custom">
                                        <div class="wrapper">
                                            <!--<div id="viewer" class="viewer"></div>-->
                                            <div id="cut_viewer" class="cut_viewer" style="margin: 0 auto;text-align: center;"></div>
                                            <div id="iviewer">
                                                <div class="loader"></div>
                                                <div class="viewer"></div>
                                                <ul class="controls">
                                                    <li class="close_btn"></li>
                                                    <li class="zoomin"></li>
                                                    <li class="zoomout"></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="full_width m-t-1" style="text-align: center;" id="fullSize">
                                            <a id="fullSizeId" class="iviewer_button"><img src="${context}/themes/images/iviewer/fullscreen_24.png" alt="NVLI" width="24" height="24" title="Maximize"> </a>
                                            <a id="minSizeId" class="iviewer_button"><img src="${context}/themes/images/iviewer/min-screen_24.png" alt="NVLI" width="24" height="24" title="Minimize"> </a>
                                        </div>
                                        <div class="row m-t-1" id="viewer-sidebar">
                                            <div class=" marginauto">
                                                <span id="in" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_in.png" width="24" height="24" alt="NVLI"> </span>
                                                <span id="out" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_out.png" width="24" height="24" alt="NVLI"> </span>
                                                <span id="orig" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_zero.png" width="24" height="24" alt="NVLI"> </span>
                                                <span id="fit" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_fit.png" width="24" height="24" alt="NVLI"> </span>
                                                <span id="rotateleft" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.rotate_left.png" width="24" height="24" alt="NVLI"> </span>
                                                <span id="rotateright" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.rotate_right.png" width="24" height="24" alt="NVLI"> </span>
                                                <a id="fullscreen" class="iviewer_button"><img src="${context}/themes/images/iviewer/fullscreen_24.png" alt="NVLI" width="24" height="24"> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- right side content starts here -->
                                    <div class="col-lg-7 col-md-6 col-sm-12 border-lt demo bg-white width_custom">
                                        <div class="p_rel row clearfix">
                                            <div class="p-a-2 search_priview_page_table1">
                                                <div class="overflow685">
                                                    <div id="payment-info"></div>
                                                    <table cellpadding="0" cellspacing="0" class="table table-bordered table-md table-responsive">
                                                        <tbody style="display: inline-table; width: 100%">
                                                            <!--Sub Resource Name-->
                                                            <c:if test="${not empty record.subResourceName && record.subResourceName ne null}">
                                                                <tr class="col_head">
                                                                    <th width="140" class="text-right"><tags:message code="label.resource"/></th>
                                                                    <td>
                                                                        ${fn:trim(record.subResourceName)}
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Title-->
                                                            <c:if test="${not empty record.title && record.title ne null}">
                                                                <tr class="col_head">
                                                                    <th width="140" class="text-right"><tags:message code="label.title"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.title}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.title.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Creator-->
                                                            <c:if test="${not empty record.creator && record.creator ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.creator"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.creator}" var="value" varStatus="increment">
                                                                            ${fn:trim(fn:replace(value, ',', ' '))}<c:if test="${increment.count ne record.creator.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Subject-->
                                                            <c:if test="${not empty record.subject && record.subject ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.subject"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.subject}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.subject.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Description-->
                                                            <c:if test="${not empty record.description && record.description ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.description"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.description}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.description.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Publisher-->
                                                            <c:if test="${not empty record.publisher && record.publisher ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.publisher"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.publisher}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.publisher.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Contributor-->
                                                            <c:if test="${not empty record.contributor && record.contributor ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.contributor"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.contributor}" var="value" varStatus="increment">
                                                                            <c:choose>
                                                                                <c:when test="${fn:startsWith(value, 'http')||fn:startsWith(value, 'www')}">
                                                                                    <a style="color:#2a5cea;" target="_blank" href="${value}"> ${fn:trim(value)}<c:if test="${increment.count ne record.contributor.size()}">&#44;</c:if></a>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    ${fn:trim(value)}<c:if test="${increment.count ne record.contributor.size()}">&#44;</c:if>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Date-->
                                                            <c:if test="${not empty record.date && record.date ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.date"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.date}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.date.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Type-->
                                                            <c:if test="${not empty record.type && record.type ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.type"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.type}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.type.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Format-->
                                                            <c:if test="${not empty record.format && record.format ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.format"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.format}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.format.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Identifier-->
                                                            <c:if test="${not empty record.identifier && record.identifier ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.identifier"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.identifier}" var="value" varStatus="increment">
                                                                            <c:choose>
                                                                                <c:when test="${fn:startsWith(value, 'http')||fn:startsWith(value, 'www')}">
                                                                                    <a style="color:#2a5cea;" target="_blank" href="${value}"> ${fn:trim(value)}<c:if test="${increment.count ne record.identifier.size()}">&#44;</c:if></a>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    ${fn:trim(value)}<c:if test="${increment.count ne record.identifier.size()}">&#44;</c:if>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Source-->
                                                            <c:if test="${not empty record.source && record.source ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.source"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.source}" var="value" varStatus="increment">
                                                                            <c:choose>
                                                                                <c:when test="${fn:startsWith(value, 'http')||fn:startsWith(value, 'www')}">
                                                                                    <a style="color:#2a5cea;" target="_blank" href="${value}">  ${fn:trim(value)}<c:if test="${increment.count ne record.source.size()}">&#44;</c:if></a>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    ${fn:trim(value)}<c:if test="${increment.count ne record.source.size()}">&#44;</c:if>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Language-->
                                                            <c:if test="${not empty record.language && record.language ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.language"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.language}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.language.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Relation-->
                                                            <c:if test="${not empty record.relation && record.relation ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.relation"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.relation}" var="value" varStatus="increment">
                                                                            <c:if test="${value ne null }">
                                                                                <c:choose>
                                                                                    <c:when test="${fn:startsWith(value, 'http')||fn:startsWith(value, 'www')}">
                                                                                        <a style="color:#2a5cea;" target="_blank" href="${value}">${fn:trim(value)}<c:if test="${increment.count ne record.relation.size()}">&#44;</c:if></a>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        ${fn:trim(value)}<c:if test="${increment.count ne record.relation.size()}">&#44;</c:if>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Coverage-->
                                                            <c:if test="${not empty record.coverage && record.coverage ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.coverage"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.coverage}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.coverage.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Rights-->
                                                            <c:if test="${not empty record.rights && record.rights ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.rights"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.rights}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.rights.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Audience-->
                                                            <c:if test="${not empty record.audience && record.audience ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.audience"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.audience}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.audience.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Provenance-->
                                                            <c:if test="${not empty record.provenance && record.provenance ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.provenance"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.provenance}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.provenance.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--ISBN-->
                                                            <c:if test="${not empty record.isbn && record.isbn ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.isbn"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.isbn}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.isbn.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--LCCN-->
                                                            <c:if test="${not empty record.lccn && record.lccn ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.lccn"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.lccn}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.lccn.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Edition-->
                                                            <c:if test="${not empty record.edition && record.edition ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.edition"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.edition}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.edition.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--ISSN-->
                                                            <c:if test="${not empty record.issn && record.issn ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.issn"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.issn}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.issn.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--Notes-->
                                                            <c:if test="${not empty record.notes && record.notes ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.notes"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.notes}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.notes.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--udcNotation-->
                                                            <c:if test="${not empty record.udcNotation && record.udcNotation ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.udc.notation"/></th>
                                                                    <td>
                                                                        <c:forEach items="${record.udcNotation}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.udcNotation.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                            <!--customNotation-->
                                                            <c:if test="${not empty record.customNotation && record.customNotation ne null}">
                                                                <tr class="col_head">
                                                                    <th class="text-right"><tags:message code="label.custom.notation"/> </th>
                                                                    <td>
                                                                        <c:forEach items="${record.customNotation}" var="value" varStatus="increment">
                                                                            ${fn:trim(value)}<c:if test="${increment.count ne record.customNotation.size()}">&#44;</c:if>
                                                                        </c:forEach>
                                                                    </td>
                                                                </tr>
                                                            </c:if>
                                                              <!--Data Warning Message-->  
                                                             <c:if test="${fn:contains(path, 'ncaa.gov.in')}">
                                                                <tr class="col_head">
                                                                    <th class="text-right" style="color: darkred;font-weight: bold;"><tags:message code="label.data.warning"/> </th>
                                                                    <td style="color: darkred;font-weight: bold;">
                                                                        This data is being served from National Cultural Audiovisual Archives (NCAA) : <a style="color:#2a5cea;" target="_blank" href="http://ncaa.gov.in/"> http://ncaa.gov.in/</a>
                                                                    </td>
                                                                </tr>
                                                            </c:if>   
                                                            <c:if test="${resourceType.resourceTypeCode eq 'VIRM'}">
                                                                <c:choose>
                                                                    <c:when test="${not empty record.threeDimensionalImagesPath && record.threeDimensionalImagesPath ne null}">
                                                                        <tr class="col_head">
                                                                            <th class="text-right"><tags:message code="label.anaglyph3d"/><img src="${context}/themes/images/3d_gallery.png"> </th>
                                                                            <td>
                                                                                <a id="viewAllJpeg" href="${context}/search/preview/all/jpeg/${record.recordIdentifier}?threeDimensional=true&language=${pageContext.response.locale}&fullTextFlag=${record.fullTextFlag}" class="card-link" target="_blank"><tags:message code="label.view.all.3dimages"/></a>
                                                                            </td>
                                                                        </tr>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <!--                                                                        <tr>
                                                                                                                                                    <th class="text-right"><tags:message code="label.anaglyph3d"/><img src="${context}/themes/images/3d_gallery.png"></th>
                                                                                                                                                    <td><tags:message code="label.imagenot.avaliable"/></td>
                                                                                                                                                </tr>-->
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:if>
                                                        </tbody>
                                                    </table>

                                                    <c:if test="${not empty record.lectureList && record.lectureList ne null}">
                                                        <table cellpadding="0" cellspacing="0" class="table table-bordered table-md table-responsive relatedLecture m-t-2 overflow330">
                                                            <tbody style="display: inline-table; width: 100%">
                                                                <tr>
                                                                    <th>Related Lectures</th>
                                                                </tr>
                                                                <c:forEach var="lectList" items="${record.lectureList}">
                                                                    <tr>
                                                                        <td>
                                                                            <a href="${context}/search/preview/${lectList.key}?language=${pageContext.response.locale}"> ${lectList.value}</a>
                                                                        </td>
                                                                    </tr>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                    </c:if>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="clear "></div>
                <!--    check is there additional information is present or not in case of artist profile-->
            </div>
        </div>
        <!--    check is there additional information is present or not in case of artist profile-->
        <c:set var="jpgCount" value="0"/>
        <c:if test="${not empty record.others && record.others ne null}">
            <c:forEach items="${record.others}" var="otherMetadata">
                <c:if test="${not empty otherMetadata.tagValues && otherMetadata.tagValues ne null}">
                    <c:if test="${fn:contains(otherMetadata.tagName, '.jpg')==true}">
                        <c:set var="jpgCount" value="${jpgCount+1}"/>
                    </c:if>
                </c:if>
            </c:forEach>
        </c:if>
        <c:if test="${(not empty record.others && record.others ne null)&&(record.others.size() ne jpgCount)}">
            <c:choose>
                <c:when test="${resourceType.resourceTypeCode eq 'BLOG'}">
                    <c:set var="isAdditionalInformation" value="false"/>
                </c:when>
                <c:otherwise>
                    <c:set var="isAdditionalInformation" value="true"/>
                </c:otherwise>
            </c:choose>
        </c:if>

        <c:if test="${marc21Record ne null}">
            <c:set var="isAdditionalInformation" value="true"/>
        </c:if>
        <!--check if portfolio is present or not-->
        <c:if test="${not empty record.others && record.others ne null}">
            <c:forEach items="${record.others}" var="otherMetadata">
                <c:if test="${not empty otherMetadata.tagValues && otherMetadata.tagValues ne null}">
                    <c:if test="${fn:contains(otherMetadata.tagName, '.jpg')==true}">
                        <c:if test="${fn:contains(otherMetadata.tagValues[0], 'Portfolio Image')==true}">
                            <c:set var="isPorfolio" value="true"/>
                        </c:if>
                    </c:if>
                </c:if>
            </c:forEach>
        </c:if>
        <c:if test="${(record.others ne null) ||(marc21Record ne null)||(isPorfolio=='true')||(record.metsXmlPath ne null)||(record.oreXmlPath ne null)}">
            <div>
                <div style="margin-bottom:0px;" class="clearfix"></div>
                <!-- tabs strats -->
                <c:if test="${isAdditionalInformation eq 'true'}">
                    <div class="cafgrd clearfix  bg-white col-lg-12 m-b-2">
                        <c:set var="counter" value="1"/>
                        <ul class="nav nav-tabs tabc font-weight-bold" id="myTab1">
                            <!---Tab-1--Addition Info----->
                            <c:if test="${(not empty record.others && record.others ne null)}">
                                <c:set var="ariaExpanded" value="false"/>
                                <c:set var="aclass" value="nav-link"/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="aclass" value="nav-link active"/>
                                    <c:set var="ariaExpanded" value="true"/>
                                </c:if>   
                                <li class="nav-item" id="additional-information"> <a class="${aclass}" data-target="#t${counter}" data-toggle="tab" aria-expanded="${ariaExpanded}"><tags:message code="sr.additional.information"/></a> </li>
                                    <c:set var="counter" value="${counter+1}"/>
                                </c:if>   


                            <!---Tab-2--MARC21 Info----->
                            <c:if test="${record.metadataType=='Marc21'}">
                                <c:if test="${not empty marc21Record && marc21Record ne null}">
                                    <c:set var="ariaExpanded" value="false"/>
                                    <c:set var="aclass" value="nav-link"/>
                                    <c:if test="${counter eq 1}">
                                        <c:set var="aclass" value="nav-link active"/>
                                        <c:set var="ariaExpanded" value="true"/>
                                    </c:if>
                                    <li class="nav-item"> <a class="${aclass}" data-toggle="tab" data-target="#t${counter}" aria-expanded="${ariaExpanded}"><tags:message code="sr.mar21.information"/></a> </li>
                                        <c:set var="counter" value="${counter+1}"/>
                                    </c:if>
                                </c:if>
                            <!---Tab-3--Portfolio----->
                            <c:if test="${(isPorfolio=='true')}">
                                <c:set var="aclass" value="nav-link"/>
                                <c:set var="ariaExpanded" value="false"/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="aclass" value="nav-link active"/>
                                    <c:set var="ariaExpanded" value="true"/>
                                </c:if>
                                <li class="nav-item" id="porfolio"> <a class="${aclass}" data-toggle="tab" data-target="#t${counter}" aria-expanded="${ariaExpanded}"><tags:message code="sr.portfolio"/> </a> </li>
                                    <c:set var="counter" value="${counter+1}"/>
                                </c:if>
                            <!---Tab-4--METS Information----->
                            <c:if test="${record.metsXmlPath ne null}">
                                <c:set var="aclass" value="nav-link"/>
                                <c:set var="ariaExpanded" value="false"/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="aclass" value="nav-link active"/>
                                    <c:set var="ariaExpanded" value="true"/>
                                </c:if>
                                <li class="nav-item"> <a class="${aclass}" data-toggle="tab" data-target="#t${counter}" aria-expanded="${ariaExpanded}"><tags:message code="sr.mets.information"/> </a> </li>
                                    <c:set var="counter" value="${counter+1}"/>
                                </c:if>

                            <!---Tab-5--ORE Information----->
                            <c:if test="${record.oreXmlPath ne null}">
                                <c:set var="aclass" value="nav-link"/>
                                <c:set var="ariaExpanded" value="false"/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="aclass" value="nav-link active"/>
                                    <c:set var="ariaExpanded" value="true"/>
                                </c:if>
                                <li class="nav-item"> <a class="${aclass}" data-toggle="tab" data-target="#t${counter}" aria-expanded="${ariaExpanded}"><tags:message code="sr.ore.information"/></a> </li>
                                </c:if>
                                <c:set var="counter" value="1"/>
                        </ul>
                        <div class="tab-content">
                            <!-- 1st tab starts  -->
                            <c:if test="${isAdditionalInformation eq 'true'}">
                                <c:if test="${(not empty record.others && record.others ne null)}">
                                    <c:set var="divActive"  value=""/>
                                    <c:if test="${counter eq 1}">
                                        <c:set var="divActive"  value="active"/>
                                    </c:if>
                                    <div class="tab-pane nopad ${divActive} overflow750" id="t${counter}">
                                        <div class="">
                                            <table cellpadding="0" cellspacing="0" class="table table-bordered">
                                                <tbody>
                                                    <c:forEach items="${record.others}" var="otherMetadata">
                                                        <c:if test="${not empty otherMetadata.tagValues && otherMetadata.tagValues ne null}">
                                                            <c:if test="${not empty otherMetadata.tagValues[0]}">
                                                                <c:if test="${fn:contains(otherMetadata.tagName, '.jpg')==false}">
                                                                    <tr class="col_head">
                                                                        <th  width="140" class="text-right">${otherMetadata.tagName}</th>
                                                                        <td>
                                                                            <c:forEach items="${otherMetadata.tagValues}" var="value" varStatus="increment">
                                                                                <c:choose>
                                                                                    <c:when test="${fn:startsWith(value, 'http')||fn:startsWith(value, 'www') || fn:startsWith(value, 'https')}">
                                                                                        <a style="color:#2a5cea;" target="_blank" href="${value}">${fn:trim(value)}<c:if test="${increment.count ne otherMetadata.tagValues.size()}">&#44;</c:if>
                                                                                            </a>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        ${fn:trim(value)}<c:if test="${increment.count ne otherMetadata.tagValues.size()}">&#44;</c:if>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </c:forEach>
                                                                        </td>
                                                                    </tr>
                                                                </c:if>
                                                            </c:if>
                                                        </c:if>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <c:set var="counter" value="${counter+1}"/>
                                </c:if>  
                            </c:if>
                            <!-- 1st tab ends -->
                            <!-- 2nd tab content starts -->
                            <c:if test="${not empty marc21Record && marc21Record ne null}">
                                <!--Marc-->
                                <c:set var="divActive"  value=""/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="divActive"  value="active"/>
                                </c:if>
                                <div class="tab-pane nopad ${divActive}" id="t${counter}">
                                    <div class="m-a-2">
                                        <table cellpadding="0" cellspacing="0" class="table table-bordered table-responsive">
                                            <tbody style="display: inline-table; width: 100%">
                                                <tr class="col_head">
                                                    <th  width="140" class="text-right">LEADER</th>
                                                    <td colspan="5">${marc21Record.leader.value}</td>
                                                </tr>
                                                <tr class="col_head table-active">
                                                    <th class="text-right" width="140"><tags:message code="label.Tag"/></th>
                                                    <th><tags:message code="label.subfield"/></th>
                                                    <th><tags:message code="label.description"/></th>
                                                    <th><tags:message code="label.ind1"/></th>
                                                    <th><tags:message code="label.ind2"/></th>
                                                    <th><tags:message code="label.value"/></th>
                                                </tr>
                                                <c:forEach items="${marc21Record.controlfield}" var="controlfield">
                                                    <c:if test="${not empty controlfield.value && controlfield.value ne null}">
                                                        <tr class="col_head">
                                                            <th width="140" class="text-right">${controlfield.tag}</th>
                                                            <td>---</td>
                                                            <c:set var="key" value="${controlfield.tag}"/>
                                                            <td>${marc21TitleMap.get(key)}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>${controlfield.value}</td>
                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                                <c:forEach items="${marc21Record.datafield}" var="datafield">
                                                    <c:if test="${not empty datafield.subfield && datafield.subfield ne null}">
                                                        <c:forEach items="${datafield.subfield}" var="subfield">
                                                            <c:if test="${not empty subfield.value && subfield.value ne null}">
                                                                <tr class="col_head">
                                                                    <th  width="140" class="text-right">${datafield.tag}</th>
                                                                    <td>${subfield.code}</td>
                                                                    <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                    <td>${marc21TitleMap.get(key)}</td>
                                                                    <td>${datafield.ind1}</td>
                                                                    <td>${datafield.ind2}</td>
                                                                    <td>${subfield.value}</td>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <c:set var="counter" value="${counter+1}"/>
                            </c:if>
                            <!-- 2nd tab ends -->
                            <!-- 3rd tab content starts -->
                            <c:if test="${(isPorfolio=='true')}">
                                <c:set var="divActive"  value=""/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="divActive"  value="active"/>
                                </c:if>
                                <div class="tab-pane nopad bg-grgeyf6 ${divActive}" id="t${counter}">
                                    <div class="m-a-1">
                                        <!-- portfolio -->
                                        <div class="documentList clearfix">
                                            <div class="scroll_list  ">
                                                <div class="p-r-1">
                                                    <c:if test="${not empty record.others && record.others ne null}">
                                                        <c:forEach items="${record.others}" var="otherMetadata">
                                                            <c:if test="${not empty otherMetadata.tagValues && otherMetadata.tagValues ne null}">
                                                                <c:if test="${fn:contains(otherMetadata.tagName, '.jpg')==true}">
                                                                    <c:if test="${fn:contains(otherMetadata.tagValues[0], 'Portfolio Image')==true}">
                                                                        <div class="col-xs-12 col-lg-2 col-md-3 col-sm-6 documentItem">
                                                                            <div class="card">
                                                                                <div class="card-img-top">
                                                                                    <span class="vimg_c">
                                                                                        <c:if test="${not empty record.paths && record.paths ne null}">
                                                                                            <c:forEach items="${record.paths}" var="pathLoc">
                                                                                                <c:if test="${fn:contains(pathLoc, otherMetadata.tagName)}">
                                                                                                    <img src="${pathLoc}"  class="center-block"/>
                                                                                                </c:if>
                                                                                            </c:forEach>
                                                                                        </c:if>
                                                                                    </span>
                                                                                </div>
                                                                                <h4 class="card-title">${otherMetadata.tagValues[1]}</h4>
                                                                                <p class="hidden60">${otherMetadata.tagValues[2]}</p>
                                                                            </div>
                                                                        </div>
                                                                    </c:if>
                                                                </c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                    <!-- ends here scroll -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <c:set var="counter" value="${counter+1}"/>
                                    <div class="clear"></div>
                                    <!-- /portfolio -->
                                </div>
                            </c:if>
                            <!-- 3rd tab content ends -->
                            <!-- 4th tab content starts -->
                            <c:if test="${record.metsXmlPath ne null}">
                                <c:set var="divActive"  value=""/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="divActive"  value="active"/>
                                </c:if>
                                <div class="tab-pane nopad ${divActive}" id="t${counter}">
                                    <c:set var="MetsXMLDisplay"  value="true"/>
                                    <div id="XMLHolderMets" class="xmlHolder"></div>
                                    <c:set var="counter" value="${counter+1}"/>
                                    <div class="clear"></div>
                                </div>
                            </c:if>
                            <!-- 4th tab content ends -->
                            <!-- 5th tab content starts -->
                            <c:if test="${record.oreXmlPath ne null}">
                                <c:set var="divActive"  value=""/>
                                <c:if test="${counter eq 1}">
                                    <c:set var="divActive"  value="active"/>
                                </c:if>
                                <div class="tab-pane nopad ${divActive}" id="t${counter}">
                                    <c:set var="OreXMLDisplay"  value="true"/>
                                    <div id="XMLHolderOre" class="xmlHolder"></div>
                                    <c:set var="counter" value="${counter+1}"/>
                                    <div class="clear"></div>
                                </div>
                            </c:if>
                            <!-- 5th tab content ends -->
                        </div>
                    </div>
                </c:if>
                <!-- tab ends -->
            </div>
            <%--</c:if>--%>
        </c:if>
        <!-- /add info -->   

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
            <div class="card m-t-2" id="avarage-rating-main-div">
                <div class=" clearfix bg-greyf6 " >
                    <div class="card-header"><tags:message code="label.average.rating"/></div>
                    <div class="" >
                        <div class="card-block average_rating">
                            <div class="col-xs-12 col-md-12 col-xl-4 text-center m-b-1">
                                <h1 class="rating-num" ><span id="avarageRating"></span></h1>
                                <div class="rating" id="showRatingStars">
                                </div>
                                <div> <span class="fa fa-user"></span><span id="totalRaingCount"></span> total </div>
                            </div>
                            <div class="col-xs-12 col-md-12 col-xl-8">
                                <div class="row rating-desc" id="ratingstatistics">
                                </div>
                                <!-- end row -->
                            </div>
                            <div class="clearfix"></div>         
                        </div>            
                    </div>
                </div>
            </div>
            <c:if test="${not empty user}">
                <div class="card m-t-1"  id="writeReviewDiv" >
                    <div class=" clearfix bg-greyf6 " >
                        <div class="card-header" id="reviewt"><tags:message code="label.review.msg"/></div>
                        <div class="">
                            <div class="card-block average_rating">
                                <div class="clearfix"></div>
                                <div class="m-b-1 m-t-0"> <span class="m-r-1"><strong><tags:message code="label.rate"/>:</strong>
                                        <span class="rateit" id="rateit5" data-rateit-min="0" data-rateit-max="5" data-rateit-step="1" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-readonly="false" data-rateit-value="${myReview.recordRating}">
                                            <button id="rateit-reset-2" type="button" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
                                        </span></div>
                                <input id="ratings-hidden" name="rating" type="hidden">
                                <textarea class="form-control animated" cols="50" id="reviewTxt" name="comment" placeholder="Enter your review here..." rows="7" ></textarea>
                            </div>
                            <div class="card-footer text-center"> <a href="javascript:;" id="postReviewAnchor" class="btn btn-primary"><tags:message code="label.publish.review"/></a> </div>
                        </div>
                    </div>
                </div>
            </c:if>
        </div>
        <!-- review -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
            <div class="card m-t-2">
                <div class="review">
                    <div class="card-header"><tags:message code="label.review.ratings"/><span id="review-rating-count-div"></span> </div>
                    <div class="ui-icon-newwin overflow500" id="reviewc">
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </c:if>
        <c:if test="${ empty record && record eq null}">
            <div class="alert alert-warning"><tags:message code="sr.no.preview.msg"/></div>
        </c:if>
    </div>
    <!--review ends-->
</div>
<div style="margin-bottom:30px;" class="clearfix"></div>

<!--citation modal-->
<div id="citation_link" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridModalLabel">Cite</h4>
            </div>
            <div class="modal-body">
                <div class="card-block">
                    <p><tags:message code="sr.p.msg"/></p>
                    <div>
                        <table>
                            <tbody id="citation-map">
                                <%--<c:forEach items="${citationMap}" var="citationObj">
                                    <tr>
                                        <th class="citation_th" scope="row">${citationObj.key}</th>
                                        <td class="citation_td"><div>${citationObj.value}</div></td>
                                    </tr>
                                </c:forEach>--%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="add_to_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="addToListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="dashboard.add.to.list"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"></label>
                    <div class="col-lg-4 col-md-4 col-sm-12"><select id="addToListSelect" size="5" class="form-control col-lg-4 col-md-4 col-sm-12"> </select></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <input type="button" id="AddToExistingListBtn" class="btn btn-primary" value="Add to List"/>
                <input type="button" id="resetBtn1" class="btn btn-secondary" value="Reset"/>
            </div>
        </div><!-- /.modal-content -->
        <div id="createNewListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="label.create.new.list"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="label.list.name"/></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" id="newListTxt" class="form-control"/>
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="label.visibility"/></label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input type="radio" name="accessRadio" value="0"/> <tags:message code="label.public"/> <input type="radio" name="accessRadio" value="1" checked/> <tags:message code="label.private"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" value="" />
                <input type="submit" id="addToNewListBtn" class="btn btn-primary" value="Add to List"/>
                <input type="button" id="backBtn" class="btn btn-primary" value="Back" />
                <input type="reset" id="resetBtn2" class="btn btn-secondary" value="Reset"/>
            </div>
        </div><!-- /.modal-content -->
        <div id="successDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="share.success"/> </h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" role="alert"><tags:message code="label.added.successfully"/> </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="label.close"/></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="nvli_share" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div id="shareWithNvliDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="share.modal.title"/></h4>
            </div>
            <div class="modal-body">
                <div>
                    <!--for Link-->
                    <div class="form-group row clearfix">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="share.link"/> :</label>
                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <div id="sharedLinkLabel" style="margin-top:5px;"></div>
                        </div>
                    </div>
                    <!--for Message-->
                    <div class="form-group row clearfix">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="share.message"/> :</label>
                        <div class="col-lg-9 col-md-8 col-sm-12">
                            <textarea id="userMsgTxt" class="form-control"  maxlength="150" placeholder="<tags:message code="share.message.placeholder"/>"></textarea>
                        </div>
                    </div>
                    <!--for To:-->
                    <div class="form-group row clearfix">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="share.to"/> :</label>
                        <div id="searchFriendsTxtDiv" class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                            <input id="searchFriendsTxt" class="form-control" placeholder="<tags:message code="share.to.placeholder"/>" autocomplete="off" type="text">
                        </div>
                    </div>
                    <!-- for labels-->
                    <div  class="form-group row clearfix">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"></label>
                        <div id="label-container" class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                            <!--user labels go here-->
                        </div>
                    </div>
                    <!-- for suggestions-->
                    <div class="form-group row clearfix" id="suggestionContainer" >
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"></label>
                        <div class="col-lg-9 col-md-8 col-sm-12  col-xs-12">
                            <div  class="clearfix bg-faded" style="border:1px solid #ccc;">
                                <div class="overflow420">
                                    <div>
                                        <div id="container-user-list" class="btn-group interest_tab" >
                                            <!--user suggestions go here-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- for no suggestions-->
                    <div class="form-group row" id="noSuggestionContainer" style="text-align: center;margin: 5px">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"></label>
                        <div class="col-lg-9 col-md-8 col-sm-12  col-xs-12">
                            <div  class="clearfix bg-faded" style="border:1px solid #ccc;">
                                <label class="text-danger"><tags:message code="share.nosuggestion"/></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" >
                <span class="pull-right">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="button" id="shareWithNvliFriendsBtn" class="btn btn-primary" value="<tags:message code="share.button"/>"/>
                    <input type="button" id="InviteFriendsBtn" class="btn btn-secondary" value="<tags:message code="invite.label"/>"/>
                </span>
            </div>
        </div><!-- /.modal-content -->
        <div id="inviteFriendsDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="invite.modal.title"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-3 col-md-4 col-sm-4 text-right"><tags:message code="invite.label"/> :</label>
                    <div class="col-lg-7 col-md-4 col-sm-8">
                        <input type="text" class="form-control" id="inviteToTxt" placeholder="<tags:message code="invite.email.placeholder"/>"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="pull-right">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="button" id="sendInviteBtn" class="btn btn-primary" value="<tags:message code="invite.label"/>"/>
                    <input type="button" id="backToShareBtn" class="btn btn-secondary" value="<tags:message code="back.button"/>"/>
                </span>
            </div>
        </div><!-- /.modal-content -->
        <div id="shareSuccessDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="share.success"/></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" role="alert"><tags:message code="share.successfully"/></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="share.close.button"/></button>
            </div>
        </div><!-- /.modal-content -->
        <div id="inviteSentDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><tags:message code="share.success"/></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" role="alert"><tags:message code="invite.send"/></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="share.close.button"/></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/template" id="record_reviews">
    <div class="media">
    <a class="media-left col-lg-1 col-md-2 col-sm-3 col-xs-12" href="javascript:;"> 
    <img class="img-fluid media-object" src="${exposedProps.userImageBase}/{{=userProfilePicPath}}" onerror="this.src='${context}/images/profile-picture.png'" style="width:130%"/></a>
    <div class="media-right">
    <span  class="rateit m-r-1" 
    data-rateit-min="0" data-rateit-max="5" data-rateit-value="{{=ratingValue}}" 
    data-rateit-ispreset="true" data-rateit-readonly="true">
    </span></br>
    <p> {{=reviewText}}</p>
    <div class="m-t-1">
    <h6 class="text-muted">
    <i class="fa fa-user"></i> &nbsp;{{=username}}  &nbsp;&nbsp;&nbsp;<i class="fa fa-calendar"></i>
    {{= $.format.date(reviewAddedTime, " E dd MMM yyyy") }} 
    </h6>
    </div>
    </div>
    </div>   
</script>
<script type="text/template" id="ratings-statistics-template">
    <div class="col-xs-3 col-md-2 text-right"> {{=ratingValue}} <span class="fa fa-star"></span> </div>
    <div class="col-xs-8 col-md-10">
    <a href="javascript:;">
    <div class="progress">
    <div id="check-if-no-rating" class="progress-bar {{=ratingCss}}" role="progressbar" aria-valuenow="{{=ratingWidth}}"
    aria-valuemin="0" aria-valuemax="100" style="width: {{=ratingWidth}}%"> <span class="sr-only">{{=ratingWidth}}%</span> </div>
    </div>
    </a>
    </div>
</script>
<script type="text/template" id="user_saved_list">
    <option value='{{=listName}}'>{{=listName}}-{{=listVisibility}}</option>
</script>
<script type="text/template" id="citation_template">
    <tr>
    <th class="citation_th" scope="row">{{=citationObjKey}}</th>
    <td class="citation_td"><div>{{=citationObjValue}}</div></td>
    </tr>
</script>
<script type="text/template" id="tpl-nvlishare-label">
    <label id="user-{{=labelId}}" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;max-width: 500;cursor:default;"> {{=labelName}} <span onclick="deleteUsernameFromList( '{{=labelId}}');" title="{{=labelName}}" class="fa fa-remove nvliShare-close-btn" style="cursor:pointer;"/></label>
</script>
<script type="text/template" id="tpl-user-item">
    <div id="user-{{=id}}" class="col-lg-6 col-md-12 col-sm-12  col-xs-12 user-selected" user-id="{{=id}}" user-name="{{=name}}">
    <div class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;"> <a class="pull-left" href="javascript:;"> <img class="media-object img-thumbnail " src= "{{=imageURL}}" onerror="this.src='{{=altImageURL}}'" style="width: 50px;height:50px;"> </a>
    <div class="media-body">
    <div class="m-l-1">
    <h5 class="media-heading">{{=name}} </h5>
    <small class="font12"><i class="fa fa-map-marker" aria-hidden="true"></i><span> {{=city}} {{=state}} {{=country}} </span></small>
    </div>
    </div>
    </div>
    </div>
</script>
<!--templates for paymnet detail service starts here-->
<script type="text/template" id="tpl-item-detail">
    <hr>
    <table cellpadding="0" cellspacing="0" class="table table-bordered table-md">
    <thead id="tblHead">
    <tr>
    <th width="160"></th>
    <th>Item Name</th>
    <th width="100">Price</th>
    </tr>
    </thead>
    <tbody >
    <tr class="col_head">
    <td>
    <img src="${context}/themes/images/logo/NVLI-Flat-100.png"/>
    </td>
    <td>{{=itemtName}}</td>
    <td>Rs. {{=itemPrice}}</td>
    </tr>
    </tbody>
    </table>
</script>
<script type="text/template" id="tpl-payment-type">
    <tr class="col_head">
    <th class="text-right">Payment Type</th>
    <td>
    {{ if(isRecordPriceAvailable) { }}
    <label class="radio-inline" style="text-align: justify">
    <input type="radio" name="chk-item-type" data-item-type="isRecord" data-item-id="{{=recordIdentifier}}"><span class="label label-primary">Rs.{{=recordPrice}}</span> for this record
    </label>
    {{ } }}
    {{ if(isResourcePriceAvailable) { }}
    <label class="radio-inline" style="text-align: justify">
    <input type="radio" name="chk-item-type" data-item-type="isResource" data-item-id="{{=resourceCode}}"><span class="label label-primary">Rs.{{=resourcePrice}}</span> for complete resource
    </label>
    {{ } }}
    </td>
    </tr>
</script>
<script type="text/template" id="tpl-payment-status">
    <div class="alert {{=alertStatus}} fade in">
    <a href="javascript:;" class="close" data-dismiss="alert">&times;</a>
    <strong>{{=paymentStatus}}!</strong>{{=message}}<a href="${context}/paymentws/record-file-list/${record.recordIdentifier}" target="_blank"> click here</a>.
    </div>
</script>
<script type="text/template" id="tpl-item-detail-footer">
    <div class="modal-footer text-center">
    <button type="button" class="btn btn-primary" id="btn-item-pay-now" data-item-id="{{=itemtId}}" data-item-type="{{=itemType}}">Pay Now</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
    </div>
</script>
<script type="text/template" id="tpl-buy-btn">
    <li>
    <button type="button" class="btn btn-primary" id="btn-buy-item">Buy Now</button>
    </li>
</script>
<script type="text/template" id="tpl-view-status">
    <div class="alert alert-info fade in">
    <a href="javascript:;" class="close" data-dismiss="alert">&times;</a>
    <a href="${context}/paymentws/record-file-list/${record.recordIdentifier}" target="_blank"> Click here </a>{{=message}}.
    </div>
</script>
<script type="text/template" id="tpl-show-rating-stars">
    <span  class="rateit m-r-1" id="rateit6" data-rateit-min="0" data-rateit-max="5" data-rateit-value="{{=ratingValue}}"  data-rateit-ispreset="true" data-rateit-readonly="true">
    </span>
</script>
<!--templates for paymnet detail service ends here-->
<script type="text/javascript">
    var myRating;
    var iv1;
    var pageNumber;
    var pageWindow = 10;
    var processing = false;
    var hasNext = true;
    var selectedFilePath;
    var recordIdentifier = '${record.recordIdentifier}';
    var recordTitle = '${record.nameToView}';
    var userDisikeFlag = '${userLikeFlag}';
    var likeProcessing = false;
    var userRatingFlag = '${userRatingFlag}';
    var loc = window.location.href;
    var avgRecRating = 0;
    var numberOfRatings = '${record.ratingCount}';
    var numberOfReviews = '${record.recordNoOfReview}';
    var existingCustomTags = '${record.customNotation.toString().toLowerCase()}';
    existingCustomTags = existingCustomTags.replace("[", "").replace("]", "").split(/[\s,]+/);
    var userSuggesstionJson = {};
    var resultsResponse;
    var shareMap = {};
    var recordType;
    function printOb() {
        window.print();
    }
    $(document).ready(function () {
        pageNumber = 1;
        $("#reviewc").empty();
        populateReviews();
        $("#reviewc").scroll(function (e) {
            if (processing)
                return false;
            if (hasNext && $("#reviewc").scrollTop() >= ($("#reviewc").height() - $("#reviewc").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch notifications
                populateReviews();
            }
        });
        userRatingStatistics();
        var crowdsourceFailReason = '${crowdsourceFailReason}';
        if (crowdsourceFailReason.trim() !== '' && crowdsourceFailReason.trim().length > 0) {
            alert(crowdsourceFailReason);
        }
        setLanguage("selectCustomLanguage", "customAutocomplete");
        /*******Chiatanya JS **************/
        $(".arrow-down").hide();
        $(".arrow-down1").hide();
        $(".card-header").click(function () {
            // $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
        });
        $(".accordion-head1").click(function () {
            // $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up1, .arrow-down1").toggle();
        });
        /************Chaitanya JS end************/
        $('#fullSize').hide();
        $('#fullSizeId').show();
        $('#minSizeId').hide();
        $('#openDownload').hide();
        $('#viewAllTif').hide();
        $('#viewAllJpeg').hide();
        $('#viewAllMp3Mp4').hide();
        $('#porfolio').hide();
        $('#viewer-sidebar').hide();
        $('#additional-information').hide();
        if ('${isPorfolio}' === 'true') {
            $('#porfolio').show();
        }
        if ('${isAdditionalInformation}' === 'true') {
            $('#additional-information').show();
        }
        if ('${MetsXMLDisplay}' === 'true') {
            var metsXmlFilePath = '${record.metsXmlPath}'.replace(/\//g, "$");
            metsXmlFilePath = metsXmlFilePath.replace(".", "||");
            LoadXML('XMLHolderMets', "${pageContext.request.contextPath}/search/preview/showXMLFile/" + metsXmlFilePath + "/mets");
        }
        if ('${OreXMLDisplay}' === 'true') {
            var oreXmlFilePath = '${record.oreXmlPath}'.replace(/\//g, "$");
            oreXmlFilePath = oreXmlFilePath.replace(".", "||");
            LoadXML('XMLHolderOre', "${pageContext.request.contextPath}/search/preview/showXMLFile/" + oreXmlFilePath + "/ore");
        }

        var fileName = '${record.originalFileName}'.toLowerCase();
        if (fileName.length === 0) {
            $('#cut_viewer').show();
            var displayHTML = "<img src='${context}/themes/images/thumb/no_image_thumb.jpg' width='100%' height='100%'/>";
            $('#cut_viewer').html(displayHTML);
        }

        if (fileName.indexOf(".tif") !== -1 || fileName.indexOf(".tiff") !== -1) {
            $('#fullSize').hide();
            $('#viewAllTif').show();
            $('#viewAllJpeg').hide();
            $('#viewAllMp3Mp4').hide();
            renderTiff();
            setTypeOfRecord(0);
        } else if (fileName.indexOf(".jpg") !== -1 || fileName.indexOf(".jpeg") !== -1 || fileName.indexOf(".gif") !== -1 || fileName.indexOf(".png") !== -1) {
            $('#fullSize').hide();
            $('#fullscreen').attr("href", "${path}");
            $('#viewAllTif').hide();
            $('#viewAllJpeg').show();
            $('#viewAllMp3Mp4').hide();
//            $('#imageTagging').addClass('link3');
//            $('#imageTagging').removeClass('disabled');
//            $('#imageTagging').attr("href", "${context}/search/view/image-tags/${record.recordIdentifier}?language=${pageContext.response.locale}");
//                        $('#imageTagging').attr("target", "_blank");
                        renderFile();
                        setTypeOfRecord(0);
                    } else if (fileName.indexOf(".pdf") !== -1) {
                        var displayHTML = "<object class='videop object' data='" + "${path}" + "' type='application/pdf' width='100%' height='100%'></object>";
                        $(function () {
                            $('#fullSize').show();
                            $('#viewAllTif').hide();
                            $('#viewAllJpeg').hide();
                            $('#viewAllMp3Mp4').show();
                            $('#cut_viewer').html(displayHTML);
                        });
                        setTypeOfRecord(1);
                    } else if (fileName.indexOf(".mp4") !== -1 || fileName.indexOf(".mpg") !== -1) {
                            var displayHTML = "<div class='videop marginauto'><div class='video_innerbox1'> <video oncontextmenu='return false;' controls controlsList='nodownload'><source src='" + "${path}" + "' type='video/mp4'>Your browser does not support the video tag.</video></div></div>";
                        $(function () {
                            $('#fullSize').hide();
                            $('#viewAllTif').hide();
                            $('#viewAllJpeg').hide();
                            $('#viewAllMp3Mp4').show();
                            $('#cut_viewer').html(displayHTML);
                        });
                        setTypeOfRecord(2);
                    } else if (fileName.indexOf(".mp3") !== -1 || fileName.indexOf(".wav") !== -1) {
                        var displayHTML = " <div class='audiop marginauto'><div class='audio_innerbox'><audio controls><source src='" + "${path}" + "' type='audio/mpeg'></audio></div></div>";
                        $(function () {
                            $('#fullSize').hide();
                            $('#viewAllTif').hide();
                            $('#viewAllJpeg').hide();
                            $('#viewAllMp3Mp4').show();
                            $('#cut_viewer').html(displayHTML);
                        });
                        setTypeOfRecord(3);
                    } else if (fileName.indexOf(".xml") !== -1) {
                        var displayHTML = "<object class='videop object' data='" + "${path}" + "' type='application/xml' width='100%' height='100%'></object>";
                        $(function () {
                            $('#fullSize').hide();
                            $('#viewAllTif').hide();
                            $('#viewAllJpeg').hide();
                            $('#viewAllMp3Mp4').hide();
                            $('#cut_viewer').html(displayHTML);
                        });
                        setTypeOfRecord(1);
                    } else if (fileName.indexOf(".html") !== -1 || fileName.indexOf(".htm") !== -1) {
                        var displayHTML = "<iframe src=" + encodeURI('${path}') + " width='700' height='650' allowfullscreen='allowfullscreen' webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>";
                        $(function () {
                            $('#fullSize').hide();
                            $('#viewAllTif').hide();
                            $('#viewAllJpeg').hide();
                            $('#viewAllMp3Mp4').hide();
                            $('#cut_viewer').html(displayHTML);
                        });
                        setTypeOfRecord(1);
                    }

                    if (userDisikeFlag === "true") {
                        $("#likeAnchor").html("<i class='fa fa-thumbs-up'></i>Liked");
                        //$("#likeAnchor").attr("disabled", "disabled");
                        //("#likeAnchor").css("color", "grey").css("cursor", "default");
                    }

                    // Binding SocialShare
                    $("#share").jsSocials({
                        shares: [{
                                share: "facebook",
                                logo: __NVLI.context + "/images/social_share_logos/facebook-logo.png",
                                title: $.trim($("meta[property='og:title']").attr("content"))
                            }, {
                                share: "twitter",
                                text: $.trim($("meta[property='og:title']").attr("content")),
                                via: "National Virtual Library of India",
                                hashtags: "nvliShare",
                                logo: __NVLI.context + "/images/social_share_logos/twitter-logo.png"
                            }, {
                                share: "linkedin",
                                logo: __NVLI.context + "/images/social_share_logos/linkedin-logo.png",
                                title: $.trim($("meta[property='og:title']").attr("content")),
                                summary: $.trim($("meta[property='og:description']").attr("content")),
                                source: $.trim($("meta[property='og:site_name']").attr("content"))
                            }, {
                                share: "googleplus",
                                logo: __NVLI.context + "/images/social_share_logos/google-plus-logo.png"
                            }, {
                                share: "nvli",
                                logo: __NVLI.context + "/images/social_share_logos/nvli-logo.png"
                            }],
                        url: loc,
                        showLabel: true,
                        showCount: false,
                        on: {
                            click: function (e) {
                                if (this.share === "nvli") {
                                    nvliShare();
                                    $('#suggestionContainer').attr("hidden", "true");
                                    $('#noSuggestionContainer').attr("hidden", "true");
                                } else {
                                    socialshare(this.providerId);
                                }
                            }
                        }
                    });
                    $("#emailshare").jsSocials({
                        shareIn: "popup",
                        shares: ["email"],
                        text: "National Virtual Library of India",
                        desc: "hi,\nPlease click on the below link to access the record.If that does not work ,then copy paste the link into the browser.\n\n",
                        showLabel: true,
                        showCount: false
                    });
                    if ($('#isAttachedArticleListEmpty').val() !== "1") {
                        $('#related-article-div').remove();// remove Related Article block if their is no related articles.
                    }
                    populateCitation();
                    usersSuggestions();
                    $("#fullSizeId").click(function () {
                        $('#fullSizeId').hide();
                        $('#minSizeId').show();
                    });
                    $("#minSizeId").click(function () {
                        $('#fullSizeId').show();
                        $('#minSizeId').hide();
                    });
                    checkForPaidRecord();
                });
                $("#rateit5").bind('rated', function (event, value) {
                    userRating(value);
                });
                $("#rateit5").bind('over', function (event, value) {
                    $(this).attr('title', value);
                });
                //A string replace function
                function str_replace(haystack, needle, replacement) {
                    var temp = haystack.split(needle);
                    return temp.join(replacement);
                }
                $(".localisation").click(function () {
                    document.getElementById($(this).attr("id")).href = "?language=" + $(this).attr("id");
                    languagePref = $(this).attr("id");
                });
                /*
                 *  Renders tiff file in a img tag.
                 *  recordIdentifier: Record Identifier
                 *
                 */
                function renderTiff() {
                    selectedFilePath = "${pageContext.request.contextPath}/search/preview/renderTiff/" + recordIdentifier;
                    $(function () {
                        $('#cut_viewer').show();
                        var iv1 = $("#cut_viewer").iviewer({
                            src: selectedFilePath
                        });
                        $('#viewer-sidebar').show();
                        iv1.iviewer('loadImage', selectedFilePath);
                    });
                }
                /*
                 *  Renders jpg file in a img tag.
                 *
                 */
                function renderFile() {
                    selectedFilePath = "${path}";
                    $(function () {
                        //            $('#loading').show();
                        //        $('#loading').attr('src','{pageContext.request.contextPath}/themes/images/loading.gif');
                        $('#cut_viewer').show();
                        $('#viewer-sidebar').show();
                        var iv1 = $("#cut_viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('loadImage', selectedFilePath);
                    });
                }
                function copyUrl() {
                    var clipboard = new Clipboard('#copyUrl', {
                        text: function () {
                            return loc;
                        }
                    });
                    clipboard.on('success', function (e) {
                        console.log(e);
                    });
                    clipboard.on('error', function (e) {
                        console.log(e);
                    });
                    $('#copyUrl').html("<i class='fa fa-external-link'></i>URL Copied");
                }
                function citation() {
                    $('#citation_link').modal('show');
                }
                function openDownload() {
                    $('#openDownload').show();
                }

                function sendNotifications(notificationActivityType, uniqueObjectIdentifier, message) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/user/notification/" + uniqueObjectIdentifier + "/" + notificationActivityType + "/" + message,
                        cache: false,
                        success: function (jsonObj) {
                        },
                        error: function () {
                            alert("Notification Service not available.Please try after some time.");
                        }
                    });
                }

                function likeActivity(e) {
                    if (likeProcessing) {
                        return;
                    }
                    likeProcessing = true;
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/user/like/" + recordIdentifier + "/" + userDisikeFlag + "/" + recordType,
                        cache: false,
                        success: function (jsonobj) {
                            if (jsonobj.message === "disliked") {
                                $("#likeAnchor").html("<i class='fa fa-thumbs-o-up'></i>Like");
                                userDisikeFlag = "false";
                            } else if (jsonobj.message === "liked") {
                                $("#likeAnchor").html("<i class='fa fa-thumbs-up'></i>Liked");
                                userDisikeFlag = "true";
                                sendNotifications(jsonobj.notificationActivityType, jsonobj.uniqueObjectIdentifier, jsonobj.message);
                            } else {
                                alert("Error Performing Like.Please try after some time.");
                            }
                        },
                        error: function () {
                            alert("Like Service not available.Please try after some time.");
                        },
                    }).done(function () {
                        likeProcessing = false;
                    });
                }

                function usersSuggestions() {
                    $('#searchFriendsTxt').devbridgeAutocomplete({
                        paramName: 'username',
                        minChars: 2,
                        serviceUrl: __NVLI.context + '/search/user/usersSuggestions',
                        scroll: true,
                        scrollHeight: 100,
                        showNoSuggestionNotice: false,
                        onSearchComplete: function (query, suggestions) {
                            $(".autocomplete-suggestions").hide();
                            if (suggestions.length === 0) {
                                $('#suggestionContainer').attr("hidden", "true");
                                $('#noSuggestionContainer').removeAttr("hidden");
                            }
                            if (suggestions.length > 0) {
                                $('#suggestionContainer').removeAttr("hidden");
                                $('#noSuggestionContainer').attr("hidden", "true");
                                populateSuggestions(suggestions);
                            }
                        }
                    });
                }

                function populateSuggestions(sugg) {
                    $("#container-user-list").empty();
                    _.each(sugg, function (item) {
                        var tpl_user_item = _.template($("#tpl-user-item").html());
                        var opts = {
                            id: item.data,
                            imageURL: "${exposedProps.userImageBase}/" + item.path,
                            altImageURL: __NVLI.context + "/images/profile-picture.png",
                            name: item.value,
                            city: item.city,
                            state: item.state,
                            country: item.country
                        };
                        var el_user_item = tpl_user_item(opts);
                        $("#container-user-list").append(el_user_item);
                        $("#user-" + item.data).on('click', function (e) {
                            var userId = e.currentTarget.getAttribute("user-id");
                            var userName = e.currentTarget.getAttribute("user-name");
                            if (!(userId in shareMap) && !(userId === undefined) && !(userId === null)) {
                                addUserToList(userId, userName);
                                addUsernameToList(userId, userName);
                            } else {
                                alert('Already selected!');
                            }
                            if (!$("#label-container").html() !== "" && Object.keys(shareMap).length !== 0)
                            {
                                $("#shareWithNvliFriendsBtn").removeAttr("disabled");
                            }
                        });
                    });
                }

                function addUserToList(userId, userName) {
                    shareMap[userId] = userName;
                }

                function deleteUserFromList(userId) {
                    if (userId in shareMap) {
                        delete shareMap[userId];
                    }
                }

                function addUsernameToList(userId, userName) {
                    if (Object.keys(shareMap).length !== 0)
                    {
                        var tplLabel = _.template($("#tpl-nvlishare-label").html());
                        var opts = {
                            labelId: userId,
                            labelName: userName
                        };
                        var el_label_item = tplLabel(opts);
                        $("#label-container").append(el_label_item);
                    }
                }

                function deleteUsernameFromList(key, div) {
                    $("#user-" + key).remove();
                    deleteUserFromList(key);
                    if ($.trim($("#label-container").html()) === '') {
                        $('#suggestionContainer').attr("hidden", "true");
                        $("#shareWithNvliFriendsBtn").attr("disabled", true);
                    }
                }

                function socialshare(provider) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/user/share/" + provider + "/" + recordIdentifier + "/" + recordType,
                        cache: false,
                        success: function (shareCount) {
                            //console.log(shareCount);
//                            if (shareCount > 0) {
//                                console.log("shared on " + siteType);
//                            } else {
//                                alert("Error Sharing Record.Please try after some time.");
//                            }
                        },
                        error: function () {
                            alert("Service not available.Please try after some time.");
                        }
                    });
                }
                /*
                 function emailRecord(recId, resType) {
                 $.ajax({
                 url: window.__NVLI.context + "/search/user/mail/" + resType + "/" + recId,
                 cache: false,
                 success: function (likecount) {
                 $("#emailshare").jsSocials({
                 shareIn: "popup",
                 shares: ["email"],
                 url: window.__NVLI.context + "/search/preview/" + resType + "/" + recId,
                 text: "NVLI record",
                 desc: "hi,\nPlease click on the below link to access the record.If that does not work ,then copy paste the link into the browser.\n",
                 showLabel: true
                 });
                 },
                 error: function () {
                 alert("error");
                 }
                 });
                 }*/
                //    $('#reviewAnchor').click(function () {
                //        $('#reviewDiv').toggle();
                //    });
                $('#postReviewAnchor').click(function () {
                    var userReview = $('#reviewTxt').val().trim();
                    if (userReview.length > 0 && userReview.length < 1000) {
                        $.ajax({
                            url: "${pageContext.request.contextPath}/search/user/review",
                            data: {"recordIdentifier": recordIdentifier, "userReview": userReview, "recordType": recordType},
                            cache: false,
                            success: function (jsonobj) {
                                if (jsonobj.reviewCount > 0) {
                                    showToastMessage("Review Published!", "success");
                                    userRatingStatistics();
                                } else if (jsonobj.reviewCount === -1) {
                                    showToastMessage("Review Updated!", "success");
                                    userRatingStatistics();
                                } else {
                                    showToastMessage("Error while Reviewing.Please try after some time", "error");
                                }
                                $("#reviewTxt").val('');
                                $("#reviewc").empty();
                                pageNumber = 1;
                                populateReviews();
                                sendNotifications(jsonobj.notificationActivityType, jsonobj.uniqueObjectIdentifier, jsonobj.message);
                            },
                            error: function () {
                                showToastMessage("Service not available.Please try after some time.", "error");
                            }
                        });
                    } else {
                        showToastMessage("Review text must between 1 to 1000 charcters!!", "info");
                    }
                });
                function userRating(rating) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/user/rating/" + recordIdentifier + "/" + avgRecRating + "/" + rating + "/" + numberOfRatings + "/" + recordType,
                        cache: false,
                        success: function (jsonobj) {
                            if (jsonobj.ratingCount > 0 || jsonobj.ratingCount === -1) {
                                if (jsonobj.ratingCount === -1) {
                                    showToastMessage("Your Rating Updated!", "success");
                                } else {
                                    showToastMessage("Thanks.We Received Your Rating.", "success");
                                    numberOfRatings = jsonobj.ratingCount;
                                }
                                userRatingStatistics();

                            } else {
                                showToastMessage("Error rating record.Please try after some time.", "error");
                            }
                            $("#reviewc").empty();
                            pageNumber = 1;
                            populateReviews();
                        },
                        error: function () {
                            showToastMessage("Service not available.Please try after some time.", "error");
                        }
                    });
                }
                function userRatingStatistics() {
                    var totalRatingValue = 0;
                    $("#avarage-rating-main-div").show();
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/ratingStatistics",
                        data: {"recordIdentifier": recordIdentifier},
                        cache: false,
                        success: function (response) {
                            if (numberOfRatings > 0) {
                                $("#ratingstatistics").empty();
                                var ratingCSS;
                                _.each(response, function (rating) {
                                    totalRatingValue = totalRatingValue + (rating.recordRatingCount * rating.recordRating);
                                    var template = _.template($("#ratings-statistics-template").html());
                                    if (rating.recordRating === 5) {
                                        ratingCSS = "bg-success";
                                    } else if (rating.recordRating === 4) {
                                        ratingCSS = "bg-success";
                                    } else if (rating.recordRating === 3) {
                                        ratingCSS = "bg-info";
                                    } else if (rating.recordRating === 2) {
                                        ratingCSS = "bg-warning";
                                    } else if (rating.recordRating === 1) {
                                        ratingCSS = "bg-danger";
                                    } else {
                                        ratingCSS = "no-rating";
                                    }
                                    var opts = {
                                        "ratingValue": rating.recordRating,
                                        "ratingWidth": parseFloat((rating.recordRatingCount / numberOfRatings) * 100).toFixed(2),
                                        "ratingCss": ratingCSS
                                    };
                                    var el = template(opts);
                                    $("#ratingstatistics").append(el);
                                    checkIfRatingNull();
                                });
                                //check whether user given review or not
                                if (numberOfRatings > 0) {
                                    avgRecRating = parseFloat(totalRatingValue / numberOfRatings).toFixed(2);
                                }
                                avgRecRating = parseFloat(avgRecRating).toFixed(2);
                                //update review count div
                                if (numberOfReviews > numberOfRatings) {
                                    document.getElementById('review-rating-count-div').innerHTML = '(' + numberOfReviews + ')';
                                } else {
                                    document.getElementById('review-rating-count-div').innerHTML = '(' + numberOfRatings + ')';
                                }
                                //to update rating 
                                $("#showRatingStars").empty();
                                var show_star_tpl = _.template($("#tpl-show-rating-stars").html());
                                var opts = {
                                    ratingValue: avgRecRating
                                };
                                $("#showRatingStars").html(show_star_tpl(opts));
                                $(".rateit").rateit();
                                //rating update ends
                                document.getElementById('avarageRating').innerHTML = avgRecRating;
                                document.getElementById('totalRaingCount').innerHTML = numberOfRatings;
                            } else {
                                $("#avarage-rating-main-div").hide();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(textStatus, errorThrown, jqXHR);
                        }
                    });
                }
                function checkIfRatingNull() {
                    console.log("check-if-no-rating");
                    if ($("#check-if-no-rating").hasClass("no-rating")) {
                        $("#ratingstatistics").hide();
                    }
                }

                $("#saveToListAnchor").click(function () {
                    $("#successDiv").hide();
                    $("#createNewListDiv").hide();
                    $("#addToListDiv").show();
                    resetAddListDiv();
                    populateUserSavedList();
                    $('#add_to_list_link').modal('show');
                });
                $('#addToListSelect').on('change', function () {
                    if ($(this).val() === 'new') {
                        resetNewListDiv();
                        $("#createNewListDiv").show();
                        $("#addToListDiv").hide();
                    }
                });
                $("#createListBtn").on(function () {
                    $("#createNewListDiv").show();
                    $("#addToListDiv").hide();
                });
                $("#resetBtn1").click(function () {
                    resetAddListDiv();
                });
                $("#resetBtn2").click(function () {
                    resetNewListDiv();
                });
                function resetAddListDiv() {
                    $("#addToListSelect").val('0');
                }

                function resetNewListDiv() {
                    $("input[name=accessRadio][value=1]").prop('checked', true);
                    $("#newListTxt").val("");
                }

                $("#backBtn").click(function () {
                    resetNewListDiv();
                    resetAddListDiv();
                    $("#createNewListDiv").hide();
                    $("#addToListDiv").show();
                });
                $("#AddToExistingListBtn").click(function () {
                    var listName = $("#addToListSelect").val();
                    if (!_.isEmpty(listName) && listName != 0) {
                        $.ajax({
                            url: "${pageContext.request.contextPath}/search/user/add/to/existing/list/" + recordIdentifier + "/" + recordTitle + "/" + listName + "/" + recordType,
                            cache: false,
                            success: function (jsonObj) {
                                if (jsonObj.key === 0) {
                                    showToastMessage(jsonObj.value, "error");
                                    $("#addToListDiv").hide();
                                } else if (jsonObj.key === 1) {
                                    showToastMessage(jsonObj.value, "info");
                                    $("#addToListDiv").hide();
                                } else {
                                    $("#successDiv").show();
                                    $("#createNewListDiv").hide();
                                    $("#addToListDiv").hide();
                                }
                            },
                            error: function () {
                                showToastMessage("Service not available.Please try after some time.", "error");
                                $("#addToListDiv").hide();
                            }
                        });
                    } else {
                        showToastMessage("Please Select Any List", "info");
                    }

                });
                $("#addToNewListBtn").click(function () {
                    var listname = $("#newListTxt").val();
                    if (listname.length < 3 || listname.length > 20) {
                        showToastMessage("List name should be within 4  to 20 characters", "warning");
                        return false;
                    }
                    var listAccess = -1;
                    var selectedRaio = $("input[type='radio'][name='accessRadio']:checked");
                    if (selectedRaio.length <= 0) {
                        showToastMessage("Please Select Access Type", "warning");
                    } else if (listname.length <= 0) {
                        showToastMessage("Please Provide List Name", "warning");
                    } else {
                        listAccess = selectedRaio.val();
                        $.ajax({
                            url: "${pageContext.request.contextPath}/search/user/add/to/new/list/" + recordIdentifier + "/" + recordTitle + "/" + listname + "/" + listAccess + "/" + recordType,
                            cache: false,
                            success: function (data) {
                                if (data === 0) {
                                    showToastMessage("some error occured while saving.Please try after some time.", "error");
                                } else if (data === 2) {
                                    showToastMessage("This List already Exists.", "info");
                                } else {
                                    $("#successDiv").show();
                                    $("#createNewListDiv").hide();
                                    $("#addToListDiv").hide();
                                }
                            },
                            error: function () {
                                showToastMessage("Service not available.Please try after some time.", "error");
                            }
                        });
                    }
                });
                function populateReviews() {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/record/reviews/" + recordIdentifier + "/" + pageNumber + "/" + pageWindow,
                        dataType: 'json',
                        type: 'GET',
                        success: function (recordReviews) {
                            var Count = 0;
                            var defaultReview;
                            _.each(recordReviews, function (reviews) {
                                if (reviews.recordRating === 5)
                                {
                                    defaultReview = "OutStanding";
                                } else if (reviews.recordRating === 4)
                                {
                                    defaultReview = "Excellent";
                                } else if (reviews.recordRating === 3)
                                {
                                    defaultReview = "Very Good";
                                } else if (reviews.recordRating === 2)
                                {
                                    defaultReview = "Good";
                                } else if (reviews.recordRating === 1)
                                {
                                    defaultReview = "Average";
                                } else {
                                    defaultReview = reviews.reviews;
                                }
                                if (!(_.isEmpty(reviews.reviews))) {
                                    defaultReview = reviews.reviews;
                                }
                                var template = _.template($("#record_reviews").html());
                                Count++;
                                var opts = {
                                    "username": reviews.userName,
                                    "reviewText": defaultReview,
                                    "reviewAddedTime": reviews.addedTime,
                                    "ratingValue": reviews.recordRating,
                                    "userProfilePicPath": reviews.userProfilePicPath,
                                    "count": Count
                                };
                                var el = template(opts);

                                $("#reviewc").append(el);

                            });
                            $(".rateit").rateit();

                            if (Count === 0)
                            {
                                if (processing) {
                                    hasNext = false;
                                    $("#reviewc").append("<center>End of Reviews!</center>");
                                } else {
                                    $("#reviewc").empty();
                                    $("#reviewc").append("<div class='card-footer'><tags:message code="label.beFirstOneToRAR"/></div>");
                                }
                            }
                            processing = false;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(textStatus, errorThrown, jqXHR);
                        }
                    });
                }

                function populateUserSavedList() {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/user/get/saved/lists",
                        dataType: 'json',
                        type: 'GET',
                        success: function (savedLists) {
                            var Count = 0;
                            $("#addToListSelect").empty();
//                            $("#addToListSelect").append("<option value='0'>Select List</option>");
                            _.each(savedLists, function (savedList) {
                                var template = _.template($("#user_saved_list").html());
                                Count++;
                                var opts = {
                                    "listId": savedList.listId,
                                    "listName": savedList.listName,
                                    "listVisibility": savedList.listVisibility
                                };
                                var el = template(opts);
                                $("#addToListSelect").append(el);
                            });
                            $("#addToListSelect").append("<option disabled='disabled'>---------</option><option value='new' style='color:red;'>Create New</option>");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(textStatus, errorThrown, jqXHR);
                        }
                    });
                }
                /*
                 *Zoom in functionality
                 */
                $("#in").click(function () {
                    $('#cut_viewer').show();
                    var iv1 = $("#cut_viewer").iviewer({
                        src: selectedFilePath
                    });
                    iv1.iviewer('zoom_by', 1);
                });
                /*
                 *Zoom out functionality
                 */
                $("#out").click(function () {
                    $('#cut_viewer').show();
                    var iv1 = $("#cut_viewer").iviewer({
                        src: selectedFilePath
                    });
                    iv1.iviewer('zoom_by', -1);
                });
                /*
                 *fit to screen functionality
                 */
                $("#fit").click(function () {
                    $('#cut_viewer').show();
                    var iv1 = $("#cut_viewer").iviewer({
                        src: selectedFilePath

                    });
                    iv1.iviewer('fit');
                });
                /*
                 *original view functionality
                 */
                $("#orig").click(function () {
                    $('#cut_viewer').show();
                    var iv1 = $("#cut_viewer").iviewer({
                        src: selectedFilePath

                    });
                    iv1.iviewer('set_zoom', 100);
                });
                /*
                 *Rotate Left functionality
                 */
                $("#rotateleft").click(function () {
                    $('#cut_viewer').show();
                    var iv1 = $("#cut_viewer").iviewer({
                        src: selectedFilePath

                    });
                    iv1.iviewer('angle', -90);
                });
                /*
                 *Rotate Right functionality
                 */
                $("#rotateright").click(function () {
                    $('#cut_viewer').show();
                    var iv1 = $("#cut_viewer").iviewer({
                        src: selectedFilePath
                    });
                    iv1.iviewer('angle', 90);
                });
                $("#cut_viewer").on('mousewheel', function (e) {
                    e.preventDefault();
                });
                /*scroll issue code start*/
                var keys = {37: 1, 38: 1, 39: 1, 40: 1};
                function preventDefault(e) {
                    e = e || window.event;
                    if (e.preventDefault)
                        e.preventDefault();
                    e.returnValue = false;
                }
                function preventDefaultForScrollKeys(e) {
                    if (keys[e.keyCode]) {
                        preventDefault(e);
                        return false;
                    }
                }
                $("#cut_viewer").mouseover(function () {
                    //e.preventDefault();

                    if (window.addEventListener) // older FF
                        window.addEventListener('DOMMouseScroll', preventDefault, false);
                    window.onwheel = preventDefault; // modern standard
                    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
                    window.ontouchmove = preventDefault; // mobile
                    document.onkeydown = preventDefaultForScrollKeys;
                });
                $("#cut_viewer").mouseout(function () {
                    if (window.removeEventListener)
                        window.removeEventListener('DOMMouseScroll', preventDefault, false);
                    window.onmousewheel = document.onmousewheel = null;
                    window.onwheel = null;
                    window.ontouchmove = null;
                    document.onkeydown = null;
                });
                /*scroll issue code end*/
                //Add tag
                function addCustomTags(tagValue) {
                    tagValue = tagValue.trim().replace(/[_,\\\/!@#$%^&*\[\]{}\'\";:]/gm, '');
                    var languageId = document.getElementById('selectCustomLanguage').value;
                    if (tagValue !== '' && tagValue.length !== 0) {
                        if (tagValue.trim().length < 100) {
                            if (_.contains(existingCustomTags, tagValue.toLowerCase())) {
                                alert("Tag already existing..");
                            } else {
                                tagValue = tagValue + '_' + languageId;
                                $.ajax({
                                    url: '${context}/search/user/saveTags',
                                    dataType: 'json',
                                    type: "post",
                                    data: {
                                        'recordIdentifier': recordIdentifier,
                                        'customTags': tagValue
                                    },
                                    success: function (response)
                                    {
                                        if (response) {
                                            document.getElementById("addButtonDiv").setAttribute("data-dismiss", "modal");
                                            document.getElementById('customAutocomplete').value = '';
                                            top.location.href = "${context}/search/preview/" + recordIdentifier;
                                        } else
                                            alert('Error in adding tag..');
                                    },
                                    error: function ()
                                    {

                                    }
                                });
                            }
                        } else {
                            alert("Maximum 100 characters allowed in custom tag value !");
                        }
                    } else
                        alert("please enter tag value");
                }
                function selectCustomLanguage()
                {
                    customTAgSuggestions();
                }

                function customTAgSuggestions()
                {
                    var languageId = document.getElementById('selectCustomLanguage').value;
                    $('#customAutocomplete').devbridgeAutocomplete({
                        paramName: 'tagPrefix',
                        serviceUrl: '${context}/cs/getCustomTagsSuggestions?languageId=' + languageId
                    });
                }
                function getParameterByName(name, url) {
                    if (!url)
                        url = window.location.href;
                    name = name.replace(/[\[\]]/g, "\\$&");
                    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                            results = regex.exec(url);
                    if (!results)
                        return null;
                    if (!results[2])
                        return '';
                    return decodeURIComponent(results[2].replace(/\+/g, " "));
                }
                function populateCitation() {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/search/preview/citation/" + recordIdentifier,
                        type: 'GET',
                        success: function (citationMap) {
                            $("#citation-map").empty();
                            _.each(citationMap, function (mapValue, mapKey) {
                                var template = _.template($("#citation_template").html());
                                var opts = {
                                    "citationObjKey": mapKey,
                                    "citationObjValue": mapValue
                                };
                                var el = template(opts);
                                $("#citation-map").append(el);
                            });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.error(textStatus, errorThrown, jqXHR);
                        }
                    });
                }
                function nvliShare() {
                    $("#shareWithNvliDiv").show();
                    $("#label-container").html("");
                    $("#inviteFriendsDiv").hide();
                    $("#shareSuccessDiv").hide();
                    $("#inviteSentDiv").hide();
                    $("#shareWithNvliFriendsBtn").attr("disabled", true);
                    $("#sharedLinkLabel").text(loc);
                    $("#userMsgTxt").val("");
                    $("#searchFriendsTxt").val("");
                    $('#nvli_share').modal('show');
                    shareMap = {};
                }
                $("#InviteFriendsBtn").click(function () {
                    $("#inviteToTxt").val("");
                    $("#inviteFriendsDiv").show();
                    $("#shareSuccessDiv").hide();
                    $("#inviteSentDiv").hide();
                    $("#shareWithNvliDiv").hide();
                });
                $("#shareWithNvliFriendsBtn").click(function () {
                    var emailIds = Object.keys(shareMap);
                    var userMsg = $("#userMsgTxt").val();
                    $.ajax({
                        url: '${context}/search/user/nvli/share',
                        cache: false,
                        type: "post",
                        data: {
                            'emailId': emailIds.toString(),
                            'userMsg': userMsg,
                            'recordIdentifier': recordIdentifier,
                            'recordType': recordType,
                            'imageUrl': $("meta[property='og:image']").length === 0 ? "${context}/nvliPortal-1.0/themes/images/thumb/no_image_thumb.jpg" : $("meta[property='og:image']")[0].content
                        },
                        success: function (response) {
                            if (response === "success") {
                                $("#inviteFriendsDiv").hide();
                                $("#shareSuccessDiv").show();
                                $("#inviteSentDiv").hide();
                                $("#shareWithNvliDiv").hide();
                            } else if (response === "failed") {
                                alert("Something going wrong,please refresh the page.");
                            } else {
                                alert(response);
                            }
                        },
                        error: function ()
                        {
                            alert('Error');
                        }
                    });
                });
                $("#sendInviteBtn").click(function () {
                    var emailIds = $("#inviteToTxt").val();
                    if (emailIds.length > 0) {
                        $("#inviteFriendsDiv").hide();
                        $("#shareSuccessDiv").hide();
                        $("#shareWithNvliDiv").hide();
                        $.ajax({
                            url: '${context}/search/user/invite/friends',
                            cache: false,
                            type: "post",
                            data: {
                                'emailIds': emailIds
                            },
                            success: function (response)
                            {
                                $("#inviteSentDiv").show();
                            },
                            error: function ()
                            {
                                alert('Error');
                            }
                        });
                    }
                });
                $("#backToShareBtn").click(function () {
                    nvliShare();
                });
                $(".full_width").click(function () {
                    $('.full_width > button').text(function (i, text) {
                        return text === "Small Size" ? "Full Size" : "Small Size";
                    });
                    $(".width_custom").toggleClass('col-lg-12');
                });
                function fetchPaymentDetail() {
                    $.ajax({
                        url: __NVLI.context + "/paymentws/fetchPaymentDetail",
                        type: 'POST',
                        dataType: 'JSON',
                        data: {recordIdentifier: recordIdentifier},
                        success: function (payment_detail) {
                            if (payment_detail["is-paid-record"]) {
                                var tpl_payment_type = _.template($("#tpl-payment-type").html());
                                var opts = {
                                    isRecordPriceAvailable: payment_detail["is-record-price-set"],
                                    isResourcePriceAvailable: payment_detail["is-resource-price-set"],
                                    recordPrice: payment_detail["record-price"],
                                    resourcePrice: payment_detail["resource-price"],
                                    resourceCode: payment_detail["resource-code"],
                                    recordIdentifier: recordIdentifier
                                };
                                var payment_type = tpl_payment_type(opts);
                                $("#modal-payment-type-body").html(payment_type);
                                $('input[name=chk-item-type]').unbind("click").bind("click", function () {
                                    fetchItemDetail();
                                });
                                $("#modal-item-detail-div").empty();
                                $('#buy-now-modal').modal('show');
                            }
                        },
                        error: function () {
                            console.log("Error in fetching payment details.");
                        }
                    });
                }
                function isUserLoggedIn(callback) {
                    $.ajax({
                        url: __NVLI.context + "/paymentws/isUserLoggedIn",
                        success: function (response) {
                            if (response === "true") {
                                callback();
                            } else {
                                window.location = __NVLI.context + "/auth/login";
                            }
                        },
                        error: function () {
                            console.log("Something going wrong,please refresh the page.");
                        }
                    });
                }
                function fetchItemDetail() {
                    var $radio = $('input[name=chk-item-type]:checked');
                    $.ajax({
                        url: __NVLI.context + "/paymentws/fetchItemDetail",
                        type: 'POST',
                        dataType: 'JSON',
                        data: {itemId: $radio.attr("data-item-id"), itemType: $radio.attr("data-item-type")},
                        success: function (item_detail) {
                            var tpl_item_detail = _.template($("#tpl-item-detail").html());
                            var opts = {
                                itemtName: item_detail["item-title"],
                                itemPrice: item_detail["item-price"]
                            };
                            $("#modal-item-detail-div").empty();
                            $("#modal-item-detail-div").html(tpl_item_detail(opts));
                            var tpl_item_detail_footer = _.template($("#tpl-item-detail-footer").html());
                            var opts = {
                                itemtId: item_detail["item-id"],
                                itemType: item_detail["item-type"]
                            };
                            $("#modal-payment-type-footer").empty();
                            $("#modal-payment-type-footer").append(tpl_item_detail_footer(opts));

                            $("#btn-item-pay-now").unbind("click").bind("click", function (e) {
                                $.ajax({
                                    url: __NVLI.context + "/paymentws/payForItem",
                                    type: 'POST',
                                    data: {itemId: $("#btn-item-pay-now").attr("data-item-id"), itemType: $("#btn-item-pay-now").attr("data-item-type")},
                                    success: function (isPaidSuccessfully) {
                                        $('#buy-now-modal').modal('toggle');
                                        paymentStatus(isPaidSuccessfully);
                                    },
                                    error: function () {
                                        console.log("Something going wrong,please refresh the page.");
                                    }
                                });
                            });
                        },
                        error: function () {
                            console.log("Error in fetching item details.");
                        }
                    });
                }
                function paymentStatus(isPaid) {
                    if (isPaid) {
                        var tpl_payment_status = _.template($("#tpl-payment-status").html());
                        var opts = {
                            alertStatus: "alert-success",
                            paymentStatus: "Success",
                            message: "You have successfully paid for this item. To view full record content"
                        };
                        $("#payment-info").html(tpl_payment_status(opts));
                        $("#btn-buy-div").empty();
                    } else {
                        var tpl_payment_status = _.template($("#tpl-payment-status").html());
                        var opts = {
                            alertStatus: "alert-warning",
                            paymentStatus: "Pending",
                            message: "In process"
                        };
                        $("#payment-info").html(tpl_payment_status(opts));
                        $("#btn-buy-div").empty();
                    }
                }
                function checkForPaidRecord() {
                    $.ajax({
                        url: __NVLI.context + "/paymentws/checkForPaidRecord",
                        type: 'POST',
                        dataType: 'JSON',
                        data: {recordIdentifier: recordIdentifier},
                        success: function (isPaidRecord) {
                            if (isPaidRecord["is-paid-record"]) {
                                if (isPaidRecord["is-user-paid-for-record"]) {
                                    var tpl_view_status = _.template($("#tpl-view-status").html());
                                    var opts = {
                                        message: "to view full record content."
                                    };
                                    $("#payment-info").html(tpl_view_status(opts));
                                } else {
                                    var tpl_buy_btn = _.template($("#tpl-buy-btn").html());
                                    $("#btn-buy-div").html(tpl_buy_btn);
                                    $("#btn-buy-item").unbind("click").bind("click", function (e) {
                                        isUserLoggedIn(fetchPaymentDetail);
                                    });
                                }
                            }
                        },
                        error: function () {
                            console.log("Payment service is not responding.");
                        }
                    });
                }

                function setTypeOfRecord(type) {
                    switch (type) {
                        case 0:
                            recordType = "image";
                            break;
                        case 1:
                            recordType = "text";
                            break;
                        case 2:
                            recordType = "video";
                            break;
                        case 3:
                            recordType = "audio";
                            break;
                    }

                }
</script>
<div class="modal fade" id="addTagModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.add.tag"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <select class="form-control" id="selectCustomLanguage" onchange="selectCustomLanguage();">
                            <c:forEach items="${languageList}" var="language">
                                <c:if test="${language.id eq 40}">
                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                </c:if>
                                <c:if test="${language.id ne 40}">
                                    <option value="${language.id}">${language.languageName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <input type="text" placeholder="Enter Tags value" name="customTags" class="form-control"  id="customAutocomplete"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="addCustomTags(document.getElementById('customAutocomplete').value);" id="addButtonDiv"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<!--Modal for item Detail starts here-->
<div id="buy-now-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">� </button>
                <h5 class="modal-title" id="exampleModalPopoversLabel"><tags:message code="sr.payment.details"/></h5>
            </div>
            <div class="modal-body">
                <table cellpadding="0" cellspacing="0" class="table table-bordered table-md">
                    <tbody id="modal-payment-type-body"></tbody>
                </table>
                <div id="modal-item-detail-div"></div>
            </div>
            <div id="modal-payment-type-footer"></div>
        </div>
    </div>
</div>
<!--Modal for Modal for item Detail ends here-->
<script type="text/template" id="tpl-label-email">
    <tags:message code="label.e-mail"/>
</script>
<script type="text/template" id="tpl-label-facebook">
    <tags:message code="label.facebook"/>
</script>
<script type="text/template" id="tpl-label-twitter">
    <tags:message code="label.twiiter"/>
</script>
<script type="text/template" id="tpl-label-google">
    <tags:message code="label.google"/>
</script>
<script type="text/template" id="tpl-label-nvli">
    <tags:message code="label.nvli"/>
</script>
<script type="text/template" id="tpl-label-linkedin">
    <tags:message code="label.linkedin"/>
</script>
