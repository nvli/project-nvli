<%--
    Document   : resource-category
    Created on : May 27, 2016, 10:58:42 AM
    Author     : Madhuri
--%>
<%@page import="in.gov.nvli.util.Constants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<link href="${context}/themes/css/salvory.css" rel="stylesheet"/>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src='${context}/themes/js/language/pramukhindic.js'></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage.js"></script>
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css" />
<script type="text/javascript" src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript">
    var action = null;
    $(document).ready(function () {
        var oldSearchIn = '${searchForm.searchingIn}';
        $("#chkboxall").unbind().bind("click", function (e) {
            if ($('#chkboxall').is(':checked')) {
                $('input:checkbox[id^=s]').each(function () {
                    $(this).prop("checked", false);
                    $(this).attr('disabled', true);
                });
                $('input:checkbox[id^=inlineCheckboxg1]').each(function () {
                    $(this).prop("checked", false);
                    $(this).attr('disabled', true);
                });
                $("#searchingIn").val("all");
            } else {
                $("#searchingIn").val(oldSearchIn);
                $('input:checkbox[id^=s]').each(function () {
                    $(this).attr('disabled', false);
                });
                $('input:checkbox[id^=inlineCheckboxg1]').each(function () {
                    $(this).attr('disabled', false);
                });
            }
        });
        changeLanguage(getLanguage('${pageContext.response.locale}'));
        $(".localisation").click(function () {
            document.getElementById($(this).attr("id")).href = "?language=" + $(this).attr("id");
            languagePref = $(this).attr("id");
            changeResourceTypeInfoByLang($(this).attr("id"));
        });
        //On clicking group check box ,all checkbox under that grop will be checked or unchecked
        $('input:checkbox[value=option1]').click(function () {
            if ($(this).prop("checked") == true) {
                $('input[data-parent-checkbox=' + $(this).attr("id") + "]").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $('input[data-parent-checkbox=' + $(this).attr("id") + "]").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
        //On clicking subresource check box ,group checkbox above that grop will be checked or unchecked
        $("input:checkbox[id^=s]").click(function () {
            var parentCheckbox = $(this).attr("data-parent-checkbox");
            var totalCheckboxLength = $('input:checkbox[data-parent-checkbox=' + parentCheckbox + ']').length;
            var checkedCheckboxLength = $('input:checkbox[data-parent-checkbox=' + parentCheckbox + ']:checked').length;
            if ($(this).prop("checked") == true) {
                if (totalCheckboxLength == checkedCheckboxLength) {
                    $('input:checkbox[value=option1][id=' + parentCheckbox + ']').prop("checked", true);
                }
            } else {
                $('input:checkbox[value=option1][id=' + parentCheckbox + ']').prop("checked", false);
            }
        });
        $('#searchElement').devbridgeAutocomplete({
            paramName: 'term',
            minChars: 3,
            serviceUrl: '${context}/search/suggestions/${mappingResourceType.resourceTypeCode}'
                    });
                    $("#submitSearch").click(function () {
                        redirectToGenericSearchResult();
                    });
                });

                function redirectToGenericSearchResult() {
                    var resourceType = $("#searchingIn").val();
                    action = "";
                    if (resourceType === "all") {
                        action = "${context}/search/across/all";
                    } else {
                        action = "${context}/search/across/resource/" + resourceType;
                    }
                    var genericSearchForm = document.getElementById("searchForm");
                    genericSearchForm.setAttribute("action", action);
                    $("#language").val("${pageContext.response.locale}");
                    genericSearchForm.setAttribute("method", "get");
                    document.getElementById("searchForm").submit();
                }
                function redirecting(event) {
                    if (event.keyCode === 13) {
                        redirectToGenericSearchResult();
                    }
                }

                function changeResourceTypeInfoByLang(lang)
                {
                    console.log(lang);
                    $.ajax({
                        url: '${context}/resourceTypeInfoByLang',
                        data: {languageCode: lang, resourceTypeCodeValue: $("#searchingIn").val()},
                        success: function (jsonObject) {
                            console.log(jsonObject)
                            _.each(jsonObject, function (object)
                            {
                                $("#label-resorce-type").text(object.title);
                            });
                        },
                        error: function (xhr) {
                            console.log(xhr);
                        }
                    });

                }
</script>

<div class="m-t-3">
    <form:form id="searchForm" commandName="searchForm" method="get">
        <div class="m-l-3 m-r-3"> <!-- Horizontal curves -->
            <div class="paper clearfix">
                <div class="clearfix row">
                    <div class="p-a-1 clearfix">
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <div class="">
                                <div class="row">
                                    <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 p-t-1">
                                        <div class="col-glg-1 colf-md-4  cofl-sm-12 pull-left m-r-0"> <img src="${rTIconLocation}/${mappingResourceType.resourceTypeIcon}"  alt="National Virtual Library of India (NVLI)" class="m-r-1" height="30"/></div>
                                        <h3><span id="label-resorce-type">${mappingResourceType.resourceType}</span></h3>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="input-group" style="float: right;width: 100%;">
                                                    <!--<span class="ex_check1"><input type="checkbox" name="isExactMatch"  title="Exact Match"> Exact Match</span>-->
                                                    <form:input autocomplete="off" path="searchElement" onKeyPress="return redirecting(event);" cssClass="form-control langSearch" placeholder="Search.."/>
                                                    <form:input path="searchingIn"  hidden="true" id="searchingIn"/>
                                                    <form:input path="searchComplexity"  hidden="true" id="searchComplexity"/>
                                                    <input type="hidden" name="language" id="language" value="${pageContext.response.locale}">
                                                    <span class="input-group-btn" style="width: 10%;">
                                                        <button class="btn btn-success" type="button" id="submitSearch"><i class="fa fa-search"></i></button>
                                                    </span>
                                                </div>
                                                <label style="cursor: pointer;margin-bottom:0px;margin-top:5px;">
                                                    <input type="checkbox" name="isAll" class="font120 font-weight-bold" id="chkboxall" style="vertical-align: text-top;"> 
                                                    <tags:message code="resource.label.all.bubble.search" /> |
                                                </label>
                                                <label style="cursor: pointer;margin-bottom:0px;margin-top:5px;">
                                                    <input id="fullText" name="fullText" class="font120 font-weight-bold" type="checkbox" style="vertical-align: text-top;">
                                                    <tags:message code="label.fullTextSearch"/> |
                                                </label>
                                                <a href="${context}/advsearch/" class="link1"><tags:message code="label.advanced.search"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-bottom:30px;" class="clearfix"></div>
        <!-- search result content starts here -->
        <div class="m-l-3 m-r-3 m-t-2" id="main">
            <div class="row">
            <div class="clear"></div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <c:set var="countSubResourceIdentifier" value="0"/>
                    <c:set var="GROUP_WITH_CLASS" value="<%=Constants.GROUP_WITH_CLASS%>"/>
                    <div class="masonry"  id="grid" data-columns>
                        <c:forEach items="${groupList}" var="groupType" varStatus="gindex">   
                            <c:set var = "isRecordExist"  value = "false"/>
                            <!--<div>-->
                            <c:if test="${not empty groupWiseResource[groupType]}">
                                <div class="masonry-item" id="head_${groupWiseResource[groupType]}">
                                    <c:set var="headerClass" value="unknown"/>
                                    <div class="sli_head clearfix">
                                        <c:choose>
                                            <c:when test="${groupType.groupIcon ne null && groupType.groupIcon ne ''}">
                                                <c:set var="groupIcon" value="${groupIconReadLocation}/${groupType.groupIcon}"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="groupIcon" value="${groupIconReadLocation}/unknown-category.png"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <span class="group_head_icon" style="background-image:url('${groupIcon}');">
                                            <label class="checkbox-inline" style="margin-top:-24px; margin-left:-5px;">
                                                <input type="checkbox" id="inlineCheckboxg1${groupType.id}" value="option1">
                                            </label>
                                            &nbsp;
                                            ${groupType.groupType}
                                        </span>
                                    </div>
                                    <div class="overflow_maxh600 table-responsive">
                                        <table class="table table-striped table-bordered table-hover ${groupWiseResource[groupType]}">
                                            <thead>
                                                <tr class="col_head">
                                                    <th width="4%">
                                                    </th>
                                                    <th width="6%"><tags:message code="label.no"/></th>
                                                    <th><tags:message code="label.name.institute"/></th>
                                                    <th class="text-center"><tags:message code="label.items"/></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:set var="count" value="0" scope="page"/>
                                                <c:forEach items="${groupWiseResource[groupType]}" var="resource">
                                                    <c:if test="${isRecordExist ne 'true'}">
                                                        <c:set var = "isRecordExist"  value = "false"/>
                                                    </c:if>  
                                                    <c:choose>
                                                        <c:when test="${resource.groupType eq groupType}">                                                
                                                            <c:if test="${resource.recordCount ne null && resource.recordCount ne 0 && not empty resource.recordCount && resource.recordCount ne ''}">
                                                                <c:set var="count" value="${count + 1}" scope="page"/>  
                                                                <tr>
                                                                    <td>
                                                                        <label class="checkbox-inline" style="margin-top:-24px;">
                                                                            <c:set var="countSubResourceIdentifier" value="${countSubResourceIdentifier+1}"/>
                                                                            <input data-parent-checkbox="inlineCheckboxg1${groupType.id}" type="checkbox" id="s${resource.id}" name="subResourceIdentifiers[${countSubResourceIdentifier}]" value="${resource.resourceCode}"/>                                                                            
                                                                        </label>
                                                                    </td>
                                                                    <td>${count}</td>
                                                                    <c:choose>
                                                                        <c:when  test="${(resource.resourceType.resourceTypeCode eq 'VALUE' || resource.resourceType.resourceTypeCode eq 'BLOG' || resource.resourceType.resourceTypeCode eq 'FEST' || resource.resourceType.resourceTypeCode eq 'WIKI' ||  resource.resourceType.resourceTypeCode eq 'GOVW' || resource.resourceType.resourceTypeCode eq 'ENEW')}">
                                                                            <td>${resource.resourceName}</td>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <td> <a href="${context}/search/${resource.resourceType.resourceTypeCode}/show/${resource.resourceCode}" class="link3">${resource.resourceName}</a> </td>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    <td width="10%" align="right">
                                                                        <c:choose>
                                                                            <c:when test="${resource.recordCount ne null && resource.recordCount ne 0 && not empty resource.recordCount && resource.recordCount ne ''}">
                                                                                ${resource.recordCount}                                                                  
                                                                                <c:set var = "isRecordExist"  value = "true"/>
                                                                            </c:when>
                                                                        </c:choose>
                                                                    </td>
                                                                </tr>
                                                            </c:if>                                    
                                                        </c:when>
                                                    </c:choose>                                                                                                     
                                                </c:forEach>                                                   
                                            </tbody>
                                        </table>
                                        <c:choose>
                                            <c:when test="${isRecordExist eq 'false'}">                                                             
                                                <script>
                                                    document.getElementById("head_${groupWiseResource[groupType]}").remove();
                                                </script>
                                            </c:when>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:if>
                            <!--</div>-->
                            <!--<div class="clear" style="margin-bottom:20px;"></div>-->

                        </c:forEach>                         
                    </div>
                    <c:if test="${empty groupWiseResource}">
                        <div class="alert alert-warning">
                            <tags:message code="label.no.group"/>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="clear22"></div>
        </div>
    </form:form>
    <div class="clear22"></div>
        </div>
</div>
<script type="text/javascript" src="${context}/themes/js/salvattore.min.js"></script> 