<%--
    Document   : group-edit
    Created on : May 16, 2016, 5:11:38 PM
    Author     : Suman Behara <sumanb@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card ">
        <div class="card-header"><tags:message code="res.edit.group"/></div>
        <div class="card-block clearfix">
            <form:form method="GET" commandName="groupType"  id="groupFormId" style="text-align:center;">
                <div class="form-group row" style="text-align:center;">
                    <label class="col-sm-2 form-control-label"><tags:message code="res.group.type"/></label>
                    <div class="col-sm-10">
                        <form:input id="groupeTypeId"  path="groupType" cssClass="form-control" />
                        <form:hidden id="groupId" path="id" cssClass="form-control" />
                    </div>
                </div>
                <input name="groupIcon" class="form-control" value="${groupIcon}" type="hidden" id="groupIconHidden"/>
                <div class="form-group row">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  for="inputPassword"><tags:message code="res.select.resource.type"/><em>*</em></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                <c:choose>
                                    <c:when test="${ not empty groupType.resourceTypes}">
                                        <select id="resourceTypeList" multiple="multiple" size="8" class="form-control">
                                            <c:forEach var="resourceType" items="${resourceTypesList}">
                                                <c:set var="completeRT" value="${resourceType.resourceType}"/>
                                                <c:if test="${not fn:contains(groupType.resourceTypes,resourceType)}" >
                                                    <option value="${resourceType.id}">${resourceType.resourceType}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                </c:choose>
                            </div>
                            <div class="col-lg-1  col-md-3 col-sm-6 col-xs-12">
                                <button type="button" class="btn btn-secondary btn-sm btn-block m-t-3" onClick="addResourceType();"><i class="fa fa-chevron-right"></i></button>
                                <button type="button"  class="btn btn-secondary btn-sm btn-block m-b-2" onClick="removeResourceType();"><i class="fa fa-chevron-left"></i></button>
                            </div>

                            <div class="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                                <div id="alreadyAssignedRTDiv">
                                    <select multiple="multiple" size="8" class="form-control" id="alreadyAssignedRT" name="resourceTypes">

                                        <c:forEach var="assignedRT" items="${alreadyAssingedRTList}">
                                            <option>${assignedRT.resourceType}</option>
                                        </c:forEach>

                                        <c:forEach var="resourceType" items="${groupType.resourceTypes}">
                                            <option value="${resourceType.id}">${resourceType.resourceType}</option>
                                        </c:forEach>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
            <input type="hidden" name="groupIcon" id="groupIconDivId" value="${groupType}"/>
            <div class="form-group row">
                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  for="inputPassword"><tags:message code="res.group.icon"/></label>
                <div class="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                    <div class="profile-img">
                        <form id="groupIconfForm" >
                            <a id="upload_group_icon_btn" href="#" onClick="callUploadIcon();" class="link1"><tags:message code="label.uploadIcon"/></a>
                            <input id="upload_group_icon" type="file" name="uploadfile" style="visibility: hidden"/>
                            <input name="groupTypeName" class="form-control" value="${groupName}" type="hidden" id="groupTypeHidden"/>
                        </form>
                        <span id="upload-file-message">
                            <c:if test="${not empty groupType.groupIcon}">
                                <img id="imageUploadSrc" src="${groupIconLocation}/${groupType.groupIcon}" height="100" width="100" />
                            </c:if>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer clearfix">
            <div class="text-center">
                <button type="submit" class="btn btn-primary" onclick="submitGroupForm();"><tags:message code="edit.button.update"/></button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#upload_group_icon").on("change", uploadFile);
    });
    function addResourceType() {
        $('#resourceTypeList option:selected').remove().appendTo('#alreadyAssignedRT');
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function removeResourceType() {
        $('#alreadyAssignedRT option:selected').remove().appendTo('#resourceTypeList');
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function submitGroupForm() {
        var groupId = $("#groupId").val();
        $('#alreadyAssignedRT option').prop('selected', true);
        var action = "${context}/resource/group/edit/" + groupId;
        $("#groupFormId").attr("action", action);
        $("#groupFormId").submit();
    }
    function callUploadIcon() {
        $("#upload_group_icon").click();
    }
    function uploadFile() {
        var groupName = $('#typeGroupId').val();
        $("#groupTypeHidden").attr("value", groupName);
        $("#upload-file-message").html("");
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/upload/group/icon",
            type: "POST",
            data: new FormData($("#groupIconfForm")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                // Handle upload success
                //$("#groupIconDivId").attr("value", data);
                $("#upload-file-message").html("");
                $("#groupIconHidden").val(data);
                $("#upload-file-message").html('<img id="imageUploadSrc" src="${groupIconLocation}/' + data + '" height="100" width="100" />');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Handle upload error
                alert(thrownError)
                $("#upload-file-message").text(
                        "File not uploaded (perhaps it's too much big)");
            }
        });
    } // function uploadFile
</script>