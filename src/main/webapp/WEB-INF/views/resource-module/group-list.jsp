<%--
Document   : group-list
Created on : May 5, 2016, 3:43:30 PM
Author     : Suman Behara
Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${not empty grpMessage}">
        <div class="alert alert-success"> <span>${grpMessage}</span></div>
    </c:if>
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <tags:message code="res.group.type.list"/> &nbsp;
                    <a href="${context}/resource/group/create" class="btn btn-success"><tags:message code="res.create.group"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control" placeholder="Search Group">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"><tags:message code="label.sno"/></th>
                        <th width="50" ><tags:message code="res.group.icon"/></th>
                        <th ><tags:message code="res.group.type"/></th>
                        <th ><tags:message code="label.resource.type"/></th>
                        <th width="9%" style="text-align:center;"><tags:message code="res.edit.delete"/></th>

                    </tr>
                </thead>
                <tbody id="groupType-tbl-body"></tbody>
            </table>
        </div>
        <div id="groupType-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });



    function deleteGroupType() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-grouptype-button").attr("href");
        } else {
            return false;
        }
    }

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/grouptype/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                console.log("success");
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.groupTypeList)) {
                        $("#groupType-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-groupType-list").html());

                        var Opts = {
                            "groupTypeList": jsonObject.groupTypeList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        var articleTableBody = articlesTpl(Opts);
                        $("#groupType-tbl-body").empty();
                        $("#groupType-tbl-body").html(articleTableBody);
                        renderPagination(fetchAndRenderTableData, "groupType-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                console.log("err");
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-groupType-list">
    {{ _.each(groupTypeList, function (groupType,i){ }}
    <tr><td>{{=i+1+recordCount}}</td>
    <td >
    {{ if(groupType.groupIcon!==null && groupType.groupIcon!==""){}}
    <img id="imageUploadSrc" src="${groupIconLocation}/${groupType.groupIcon}" height="50"/>
    {{ } else{}}
    <img id="imageUploadSrc" src="${groupIconLocation}/news-paper_preview.jpg" height="50"/>
    {{ } }}
    </td>
    <td >
    {{=groupType.groupType}}
    </td>
    <td>
    {{_.each(groupType.resourceTypes, function (resourceType) {}}
    {{=resourceType.resourceType}}<br>
    {{});}}
    </td>
    <td style="text-align:center;">
        <div class="btn-group">
        <a href="${pageContext.request.contextPath}//resource/group/populate/{{=groupType.id}}" class="btn btn-secondary btn-sm" title="Edit"><span class="fa fa-pencil" aria-hidden="true"></span></a>
        <a id="remove-grouptype-button" href="${context}/resource/group/remove/{{=groupType.id}}" class="btn btn-secondary btn-sm" title="Delete" onclick="return deleteGroupType();" ><span class="fa fa-trash" aria-hidden="true"></span></a>
       </div>
    </td>
    </tr>
    {{ }); }}
</script>