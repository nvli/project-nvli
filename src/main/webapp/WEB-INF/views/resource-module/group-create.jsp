<%--
    Document   : group-create
    Created on : May 5, 2016, 3:41:08 PM
    Author     : Suman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">

    <c:choose>
        <c:when test="${isSaved eq true}">
            <div class="alert alert-success" > <span><tags:message code="res.group.create.success.msg"/></span></div>
        </c:when>
        <c:when test="${isSaved eq false}">
            <div class="alert alert-success" > <span><tags:message code="res.group.create.error.msg"/></span></div>
        </c:when>
        <c:otherwise>  <div class="alert alert-success" id="meg" hidden="true"> <span><tags:message code="res.add.add.resource.type"/></span></div></c:otherwise>

    </c:choose>
    <div class="card ">
        <div class="card-header clearfix"> <span class="pull-left"> <tags:message code="res.create.group"/></span> <a href="${context}/resource/group/list" class="btn btn-secondary btn-sm pull-right">View Groups List</a></div>
        <div class="card-block clearfix">
            <form:form method="post" commandName="groupType"  id="groupFormId" style="text-align:center;">
                <div class="form-group row" style="text-align:center;">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  for="inputPassword"><tags:message code="res.group.type"/><em>*</em></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <form:input id="typeGroupId" path="groupType" cssClass="form-control" required="required" placeholder="Enter Group Name"onkeypress="return onlySpecificChar(event, this);"  onkeyup="findDuplicateGroupName();" />
                        <span class="text-danger font-bold" id="isduplicateId"> </span>
                    </div>
                </div>
                <input name="groupIcon" class="form-control" value="${groupIcon}" type="hidden" id="groupIconHidden"/>
                <div class="form-group row">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right"  for="inputPassword"><tags:message code="res.select.resource.type"/><em>*</em></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                <c:choose>
                                    <c:when test="${ not empty resourceTypeList}">
                                        <select id="resourceTypeList" multiple="multiple" size="8" class="form-control">
                                            <c:forEach var="resourceType" items="${resourceTypeList}">
                                                <option value="${resourceType.id}">${resourceType.resourceType}</option>
                                            </c:forEach>
                                        </select>
                                    </c:when>

                                </c:choose>
                            </div>
                            <div class="col-lg-1  col-md-3 col-sm-6 col-xs-12">
                                <button type="button" class="btn btn-secondary btn-sm btn-block m-t-3" onClick="addResourceType();"><i class="fa fa-chevron-right"></i></button>
                                <button type="button"  class="btn btn-secondary btn-sm btn-block m-b-2" onClick="removeResourceType();"><i class="fa fa-chevron-left"></i></button>

                            </div>
                            <div class="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                                <select size="8" class="form-control" id="alreadyAssignedRT" name="resourceTypes" multiple="multiple" >

                                    <c:forEach var="assignedRT" items="${alreadyAssingedRTList}">
                                        <option>${assignedRT.resourceType}</option>
                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </form:form>
            <input type="hidden" name="groupIcon" id="groupIconDivId" value="${groupType}"/>
            <div class="form-group row">
                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  for="inputPassword"><tags:message code="res.group.icon"/></label>
                <div class="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                    <div class="profile-img">
                        <form id="groupIconfForm" >
                            <a id="upload_group_icon_btn" href="#" onClick="callUploadIcon();" class="link2"><tags:message code="label.uploadIcon"/></a>
                            <input id="upload_group_icon" type="file" name="uploadfile" style="visibility: hidden"/>
                            <input name="groupTypeName" class="form-control" value="${groupName}" type="hidden" id="groupTypeHidden"/>
                        </form>
                        <span id="upload-file-message">
                            <c:if test="${not empty groupType.groupIcon}">
                                <img id="imageUploadSrc" src="${groupIconLocation}/${groupType.groupIcon}" height="100" width="100" />
                            </c:if>
                        </span>

                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer clearfix">
            <div class="text-center">
                <button type="submit" class="btn btn-primary" onclick="submitGroupForm();" id="grpBtnId" disabled="disabled"><tags:message code="label.save"/></button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#isduplicateId').hide();
        $("#upload_group_icon").on("change", uploadFile);
    });
    function onlySpecificChar(e, t) {
        var regex = new RegExp("^[a-zA-Z0-9-_\b\t ]*$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            // str = str.replace(/\s\s+/g, ' ');
            return true;
        }
        e.preventDefault();
    }
    function addResourceType() {
        $('#resourceTypeList option:selected').remove().appendTo('#alreadyAssignedRT')
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function removeResourceType() {
        $('#alreadyAssignedRT option:selected').remove().appendTo('#resourceTypeList')
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function submitGroupForm() {
        var alreadyassignValues = $("#alreadyAssignedRT").val();
        if (alreadyassignValues !== null) {
            $('#alreadyAssignedRT option').prop('selected', true);
            var action = "${context}/resource/group/save";
            $("#groupFormId").attr("action", action);
            $("#groupFormId").submit();

        } else {
            $('#meg').html("<tags:message code="res.add.add.resource.type"/>");
            // $('#meg').delay(1000).hide(500); 
            $('#meg').delay(2000).hide(500);
            $('#meg').removeAttr("hidden");
            //$("#myModal").modal("show");
        }
    }
    function findDuplicateGroupName() {
        var groupName = $('#typeGroupId').val();
        if (groupName.length > 3) {
            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/checkduplicate/group/" + groupName,
                cache: false,
                data: "groupName=" + groupName,
                success: function (data) {
                    if (data === true) {
                        $('#isduplicateId').show();
                        $('#isduplicateId').html("Group name already exists...");
                        $("#grpBtnId").attr("disabled", "disabled");
                    } else {
                        $('#isduplicateId').hide();
                        $("#grpBtnId").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }
            });
        } else {
            $('#isduplicateId').show();
            $('#isduplicateId').html("Group name require minimum 4 characters...");
        }

    }
    function callUploadIcon() {
        $("#upload_group_icon").click();
    }
    function uploadFile() {
        var groupName = $('#typeGroupId').val();
        $("#groupTypeHidden").attr("value", groupName);
        $("#upload-file-message").html("");
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/upload/group/icon",
            type: "POST",
            data: new FormData($("#groupIconfForm")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                // Handle upload success
                //$("#groupIconDivId").attr("value", data);
                $("#upload-file-message").html("");
                $("#groupIconHidden").val(data);
                $("#upload-file-message").html('<img id="imageUploadSrc" src="${groupIconLocation}/' + data + '" height="100" width="100" />');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Handle upload error
                alert(thrownError)
                $("#upload-file-message").text(
                        "File not uploaded (perhaps it's too much big)");
            }
        });
    } // function uploadFile
</script>

