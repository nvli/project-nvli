<%-- 
    Document   : oar-register
    Created on : Apr 25, 2016, 10:56:35 AM
    Author     : Suman
    Author     : Sujata Aher
    Author     : Savita Kakad
--%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="in.gov.nvli.domain.resource.Resource"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script src="${context}/scripts/libs/jquery-date-format.min.js" type="text/javascript"></script>
<%@include file="../../../static/components/pagination/pagination.html"%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="eventMsg"></div>
    <c:if test="${not empty orMessage}">
        <div class="alert alert-success"> <span>${orMessage}</span></div>
    </c:if>
    <c:if test="${not empty rescMessage}">
        <div class="alert alert-success"> <span>${rescMessage}</span></div>
    </c:if>  
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin ">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    ${resourceType.resourceType} <tags:message code="sb.resource.register"/>
                    <a href="${context}/resource/create" class="btn btn-success btn-sm"><tags:message code="res.create"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search Resource">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>                          
                        <th><tags:message code="label.serial.no"/></th>
                        <th ><tags:message code="res.icon"/></th>
                        <th><tags:message code="res.code"/></th>
                        <th><tags:message code="res.reponame"/></th>
                        <th><tags:message code="res.total.record.count"/></th>
                        <th><tags:message code="label.status"/></th>
                        <th><tags:message code="label.status"/> <tags:message code="label.View"/></th>
                        <th ><tags:message code="res.actions"/></th>
                    </tr>
                </thead>
                <form  id="hiddenForm" method="POST">
                    <tbody id="resource-tbl-body"></tbody>
                </form>
            </table>
        </div>
        <div id="resource-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<div class="modal fade" id="myModalDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Details</h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th><tags:message code="res.dor"/></th>
                            <th><tags:message code="res.baseUrl"/></th>
                            <th><tags:message code="res.last.harvest"/></th>
                            <th><tags:message code="res.record.count"/></th>
                        </tr>
                    </thead>
                    <tbody id="detailsTable">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><tags:message code="res.headding.metadata.standard"/> <span id="rcLabel"></span></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="metaTable">
                    <thead>
                        <tr>
                            <th><tags:message code="res.label.standard"/></th>
                            <th><tags:message code="res.label.enabled"/></th>
                        </tr>
                    </thead>
                    <tbody id="metadataStdTble">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="statusModalLabel"></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th><tags:message code="res.label.standard"/></th>
                            <th><tags:message code="res.status"/></th>
                            <th><tags:message code="res.status"/> <tags:message code="label.description"/></th>
                        </tr>
                    </thead>
                    <tbody id="statusDetailTable">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




<script type="text/template" id="tpl-resource-list">
    {{ _.each(resourceList, function (resource,i){ }}
    <tr>
    <td width="3%">{{=i+1+recordCount}}</td>
    <td align="center" width="150px">
    {{ if(resource.resourceIcon!==null && resource.resourceIcon!==""){}}
    <img id="imageUploadSrc" src="${resourceIconLocation}/{{=resource.resourceIcon}}" width="150px"/>
    {{ } else{}}
    <img id="imageUploadSrc" src="${resourceIconLocation}/news-paper_preview.jpg"  width="150px"/>
    {{ } }}
    </td>  
    <td width="9%">{{=resource.resourceCode}}</td>
    <td >{{ var urlLink=resource.repositoryUrl; }} 
    {{ if(urlLink === undefined || urlLink == null || urlLink.length <= 0  || urlLink ==='null') {}}
    {{urlLink='---';}else }}
    {{ { }}
    {{ if(urlLink.indexOf("http") === -1) {  }}
    {{  urlLink="http://"+urlLink;}}
    {{ } }} 
    {{ } }}
    <a href="{{=urlLink}}" target="_blank" class="link2">{{=resource.resourceName}}</a> 
    </td>
    <td width="12%">{{=resource.recordCount}}</td>
    <td id="{{=resource.id}}-status btn-group" align="center">            
    {{ if(resource.resourceStatus.statusCode==4 || resource.resourceStatus.statusCode==7){ }}
    <span class="label label-danger"> {{=resource.resourceStatus.statusDesc}}</span>
    {{ }else if(resource.resourceStatus.statusCode==1){ }}
    <span class="label label-warning">{{=resource.resourceStatus.statusDesc}}</span>
    {{ } else if(resource.resourceStatus.statusCode==3 || resource.resourceStatus.statusCode==6){ }}
    <span class="label label-info">{{=resource.resourceStatus.statusDesc}}</span>
    {{ }else{ }}
    <span class="label label-success"> {{=resource.resourceStatus.statusDesc}}
    {{ } }}</span>
    </td>
    <td width="8%" align="center">
     <button onclick="return getStatusDetails('{{=resource.resourceCode}}','{{=i}}')" class="btn btn-secondary btn-sm statusBtn" data-tooltip="Metadata Standards and Status Details"> <span class="fa fa-eye" aria-hidden="true"></span></button>
    </td>
    
    <td width="24%" align="center">
    <div class="btn-group">
    
    <button onclick="return getDetails('{{=i}}');" class="btn btn-secondary" data-tooltip="Extra Information View"> <span class="fa fa-eye" aria-hidden="true"></span></button>
   
    <button class="btn btn-secondary" data-tooltip="Edit Resource" onclick="editResourceForm('{{=resource.resourceCode}}');"><span class="fa fa-pencil" aria-hidden="true"></span></button>
    
    <a data-resource-code="{{=resource.resourceCode}}" data-resource-name="{{=resource.resourceName}}" href="#" class="btn btn-secondary delete-btn" data-tooltip="Delete Resource" ><span class="fa fa-trash" aria-hidden="true"></span></a>
    
    <span id="{{=resource.id}}-active">
    {{ if(resource.resourceStatus.statusCode==5 && resource.active){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Stop Record Processing" data-resource-id="{{=resource.id}}" data-event="deactivate" data-resource-name="{{=resource.resourceName}}" >
    <img src="${context}/images/disable.png" width="19">
    </a>
    {{ }else if(resource.resourceStatus.statusCode==5 && !resource.active){ }}
    <a  href="#" class="event-btn btn btn-secondary" data-tooltip="Start Record Processing" data-resource-id="{{=resource.id}}" data-event="activate" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/enable.png" width="19">
    </a>
    {{ } }}
    </span>
    
    <span id="{{=resource.id}}-publish">
    {{ if(resource.publish){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Unpublish Resource" data-resource-id="{{=resource.id}}" data-event="unpublish" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/withdraw.png" width="19">
    </a>
    {{ }else if(!resource.publish){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Publish Resource" data-resource-id="{{=resource.id}}" data-event="publish" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/publish.png" width="19">
    </a>
    {{ } }}
    </span>
    
    <span id="{{=resource.id}}-action">
    {{ if(resource.resourceStatus.statusCode==5 || resource.resourceStatus.statusCode==7 || resource.resourceStatus.statusCode==4) { }}
    <button data-tooltip="Start Incremental Harvesting of Repository" class="action-btn btn btn-secondary" data-resource-id="{{=resource.id}}" data-event="update" data-resource-name="{{=resource.resourceName}}"><img src="${context}/images/update-process.png" width="19"/> </button>
    {{ } else if(resource.resourceStatus.statusCode==2|| resource.resourceStatus.statusCode==9) { }}
    <button data-tooltip="Start Harvesting of Repository" class="action-btn btn btn-secondary" data-resource-id="{{=resource.id}}" data-event="harvest" data-resource-name="{{=resource.resourceName}}"><img src="${context}/images/start-process.png" width="19"/> </button>
    {{ } else if(resource.resourceStatus.statusCode==3 || resource.resourceStatus.statusCode==6) { }}
    <button  class="btn btn-secondary" data-tooltip="Repository Harvest Processing..."><img style="-webkit-user-select: none" width="20" src="${context}/themes/images/processing.gif"></button>
    {{ } }}
    </span>
    
    </div>
    </td>
    </tr> 
    {{ }); }}
</script>
<%--    <button onclick="return getMetaStd('{{=i}}');" class="btn btn-secondary" data-tooltip="Metadata Standards View"> <span class="fa fa-eye" aria-hidden="true"></span></button>--%>
<script src="${context}/scripts/resource-loading.js" type="text/javascript"></script>
<script type="text/javascript">
    _resourceTypeId = '${resourceType.id}';
    _resourceTypeCategory = '${resourceType.resourceTypeCategory}';
    $(document).ready(function ()
    {
        syncOpenRepositoryStatusFromHarvester();

        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });
        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function syncOpenRepositoryStatusFromHarvester() {
        $.ajax({
            url: '${context}/resource/syncRepoStatus',
            type: 'POST',
            success: function (data) {
                console.info("Synchronizing");
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

    function deleteOar() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-oar-button").attr("href");
        } else {
            return false;
        }
    }

    function getStatusDetails(resourceCode, loopInd)
    {
        $.ajax({
            url: '${context}/resource/getstatusdetails/' + resourceCode,
            type: 'POST',
            success: function (data) {
                if (data != null && data != "") {
                    var metadataList = JSON.parse(data);

                    var message;
                    if (metadataList.repo_status_discription != null) {
                        message = "Status of " + resourceCode + " :" + metadataList.repo_status_discription;
                    } else {
                        message = "Status of " + resourceCode + " :";
                    }
                    $("#statusModalLabel").html(message);
                    var str;
                    _.each(metadataList.metadata, function (metaDataStd) {
                        var standDesc = "-";

                        if (metaDataStd.status_desc != null) {
                            standDesc = metaDataStd.status_desc;
                        }
                        str = str + "<tr><td>" + metaDataStd.metadata_name + "</td><td>" + metaDataStd.status_name + "</td><td>" + standDesc + "</td></tr>"
                        console.log(str);
                    });
                    $("#statusDetailTable").html(str);
                    $("#statusModal").modal("show");
                } else {
                    getMetaStd(loopInd);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {

            }
        });


    }

    function getDetails(loopInd) {
        var resource = resourceListG[loopInd];
        var createDate = $.format.date(new Date(resource.createdOn), "dd-MM-yyyy");
        console.log(resource.harvestStartTime);
        var harvestTime;
        if (resource.harvestStartTime !== null) {
            harvestTime = $.format.date(new Date(resource.harvestStartTime), "dd-MM-yyyy");
        } else {
            harvestTime = "-";
        }
        var str = "<tr><td>" + createDate + "</td><td>" + resource.harvestUrl + "</td><td>" + harvestTime + "</td><td>" + resource.recordCount + "</td></tr>";
        $("#detailsTable").html(str);
        $("#myModalDetails").modal("show");
    }

    function getMetaStd(loopInd) {
        var str = "";
        var metadataList = resourceListG[loopInd].recordMetadataStandards;
        _.each(metadataList, function (metaDataStd) {
            str = str + "<tr><td>" + metaDataStd.metadataType + "</td><td>" + metaDataStd.metadataEnabled + "</td></tr>";
        });
        $("#metadataStdTble").html(str);
        $("#rcLabel").html("of " + resourceListG[loopInd].resourceCode);
        $("#myModal").modal("show");
    }

    function actionOnSelectedRepo(event) {
        var action = "${context}/resource/" + event + "/selected";
        $("#hiddenForm").attr("action", action);
        $("#hiddenForm").submit();
    }

    function editResourceForm(resourceCode) {
        var action = "${context}/resource/edit/" + resourceCode + "?type=-1";
        $("#hiddenForm").attr("action", action);
        $("#hiddenForm").submit();
    }
</script>
