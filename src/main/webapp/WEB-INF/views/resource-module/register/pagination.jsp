<%-- 
    Document   : This pagination page is used for pagination of all resource register.
    Created on : Jun 10, 2016, 10:01:29 AM
    Author     : Sujata Aher
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<ul class="pagination pagination-sm ">
    <c:choose>
        <c:when test="${(totalpages eq 0) || (pageNum eq 1)}">
            <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="Previous"> 
                    <span aria-hidden="true">&laquo;</span> 
                    <span class="sr-only"><tags:message code="label.previous"/></span>
                </a> 
            </li>
        </c:when>
        <c:otherwise>
            <li class="page-item"> 
                <a class="page-link" href="#" aria-label="Previous" onClick="submitFormForPagination('${pageNum - 1}', '${pageWin}');">
                    <span aria-hidden="false">&laquo;</span> 
                    <span class="sr-only"><tags:message code="label.previous"/></span>
                </a> 
            </li>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${totalpages eq 0}">
            <li class="page-item active"> 
                <a class="page-link" href="#">1 
                    <span class="sr-only">(<tags:message code="label.current"/>)</span>
                </a> 
            </li>
        </c:when>
        <c:otherwise>
            <c:choose>
                <c:when test="${totalpages lt 3}">
                    <c:forEach begin="1" end="${totalpages}" var="i">
                        <c:choose>
                            <c:when test="${pageNum eq i}">
                                <li class="page-item active"> 
                                    <a class="page-link" href="#">${i} 
                                        <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                    </a> 
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item"> 
                                    <a class="page-link" href="#" onClick="submitFormForPagination('${i}', '${pageWin}');">${i} 
                                        <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                    </a> 
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <c:set var="begin" value="1"/>
                    <c:set var="end" value="${totalpages}"/>
                    <c:if test="${pageNum lt 3}">
                        <c:set var="end" value="3"/> 
                    </c:if>
                    <c:if test="${(pageNum ge 3)}">
                        <c:set var="begin" value="${pageNum-1}"/>
                        <c:set var="end" value="${pageNum+1}"/>
                    </c:if>
                    <c:if test="${(totalpages-1) lt pageNum}">
                        <c:set var="end" value="${totalpages}"/>
                    </c:if>
                    <c:forEach begin="${begin}" end="${end}" var="i">
                        <c:choose>
                            <c:when test="${pageNum eq i}">
                                <li class="page-item active"> 
                                    <a class="page-link" href="#">${i} 
                                        <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                    </a> 
                                </li>  
                            </c:when>
                            <c:otherwise>
                                <li class="page-item"> 
                                    <a class="page-link" href="#" onClick="submitFormForPagination('${i}', '${pageWin}');">${i} 
                                        <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                    </a> 
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${(pageNum lt totalpages)}">
            <li class="page-item"> 
                <a class="page-link" href="#" aria-label="Next" onClick="submitFormForPagination('${pageNum + 1}', '${pageWin}');"> 
                    <span aria-hidden="true">&raquo;</span> 
                    <span class="sr-only"><tags:message code="label.next"/></span> 
                </a> 
            </li>
        </c:when>
        <c:otherwise>
            <li class="page-item disabled"> 
                <a class="page-link" href="#" aria-label="Next"> 
                    <span aria-hidden="true">&raquo;</span> 
                    <span class="sr-only"><tags:message code="label.next"/></span> 
                </a> 
            </li>
        </c:otherwise>
    </c:choose>
</ul>

<script type="text/javascript">
    function submitFormForPagination(pageNum, pageWindow) {
        var resourceTypeId = "${resourceType.id}";
        $("#hiddenForm").attr("method", "POST")
        $("#hiddenForm").attr("action", "${pageContext.request.contextPath}/resource/register/" + resourceTypeId + "/" + pageNum + "/" + pageWindow);
        $("#hiddenForm").submit();
    }
</script>


