<%-- 
    Document   : group-register
    Created on : May 6, 2016, 5:14:06 PM
    Author     : Suman Behara <sumanb@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%
    int pageNum = Integer.parseInt(String.valueOf(request.getAttribute("pageNum")));
    int pageWin = Integer.parseInt(String.valueOf(request.getAttribute("pageWin")));
    int serialNo = 1;
    int pageNo = pageWin * (pageNum - 1);
%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="card">
            <div class="card-header"><tags:message code="res.headding.grpResReg"/>
                <div class="pull-rights font12"> 
                    <input id="searchString" placeholder="Search Group">
                    Per Page
                    <select id="limitResourceType">
                        <option  data-link="#" value="20">20</option>
                        <option  data-link="#" value="30">30</option>
                        <option data-link="#" value="50">50</option>
                    </select>
                </div>
            </div>
            <div class="card-blojck">
                <div class="table-responsive">
                    <form method="post" id="groupTypeHiddenForm">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="text-align:center"><span><tags:message code="label.serial.no"/></span></th>
                                    <th style="text-align:center"><tags:message code="res.group.type"/></th>
                                    <th style="text-align:center"><tags:message code="label.resource"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${groupList}" var="groupType">
                                    <tr>
                                        <td>
                                            <c:choose>
                                                <c:when test="${pageNum == 1}" >
                                                    <%=serialNo%>
                                                </c:when>
                                                <c:otherwise>
                                                    <%=serialNo + pageNo%>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td>
                                            ${groupType.groupType}
                                        </td>
                                        <td>
                                            <c:forEach items="${groupType.resources}" var="resource">
                                                ${resource.resourceName}<br>
                                            </c:forEach>
                                        </td>
                                    </tr>  
                                    <%serialNo++;%>
                                </c:forEach>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="card-footer clearfix"> <span class="pull-left">
                </span> <span class="pull-right">
                    <ul class="pagination pagination-sm ">
                        <c:choose>
                            <c:when test="${(totalpages eq 0) || (pageNum eq 1)}">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" aria-label="Previous"> 
                                        <span aria-hidden="true">&laquo;</span> 
                                        <span class="sr-only"><tags:message code="label.previous"/></span>
                                    </a> 
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item"> 
                                    <a class="page-link" href="#" aria-label="Previous" onClick="submitFormForGroupRegisterPagination('${pageNum - 1}', '${pageWin}');">
                                        <span aria-hidden="false">&laquo;</span> 
                                        <span class="sr-only"><tags:message code="label.previous"/></span>
                                    </a> 
                                </li>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${totalpages eq 0}">
                                <li class="page-item active"> 
                                    <a class="page-link" href="#">1 
                                        <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                    </a> 
                                </li>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${totalpages lt 3}">
                                        <c:forEach begin="1" end="${totalpages}" var="i">
                                            <c:choose>
                                                <c:when test="${pageNum eq i}">
                                                    <li class="page-item active"> 
                                                        <a class="page-link" href="#">${i} 
                                                            <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                                        </a> 
                                                    </li>
                                                </c:when>
                                                <c:otherwise>
                                                    <li class="page-item"> 
                                                        <a class="page-link" href="#" onClick="submitFormForGroupRegisterPagination('${i}', '${pageWin}');">${i} 
                                                            <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                                        </a> 
                                                    </li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="begin" value="1"/>
                                        <c:set var="end" value="${totalpages}"/>
                                        <c:if test="${pageNum lt 3}">
                                            <c:set var="end" value="3"/> 
                                        </c:if>
                                        <c:if test="${(pageNum ge 3)}">
                                            <c:set var="begin" value="${pageNum-1}"/>
                                            <c:set var="end" value="${pageNum+1}"/>
                                        </c:if>
                                        <c:if test="${(totalpages-1) lt pageNum}">
                                            <c:set var="end" value="${totalpages}"/>
                                        </c:if>
                                        <c:forEach begin="${begin}" end="${end}" var="i">
                                            <c:choose>
                                                <c:when test="${pageNum eq i}">
                                                    <li class="page-item active"> 
                                                        <a class="page-link" href="#">${i} 
                                                            <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                                        </a> 
                                                    </li>  
                                                </c:when>
                                                <c:otherwise>
                                                    <li class="page-item"> 
                                                        <a class="page-link" href="#" onClick="submitFormForGroupRegisterPagination('${i}', '${pageWin}');">${i} 
                                                            <span class="sr-only">(<tags:message code="label.current"/>)</span>
                                                        </a> 
                                                    </li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${(pageNum lt totalpages)}">
                                <li class="page-item"> 
                                    <a class="page-link" href="#" aria-label="Next" onClick="submitFormForGroupRegisterPagination('${pageNum + 1}', '${pageWin}');"> 
                                        <span aria-hidden="true">&raquo;</span> 
                                        <span class="sr-only"><tags:message code="label.next"/></span> 
                                    </a> 
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item disabled"> 
                                    <a class="page-link" href="#" aria-label="Next"> 
                                        <span aria-hidden="true">&raquo;</span> 
                                        <span class="sr-only"><tags:message code="label.next"/></span> 
                                    </a> 
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </span> </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function ()
    {
        var pageWin = '${pageWin}';
        $("#limit option[value='" + pageWin + "']").attr('selected', 'selected');
        $("#limit").change(function () {
            var pageWindow = $(this).val();
            $("#groupTypeHiddenForm").attr("method", "POST")
            $("#groupTypeHiddenForm").attr("action", "${pageContext.request.contextPath}/resource/group/register/1/" + pageWindow);
            $("#groupTypeHiddenForm").submit();
        });
    });
    function submitFormForGroupRegisterPagination(pageNum, pageWindow) {

        $("#groupTypeHiddenForm").attr("method", "POST")
        $("#groupTypeHiddenForm").attr("action", "${pageContext.request.contextPath}/resource/group/register/" + pageNum + "/" + pageWindow);
        $("#groupTypeHiddenForm").submit();
    }
</script>
