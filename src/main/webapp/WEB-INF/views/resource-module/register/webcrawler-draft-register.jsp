<%-- 
    Document   : webcrawler-draft-register
    Created on : Jul 18, 2017, 10:06:56 AM
    Author     : savita kakad
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script src="${context}/scripts/libs/jquery-date-format.min.js" type="text/javascript"></script>
<%@include file="../../../static/components/pagination/pagination.html"%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
               ${resourceType.resourceType} <tags:message code="res.label.drResReg"/>
                <a href="${context}/resource/create" class="btn btn-success"><tags:message code="res.create"/></a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <input id="searchString" class="form-control" placeholder="Search Resource">
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                <select id="limitFilter" class="form-control">
                    <option value="10" selected>10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%"><tags:message code="label.serial.no"/></th>
                    <th width="50" ><tags:message code="res.icon"/></th>
                    <th><tags:message code="label.subinfo.resourceName"/></th>
                    <th><tags:message code="res.web.url"/></th>
                    <th><tags:message code="res.dor"/></th>
                    <th><tags:message code="label.status"/></th>
                    <th><tags:message code="res.actions"/></th>
                    <th><tags:message code="res.edit.delete"/></th>
                </tr>
            </thead>
            <form  id="hiddenFormDraftRegister" method="POST">
                <tbody id="webcrawlerdraft-tbl-body"></tbody>
            </form>
        </table>
        <div id="webcrawlerdraft-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    var _resourceTypeId = '${resourceType.id}';
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/draft/register/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo, resourceTypeId: _resourceTypeId},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.resourceList)) {
                        $("#webcrawlerdraft-tbl-body").html('<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-webcrawlerdraft-list").html());

                        var Opts = {
                            "resourceList": jsonObject.resourceList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val()),
                            "resourceTypeId":_resourceTypeId
                        };
                        console.log(Opts);
                        var articleTableBody = articlesTpl(Opts);
                        $("#webcrawlerdraft-tbl-body").empty();
                        $("#webcrawlerdraft-tbl-body").html(articleTableBody);
                    }
                    renderPagination(fetchAndRenderTableData, "webcrawlerdraft-tbl-footer", "page-btn",pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-webcrawlerdraft-list">
    {{ _.each(resourceList, function (resource,i){ }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>
    {{ if(resource.resourceIcon!==null && resource.resourceIcon!==""){}}
    <img id="imageUploadSrc" src="${pageContext.request.contextPath}/resource/get/icon/{{=resource.resourceIcon}}" height="50"/>
    {{ } else{}}
    <img id="imageUploadSrc" src="${pageContext.request.contextPath}/images/icons/no-preview-available.jpg" height="50"/>   
    {{ } }}
    </td>
    <td >{{ var urlLink=resource.webUrl; }}
        {{ if(urlLink === undefined || urlLink == null || urlLink.length <= 0  || urlLink ==='null') {}}
        {{urlLink='---';}else }}
        {{ { }}
        {{ if(urlLink.indexOf("http") === -1) {  }}
        {{  urlLink="http://"+urlLink;}}
        {{ } }} 
        {{ } }}
        <a href="{{=urlLink}}" target="_blank" class="link2">{{=resource.resourceName}}</a> 
    </td>
    <td >
    {{=resource.webUrl}}
    </td>
    <td>
    {{if(resource.createdOn!=null){}}
    {{=$.format.date(new Date(resource.createdOn), "dd-MM-yyyy")}}
    {{}else{ }} - {{ } }}
    </td>
    <td>
    {{=resource.resourceStatus.statusDesc}}
    </td>
    <td>
    {{if(resource.resourceStatus.statusCode==9){}}
    <a href="#" onclick="resubmitURL('{{=resource.id}}', '{{=resourceTypeId}}');"  class="link2"><tags:message code="button.re-submit"/></a>
    {{ } }}
    </td>
    <td>
    <a href="#" class="btn btn-secondary" title="Edit" onclick="editResourceForm('{{=resource.resourceCode}}');">
    <span class="fa fa-pencil" aria-hidden="true"></span></a>
    <a data-resource-code="{{=resource.resourceCode}}" data-resource-name="{{=resource.resourceName}}" href="#" class="btn btn-secondary delete-btn" data-tooltip="Delete Resource" ><span class="fa fa-trash" aria-hidden="true"></span></a>
    </td>
    </tr>
    {{ }); }}
</script>
<script type="text/javascript">
 function submitFormForDraftRegisterPagination(pageNum, pageWindow) {

        $("#hiddenFormDraftRegister").attr("method", "POST")
        $("#hiddenFormDraftRegister").attr("action", "${pageContext.request.contextPath}/resource/group/list/" + pageNum + "/" + pageWindow);
        $("#hiddenFormDraftRegister").submit();
    }

    function resubmitURL(resourceId, resourceTypeId) {
        var action = "${context}/resource/resubmit/" + resourceTypeId + "/" + resourceId;
        $("#hiddenFormDraftRegister").attr("action", action);
        $("#hiddenFormDraftRegister").submit();
    }

    function editResourceForm(resourceCode) {
        var action = "${context}/resource/edit/" + resourceCode+"?type=1";
        $("#hiddenFormDraftRegister").attr("action", action);
        $("#hiddenFormDraftRegister").submit();
    }
</script>
