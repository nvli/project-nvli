<%-- 
    Document   : this limit-pagination is used for limit window of all resource register.
    Created on : Jun 10, 2016, 10:34:58 AM
    Author     : Sujata Aher
--%>
Per Page :
<select id="limit">
    <option  data-link="#" value="20">20</option>
    <option  data-link="#" value="30">30</option>
    <option data-link="#" value="50">50</option>
</select>
<script type="text/javascript">
    $(document).ready(function ()
    {
        var pageWin = '${pageWin}';
        $("#limit option[value='" + pageWin + "']").attr('selected', 'selected');
        $("#limit").change(function () {
            var resourceTypeId = "${resourceType.id}"
            var pageWindow = $(this).val();
            $("#hiddenForm").attr("method", "POST")
            $("#hiddenForm").attr("action", "${pageContext.request.contextPath}/resource/register/" + resourceTypeId + "/1/" + pageWindow);
            $("#hiddenForm").submit();
        });
    });
</script>
