<%-- 
    Document   : library-register
    Created on : May 25, 2016, 12:16:16 PM
    Author     : Suman Behara <sumanb@cdac.in>
    Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script src="${context}/scripts/libs/jquery-date-format.min.js" type="text/javascript"></script>
<%@include file="../../../static/components/pagination/pagination.html"%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="eventMsg"></div>
    <c:if test="${not empty libMessage}">
        <div class="alert alert-success"> <span>${libMessage}</span></div>
    </c:if>
    <c:if test="${not empty rescMessage}">
        <div class="alert alert-success"> <span>${rescMessage}</span></div>
    </c:if>  
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin ">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    ${resourceType.resourceType} <tags:message code="label.register"/>  
                    <a href="${context}/resource/create" class="btn btn-success btn-sm"><tags:message code="res.create"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search Resource">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th><tags:message code="label.serial.no"/></th>
                        <th><tags:message code="res.icon"/></th>
                        <th><tags:message code="res.code"/></th>
                        <th><tags:message code="label.subinfo.resourceName"/></th>
                        <th><tags:message code="res.dor"/></th>
                        <th><tags:message code="res.total.record.count"/></th>
                        <th><tags:message code="res.actions"/></th>
                    </tr>
                </thead>
                <form  id="hiddenForm" method="POST">
                    <tbody id="resource-tbl-body"></tbody>
                </form>
            </table>
        </div>
        <div id="resource-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    _resourceTypeId = '${resourceType.id}';
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });


    function deleteLibrary() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-library-button").attr("href");
        } else {
            return false;
        }
    }

</script>
<script type="text/template" id="tpl-resource-list">
    {{ _.each(resourceList, function (resource,i){ }}
    <tr>
    <td width="3%">{{=i+1+recordCount}}</td>
    <td align="center" width="150px">
    {{ if(resource.resourceIcon!==null && resource.resourceIcon!==""){}}
    <img id="imageUploadSrc" src="${resourceIconLocation}/{{=resource.resourceIcon}}" width="150px"/>
    {{ } else{}}
    <img id="imageUploadSrc" src="${resourceIconLocation}/news-paper_preview.jpg"  width="150px"/>
    {{ } }}
    </td>  
    <td  width="9%">{{=resource.resourceCode}}</td>
    <td >{{ var urlLink=null ;if(resource.organization!==null){urlLink=resource.organization.organisationUrl; } }} 
    {{ if(urlLink === undefined || urlLink == null || urlLink.length <= 0  || urlLink ==='null') {}}
    {{urlLink='---';}else }}
    {{ { }}
    {{ if(urlLink.indexOf("http") === -1) {  }}
    {{  urlLink="http://"+urlLink;}}
    {{ } }} 
    {{ } }}
    <a href="{{=urlLink}}" target="_blank" class="link2">{{=resource.resourceName}}</a> 
    </td>
    <td width="9%">
    {{if(resource.createdOn!=null){}}
    {{=$.format.date(new Date(resource.createdOn), "dd-MM-yyyy")}}
    {{}else{ }} - {{ } }}
    </td>
    <td width="12%">
    {{=resource.recordCount}}
    </td>
    <td width="18%">
    <span id="{{=resource.id}}-active">
    {{ if(resource.active){ }}
    <a  href="#" class="event-btn btn btn-secondary" data-tooltip="Stop Record Processing" data-resource-id="{{=resource.id}}" data-event="deactivate" data-resource-name="{{=resource.resourceName}}" >
    <img src="${context}/images/disable.png">
    </a>
    {{ }else if(!resource.active){ }}
    <a  href="#" class="event-btn btn btn-secondary" data-tooltip="Start Record Processing" data-resource-id="{{=resource.id}}" data-event="activate" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/enable.png">
    </a>
    {{ } }}
    </span>
    <span id="{{=resource.id}}-publish">
    {{ if(resource.publish){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Unpublish Resource" data-resource-id="{{=resource.id}}" data-event="unpublish" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/withdraw.png">
    </a>
    {{ }else if(!resource.publish){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Publish Resource" data-resource-id="{{=resource.id}}" data-event="publish" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/publish.png">
    </a>
    {{ } }}
    </span>
    <a href="#" class="btn btn-secondary" data-tooltip="Edit resource" onclick="editResourceForm('{{=resource.resourceCode}}');">
    <span class="fa fa-pencil" aria-hidden="true"></span></a>
    <a data-resource-code="{{=resource.resourceCode}}" data-resource-name="{{=resource.resourceName}}" href="#" class="btn btn-secondary delete-btn" data-tooltip="Delete Resource" ><span class="fa fa-trash" aria-hidden="true"></span></a>
    </td>
    </tr>
    {{ }); }}
</script>
<script src="${context}/scripts/resource-loading.js" type="text/javascript"></script>
<script type="text/javascript">
    function editResourceForm(resourceCode) {
        var action = "${context}/resource/edit/" + resourceCode + "?type=-1";
        $("#hiddenForm").attr("action", action);
        $("#hiddenForm").submit();
    }
</script>
