<%--
    Document   : enews-register
    Created on : May 30, 2016, 12:12:30 PM
    Author     : Suman Behara <sumanb@cdac.in>
    Author     : Savita Kakad
--%>
<%@page import="in.gov.nvli.domain.resource.Resource"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script src="${pageContext.request.contextPath}/scripts/libs/jquery-date-format.min.js" type="text/javascript"></script>
<%@include file="../../../static/components/pagination/pagination.html"%>

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="eventMsg"></div>
    <c:if test="${not empty rescMessage}">
        <div class="alert alert-success" > <span>${rescMessage}</span></div>
    </c:if> 
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin ">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <tags:message code="res.heading.enewsReg"/>
                    <a href="${context}/resource/create" class="btn btn-success btn-sm"><tags:message code="res.create"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search Resource">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th ><tags:message code="label.serial.no"/></th>
                        <th><tags:message code="res.icon"/></th>
                        <th><tags:message code="res.code"/></th>
                        <th><tags:message code="res.enews.name"/></th>
                        <th><tags:message code="res.total.record.count"/></th>
                        <th><tags:message code="res.dor"/></th>
                        <th><tags:message code="res.actions"/></th>
                    </tr>
                </thead>
                <form  id="hiddenForm" method="POST">
                    <tbody id="resource-tbl-body"></tbody>
                </form>
            </table>
        </div>
        <div id="resource-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><span id="feedLabel"></span> rss feeds </h4>
            </div>
            <div class="modal-body mod_scroll" id="feedTableDiv">

                <div class="table-responsive" >

                    <table class="table table-bordered" >
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Url</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody id="feedTable">

                        </tbody>

                    </table>

                </div>
            </div>
            <div class="modal-body mod_scroll" id="feedTableDiv2">
                <div class="table-responsive" >
                    <form:form id='hiddenFeedForm' method='post' commandName="newsFeeds">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Url</th>
                                    <th>Status</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody id="feedTable2">

                                <!--<input type="hidden" value="" id="forFeedUrl2" name="feedUrlInForm" class="form-control"/>-->
                            <input type="hidden" value="" id="refreshFeeds" name="refreshFeeds" class="form-control"/>
                            <input type="hidden" value="" id="resourceCodeInFeed" name="resourceCodeInFeed" class="form-control"/>

                            </tbody>
                        </table>
                    </form:form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onClick="saveNewsFeeds();" >save</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/template" id="tpl-resource-list">
    {{ _.each(resourceList, function (resource,i){ }}
    <tr>
    <td width="3%">{{=i+1+recordCount}}</td>
    <td align="center" width="150px">
    {{ if(resource.resourceIcon!==null && resource.resourceIcon!==""){}}
    <img id="imageUploadSrc" src="${resourceIconLocation}/{{=resource.resourceIcon}}" width="150px"/>
    {{ } else{}}
    <img id="imageUploadSrc" src="${resourceIconLocation}/news-paper_preview.jpg"  width="150px"/>
    {{ } }}
    </td>  
    <td  width="9%">{{=resource.resourceCode}}</td>
    <td >{{ var urlLink; if(resource.organisation!=null){urlLink=resource.organisation.organisationUrl;} }}
    {{ if(urlLink === undefined || urlLink == null || urlLink.length <= 0  || urlLink ==='null') {}}
    {{urlLink='---';}else }}
    {{ { }}
    {{ if(urlLink.indexOf("http") === -1) {  }}
    {{  urlLink="http://"+urlLink;}}
    {{ } }} 
    {{ } }}
    <a href="{{=urlLink}}" target="_blank" class="link2">{{=resource.resourceName}}</a> 
    </td>
    <td width="12%">{{=resource.recordCount}}</td>
    <td width="7%">
    {{if(resource.createdOn!=null){}}
    {{=$.format.date(new Date(resource.createdOn), "dd-MM-yyyy")}}
    {{}else{ }} - {{ } }}
    </td>
    <td width="23%">
    <a href="#" class="btn btn-secondary" onclick="getFeedsRegister('{{=i}}')" data-tooltip="View Feeds"> <span class="fa fa-eye" aria-hidden="true"></span></a>
    <a href="#" class="btn btn-secondary" data-tooltip="Refresh Feeds" onclick="refreshFeeds('{{=resource.resourceCode}}');"><span class="fa fa-refresh" aria-hidden="true"></span></a>
    <span id="{{=resource.id}}-active">
    {{ if(resource.active){ }}
    <a  href="#" class="event-btn btn btn-secondary" data-tooltip="Disable Resource" data-resource-id="{{=resource.id}}" data-event="deactivate" data-resource-name="{{=resource.resourceName}}" >
    <img src="${context}/images/disable.png">
    </a>
    {{ }else if(!resource.active){ }}
    <a  href="#" class="event-btn btn btn-secondary" data-tooltip="Enable Resource" data-resource-id="{{=resource.id}}" data-event="activate" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/enable.png">
    </a>
    {{ } }}
    </span>
    <span id="{{=resource.id}}-publish">
    {{ if(resource.publish){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Unpublish Resource" data-resource-id="{{=resource.id}}" data-event="unpublish" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/withdraw.png">
    </a>
    {{ }else if(!resource.publish){ }}
    <a href="#" class="event-btn btn btn-secondary" data-tooltip="Publish Resource" data-resource-id="{{=resource.id}}" data-event="publish" data-resource-name="{{=resource.resourceName}}">
    <img src="${context}/images/publish.png">
    </a>
    {{ } }}
    </span>
    <a href="#" class="btn btn-secondary" data-tooltip="Edit resource" onclick="editResourceForm('{{=resource.resourceCode}}');">
    <span class="fa fa-pencil" aria-hidden="true"></span></a>
    <a data-resource-code="{{=resource.resourceCode}}" data-resource-name="{{=resource.resourceName}}" href="#" class="btn btn-secondary delete-btn" data-tooltip="Delete Resource" ><span class="fa fa-trash" aria-hidden="true"></span></a>
    </td>
    </tr>
    {{ }); }}
</script>

<script src="${context}/scripts/resource-loading.js" type="text/javascript"></script>
<script type="text/javascript">
                        _resourceTypeId = '${resourceType.id}';
                        $(document).ready(function ()
                        {
                            fetchAndRenderTableData(1);
                            $("#searchString").unbind().bind("keyup", function () {
                                fetchAndRenderTableData(1);
                            });

                            $("#limitFilter").unbind().bind("change", function () {
                                fetchAndRenderTableData(1);
                            });
                        });

                        function editResourceForm(resourceCode) {
                            var action = "${context}/resource/edit/" + resourceCode + "?type=-1";
                            $("#hiddenForm").attr("action", action);
                            $("#hiddenForm").submit();
                        }

                        function getFeedsRegister(value) {
                            $("#feedTableDiv2").hide();
                            $("#feedTableDiv").show();
                            if (value !== "") {
                                var newsFeeds = resourceListG[value].newsFeedUrl;
                                var obj = JSON.parse(newsFeeds);
                                var str = "";
                                var isAvtiveStr;
                                if (obj !== null) {
                                    for (var i = 0; i < obj.length; i++) {
                                        if (obj[i].activeFeed) {
                                            isAvtiveStr = '<span class="fa fa-check" aria-hidden="true"></span>';
                                        } else {
                                            isAvtiveStr = '<span class="fa fa-times" aria-hidden="true"></span>';
                                        }
                                        str = str + "<tr><td>" + obj[i].labelName + "</td><td>" + obj[i].feedUrl + "</td><td>" + isAvtiveStr + "</td></tr>";
                                    }
                                } else {
                                    str = str + '<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No feeds available</b></td></tr>';
                                }
                                $("#feedLabel").html(resourceListG[value].resourceName);
                                $("#feedTable").html(str);
                                $("#myModal").modal("show");
                            } else {
                                alert("No feeds available")
                            }
                        }

                        function refreshFeeds(resourceCode) {
                            var feedURL = "${pageContext.request.contextPath}/resource/refresh/autocapture/newsfeed/" + resourceCode;
                            var isAvtiveStr;
                            $.ajax(
                                    {
                                        type: 'POST',
                                        url: feedURL,
                                        cache: false,
                                        success: function (data) {
                                            $("#feedTableDiv").hide();
                                            $("#feedTableDiv2").show();
                                            for (var i = 0; i < data.length; i++) {
                                                var divIdCount = i;
                                                if (data[i].activeFeed) {
                                                    isAvtiveStr = '<span class="fa fa-check" aria-hidden="true"></span>';
                                                } else {
                                                    isAvtiveStr = '<span class="fa fa-times" aria-hidden="true"></span>';
                                                }
                                                $("<tr />", {id: divIdCount + "newsFeedsUrlDiv", 'class': "newsfeedClass"}).appendTo('#feedTable2');
                                                $("<td />", {id: divIdCount + "newsLabelDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
                                                //  $("<label />", {id: divIdCount + "newsLabelLabel", 'class': "form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"}).html("Category").appendTo('#' + divIdCount + "newsLabelDiv");
                                                $("<div />", {id: divIdCount + "newsLabelDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsLabelDiv");
                                                $("<input />", {id: divIdCount + "newsFeedLabelId", value: data[i].labelName, 'class': "form-control", type: "text", name: "newsFeeds[" + divIdCount + "].labelName"}).appendTo("#" + divIdCount + "newsLabelDivIn");
                                                $("<td />", {id: divIdCount + "newsUrlDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
                                                // $("<label />", {id: divIdCount + "newsUrlLabel", 'class': "form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"}).html("URL").appendTo('#' + divIdCount + "newsUrlDiv");
                                                $("<div />", {id: divIdCount + "newsUrlDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsUrlDiv");
                                                $("<input />", {id: divIdCount + "newsUrlId", value: data[i].feedUrl, 'class': "form-control", type: "text", name: "newsFeeds[" + divIdCount + "].feedUrl"}).appendTo("#" + divIdCount + "newsUrlDivIn");
                                                $("<input />", {id: divIdCount + "newsUrlId", value: data[i].activeFeed, 'class': "form-control", type: "hidden", name: "newsFeeds[" + divIdCount + "].activeFeed"}).appendTo("#" + divIdCount + "newsUrlDivIn");
                                                $('<td />', {id: divIdCount + "newsUrlIdspan"}).html(isAvtiveStr + '<input type="hidden" value="' + data[i].activeFeed + '" id="forFeedUrl2" name: "newsFeeds["' + divIdCount + '"].activeFeed" class="form-control"/>').appendTo("#" + divIdCount + "newsFeedsUrlDiv");
                                                $('<td />', {id: divIdCount + "newsUrlIdspan"}).html('<button type="button" class="btn btn-primary" onClick=\"removeTag(\'' + i + 'newsFeedsUrlDiv\')\"><span class="fa fa-trash" aria-hidden="true"></button>').appendTo("#" + divIdCount + "newsFeedsUrlDiv");
                                            }
                                            $("#refreshFeeds").attr('value', data)
                                            $("#resourceCodeInFeed").attr('value', resourceCode);
                                            $("#myModal").modal("show");
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            alert("Could not auto capture feeds for this URl. Please add manually.");
                                        }
                                    });
                        }
                        function removeTag(divId) {
                            $("#" + divId).remove();
                            saveNewsFeeds();
                        }
                        function saveNewsFeeds() {
                            var resourceCode = $("#resourceCodeInFeed").val();
                            var feedURL = "${pageContext.request.contextPath}/resource/refresh/newsfeed/save/" + resourceCode;
                            var resourceForm = $("#hiddenFeedForm").serialize();
                            $.ajax(
                                    {
                                        type: 'POST',
                                        url: feedURL,
                                        data: resourceForm,
                                        cache: false,
                                        success: function (data) {
                                            alert("Successfully Saved.");
                                            $("#newsFeedUrlsID").attr("value", data);
                                            location.reload();
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            alert("Error in save feeds.");
                                        }
                                    });
                        }
</script>

