<%-- 
    Document   : oar-draft-register
    Created on : Jun 7, 2016, 4:55:34 PM
    Author     : Sujata 
    Author     : Savita Kakad 
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script src="${context}/scripts/libs/jquery-date-format.min.js" type="text/javascript"></script>
<%@include file="../../../static/components/pagination/pagination.html"%>
<form id="hiddenForm" method="post"></form>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
              <tags:message code="res.label.draftreg"/>
                <a href="${context}/resource/create" class="btn btn-success"><tags:message code="res.create"/></a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <input id="searchString" class="form-control" placeholder="Search Open Repository">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                <select id="limitFilter" class="form-control">
                    <option value="10" selected>10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%"><tags:message code="label.serial.no"/></th>
                    <th width="50" ><tags:message code="res.icon"/></th>
                    <th><tags:message code="res.reponame"/></th>
                    <th><tags:message code="res.dor"/></th>
                    <th><tags:message code="res.baseUrl"/></th>
                    <th><tags:message code="label.status"/></th>
                    <th><tags:message code="res.actions"/></th>
                    <th><tags:message code="res.edit.delete"/></th>
                </tr>
            </thead>
            <form  id="hiddenFormDraftRegister" method="POST">
                <tbody id="oardraft-tbl-body"></tbody>
            </form>
        </table>
        <div id="oardraft-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><tags:message code="res.headding.metadata.standard"/></h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="metaTable">
                    <thead>
                        <tr>
                            <th><tags:message code="res.label.standard"/></th>
                            <th><tags:message code="res.label.enabled"/></th>
                        </tr>
                    </thead>
                    <tbody id="feedTable">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var _resourceTypeId = '${resourceType.id}';
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/draft/register/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo, resourceTypeId: _resourceTypeId},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.resourceList)) {
                        $("#oardraft-tbl-body").html('<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-oardraft-list").html());

                        var Opts = {
                            "resourceList": jsonObject.resourceList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val()),
                            "resourceTypeId":_resourceTypeId
                        };
                        console.log(Opts);
                        var articleTableBody = articlesTpl(Opts);
                        $("#oardraft-tbl-body").empty();
                        $("#oardraft-tbl-body").html(articleTableBody);
                    }
                    renderPagination(fetchAndRenderTableData, "oardraft-tbl-footer","page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-oardraft-list">
    {{ _.each(resourceList, function (resource,i){ }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>
    {{ if(resource.resourceIcon!==null && resource.resourceIcon!==""){}}
    <img id="imageUploadSrc" src="${pageContext.request.contextPath}/resource/get/icon/{{=resource.resourceIcon}}" height="50"/>
    {{ } else{}}
    <img id="imageUploadSrc" src="${pageContext.request.contextPath}/images/icons/no-preview-available.jpg" height="50"/>
    {{ } }}
    </td>
    <td >{{ var urlLink=resource.repositoryUrl; }} 
               {{ if(urlLink === undefined || urlLink == null || urlLink.length <= 0  || urlLink ==='null') {}}
               {{urlLink='---';}else }}
               {{ { }}
               {{ if(urlLink.indexOf("http") === -1) {  }}
               {{  urlLink="http://"+urlLink;}}
               {{ } }} 
               {{ } }}
               <a href="{{=urlLink}}" target="_blank" class="link2">{{=resource.resourceName}}</a> 
    </td>
    <td>
    {{if(resource.createdOn!=null){}}
    {{=$.format.date(new Date(resource.createdOn), "dd-MM-yyyy")}}
    {{}else{ }} - {{ } }}
    </td>
    <td>
    {{=resource.harvestUrl}}
    </td>
    <td>
    {{=resource.resourceStatus.statusDesc}}
    </td>
    <td>
    {{if(resource.resourceStatus.statusCode==8){}}
    <a href="#" onclick="return checkHarvestUrlInDraft({{=resource.id}});"  class="link2">Check URL</a>
   
    {{ } }}
    {{if(resource.resourceStatus.statusCode==9){}}
    <a href="#" onclick="resubmitURL('{{=resource.id}}', '{{=resourceTypeId}}');"  class="link2"><tags:message code="button.re-submit"/></a>
    {{ } }}
    </td>
    <td>
     <a href="#" class="btn btn-secondary" title="Edit" onclick="editResourceForm('{{=resource.resourceCode}}');">
     <span class="fa fa-pencil" aria-hidden="true"></span></a>
      <a data-resource-code="{{=resource.resourceCode}}" data-resource-name="{{=resource.resourceName}}" href="#" class="btn btn-secondary delete-btn" data-tooltip="Delete Resource" ><span class="fa fa-trash" aria-hidden="true"></span></a>
    </td>
    </tr>
    {{ }); }}
</script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><tags:message code="hedding.message"/></h4>
            </div>
            <form id='popupHiddenForm' method='post'></form>
            <input type='hidden' id='opoupResourceId'/>
            <div id="popupContentId" class="modal-body">
                <tags:message code="res.alert.draftreg"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onClick="checkValueForDialog('yes', '${resource.id}')" >OK</button>
                <!--<button type="button" class="btn btn-primary" onClick="checkValueForDialog('no', '${resource.id}')" >No</button>-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $('#myModal').modal("hide");
    });
    function checkHarvestUrlInDraft(resourceId) {
        var checkBaseURL_URL = "${pageContext.request.contextPath}/resource/check/draft/url/" + resourceId;
        $.ajax(
                {
                    type: 'GET',
                    url: checkBaseURL_URL,
                    cache: false,
                    success: function (data) {
                        if (data === "200") {
                            var resourceTypeURL = "${pageContext.request.contextPath}/resource/harvester/get/information/" + resourceId;
                            $.ajax(
                                    {
                                        type: 'GET',
                                        url: resourceTypeURL,
                                        cache: false,
                                        success: function (data) {

                                            if (data === "200") {
                                                $("#opoupResourceId").val(resourceId);
                                                $('#myModal').modal("show")
                                            } else {
                                                alert(data);
                                            }
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            alert("Server is currently unavailable or its taking time to respond. Please try later");
                                        }
                                    });
                        } else {

                            alert(data);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                    }

                });
    }
    
    
    function getDetails(loopInd) {
        var resource = resourceListG[loopInd];
        var createDate = $.format.date(new Date(resource.createdOn), "dd-MM-yyyy");
        console.log(resource.harvestStartTime);
        var harvestTime;
        if (resource.harvestStartTime !== null) {
            harvestTime = $.format.date(new Date(resource.harvestStartTime), "dd-MM-yyyy");
        } else {
            harvestTime = "-";
        }
        var str = "<tr><td>" + createDate + "</td><td>" + resource.harvestUrl + "</td><td>" + harvestTime + "</td><td>" + resource.recordCount + "</td></tr>";
        $("#detailsTable").html(str);
        $("#myModalDetails").modal("show");
    }
     function getMetaStd(loopInd) {
        var str = "";
        var metadataList = resourceListG[loopInd].recordMetadataStandards;
        _.each(metadataList, function (metaDataStd) {
            str = str + "<tr><td>" + metaDataStd.metadataType + "</td><td>" + metaDataStd.metadataEnabled + "</td></tr>";
        });
        $("#metaTable").html(str);
        $("#myModal").modal("show");
    }

    function actionOnSelectedRepo(event) {
        var action = "${context}/resource/" + event + "/selected";
        $("#hiddenFormDraftRegister").attr("action", action);
        $("#hiddenFormDraftRegister").submit();
    }

    function checkValueForDialog(value, resourceId) {
        var resourceTypeURL;
        resourceId = $("#opoupResourceId").val();
        if (value === 'yes') {

            $('#myModal').modal("hide");
            resourceTypeURL = "${pageContext.request.contextPath}/resource/set/draft/" + resourceId;

        }
        $("#popupHiddenForm").attr("action", resourceTypeURL);
        $("#popupHiddenForm").submit();
    }

    function submitFormForDraftRegisterPagination(pageNum, pageWindow) {
        var resourceTypeId = "${resourceType.id}";
        $("#hiddenFormDraftRegister").attr("method", "POST")
        $("#hiddenFormDraftRegister").attr("action", "${pageContext.request.contextPath}/resource/draft/register/" + resourceTypeId );
        $("#hiddenFormDraftRegister").submit();
    }
     function resubmitURL(resourceCode, resourceTypeId) {
        var action = "${context}/resource/resubmit/" + resourceTypeId + "/" + resourceCode;
        $("#hiddenFormDraftRegister").attr("action", action);
        $("#hiddenFormDraftRegister").submit();
    }

    function editResourceForm(resourceCode) {
        var action = "${context}/resource/edit/" + resourceCode+"?type=1";
        $("#hiddenFormDraftRegister").attr("action", action);
        $("#hiddenFormDraftRegister").submit();
    }

</script>
