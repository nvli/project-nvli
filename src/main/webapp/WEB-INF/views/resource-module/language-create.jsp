<%--
    Document   : language-create
    Created on : Apr 8, 2016, 10:18:40 AM
    Author     : Suman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />



<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="card ">
            <div class="card-header"> <tags:message code="res.create.langauge"/></div>
            <div class="card-block clearfix">
                <form:form method="post" commandName="language"  id="language" action="${context}/resource/language/save" >
                    <div class="form-group row">
                        <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.language.name"/></label>
                        <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                            <form:input id="languageNameId" path="languageName" cssClass="form-control" required="required" placeholder="Enter Language Name" onkeyup="findDuplicateLanguageName();" />
                            <span class="text-danger font-bold" id="isLanguageNameduplicateId"> </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.language.code"/></label>
                        <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                            <form:input id="languageCodeId" path="languageCode" cssClass="form-control" required="required" placeholder="Enter Language Code" onkeyup="findDuplicateLanguageCode();"/>
                            <span class="text-danger font-bold" id="isLanguageCodeduplicateId"> </span>
                        </div>
                    </div>
                </div>
                <div class="card-footer clearfix">
                    <div class="text-center">
                        <button id="langBtnId" type="submit" class="btn btn-primary btn-sm"><tags:message code="button.submit"/></button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function findDuplicateLanguageName() {
        var languageName = $('#languageNameId').val();

        if (languageName.length > 3) {

            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/checkduplicate/languagename/" + languageName,
                cache: false,
                data: "languageName=" + languageName,
                success: function (data) {
                    console.log(data);
                    if (data === true) {
                        $('#isLanguageNameduplicateId').show();
                        $('#isLanguageNameduplicateId').html("Language name already exists...");
                        $("#langBtnId").attr("disabled", "disabled");

                    } else {
                        $('#isLanguageNameduplicateId').hide();
                        $("#langBtnId").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }
            });
        } else {
            $('#isLanguageNameduplicateId').show();
            $('#isLanguageNameduplicateId').html("Language name require minimum 4 characters...");
        }
    }

    function findDuplicateLanguageCode() {
        var languageCode = $('#languageCodeId').val();
        if (languageCode.length > 1) {
            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/checkduplicate/languagecode/" + languageCode,
                cache: false,
                data: "languageCode=" + languageCode,
                success: function (data) {
                    if (data === true) {
                        $('#isLanguageCodeduplicateId').show();
                        $('#isLanguageCodeduplicateId').html("Language code already exists...");
                        $("#langBtnId").attr("disabled", "disabled");

                    } else {
                        $('#isLanguageCodeduplicateId').hide();
                        $("#langBtnId").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }
            });
        } else {
            $('#isLanguageCodeduplicateId').show();
            $('#isLanguageCodeduplicateId').html("Language code require minimum 2 characters...");
        }
    }
</script>
