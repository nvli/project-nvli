<%-- 
    Document   : resource-type-create
    Created on : Apr 6, 2016, 11:41:44 AM
    Author     : Sujata Aher
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class=" "> 
        <div class="card ">
            <div class="card-header">   
                <span>
                    <c:choose>
                        <c:when test="${isEdit=='true'}">
                            <tags:message code="label.edit"/> <tags:message code="label.resource"/>
                        </c:when>    
                        <c:otherwise>
                            <tags:message code="button.AddResource"/>
                        </c:otherwise>
                    </c:choose>   
                </span>
                <br />
            </div>
            <div class="card-block clearfix p-b-0">
                <div class="form-group row" id="resourceTypeIdDiv">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resource.type"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <select path="resourceType" class="form-control" id="resourceTypeId">
                            <option value="-1" label="Select Resource Type"><tags:message code="res.select.resource.type"/></option>
                            <c:forEach items="${resourceTypeList}" var="resourceType">
                                <option value="${resourceType.id}" >${resourceType.resourceType}</option>
                            </c:forEach>                     
                        </select>
                        <c:if test="${empty resourceTypeList}">
                            <span class="text-danger font-bold" id="errorForRCode"><tags:message code="res.create.resource.type"/></span>
                        </c:if>
                    </div>
                </div>
            </div>
            <input id="resourceStatusDIVID" name="resourceStatus" type="hidden"/> 
            <div id="resourceContentDivId"></div>
            
        </div>
    </div>
</div>
<script type="text/javascript">


    $(document).ready(function ()
    {
        window.history.pushState({page: 1}, "Title 1", "#no-back");
        window.onhashchange = function (event) {
            window.location.hash = "no-back";
        };
        if ('${isEdit}' === "true")
        {
            getResourceFormEdit('${resource.id}')
        }
        $('input[name="resourceInfoFillType"]') // select the radio by its id
                .change(function () { // bind a function to the change event
                    if ($(this).is(":checked")) { // check if the radio is checked
                        var val = $(this).val(); // retrieve the value
                        if (val === "manual") {
                            $("#resourceInfoTagId").show();
                        } else if (val === "automatic") {
                            $("#uploadFileId").show();
                        }
                    }
                });
        $("#resourceTypeId").change(function () {
            var value = $(this).val();
            if (value !== "-1") {
                getResourceForm(value);
            }
        });

    });

    /**
     * Call for to get Resource form as per Resource Type selection
     */
    function getResourceFormEdit(resourceId) {
        var resourceTypeURL;
        if ('${isDraft}' === 'true') {
            resourceTypeURL = "${pageContext.request.contextPath}/resource/getResourceForm/edit/${resource.resourceType.id}/" + resourceId + "?type=1";
        } else {
            resourceTypeURL = "${pageContext.request.contextPath}/resource/getResourceForm/edit/${resource.resourceType.id}/" + resourceId;
        }

        var resourceForm = $("#resourceForm").serialize();
        $.ajax({
            type: 'GET',
            url: resourceTypeURL,
            data: resourceForm,
            cache: false,
            success: function (data) {
                var selectBox = document.getElementById("resourceTypeIdDiv");

                selectBox.style.display = "none";
                $("#resourceContentDivId").html(data);

                $("#resourceFormbtn").removeAttr("disabled");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("In error");
            }
        });
    }

    /**
     * To get the resource form as per resource type.
     * parameter: value of resourceTypeId select option
     */
    function getResourceForm(resourceTypeId)
    {
        var resourceTypeURL = "${pageContext.request.contextPath}/resource/getResourceForm/" + resourceTypeId;
        var resourceForm = $("#resourceForm").serialize();
        $.ajax({
            type: 'GET',
            url: resourceTypeURL,
            data: resourceForm,
            cache: false,
            success: function (data) {
                var selectBox = document.getElementById("resourceTypeIdDiv");
                selectBox.style.display = "none";
                $("#resourceContentDivId").html("");
                $("#resourceContentDivId").html(data);
                $("#resourceFormbtn").removeAttr("disabled");

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Resource Type data is not present");

            }
        });
    }

    function checkHarvestUrl() {
        $("#loaderDiv").show();
        var urlValue = $("#harvestUrlId").val();
        if (urlValue !== null && urlValue !== "") {
            var checkBaseURL_URL = "${pageContext.request.contextPath}/resource/check/url";
            var resourceForm = $("#resourceForm").serialize();
            $.ajax(
                    {
                        type: 'GET',
                        url: checkBaseURL_URL,
                        data: resourceForm,
                        cache: false,
                        success: function (data) {
                            if (data === "200") {
                                getHarvesterInfo();
                            } else {
                                alert(data);
                                $("#harvestUrlId").val("");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loaderDiv").hide();
                            alert("Server is taking timess to respond. Please try later.");
                            $("#harvestUrlId").val("");

                        }

                    });
        } else {
            $("#loaderDiv").hide();
            $("#harvestUrlSpanId").html("Please Enter URL.");

        }
    }
    /**
     * To get the Extracetd information for provided harvest base URL.
     */
    function getHarvesterInfo() {
        console.log($("#resourceForm").serialize());
        var resourceForm = $("#resourceForm").serialize();
        var resourceTypeURL = "${pageContext.request.contextPath}/resource/harvester/get/information";
        $.ajax(
                {
                    type: 'GET',
                    data: resourceForm,
                    url: resourceTypeURL,
                    cache: false,
                    success: function (data) {
                        $("#loaderDiv").hide();
                        $("#resourceContentDivId").html(data);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loaderDiv").hide();
                        alert("Server is currently unavailable or its taking time to respond. Please try later");
                        $("#harvestUrlId").val("");

                    }
                });
    }

    /**
     * To submit the Resource Form. 
     */
    function submitResourceForm() {
        var resourceTypeId;
        if ('${isEdit}' === "true") {
            resourceTypeId = "${resource.resourceType.id}"
        } else {
            resourceTypeId = $("#resourceTypeId").val()
        }
        var isNotReady = false;
        if ('${isEdit}' !== "true") {
            if ($("#resourceCodeId").val().trim() === "") {
                document.getElementById("resourceCodeId").focus();
                $("#errorForRCode").html("Resource code is required.");
                isNotReady = true;
            } else {
                $("#errorForRCode").html("")
            }
        }
        var organizationVal = $("#organizationId").val();
        if (organizationVal === "-1") {
            $("#organizationError").html("Please Select Organization");
            document.getElementById("organizationId").focus();
            isNotReady = true;
        } else {
            $("#organizationError").html("");
        }
        var grpValue = $("#groupTypeId").val();
        if (grpValue === "-1") {
            $("#grpTypeError").html("Please Select Group Type");
            document.getElementById("groupTypeId").focus();
            isNotReady = true;
        } else {
            $("#grpTypeError").html("");
        }
        if ($("#resourceNameId").val().trim() === "") {
            document.getElementById("resourceNameId").focus();
            $("#errorForRName").html("Resource name is required.");
            $("#errorForRName").addClass("text-danger");
            isNotReady = true;
            afterRNameCheckFormSubmit(isNotReady);
        } else if ($("#resourceNameId").val().length < 4) {
            $("#errorForRName").addClass("text-danger");
            $('#errorForRName').html("Resource type name require minimum 4");
        } else {
            if (grpValue !== "-1" && organizationVal !== "-1") {
                findDuplicateResourceName($("#resourceNameId").val(), resourceTypeId, "form");
            } else {
                afterRNameCheckFormSubmit(isNotReady);
            }

        }


    }

    function afterRNameCheckFormSubmit(isDuplicate) {
        if (!isDuplicate) {
            console.log(isDuplicate);
            var action = "";
            var resourceId = "${resource.id}";
            console.log(resourceId);
            selectAll();
            if ('${isEdit}' === 'true') {
                if ('${isDraft}' === 'true') {
                    action = "${context}/resource/update/draft/" + resourceId;
                } else {
                    action = "${context}/resource/update/" + resourceId;
                }
            } else {
                action = "${context}/resource/submit";
            }
            $("#resourceForm").attr("action", action);
            $("#resourceForm").submit();
        }
    }

    function uploadFileXLS() {
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/upload/file",
            type: "POST",
            data: new FormData($("#uploadFileXLSFormId")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                $("#resourceContentDivId").html(data);
                $("#tempId").hide();
                $("#resourceInfoTagId").show();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //  alert("Could not load content. Please check the file.");
            }
        });
    } // function uploadFile
</script>

