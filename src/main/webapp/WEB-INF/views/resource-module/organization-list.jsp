<%--
    Document   : organization-list
    Created on : May 24, 2017, 12:26:20 PM
    Author     : Sujata Aher
    Author     : Savita Kakad
--%>


<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<script src="${context}/scripts/libs/jquery-date-format.min.js" type="text/javascript"></script>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${not empty orgMessage}">
        <div class="alert alert-success"> <span>${orgMessage}</span></div>
    </c:if>
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin ">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <tags:message code="label.organization.list"/> &nbsp;
                    <a href="${context}/resource/organization/create" class="btn btn-success btn-sm"><tags:message code="label.organization.create"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search organization">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"> <tags:message code="label.sno"/></th>
                        <th ><tags:message code="label.organization"/></th>
                        <th width="8%"><tags:message code="res.dor"/></th>
                        <th ><tags:message code="label.organization.url"/></th>
                        <th width="9%" style="text-align:center;"><tags:message code="res.edit.delete"/></th>

                    </tr>
                </thead>
                <tbody id="organization-tbl-body"></tbody>
            </table>
        </div>
        <div id="organization-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });
    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/organization/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                console.log("success");
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.organizationList)) {
                        $("#organization-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var organisationTpl = _.template($("#tpl-organizationTpl-list").html());

                        var Opts = {
                            "organizationList": jsonObject.organizationList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        console.log(Opts);
                        var orgTableBody = organisationTpl(Opts);
                        $("#organization-tbl-body").empty();
                        $("#organization-tbl-body").html(orgTableBody);
                        renderPagination(fetchAndRenderTableData, "organization-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                console.log("err");
                alertErrorMsg();
            }
        });
    }
    function deleteOrganization() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-organization-button").attr("href");
        } else {
            return false;
        }
    }
    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>

<script type="text/template" id="tpl-organizationTpl-list">
    {{ _.each(organizationList, function (organization,i){ }}
    <tr><td>{{=i+1+recordCount}}</td>
    <td >
    {{=organization.name}}
    </td>
    <td>
    {{if(organization.createdOn!=null){}}
    {{=$.format.date(new Date(organization.createdOn), "dd-MM-yyyy")}}
    {{}else{ }} - {{ } }}
    </td>
    <td>
    {{=organization.organisationUrl}}
    </td>
    <td style="text-align:center;">
     <div class="btn-group">
      <a href="${pageContext.request.contextPath}/resource/organization/populate/edit/{{=organization.id}}" class="btn btn-secondary" title="Edit"><span class="fa fa-pencil" aria-hidden="true"></span></a>
      <a id="remove-organization-button" href="${context}/resource/organization/remove/{{=organization.id}}" class="btn btn-secondary" title="Delete" onclick="return deleteOrganization();" ><span class="fa fa-trash" aria-hidden="true"></span></a>
     </div>
    </td>
    {{ }); }}
</script>
