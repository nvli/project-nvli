<%-- 
    Document   : imageUpload
    Created on : Jun 3, 2016, 4:36:47 PM
    Author     : user
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resourceIcon"/></label>
     <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <div class="profile-img"> 
            <form id="resourceIconfForm" > 
                <a id="upload_resource_icon_btn" href="javascript:void(0)" onClick="callUploadIcon();" class="link2"><tags:message code="label.uploadIcon"/></a>
                <input id="upload_resource_icon" type="file" name="uploadfile" style="visibility: hidden"/>
                <input name="resourceCode" class="form-control" value="${resource.resourceCode}" type="hidden"/>
            </form>
            <span id="upload-file-message">
                <c:if test="${not empty resource.resourceIcon}">
                      <img id="imageUploadSrc" src="${resourceIconLocation}/${resource.resourceIcon}" height="100" width="100" />
                </c:if>
              </span>
            
        </div>
    </div>
</div> 

<script type="text/javascript">

    function callUploadIcon() {
        $("#upload_resource_icon").click();
    }
    function uploadFile() {
        $("#upload-file-message").html("");
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/upload/icon",
            type: "POST",
            data: new FormData($("#resourceIconfForm")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                // Handle upload success
                $("#resourceiconDivId").attr("value", data);
                $("#upload-file-message").html("");

                $("#upload-file-message").html('<img id="imageUploadSrc" src="${resourceIconLocation}/' + data + '" height="100" width="100" />');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Handle upload error
                alert(thrownError)
                $("#upload-file-message").text(
                        "File not uploaded (perhaps it's too much big)");
            }
        });
    } // function uploadFile


    $(document).ready(function () {
        $("#upload_resource_icon").on("change", uploadFile);

    });
</script>