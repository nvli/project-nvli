<%-- 
    Document   : harvester
    Created on : Apr 22, 2016, 12:35:24 PM
    Author     : Sujata Aher
--%>

<%@page import="in.gov.nvli.domain.resource.OpenRepository"%>
<%@page import="in.gov.nvli.domain.resource.Resource"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link href="${pageContext.request.contextPath}/styles/css/resource-loading.css" rel="stylesheet"/>
<div class="loader_overlay" id="loaderDiv"><img class="loading_image" src="${pageContext.request.contextPath}/images/Loading_icon.gif"/></div>
    <c:set var="context" value="${pageContext.request.contextPath}" />
<div class="card-block clearfix p-b-0">
    <form:form method="post" id="resourceForm" modelAttribute="resource">
        <div class="form-group row" id="resourceTypeIdDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resource-type"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <c:choose>
                    <c:when test="${isEdit eq false}">
                        <form:select path="resourceType" class="form-control" id="resourceTypeFormId">
                            <form:option value="-1" label="Select Resource Type"/>
                            <form:options items="${resourceTypeList}"  itemLabel="resourceType" itemValue="id"/>
                        </form:select>
                    </c:when>
                    <c:otherwise>
                        <input name="resourceTypeName" class="form-control" value="${resource.resourceType.resourceType}" disabled>
                        <input name="resourceType" class="form-control" value="${resource.resourceType.id}" type="hidden" >
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="form-group row" id="harvestUrlDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.oaiBaseUrl"/><em>*</em></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input name="harvestUrl" class="form-control"  id="harvestUrlId" type="text" value="${resource.harvestUrl}"/>
                <c:if test="${isEdit eq true && resource.resourceStatus.statusCode!=8}">
                    <input name="harvestUrl" class="form-control"  id="harvestUrlId" type="hidden" value="${resource.harvestUrl}"/>
                </c:if>
                <a onclick="checkHarvestUrl();" href="#" class="link1" id="harvestUrlAncId">Get Info</a>

                <span class="text-danger font-bold" id="harvestUrlSpanId"></span>
            </div>
        </div>
        <c:if test="${not empty resource.recordMetadataStandards}">
            <c:forEach items="${resource.recordMetadataStandards}" var="metadataStandard" varStatus="cnt">
                <input type="hidden" name="resource.recordMetadataStandards[${cnt.index}].metadataSupport" value="${metadataStandard.metadataSupport}">
                <div class="form-group row"> 
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"> ${metadataStandard.metadataType} Enable</label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 form-control-label">

                        <label class="radio-inline">
                            <input name="recordMetadataStandards[${cnt.index}].metadataEnabled" id="no" value="false" type="radio"   ${metadataStandard.metadataEnabled == false? 'checked' : ' '}/>
                            No</label>
                        <label class="radio-inline">
                            <c:choose>
                                <c:when test="${isEdit eq true}">
                                    <input name="recordMetadataStandards[${cnt.index}].metadataEnabled" id="yes" value="true" type="radio"   ${metadataStandard.metadataEnabled == true? 'checked' : ' '}/>
                                </c:when>
                                <c:otherwise>
                                    <input name="recordMetadataStandards[${cnt.index}].metadataEnabled" id="yes" value="true" type="radio" checked/>
                                </c:otherwise>
                            </c:choose>  Yes
                        </label>
                    </div>
                </div>
            </c:forEach>
        </c:if>
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.subinfo.repository.url"/><em>*</em></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input name="repositoryUrl" class="form-control"  type="text" value="${resource.repositoryUrl}" id="repositoryUrlId"/> 
                <span class="text-danger font-bold" id="repositoryUrlSpanId"></span>
            </div>
        </div>   
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.software"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <select name="harvestSoftware" class="form-control">
                    <option value="">Not specified</option>
                    <c:forEach items="${softwareList}" var="software">
                        <option value="${software}"  ${resource.harvestSoftware == software? 'selected' : ' '}>${software}</option>
                    </c:forEach>
                </select>
            </div>
        </div>   
        <jsp:include page="mainRT.jsp"/>   
        <div id="languageSelectionDiv">
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.language"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                        <div class="col-lg-5 col-sm-5 col-xs-12">
                            <select id="languageList" multiple="multiple" size="8" class="form-control">

                                <c:forEach var="language" items="${languageList}">
                                    <c:if test="${not fn:contains(addedLangaugeList,language)}" >
                                        <option value="${language.id}">${language.languageName}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-xs-12">
                            <button type="button" class="btn btn-secondary btn-block m-t-3" onClick="addLangauge();"><i class="fa fa-chevron-right"></i></button>
                            <button type="button"  class="btn btn-block btn-secondary" onClick="removeLangauge();"><i class="fa fa-chevron-left"></i></button>

                            <div class="hidden-md-up " style="margin-bottom:30px;"></div>
                        </div>
                        <div class="col-lg-5 col-sm-5 col-xs-12">
                            <select id="languagesSelected" name="languages" multiple="multiple" size="8" class="form-control">

                                <c:forEach var="assignedLang" items="${addedLangaugeList}">
                                    <option value="${assignedLang.id}">${assignedLang.languageName}</option>
                                </c:forEach>

                            </select>
                        </div>
                    </div> 
                </div>
            </div>
        </div>  
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.crowdsourcing"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 form-control-label">
                <label class="radio-inline">
                    <input name="crowdSourcing" id="no" value="false" type="radio" ${resource.crowdSourcing == false? 'checked' : ' '}/>
                    No </label>
                <label class="radio-inline">
                    <input name="crowdSourcing" id="yes" value="true" type="radio" ${resource.crowdSourcing == true? 'checked' : ' '}/>
                    Yes </label>
            </div>
        </div>
    </form:form>
    <jsp:include page="imageUpload.jsp"/> 
</div>
<div class="card-footer clearfix">
    <div class="text-center">
        <button type="submit" class="btn btn-primary" onclick="submitHarResourceForm();" id="resourceFormbtn"><tags:message code="button.submit"/></button>
    </div>
</div>   
<script type="text/javascript">
    $(document).ready(function () {
        $("#loaderDiv").hide();
        if ('${isEdit}' === 'true' && '${resource.resourceStatus.statusCode}' !== '8') {
            $("#harvestUrlId").attr("disabled", "disabled");
            document.getElementById('harvestUrlAncId').removeAttribute("onclick");
            document.getElementById('harvestUrlAncId').removeAttribute("href");
        }
        $("#resourceTypeFormId").change(function () {
            var value = $(this).val();
            if (value !== "-1") {
                getResourceForm(value);
            }
        });
    });
    function addLangauge() {
        $('#languageList option:selected').remove().appendTo('#languagesSelected');
        $('#languagesSelected option').prop('.selected', true);
        $('#languageList option').prop('.selected', false);
    }

    function removeLangauge() {
        $('#languagesSelected option:selected').remove().appendTo('#languageList');
        $('#languagesSelected option').prop('.selected', true);
        $('#languageList option').prop('.selected', false);
    }

    function submitHarResourceForm() {
        var isNotReady = true;
        var urlValue = $("#harvestUrlId").val();
        if (urlValue === "") {
            $("#harvestUrlSpanId").html("Please Enter Open Repository Base Url");
            document.getElementById("harvestUrlId").focus();
            isNotReady = true;

        } else {
            isNotReady = false;
            $("#harvestUrlSpanId").html("");
        }
        var repoUrlValue = $("#repositoryUrlId").val();
        if (repoUrlValue === "") {
            $("#repositoryUrlSpanId").html("Please Enter Repository Url");
            document.getElementById("repositoryUrlId").focus();
            isNotReady = true;

        } else {
            isNotReady = false;
            $("#repositoryUrlSpanId").html("");
        }

        if (!isNotReady) {
            submitResourceForm();
        }

    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>
