<%-- 
    Document   : libraryBook
    Created on : Apr 22, 2016, 11:31:40 AM
    Author     : Suman Behara
    Author     : Sujata Aher
--%>
<%@page import="in.gov.nvli.domain.resource.Resource"%>
<%@page import="in.gov.nvli.domain.resource.MetadataIntegrator"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<c:if test="${isEdit eq false}">
    <div class="card-block clearfix p-b-0">
        <div id="tempId">
            <div class="form-group row" id="resourceTypeIdDiv">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resource-type"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                    <select name="resourceType" class="form-control" id="resourceTypeTempId">
                        <option value="-1" label="Select Resource Type">Select Resource Type</option>
                        <c:forEach items="${resourceTypeList}" var="resourceType">
                            <c:if test="${resource.resourceType.id==resourceType.id}">
                                <option value="${resourceType.id}" selected >${resourceType.resourceType}</option>
                            </c:if>
                            <option value="${resourceType.id}" >${resourceType.resourceType}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.info.filling"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 form-control-label">
                    <label class="radio-inline">
                        <input id="resourceInfoFillTypeId" name="resourceInfoFillType" id="no" value="manual" type="radio"/>
                        Manual </label>
                    <label class="radio-inline">
                        <input  id="resourceInfoFillTypeId" name="resourceInfoFillType" id="yes" value="automatic" type="radio"/>
                        Automatic </label>
                </div>
            </div>
            <form id="uploadFileXLSFormId">
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.uploadFile"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <input type="file" name="xlsFileUpload" id="xlsFileId" accept=".xls,.xlsx"/>
                        <input type="hidden" name="resourceTypeFileload" id="resourceTypeFileloadId" value="${resource.resourceType.id}"/>
                    </div>
                </div>
            </form>
        </div>
    </c:if>
    <div id="resourceInfoTagId">
        <form:form method="post" id="resourceForm" modelAttribute="resource">
            <div class="form-group row" id="resourceTypeIdDiv">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resource-type"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                    <c:choose>
                        <c:when test="${isEdit eq false}">
                            <form:select path="resourceType" class="form-control" id="resourceTypeFormId">
                                <form:option value="-1" label="Select Resource Type"/>
                                <form:options items="${resourceTypeList}"  itemLabel="resourceType" itemValue="id"/>
                            </form:select>
                        </c:when>
                        <c:otherwise>
                            <input name="resourceTypeName" class="form-control" value="${resource.resourceType.resourceType}" disabled>
                            <input name="resourceType" class="form-control" value="${resource.resourceType.id}" type="hidden" >
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <jsp:include page="mainRT.jsp"/>
            <div class="form-group row" id="resourceTypeIdDiv">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.classification.level"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                    <form:select path="classificationLevel" class="form-control">
                        <form:option value="-1" label="Select Classifacation Level"/>
                        <form:options items="${classificationLevelList}"  itemLabel="value" itemValue="value"/>
                    </form:select>
                </div>
            </div>
            <div id="languageSelectionDiv">
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.language"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <div class="row">
                            <div class="col-lg-5 col-sm-5 col-xs-12">
                                <select id="languageList" multiple="multiple" size="8" class="form-control">
                                    <c:forEach var="language" items="${languageList}">
                                        <c:if test="${not fn:contains(addedLangaugeList,language)}" >
                                            <option value="${language.id}">${language.languageName}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-12">
                                <button type="button" class="btn btn-secondary btn-block m-t-3" onClick="addLangauge();"><i class="fa fa-chevron-right"></i></button>
                                <button type="button"  class="btn btn-block btn-secondary" onClick="removeLangauge();"><i class="fa fa-chevron-left"></i></button>
                                <div class="hidden-md-up " style="margin-bottom:30px;"></div>
                            </div>
                            <div class="col-lg-5 col-sm-5 col-xs-12">
                                <select id="languagesSelected" name="languages" multiple="multiple" size="8" class="form-control">
                                    <c:forEach var="assignedLang" items="${addedLangaugeList}">
                                        <option value="${assignedLang.id}">${assignedLang.languageName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div> 
                    </div>
                </div>
            </div> 
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.total.projected.record"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                    <input name="totalCount" class="form-control"  type="text" value="${resource.totalCount}"/> 
                </div>
            </div>
            <div id="mainDivContentWithMetadata">
                <div id="contentWithMetadataDiv">
                    <div class="form-group row">
                        <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.metadata.with.content"/></label>
                        <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 form-control-label" style="margin-top:4px;">
                            <label class="radio-inline">
                                <input name="contentWithMetadata" id="no" value="false" type="radio"  ${resource.contentWithMetadata == false? 'checked' : ' '} />
                                No </label>
                            <label class="radio-inline">
                                <input name="contentWithMetadata" id="yes" value="true" type="radio" ${resource.contentWithMetadata == true? 'checked' : ' '} />
                                Yes </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.crowdsourcing"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 form-control-label" style="margin-top:4px;">
                    <label class="radio-inline">
                        <input name="crowdSourcing" id="no" value="false" type="radio" ${resource.crowdSourcing == false? 'checked' : ' '}/>
                        No </label>
                    <label class="radio-inline">
                        <input name="crowdSourcing" id="yes" value="true" type="radio" ${resource.crowdSourcing == true? 'checked' : ' '}/>
                        Yes </label>
                </div>
            </div>
        </form:form>
        <jsp:include page="imageUpload.jsp"/>  
    </div>
</div>
<div class="card-footer clearfix">
    <div class="text-center">
        <button type="submit" class="btn btn-primary" onclick="submitResourceForm();" id="resourceFormbtn"><tags:message code="button.submit"/></button>
    </div>
</div>   
<script  type="text/javascript">
    $(document).ready(function ()
    {
        var isEdit = "${isEdit}";
        if (isEdit === "false") {
            $("#xlsFileId").on("change", xlsFileUpload);
            $("#resourceInfoTagId").hide();
            $("#uploadFileXLSFormId").hide();
        } else {
            $("#resourceInfoTagId").show();
        }
        $('input[name="resourceInfoFillType"]') // select the radio by its id
                .change(function () { // bind a function to the change event
                    if ($(this).is(":checked")) { // check if the radio is checked
                        var val = $(this).val(); // retrieve the value
                        if (val === "manual") {
                            $("#resourceInfoTagId").show();
                            $("#uploadFileXLSFormId").hide();
                            $("#tempId").hide();
                        } else if (val === "automatic") {
                            $("#uploadFileXLSFormId").show();
                            $("#resourceInfoTagId").hide();
                        }
                    }
                });
        $("#resourceTypeTempId").change(function () {
            var value = $(this).val();
            if (value !== "-1") {
                getResourceForm(value);
            }
        });
        $("#resourceTypeFormId").change(function () {
            var value = $(this).val();
            if (value !== "-1") {
                getResourceForm(value);
            }
        });



    });
    function xlsFileUpload() {
        uploadFileXLS();
    }
    function addLangauge() {
        $('#languageList option:selected').remove().appendTo('#languagesSelected');
        $('#languagesSelected option').prop('.selected', true);
        $('#languageList option').prop('.selected', false);
    }
    function removeLangauge() {
        $('#languagesSelected option:selected').remove().appendTo('#languageList');
        $('#languagesSelected option').prop('.selected', true);
        $('#languageList option').prop('.selected', false);
    }
</script>
