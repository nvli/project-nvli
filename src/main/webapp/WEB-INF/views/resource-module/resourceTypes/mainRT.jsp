<%-- 
    Document   : mainRT
    Created on : Apr 22, 2016, 11:32:08 AM
    Author     : Sujata Aher
--%>

<%@page import="java.util.Collections"%>
<%@page import="in.gov.nvli.domain.resource.GroupType"%>
<%@page import="java.util.List"%>
<%@page import="in.gov.nvli.domain.resource.Resource"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
    Resource resorce = (Resource) request.getAttribute("resource");
    List<GroupType> groupTypeListRTWise = resorce.getResourceType().getGroupTypes();
    Collections.sort(groupTypeListRTWise, GroupType.GroupTypeComparator);

%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.organization"/><em>*</em></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <select name="organization" class="form-control" required id="organizationId">
            <option value="-1">Select Organization</option>
            <c:forEach items="${organizationsList}" var="organization1">
                <option  value="${organization1.id}" ${resource.organization.id == organization1.id? 'selected' : ' '}>${organization1.name}</option>
            </c:forEach>
        </select>
        <span class="text-danger font-bold" id="organizationError"> </span>
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"> <tags:message code="label.resourcecode"/><em>*</em></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <c:choose>
            <c:when test="${isEdit eq false}">
                <input id="resourceCodeId" name="resourceCode" class="form-control" value="${resource.resourceCode}" type="text" onkeypress="return onlySpecificChar(event, this);" style="text-transform:uppercase;" onkeyup="findDuplicateRCode(this);"/>
                <span class="text-danger font-bold" id="errorForRCode"> </span>
            </c:when>
            <c:otherwise>
                <input name="resourceCode" class="form-control" value="${resource.resourceCode}" type="text"  disabled/>
                <input name="resourceCode" class="form-control" value="${resource.resourceCode}" type="hidden"/>
            </c:otherwise>
        </c:choose>

    </div>
</div>

<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"> <tags:message code="label.group"/><em>*</em></label>
    <div id="groupContentDivId"></div>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <select name="groupType" id="groupTypeId" class="form-control" required>
            <option value="-1">Select Group</option>
            <c:forEach items="<%=groupTypeListRTWise%>" var="groupType">
                <option  value="${groupType.id}" ${resource.groupType.id == groupType.id? 'selected' : ' '}>${groupType.groupType}</option>
            </c:forEach>
        </select>
        <span class="text-danger font-bold" id="grpTypeError"> </span>
    </div>
</div>
<input type="hidden" name="resourceIcon" id="resourceiconDivId" value="${resource.resourceIcon}"/>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resName"/><em>*</em></label> 
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <input id="resourceNameId"  name="resourceName" class="form-control" type="text" value="${resource.resourceName}"  placeholder="Enter resource Name"onkeypress="return onlySpecificChar(event, this);" onkeyup="return findDuplicateResourceName(this, '${resource.resourceType.id}', '')"/> 
        <span class="font-bold" id="errorForRName"> </span>
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.description"/></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <textarea name="description" class="form-control" value="${resource.description}">${resource.description}</textarea>
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.contact.person"/></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <input name="contactPersonName" class="form-control"  type="text" value="${resource.contactPersonName}"/> 
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.organization.designation"/></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <input name="designation" class="form-control" type="text" value="${resource.designation}"/> 
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.organization.email"/><em>*</em></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <input name="email" class="form-control"  type="text" value="${resource.email}" required/> 
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.phone"/></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <input name="phoneNumber" class="form-control"  type="text" value="${resource.phoneNumber}"/> 
    </div>
</div>
<div class="form-group row" id="thematicTypeId">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="themes"/> </label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <div class="row">
            <div class="col-lg-5 col-sm-5 col-xs-12">
                <select id="thematicTypeList" multiple="multiple" class="form-control" size="8" >
                    <c:if test="${not empty thematicTypeList}" >
                        <c:forEach var="thematicType" items="${thematicTypeList}">
                            <c:if test="${not fn:contains(alreadyAssingedTTList,thematicType)}" >
                                <option value="${thematicType.id}">${thematicType.thematicType}</option>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
            <div class="col-lg-2 col-sm-2 col-xs-12">
                <button type="button" class="btn btn-secondary btn-block m-t-3" onClick="addThematicType();"><i class="fa fa-chevron-right"></i></button>
                <button type="button"  class="btn btn-block btn-secondary" onClick="removeThematicType();"><i class="fa fa-chevron-left"></i></button>

                <div class="hidden-md-up " style="margin-bottom:30px;"></div>
            </div>
            <div class="col-lg-5 col-sm-5 col-xs-12">
                <select id="thematicSelected" name="thematicTypes" multiple="multiple" class="form-control" size="8" >
                    <c:if test="${not empty alreadyAssingedTTList}" >
                        <c:forEach var="assignedTT" items="${alreadyAssingedTTList}">
                            <option value="${assignedTT.id}">${assignedTT.thematicType}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.ContactType"/></label>
    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
        <div class="row">
            <div class="col-lg-5 col-sm-5 col-xs-12">
                <select id="contentTypeList" multiple="multiple" size="8" class="form-control">
                    <c:if test="${not empty contentTypeList}" >
                        <c:forEach var="contentType" items="${contentTypeList}">
                            <c:if test="${not fn:contains(alreadyAssingedCTList,contentType)}" >
                                <option value="${contentType.id}">${contentType.contentType}</option>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
            <div class="col-lg-2 col-sm-2 col-xs-12">
                <button type="button" class="btn btn-secondary btn-block m-t-3" onClick="addContentType();"><i class="fa fa-chevron-right"></i></button>
                <button type="button"  class="btn btn-block btn-secondary" onClick="removeContentType();"><i class="fa fa-chevron-left"></i></button>

                <div class="hidden-md-up " style="margin-bottom:30px;"></div>
            </div>
            <div class="col-lg-5 col-sm-5 col-xs-12">
                <select  id="contentTypeSelected" name="contentTypes" multiple="multiple"  size="8" class="form-control" >
                    <c:if test="${not empty alreadyAssingedCTList}" >
                        <c:forEach var="assignedCT" items="${alreadyAssingedCTList}">
                            <option value="${assignedCT.id}">${assignedCT.contentType}</option>
                        </c:forEach>
                    </c:if>
                </select>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="recordCount" value="${resource.recordCount}" />
<script type="text/javascript">
    function removeIcon(iconName) {
        $.ajax({
            type: 'GET',
            url: "${pageContext.request.contextPath}/resource/get/" + iconName,
            cache: false,
            success: function () {
                alert("Icon removed");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("In error");
            }
        });
    }
    function findDuplicateResourceName(resourceNameV, resourceTypeId, callFrom) {
        var resourceName;
        if (callFrom === "form") {
            resourceName = resourceNameV;
        } else {
            resourceName = resourceNameV.value;
        }
        var resourceId;
        if ('${isEdit}' === "true") {
            resourceId = "${resource.id}"
        } else {
            resourceId = null;
        }
        var isDuplicate = false;
        if (resourceName.length > 3) {
            $('#errorForRName').html("");
            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/resourcename/check/" + resourceTypeId,
                cache: false,
                data: {resourceName: resourceName, resourceId: resourceId},
                success: function (data) {
                    if (data === true || data === "true") {
                        document.getElementById("errorForRName").innerText = "Resource Name is already Used...";
                        $("#errorForRName").removeClass("text-success");
                        $("#errorForRName").addClass("text-danger");
                        document.getElementById("resourceNameId").focus();
                        isDuplicate = true;
                    } else {
                        $("#errorForRName").removeClass("text-danger");
                        $("#errorForRName").addClass("text-success");
                        $('#errorForRName').html("Resource Name is Available...");
                    }
                    if ('${isEdit}' !== "true") {
                        var resourceCode = $('#resourceCodeId').val();

                        $.ajax({
                            type: 'GET',
                            url: "${pageContext.request.contextPath}/resource/checkduplicate/code/" + resourceCode,
                            cache: false,
                            success: function (data) {

                                if (data === true || data === "true") {

                                    $('#errorForRCode').show();
                                    $('#errorForRCode').html("Resource type code already exists...");
                                    isDuplicate = true;
                                } else {

                                    var regex = new RegExp("^[a-zA-Z0-9-\b\t ]*$");
                                    var str = resourceCode;
                                    if (!regex.test(str)) {
                                        isDuplicate = true;
                                        $('#errorForRCode').show();
                                        $('#errorForRCode').html("Only alpha numeric characters and \"-\" (hyphen) allowed in Resource code...");
                                    } else {
                                        $('#errorForRCode').hide();
                                    }
                                }
                                if (callFrom === "form") {
                                    afterRNameCheckFormSubmit(isDuplicate);
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                if (callFrom === "form") {
                                    afterRNameCheckFormSubmit(isDuplicate);
                                }
                            }
                        });
                    } else {
                        if (callFrom === "form") {
                            afterRNameCheckFormSubmit(isDuplicate);
                        }
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    afterRNameCheckFormSubmit(false);
                }

            });
        } else {
            $("#errorForRName").removeClass("text-success");
            $("#errorForRName").addClass("text-danger");
            $('#errorForRName').show();
            // $("#errorForRName").attr("disabled", "disabled");
            $('#errorForRName').html("Resource type name require minimum 4");
        }

    }
    function addThematicType() {
        $('#thematicTypeList option:selected').remove().appendTo('#thematicSelected');
    }
    function removeThematicType() {
        $('#thematicSelected Option:selected').remove().appendTo('#thematicTypeList');
    }
    function addContentType() {
        $('#contentTypeList option:selected').remove().appendTo('#contentTypeSelected');
    }
    function removeContentType() {
        $('#contentTypeSelected option:selected').remove().appendTo('#contentTypeList');
    }

    function selectAll() {
        $('#thematicSelected option').prop('selected', true);
        $('#contentTypeSelected option').prop('selected', true);
        $('#thematicTypeList option').prop('selected', false);
        $('#contentTypeList option').prop('selected', false);
        $('#languagesSelected option').prop('selected', true);
    }

    function onlySpecificChar(e, t) {
        var regex = new RegExp("^[a-zA-Z0-9-\b\t_ ]*$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();

    }

    function findDuplicateRCode(codeValue) {
        codeValue.value = codeValue.value.toUpperCase();
        var resourceCode = codeValue.value;
        if (resourceCode.length > 3) {
            if (resourceCode.length < 9) {
                $.ajax({
                    type: 'GET',
                    url: "${pageContext.request.contextPath}/resource/checkduplicate/code/" + resourceCode,
                    cache: false,
                    success: function (data) {
                        if (data === true) {
                            $('#errorForRCode').show();
                            $('#errorForRCode').html("Resource code already exists...");
                            $("#resourceFormbtn").attr("disabled", "disabled");
                        } else {

                            $('#errorForRCode').hide();
                            $("#resourceFormbtn").removeAttr("disabled");


                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("In error");
                    }
                });
            } else {
                $('#errorForRCode').show();
                $("#resourceFormbtn").attr("disabled", "disabled");
                $('#errorForRCode').html("Resource code require maximum 8 characters...");
            }
        } else {
            $('#errorForRCode').show();
            $("#resourceFormbtn").attr("disabled", "disabled");
            $('#errorForRCode').html("Resource type code require minimum 4");
        }
    }
</script>
