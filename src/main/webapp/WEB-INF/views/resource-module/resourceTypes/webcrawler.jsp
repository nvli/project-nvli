<%-- 
    Document   : webcrawler
    Created on : Jun 10, 2016, 12:33:41 PM
    Author     : Sujata Aher
--%>

<%@page import="in.gov.nvli.domain.resource.OpenRepository"%>
<%@page import="in.gov.nvli.domain.resource.Resource"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="card-block clearfix p-b-0">
    <form:form method="post" id="resourceForm" modelAttribute="resource">
        <div class="form-group row" id="resourceTypeIdDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resource-type"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <c:choose>
                    <c:when test="${isEdit eq false}">
                        <form:select path="resourceType" class="form-control" id="resourceTypeFormId">
                            <form:option value="-1" label="Select Resource Type"/>
                            <form:options items="${resourceTypeList}"  itemLabel="resourceType" itemValue="id"/>
                        </form:select>
                    </c:when>
                    <c:otherwise>
                        <input name="resourceTypeName" class="form-control" value="${resource.resourceType.resourceType}" disabled>
                        <input name="resourceType" class="form-control" value="${resource.resourceType.id}" type="hidden" >
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.web.url"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input name="webUrl" class="form-control"  type="text" value="${resource.webUrl}"/> 
            </div>
        </div>
        <jsp:include page="mainRT.jsp"/>
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.language"/> *</label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <select id="languageList" name="languages" class="form-control">
                    <option value="-1" label="Select Language">Select Language</option>
                    <c:forEach var="language" items="${languageList}" varStatus="cnt">

                        <option value="${language.id}" ${resource.languages.iterator().next().id == language.id? 'selected' : ' '}>${language.languageName}</option>

                    </c:forEach>
                </select>
                <span class="text-danger font-bold" id="langErrorId"></span>
            </div>
        </div>
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword">Twitter Url</label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input name="twitterUrl" class="form-control"  type="text" value="${resource.twitterUrl}"/> 
            </div>
        </div>
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword">Facebook Url</label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input name="facebookUrl" class="form-control"  type="text" value="${resource.facebookUrl}"/> 
            </div>
        </div>

    </form:form>
    <jsp:include page="imageUpload.jsp"/> 
</div>
<div class="card-footer clearfix">
      <div class="text-center">
            <button type="submit" class="btn btn-primary" onclick="submitWBResourceForm();" id="resourceFormbtn"><tags:message code="label.submit"/></button>
    </div>
</div>   
<script type="text/javascript">
    $(document).ready(function () {
        $("#resourceTypeFormId").change(function () {
            var value = $(this).val();
            if (value !== "-1") {
                getResourceForm(value);
            }
        });
    });
    function submitWBResourceForm() {
        var langValue = $('#languageList option:selected').val();
        var resourceName = $("#resourceNameId").val();
        if (langValue !== "-1" && resourceName !== "") {
            submitResourceForm();
            $("#isduplicateId").html();
            $("#langErrorId").html();
        }
        if (langValue === "-1") {
            $("#langErrorId").html("Select Language.");
        } else {
            $("#langErrorId").html("");
        }

        if (resourceName === "") {
            $("#isduplicateId").html("Insert Resource Name.");
        } else {
            $("#isduplicateId").html("");
        }
    }
</script>