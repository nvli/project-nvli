<%-- 
    Document   : e-news
    Created on : Apr 22, 2016, 12:36:40 PM
    Author     : Sujata Aher

--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<c:set value="${newsFeeds}" var="newsFeeds"></c:set>
<c:set value="${resource.newsFeedUrl}" var="jsonFeeds"></c:set>
    <div class="card-block clearfix p-b-0">
    <form:form method="post" id="resourceForm" modelAttribute="resource">
        <div class="form-group row" id="resourceTypeIdDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.resource-type"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <c:choose>
                    <c:when test="${isEdit eq false}">
                        <form:select path="resourceType" class="form-control" id="resourceTypeFormId">
                            <form:option value="-1" label="Select Resource Type"/>
                            <form:options items="${resourceTypeList}"  itemLabel="resourceType" itemValue="id"/>
                        </form:select>
                    </c:when>
                    <c:otherwise>
                        <input name="resourceTypeName" class="form-control" value="${resource.resourceType.resourceType}" disabled>
                        <input name="resourceType" class="form-control" value="${resource.resourceType.id}" type="hidden" >
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="form-group row" id="newsUrlDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="res.enewsOrgUrl"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input id="organisationUrlId" name="newsUrl" class="form-control" required="required" type="text" value="${resource.newsUrl}"/> 
            </div>
        </div>
        <div class="form-group row" id="newsUrlDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="res.enewsFeedUrl"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <input id="newsUrlId" name="newsFeedUrl" class="form-control" required="required" type="text" value="${resource.newsFeedUrl}"/> 
            </div>
        </div>
        <div class="form-group row" id="newsUrlDiv">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="res.enewsFeed"/> </label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <!--<input name="newsFeedUrl" class="form-control" required="required"  type="text" value="${resource.newsFeed}"/>--> 
                <!--<a onclick="getNewsFeeds();" href="#" class="link1">Get Feeds</a>-->
                <a onclick="return addNewsFeeds();" href="#" class="link2">Add News Feeds</a>
                <span class="text-danger font-bold" id="FeedErrorId"></span>
            </div>
        </div>
        <jsp:include page="mainRT.jsp"/>
        <div class="form-group row">
            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.language"/></label>
            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                <select id="languageList" name="languages" class="form-control">
                    <option value="-1" label="Select Language">Select Language</option>
                    <c:if test="${not empty languageList}">
                        <c:forEach var="language" items="${languageList}" varStatus="cnt">

                            <option value="${language.id}" ${resource.languages.iterator().next().id == language.id? 'selected' : ' '}>${language.languageName}</option>

                        </c:forEach>
                    </c:if>
                </select>
                <span class="text-danger font-bold" id="langErrorId"></span>
            </div>
        </div>
        <div id="newsPaperTypeDiv">
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.news.paper.type"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                    <select name="newsPaperType" class="form-control">
                        <option value="-1" label="Select Type"/>
                        <option value="Regional" ${resource.newsPaperType == "Regional"? 'selected' : ' '}>Regional</option>
                        <option value="National" ${resource.newsPaperType == "National"? 'selected' : ' '}>National</option>
                    </select>
                </div>
            </div>
        </div>
        <%--<div id="newsFeedsTypeDiv">
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="res.enewsFeed"/></label>
                <div class="col-lg-10 col-md-7 col-sm-12" style="margin-top:4px;">
                    <label class="radio-inline">
                        <input type="radio" id="rssCheckbox" value="RSS" name="newsFeed" ${resource.newsFeed == 'RSS'? 'checked' : ' '}>
                        RSS </label>
                    <label class="checkbox-inline">
                        <input type="radio" id="atomCheckbox" value="ATOM" name="newsFeed" ${resource.newsFeed == 'ATOM'? 'checked' : ' '}>
                        ATOM</label>
                </div>
            </div>--%>
        <input id="newsFeedUrlsId" name="newsFeed" class="form-control" type="hidden" value="${resource.newsFeed}"/> 
    </form:form>
    <jsp:include page="imageUpload.jsp"/> 
</div>
<div class="card-footer clearfix">
    <div class="text-center">
        <button type="submit" class="btn btn-primary" onclick="submitBefore();" id="resourceFormbtn"><tags:message code="button.submit"/></button>
    </div>
</div> 
<!--Pop-up for add news feeds-->
<div class="modal  fade" id="newsFeedModelId" tabindex="-1" role="dialog" aria-labelledby="newsFeedModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="newsFeedModelLabel">e-News Feeds </h4>
            </div>
            <div class="modal-body mod_scroll">
                <form:form id='popupHiddenForm' method='post' commandName="newsFeeds">

                    <div class="form-group row" id="${cnt.index}newsLabelDiv">
                        <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" >Feed Url</label>
                        <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="${cnt.index}newsLabelDivIn">
                            <input type="text" value="" id="forFeedUrl" class="form-control" disabled="disabled"/>
                            <input type="hidden" value="" id="forFeedUrl2" name="feedUrlInForm" class="form-control"/>
                            <input type="hidden" value="" id="organisationUrlFeed" name="organisationUrlFeed" class="form-control"/>
                        </div>
                    </div>
                    <table class="table table-bordered" id="newsFeedTable">
                        <thead>
                            <tr>
                                <th width="300"> Category</th>
                                <th>Url</th>
                                <th width="100">Edit</th>

                            </tr>
                        </thead>
                        <tbody id="newsfeedFormId">
                            <c:choose>
                                <c:when test="${not empty newsFeeds.newsFeeds}">
                                    <c:forEach items="${newsFeeds.newsFeeds}" var="newsFeed" varStatus="cnt">
                                        <tr id="${cnt.index}newsFeedsUrlDiv" class="newsfeedClass">
                                            <td class="form-group row" id="${cnt.index}newsLabelDiv">
                                                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="${cnt.index}newsLabelDivIn">
                                                    <form:input path="newsFeeds[${cnt.index}].labelName" cssClass="form-control"  id="${cnt.index}newsFeedLabelId"/> 
                                                </div>
                                            </td>
                                            <td class="form-group row" id="${cnt.index}newsUrlDiv">
                                                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="${cnt.index}newsUrlDivIn">
                                                    <form:input path="newsFeeds[${cnt.index}].feedUrl" cssClass="form-control"  id="${cnt.index}newsUrlId" /> 
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary"  onclick="removeTag('${cnt.index}newsFeedsUrlDiv');" ><span class="fa fa-trash" aria-hidden="true"></button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>






                                    <!--                            <div id="defaultDivNewsFeed">
                                                                    <div id="0newsFeedsUrlDiv" class="newsfeedClass">
                                                                        <div class="form-group row" id="0newsLabelDiv">
                                                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" id="0newsLabelLabel">Category</label>
                                                                            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="0newsLabelDivIn">
                                    <form:input path="newsFeeds[0].labelName" cssClass="form-control"  id="0newsFeedLabelId"/> 
                                </div>
                            </div>
                            <div class="form-group row" id="0newsUrlDiv">
                                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" id="0newsUrlLabel">URL</label>
                                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="0newsUrlDivIn">
                                    <form:input path="newsFeeds[0].feedUrl" cssClass="form-control"  id="0newsUrlId" /> 
                                </div>
                            </div>
                        </div>
                    </div>-->
                                </c:otherwise>
                            </c:choose>



                        </tbody>
                    </table>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onClick="addMoreFeeds();" >Add More Feeds</button>
                <button type="button" class="btn btn-primary" onClick="saveNewsFeeds();" >save</button>
                <button type="button" class="btn btn-primary" onClick="autoCaptureFeeds('${isEdit}');" >Auto Capture Feeds</button>
                <button type="button" class="btn btn-primary" onClick="resetNewsFeeds();" >Clear All</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var isFeedEdit = false;
    $(document).ready(function ()
    {
        $('#newsFeedModelId').modal("hide");
        $("#newsFeedTable").hide();
        $("#resourceTypeFormId").change(function () {
            var value = $(this).val();
            if (value !== "-1") {
                getResourceForm(value);
            }
        });
    });
    function resetNewsFeeds() {
        $("#newsFeedTable> tbody").html("");
        $("#newsFeedTable").hide();
    }
    function submitBefore() {
        var jsonFeeds = '${jsonFeeds}';
        if (!isFeedEdit && jsonFeeds !== null && jsonFeeds !== "") {
            $("#newsFeedUrlsId").attr("value", jsonFeeds);
        }
        var langValue = $('#languageList option:selected').val();
        var resourceName = $("#resourceNameId").val();
        if (langValue !== "-1") {
            submitResourceForm();
            $("#isduplicateId").html();
            $("#langErrorId").html();
        }
        if (langValue === "-1") {
            $("#langErrorId").html("Select Language.");
        } else {
            $("#langErrorId").html("");
        }

        if (resourceName === "") {
            $("#isduplicateId").html("Insert Resource Name.");
        } else {
            $("#isduplicateId").html("");
        }


    }
    function addMoreFeeds() {
        var count = $('#newsfeedFormId > .newsfeedClass').length;
        var lastDiv;
        var num;
        var divIdCount;
        if (count !== 0) {
            lastDiv = $('#newsfeedFormId > .newsfeedClass')[count - 1].id;
            num = lastDiv.substring(0, lastDiv.indexOf("newsFeedsUrlDiv"));
            divIdCount = parseInt(num) + 1;
        } else {
            count = 1;
            num = 1;
            divIdCount = 1;
        }
        $("#newsFeedTable").show();
        console.log(count);
        $("<tr />", {id: divIdCount + "newsFeedsUrlDiv", 'class': "newsfeedClass"}).appendTo('#newsfeedFormId');
        $("<td />", {id: divIdCount + "newsLabelDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
        //  $("<label />", {id: divIdCount + "newsLabelLabel", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("Category").appendTo('#' + divIdCount + "newsLabelDiv");
        $("<div />", {id: divIdCount + "newsLabelDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsLabelDiv");
        $("<input />", {id: divIdCount + "newsFeedLabelId", 'class': "form-control", type: "text", name: "newsFeeds[" + count + "].labelName"}).appendTo("#" + divIdCount + "newsLabelDivIn");
        $("<td />", {id: divIdCount + "newsUrlDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
        // $("<label />", {id: divIdCount + "newsUrlLabel", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("URL").appendTo('#' + divIdCount + "newsUrlDiv");
        $("<div />", {id: divIdCount + "newsUrlDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsUrlDiv");
        $("<input />", {id: divIdCount + "newsUrlId", 'class': "form-control", type: "text", name: "newsFeeds[" + count + "].feedUrl"}).appendTo("#" + divIdCount + "newsUrlDivIn");
        $('<td />', {id: divIdCount + "newsUrlIdspan"}).html('<button type="button" class="btn btn-primary" onClick=\"removeTag(\'' + divIdCount + 'newsFeedsUrlDiv\')\"><span class="fa fa-trash" aria-hidden="true"></button>').appendTo("#" + divIdCount + "newsFeedsUrlDiv");

    }
    function addNewsFeeds() {
        var valueFeed = $("#newsUrlId").val();
        var organisationUrl = $("#organisationUrlId").val();

        if (valueFeed !== "" && organisationUrl !== "") {
            var rowCount = $('#newsFeedTable tr').length;
            if (rowCount !== 0 && rowCount !== 1) {

                $("#newsFeedTable").show();
            } else {
                $("#newsFeedTable").hide();

            }
            $('#newsFeedModelId').modal("show");
            $("#forFeedUrl").attr('value', valueFeed);
            $("#organisationUrlFeed").attr('value', organisationUrl);
            $("#forFeedUrl2").attr('value', valueFeed);
        } else {
            alert("Please provide Organisation and Feed URL.");
        }
    }
    function removeTag(divId) {
        $("#" + divId).remove();
        saveNewsFeeds();
    }
    function saveNewsFeeds() {
        var feedURL = "${pageContext.request.contextPath}/resource/newsfeed/save";
        var resourceForm = $("#popupHiddenForm").serialize();
        $.ajax(
                {
                    type: 'POST',
                    url: feedURL,
                    data: resourceForm,
                    cache: false,
                    success: function (data) {
                        isFeedEdit = true;
                        alert("Successfully Saved.");
                        $("#newsFeedUrlsId").attr("value", data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error in save feeds.");
                    }
                });
    }
    function autoCaptureFeeds(isEdit) {
        var feedURL;
        $("#newsFeedTable > tbody").html("");
        if (isEdit === "true") {
            var resourceCode = '<c:out value="${resource.resourceCode}" />';
            feedURL = "${pageContext.request.contextPath}/resource/refresh/autocapture/newsfeed/" + resourceCode;
            $.ajax(
                    {
                        type: 'POST',
                        url: feedURL,
                        cache: false,
                        success: function (data) {

                            addRefreshFeeds(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("Could not auto capture feeds for this URl. Please add manually.");
                        }
                    });
        } else {
            feedURL = "${pageContext.request.contextPath}/resource/autocapture/newsfeed";
            var resourceForm = $("#popupHiddenForm").serialize();
            $.ajax(
                    {
                        type: 'POST',
                        url: feedURL,
                        data: resourceForm,
                        cache: false,
                        success: function (data) {
                            addRefreshFeeds(data);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("Could not auto capture feeds for this URl. Please add manually.");
                        }
                    });
        }



    }

    function addRefreshFeeds(data) {
        var rowCount = $('#newsFeedTable tr').length;
        if (data.length !== 0) {
            $("#newsFeedTable").show();
            var divIdCount;// = i;
            for (var i = 0; i < data.length - 1; i++) {
                if (rowCount !== 0 && rowCount !== 1) {
                    if (i === 0) {
                        divIdCount = rowCount + 1;
                    } else {
                        divIdCount++;
                    }

                } else {

                    divIdCount = i;
                }

                $("<tr />", {id: divIdCount + "newsFeedsUrlDiv", 'class': "newsfeedClass"}).appendTo('#newsfeedFormId');
                $("<td />", {id: divIdCount + "newsLabelDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
                //  $("<label />", {id: divIdCount + "newsLabelLabel", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("Category").appendTo('#' + divIdCount + "newsLabelDiv");
                $("<div />", {id: divIdCount + "newsLabelDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsLabelDiv");
                $("<input />", {id: divIdCount + "newsFeedLabelId", value: data[i].labelName, 'class': "form-control", type: "text", name: "newsFeeds[" + divIdCount + "].labelName"}).appendTo("#" + divIdCount + "newsLabelDivIn");
                $("<td />", {id: divIdCount + "newsUrlDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
                // $("<label />", {id: divIdCount + "newsUrlLabel", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("URL").appendTo('#' + divIdCount + "newsUrlDiv");
                $("<div />", {id: divIdCount + "newsUrlDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsUrlDiv");
                $("<input />", {id: divIdCount + "newsUrlId", value: data[i].feedUrl, 'class': "form-control", type: "text", name: "newsFeeds[" + divIdCount + "].feedUrl"}).appendTo("#" + divIdCount + "newsUrlDivIn");
                $("<input />", {id: divIdCount + "newsUrlId", value: data[i].activeFeed, 'class': "form-control", type: "hidden", name: "newsFeeds[" + divIdCount + "].activeFeed"}).appendTo("#" + divIdCount + "newsUrlDivIn");
                $('<td />', {id: divIdCount + "newsUrlIdspan"}).html('<button type="button" class="btn btn-primary" onClick=\"removeTag(\'' + i + 'newsFeedsUrlDiv\')\"><span class="fa fa-trash" aria-hidden="true"></button>').appendTo("#" + divIdCount + "newsFeedsUrlDiv");

                //                            divIdCount = divIdCount + 1
            }
        } else {
            alert("Could not auto capture feeds for this URl. Please add manually .");
        }
    }
    function getFeedsRegister(value) {
        if (value !== "") {
            var data = JSON.parse(value);
            var str = "";
            for (var i = 0; i < data.length; i++) {
                var divIdCount = i;
                $("<tr />", {id: divIdCount + "newsFeedsUrlDiv", 'class': "newsfeedClass"}).appendTo('#newsfeedFormId');
                $("<td />", {id: divIdCount + "newsLabelDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
                //  $("<label />", {id: divIdCount + "newsLabelLabel", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("Category").appendTo('#' + divIdCount + "newsLabelDiv");
                $("<div />", {id: divIdCount + "newsLabelDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsLabelDiv");
                $("<input />", {id: divIdCount + "newsFeedLabelId", value: data[i].labelName, 'class': "form-control", type: "text", name: "newsFeeds[" + divIdCount + "].labelName"}).appendTo("#" + divIdCount + "newsLabelDivIn");
                $("<td />", {id: divIdCount + "newsUrlDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "newsFeedsUrlDiv");
                // $("<label />", {id: divIdCount + "newsUrlLabel", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("URL").appendTo('#' + divIdCount + "newsUrlDiv");
                $("<div />", {id: divIdCount + "newsUrlDivIn", 'class': "col-lg-12 col-md-12 col-sm-12"}).appendTo('#' + divIdCount + "newsUrlDiv");
                $("<input />", {id: divIdCount + "newsUrlId", value: data[i].feedUrl, 'class': "form-control", type: "text", name: "newsFeeds[" + divIdCount + "].feedUrl"}).appendTo("#" + divIdCount + "newsUrlDivIn");
                $('<td />', {id: divIdCount + "newsUrlIdspan"}).html('<button type="button" class="btn btn-primary" onClick=\"removeTag(\'' + i + 'newsFeedsUrlDiv\')\"><span class="fa fa-trash" aria-hidden="true"></button>').appendTo("#" + divIdCount + "newsFeedsUrlDiv");
            }
            $("#feedTable").html(str);
            $("#myModal").modal("show");
        } else {
            alert("No feeds available")
        }
    }
</script>