<%--
    Document   : thematic-type-edit
    Created on : May 17, 2016, 2:15:08 PM
    Author     : Suman Behara <sumanb@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card ">
        <div class="card-header">Edit Thematic</div>
        <div class="card-block clearfix">
            <form:form method="GET" commandName="thematicType"  id="thematicEditFormId">
                <div class="form-group row" style="text-align:center;">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  for="inputPassword"><tags:message code="res.thematic.list"/></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <form:input id="thematicId"  path="thematicType" cssClass="form-control" />
                        <form:hidden id="thematicTypeId" path="id" cssClass="form-control" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right"  for="inputPassword"><tags:message code="res.select.resource.type"/><em>*</em></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                <c:choose>
                                    <c:when test="${ not empty thematicType.resourceTypes}">
                                        <select id="resourceTypeList" multiple="multiple" size="8" class="form-control">
                                            <c:forEach var="resourceType" items="${resourceTypesList}">
                                                <c:set var="completeRT" value="${resourceType.resourceType}"/>
                                                <c:if test="${not fn:contains(thematicType.resourceTypes,resourceType)}" >
                                                    <option value="${resourceType.id}">${resourceType.resourceType}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                </c:choose>
                            </div>
                            <div class="col-lg-1  col-md-3 col-sm-6 col-xs-12">
                                <button type="button" class="btn btn-secondary btn-sm btn-block m-t-3" onClick="addResourceType();"><i class="fa fa-chevron-right"></i></button>
                                <button type="button"  class="btn btn-secondary btn-sm btn-block m-b-2" onClick="removeResourceType();"><i class="fa fa-chevron-left"></i></button>
                            </div>

                            <div class="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                                <div id="alreadyAssignedRTDiv">
                                    <select  multiple="multiple" size="8" class="form-control" id="alreadyAssignedRT" name="resourceTypes">
                                        <c:forEach var="assignedRT" items="${alreadyAssingedRTList}">
                                            <option>${assignedRT.resourceType}</option>
                                        </c:forEach>

                                        <c:forEach var="resourceType" items="${thematicType.resourceTypes}">
                                            <option value="${resourceType.id}">${resourceType.resourceType}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
            <div class="card-footer clearfix">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-sm" onclick="submitThematicForm();"><tags:message code="label.update"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function addResourceType() {
        $('#resourceTypeList option:selected').remove().appendTo('#alreadyAssignedRT')
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function removeResourceType() {
        $('#alreadyAssignedRT option:selected').remove().appendTo('#resourceTypeList')
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function submitThematicForm() {
        var thematicTypeId = $("#thematicTypeId").val();
        $('#alreadyAssignedRT option').prop('selected', true);
        var action = "${context}/resource/thematic/edit/" + thematicTypeId;
        $("#thematicEditFormId").attr("action", action);
        $("#thematicEditFormId").submit();
    }
</script>