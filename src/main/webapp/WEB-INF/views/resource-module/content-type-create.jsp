<%--
    Document   : content-type-create
    Created on : Apr 6, 2016, 12:35:12 PM
    Author     : Suman Behara
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${isSaved eq true}">
        <div id="alert-box" class="alert alert-success"> <span><tags:message code="res.content.type.create.success.msg"/></span></div>
    </c:if>  
    <div class="card ">
        <div class="card-header">             
            <c:choose>
                <c:when test="${isEditType=='true'}">
                    <span><tags:message code="label.edit"/> <tags:message code="label.ContactType"/></span>
                    <br />
                </c:when>    
                <c:otherwise>
                    <span> <tags:message code="label.add"/> <tags:message code="label.ContactType"/></span><a href="${context}/resource/contenttype/list" class="btn btn-secondary btn-sm pull-right"><tags:message code="res.view.content.type.list"/></a>
                    <br />
                </c:otherwise>
            </c:choose>                       
        </div>
        <form:form method="post" commandName="contentType"  id="contentTypeForm">
            <div class="card-block clearfix">

                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.ContactType"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input id="typeContentId" path="contentType" cssClass="form-control" required="required" placeholder="Enter Content Name" onkeypress="return onlySpecificChar(event, this);" onkeyup="findDuplicateContentName();"/>
                        <span class="text-danger font-bold" id="isduplicateId"> </span>
                    </div>
                </div>


            </div>
            <div class="card-footer clearfix">
                <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-sm" id="contentBtnId" onclick="submitContentType()" disabled="disabled"><tags:message code="label.submit"/></button>

                </div>
            </div>
        </form:form>
    </div>
</div>

<script type="text/javascript">
    function onlySpecificChar(e, t) {
        var regex = new RegExp("^[a-zA-Z0-9-_\b\t ]*$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();

    }
    function findDuplicateContentName() {
        var contentName = $('#typeContentId').val();
        if (contentName.length > 3) {
            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/checkduplicate/content/" + contentName,
                cache: false,
                data: "contentName=" + contentName,
                success: function (data) {
                    if (data === true) {
                        $('#isduplicateId').show();
                        $('#isduplicateId').html("Content name already exists...");
                        $("#contentBtnId").attr("disabled", "disabled");
                    } else {
                        $('#isduplicateId').hide();
                        $("#contentBtnId").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }
            });
        } else {
            $('#isduplicateId').show();
            $('#isduplicateId').html("Content name require minimum 4 characters...");
        }

    }
    function submitContentType() {
        var contentTypeId;
        var isEditType = "${isEditType}";
        if ('${isEditType}' === "true") {
            contentTypeId = "${contentType.id}";
        } else {
            contentTypeId = $("#typeContentId").val();
        }

        if (isEditType === "true") {
            action = "${context}/resource/content/edit/submit/" + contentTypeId;
        } else {
            $("#typeContentId").val($("#typeContentId").val());
            action = "${context}/resource/contenttype/submit";
        }
        $("#contentTypeForm").attr("action", action);
        $("#contentTypeForm").submit();
    }
</script>
