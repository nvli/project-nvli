<%--
    Document   : thematic-type-create
    Created on : Apr 8, 2016, 10:19:50 AM
    Author     : Suman
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${isSaved eq true}">
        <div class="alert alert-success"> <span>Thematic created successfully.</span></div>
    </c:if>
    <div class="card ">
        <div class="card-header clearfix"> <span class="pull-left">  Create Thematic</span>  <a href="${context}/resource/thematic/list" class="btn btn-secondary btn-sm pull-right">View Themes List</a></div>
        <div class="card-block clearfix" style="text-align:center;">
            <form:form method="post" commandName="thematicType"  id="thematicTypeFormId" style="text-align:center;">
                <div class="form-group row" style="text-align:center;">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  for="inputPassword"><tags:message code="res.create.thematic.type"/></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <form:input id="typeThemeId" path="thematicType" cssClass="form-control" required="required" placeholder="Enter Theme Name" onkeypress="return onlySpecificChar(event, this);" onkeyup="findDuplicateThemeName();"/>
                        <span class="text-danger font-bold" id="isduplicateId"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right"  for="inputPassword">Select Resource Type<em>*</em></label>
                    <div class="col-lg-9 col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                <c:choose>
                                    <c:when test="${ not empty resourceTypeList}">
                                        <select id="resourceTypeList" multiple="multiple" size="8" class="form-control">
                                            <c:forEach var="resourceType" items="${resourceTypeList}">
                                                <option value="${resourceType.id}">${resourceType.resourceType}</option>
                                            </c:forEach>
                                        </select>
                                    </c:when>

                                </c:choose>
                            </div>
                            <div class="col-lg-1  col-md-3 col-sm-6 col-xs-12">
                                <button type="button" class="btn btn-secondary btn-sm btn-block m-t-3" onClick="addResourceType();"><i class="fa fa-chevron-right"></i></button>
                                <button type="button"  class="btn btn-secondary btn-sm btn-block m-b-2" onClick="removeResourceType();"><i class="fa fa-chevron-left"></i></button>

                            </div>
                            <div class="col-lg-6  col-md-12 col-sm-12 col-xs-12">
                                <select size="8" class="form-control" id="alreadyAssignedRT" name="resourceTypes" multiple="multiple" >

                                    <c:forEach var="assignedRT" items="${alreadyAssingedRTList}">
                                        <option>${assignedRT.resourceType}</option>
                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </form:form>
        </div>
        <div class="card-footer clearfix">
            <div class="text-center">
                <button type="submit" class="btn btn-primary btn-sm" onclick="submitThemeForm();" id="themBtnId"><tags:message code="label.save"/></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><tags:message code="label.alert"/>Alert</h4>
            </div>
            <div class="card-block clearfix">
                <span class="pull-left font-weight-bold m-l-1 font120"><tags:message code="res.add.add.resource.type"/></span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#myModal").modal("hide");
    });
    function onlySpecificChar(e, t) {
        var regex = new RegExp("^[a-zA-Z0-9-_\b\t ]*$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            // str = str.replace(/\s\s+/g, ' ');
            return true;
        }
        e.preventDefault();
    }
    function addResourceType() {
        $('#resourceTypeList option:selected').remove().appendTo('#alreadyAssignedRT')
        $('#alreadyAssignedRT option').prop('.selected', true);
        $('#resourceTypeList option').prop('.selected', false);
    }
    function removeResourceType() {
        $('#alreadyAssignedRT Option:selected').remove().appendTo('#resourceTypeList')
        $('#alreadyAssignedRT option').prop('selected', true);
        $('#resourceTypeList option').prop('selected', false);
    }
    function submitThemeForm() {
        var alreadyassignValues = $("#alreadyAssignedRT").val();
        if (alreadyassignValues !== null) {
            $('#alreadyAssignedRT option').prop('selected', true);
            var action = "${context}/resource/thematictype/save";
            $("#thematicTypeFormId").attr("action", action);
            $("#thematicTypeFormId").submit();
        } else {
            $("#myModal").modal("show");
        }
    }
    function findDuplicateThemeName() {
        var themeName = $('#typeThemeId').val();
        if (themeName.length > 3) {
            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/checkduplicate/theme/" + themeName,
                cache: false,
                data: "themeName=" + themeName,
                success: function (data) {
                    if (data === true) {
                        $('#isduplicateId').show();
                        $('#isduplicateId').html("Theme name already exists...");
                        $("#themBtnId").attr("disabled", "disabled");
                    } else {
                        $('#isduplicateId').hide();
                        $("#themBtnId").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }
            });
        } else {
            $('#isduplicateId').show();
            $('#isduplicateId').html("Theme name require minimum 4 characters...");
        }
    }
</script>
