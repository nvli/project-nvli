<%--
    Document   : content-type-list
    Created on : Apr 15, 2016, 1:54:02 PM
    Author     : Suman Behara
    Author     : Savita kakad
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${not empty contentMessage}">
        <div class="alert alert-success"> <span>${contentMessage}</span></div>
    </c:if>
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <tags:message code="sb.content.type.list"/> &nbsp;
                    <a href="${context}/resource/contenttype/create" class="btn btn-success btn-sm"><tags:message code="sb.create.content.type"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search content type">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"><tags:message code="label.sno"/> </span></th>
                        <th><tags:message code="label.ContactType"/></th>
                        <th width="9%" style="text-align:center;"><tags:message code="res.edit.delete"/></th>
                    </tr>
                </thead>
                <tbody id="contentType-tbl-body"></tbody>

            </table>
        </div>
        <div id="contentType-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });



    function deleteContentType() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-contentType-button").attr("href");
        } else {
            return false;
        }
    }

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/contentType/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                console.log("success");
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.contentTypeList)) {
                        $("#contentType-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-contentType-list").html());

                        var Opts = {
                            "contentTypeList": jsonObject.contentTypeList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        console.log(Opts);
                        var articleTableBody = articlesTpl(Opts);
                        $("#contentType-tbl-body").empty();
                        $("#contentType-tbl-body").html(articleTableBody);
                        renderPagination(fetchAndRenderTableData, "contentType-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                console.log("err");
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-contentType-list">
    {{ _.each(contentTypeList, function (contentType,i){ }}
    <tr>
        <td>{{=i+1+recordCount}}</td>
        <td >{{=contentType.contentType}}</td>
        <td style="text-align:center;">
         <div class="btn-group">
            <a href="${pageContext.request.contextPath}/resource/content/edit/populate/{{=contentType.id}}" class="btn btn-secondary btn-sm" title="Edit"><span class="fa fa-pencil" aria-hidden="true"></span></a>
            <a id="remove-contentType-button" href="${context}/resource/contenttype/remove/{{=contentType.id}}" class="btn btn-secondary btn-sm" title="Delete" onclick="return deleteContentType();" ><span class="fa fa-trash" aria-hidden="true"></span></a>
         </div>
        </td>
    </tr>
    {{ }); }}
</script>