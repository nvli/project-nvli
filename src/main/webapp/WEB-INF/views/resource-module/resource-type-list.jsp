<%--
    Document   : resource-type-list
    Created on : Apr 15, 2016, 5:12:13 PM
    Author     : Suman Behara
    Author     : Sujata Aher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${not empty resTypeMessage}">         
        <div class="alert alert-success"> <span>${resTypeMessage}</span></div>
    </c:if>
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                    <tags:message code="res.resource.type.list"/>
                    <a href="${context}/resource/type/create" class="btn btn-sm btn-success"><tags:message code="res.create.resource.type"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-2">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search resource type">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 col-xl-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"><tags:message code="label.sno"/></th>
                        <th><tags:message code="label.resource.type"/></th>
                        <th width="9%" ><tags:message code="label.total.records"/></th>
                        <th width="12%" align="center"><tags:message code="sb.resource.register"/></th>
                            <%--<th width="9%"><tags:message code="res.draft.register"/></th>--%>
                        <th width="14%" align="center"><tags:message code="res.edit.delete"/></th>
                    </tr>
                </thead>
                <tbody id="resourceType-tbl-body"></tbody>
            </table>
        </div>
        <div id="resourceType-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function deleteResourceType(type) {
        if (confirm("\n Are you sure you want to delete " + type + " resource type?")) {
            if (confirm("All rsource associated with " + type + " will get delted after deleting this resource Type.\n Are you sure you want to delete?")) {
                $("#remove-resourcetype-button").attr("href");
            }
        } else {
            return false;
        }
    }

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/resourceType/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                console.log("success");
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.resourceTypeList)) {
                        $("#resourceType-tbl-body").html('<tr><td colspan="7" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-resourceType-list").html());

                        var Opts = {
                            "resourceTypeList": jsonObject.resourceTypeList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        console.log(Opts);
                        var articleTableBody = articlesTpl(Opts);
                        $("#resourceType-tbl-body").empty();
                        $("#resourceType-tbl-body").html(articleTableBody);
                    }
                    renderPagination(fetchAndRenderTableData, "resourceType-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                console.log("err");
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-resourceType-list">
    {{ _.each(resourceTypeList, function (resourceType,i){ }}
    <tr><td>{{=i+1+recordCount}}</td>
    <td >
    {{=resourceType.resourceType}}
    </td><td>
    {{=resourceType.totalRecordCount}}
    </td>
    <td align="center">
    <a href="${context}/resource/register/{{=resourceType.id}}" class="btn btn-secondary" title="View"> <span class="fa fa-eye" aria-hidden="true"></span></a>
    </td>
    <%--<td>
    <a href="${context}/resource/draft/register/{{=resourceType.id}}" class="btn btn-secondary" title="View"> <span class="fa fa-eye" aria-hidden="true"></span></a>
    </td>--%>
    <td align="center">
     <div class="btn-group">
    {{ if(resourceType.publishFlag){ }}
    <a  class="btn btn-secondary" href="${pageContext.request.contextPath}/resource/type/deactivate/{{=resourceType.resourceTypeCode}}" >
    <img src="${context}/images/withdraw.png" title="Withdraw" width="20">
    </a>
    {{ }else if(!resourceType.publishFlag){ }}
    <a  class="btn btn-secondary" href="${pageContext.request.contextPath}/resource/type/activate/{{=resourceType.resourceTypeCode}}" >
    <img src="${context}/images/publish.png" title="Publish" width="20">
    </a>
    {{ } }}
    <a href="${pageContext.request.contextPath}/resource/type/edit/{{=resourceType.id}}" class="btn btn-secondary" title="Edit"><span class="fa fa-pencil" aria-hidden="true"></span></a>
    <a id="remove-resourcetype-button" href="${context}/resource/type/remove/{{=resourceType.resourceTypeCode}}" class="btn btn-secondary" title="Delete" onclick="return deleteResourceType('{{=resourceType.resourceType}}');" ><span class="fa fa-trash" aria-hidden="true"></span></a>
  </div>
            </td>
    </tr>

    {{ }); }}
</script>

