<%--
    Document   : thematic-type-list
    Created on : Apr 15, 2016, 2:57:48 PM
    Author     : Suman Behara
    Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${not empty thematicMessage}">
        <div class="alert alert-success"> <span>${thematicMessage}</span></div>
    </c:if>
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <tags:message code="res.thematic.list"/> &nbsp; 
                    <a href="${context}/resource/thematictype/create" class="btn btn-success btn-sm"><tags:message code="res.create.thematic.type"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search Thematic Type">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"><tags:message code="label.sno"/></th>
                        <th width="8%" ><tags:message code="res.thematic.list"/></th>
                        <th ><tags:message code="label.resource-type"/></th>
                        <th width="9%" style="text-align:center;"><tags:message code="res.edit.delete"/></th>

                    </tr>
                </thead>
                <tbody id="thematicType-tbl-body"></tbody>
            </table>
        </div>
        <div id="thematicType-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function deleteThematicType() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-thematicType-button").attr("href");
        } else {
            return false;
        }
    }

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/thematicType/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                console.log("success");
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.thematicTypeList)) {
                        $("#thematicType-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-thematicType-list").html());

                        var Opts = {
                            "thematicTypeList": jsonObject.thematicTypeList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        console.log(Opts);
                        var articleTableBody = articlesTpl(Opts);
                        $("#thematicType-tbl-body").empty();
                        $("#thematicType-tbl-body").html(articleTableBody);
                        renderPagination(fetchAndRenderTableData, "thematicType-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                console.log("err");
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-thematicType-list">
    {{ _.each(thematicTypeList, function (thematicType,i){ }}
    <tr><td>{{=i+1+recordCount}}</td>
    <td >
    {{=thematicType.thematicType}}
    </td>
    <td>
    {{_.each(thematicType.resourceTypes, function (resourceType) {}}
    {{=resourceType.resourceType}}
    {{});}}
    </td>
    <td style="text-align:center;">
     <div class="btn-group">
        <a href="${pageContext.request.contextPath}//resource/thematic/populate/{{=thematicType.id}}" class="btn btn-secondary btn-sm" title="Edit"><span class="fa fa-pencil" aria-hidden="true"></span></a>
        <a id="remove-thematicType-button" href="${context}/resource/thematic/remove/{{=thematicType.id}}" class="btn btn-secondary btn-sm" title="Delete" onclick="return deleteThematicType();" ><span class="fa fa-trash" aria-hidden="true"></span></a>
     </div>
    </td>
    </tr>
    {{ }); }}
</script>

