<%-- 
    Document   : resource-type-create
    Created on : Apr 6, 2016, 11:41:44 AM
    Author     : Sujata Aher
--%>

<%@page import="in.gov.nvli.domain.resource.ResourceTypeInfo"%>
<%@page import="java.util.List"%>
<%@page import="in.gov.nvli.domain.crowdsource.UDCLanguage"%>
<%@page import="in.gov.nvli.domain.resource.ResourceType"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%
    ResourceType resourceType = (ResourceType) request.getAttribute("resourceType");
    List<UDCLanguage> languageListRm = (List<UDCLanguage>) request.getAttribute("languageList");
%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header cardHeaderInputMargin">             
            <c:choose>
                <c:when test="${isEditType=='true'}">
                    <tags:message code="label.edit"/> <tags:message code="label.resource.type"/> <span class="pull-right"> <a href="javascript:void(0)" class="btn btn-secondary btn-sm"  onClick="addInfoTag();"><i class="fa fa-plus"></i> <tags:message code="label.add.language"/></a> </span>
                    <br />
                </c:when>    
                <c:otherwise>
                    <tags:message code="button.AddResource"/><span class="pull-right"> <a href="javascript:void(0)" class="btn btn-secondary btn-sm"  onClick="addInfoTag();"><i class="fa fa-plus"></i> <tags:message code="label.add.language"/></a> </span>
                    <br />
                </c:otherwise>
            </c:choose>                       
        </div>
        <div class="card-block">
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" for="inputPassword"><tags:message code="label.language"/></label>
                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12"> 
                    <input disabled  class="form-control" type="text" value="${engLanguage.languageName}"/> 
                </div>
            </div>
            <form:form method="post" commandName="resourceType"  id="resourceTypeForm" >
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" for="inputPassword"><tags:message code="label.category"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12"> 
                        <c:choose>
                            <c:when test="${isEditType}">
                                <input disabled name="resourceTypeCategory" class="form-control" type="text" value=" ${resourceType.resourceTypeCategory}"/> 
                                <input name="resourceTypeCategory" class="form-control" type="hidden" value=" ${resourceType.resourceTypeCategory}"/> 
                            </c:when>
                            <c:otherwise>
                                <select name="resourceTypeCategory" class="form-control" id="rtCategoryId" onclick="return changeResourceType('resourceTypeId');">
                                    <option value="" label="Select Resource Type Category"><tags:message code="res.resource.type.category"/></option>
                                    <c:forEach items="${categoryList}" var="category">
                                        <option value="${category}" >${category}</option>
                                    </c:forEach>
                                </select>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" for="inputPassword"><tags:message code="label.code"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12"> 
                        <c:choose>
                            <c:when test="${isEditType}">
                                <input disabled name="resourceTypeCode" class="form-control" type="text" value=" ${resourceType.resourceTypeCode}" id="resourceTypeCodeId"/> 
                                <input name="resourceTypeCode" class="form-control" type="hidden" value=" ${resourceType.resourceTypeCode}"/> 
                            </c:when>
                            <c:otherwise>
                                <input onchange="" onkeypress="return onlyAlphabets(event, this);"  id="resourceTypeCodeId" name="resourceTypeCode" class="form-control"  value="${resourceType.resourceTypeCode}" type="text" onkeyup="findDuplicateRTCode(this);">
                            </c:otherwise>
                        </c:choose>

                        <span class="text-danger font-bold" id="errorForRTCode"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left" for="inputPassword"><tags:message code="label.name"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12"> 
                        <input onkeyup="findDuplicateRTName();" id="resourceTypeNameId"  name="resourceType" class="form-control"  value="${resourceType.resourceType}" type="text" >
                        <span class="text-danger font-bold" id="errorForRTName"> </span>
                    </div>
                </div>
                <input  name="resourceTypeIcon" id="resourceTypeIconHiddenId" type="hidden" value="${resourceType.resourceTypeIcon}">
                <input  name="resourceTypedefaultIcon" id="rTDefaultIconHiddenId" type="hidden" value="${resourceType.resourceTypedefaultIcon}">
                <input  name="resourceTypeSearchIcon" id="rTSearchIconHiddenId" type="hidden" value="${resourceType.resourceTypeSearchIcon}">
                <div id="resourceTypeInfoId">

                    <c:choose>
                        <c:when test="${not empty resourceType.resourceTypeInfo}">
                            <c:forEach items="${resourceType.resourceTypeInfo}" var="resourceTypeInfo" varStatus="rtCount">
                                <div id="${rtCount.index}resourceTypeInfoDiv" class="resourceTypeInfoDiv">
                                    <c:if test="${rtCount.index!=0}">
                                        <div class="m-t-2 p-b-1" id="${rtCount.index}resourceTypeInnerDiv">
                                           <h4 id="1removeDivId" class="clearfix m-a-0"><span><tags:message code="label.language"/> ${rtCount.index}</span>
                                                <span class="pull-right" id="${rtCount.index}removeSpan"> <a onclick="removeTag('${rtCount.index}resourceTypeInfoDiv', '${resourceTypeInfo.id}');" href="#" class="btn btn-secondary btn-sm btn-danger"><i class="fa fa-trash"></i> Delete Block</a> </span>
                                            </h4>
                                            <hr/>
                                        </c:if>
                                        <div class="form-group row" id="${rtCount.index}langDiv">
                                            <c:choose>
                                                <c:when test="${isEditType}">
                                                    <c:if test="${rtCount.index!=0}">
                                                        <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword" id="${rtCount.index}LangLbl"><tags:message code="label.language"/></label>
                                                        <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 rtLangDivClass" id="${rtCount.index}LangInputDiv">
                                                            <input disabled name="resourceTypeInfo[${rtCount.index}].language" class="form-control" type="text" value="${resourceType.resourceTypeInfo.get(rtCount.index).language.languageName}"/> 

                                                        </div>
                                                    </c:if>
                                                    <input id="${rtCount.index}languageListSel" name="resourceTypeInfo[${rtCount.index}].language" type="hidden" value="${resourceTypeInfo.language.id}"/> 
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 rtLangDivClass" id="${rtCount.index}LangInputDiv">
                                                        <select required id="${rtCount.index}languageListSel" name="resourceTypeInfo[${rtCount.index}].language" class="form-control" onchange="onChangeLang('${rtCount.index}languageListSel')">
                                                            <option value="" label="Select Language">Select Language</option>
                                                            <c:forEach var="language" items="${languageList}" varStatus="cnt">
                                                                <option value="${language.id}" ${resourceTypeInfo.language.id == language.id? 'selected' : ' '}>${language.languageName}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>

                                            <span class="text-danger font-bold" id="langErrorId"></span>

                                        </div>
                                        <c:if test="${rtCount.index!=0}">
                                            <div class="form-group row" id="${rtCount.index}titleDiv">
                                                <label id="${rtCount.index}titleLbl" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.name"/></label>
                                                <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="${rtCount.index}titleInputDiv">
                                                    <input required id="${rtCount.index}titleId" name="resourceTypeInfo[${rtCount.index}].title" class="form-control" type="text" value="${resourceType.resourceTypeInfo.get(rtCount.index).title}"/> 
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${rtCount.index==0}">
                                            <input required id="${rtCount.index}titleId" name="resourceTypeInfo[${rtCount.index}].title" class="form-control" type="hidden" value="${resourceType.resourceTypeInfo.get(rtCount.index).title}"/> 
                                        </c:if>
                                        <div class="form-group row" id="${rtCount.index}desDiv">
                                            <label id="${rtCount.index}DesLbl" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.description"/></label>
                                            <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="${rtCount.index}desInputDiv">
                                                <textarea required="" id="${rtCount.index}DesId" name="resourceTypeInfo[${rtCount.index}].description" class="form-control" cols="5" rows="5">${resourceType.resourceTypeInfo.get(rtCount.index).description.trim()}</textarea>
                                            </div>
                                        </div>
                                        <input type="hidden" name="resourceTypeInfo[${rtCount.index}].id" value="${resourceType.resourceTypeInfo.get(rtCount.index).id}"/> 
                                        <input type="hidden" name="resourceTypeInfo[${rtCount.index}].resourceType" value="${resourceType.id}"/> 
                                        <c:if test="${rtCount.index!=0}">
                                        </div>
                                    </c:if>
                                </div>  

                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <div id="0resourceTypeInfoDiv" class="resourceTypeInfoDiv">
                                <div class="form-group row" id="0langDiv">
                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12 rtLangDivClass" id="0LangInputDiv">
                                        <input name="resourceTypeInfo[0].language" type="hidden" value="${engLanguage.id}"/> 
                                        <span class="text-danger font-bold" id="langErrorId"></span>
                                    </div>
                                </div>
                                <input id="0titleId" name="resourceTypeInfo[0].title" class="form-control" type="hidden"/> 
                                <div class="form-group row" id="0desDiv">
                                    <label id="0DesLbl" class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="label.description"/></label>
                                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12" id="0desInputDiv">
                                        <textarea id="0DesId" name="resourceTypeInfo[0].description" required="" class="form-control" cols="5" rows="5"><c:if test="${not empty resourceType.resourceTypeInfo}">${resourceType.resourceTypeInfo.get(0).description.trim()}</c:if></textarea>
                                        </div>
                                    </div>
                                    <input type="hidden" name="resourceTypeInfo[0].resourceType" value="${resourceType.id}"/>    
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </form:form>
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.resource.type.icon"/></label>
                <div class="col-lg-6 col-md-7 col-sm-12">
                    <div class="profile-img"> 
                        <form id="resourceTypeIconForm" > 
                            <a id="upload_resourceType_icon_btn" href="javascript:void(0)" onClick="callUploadIcon('main');" class="link2"><tags:message code="label.uploadIcon"/></a>
                            <input id="upload_resourceType_icon" type="file" name="uploadfile" style="visibility: hidden"/>
                            <input name="resourceType" type="hidden" id="resourceTypeHidden"/>
                        </form>
                        <span id="upload-file-message">
                            <c:if test="${not empty resourceType.resourceTypeIcon}">
                                <img id="imageUploadSrc" src="${rtIconLocation}/${resourceType.resourceTypeIcon}" height="100" width="100" />
                            </c:if>
                        </span>

                    </div>
                </div>
            </div> 
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.resource.type.default.icon"/></label>
                <div class="col-lg-6 col-md-7 col-sm-12">
                    <div class="profile-img"> 
                        <form id="rTDefaultIconForm" > 
                            <a id="upload_rt_default_icon_btn" href="javascript:void(0)" onClick="callUploadIcon('default');" class="link2"><tags:message code="label.uploadIcon"/></a>
                            <input id="upload_rt_default_icon" type="file" name="uploadfileDefault" style="visibility: hidden"/>
                            <input name="resourceType" type="hidden" id="rtDefaultHidden"/>
                        </form>
                        <span id="upload-file-message-default">
                            <c:if test="${not empty resourceType.resourceTypedefaultIcon}">
                                <img id="imageUploadSrcDefault" src="${rtIconDefalutLocation}/${resourceType.resourceTypedefaultIcon}" height="100" width="100" />
                            </c:if>
                        </span>

                    </div>
                </div>
            </div> 
            <div class="form-group row">
                <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><tags:message code="res.resource.type.search.icon"/></label>
                <div class="col-lg-6 col-md-7 col-sm-12">
                    <div class="profile-img"> 
                        <form id="rTSearchIconForm" > 
                            <a id="upload_rt_search_icon_btn" href="javascript:void(0)" onClick="callUploadIcon('search');" class="link2"><tags:message code="label.uploadIcon"/></a>
                            <input id="upload_rt_search_icon" type="file" name="uploadFileSearch" style="visibility: hidden"/>
                            <input name="resourceType" type="hidden" id="rtSearchHidden"/>
                        </form>
                        <span id="upload-file-message-search">
                            <c:if test="${not empty resourceType.resourceTypeSearchIcon}">
                                <img id="imageUploadSrcSearch" src="${rtIconSearchLocation}/${resourceType.resourceTypeSearchIcon}" height="24" width="24" />
                            </c:if>
                        </span>

                    </div>
                </div>
            </div> 
        </div>
        <div class="card-footer clearfix">
            <div class="marginauto">
                <button type="submit" class="btn btn-primary" onclick="submitResourceType()" id="resourceTypeBtnId"><tags:message code="button.submit"/></button>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"><tags:message code="label.alert"/></h4>
                </div>
                <div class="card-block clearfix">
                    <span class="pull-left font-weight-bold m-l-1 font120 " id="alertBoxId"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .border {border-bottom:1px solid #ccc;}
</style>
<script type="text/javascript">
    $(document).ready(function ()
    {
        $("#myModal").modal("hide");
        $("#upload_resourceType_icon").on("change", uploadFile);
        $("#upload_rt_default_icon").on("change", uploadFileDefault);
        $("#upload_rt_search_icon").on("change", uploadFileSearch);
    });

    function callUploadIcon(type) {
        if (type === "main") {
            $("#upload_resourceType_icon").click();
        } else if (type === "default") {
            $("#upload_rt_default_icon").click();
        } else if (type === "search") {
            $("#upload_rt_search_icon").click();
        }
    }

    function uploadFile() {
        var typeName = $('#resourceTypeCodeId').val().trim();
        $("#resourceTypeHidden").attr("value", typeName);
        $("#upload-file-message").html("");
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/type/upload/icon",
            type: "POST",
            data: new FormData($("#resourceTypeIconForm")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                // Handle upload success
                $("#upload-file-message").html("");
                $("#resourceTypeIconHiddenId").val(data);
                $("#upload-file-message").html('<img id="imageUploadSrc" src="${rtIconLocation}/' + data + '" height="100" width="100" />');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Handle upload error
                $("#upload-file-message").text(
                        "File not uploaded (perhaps it's too much big)");
            }
        });
    } // function uploadFile

    function uploadFileDefault() {
        var typeName = $('#resourceTypeCodeId').val().trim();
        $("#rtDefaultHidden").attr("value", typeName);
        $("#upload-file-message-default").html("");
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/type/default/upload/icon",
            type: "POST",
            data: new FormData($("#rTDefaultIconForm")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                // Handle upload success
                $("#upload-file-message-default").html("");
                $("#rTDefaultIconHiddenId").val(data);
                $("#upload-file-message-default").html('<img id="imageUploadSrcDefault" src="${rtIconDefalutLocation}/' + data + '" height="100" width="100" />');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Handle upload error
                $("#upload-file-message-default").text(
                        "File not uploaded (perhaps it's too much big)");
            }
        });
    } // function uploadFile
    function uploadFileSearch() {
        var typeName = $('#resourceTypeCodeId').val().trim();
        $("#rtSearchHidden").attr("value", typeName);
        $("#upload-file-message-search").html("");
        $.ajax({
            url: "${pageContext.request.contextPath}/resource/type/search/upload/icon",
            type: "POST",
            data: new FormData($("#rTSearchIconForm")[0]),
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                // Handle upload success
                $("#upload-file-message-search").html("");
                $("#rTSearchIconHiddenId").val(data);
                $("#upload-file-message-search").html('<img id="imageUploadSrcSearch" src="${rtIconSearchLocation}/' + data + '" height="24" width="24" />');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // Handle upload error
                $("#upload-file-message-search").text(
                        "File not uploaded (perhaps it's too much big)");
            }
        });
    } // function uploadFile
    function onChangeLang(selecttagId) {
        var value = $("#" + selecttagId).val();
        var languageCount = $('.rtLangDivClass').length;
        if (value !== "") {
            for (var i = 0; i < languageCount; i++) {
                if (value === $("#" + i + "languageListSel").val() && selecttagId !== i + "languageListSel") {
                    alert("Language already selected. Please select other value");
                    $("#" + selecttagId).val("");
                    break;
                }
            }
        }
    }
    function submitResourceType() {
        var resourceTypeId = "${resourceType.id}";
        var isEditType = "${isEditType}";
        var langaugeFull = true;
        var titleFull = true;
        var desFull = true;
        var action;

        if ($("#resourceTypeNameId").val() === "") {
            document.getElementById("resourceTypeNameId").focus();
            titleFull = false
        }
        if ($("#rtCategoryId").val() === "") {
            document.getElementById("rtCategoryId").focus();
            langaugeFull = false;
        }
        if ($("#resourceTypeCodeId").val() === "") {
            document.getElementById("resourceTypeCodeId").focus();
            titleFull = false
        }

        $('select').each(function (n, element) {
            if ($(element).val() === '') {
                $(this).focus();
                langaugeFull = false;
            }
        });
        $('input[type=text]').each(function (n, element) {
            if ($(element).val() === '') {
                $(this).focus();
                titleFull = false
            }
        });
        $('textarea').each(function (n, element) {
            if ($(element).val() === '') {
                $(this).focus();
                desFull = false;

            }
        });
        if (!desFull || !titleFull || !langaugeFull) {
            alert("Please Fill all the Fields");
        } else {
            $("#0titleId").val($("#resourceTypeNameId").val());

            if (isEditType === "true") {
                action = "${context}/resource/type/edit/submit/" + resourceTypeId;
            } else {
                action = "${context}/resource/type/submit";
            }
            $("#resourceTypeForm").attr("action", action);
            $("#resourceTypeForm").submit();
        }

    }
    function addInfoTag() {
        var isEditType = "${isEditType}";
        var count = $('#resourceTypeInfoId > .resourceTypeInfoDiv').length;
        var lastDiv;
        if (count === 0) {
            lastDiv = $('#resourceTypeInfoId > .resourceTypeInfoDiv')[0].id;
        } else {
            lastDiv = $('#resourceTypeInfoId > .resourceTypeInfoDiv')[count - 1].id;
        }
        var num = lastDiv.substring(0, lastDiv.indexOf("resourceTypeInfoDiv"));
        var divIdCount = parseInt(num) + 1;
        var typeId;
        if (isEditType === 'true') {
            typeId = "${resourceType.id}";
        }
        $("<div />", {id: divIdCount + "resourceTypeInfoDiv", 'class': "resourceTypeInfoDiv"}).appendTo('#resourceTypeInfoId');
        $("<div />", {id: divIdCount + "resourceTypeInnerDiv", 'class': "m-t-2 p-b-1"}).appendTo('#' + divIdCount + 'resourceTypeInfoDiv');
        $("<h4 />", {id: divIdCount + "removeDivId", 'class': "clearfix m-a-0"}).appendTo('#' + divIdCount + "resourceTypeInnerDiv");
        $("<span />").html("Language " + (parseInt(divIdCount))).appendTo('#' + divIdCount + "removeDivId");
        $("<span />", {id: divIdCount + "removeSpan", 'class': "pull-right"}).appendTo('#' + divIdCount + "removeDivId");
        $("<a/>", {href: "#", 'class': "btn btn-secondary btn-sm btn-danger", click: function () {
                removeTag(count + "resourceTypeInfoDiv", '0');
            }}).html('<i class="fa fa-trash"></i> Delete Block').appendTo("#" + divIdCount + "removeSpan");
        $("<hr/>").appendTo('#' + divIdCount + "removeDivId");
        $("<div />", {id: divIdCount + "langDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "resourceTypeInnerDiv");
        $("<label />", {id: divIdCount + "LangLbl", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("Lanugage").appendTo('#' + divIdCount + "langDiv");
        $("<div />", {id: divIdCount + "langInputDiv", 'class': "col-lg-6 col-md-7 col-sm-12 rtLangDivClass"}).appendTo('#' + divIdCount + "langDiv");
        $("<select/>", {id: divIdCount + "languageListSel", 'class': "form-control", name: "resourceTypeInfo[" + divIdCount + "].language", required: "required", click: function () {
                onChangeLang(count + 'languageListSel');
            }}).html("<option label='Select' value=''>Select</option><c:forEach items='<%=languageListRm%>' var='language'>" + "<option label='${language.languageName}' value='${language.id}'>${language.languageName}</option></c:forEach>").appendTo('#' + divIdCount + "langInputDiv");

        $("<div />", {id: divIdCount + "titleDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "resourceTypeInnerDiv");
        $("<label />", {id: divIdCount + "titleLbl", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("Name").appendTo('#' + divIdCount + "titleDiv");
        $("<div />", {id: divIdCount + "titleInputDiv", 'class': "col-lg-6 col-md-7 col-sm-12"}).appendTo('#' + divIdCount + "titleDiv");
        $("<input />", {required: "required", id: divIdCount + "titleId", 'class': "form-control", type: "text", name: "resourceTypeInfo[" + divIdCount + "].title"}).appendTo("#" + divIdCount + "titleInputDiv");

        $("<div />", {id: divIdCount + "desDiv", 'class': "form-group row"}).appendTo('#' + divIdCount + "resourceTypeInnerDiv");
        $("<label />", {id: divIdCount + "desLbl", 'class': "form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"}).html("Description").appendTo('#' + divIdCount + "desDiv");
        $("<div />", {id: divIdCount + "desInputDiv", 'class': "col-lg-6 col-md-7 col-sm-12"}).appendTo('#' + divIdCount + "desDiv");
        $("<textarea />", {cols: 5, rows: 5, required: "required", id: divIdCount + "desId", 'class': "form-control", type: "text", name: "resourceTypeInfo[" + divIdCount + "].description"}).appendTo("#" + divIdCount + "desInputDiv");
        $("<input />", {id: divIdCount + "hiddenId", 'class': "form-control", type: "hidden", name: "resourceTypeInfo[" + divIdCount + "].resourceType", value: typeId}).appendTo("#" + divIdCount + "desInputDiv");

    }

    function removeTag(divId, index) {
        $("#" + divId).remove();
        if (index !== 0) {
            var feedURL = "${pageContext.request.contextPath}/resource/type/remove/info/${resourceType.id}/" + index;
            $.ajax(
                    {
                        type: 'POST',
                        url: feedURL,
                        cache: false,
                        success: function (data) {

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
        }
    }

    function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            } else if (e) {
                var charCode = e.which;
            } else {
                return true;
            }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                return true;
            else
                return false;
        } catch (err) {
            alert(err.Description);
        }
    }

    function findDuplicateRTCode(codeVal) {
        codeVal.value = codeVal.value.toUpperCase();
        var resourceTypeCode = codeVal.value;
        if (resourceTypeCode.length > 3) {
            if (resourceTypeCode.length < 7) {
                $.ajax({
                    type: 'GET',
                    url: "${pageContext.request.contextPath}/resource/type/checkduplicate/code/" + resourceTypeCode,
                    cache: false,
                    success: function (data) {
                        if (data === true) {
                            $('#errorForRTCode').show();
                            $('#errorForRTCode').html("Resource type code already exists...");
                            $("#resourceTypeBtnId").attr("disabled", "disabled");
                        } else {
                            $('#errorForRTCode').hide();
                            $("#resourceTypeBtnId").removeAttr("disabled");
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("In error");
                    }
                });
            } else {
                $('#errorForRTCode').show();
                $("#resourceTypeBtnId").attr("disabled", "disabled");
                $('#errorForRTCode').html("Resource type code require maximum 6 characters...");
            }
        } else {
            $('#errorForRTCode').show();
            $("#resourceTypeBtnId").attr("disabled", "disabled");
            $('#errorForRTCode').html("Resource type code require minimum 4");
        }
    }


    function findDuplicateRTName() {
        var resourceTypeName = $('#resourceTypeNameId').val();
        var isEditType = "${isEditType}";
        if (resourceTypeName.length > 3) {
            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/type/checkduplicate/name/" + resourceTypeName,
                cache: false,
                success: function (data) {
                    if (data === true) {
                        $('#errorForRTName').show();
                        $('#errorForRTName').html("Resource type name already exists...");
                        $("#resourceTypeBtnId").attr("disabled", "disabled");
                    } else {
                        $('#errorForRTName').hide();
                        $("#resourceTypeBtnId").removeAttr("disabled");
                    }
                    if (isEditType === "true") {
                        $("#resourceTypeBtnId").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }
            });
        } else {
            $('#errorForRTName').show();
            $("#resourceTypeBtnId").attr("disabled", "disabled");
            $('#errorForRTName').html("Resource type name require minimum 4 characters...");
        }
        if (isEditType === "true") {
            $("#resourceTypeBtnId").removeAttr("disabled");
        }
    }
</script>
