<%--
    Document   : organization_create
    Created on : May 23, 2017, 2:52:25 PM
    Author     : Sujata Aher
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>   
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <c:if test="${not empty organizationMsg}">
        <div class="alert alert-success"> <span>${organizationMsg}</span></div>
    </c:if>
    <div class="card ">
        <div class="card-header clearfix">  
            <i class="fa fa-sitemap"></i>
            <c:choose>
                <c:when test="${isEditType=='true'}">
                    <span><tags:message code="label.edit"/> <tags:message code="label.organization"/></span>
                    <br />
                </c:when>    
                <c:otherwise>
                    <span><spring:message code="label.organization.create"/></span> <a href="${context}/resource/organization/list" class="btn btn-secondary btn-sm pull-right">View <spring:message code="label.organization.list"/></a>
                    <br />
                </c:otherwise>
            </c:choose>                       
        </div>           

        <c:choose>
            <c:when test="${isEditType}">
                <c:set value="${context}/resource/organization/edit/submit/${organization.id}" var="actionUrl"></c:set>
            </c:when>
            <c:otherwise>
                <c:set value="${context}/resource/organization/submit" var="actionUrl"></c:set>
            </c:otherwise>
        </c:choose>
        <form:form commandName="organization"  id="organizationFormId" action="${actionUrl}">
            <div class="card-block clearfix">
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.name"/><em>*</em></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input id="organizationNameId" path="name" cssClass="form-control form-control-sm" required="required" placeholder="Enter Organization Name" onkeypress="return onlySpecificChar(event, this);" onkeyup="findDuplicateOrganizationName();" />
                        <span class="text-danger font-bold" id="isOrganizationNameduplicateId"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.url"/><em>*</em></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="organisationUrl" cssClass="form-control" required="required" id="organisationURLID" placeholder="Enter Organization URL" />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.contactPersonName"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="contactPersonName" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.designation"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="designation" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.email"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="email" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.phoneNumber"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="phoneNumber" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.address"/><em>*</em></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:textarea path="address" cssClass="form-control" cols="5" rows="5" required="required" id="adressId"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.city"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="city" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.country"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="country" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.latitude"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="latitude" cssClass="form-control"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"  for="inputPassword"><spring:message code="label.organization.longitude"/></label>
                    <div class="col-xl-6 col-lg-8 col-md-9 col-sm-9 col-xs-12">
                        <form:input path="longitude" cssClass="form-control form-control-sm"/>
                    </div>
                </div>
            </div>
            <div class="card-footer clearfix">
                <div class="text-center">
                    <button  id="orgSubmitBtn" type="submit" class="btn btn-primary btn-sm"><spring:message code="label.save"/></button>
                </div>
            </div>
        </form:form>


    </div>    
</div>
<script type="text/javascript">
    
    function onlySpecificChar(e, t) {
        var regex = new RegExp("^[a-zA-Z0-9-_\b\t ]*$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();

    }
    function findDuplicateOrganizationName() {
        var organizationName = $('#organizationNameId').val();

        if (organizationName.length > 2) {

            $.ajax({
                type: 'GET',
                url: "${pageContext.request.contextPath}/resource/checkduplicate/organizationname/" + organizationName,
                cache: false,
                data: "organizationName=" + organizationName,
                success: function (data) {
                    console.log(data);
                    if (data === true) {
                        $('#isOrganizationNameduplicateId').show();
                        $('#isOrganizationNameduplicateId').html("Organization name already exists...");
                        $("#orgSubmitBtn").attr("disabled", "disabled");

                    } else {
                        $('#isOrganizationNameduplicateId').hide();
                        $("#orgSubmitBtn").removeAttr("disabled");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("In error");
                }});
        } else {
            $('#isOrganizationNameduplicateId').show();
            $('#isOrganizationNameduplicateId').html("Organization name require minimum 3 characters...");
            $("#orgSubmitBtn").attr("disabled", "disabled");
        }
    }
</script>

