<%--
    Document   : language-list
    Created on : Apr 15, 2016, 2:45:20 PM
    Author     : Suman Behara
    Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin ">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <tags:message code="res.language.register"/>
                    <a href="${context}/resource/language/create" class="btn btn-success btn-sm"><tags:message code="res.create.langauge"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search Language">
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"><tags:message code="label.sno"/></th>
                        <th><tags:message code="res.language.name"/></th>
                        <th width="9%" ><tags:message code="res.language.code"/></th>
                    </tr>
                </thead>
                <tbody id="language-tbl-body"></tbody>
            </table>
        </div>
        <div id="language-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function ()
    {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });
    function deleteResourceType() {
        if (confirm("NVLI \n\n\ Are you sure you want to delete this?")) {
            $("#remove-resourcetype-button").attr("href");
        } else {
            return false;
        }
    }
    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/resource/async/language/list',
            data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                console.log("success");
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.languageList)) {
                        $("#language-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var languageTpl = _.template($("#tpl-language-list").html());

                        var Opts = {
                            "languageList": jsonObject.languageList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        console.log(Opts);
                        var languageTableBody = languageTpl(Opts);
                        $("#language-tbl-body").empty();
                        $("#language-tbl-body").html(languageTableBody);
                        renderPagination(fetchAndRenderTableData, "language-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                console.log("err");
                alertErrorMsg();
            }
        });
    }
    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
<script type="text/template" id="tpl-language-list">
    {{ _.each(languageList, function (language,i){ }}
    <tr><td>{{=i+1+recordCount}}</td>
    <td >
    {{=language.languageName}}
    </td><td>
    {{=language.languageCode}}
    </td>
    {{ }); }}
</script>

