<%-- 
    Document   : widget-manager
    Created on : Mar 31, 2017, 5:56:17 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div class="col-xs-12 col-sm-6 col-md-5 col-lg-7">
    <div class="card">
        <div class="card-header">
            <tags:message code="dashboard.widget.manager.heading"/>
        </div>
        <div class="card-block" style="padding: 0">
            <div id="widget-manager-block">
                <div class="project-list">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><tags:message code="widget.name"/></th>
                                <th><tags:message code="widget.description"/></th>
                                <th><tags:message code="widget.version"/></th>
                                <th><tags:message code="widget.author"/></th>
                                <th><tags:message code="widget.author.email"/></th>
                                <th><tags:message code="widget.addedon"/></th>
                                <th><tags:message code="widget.configure"/></th>
                            </tr>
                        </thead>
                        <tbody id="widget-list-container">

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
    <button class="btn btn-primary btn-lg btn-block" id="btn-show-widget-uploader"><span class="fa fa-upload"></span> <tags:message code="dashboard.widget.uploader.heading"/></button>
    <div class="card" id="widget-uploader-container" style="display: none;">
        <div class="card-header">
            <tags:message code="dashboard.widget.uploader.heading"/>
        </div>
        <div class="card-block">
            <div class="widget-uploader-block">
                <form id="frm-widget-uploader">
                    <div class="form-group">
                        <label for="exampleInputFile"><tags:message code="widget.label.exampleInputFile"/></label>
                        <input type="file" name="uploadfile" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                        <input type="hidden" name="widgetId" value="" id="widgetId">
                        <small id="fileHelp" class="form-text text-muted"><tags:message code="widget.label.fileHelp"/> <a href="#"><tags:message code="widget.label.filehelp.link"/></a>.</small>
                    </div>
                    <button type="submit" class="btn btn-primary"><tags:message code="widget.upload"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-widget-msg-err">
    <tags:message code="widget.upload.error.message"/> 
</script>

<script type="text/template" id="tpl-widget-msg-success">
    <tags:message code="widget.upload.success.message"/> {{=message}}
</script>
<script type="text/template" id="tpl-widget-item">
    <tr>
        <td>{{=count}}</td>
        <td>{{=widgetName}}</td>
        <td>{{=widgetDescription}}</td>
        <td>{{=widgetVersion}}</td>
        <td>{{=widgetAuthorName}}</td>
        <td>{{=widgetAuthorEmail}}</td>
        <td>{{=new Date(createdOn).toLocaleString()}}</td>
        <td align="center"><a href="#" data-tooltip="Configure"><span class="fa fa-gear"></a></td>
    </tr>
</script>