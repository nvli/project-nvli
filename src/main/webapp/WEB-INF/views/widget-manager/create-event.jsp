<%-- 
    Document   : create-event
    Created on : 28 Jun, 2017, 5:33:48 PM
    Author     : Ankita Dhongde <dankita@cdac.in>
--%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-md-8">
            <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
            <div id="alert-box1" class="alert alert-success" style="display: none;"></div>
            <div class="card">
                <div class="card-header row" style="margin: 0;">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><tags:message code="label.calEvent"/>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                        <input id="searchString" class="form-control" placeholder="Search title">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                        <select id="limitFilter" class="form-control">
                            <option value="10" selected>10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                </div>
                <table id="listEventTable" class="table table-bordered table-striped" data-page-size="15">
                    <thead>
                        <tr id="sort-by-tr">
                            <th class="sort-by-th"> <tags:message code="label.serial.no"/></th>
                            <th class="sort-by-th"><i class="fa fa-calendar"></i> <tags:message code="label.title"/></th>
                            <th class="sort-by-th"><i class="fa fa-calendar-plus-o"></i> <tags:message code="label.description"/></th>
                            <th class="sort-by-th"><i class="fa fa-clock-o"></i>  <tags:message code="label.start.date"/></th>
                            <th class="sort-by-th"><i class="fa fa-clock-o"></i> <tags:message code="label.end.date"/></th>
                            <th class="sort-by-th" style="text-align:center;" data-sort-ignore="true" width="100"><i class="fa fa-gear"></i> <tags:message code="user.action"/></th>
                        </tr>
                    </thead>
                    <tbody id="listEventDiv">
                    </tbody>
                </table>
                <div id="event-tbl-footer" class="card-footer clearfix"></div>
            </div>               
        </div>

        <div class="col-md-4">
            <form id="frm-create-event" >
                <div class="card">
                    <div class="card-header"><tags:message code="label.create.event"/></div>
                    <div class="card-block">
                        <div class="form-group row">
                            <label for="title" class="col-sm-3 form-control-label"><strong> <tags:message code="label.title"/></strong></label>
                            <div class="col-sm-9">
                                <input path="header" class="form-control" id="title" placeholder="Event Title"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-3 form-control-label"><strong> <tags:message code="label.description"/></strong></label>
                            <div class="col-sm-9">
                                <input path="description" class="form-control" id="description" placeholder="Event Description"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="eventStartDate" class="col-sm-3 form-control-label"><strong> <tags:message code="label.start.date"/></strong></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control eventStartDate" name="eventStartDate" id="eventStartDate" size="12"  value="" data-datepick="showOtherMonths: true, firstDay: 1, dateFormat: 'yyyy-mm-dd', minDate: 'new Date()'">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="eventEndDate" class="col-sm-3 form-control-label"><strong> <tags:message code="label.end.date"/></strong></label>
                            <div class="col-sm-3">
                                <input type="text"  class="form-control eventEndDate" name="eventEndDate" id="eventEndDate" size="12"  value="" data-datepick="showOtherMonths: true, firstDay: 1, dateFormat: 'yyyy-mm-dd', minDate: 'new Date()'">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <div class="col-xs-offset-3 col-lg-10 col-sm-12">
                            <input type="submit" id="create-event" value="Create Event"  class ="btn btn-primary">
                            <input type="reset" class="btn btn-secondary" value="Reset">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/template" id="list_events_tpl">
    <tr>
        <td>{{=sno}}</td>
        <td id="title-{{=eventId}}">{{=title}}</td>
        <td id="desc-{{=eventId}}">{{=description}}</td>
        <td id="sdate-{{=eventId}}">{{=startDate}}</td>
        <td id="edate-{{=eventId}}">{{=endDate}}</td>   
        <td style="text-align:center; width:120px;">
            <div class="btn-group">
                <button type="button" class="btn-edit-event btn btn-secondary" id="eventEdit-{{=eventId}}" title="Edit" data-event-id="{{=eventId}}" ><span class="fa fa-pencil"></span></button>
                <button style="display:none;" type="button" class="btn-save-event btn btn-secondary" id="saveButton-{{=eventId}}" title="saveButton" data-event-id="{{=eventId}}"><span class="fa fa-save"></span></button>
                <button type="button" class="btn-remove-event btn btn-secondary" id="removeEvent-{{=eventId}}" title="Remove" data-event-id="{{=eventId}}" ><span class="fa fa-remove"></span></button>
            </div>
        </td>
    </tr>
</script>

<script type="text/javascript">
    $(document).ready(function () {
        fetchAndRenderCalenderEventData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderCalenderEventData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderCalenderEventData(1);
        });

//        var onstartSelectDate = function () {
//            var startDate = $("#eventStartDate").val();
//        };
//        $("#eventStartDate").datepick({onSelect: onstartSelectDate});
//
//        var onEndSelectDate = function () {
//            var endDate = $("#eventEndDate").val();
//        };
//        $("#eventEndDate").datepick({onSelect: onEndSelectDate});

        $('#eventStartDate').datepick({
            onSelect: function(dateText, inst){
            $('#eventEndDate').datepick('option', 'minDate', new Date(dateText));
            }
        });

        $('#eventEndDate').datepick({
            onSelect: function(dateText, inst){
                $('#eventStartDate').datepick('option', 'maxDate', new Date(dateText));
            }
        });

        $("#frm-create-event").on("submit", function (e) {
            e.preventDefault();
            saveEvent();
        });

    });

    function fetchAndRenderCalenderEventData(pageNo) {
        $.ajax({
            url: '${context}/widgets/getCalenderEventsByFiltersWithLimit',
            data: {searchString: $('#searchString').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.eventList)) {
                        $("#listEventDiv").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        $("#listEventDiv").empty();
                        _.each(jsonObject.eventList, function (events, i) {
                            var template = _.template($("#list_events_tpl").html());
                            var opts = {
                                sno: (++i),
                                title: events.title,
                                description: events.description,
                                startDate: events.startDate,
                                endDate: events.endDate,
                                eventId: events.id
                            };
                            var el = template(opts);
                            $("#listEventDiv").append(el);


                        });
                        $(".btn-edit-event").unbind().bind("click", editRow);

                        $(".btn-save-event").unbind().bind("click", saveRow);
                        
                        $(".btn-remove-event").unbind().bind("click", deleteEvent);

                       renderPagination(fetchAndRenderCalenderEventData, "event-tbl-footer","page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function deleteEvent(event) {
        var eventId = event.currentTarget.getAttribute('data-event-id');
        if(confirm('Are u sure you want to delete this event?')) {
            $.ajax({
                url: __NVLI.context + '/widgets/deleteCalenderEvent',
                data: {eventId: eventId},
                type: 'POST',
                dataType: 'json',
                success: function (response) {
                    if (response.status === "success") {
                        showToastMessage("Event Deleted ","success");
                        fetchAndRenderCalenderEventData(1);
                    } else {
                        alertErrorMsg();
                    }
                },
                error: function () {
                    alertErrorMsg();
                }
            });
        }
    }

    function editRow(event) {
        var eventId = event.currentTarget.getAttribute('data-event-id');
        document.getElementById("eventEdit-" + eventId).style.display = "none";
        document.getElementById("saveButton-" + eventId).style.display = "block";

        var title = document.getElementById("title-" + eventId);
        var description = document.getElementById("desc-" + eventId);
        var startDate = document.getElementById("sdate-" + eventId);
        var endDate = document.getElementById("edate-" + eventId);

        var title_data = title.innerHTML;
        var description_data = description.innerHTML;
        var startDate_data = startDate.innerHTML;
        var endDate_data = endDate.innerHTML;

        title.innerHTML = "<input type='text' id='input-title-" + eventId + "' value='" + title_data + "' class='form-control'>";
        description.innerHTML = "<input type='text' id='input-description-" + eventId + "' value='" + description_data + "' class='form-control'> ";
//        <input type="text" class="form-control eventStartDate" name="eventStartDate" id="eventStartDate" size="12"  value="" data-datepick="showOtherMonths: true, firstDay: 1, dateFormat: 'yyyy-mm-dd', minDate: 'new Date()'">
        
        startDate.innerHTML = "<input type='text' id='input-startDate-" + eventId + "' value='" + startDate_data + "' class='form-control eventStartDate' data-datepick='showOtherMonths: true, firstDay: 1, dateFormat: 'yyyy-mm-dd', minDate: 'new Date()'' >";
        endDate.innerHTML = "<input type='text' id='input-endDate-" + eventId + "' value='" + endDate_data + "' class='form-control eventEndDate' data-datepick='showOtherMonths: true, firstDay: 1, dateFormat: 'yyyy-mm-dd', minDate: 'new Date()''>";

        $("#input-startDate-" + eventId).ready(function () {
            var onnstartSelectDate = function () {
                var startDate = $("#input-startDate-" + eventId).val();
            };
            $("#input-startDate-" + eventId).datepick({onSelect: onnstartSelectDate,dateFormat: 'yyyy-mm-dd'});

        });
        $("#input-endDate-" + eventId).ready(function () {
            var onnEndSelectDate = function () {
                var endDate = $("#input-endDate-" + eventId).val();
            };
            $("#input-endDate-" + eventId).datepick({onSelect: onnEndSelectDate, dateFormat: 'yyyy-mm-dd' });
        });
        
    }

    function saveRow(event) {
        var eventId = event.currentTarget.getAttribute('data-event-id');
        var title = document.getElementById("input-title-" + eventId);
        var desc = document.getElementById("input-description-" + eventId);
        var startDate = document.getElementById("input-startDate-" + eventId);
        var endDate = document.getElementById("input-endDate-" + eventId);
        var title_data = $(title).val();
        var desc_data = $(desc).val();
        
        var start_date_data = $(startDate).val();
        var end_date_data = $(endDate).val();
        
        $.ajax({
            url: __NVLI.context + '/widgets/save/cal/event',
            data: {eventId: eventId, title: title_data, description: desc_data, startDate: start_date_data, endDate: end_date_data},
            type: 'post',
            dataType: 'JSON',
            success: function (response) {
                if (response.status === "success") {
                    document.getElementById("title-" + eventId).innerHTML=response.data.title;
                    document.getElementById("desc-" + eventId).innerHTML=response.data.description;
                    document.getElementById("sdate-" + eventId).innerHTML=response.data.startDate;
                    document.getElementById("edate-" + eventId).innerHTML=response.data.endDate;
                    
                    document.getElementById("eventEdit-" + eventId).style.display = "block";
                    document.getElementById("saveButton-" + eventId).style.display = "none";
                    showToastMessage("Event updated ","success");
                    
                
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }

    function saveEvent() {
        var title = document.getElementById("title").value;
        var description = document.getElementById("description").value;
        var startDate = document.getElementById("eventStartDate").value;
        var endDate = document.getElementById("eventEndDate").value;

        $.ajax({
            url: __NVLI.context + "/widgets/addNewEvent",
            type: 'POST',
            data: {title: title, description: description, startDate: startDate, endDate: endDate},
            enctype: 'json',
            success: function (response) {
                if (response.status === "success") {
                    showToastMessage("Event Added", "success");
                    $("#frm-create-event")[0].reset();
                    fetchAndRenderCalenderEventData(1);
                } else {
                    showToastMessage("Error. " + response.message, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("Error ", "danger");
            }
        });
    }

</script>
