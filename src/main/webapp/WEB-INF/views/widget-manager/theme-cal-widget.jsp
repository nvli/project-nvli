<%-- 
    Document   : theme-cal-widget
    Created on : 7 Jun, 2017, 11:30:49 AM
    Author     : Ankita Dhongde <dankita@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<link rel='stylesheet' href='${context}/components/fullcalendar/dist/fullcalendar.css' />

<style>
    <c:forEach items="${globalThemes}" var="theme">
        <c:set var="tCode" value="${theme.themeCode}"/>
        .widget-theme-thumb-${tCode} {
            height:150px;
            color: #000;
            font-size:14px;
            line-height:14px;
            background : url('${context}/uploadedThemes/${tCode}_theme/images/${tCode}_theme_thumbnail.png');
            background-size: 150px 150px;
            background-repeat: no-repeat;
            border:none;
        }
        .widget-theme-thumb-${tCode}:hover{
            color: #444;
        }
        
        
    </c:forEach>
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header">
            <tags:message code="dashboard.widget.theme.heading"/> 
            <div class="pull-right">
                    <a class="btn btn-sm btn-blue" href="${context}/admin/manage/themes"><span class="fa fa-gear"></span> <tags:message code="theme.manage"/></a>
                    <a class="btn btn-sm btn-blue" href="${context}/widgets/createCalenderEvent"><span class="fa fa-cog"></span> <tags:message code="label.create.event"/></a>
                </div>
        </div>
        <div class="card-block" style="padding: 0">
            <div id="cal-widget-block" class="container" style="padding: 20px">
                <br>
                <h5><tags:message code="dashboard.widget.theme.desc"/></h5>
                <br>
                <div id="cal-widget"></div>
            </div>

        </div>
    </div>
</div>

<!-- Theme Modal -->
<div class="modal fade" id="uploadThemeBlock" tabindex="-1" role="dialog" aria-labelledby="installthemeFileLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-upload"></i> <tags:message code="theme.add"/>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="frm-upload-theme">
                <div class="modal-body">
                    <div class="container">
                        <div  style="padding: 50px auto;">
                            <div>
                                <label for="" class="control-label"><strong><tags:message code="theme.selected.date"/> :</strong></label>
                                <input type="text" style="border:none;background-color:transparent" value="" id="selected-date" disabled="true"/>
                            </div>
                            <div class="form-group">
                                <label for="" class="control-label"><strong></strong></label>
                                <input id="uploadfile" type="file" name="addfile" class="form-control" accept="application/x-zip-compressed" required="">
                                <p class="help-block text-muted">
                                    <tags:message code="theme.upload"/>
                                </p>
                            </div>
                            <output id="output"></output>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="button.close"/></button>
                    <input type="submit" value="Add Theme" id="btn-theme-add" class ="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>         
                    
<!-- Delete Modal -->
<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> Delete Theme </h4>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete this theme?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><tags:message code="button.cancel"/></button>
                <button type="button" class="btn btn-danger" id="confirm"> <i class="fa fa-remove"></i> <tags:message code="button.delete"/></button>
            </div>
        </div>
    </div>
</div>
                    
<script src='${context}/components/jquery/dist/jquery.min.js'></script>
<script src='${context}/components/moment/min/moment.min.js'></script>
<script src="${context}/components/fullcalendar/dist/fullcalendar.js"></script>
<script>
    
    var json1;
    
//    function getGlobalThemes() {
//        $.ajax({
//            url: __NVLI.context + "/widgets/get/global-themes",
//            dataType: 'json',
//            type: 'GET',
//            success: function (data, textStatus, jqXHR) {
//                json1 = _.clone(data);
//                 console.log("dhfdhjs",json1);
//                activateCalWidget(data);
//            }
//        });
//    }
    
    function getCalenderEventList() {
        $.ajax({
            url: __NVLI.context + "/widgets/get/calenderEventList",
            dataType: 'json',
            type: 'GET',
            success: function (data, textStatus, jqXHR) {
                json1 = _.clone(data);
                 console.log("dhfdhjs",json1);
                activateCalWidget(data);
            }
        });
    }
    
    function activateCalWidget(data) {
        $('#cal-widget').fullCalendar({
            // put your options and callbacks here
            events: data,
            dayClick: function (date, jsEvent) {
                var dd = date.format();
                console.log('day', date.format()); // date is a moment
                console.log('coords', jsEvent.pageX, jsEvent.pageY);
                $('#uploadThemeBlock').modal('show');
                $('#uploadThemeBlock').find('.modal-footer #btn-theme-add')[0].setAttribute('data-theme-date',dd);
                $('#uploadThemeBlock').find('.modal-body #selected-date').val(dd);
                $("#frm-upload-theme").bind("submit", function (e) {
                    e.preventDefault();
                    var themeDate = $('#uploadThemeBlock').find('.modal-footer #btn-theme-add')[0].getAttribute('data-theme-date');
                    // TODO : show upload progress bar.
                    var formData = new FormData($("#frm-upload-theme")[0]);
                     //console.log(themeDate);
                    addTheme(formData,themeDate);
                });
            },
            eventMouseover: function(e){
                console.log("Hovering", e);
                var themeId = e.id;
                var layer="<div id='events-layer' style='text-align:right; position:absoulte;top:0px;'><button class='delete-theme btn btn-danger-outline btn-sm' data-theme-id='"+themeId+"' ><span class ='fa fa-close'></span></button></div>";
                $(this).append($(layer));
                $(".delete-theme").bind("click", function (event){
                    var themeId = event.currentTarget.getAttribute('data-theme-id');
                    console.log("THEME ID ", themeId);
                    $('#confirmDelete').modal('show');
                    $('#confirmDelete').find('.modal-footer #confirm')[0].setAttribute('data-theme-id', themeId);
                    $("#confirmDelete").find('.modal-footer #confirm').on('click', function (event) {
                        var themeId = event.currentTarget.getAttribute('data-theme-id');
                        $.ajax({
                            url: __NVLI.context + "/admin/deleteTheme",
                            type: 'POST',
                            data: {themeId: themeId},
                            dataType: 'json',
                            success: function (response) {
                                if (response.statusCode) {
                                    $('#confirmDelete').modal('hide');
                                    location.reload(true);
                                    showToastMessage("Theme Deleted Sucessfully", "success");
                                    fetchAllThemes();
                                    location.reload();
                                } else {
                                    showToastMessage("something went wrong", "warning");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                showToastMessage("Error while deleteing theme", "danger");
                            }
                        });
                    });
                });
            },
            eventMouseout: function (){
                $('.fc-day-grid-event').find('div[id*=events-layer]').remove();
            }
        });
    }

    function addTheme(formData,themeDate) {
        $.ajax({
            url: __NVLI.context + "/admin/themeFileAdd?themeDate=" + themeDate,
            type: 'POST',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                if (undefined !== response.status && response.status === 'Failed') {
                    showToastMessage("Theme could not be uploaded. " + response.message, "error");
                    $("#uploadThemeBlock").modal('hide');
                } else {
                    
                    $("#uploadThemeBlock").modal('hide');
                    location.reload(true);
                    showToastMessage("Theme Uploaded Successfully", "success");
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("Error while uploading theme", "danger");
            }
        });
    }

    $(document).ready(function () {
        getCalenderEventList();
        // page is now ready, initialize the calendar...
    });
</script>
