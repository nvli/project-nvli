<%-- 
    Document   : e-news
    Created on : 29 Jun, 2017, 10:19:29 AM
    Author     : Darshana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${context}/themes/css/font-awesome.css"/>
<link href="${context}/themes/css/animate.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="${context}/themes/css/rateit.css"/>
<style>
    #newsdesc a {
        color: #2A5FFF;
        margin-right: 5px;
    }
    #newsdesc a:hover {
        color: #F4B47C;
    }
</style>
<script type="text/javascript">
    var _langCode = '${pageContext.response.locale}';
    $(document).ready(function () {
        $('#languages-optn option[value=' + _langCode + ']').attr('selected', 'selected');
        fetchNewsCategories(_langCode);
        $('#languages-optn').on('change', function () {
            _langCode = this.value;
            fetchNewsCategories(_langCode);
        });
    });
    function addAnchorTarget(text) {
        var div = document.createElement('div');
        div.innerHTML = text;
        var anchors = div.getElementsByTagName('a');
        for (var i = 0; i < anchors.length; i++) {
            anchors[i].setAttribute('target', '_blank');
        }
        return $(div).html();
    }

    function newsDescriptionFilter(text, filterType) {
        var div = document.createElement('div');
        div.innerHTML = text;
        var firstImage = div.getElementsByTagName('img')[0];
        if (filterType === "image") {
            return (firstImage !== undefined ? firstImage.src : "");
        } else {
            if (firstImage !== undefined) {
                return $(div).text();
            } else {
                return text;
            }
        }
    }
    function fetchNewsCategories(langCode) {
        $.ajax({
            url: "${context}/nac/news-categories",
            data: {langCode: langCode},
            type: 'POST',
            dataType: 'json'
        })
        .done(function (categoryList) {
            $('#news-categories-ul').empty();
            var news_category_temp = _.template($('#tpl-news-category').html());
            var opts = {
                categoryList: categoryList
            };
            var news_category_el = news_category_temp(opts);
            $('#news-categories-ul').append(news_category_el);
            bindClickEvent();
        })
        .done(function (response) {
            $('#news-container').empty();
            $('#news-container').html("<b>Loading...</b>");
            $("#headerLabel").text(response[0]);
            fetchNewsFeeds(_langCode, response[0]);
        })
        .fail(function (xhr) {
            console.log('error ::', xhr);
        });
    }
    function bindClickEvent() {
        $('.news-category-name').unbind().bind("click", function (e) {
            $('#news-container').empty();
            $('#news-container').html("<b>Loading...</b>");
            $("#headerLabel").text(e.currentTarget.getAttribute("data-name"));
            fetchNewsFeeds(_langCode, e.currentTarget.getAttribute("data-name"));
        });
    }
    function fetchNewsFeeds(languageCode, categoryName) {
        $.ajax({
            url: '${context}/nac/google-news',
            data: {
                languageCode: languageCode,
                categoryName: categoryName
            },
            type: 'POST',
            dataType: 'json'
        })
        .done(function (response) {
            if (!_.isEmpty(response)) {
                $('#news-container').empty();
                _.each(response[0].feedDataList, function (feedDataObj) {
                    var news_feed_template = _.template($('#tpl-news-feed').html());
                    var opts = {
                        link: feedDataObj.link,
                        imageUrl: feedDataObj.imageUrl,
                        title: feedDataObj.title,
                        descriptionAsHTML: feedDataObj.descriptionAsHTML,
                        publishDate: feedDataObj.publishDate,
                        filterRequired: feedDataObj.filterRequired,
                        descriptionAsText: feedDataObj.descriptionAsText
                    };
                    var news_feed_el = news_feed_template(opts);
                    $('#news-container').append(news_feed_el);
                });
                $("table").addClass('table table-responsive');
            }
        })
        .fail(function (xhr) {
            console.log('error ::', xhr);
        });
    }

</script>
<ol class="breadcrumb">
    <li><a href="${context}/home">Home</a></li>
    <li class="active">News</li>
</ol>

<div id="main" class="m-l-3 m-r-3 m-t-3" style="z-index: 1;">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
            <div class="card clearfix bg-white">
                <div class="card-header"><tags:message code="label.news.categories"/></div>
                <div class="news_ul card-block ullilinks">
                    <ul id="news-categories-ul"></ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
            <div class="clearfix card card bg-greyf6 thin_brdcr">
                <div class="card-header clearfix">
                    <i class="fa fa-rss" aria-hidden="true"></i> <tags:message code="label.latest.newsfeed"/> :: <span id="headerLabel"></span>
                    <div class="pull-right">
                        <select class="form-control form-control-sm languages-optn" id="languages-optn">
                            <option value='en'>English</option>
                            <option value='hi'>Hindi</option>
                            <option value='bn'>Bengali</option>
                            <option value='te'>Telugu</option>
                            <option value='mr'>Marathi</option>
                            <option value='ta'>Tamil</option>
                            <option value='gu'>Gujarati</option>
                            <option value='kn'>Kannada</option>
                            <option value='ml'>Malayalam</option>
                            <option value='pa'>Punjabi</option>
                        </select>
                    </div>
                </div>
                <div class="card-block bg-white clearfix">                        
                    <div id="news-container"> 
                    </div>
                </div>
                <!-- end of card block-->
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-news-feed">    
    <article class="post">
    {{ if(filterRequired) { var imageSrc=newsDescriptionFilter(descriptionAsHTML,'image');}}

    <div class="bg-thumb">  
    <div class="input-group col-lg-1 col-md-3 col-sm-12 input-group-lg">
    <a href="{{=link}}" target="_blank"><img src="{{=imageSrc}}" onerror="this.src='${context}/themes/images/img.jpg'"></a>
    </div>
    </div>  

    {{ } }}    

    {{ if(filterRequired) { }}
    <div class="col-lg-11 col-md-11 col-sm-12  post-content">
    <h3 class="card-header1 clearfix">
    <a href="{{=link}}" class="card-link pull-left" target="_blank">{{=title}}</a>
    </h3>    
    <div id="newsdesc" class="p-t-1 p-b-1 p-l-1" >
    {{=addAnchorTarget(descriptionAsText)}}
    </div>
    {{ } else{ }}
    <div class="col-lg-12 col-md-12 col-sm-12  post-content">
    <h3 class="card-header1 clearfix">
    <a href="{{=link}}" class="card-link pull-left" target="_blank">{{=title}}</a>
    </h3>
    <div id="newsdesc" class="p-t-1 p-b-1 p-l-1" >
    {{=addAnchorTarget(descriptionAsHTML)}}
    </div>  
    {{ } }}    
    <div class="clearfix m-t-0 postfooter">
    <ul class="nav nav-pills  font85" style="margin-top: 0px;">                    
    <li class="nav-item "> <span class="m-r-1"><i class="fa fa-clock-o"></i> Last updated on {{=publishDate}}</span></li>
    </ul>
    </div>
    </div>
    <div class="clear"></div>
    </article>
</script>
<script type="text/template" id="tpl-news-category">
    {{ _.each(categoryList, function (category) { }}
        <li class='news-category-name' data-name='{{=category}}'> 
            <a href="#">
                <i class="fa fa-link"></i>
                {{=category}}
            </a>
        </li>
    {{ }); }}
</script>


