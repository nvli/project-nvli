<%-- 
    Document   : 500
    Created on : May 27, 2016, 4:15:59 PM
    Author     : Madhuri
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="wrapper">
    <!-- Search Block -->
    <div class="row">
        <div class="col-xs-12">
            <div class="well text-center">
                <h2><spring:message code="label.500"/></h2>
                <p class="text-info"><spring:message code="error.500" /></p>
            </div>
        </div>
    </div>
</div>