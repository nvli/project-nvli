<%-- 
    Document   : 404
    Created on : May 2, 2016, 11:32:58 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="wrapper">
  <!-- Search Block -->
  <div class="row">
    <div class="col-xs-12">
      <div class="well text-center">
          <h2 class="text-danger"><spring:message code="label.access.denied" /></h2>
          <p><spring:message code="error.403" /></p>
      </div>
    </div>
  </div>
</div>