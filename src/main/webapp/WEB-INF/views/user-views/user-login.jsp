<%--
    Document   : user-login
    Created on : Apr 1, 2016, 1:44:19 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    .login_box{
        border: 1px solid #dcdada;
        box-shadow: 0px 5px 15px 3px #dcdcdc;
        max-width: 500px;
        width: 100%;
    }
    .login-container-block{
        padding-left: 40px;
        padding-right: 40px;
    }
    .login-card-header{
        padding: .75rem 1.25rem;
    }
</style>
<div class="container-fliud" id="main" style="z-index: 1;">
    <div class="clear"></div>
    <div class="col-lg-12">
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div class="login_box">
            <div class="card-header login-card-header"><tags:message code="sign.in.here"/>
                <span class="pull-right">
                    <a href="${context}/auth/registration" class="link3 font12"><tags:message code="login.register"/></a>
                </span>
                <div class="clearfix"></div>
            </div>
            <div class="card-block login-container-block">
                <c:if test="${not empty failureMessage}">
                    <div class="alert alert-warning"><span class="fa fa-warning"></span> ${failureMessage}</div>
                </c:if>
                <div>
                    <img src="${context}/themes/images/topimg.png" style="width:100%; display: block;margin: 0 auto; "/>
                </div>
                <div class="text-justify" style="margin-bottom: 10px;color: #752D01;margin-top: 10px;">
                    <p>The NVLI Portal is currently restricted to invited users only. Users having <strong>security code</strong> can register in the portal. The NVLI Portal will be launched for public soon.</p>
                </div>
                <form role="form" id="frmLogin" action="<c:url value='/j_spring_security_check' />"  method="post">
                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="" for="inputtitle"><tags:message code="label.your.email"/></label>
                            <input name="j_username" type="text" class="form-control" placeholder="" required="" autofocus="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label class="" for="inputPassword"><tags:message code="label.password"/></label>
                            <input name="j_password" type="password" class="form-control" placeholder="" required="">
                        </div>
                    </div>
                    <c:if test="${captchaBlock == true}">
                        <div class="form-group row" id="captacha-block">
                            <div>
                                <label for="inputCaptcha" class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><span class="required-field"></span><tags:message code="label.captcha"/></label>
                            </div>

                            <div class="col-lg-8 col-md-12 col-sm-12 captcha-input50">
                                <img src="${context}/auth/generateCaptchImage" id="captcha" border="0" class="captcha" width="130"/>
                                <button title="Refresh Captcha" class="ab-refresh-captcha  btn btn-default" type="button" style="  padding: 4px;  border: none;">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                </button>
                                <hr>
                                <input type="text" class="form-control pull-right" name="captchatxt" id="inputCaptcha" placeholder="Enter the text shown below" maxlength="6" required maxlength="6">
                                <p class="help-block" id="captchaHelp"></p>
                            </div>
                        </div>
                    </c:if>
                    <div class="form-group row">
                        <div class="checkbox col-lg-12 col-md-12 col-sm-12">
                            <label class="checkbox">
                                <input name="remember-me" type="checkbox"> <tags:message code="login.remember"/>
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <!--                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right" for="inputPassword"></label>-->
                        <div class="text-center">
                            <hr style="margin-top: 0;"/>
                            <input type="hidden" name="url_prior_login" id="url_prior_login">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                            <input type="submit" class="btn btn-primary" value='<tags:message code="label.login"/>'>
                            <a class="btn btn-secondary" href="${context}/auth/forgot-password"><tags:message code="login.forgot.password"/></a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="card-block social-buttons row" align="center">
                    <div class="btn-group" style="">
                        <div style="margin-bottom: 15px;"><tags:message code="login.social.login.desc"/></div>
                        <form id="social_facebook" action="${context}/signin/facebook" method="POST" novalidate="novalidate" class="">
                            <button type="submit" data-tooltip="Sign in with Facebook" class="btn btn-lg btn-block btn-facebook social-button facebook"  id="facebook-button"> <span class="fa fa-facebook"></span></button>
                            <input type="hidden" name="scope" value="public_profile,email"/>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                        <form id="social_linkedin" action="${context}/signin/linkedin" method="POST" novalidate="novalidate" class="">
                            <button type="submit" data-tooltip="Sign in with LinkedIn" class="btn btn-lg btn-block btn-linkedin social-button linkedin"  id="linkedin-button"> <span class="fa fa-linkedin"></span></button>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                        <form id="social_google" action="${context}/signin/google" method="POST" novalidate="novalidate" class="">
                            <button type="submit" data-tooltip="Sign in with Google" class="btn btn-lg btn-block btn-google social-button google"  id="google-button"> <span class="fa fa-google"></span></button>
                            <input type="hidden" name="scope" value="https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"/>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
<!--                            <form id="social_twitter" action="${context}/signin/twitter" method="POST" novalidate="novalidate" class="pull-left margin10">
                            <button type="submit" data-tooltip="Sign in with Twitter" class="btn btn-lg btn-block btn-twitter social-button twitter"  id="twitter-button"> <span class="fa fa-twitter"></span></button>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>-->
                        <form id="social_github" action="${context}/signin/github" method="POST" novalidate="novalidate" class="">
                            <button type="submit"  data-tooltip="Sign in with GitHub" class="btn btn-lg btn-block btn-github social-button github"  id="github-button"> <span class="fa fa-github"></span></button>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var __NVLI = __NVLI || {};
    __NVLI['captcha_enabled_login'] = ${captchaBlock};
    if (!${login_attempt}) {
        localStorage.setItem("url_prior_login", "${url_prior_login}");
        $("#url_prior_login").val(localStorage.getItem("url_prior_login"));
    } else {
        $("#url_prior_login").val(localStorage.getItem("url_prior_login"));
    }
</script>
