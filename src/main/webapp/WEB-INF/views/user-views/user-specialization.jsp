<%-- 
    Document   : interest
    Created on : Apr 15, 2016, 10:04:55 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-wrench"></i><tags:message code="profile.settings.heading"/></div>
        <div class="">
            <div class="tabs" style="margin-top: 0;padding:0 !important;">
                <ul id="myTab" class="nav nav-tabs font-weight-bold">
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/interests" ><tags:message code="profile.setting.interest"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/profession"><tags:message code="profile.setting.profession"/></a> </li>
                    <%--<li class="nav-item"> <a class="nav-link active"><tags:message code="profile.setting.specialization"/></a> </li>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/languages"><tags:message code="profile.setting.language"/></a> </li>
                    <%--<security:authorize access="hasAnyRole('LIB_USER','CURATOR_USER')"><li class="nav-item"> <a class="nav-link" href="${context}/user/resource_specialization"><tags:message code="profile.setting.resource.specialization"/></a> </li></security:authorize>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news"><tags:message code="profile.setting.news.papers"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news-channels"><tags:message code="profile.setting.news.channels"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div id="specialization-tab-content" class="tab active">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="card panel-default">
                                    <div class="card-header clearfix">
                                      <tags:message code="profile.setting.select.specialization"/>
                                        <span class="pull-right font14">
                                            <a href="#" class="label label-default" id="btn-collapse-all"><tags:message code="profile.setting.Specialization.collapse.all"/> </a>
                                            <a href="#" class="label label-default m-l-1" id="btn-expand-all"> <tags:message code="profile.setting.Specialization.expand.all"/> </a>
                                        </span>
                                    </div>
                                    <div class="index_bg p-a-1 clearfix">
                                        <div class="pull-left font14">
                                            <div class="btn-group marginauto" role="group" aria-label="Basic example">
                                                <select class="form-control" id="udc-language" onchange="changeUDCLanguage();">
                                                    <c:forEach items="${languageList}" var="language">
                                                        <c:if test="${language.id eq 40}">
                                                            <option value="${language.id}" selected="true">${language.languageName}</option>
                                                        </c:if>
                                                        <c:if test="${language.id ne 40}">
                                                            <option value="${language.id}">${language.languageName}</option>
                                                        </c:if>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="pull-right font14">
                                            <div class="btn-group marginauto" role="group" aria-label="Basic example">
                                                <c:forEach items="${udcParentMap}" var="udcParent" varStatus="udcCount">
                                                    <c:if test="${udcCount.index eq 0}">
                                                        <button type="button" id="${udcParent.key}" class="btn btn-secondary treeview-pagination-btn active" data-parent-id="${udcParent.key}" data-parent-value="${udcParent.value}">${udcParent.value}</button>
                                                    </c:if>
                                                    <c:if test="${udcCount.index ne 0}">
                                                        <button type="button" id="${udcParent.key}" class="btn btn-secondary treeview-pagination-btn" data-parent-id="${udcParent.key}" data-parent-value="${udcParent.value}">${udcParent.value}</button>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  Treeview content goes here-->

                                    <div class="card-block clearfix">
                                        <div class="clear"></div>
                                        <div>
                                            <div id="udc-treeview-container" class="col-sm-12 col-md-6 col-lg-12"></div>
                                        </div>
                                    </div>
                                    <!--  Treeview content ends here-->
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card">
                                    <div class="card-header"><tags:message code="profile.setting.Specialization.choose"/></div>
                                    <div class="card-block">
                                        <div id="specialization-container"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<script type="text/template" id="tpl-specialization-in">
    <div class="form-group row" id="row-specialization-{{=specializationId}}">
        <label class="control-label col-xs-10">
            <h4>{{=selectedUDCLabel}}</h4>
        </label>
        <div class="col-xs-2">
            <button data-specialization-div-id="{{=specializationId}}"
                    data-specialization-label="{{=selectedUDCLabel}}"
                    type="button"
                    class="btn btn-sm btn-secondary btn-remove-specialization pull-right"
                    title="Delete">
                <span class="fa fa-trash" aria-hidden="true"></span>
            </button>
        </div>
    </div>
</script>
<script>
    var _maxSpecializationAllowed = 3;
    var _treeviewUDCLanguageId = "40";
    var _parentId;
    var _parentValue;
    var _specializationMap={};
    $(document).ready(function () {
        _parentId=$(".treeview-pagination-btn.active").attr("data-parent-id");
        _parentValue=$(".treeview-pagination-btn.active").attr("data-parent-value");
        _treeviewUDCLanguageId=document.getElementById("udc-language").value;

        $(".treeview-pagination-btn").unbind().bind("click", function (e) {
            _parentId=e.currentTarget.getAttribute("data-parent-id");
            _parentValue=e.currentTarget.getAttribute("data-parent-value");
            $(".treeview-pagination-btn.active").removeClass("active");
            $(e.currentTarget).addClass("active");
            getUDCTreeviewData(false);
        });
        getUDCTreeviewData(true);
    });

    function changeUDCLanguage()
    {
        _treeviewUDCLanguageId=document.getElementById("udc-language").value;
        getUDCTreeviewData(true);
    }

    function fetchUserSpecialization()
    {
        $.ajax({
            url: '${context}/user/fetchUserSpecialization/'+_treeviewUDCLanguageId,
            type: 'POST',
            success: function (response)
            {
                _specializationMap={};
                $('#specialization-container').empty();
                _.each(response, function (udcConceptDescription) {
                    var specialization_template = _.template($("#tpl-specialization-in").html());
                    opts = {
                        "selectedUDCLabel": udcConceptDescription.description,
                        "specializationId": udcConceptDescription.udcConceptId.id
                    };
                    var specialization_template_el = specialization_template(opts);

                    $('#specialization-container').append(specialization_template_el);

                    $(".btn-remove-specialization").unbind().bind("click", function (e) {
                        deleteUserSpecialization(e);
                    });

                    _specializationMap[udcConceptDescription.udcConceptId.id]="\\["+udcConceptDescription.udcConceptId.udcNotation+"\\]";
                });

                $.each(_specializationMap, function (key, value){
                    var nodeArray=$('#udc-treeview-container').treeview('search', [ value, {
                        exactMatch: false
                    }]);
                    $('#udc-treeview-container').treeview('clearSearch');
                    if (nodeArray[0] != undefined)
                    {
                        $('#udc-treeview-container').treeview('disableNode', [nodeArray[0].nodeId, {silent: true}]);
                        $('#udc-treeview-container').treeview('checkNode', [nodeArray[0].nodeId, {silent: true}]);
                    }
                });
            },
            error: function ()
            {
                alert("Service not available,Please Try after some time.");
            }
        });
    }

    function getUDCTreeviewData(isCallFetchUserSpecialization)
    {
        $.ajax({
            url: '${context}/cs/getUDCTreeviewDataWithPagination',
            data: {parentId: _parentId, languageId: _treeviewUDCLanguageId},
            type: 'POST',
            success: function (response)
            {
                $('#udc-treeview-container').empty();
                $('#udc-treeview-container').treeview({
                    data: response,
                    showCheckbox: true,
                    onNodeChecked: function (event,node) {
                        if(_.size(_specializationMap)<_maxSpecializationAllowed)
                        {
                            $.ajax({
                                url: '${context}/user/updateUserSpecialization/add/'+node.href,
                                type: 'POST',
                                success: function ()
                                {
                                    var notation = (node.text.substring(1)).split("]")[0];
                                    _specializationMap[node.href]="\\["+notation+"\\]";
                                    $('#udc-treeview-container').treeview('disableNode', [node.nodeId, {silent: true}]);
                                    $('#udc-treeview-container').treeview('checkNode', [node.nodeId, {silent: true}]);

                                    var specialization_template = _.template($("#tpl-specialization-in").html());
                                    opts = {
                                        "selectedUDCLabel": node.text,
                                        "specializationId": node.href
                                    };
                                    var specialization_template_el = specialization_template(opts);

                                    $('#specialization-container').append(specialization_template_el);

                                    $(".btn-remove-specialization").unbind().bind("click", function (e) {
                                        deleteUserSpecialization(e);
                                    });
                                },
                                error: function ()
                                {
                                    alert("Service not available,Please Try after some time.");
                                }
                            });
                        }
                        else
                        {
                            alert("Maximum 3 specialization allowed.");
                            $('#udc-treeview-container').treeview('uncheckNode', [node.nodeId, {silent: true}]);
                        }
                    }
                });

                if(isCallFetchUserSpecialization)
                {
                    fetchUserSpecialization();
                }
                else
                {
                    $.each(_specializationMap, function (key, value){
                        var nodeArray=$('#udc-treeview-container').treeview('search', [ value, {
                            exactMatch: false
                        }]);
                        $('#udc-treeview-container').treeview('clearSearch');
                        if (nodeArray[0] != undefined)
                        {
                            $('#udc-treeview-container').treeview('disableNode', [nodeArray[0].nodeId, {silent: true}]);
                            $('#udc-treeview-container').treeview('checkNode', [nodeArray[0].nodeId, {silent: true}]);
                        }
                    });
                }

                $('#btn-expand-all').on('click', function (e) {
                    $('#udc-treeview-container').treeview('expandAll');
                });

                $('#btn-collapse-all').on('click', function (e) {
                    $('#udc-treeview-container').treeview('collapseAll');
                });
            },
            error: function ()
            {
            }
        });
    }

    function deleteUserSpecialization(e)
    {
        var spacializationId=e.currentTarget.getAttribute("data-specialization-div-id");
        $.ajax({
            url: '${context}/user/updateUserSpecialization/remove/'+spacializationId,
            type: 'POST',
            success: function ()
            {
                $("#row-specialization-"+spacializationId).remove();
                var notation = (e.currentTarget.getAttribute("data-specialization-label").substring(1)).split("]")[0];
                var specializationLabel="\\["+notation+"\\]";
                var nodeArray=$('#udc-treeview-container').treeview('search', [ specializationLabel, {
                    exactMatch: false
                }]);
                $('#udc-treeview-container').treeview('clearSearch');
                delete _specializationMap[nodeArray[0].href];
                $('#udc-treeview-container').treeview('enableNode', [nodeArray[0].nodeId, {silent: true}]);
                $('#udc-treeview-container').treeview('uncheckNode', [nodeArray[0].nodeId, {silent: true}]);
            },
            error: function ()
            {
                alert("Service not available,Please Try after some time.");
            }
        });
    }
</script>