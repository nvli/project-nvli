<%-- 
    Document   : user-view-news-article-feed
    Created on : May 05, 2017, 10:52:55 AM
    Author     : Ritesh Malviya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request"/>
<script type="text/javascript" src="${context}/themes/js/scrolling.js"></script>
<script src="${context}/themes/js/news-ticker/jquery.easing.min.js"></script>
<script src="${context}/themes/js/news-ticker/jquery.easy-ticker.min.js"></script>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-rss"></i> <tags:message code="nvli.news.article.feeds"/>
                <a class="nav-link pull-right" href="${context}/user/subscribe-news" title="News Article Setting"><i class="fa fa-gears"></i></a>
            </div>
            <div class="card-block">
                <div class="row" id="news-feed-container">
                    
                </div>
            </div>
        </div>
    </div>
</div>
                            
<script type="text/template" id="tpl-news-article-block">
    <div class="col-md-12 col-lg-6 col-xl-4 news-block-container" data-paper-code="{{=paperCode}}">
        <div class="card bg-white m-b-2">
            <div class="card-header">
                <img src="${context}/news-agencies-logo/{{=paperCode}}.png" alt="{{=paperName}}" width="30" height="30"/>
                <b>{{=paperName}}</b>
            </div>
            <div id="news-feed-{{=paperCode}}">
                <div class="enews_loader"><img src="${context}/themes/images/processing.gif"></div>
            </div>
            <div id="news-feed-controls-{{=paperCode}}" class="list-group" style="display:none;">                                    
                <a href="#" class="btnUp enews_up"><span class="fa fa-arrow-up"></span></a>
                <a href="#" class="toggle enews_toggle"><i class="fa fa-play"></i></a>
                <a href="#" class="btnDown enews_down"><span class="fa fa-arrow-down"></span></a>
            </div>
        </div>
    </div>
</script>

<script type="text/template" id="tpl-news-feed">
    <ul class="nav nav-tabs" role="tablist" style="margin-top: 0px;">
        {{ _.each(newsArticleFeedList, function (newsFeed,index) { var href="#"+paperCode+"-"+index; }}
            {{ if(index === 0) { }}
                <li role="presentation" class="active" data-toggle="tab"><a href="{{=href}}" class="tab-anchor" role="tab" data-toggle="tab">{{=newsFeed.category}} </a></li>
            {{ } else { }}
                <li role="presentation" data-toggle="tab"><a href="{{=href}}" role="tab" class="tab-anchor" data-toggle="tab">{{=newsFeed.category}} </a></li>
            {{ } }}
        {{ }); }}
    </ul>
    <div class="tab-content" style="margin-left: -15px; margin-right: -15px; ">
        {{ _.each(newsArticleFeedList, function (newsFeed,index) { }}
            {{ if(index === 0) { }}
                <div role="tabpanel" class="tab-pane active" id="{{=paperCode}}-{{=index}}">
            {{ } else { }}
                <div role="tabpanel" class="tab-pane"  id="{{=paperCode}}-{{=index}}">
            {{ } }}    
                <div class="vticker" style="margin-top: -10px; position: relative; height: 530px; overflow: hidden;">
                    <ul style="width: 100%;margin: 0px; position: absolute; top: 0px;">
                        {{ if(_.isEmpty(newsFeed.feedDataList)) { }}
                            <li>Service not available</li>
                        {{ } }}
                        {{ _.each(newsFeed.feedDataList, function (feedData) { }}
                            <li style="margin: 0px; display: list-item;">
                                <div class="news-block" style="margin-top: -5px;">
                                    <div class="news-wrapper">
                                        <h5 style="text-align: left;"><b><a href="{{=feedData.link}}" target="_blank">{{=feedData.title}}</a></b></h5>
                                    </div>
                                    {{ if(feedData.imageUrl === null) { var imageSrc=newsDescriptionFilter(feedData.descriptionAsHTML,'image'); if(imageSrc !== "") { }}
                                        <div class="news_link"> <a href="{{=feedData.link}}" target="_blank"><img src="{{=imageSrc}}" style="max-width:100%;"></a></div>
                                    {{ } } else { }}   
                                        <div class="news_link"> <a href="{{=feedData.link}}" target="_blank"><img src="{{=feedData.imageUrl}}" style="max-width:100%;"></a></div>
                                    {{ } }}
                                    <div class="news_link" style="text-align: justify; margin-top: 15px;"> {{=newsDescriptionFilter(feedData.descriptionAsHTML,'description')}} </div>
                                    {{ if(feedData.author !== null) { }}
                                         <div class="text-left" style="text-align: justify;" ><b>Author:</b> {{=feedData.author}} </div>
                                    {{ } }}
                                    {{ if(feedData.publishDate !== null) { }}
                                        <div class="text-left"><b>Last Updated:</b> {{=feedData.publishDate}} </div>
                                    {{ } }}
                                    <hr style="border-top: 1px solid #aaa"/>
                                </div>
                            </li>
                        {{ }); }}
                    </ul>
                </div>
            </div>
        {{ }); }}
    </div>
</script>

<script>

    function renderNewsArticleBlock(response) {
        $("#news-feed-container").empty();
        _.each(response, function (newsPaper) {
            var tpl = _.template($("#tpl-news-article-block").html());
            var opts = {
                paperCode: newsPaper.paperCode,
                paperName: newsPaper.paperName
            };
            var elem = tpl(opts);
            $("#news-feed-container").append(elem);
            fetchNewsArticleFeeds(newsPaper.paperCode);
            
            $(".news-block-container").mouseover(function (e) {
                $("#news-feed-controls-"+e.currentTarget.getAttribute("data-paper-code")).show();
            });

            $(".news-block-container").mouseout(function (e) {
                $("#news-feed-controls-"+e.currentTarget.getAttribute("data-paper-code")).hide();
            });
        });
    }
    
    function fetchUserSubscribedNewsArticle(callback) {
        $.ajax({
            url: __NVLI.context + "/nac/fetch-user-subscribed-news-article",
            success: callback,
            error: function (err) {
                console.log(err);
            }
        });
    }
    
    function fetchNewsArticleFeeds(paperCode) {
        $.ajax({
            url: __NVLI.context + "/nac/fetch-news-article-feeds",
            data:{"paperCode": paperCode},
            dataType: 'json',
            contentType: "application/json",
            success: function (response) {
                $("#news-feed-"+paperCode).empty();
                var tpl = _.template($("#tpl-news-feed").html());
                var opts = {
                    paperCode: paperCode,
                    newsArticleFeedList: response
                };
                var elem = tpl(opts);
                $("#news-feed-"+paperCode).append(elem);
                applyVticker("#news-feed-controls-"+paperCode);
                (function () {
                    'use strict';
                    $(activate());
                    function activate() {
                        $('.nav-tabs').scrollingTabs().on('ready.scrtabs', function () {
                            $('.tab-content').show();
                        });
                    }
                }());
                
                $(".tab-anchor").on('click', function(e) {
                    $(e.currentTarget).removeClass("active");
                });
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
    
    function applyVticker(divId){
        $('.vticker').easyTicker({
            direction: 'up',
            easing: 'swing',
            speed: 'slow',
            interval: 2000,
            height: '530px',
            visible: 3,
            mousePause: 1,
            controls: {
                up: divId+' .btnUp',
                down: divId+' .btnDown',
                toggle: divId+' .toggle'
            }
        }).data('easyTicker');
    }
    
    function newsDescriptionFilter(text,filterType){
        var div = document.createElement('div');
        div.innerHTML = text;
        var firstImage = div.getElementsByTagName('img')[0];
        if(filterType==="image") {
            return (firstImage !== undefined?firstImage.src:"");
        } else {
            if(firstImage !== undefined) {
                return $(div).text();
            } else {
                return text;
            }
        }
    }
    
    $(document).ready(function () {
        fetchUserSubscribedNewsArticle(renderNewsArticleBlock);
    });
</script>
