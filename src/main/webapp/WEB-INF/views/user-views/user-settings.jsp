<%--
    Document   : user-settings
    Created on : Apr 1, 2016, 12:55:18 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="col-md-12">
        <div class="card row">
            <div class="card-header">
                <i class="fa fa-gears"></i> <tags:message code="account.setting.heading"/>
            </div>
            <div class="card-block">
                <div class="container">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <div class="" role="tab" id="headingTwo">
                                <div class="panel-title">
                                    <h4 class="">
                                        <a class="h4 collapsed"
                                           role="button" data-toggle="collapse"
                                           data-parent="#accordion" href="#collapseTwo"
                                           aria-expanded="true" aria-controls="collapseTwo">
                                            <tags:message code="account.setting.password"/>
                                        </a>
                                        <button type="button" id="expandTwo" class="pull-right btn btn-sm btn-primary btn-outline collapsed" data-toggle="collapse" data-target="#collapseTwo" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-collapse-down"></span><tags:message code="account.setting.expand"/>
                                        </button>
                                    </h4>
                                    <p><tags:message code="account.setting.update.password"/></p>
                                </div>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <form class="card-inner-block">
                                        <div class="form-group row">
                                            <label class="col-md-4"><tags:message code="account.setting.current.password"/>
                                                <input type="password" id="password" class="form-control form-control-sm" placeholder="">
                                            </label>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4"><tags:message code="account.setting.new.password"/>
                                                <input type="password" id="new-password" class="form-control form-control-sm" placeholder="">
                                            </label>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4"><tags:message code="account.setting.retype.password"/>
                                                <input type="password" id="re-new-password" class="form-control form-control-sm" placeholder="">
                                            </label>
                                        </div>
                                        <button id="passbtn-change-pass" class="btn btn-sm btn-primary"><tags:message code="account.setting.change.password"/></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="panel ">
                            <div class="" role="tab" id="headingThree">
                                <div class="panel-title">
                                    <h4 class="">
                                        <a class="h4 collapsed"
                                           role="button" data-toggle="collapse"
                                           data-parent="#accordion" href="#collapseThree"
                                           aria-expanded="true" aria-controls="collapseThree">
                                            <tags:message code="label.email"/>
                                        </a>
                                        <button type="button" id="expandThree" class="pull-right btn btn-sm btn-primary btn-outline collapsed" data-toggle="collapse" data-target="#collapseThree" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-collapse-down"></span><tags:message code="account.setting.expand"/>
                                        </button>
                                    </h4>
                                            <p>  <tags:message code="account.setting.update.email.address"/></p>
                                </div>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <form class="card-inner-block">
                                        <div class="form-group row">
                                            <label class="col-md-4"><tags:message code="account.setting.current.email"/>
                                                   <input id="current-email" type="email" class="form-control form-control-sm " placeholder="">
                                            </label>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4"><tags:message code="account.setting.new.email"/>
                                                <input id="new-email" type="email" class="form-control form-control-sm" placeholder="">
                                            </label>
                                        </div>
                                        <button id="update-email-btn" class="btn btn-sm btn-primary"><tags:message code="account.setting.update.email"/></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="panel ">
                            <div class="" role="tab" id="headingFour">
                                <div class="panel-title">
                                    <h4 class="">
                                        <a class="h4 collapsed"
                                           role="button" data-toggle="collapse"
                                           data-parent="#accordion" href="#collapseFour"
                                           aria-expanded="true" aria-controls="collapseFour">
                                            <tags:message code="profile.tooltip.contact"/>
                                        </a>
                                        <button type="button" id="expandFour" class="pull-right btn btn-sm btn-primary btn-outline collapsed" data-toggle="collapse" data-target="#collapseFour" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-collapse-down"></span><tags:message code="account.setting.expand"/>
                                        </button>
                                    </h4>
                                    <tags:message code="account.setting.contact.desc"/>
                                </div>
                            </div>
                            <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <form class="card-inner-block">
                                        <strong class="text-help text-muted"></strong>
                                        <div class="form-group row">
                                            <label class="col-md-4">
                                                <input type="text" class="form-control form-control-sm" id="contactNo" value="${user.contact}">
                                            </label>
                                            <input type="submit" id="update-contact-btn" value="Update Contact Number" class="btn btn-sm btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="panel">
                            <div class="" role="tab" id="headingFour">
                                <div class="panel-title">
                                    <h4 class="">
                                        <div class="h4">
                                            <p><tags:message code="account.setting.deactivate"/></p>
                                            <button id="btn-deactivate-user" href="#deactivate-account" class="btn btn-sm btn-danger-outline pull-right"><tags:message code="account.setting.deactivate"/></button>
                                        </div>
                                    </h4>
                                    <tags:message code="account.setting.deactivate.desc"/>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" id="csrf-token"/>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close deactive-account-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel"><tags:message code="label.enter.password"/></h4>

                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="control-label"><tags:message code="label.password"/></label>
                                            <input type="password" class="form-control form-control-sm"
                                                   id="password_deactivate" placeholder="Enter password" required="required"/>
                                        </div>

                                    </div>
                                    <div id="alerts">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-sm deactive-account-close" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary btn-sm" id="btn-password-deactivate">continue</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModalForFeedback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close deactive-account-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="feedbackModalLabel">Are you sure you want to deactivate your account?</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Deactivating your account will disable your Profile and remove your name and photo from most things that you've shared on NVLI.</p>
                                        <br>
                                        <div class="form-group">
                                            <div id="radio-btn-list"></div>
                                        </div>
                                        <div id="other-alerts"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default deactive-account-close" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary" id="btn-submit-feedback">Submit Feedback</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="myModalForDeactivation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close deactive-account-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabelForDeactivation">Your account has been deactivated</h4>

                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <p>To reactivate your account, log in using your old login email address/username and password. You will then be able to use the site as before.
                                                <strong>We hope you come back soon.</strong></p>
                                        </div>
                                    </div>
                                    <div id="alertsForDeactivation">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary pull-right" id="deactive-account">Deactivate and Logout</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--                        <hr>
                                                <div class="panel ">
                                                    <div class="" role="tab" id="headingFive">
                                                        <div class="panel-title">
                                                            <h4 class="">
                                                                <a class="h4"
                                                                   role="button" data-toggle="collapse"
                                                                   data-parent="#accordion" href="#collapseFive"
                                                                   aria-expanded="true" aria-controls="collapseFive">
                        <tags:message code="account.setting.time.zone"/>
                    </a>
                    <button type="button" id="expandFive" class="pull-right btn btn-sm btn-primary btn-outline" data-toggle="collapse" data-target="#collapseFive" data-parent="#accordion">
                        <span class="glyphicon glyphicon-collapse-down"></span><tags:message code="account.setting.expand"/>
                    </button>
                </h4>
                <p> <tags:message code="account.setting.update.time.zone"/></p>
            </div>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseFive">
            <div class="panel-body">
                <form class="card-inner-block">
                    <strong class="text-help text-muted"><tags:message code="account.setting.select.time.zone"/></strong>
                    <div class="form-group row">
                        <label class="col-md-4">
                            <select  class="form-control ">
                                <option><tags:message code="account.setting.select"/></option>
                                <option></option>
                                <option></option>
                                <option></option>
                                <option></option>
                                <option></option>
                            </select>
                        </label>
                        <input type="submit" value="Update Time Zone" class="btn btn-lg btn-primary">
                    </div>
        
                </form>
            </div>
        </div>
        </div>
        <hr>
        <div class="panel ">
        <div class="" role="tab" id="headingSIX">
            <div class="panel-title">
                <h4 class="">
                    <a class="h4"
                       role="button" data-toggle="collapse"
                       data-parent="#accordion" href="#collapseSix"
                       aria-expanded="true" aria-controls="collapseSix">
                        <tags:message code="account.setting.update.subscription"/>
                    </a>
                    <button type="button" id="expandSix" class="pull-right btn btn-sm btn-primary btn-outline" data-toggle="collapse" data-target="#collapseSix" data-parent="#accordion">
                        <span class="glyphicon glyphicon-collapse-down"></span><tags:message code="account.setting.expand"/>
                    </button>
                </h4>
                <p><tags:message code="account.setting.change.subscription"/></p>
            </div>
        </div>
        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseSix">
            <div class="panel-body">
                <form class="card-inner-block">
                    <strong class="text-help text-muted"><tags:message code="account.setting.select.time.zone"/></strong>
                    <div class="form-group row">
                        <label class="col-md-4">
                            <input type="checkbox" checked=""><tags:message code="account.setting.subscription.newslatters"/>
                        </label>
                        <input type="submit" value="Update Subscriptions" class="btn btn-lg btn-primary">
                    </div>
                </form>
            </div>
        </div>
        </div>
        <hr>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/template" id="tpl-suggestion-item">
        <a href="#" class="suggestion" data-suggestion="{{=suggestionStr}}">{{=suggestionStr}}</a>
    </script>
    <script type="text/template" id="tpl-feedback-item">
        <div class="radio">
        <label for="{{=feedbackId}}">
        <input type="radio" name="feedback-radio-btn"  class="radio-feedback {{ if(feedbackStr === 'Other'){ }} Other {{ } }}" id="{{=feedbackId}}" value="{{=feedbackId}}">
        {{=feedbackStr}}
        </label>
        <p class="f-desc alert alert-info" id="f-desc-{{=feedbackId}}">{{=feedbackDesc}}</p>
        </div>
    </script>
    <script>
        function changeCollapseButton() {
            $("#collapseOne").on("hide.bs.collapse", function () {
                $("#expandOne").html('<span class="glyphicon glyphicon-collapse-down"></span> Expand');
            });
            $("#collapseOne").on("show.bs.collapse", function () {
                $("#expandOne").html('<span class="glyphicon glyphicon-collapse-up"></span> Collapse');
            });
            $("#collapseTwo").on("hide.bs.collapse", function () {
                $("#expandTwo").html('<span class="glyphicon glyphicon-collapse-down"></span> Expand');
            });
            $("#collapseTwo").on("show.bs.collapse", function () {
                $("#expandTwo").html('<span class="glyphicon glyphicon-collapse-up"></span> Collapse');
            });
            $("#collapseThree").on("hide.bs.collapse", function () {
                $("#expandThree").html('<span class="glyphicon glyphicon-collapse-down"></span> Expand');
            });
            $("#collapseThree").on("show.bs.collapse", function () {
                $("#expandThree").html('<span class="glyphicon glyphicon-collapse-up"></span> Collapse');
            });
            $("#collapseFour").on("hide.bs.collapse", function () {
                $("#expandFour").html('<span class="glyphicon glyphicon-collapse-down"></span> Expand');
            });
            $("#collapseFour").on("show.bs.collapse", function () {
                $("#expandFour").html('<span class="glyphicon glyphicon-collapse-up"></span> Collapse');
            });
            $("#collapseFive").on("hide.bs.collapse", function () {
                $("#expandFive").html('<span class="glyphicon glyphicon-collapse-down"></span> Expand');
            });
            $("#collapseFive").on("show.bs.collapse", function () {
                $("#expandFive").html('<span class="glyphicon glyphicon-collapse-up"></span> Collapse');
            });
            $("#collapseSix").on("hide.bs.collapse", function () {
                $("#expandSix").html('<span class="glyphicon glyphicon-collapse-down"></span> Expand');
            });
            $("#collapseSix").on("show.bs.collapse", function () {
                $("#expandSix").html('<span class="glyphicon glyphicon-collapse-up"></span> Collapse');
            });
        }
        function logout() {
            window.location.href = __NVLI.context + "/auth/logout";
        }
        function validationForSpecialchar(e) {
            var regex = new RegExp("^[a-zA-Z0-9-]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            e.preventDefault();
            return false;
        }
        function populateFeedback()
        {
            $.ajax({
                url: __NVLI.context + "/api/fetch/feedback",
                dataType: 'json',
                type: 'GET',
                success: function (feedback) {
                    _.each(feedback, function (option) {
                        var tpl = _.template($("#tpl-feedback-item").html());
                        var opts = {
                            feedbackStr: option.feedbacklabel,
                            feedbackId: option.feedbackid,
                            feedbackDesc: option.feedbackdesc
                        }
                        var el = tpl(opts);
                        $("#radio-btn-list").append(el);
                        $(".f-desc").hide();
                        $(".radio-feedback").change(function (e) {
                            if (e.currentTarget.checked === true && !$(this).hasClass('Other')) {
                                $("#other-f-textarea").remove();
                                $("#other-alerts").hide();
                                $(".f-desc").hide();
                                $("#f-desc-" + e.currentTarget.getAttribute('id')).show();
                            }
                        });
                        $(".radio-feedback.Other").change(function (e) {
                            if (e.currentTarget.checked === true) {
                                $("#radio-btn-list").append("<textarea name='other-feedback' id='other-f-textarea' rows='4' class='form-control'></textarea>");
                                $(".f-desc").hide();
                            } else {
                                $("#other-f-textarea").remove();
                            }

                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        }
        $(document).ready(function () {
            changeCollapseButton();
            $('#username').keypress(function (e) {
                validationForSpecialchar(e);  
            });
            $('.deactive-account-close').unbind().bind("click", function () {
                $("#password_deactivate").val('');
                $("#alerts").removeClass("alert");
                $("#alerts").removeClass("alert-danger");
                $("#alerts").empty();
                $("#other-alerts").removeClass("alert");
                $("#other-alerts").removeClass("alert-danger");
                $("#other-alerts").empty();
                $("#radio-btn-list").empty();
            });
            $("#btn-submit-feedback").unbind().bind("click", function (e) {
                console.log($('input[name="feedback-radio-btn"]:checked').val());
                var data = {};
                data["fId"] = $('input[name="feedback-radio-btn"]:checked').val();
                data["fMessage"] = "";
                if ($('input[name="feedback-radio-btn"]:checked').val() === '8')
                {
                    if ($("#other-f-textarea").val().length !== 0)
                        data["fMessage"] = $("#other-f-textarea").val();
                    else
                    {
                        $("#other-alerts").addClass("alert");
                        $("#other-alerts").addClass("alert-danger");
                        $("#other-alerts").empty().append('<strong>Please explain further...</strong>');
                        return;
                    }
                }
                $.post(window.__NVLI.context + "/api/deactivate/account", data, function (response) {
                    if (response.status === "success") {
                        $('#myModalForFeedback').modal('hide');
                        $('#myModalForDeactivation').modal('show');

                    } else if (response.status === 'error')
                    {
                        console.log("not activated");
                    }
                });
            });
            $("#btn-password-deactivate").unbind().bind("click", function (e) {
                var data = {};
                data["password"] = $("#password_deactivate").val();
                if ($("#password_deactivate").val().length !== 0)
                {
                    $.post(window.__NVLI.context + "/auth/check/password", data, function (response) {
                        if (!response)
                        {
                            $("#alerts").addClass("alert");
                            $("#alerts").addClass("alert-danger");
                            $("#alerts").empty().append('<strong>The password that you have entered is incorrect. Please try again.</strong>');
                        } else
                        {
                            $('#myModal').modal('hide');
                            populateFeedback();
                            $('#myModalForFeedback').modal('show');
                        }
                    });
                } else
                {
                    $("#alerts").addClass("alert");
                    $("#alerts").addClass("alert-danger");
                    $("#alerts").empty().append('<strong>Enter your password</strong>');
                }
            });
            $("#deactive-account").unbind().bind("click", function (e) {
                logout();
            });
            // Update Password
            $("#passbtn-change-pass").unbind().bind("click", function (e) {
                e.preventDefault();
                console.log("hye");
                console.log("in change password script");
                var csrf_token_value = $("#csrf-token").val();
                var csrf_token_name = $("#csrf-token").attr("name");
                var data = {};
                data[csrf_token_name] = csrf_token_value;
                data['current_password'] = $("#password").val();
                data['new_password'] = $("#new-password").val();
                data['re_new_password'] = $("#re-new-password").val();
                var currentPassword = $("#password").val();
                var newPassword = $("#new-password").val();
                var reNewPassword = $("#re-new-password").val();
                if ((currentPassword.length !== 0) && (newPassword.length !== 0) && (reNewPassword.length !== 0))
                {
                    $.post(window.__NVLI.context + "/auth/update/password", data, function (response) {
                        console.log(response);
                        if (response.status === "success") {
                            showToastMessage("Password Updated!", "success");
                            logout();
                        } else if (response.status === "access_denied") {
                            showToastMessage("Enter Correct current password!", "error");
                        } else if (response.status === "input_error") {
                            showToastMessage("password not matched", "error");
                        }

                    });
                } else
                {
                    showToastMessage("All fields are mandatory", "error");
                }
            });
            // Update Contact no
            $("#update-contact-btn").unbind().bind("click", function (e) {
                e.preventDefault();
                console.log("in update contact no");
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
                var csrf_token_value = $("#csrf-token").val();
                var csrf_token_name = $("#csrf-token").attr("name");
                var data = {};
                data[csrf_token_name] = csrf_token_value;
                data["contactNo"] = $("#contactNo").val();
                data[csrf_token_name] = csrf_token_value;
                if ($("#contactNo").val().match(phoneno))
                {
                    $.post(window.__NVLI.context + "/user/update/contactNo", data, function (response) {
                        console.log(response.status);
                        if (response.status === "OK") {
                            showToastMessage("contact No Updated!", "success");
                        } else
                            showToastMessage("contact No Not Updated!", "error");
                    });
                } else
                    showToastMessage("Invalid Contact No", "error");
            });
            $("#update-email-btn").unbind().bind("click", function (e) {
                var csrf_token_value = $("#csrf-token").val();
                var csrf_token_name = $("#csrf-token").attr("name");
                var data = {};
                data[csrf_token_name] = csrf_token_value;
                data['current_email'] = $("#current-email").val();
                data['new_email'] = $("#new-email").val();
                $.ajax({
                    url: window.__NVLI.context + "/auth/validate/email",
                    data: {currentEmail: data.current_email},
                    success: function (response) {
                        if (response.status === "success") {
                            $.post(window.__NVLI.context + "/auth/update/email", data, function (response) {
                                console.log(response);
                                if (response.status === "success") {
                                    showToastMessage("Email Updated!", "success");
                                }
                            });
                        } else {
                            console.log("Could Not Change your EMAIL ID : Response = ", response.status);
                        }
                    }
                });
            });
            $("#btn-deactivate-user").unbind().bind("click", function (e) {
                $('#myModal').modal('show');
            });
        });
    </script>
