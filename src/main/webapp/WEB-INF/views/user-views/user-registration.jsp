<%--
    Document   : user-registration
    Created on : Apr 1, 2016, 1:45:08 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
    Author     : Ruturaj Powar <ruturajp@cdac.in>
    Author     : Gulafsha Khan <gulafsha@cdac.in>
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    #link-privacy, #link-terms{
        border-bottom : 1px dotted #003eff;
        color: #888;
    }
    #username-help{
        color: red;
    }
    .suggestion{
        color: blue;
        margin: 0 8px;
    }
    .suggestion:hover{
        border-bottom: 1px solid blue;
    }
    .form-group.required .control-label:after { 
        content:"*";
        color:red;
    }
    .text_red{
        color: red;
    }
    .progress-bar {
        margin-top: 10px;
        color: #333;
        padding: 2px;
        border-radius: 4px;
        /*        border:2px solid #efefef;*/
    } 

    .login_box{
        border: 1px solid #dcdada;
        box-shadow: 0px 5px 15px 3px #dcdcdc;
        max-width: 400px;
        width: 100%;
    }
    .login-container-block{
        padding-left: 40px;
        padding-right: 40px;
    }
    .login-card-header{
        padding: .75rem 1.25rem;
    }
    .login-card-footer{
        border: none;
        padding: 8px 30px;
    }
    .frm-register{
        margin: 0;
    }
    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
        margin-right: 10px;
    }
    .help-text{
        color:#444;
        font-size: 12px;
    }
    #input-secret-key{
        border-color: #f0ad4e;
        /*        background: #ffe1b5;*/
    }
</style>
<div class="container-fluid" id="main">
    <div class="clear"></div><br>
    <div class="col-lg-12">
        <div style="margin-bottom:10px;" class="clearfix"></div>
        <div>
            <c:if test="${not empty msgSecretKey}">
                <div class="alert alert-warning">${msgSecretKey}</div>
            </c:if>
            <div class="login_box" style="max-width: 567px;">
                <form:form  class="m-t form-group required frm-register" role="form" id="user-registration-form" action="${context}/auth/register"  modelAttribute="newUserBean" commandName="newUserBean" onSubmit="JavaScript: process_form_submission()">
                    <div class="card-header login-card-header"><tags:message code="reg.heading"/></div>
                    <div style="padding-left: 30px;padding-right: 30px;padding-top: 20px;">
                        <img src="${context}/themes/images/topimg.png" style="width:83%; display: block;margin: 0 auto; "/>

                        <div class="text-justify" style="margin-bottom: 10px;color: #752D01;margin-top: 10px;">
                            <p>The NVLI Portal is currently restricted to invited users only. Users having <strong>security code</strong> can register in the portal. The NVLI Portal will be launched for public soon.</p>
                        </div>
                    </div>
                    <div class="login-container-block">
                        <div class="form-group row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <c:choose>
                                    <c:when test="${isRedirectedFromProvider==true}">
                                        <div class="alert alert-info col-lg-12 col-md-12 col-sm-12 text-center">Please provide some additional information to complete your registration in NVLI</div>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.firstName"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:input path="firstName" type="text" class="form-control" placeholder="First Name" required="required" autofocus="true" id="firstName" />
                                <p class="help-block text_red error_firstName"><form:errors path="firstName" element="div" cssClass="text-danger"/></p>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.lastName"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:input path="lastName" type="text" class="form-control" placeholder="Last Name" required="required" id="lastName" />
                                <p class="help-block text_red error_lastName"><form:errors path="lastName" element="div" cssClass="text-danger"/></p>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.email"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:input id="userMail" path="email" type="email" class="form-control" placeholder="E-mail" required="required" ></form:input>
                                <p class="help-block text_red error_userMail"><form:errors path="email" element="div" cssClass="text-danger"/></p>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.username"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12 parent-div">
                                <form:input path="username" type="text" class="form-control" placeholder="Choose a username" required="required" id="username" />
                                <p class="help-block text_red error_username"><form:errors path="username" cssStyle="text_red"/></p>
                                <!--                                <div class="help-block form-group row clearfix" id="userSuggestion"></div>-->
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.gender"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:select class="form-control" required="required" path="gender">
                                    <form:option value="male"><tags:message code="label.gender.male"/></form:option>
                                    <form:option value="female"><tags:message code="label.gender.female"/></form:option>
                                    <form:option value="other"><tags:message code="label.gender.other"/></form:option>
                                </form:select>
                                <form:errors path="gender" element="div" cssClass="text-danger"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix" id="pwd-container">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.password"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:input path="password" type="password" id="password" class="form-control" placeholder="Password" required="required"/>
                                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <p class="help-block text_red error_password"><form:errors path="password" element="div" cssClass="text-danger"/></p>
                            </div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.repeat.password"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:input path="matchingPassword" name="re-password" id="matchingPassword" type="password" class="form-control" placeholder="Repeat Password" required="required"/>
                                <span toggle="#matchingPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <p class="help-block text_red error_matchingPassword"><form:errors path="matchingPassword" element="div" cssClass="text-danger"/></p>
                            </div>
                        </div>
                        <%--                  <div class="form-group row clearfix" id="countryLabel">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label"><strong><tags:message code="label.country"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <input type="hidden" value="INDIA" id="user-country">
                                <form:select class="form-control" required="required" path="country" id="countries">
                                </form:select>
                                <form:errors path="country" element="div" cssClass="text-danger"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix" id="stateLabel">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="label.state"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:select class="form-control"  path="state" id="states">
                                </form:select>
                                <form:errors path="state" element="div" cssClass="text-danger"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix" id="districtLabel">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="label.district"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:select class="form-control" path="district" id="district">
                                </form:select>
                                <form:errors path="district" element="div" cssClass="text-danger"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix" id="cityLabel1">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="label.city"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:select class="form-control"  path="city" id="city">
                                </form:select>
                                <form:errors path="city" element="div" cssClass="text-danger"/>
                            </div>
                        </div>
                        <div class="form-group row clearfix" id="cityLabel2">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="label.address"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12">
                                <form:textarea class="form-control"  path="address" id="address"/>
                                <p class="help-block text_red error_address"><form:errors path="address" element="div" cssClass="text-danger"/></p>
                            </div>
                                                </div>--%>   

                        <c:if test="${captchaBlock == true}">
                            <div class="form-group row clearfix">
                                <!--                            <div class="col-lg-offset-4 col-md-offset-4 col-lg-8 col-md-8 col-sm-12">
                                                                <div class="g-recaptcha" data-sitekey="<tags:message code="recaptcha.google.siteKey"/>"></div>
                                                            </div>-->
                                <div>
                                    <label for="inputCaptcha" class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><span class="required-field"></span><tags:message code="label.captcha"/></label>
                                </div>
                                <div class="col-lg-8 col-md-12 col-sm-12 captcha-input50">
                                    <img class="" src="${context}/auth/generateCaptchImage" id="captcha" border="0" class="captcha" width="130"/>
                                    <button title="Refresh Captcha" class="ab-refresh-captcha  btn btn-default" type="button" style="  padding: 4px;  border: none;">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"></label>
                                <div class="col-lg-7 col-md-12 col-sm-12">
                                    <input type="text" class="form-control col-lg-7 col-md-12 col-sm-12" name="captchatxt" id="inputCaptcha" placeholder="Enter the text shown below" maxlength="6" required maxlength="6">
                                    <p id="captchaHelp" class="help-block">Please enter the text shown in image above.</p>
                                    <p class="help-block text_red error_inputCaptcha"><form:errors path="captchatext" cssStyle="text_red"/></p>    
                                </div>
                            </div>
                        </c:if>
                        <div class="form-group row clearfix">
                            <label class="col-xs-12 col-lg-4 col-md-4 form-control-label text-right control-label" for="input-secret-key">
                                <strong>Security Code</strong>

                            </label>
                            <div class="col-xs-12 col-lg-7 col-md-7">
                                <input type="text" class="form-control" placeholder="Security Code" required="required" name="secret-key" id="input-secret-key"/>
                                <p class="help-text help-block">Please enter the security code given to you for registering into NVLI Portal.</p>
                            </div>

                        </div>
                        <div class="form-group row clearfix">
                            <div class="col-xs-12 col-lg-12 col-md-12">
                                <br>
                                <label class="col-sm-12  col-xs-12 text-muted small" style="text-align: center;">
                                    You agree to the NVLI <a id="link-terms" href="${context}/policies/terms-of-use">
                                        Terms of Service</a> and <a href="${context}/policies/privacy" id="link-privacy"><tags:message code="label.privacyPolicy"/></a>
                                    by clicking following <span class="text-color">"Create Account"</span> button below.
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer login-card-footer">
                        <div class="form-group " style="margin: 15px 20px;">
                            <div class="row">
                                <div class="col-sm-12  col-xs-12" style="">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    <input type="hidden" name="inviteIdHash" value="${inviteIdHash}" />
                                    <form:button type="submit" class="btn btn-primary btn-block" id="btn-register-user"><tags:message code="button.create.account"/></form:button>
                                    </div>
                                </div>
                            </div>
        <!--                        <p>Already registered? <a href="${context}/auth/login" class="btn btn-secondary">Sign in</a></p>-->
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div><br>
<script type="text/template" id="tpl-suggestion-item">
    <a href="#" class="suggestion" data-suggestion="{{=suggestionStr}}">{{=suggestionStr}}</a>
</script>
<script>
//    function  populateCity(district) {
//        $("#city").empty();
//        $("#city").append("<option value='' selected>Select</option>");
//        $.ajax({
//            url: __NVLI.context + "/api/fetch/city/" + parseInt(district),
//            dataType: 'json',
//            type: 'GET',
//            success: function (city) {
//                $.each(city, function (i, option) {
//                    var opt = document.createElement('option');
//                    $(opt).text(option.cityname);
//                    $(opt).attr("value", option.cityname);
//                    $(opt).attr("district-id", option.cityid);
//                    $('#city').append(opt);
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log(jqXHR, textStatus, errorThrown);
//            }
//        });
//    }
//    function  populateDistrict(state) {
//        $("#district").empty();
//        $("#city").empty();
//        $("#district").append("<option value='' selected>Select</option>");
//        $.ajax({
//            url: __NVLI.context + "/api/fetch/district/" + parseInt(state),
//            dataType: 'json',
//            type: 'GET',
//            success: function (district) {
//                $.each(district, function (i, option) {
//                    var opt = document.createElement('option');
//                    $(opt).text(option.districtname);
//                    $(opt).attr("value", option.districtname);
//                    $(opt).attr("district-id", option.districtid);
//                    $('#district').append(opt);
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log(jqXHR, textStatus, errorThrown);
//            }
//        });
//    }
//    function  populateStates(country) {
//        $("#states").empty();
//        $("#district").empty();
//        $("#city").empty();
//        $("#states").append("<option value='' selected>Select</option>");
//        $.ajax({
//            url: __NVLI.context + "/api/fetch/states/" + parseInt(country),
//            dataType: 'json',
//            type: 'GET',
//            success: function (states) {
//                $.each(states, function (i, option) {
//                    var opt = document.createElement('option');
//                    $(opt).text(option.statename);
//                    $(opt).attr("value", option.statename);
//                    $(opt).attr("state-id", option.stateid);
//                    $('#states').append(opt);
//                });
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log(jqXHR, textStatus, errorThrown);
//            }
//        });
//    }
//    function  populateCountries() {
//        $("#countries").empty();
//        $("#states").empty();
//        $("#district").empty();
//        $("#city").empty();
//        $.ajax({
//            url: __NVLI.context + "/api/fetch/countries",
//            dataType: 'json',
//            type: 'GET',
//            success: function (countries) {
//                $.each(countries, function (i, option) {
//                    var opt = document.createElement('option');
//                    $(opt).text(option.countryname);
//                    $(opt).attr("value", option.countryname);
//                    $(opt).attr("data-id", option.countryid);
//                    $('#countries').append(opt);
//                });
//                var userCountry = $("#user-country").val();
//                $('#countries').val(userCountry);
//
//                var countryname = $('#countries').val();
//                console.log(countryname);
//                if (countryname !== 'INDIA')
//                {
//                    $("#stateLabel").hide();
//                    $("#districtLabel").hide();
//                    $("#cityLabel1").hide();
//                    $("#cityLabel2").show();
//                } else
//                {
//                    console.log("hye");
//                    $("#stateLabel").show();
//                    $("#districtLabel").show();
//                    $("#cityLabel2").hide();
//                    $("#cityLabel1").show();
//                    var country = document.getElementById('countries');
//                    var id = country.options[country.selectedIndex].getAttribute('data-id');
//                    if (!_.isEmpty(id)) {
//                        populateStates(id);
//                    }
//                }
//            },
//            error: function (jqXHR, textStatus, errorThrown) {
//                console.log(jqXHR, textStatus, errorThrown);
//            }
//        });
//    }
    function fetchUserSuggetsion(data, id) {
        console.log("hetertee");
        data["firstName"] = $("#firstName").val();
        data["lastName"] = $("#lastName").val();
        if (($("#firstName").val().length !== 0) && ($("#lastName").val().length !== 0))
        {
            $.ajax({
                url: __NVLI.context + "/api/username/suggestions/registration",
                data: data,
                dataType: 'JSON',
                type: 'GET',
                contentType: 'application/json',
                success: function (suggestions) {
                    console.log(suggestions);
                    var div = document.createElement("div");
                    div.className = "help-block form-group row clearfix";
                    div.setAttribute("id", "userSuggestion");
                    $(".parent-div").append($(div));
                    $("#userSuggestion").html("Available:");
                    _.each(suggestions, function (value) {
                        console.log("value::", value);
                        var tpl = _.template($("#tpl-suggestion-item").html());
                        var el = tpl({
                            suggestionStr: value
                        });
                        $("#userSuggestion").append(el);
                        $(".suggestion").unbind("click").bind("click", function (e) {
                            e.preventDefault();
                            $("#username").val(e.currentTarget.getAttribute("data-suggestion"));
                            $(".error_" + id).empty();
                            $("#userSuggestion").empty();
                            //$(".parent-div").re($("#userSuggestion"));
                            //$("#userSuggestion").empty();
                        });
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        } else
        {
            $("#username-help").empty().append("<html>Enter Firstname and Lastname!!</html>");
        }
    }
    function validationForSpecialchar(e) {
        var regex = new RegExp("^[a-zA-Z0-9-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    }
    $(document).ready(function () {
//        populateCountries();
//        console.log("jasda" +${captchaBlock});
        $("#username").keypress(function (e) {
            validationForSpecialchar(e);
        });
        $("#username").on("blur", function (e) {
            var data = {};
            data["username"] = e.currentTarget.value;
            //Check ID Availability;
            $.get(window.__NVLI.context + "/api/username/exists", data, function (exist) {
                console.log(exist);
                if (exist === true)
                {
                    $(this).attr("data-has-err", true);
                    //manageFormSubmit();
                    $(".error_" + e.currentTarget.id).text("That Username is taken. Try another!!");
                    fetchUserSuggetsion(data, e.currentTarget.id);
                } else {
                    if (e.currentTarget.value.length >= 3)
                    {
                        $(this).attr("data-has-err", false);
                        //manageFormSubmit();
                        $(".error_" + e.currentTarget.id).text("");
                    } else
                    {
                        $(this).attr("data-has-err", true);
                        //manageFormSubmit();
                        $(".error_" + e.currentTarget.id).text("Username should be minimum of 3 char");
                    }
                    if ($.isNumeric(e.currentTarget.value.toString().charAt(0)))
                    {
                        $(this).attr("data-has-err", true);
                        //manageFormSubmit();
                        $(".error_" + e.currentTarget.id).text("Username should not start with number");
                    }
                }
            });
        });
//        $('#countries').change(function (e) {
//            var country = $('#countries').val();
//            if (country !== 'INDIA')
//            {
//                $("#states").val('');
//                $("#district").val('');
//                $("#city").val('');
//                $("#address").val('');
//                $("#stateLabel").hide();
//                $("#districtLabel").hide();
//                $("#cityLabel1").hide();
//                $("#cityLabel2").show();
//            } else
//            {
//                $("#states").val('');
//                $("#district").val('');
//                $("#city").val('');
//                $("#stateLabel").show();
//                $("#districtLabel").show();
//                $("#cityLabel2").hide();
//                $("#cityLabel1").show();
//                populateStates(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("data-id"));
//            }
//        });
//        $('#states').change(function (e) {
//            populateDistrict(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("state-id"));
//        });
//        $('#district').change(function (e) {
//            populateCity(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("district-id"));
//        });
        $(".toggle-password").click(function () {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    });
</script>
<script>
    var __NVLI = __NVLI || {};
    __NVLI['captcha_enabled_registration'] = ${captchaBlock};
</script>
<script src="${context}/scripts/ps.js"></script>

