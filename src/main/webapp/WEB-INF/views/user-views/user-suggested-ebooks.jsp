<%--
    Document   : Suggested e-Books
    Created on : Jun 6, 2016, 11:24:03 AM
    Author     : vootla
    Author     : Ruturaj Powar<ruturajp@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="in.gov.nvli.util.Constants"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" style="display: none;"></div>
    <div class="card">
        <div class="card-header"><i class="fa fa-book"></i>  <tags:message code="dashboard.freeebooks.heading"/></div>
        <div class="card-block">
            <div id="dashboard-fav-libs">
                <div class="project-list">
                    <table class="table table-hover">
                        <tbody id="container_div">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<script type="text/template" id="tpl">
    <tr>
    <td class="lib-title">
    <i class="fa fa-book"></i>
    <strong><a href="${context}/{{=link}}"   style="color: #2a5cea;"> {{=title}}</a></strong><br>
    </td>
    <td class="lib-actions text-right">
    <a id="{{=identifier}}" data-tooltip = "Save" style="color: #2a5cea;" title="{{=title}}" onClick="saveEbook(this);" href="#"><i class="fa fa-floppy-o"></i></a>
    </td>
    </tr>
</script>

<script type="text/javascript">
    var isCacheCleared;
    function saveEbook(tag) {
        $.ajax({
            url: __NVLI.context + "/user/ebooks/" + tag.id + "/" + tag.title,
            type: 'POST',
            success: function (message) {
                if (message === "success")
                {
                    $("#alert-box").html('<div class="alert alert-success">Saved Successfully,Check on your eBooks</div>').fadeIn();
                    $("#alert-box").fadeOut(3000);
                    if ($('#container_div').children().length === 0)
                    {
                        $("#container_div").append("<center>No Suggestion's Found ! , To Get Suggestions  <a href='" + __NVLI.context + "/user/interests' style='color: #2a5cea;'>Choose</a> Your Interest's </center>");
                    }
                    $("#container_div").empty();
                    populate();
                } else
                {
                    $("#alert-box").html('<div class="alert alert-danger">Problem While saving, Please try again!</div>').fadeIn();
                    $("#alert-box").fadeOut(3000);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error");
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function populate() {
        $.ajax({
            url: __NVLI.context + "/user/suggestedebooks/results",
            dataType: 'json',
            type: 'GET',
            success: function (suggestions) {
                var Count = 0;
                _.each(suggestions, function (suggestion) {
                    var template = _.template($("#tpl").html());
                    Count++;
                    var opts = {
                        "link": suggestion.link,
                        "title": suggestion.title,
                        "identifier": suggestion.identifier
                    };
                    var el = template(opts);
                    $("#container_div").append(el);
                });

                if (Count === 0)
                {
                    $("#container_div").empty();
                    $("#container_div").append("<center>No Suggestion's Found ! , To Get Suggestions  <a href='" + __NVLI.context + "/user/interests' style='color: #2a5cea;'>Choose</a> Your Interest's </center>");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
//    function clearCache(){
//            if(!isCacheCleared){
//            $.ajax({
//                url: __NVLI.context + "/user/delete-redis-cache/" +'<%=Constants.FilterActivityLogs.EbookActivities%> ',
//                type: 'POST',
//                success: function (message) {
//                    console.log("cache cleared..");
//                    isCacheCleared=true;
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    console.error(textStatus, errorThrown, jqXHR);
//                }
//            });
//        }
//    }
    $(document).ready(function () {
        isCacheCleared = false;
        populate();
    });
</script>