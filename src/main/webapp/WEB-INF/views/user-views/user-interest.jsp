<%--
    Document   : interest
    Created on : Apr 15, 2016, 10:04:55 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    #interest-tab-content .tab-content>.active{
        padding-right: 0px;
        padding-left: 0px;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-wrench"></i> <tags:message code="profile.settings.heading"/></div>
        <div class="">
            <div class="tabs" style="margin-top: 0;padding:0 !important;">
                <ul id="myTab" class="nav nav-tabs font-weight-bold">
                    <li class="nav-item"> <a class="nav-link active" ><tags:message code="profile.setting.interest"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/profession"><tags:message code="profile.setting.profession"/></a> </li>
                        <%--<li class="nav-item"> <a class="nav-link" href="${context}/user/specialization"><tags:message code="profile.setting.specialization"/></a> </li>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/languages"><tags:message code="profile.setting.language"/></a> </li>
                        <%--<security:authorize access="hasAnyRole('LIB_USER','CURATOR_USER')"><li class="nav-item"> <a class="nav-link" href="${context}/user/resource_specialization"><tags:message code="profile.setting.resource.specialization"/></a></li></security:authorize>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news"><tags:message code="profile.setting.news.papers"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news-channels"><tags:message code="profile.setting.news.channels"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div id="interest-tab-content" class="tab active">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="card table-responsive">
                                    <div class="card-header"><tags:message code="profile.setting.select.interest"/></div>
                                    <div class="index_bg p-a-1 col-lg-12 clearfix">
                                        <div id="alphabet-navs" class="btn-group" role="group"></div>
                                    </div>
                                    <div class="card-block">
                                        <div id="interestTabContent" class="tab-content" data-toggle="buttons"></div>
                                    </div>
                                    <div class="card-footer clearfix"></div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card">
                                    <div class="card-header"><tags:message code="profile.setting.interest.interested"/></div>
                                    <div class="card-block">
                                        <div id="selected-box"></div>
                                    </div>
                                    <div class="card-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/template" id="tpl-interested-in">
    <div class="intersted_in" data-keywords="" id="row-keywords-{{=id}}" data-interest-id="{{=interestId}}">
    <label class="control-label col-xs-8"><h4>{{=value}}</h4></label>
    <div class="col-xs-4">
    <button data-thematic-type-id="{{=id}}"
    type="button"
    class="btn btn-sm btn-secondary btn-remove-interest"
    title="Delete" data-value="{{=value}}"
    data-interest-id="{{=interestId}}"
    data-index="{{=index}}">
    <span class="fa fa-times" aria-hidden="true"></span>
    </button>
    </div>
    <label class="col-xs-8">
    <input placeholder="Enter keywords" data-thematic-type-id="{{=id}}"
    value="" id="keyword-{{=interestId}}"
    type="text"
    data-base-value="{{=value}}" class="form-control interest-keyword">
    </label>
    <div class="col-xs-4">
    <button class="btn btn-secondary btn-add-keywords"
    data-target-input="#keyword-{{=interestId}}"
    data-interest-id="{{=interestId}}"
    data-target="#keywords-block-{{=interestId}}">Add</button>
    </div>
    <div class="col-xs-12 m-t-1 help" id="keywords-block-{{=interestId}}" data-keywords="{{=keywords}}"></div>
    </div>
    <hr>
</script>

<script type="text/template" id="tpl-keyword">
    <span class="alert bg-greye9">{{=keyword}} <a href="#" title="Remove" class="delete-keyword" data-keyword="{{=keyword}}" data-interest-id="{{=id}}"><i class="fa fa-times" ></i></a></span>
</script>

<script type="text/template" id="tpl-tabbed-nav">
    <button href="{{=alphaId}}" class="btn btn-secondary alpha-tab" data-target-id="{{=alphaId}}" data-toggle="tab" aria-expanded="false">{{=alpha}}</button>
</script>

<script type="text/template" id="tpl-tabbed-content">
   <div class="tab-pane fade alpha-content" id="{{=alpha}}">
    <div id="interests-block-{{=alpha}}" class="row interest_tab" data-toggle="buttons" style="padding: 20px 0; overflow: hidden; width: auto; height: 550px;">
    </div>
    </div>
</script>

<script type="text/template" id="tpl-interest-theme">
    <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-xs-12">
    <label class="interest-item btn btn-secondary btn-lg btn-block"
    data-thematic-type-id="{{=thematicTypeId}}"
    data-thematic-type="{{=thematicName}}"
    style="margin:5px 0px 5px 0px; text-align:left !important;">
    {{= thematicName}}
    </label>
    </div>
</script>
<script type="text/javascript">
    var MIN_SELECTED_CATEGORY = 3;
    function toggleNextButton() {
        if ($(".interest-item.checked").length >= MIN_SELECTED_CATEGORY) {
            $("#btn-next-to-profile").removeClass('disabled');
        } else {
            $("#btn-next-to-profile").addClass('disabled');
        }
    }

    function checkSelected() {
        _.each(__NVLI.savedInterests, function (interest) {
            var elems = $("label.interest-item[data-thematic-type-id='" + interest.id + "']");
            _.each(elems, function (el) {
                $(el).addClass("active");

            });
        });
    }
    function isAlphaKeyword(keyword) {
        if (/^[a-zA-Z]\w+$/.test(keyword)) {
            return true;
        }
        return false;
    }
    function saveAndUpdateKeywords(interestId, keywords) {
        $.ajax({
            url: __NVLI.context + "/user/interests/keywords/save/" + interestId + "?keywords=" + keywords,
            dataType: 'json',
            type: 'POST',
            success: function (response) {
                getSavedInterests(renderSelectedBOX);
            },
            error: function (err) {
                console.log(err);
            }

        });
    }

    function renderSelectedBOX(items) {
        __NVLI['savedInterests'] = items;
        $("#selected-box").empty();
        _.each(items, function (item, index) {
            var tpl_selected_fields = _.template($("#tpl-interested-in").html());
            var opts = {
                index: index + 1,
                value: item.thematicType,
                interestId: item.interestId,
                id: item.id,
                keywords: item.keywords
            };
            var el = tpl_selected_fields(opts);
            $("#selected-box").append(el);
            var keywords;
            if (item.keywords !== null) {
                keywords = item.keywords.split(",");
            }
            if (_.isArray(keywords) && _.size(keywords) && keywords[0] !== "") {
                _.each(keywords, function (keyword) {
                    var tplKeyword = _.template($("#tpl-keyword").html());
                    var obj = {
                        keyword: keyword,
                        id: item.interestId
                    };
                    var el_keyword = tplKeyword(obj);
                    $("#keywords-block-" + item.interestId).prepend(el_keyword);
                });
                $(".delete-keyword").unbind().bind("click", function (e) {
                    var keywordToRemove = e.currentTarget.getAttribute("data-keyword");
                    var id = e.currentTarget.getAttribute("data-interest-id");
                    var keywords = $("#keywords-block-" + id).attr("data-keywords").split(",");
                    keywords = _.reject(keywords, function (key) {
                        return key === keywordToRemove;
                    });
                    var str = "";
                    _.each(keywords, function (k, i, keywords) {
                        if (_.size(str) === 0) {
                            str = k;
                        } else {
                            str = str + "," + k;
                        }
                    });
                    saveAndUpdateKeywords(id, str);
                });
            }
        });
        $(".btn-add-keywords").unbind().bind("click", function (e) {
            var interestIdd = e.currentTarget.getAttribute('data-interest-id');
            var keyword = $("#keyword-" + interestIdd).val();
            if (isAlphaKeyword(keyword)) {
                var interestId = e.currentTarget.getAttribute('data-interest-id');
                var targetInput = e.currentTarget.getAttribute('data-target-input');
                var dataTarget = e.currentTarget.getAttribute('data-target');
                var keywords = $(dataTarget).attr("data-keywords");
                if (keywords.length) {
                    keywords = keywords + "," + $(targetInput).val();
                } else {
                    keywords = $(targetInput).val();
                }
                $(dataTarget).attr("data-keywords", keywords);
                saveAndUpdateKeywords(interestId, keywords);
            } else {
                var thematicTypeId = e.currentTarget.getAttribute("data-thematic-type-id");
                $("#keywords-block-" + interestIdd).empty();
                $("#keywords-block-" + interestIdd).text("Only Alpabets Are Allowed..");
            }
        });
        $(".btn-remove-interest").unbind().bind("click", function (e) {
            var selected_el = $(".interest-item.active");
            var item_to_remove = $(".interest-item.active[data-thematic-type='" + e.currentTarget.getAttribute("data-value") + "']");
            var thematicTypeId = e.currentTarget.getAttribute("data-thematic-type-id");
            updateInterests(thematicTypeId, "rm");
            $(item_to_remove).trigger("click");
        });
        checkSelected();
    }

    function bindClickInterest() {
        $(".interest-item").unbind().bind('click', function (e) {
            $(this).toggleClass('active');
            var thematicTypeId = e.currentTarget.getAttribute("data-thematic-type-id");
            var checked = $(this).hasClass('active');
            var action = checked ? "add" : "rm";
            updateInterests(thematicTypeId, action);
        });
    }

    function renderThematicTypesAlphabetically(data) {
        var grouped_data = groupAlphabetically(data);
        $("#alphabet-navs").empty();
        $("#interestTabContent").empty();
        _.each(grouped_data, function (items, alpha) {
            // Populate Navs
            renderNavPills(alpha);
            // Populate Contents
            var content_tpl = _.template($("#tpl-tabbed-content").html());
            var content_el = content_tpl({alpha: alpha});
            $("#interestTabContent").append(content_el);
            _.each(items, function (item) {

                var tpl = _.template($("#tpl-interest-theme").html());
                var el = tpl({
                    thematicTypeId: item.thematicTypeId, thematicName: item.thematicType
                });

                $("#interests-block-" + alpha).append(el);

            });
            applySlimScroll("interests-block-" + alpha, "550px");
            bindClickInterest();
        });
        $("#interestTabContent>div").last().addClass("active in");
        $(".alpha-tab").last().addClass("active");
        $(".alpha-tab").bind("click", function (e) {
            $(".alpha-tab, .alpha-content").removeClass("active in");
            $(this).addClass("active");
            var content = e.currentTarget.getAttribute("data-target-id");
            $(content).addClass("active in");
        });
        checkSelected();
    }

    function renderNavPills(alpha) {
        var nav_tpl = _.template($("#tpl-tabbed-nav").html());
        var opts = {alpha: alpha, alphaId: "#" + alpha};
        var nav_el = nav_tpl(opts);
        $("#alphabet-navs").append(nav_el);
        $("#" + alpha).click(function (e) {
            checkSelected();
        });
    }

    function groupAlphabetically(data) {
        var grouped = _.groupBy(data, function (item) {
            return item.thematicType.substr(0, 1).toUpperCase();
        });
        grouped['ALL'] = data;
        return grouped;
    }

    function applySlimScroll(id, height) {
        $("#" + id).slimScroll({
            height: height,
            size: '5px',
            position: 'right',
            color: '#888',
            alwaysVisible: false,
            distance: '0px',
            railVisible: false,
            railColor: '#efefef',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: false
        });
    }

    function fetchInterest(callback) {
        $.ajax({
            url: window.__NVLI.context + "/api/thematic-types",
            type: 'get',
            dataType: 'json',
            success: callback,
            error: function (xhr) {
                console.error("error while fetching user interest ::", xhr);
            }
        });
    }

    function updateInterests(thematicTypeId, action) {
        $.ajax({
            url: window.__NVLI.context + "/user/interest/" + action + "/" + thematicTypeId,
            type: "post",
            dataType: 'json',
            contentType: "application/json",
            success: function (data) {
                getSavedInterests(renderSelectedBOX);
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    function getSavedInterests(callback) {
        $.ajax({
            url: __NVLI.context + "/api/my/interests",
            type: 'get',
            dataType: 'json',
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function validationForSpecialchar(e) {
        var regex = new RegExp("^[a-zA-Z0-9-]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    }
    $(document).ready(function () {
        // getProfileDetails();
        getSavedInterests(renderSelectedBOX);
        // Fetch Interest DATA & Render
        fetchInterest(renderThematicTypesAlphabetically);
        $("#btn-next-to-profile").unbind().bind("click", function (e) {
            var selectedInterestes = __NVLI['interests'];
            if (!_.isEmpty(selectedInterestes)) {
                saveInterests(selectedInterestes);
            }
        });
        applySlimScroll("selected-box", "638px");
    });

</script>