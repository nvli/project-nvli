<%-- 
    Document   : feedback
    Created on : 12 Feb, 2018, 3:15:47 PM
    Author     : Vivek Bugale <bvivek@cdac.in>
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags"%>

<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />

<script type="text/javascript">
    $(document).ready(function () {
        if(!_.isEmpty("${user}")){
            $("#feedback-name").val(getFullName("${user.firstName}","${user.middleName}","${user.lastName}"));       
            $("#feedback-email").val("${user.email}");
            $("#feedback-mobile").val("${user.contact}");
        }
        bindFeedbackSubmit();
    });
    
    function getFullName(first,middle,last){
        return !_.isEmpty(middle) ? first+" "+middle+" "+last : first+" "+last;       
    }

    function bindFeedbackSubmit() {
        $("#btn-feedback-submit").on("click", function () {
            var msg = checkRequiredFields();
            msg === "true" ? saveFeedback() : showWarning(msg);
        });
    }

    function checkRequiredFields() {
        if (!$("#feedback").val().trim().length) {
            return "Enter feedback";
        } else if($("#feedback").val().trim().length >500){
            return "Only 500 characters allowed in feedback";
        } else if (!$("#feedback-name").val().trim().length) {
            return "Enter name";
        } else if (!$("#feedback-email").val().trim().length) {
            return "Enter email address";
        } else if (!validateEmail($("#feedback-email").val())) {
            return "Enter valid email address";           
        }else if ($("#feedback-mobile").val().trim().length !== 0 && (!(/^\d+$/.test($("#feedback-mobile").val().trim())) || $("#feedback-mobile").val().trim().length!==10)) {
            return "Enter valid mobile number";
        }else{         
            return "true";
        }
    }

    function validateEmail(email) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return filter.test(email);
    }


    function showWarning(msg) {
        $("#alert-msg").removeClass().addClass("alert alert-warning");
        $("#alert-msg").text(msg);
        $("#alert-msg").show();
    }

    function saveFeedback() { 
        var feedbackJson={};        
        feedbackJson.feedback=$("#feedback").val().trim();
        feedbackJson.name=$("#feedback-name").val().trim();
        feedbackJson.email=$("#feedback-email").val().trim();
        feedbackJson.phoneNumber=$("#feedback-mobile").val().trim();
        feedbackJson.designation=$("#feedback-designation").val().trim();
        feedbackJson.organization=$("#feedback-organization").val().trim();        
        $("#alert-msg").hide();
        if(!_.isEmpty(feedbackJson)){
            $.ajax({
                url: __NVLI.context+"/fc/feedback/submit",
                type: 'POST',
                dataType: 'json', 
                data : {
                    feedbackJson :JSON.stringify(feedbackJson),
                    userId : "${user.id}"
                }
            })
            .done(function (response){
                response ? feedbackSucceed(): feedbackFailed();
                clearForm();
            })
            .fail(function (xhr){
                console.error("Error in saveFeedback ::",xhr);
            });
        }
    }
    
    function feedbackSucceed(){
        $("#alert-msg").removeClass().addClass("alert alert-success");
        $("#alert-msg").text("Thank you for your valuable feedback!");
        $("#alert-msg").show();
    }
    
    function feedbackFailed(){
        $("#alert-msg").removeClass().addClass("alert alert-danger");
        $("#alert-msg").text("Oops! Something went wrong. Please try again.");
        $("#alert-msg").show();
    }
    
    function clearForm(){
        $("#feedback").val("");
        $("#feedback-name").val("");       
        $("#feedback-email").val("");
        $("#feedback-mobile").val("");
        $("#feedback-designation").val("");
        $("#feedback-organization").val("");
    }

</script>

<div class="col-lg-12">
    <div style="margin-bottom:20px;" class="clearfix"></div>
    <div>
        <div class="login_box" style="max-width: 567px;">
            <div class="card-header login-card-header">
                <tags:message code="label.feedback.form"/>
            </div>
            <div class="card-block login-container-block">
                <div class="form-group row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12"></div>
                </div>
                <div id="alert-msg" style="display: none;"></div>
                <div class="form-group row clearfix" id="cityLabel2">
                    <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right">
                        <strong>
                            <tags:message code="label.feedback"/>
                            <em style="color: red;">*</em> 
                        </strong>
                        <div class="clear"></div>
                        <img src="${context}/themes/images/feedback.png" alt="feedback" width="70"/>
                    </label>
                    <div class="col-lg-7 col-md-12 col-sm-12">
                        <textarea id="feedback" name="feedback" class="form-control" maxlength="500" rows="4" placeholder="Enter your feedback"></textarea>                        
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label">
                        <strong>
                            <tags:message code="label.user.name"/>
                            <em style="color: red;">*</em> 
                        </strong>
                    </label>
                    <div class="col-lg-7 col-md-12 col-sm-12">
                        <input id="feedback-name" name="feedback-name" placeholder="Enter your name" type="text" class="form-control" required="required"/>                        
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label">
                        <strong>
                            <tags:message code="label.email"/>
                            <em style="color: red;">*</em> 
                        </strong>
                    </label>
                    <div class="col-lg-7 col-md-12 col-sm-12">
                        <input id="feedback-email" name="feedback-email" placeholder="Enter your e-mail" type="email" class="form-control" required="required" value=""/>
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label">
                        <strong>
                            <tags:message code="label.mobile"/>
                        </strong>
                    </label>
                    <div class="col-lg-7 col-md-12 col-sm-12 parent-div">
                        <input id="feedback-mobile" name="feedback-mobile" placeholder="Enter your mobile number" type="text" class="form-control" required="required" value="" maxlength="10"/>                       
                    </div>
                </div>
                <div class="form-group row clearfix" id="pwd-container">
                    <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label">
                        <strong>
                            <tags:message code="label.organization.designation"/>
                        </strong>
                    </label>
                    <div class="col-lg-7 col-md-12 col-sm-12">
                        <input id="feedback-designation" name="feedback-designation" placeholder="Enter your designation" type="text" class="form-control" required="required" value=""/>                       
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right control-label">
                        <strong>
                            <tags:message code="label.organization"/>
                        </strong>
                    </label>
                    <div class="col-lg-7 col-md-12 col-sm-12">
                        <input id="feedback-organization" name="feedback-organization" placeholder="Enter your organization" type="text" class="form-control" required="required" value=""/>                       
                    </div>
                </div>
            </div>
            <div class="card-footer login-card-footer">
                <div class="form-group " style="margin: 15px 20px;">
                    <div class="row">
                        <div class="col-sm-12  col-xs-12">
                            <button id="btn-feedback-submit" type="submit" class="btn btn-primary btn-block" type="submit" value="Submit">
                                <tags:message code="button.submit"/>
                            </button>
                        </div>
                    </div>
                </div>              
            </div>

        </div>
    </div>
</div>
