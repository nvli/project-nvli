<%-- 
    Document   : edit-user-profile
    Created on :May 27, 2016, 3.28 PM
    Author     : Bhumika Gupta
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ page session="false"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-user"></i> Edit User Profile</div>
                <div class="card-block">
                    <div class="row">
                        <form:form  class="m-t" role="form" id="user-edit-form" action="${context}/user/profile/update" commandName="userObj" method="POST" onSubmit="return validateProfile()">
                            <form:hidden path="id" id="userid"/>
                            <form:hidden path="enabled" id="enabledstatus" />

                            <div class="profile-image-container text-xs-center col-md-5">
                                <img src="${exposedProps.userImageBase}/${userObj.profilePicPath}"
                                     width="100%"
                                     class="img-rounded profile-view-img"
                                     alt="${user.firstName} ${user.lastName}"
                                     onerror="this.src='${context}/images/profile-picture.png'"/>
                            </div>
                            <div class="profile-content col-md-7">
                                <div class="col-lg-12 col-md-12 col-sm-12 ">
                                    <div class="table-responsive">
                                        <table class="table text-left">
                                            <tbody>
                                                <tr>
                                                    <th class="text-right" width="200"><tags:message code="edit.name"/></th>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <div class="">  
                                                            <input type="hidden" name="firstName" value="${userObj.firstName}" />
                                                            <input type="hidden" name="middleName" value="${userObj.middleName}" />
                                                            <input type="hidden" name="lastName" value="${userObj.lastName}" />
                                                            <input type="hidden" name="id" value="${userObj.id}" />
                                                            <strong>${userObj.firstName} ${userObj.middleName} ${userObj.lastName}</strong>  
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="text-right" width="200"><tags:message code="edit.email"/></th>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <div class="">    
                                                            <input type="hidden" name="email" value="${userObj.email}" />
                                                            <strong>${userObj.email}</strong>                                                             
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="text-right" width="200"><tags:message code="edit.username"/></th>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <div class="">  
                                                            <input type="hidden" name="username" value="${userObj.username}" />
                                                            <strong>${userObj.username}</strong>                                                             
                                                        </div>
                                                    </td>
                                                </tr>                                                
                                                <c:if test="${(userObj.contact ne NULL )&& (not empty(userObj.contact))}">
                                                    <tr>
                                                        <th class="text-right" width="200"><tags:message code="edit.contact"/></th>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <div class="">  
                                                                <input type="hidden" name="contact" value="${userObj.contact}" />
                                                                <strong>${userObj.contact}</strong>                                                             
                                                            </div>
                                                        </td>
                                                    </tr>  
                                                </c:if>
                                                <tr>
                                                    <th class="text-right" width="200"><tags:message code="edit.gender"/></th>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <div class="">  
                                                            <input type="hidden" name="gender" value="${userObj.gender}" />
                                                            <strong>${userObj.gender}</strong>                                                             
                                                        </div>
                                                    </td>
                                                </tr>  
                                                <tr>
                                                    <th class="text-right" width="200"><tags:message code="edit.country"/></th>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <div class="">  
                                                            <input type="hidden" name="country" value="${userObj.country}" />
                                                            <strong>${userObj.country}</strong>                                                             
                                                        </div>
                                                    </td>
                                                </tr>  
                                                <c:if test="${userObj.state ne NULL && (not empty(userObj.state))}">
                                                    <tr>
                                                        <th class="text-right" width="200"><tags:message code="edit.state"/></th>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <div class="">    
                                                                <input type="hidden" name="state" value="${userObj.state}" />
                                                                <strong>${userObj.state}</strong>                                                             
                                                            </div>
                                                        </td>
                                                    </tr>  
                                                </c:if>
                                                <c:if test="${userObj.district ne NULL && (not empty(userObj.district))}">
                                                    <tr>
                                                        <th class="text-right" width="200"><tags:message code="edit.district"/></th>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <div class="">  
                                                                <input type="hidden" name="district" value="${userObj.district}" />
                                                                <strong>${userObj.district}</strong>                                                             
                                                            </div>
                                                        </td>
                                                    </tr>   
                                                </c:if>
                                                <c:if test="${userObj.city ne NULL && (not empty(userObj.city))}">
                                                    <tr>
                                                        <th class="text-right" width="200"><tags:message code="edit.city"/></th>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <div class="">  
                                                                <input type="hidden" name="city" value="${userObj.city}" />
                                                                <strong>${userObj.city}</strong>                                                             
                                                            </div>
                                                        </td>
                                                    </tr>  
                                                </c:if>
                                                <c:if test="${userObj.address ne NULL && (not empty(userObj.address))}">
                                                    <tr>
                                                        <th class="text-right" width="200"><tags:message code="label.address"/></th>
                                                        <td align="center">:</td>
                                                        <td>
                                                            <div class="">   
                                                                <input type="hidden" name="address" value="${userObj.address}" />
                                                                <strong>${userObj.address}</strong>                                                             
                                                            </div>
                                                        </td>
                                                    </tr>  
                                                </c:if>
                                                <tr>
                                                    <th class="text-right" width="200"><tags:message code="edit.assigh.role"/></th>
                                                    <td align="center">:</td>
                                                    <td>
                                                        <div class="checkbox">
                                                            <c:forEach items="${userRolesMap}" var="rolevar">
                                                                <c:if test="${  userObj.roleIds.contains( rolevar.key ) }">
                                                                    <label class="checkbox">
                                                                        <input type="checkbox" name="userroleids"  value="${rolevar.key}" checked="checked"/><strong> ${rolevar.value}</strong>
                                                                    </label>
                                                                </c:if>
                                                                <c:if test="${ not  userObj.roleIds.contains( rolevar.key ) }">
                                                                    <label class="checkbox">
                                                                        <input type="checkbox" name="userroleids"  value="${rolevar.key}" /><strong>${rolevar.value}</strong>
                                                                    </label>
                                                                </c:if>
                                                            </c:forEach>
                                                        </div>
                                                    </td>
                                                </tr> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <input type="hidden" name="selectedRoleId" id="selectedRoleId" value="${roleId}"/>
                            </div>

                            <div class="card-footer">
                                <div class="text-xs-center small">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    <form:button type="submit" class="btn btn-sm btn-secondary"><tags:message code="edit.button.update"/></form:button>
                                    </div>
                                </div>
                            </div>
                    </form:form>
                </div>
            </div>    
        </div>
    </div>
</div>
<script type="text/javascript">    
    function validateProfile() { 
        var list, index, item, checkedCount;
        checkedCount = 0;
        list = document.getElementsByName('userroleids');
        for (index = 0; index < list.length; ++index) {
            item = list[index];
            if (item.checked) {
                ++checkedCount;
            }
        }
        return true;
    }
</script>