<%-- 
    Document   : user-profession
    Created on : Jun 7, 2016, 5:44:56 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card newchanneloptions">
        <div class="card-header"><i class="fa fa-wrench"></i><tags:message code="profile.settings.heading"/></div>
        <div class="">
            <div class="tabs" style="margin-top: 0;padding:0 !important;">
                <ul id="myTab" class="nav nav-tabs font-weight-bold">
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/interests"><tags:message code="profile.setting.interest"/></a> </li>
                    <li class="nav-item"> <a data-toggle="tab" data-target="#tab1" class="nav-link active"><tags:message code="profile.setting.profession"/></a> </li>
                        <%--<li class="nav-item"> <a class="nav-link" href="${context}/user/specialization"><tags:message code="profile.setting.specialization"/></a> </li>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/languages"><tags:message code="profile.setting.language"/></a> </li>
                        <%--<security:authorize access="hasAnyRole('LIB_USER','CURATOR_USER')"><li class="nav-item"> <a class="nav-link" href="${context}/user/resource_specialization"><tags:message code="profile.setting.resource.specialization"/></a> </li></security:authorize>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news"><tags:message code="profile.setting.news.papers"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news-channels"><tags:message code="profile.setting.news.channels"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div id="tab1" class="tab active">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="card">
                                    <div class="card-header"><tags:message code="profile.setting.select.profession"/><div class="pull-right" id="feedback"></div></div>
                                    <div class="card-block">
                                        <div id="getstarted-complete-profile" class="animated fadeIn">
                                            <div class="nvli-r-c-section well">
                                                <div class="row">
                                                    <div class="">
                                                        <div class="">
                                                            <form class="">
                                                                <fieldset class="form-group">
                                                                    <label for="select-profession" class="col-md-2 form-control-label text-right" >
                                                                        <strong><tags:message code="label.select.profession"/></strong>
                                                                    </label>
                                                                    <div class="col-md-4">
                                                                        <select class="form-control" id="select-profession">
                                                                            <option value=""><tags:message code="select.profession.default.option"/></option>
                                                                            <c:forEach items="${userProfessions}" var="profession">
                                                                                <c:choose>
                                                                                    <c:when test="${not empty(profession.subProfessions)}">
                                                                                        <c:set var="hasChildren" value="true"/>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <c:set var="hasChildren" value="false"/>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                                <option value="${profession.professionName}" 
                                                                                        data-udc-tags="${profession.professionName}"
                                                                                        data-profession-id="${profession.professionId}"
                                                                                        data-has-children="${hasChildren}"
                                                                                        >
                                                                                    ${profession.professionName}
                                                                                </option>
                                                                            </c:forEach>
                                                                        </select>
                                                                    </div>        
                                                                </fieldset>
                                                                <div class="col-md-8 col-md-offset-2">
                                                                    <fieldset class="form-group">
                                                                        <div id="core-profession-heading">
                                                                        </div>
                                                                        <div id="core-functional-areas" 
                                                                             style="margin-left: 20px; overflow-y: auto;"></div>
                                                                    </fieldset>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card">
                                    <div class="card-header"><tags:message code="profile.setting.profession.choose"/></div>
                                    <div class="card-block" id='selected-box-container'>
                                        <ul id="selected-box" class="list-group">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div> 
    </div>
</div>

<script type="text/template" id="tpl-checkbox">
    <div class="checkbox">
    <label class="checkbox">
    <input data-udc="{{=udcTags}}" 
    data-name="{{=professionName}}" 
    type="checkbox" 
    data-has-children="{{=hasChildren}}" 
    id="check-{{=professionId}}" 
    data-profession-id="{{=professionId}}"
    data-value="{{=professionName}}"> 
    {{=professionName}}
    </label>

    {{ if(hasChildren) { }}
    <div id="core-functional-areas-{{=professionId}}" style="display:none; margin-left:30px;" ></div>
    {{ } }}
    </div>
</script>
<script type="text/template" id="tpl-selected-item">
    <li class="list-group-item" >{{=professionName}} 
    <button data-profession-id="{{=professionId}}" title="Remove" class="pull-right btn btn-xs btn-secondary delete-profession">
    <i class="fa fa-trash"></i>
    </button>
    </li>
</script>
<script type="text/template" id="tpl-other-profession">
    <div class="other-profession-class" id="other-profession-{{=id}}">
    <br><h6><b>Enter other profession type</b></h6>
    <input type="text" id="other-profession-input" class="form-control" data-parent-id="{{=parentId}}">
    <button id="btn-save-other-profession" class="btn btn-secondary btn-sm" >Save</button>
    </div>
</script>
<script>
    function showFeedback() {
        $("#feedback").html("<span class=\"text-success animated fadeOut\"><i class=\"fa fa-check-circle\"> Saved</span>");
    }

    function renderSelectedProfessionBlock(response) {
        $("#selected-box").empty();
        __NVLI['professions'] = response.data;
        var data = _.sortBy(response.data, 'professionName');
        _.each(data, function (profession) {
            var tpl = _.template($("#tpl-selected-item").html());
            var opts = {
                professionId: profession.professionId,
                professionName: profession.professionName,
                udcTags: profession.professionUdcTags,
                hasChildren: profession.hasChildren,
                parentId: profession.parentId
            };
            var elem = tpl(opts);
            $("#selected-box").append(elem);
            $(".delete-profession").mouseover(function (e) {
                $(this).addClass("btn-danger");
                $(this).removeClass("btn-secondary");

            });
            $(".delete-profession").mouseout(function (e) {
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-secondary");
            });
            $(".delete-profession").unbind().bind("click", function (e) {
                var prof_id = e.currentTarget.getAttribute("data-profession-id");
                $("#check-" + prof_id).removeAttr("checked");
                updateProfessions(prof_id, "rm");
            });
        });
    }

    function fetchAllSelectedProfessions(callback) {
        $.ajax({
            url: __NVLI.context + "/user/profession/saved/list",
            type: 'GET',
            dataType: 'JSON',
            contentType: 'application/json',
            success: callback,
            error: function (err) {
            }
        });
    }

    function updateProfessions(professionId, action) {
        $.ajax({
            url: window.__NVLI.context + "/user/profession/" + action + "/" + professionId,
            type: "post",
            dataType: 'json',
            contentType: "application/json",
            success: function (responseData) {
                console.log(responseData);
                showFeedback();
                fetchAllSelectedProfessions(renderSelectedProfessionBlock);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ajaz Error While Saving Professions", jqXHR, textStatus, errorThrown);
            }
        });
    }

    function fetchChildProfessions(parentId, callback) {
        $.ajax({
            url: __NVLI.context + "/user/profession/" + parentId,
            type: 'GET',
            dataType: 'JSON',
            contentType: 'application/json',
            success: callback
        });
    }

    function populateSubProfession(professionList) {
        _.each(professionList.children, function (profession) {
            var tpl = _.template($("#tpl-checkbox").html());
            var opts = {
                professionId: profession.professionId,
                professionName: profession.professionName,
                udcTags: profession.udcTags,
                hasChildren: profession.hasChildren
            };
            var el = tpl(opts);
            $("#core-functional-areas-" + professionList.parentId).append(el);
            if (profession.hasChildren) {
                $("#check-" + profession.professionId).unbind("change").bind("change", function (e) {
                    var id = e.currentTarget.getAttribute("data-profession-id");
                    var el = "#core-functional-areas-" + id;
                    if (e.currentTarget.checked) {
                        updateProfessions(id, "add");
                        $(el).show();
                        $(el).empty();
                        $(el).append("<br><h6><b>Select Sub Functional Area</b></h6>");
                        fetchChildProfessions(id, populateSubProfession);
                        $(el).unbind("change").bind("change", function (e) {
                            //addSubFunctionalAreaToList(e.currentTarget.value);
                        });
                    } else {
                        updateProfessions(id, "rm");
                        $(el).hide();
                    }
                });
            } else {
                $("#check-" + profession.professionId).unbind("change").bind("change", function (e) {
                    var id = e.currentTarget.getAttribute("data-profession-id");
                    var el = "#core-functional-areas-" + id;
                    if (e.currentTarget.checked) {
                        if (e.currentTarget.getAttribute("data-value") === "Other") {
                            populateOtherProfession(id, professionList.parentId, e.currentTarget);
                        } else {
                            updateProfessions(id, "add");
                        }
                    } else {
                        if (e.currentTarget.getAttribute("data-value") === "Other") {
                            $("#other-profession-" + id).remove();
                        } else {
                            updateProfessions(id, "rm");
                        }
                    }
                });
            }
        });
    }

    function activateSlimScroll(id, height) {
        $("#" + id).slimScroll({
            height: height,
            size: '8px',
            position: 'right',
            color: '#666',
            alwaysVisible: true,
            distance: '0px',
            railVisible: true,
            railColor: '#DDD',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: true
        });
    }

    function saveOtherProfession(otherPorfession, parentId) {
        $.ajax({
            url: __NVLI.context + "/user/profession/save/other/" + otherPorfession + "/" + parentId,
            type: "post",
            dataType: 'json',
            contentType: "application/json",
            success: function (response) {
//                top.location.reload();
            },
            error: function (err) {
            }
        });
    }

    function populateOtherProfession(id, parentId, target) {
        $("#tpl-other-profession").hide();
        var tpl = _.template($("#tpl-other-profession").html());
        var opts = {
            id: id,
            parentId: parentId
        };
        var el = tpl(opts);
        // $(target).hide();
        ;

//        $(target).parent().append("<br><h6><b>Enter other profession type</b></h6>");
        $(target).parent().append(el);
        $(target).parent().show();
        //$(target).hide();
        //$('#tpl-other-profession').trigger("chosen:updated");
        $("#btn-save-other-profession").unbind("click").bind("click", function (e) {
            // TODO : validate input $("#other-profession-input") fot [a-zA-Z]
            saveOtherProfession($("#other-profession-input").val(), $("#other-profession-input").attr("data-parent-id"));

        });

    }

    $(document).ready(function () {
        // On select-profession select box is loaded fetch corresponding data and populate
        $("#select-profession").ready(function () {
            // Fetch & Render Already Saved Professions
            fetchAllSelectedProfessions(renderSelectedProfessionBlock);
            $("#select-profession").unbind("change").bind("change", function (e) {
                $("#core-functional-areas").empty();
                $("#core-profession-heading").empty();
                $(".other-profession-class").empty();

                var selectedIndex = e.currentTarget.selectedIndex;
                var selectedOption = e.currentTarget.options[selectedIndex];
                var hasChildren = e.currentTarget.options[selectedIndex].getAttribute("data-has-children");
                var id = e.currentTarget.options[selectedIndex].getAttribute("data-profession-id");
                if (hasChildren === "true") {
                    $("#core-profession-heading").html("<h6><b>Select Functional Area</b></h6>");
                    $("#core-functional-areas").append("<div id='core-functional-areas-" + id + "'></div>");
                    var parentId = e.currentTarget.options[selectedIndex].getAttribute("data-profession-id");
                    fetchChildProfessions(parentId, populateSubProfession);
                } else if (e.currentTarget.options[selectedIndex].value === "Other" ||
                        e.currentTarget.options[selectedIndex].value === "other") {
                    $("#core-functional-areas").empty();
                    $("#core-profession-heading").empty();
                    populateOtherProfession(id, -1, e.currentTarget);
                } else {
                    updateProfessions(id, "add");
                }
            });
        });
        activateSlimScroll("getstarted-complete-profile", "550px");
        activateSlimScroll("selected-box", "550px");
    });
</script>
