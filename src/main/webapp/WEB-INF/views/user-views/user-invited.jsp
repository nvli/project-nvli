<%--
    Document   : user-invited
    Created on : 27 Dec, 2016, 12:42:26 PM
    Author     : Ankita
    Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-xs-12">
               <div class="card">
                <div class="card-header row" style="margin: 0;">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><tags:message code="user.invited.page"/>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                        <input id="searchString" class="form-control" placeholder="Search Email">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                        <select id="limitFilter" class="form-control">
                            <option value="10" selected>10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="invitedUsersTable" class="table table-bordered table-striped" data-page-size="15">
                        <thead>
                            <tr id="sort-by-tr">
                                <th> <tags:message code="user.sno"/></th>
                                <th><i class="fa fa-envelope-o"></i> <tags:message code="user.email"/></th>
                                <th><i class="fa fa-users"></i> <tags:message code="user.invited.roles"/></th>
                                <th><i class="fa fa-clock-o"></i>  <tags:message code="user.invited.date"/></th>
                                <th><i class="fa fa-info-circle"></i> <tags:message code="user.status"/></th>
                                <th style="text-align:center;" data-sort-ignore="true" width="125"><i class="fa fa-gear"></i> <tags:message code="user.action"/></th>
                            </tr>
                        </thead>
                        <tbody id="invitedUsersDiv">
                        </tbody>
                    </table>
                </div>
                <div id="event-tbl-footer" class="card-footer clearfix"></div>   
            </div>               
        </div>

        <div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-xs-12">
            <form>
                <div class="card">
                    <div class="card-header"><tags:message code="user.invitation.new"/></div>
                    <div class="card-block inviteauser">
                        <div class="form-group row">
                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12"> <tags:message code="user.email"/></label>
                            <div class="col-lg-10 col-md-8 col-sm-12">
                                <input type="email" class="form-control" name="user-email" id="useremail"  placeholder="username@example.com">
                                <p id="email-status" style="color: red; display: none;" ><strong> Email id is already exist</strong></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user-role" class="form-control-label col-lg-3 col-md-4 col-sm-12"> <tags:message code="user.role"/></label>
                            <div class="col-lg-9 col-md-8 col-sm-12">
                                <c:forEach items="${userRolesMap}" var="rolevar">
                                    <label class="checkbox">
                                        <input type="checkbox" name="user-role" id="user-role" value="${rolevar.key}"/> ${rolevar.value}  
                                    </label>
                                </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <div class="col-xs-offset-3 col-lg-10 col-sm-12">
                            <button id="btnInviteUser" class="btn btn-sm btn-primary" type="button"><tags:message code="user.invite.button"/></button>
                            <input class="btn btn-sm btn-secondary" type="reset" value="Reset">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="edit-invited-user-modal">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Edit Invitation</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><tags:message code="label.organization.email"/></div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                            <input type="text" style="border:none;background-color:transparent" id="invited-user-email" disabled="true"/>
                            <input type="hidden"  id="invitation-id" disabled="true"/>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><tags:message code="user.roles"/></div>
                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12"> 
                            <select class="form-control" name="userRole" id="userRoles" multiple="multiple" size="4" >
                                <c:forEach items="${userRolesMap}" var="rolevar">
                                    <option value="${rolevar.key}">${rolevar.value}</option>
                                </c:forEach>
                            </select> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="button.close"/></button>
                    <button id="btnUpdateInvite" type="button" class="btn btn-primary"><tags:message code="edit.button.update"/></button>
                    <button id="btnCancelInvite" type="button" class="btn btn-primary"><tags:message code="button.Cancel.invitaion"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="invited_user_tpl">
    <tr>
    <td>{{=sno}}</td>
    <td>{{=uEmail}}</td>
    <td class="role-block">{{_.each(invitedRoles, function(role){    }}
    <a class="role-underlined">{{=role.roleName}}</a> <br>
    {{          }); }}</td>
    <td>{{=inviteDate}}</td>
<td>{{=inviteAcceptedStatus}}</td>
<td style="text-align:center">
    <div        class="btn-group">
                    {{ if( inviteAcceptedStatus === "Cancelled" ) { }}
                <button type="button" class="delete-invite btn btn-secondary" data-invitation-email="{{=uEmail}}" data-invitation-id="{{=invitationId}}" title="delete"><span class="fa fa-trash-o"></span></button>
                {{ } else { }}
            <button type="button" class="edit-invite btn btn-secondary" data-toggle="modal" data-target="#exampleModal" data-invitation-email="{{=uEmail}}" data-invitation-id="{{=invitationId}}" title="Edit"><span class="fa fa-edit"></span></button>
            <button type="button" id="btnSendReminder" class="send-reminder btn btn-secondary" data-invitation-email="{{=uEmail}}" data-invitation-id="{{=invitationId}}" title="Invitation Reminder"><span class="fa fa-send"></span></button>
            {{ }        }}
    </div>
</td>
</tr>
</script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderInvitedUserData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderInvitedUserData(1);
        });

        invitationId = "${invitationId}";
        $('#email-status').hide();
        fetchAndRenderInvitedUserData(1);
        $("#btnInviteUser").click(function (e) {
            var useremail;
            var selected_roles = [];
            $("input:checkbox[name=user-role]:checked").each(function () {
                selected_roles.push($(this).val());
            });
            useremail = $("#useremail").val();
            var csrf_token_value = $("#csrf-token").val();
            var csrf_token_name = $("#csrf-token").attr("name");
            var data = {};
            data["useremail"] = useremail;
            data[csrf_token_name] = csrf_token_value;
            $.get(window.__NVLI.context + "/api/useremail/exists", data, function (exist) {
                if (!exist) {
                    checkEmailAvailable(useremail, selected_roles, false);
                } else {
                    $("#email-status").html('<div class="alert alert-danger">User already Exist ...</div>').fadeIn();
                    $("#email-status").fadeOut(10000);
                }
            });
        });

        $("#btnDelete").click(function (e) {
            console.log("shs");
        });
    });

    function fetchAndRenderInvitedUserData(pageNo) {

        $.ajax({
            url: "${pageContext.request.contextPath}/user/manage/invitedUserList",
            data: {searchString: $('#searchString').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo},
            type: 'POST',
            success: function (jsonObject) {
                renderUsers(jsonObject.allUsers);
                renderPagination(fetchAndRenderInvitedUserData, "event-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function renderUsers(allUsers) {
        $("#invitedUsersDiv").empty();
        var Count = 0;
        _.each(allUsers, function (invitedUser) {
            var template = _.template($("#invited_user_tpl").html());
            Count++;
            var opts = {
                sno: Count,
                uEmail: invitedUser.inviteeEmail,
                invitedRoles: invitedUser.roleArray,
                inviteDate: (new Date(invitedUser.invitationDate)).toLocaleDateString(),
                inviteAcceptedStatus: invitedUser.userStatus,
                invitationId: invitedUser.invitationId
            };
            var el = template(opts);
            $("#invitedUsersDiv").append(el);
            $(".edit-invite").unbind().bind("click", function (e) {
                var invitationId = e.currentTarget.getAttribute("data-invitation-id");
                $("#invited-user-email").val(e.currentTarget.getAttribute("data-invitation-email"));
                $("#invitation-id").val(invitationId);
            });
            $(".delete-invite").unbind().bind("click", function (e) {
                deleteInvition(e.currentTarget.getAttribute("data-invitation-id"));
            });
            $(".send-reminder").unbind().bind("click", function (e) {
                var invitationId = e.currentTarget.getAttribute("data-invitation-id");
                var invitationEmail = e.currentTarget.getAttribute("data-invitation-email");
                console.log(invitationId);
                console.log(invitationEmail);
                sendReminder(invitationEmail, invitationId);
            });
        });
        if (Count === 0) {
            $("#invitedUsersDiv").empty();
            $("#invitedUsersDiv").append("<center><tags:message code="sb.no.invited.users"/>!</center>");
        }
    }

    function deleteInvition(invitationId) {
         $.ajax({
            url: __NVLI.context + "/user/deleteInvitation",
            data: { invitationId : invitationId },
            dataType: 'json',
            type: "POST",
            success: function (data, textStatus, jqXHR) {
                fetchAndRenderInvitedUserData(1);
                showToastMessage("Invitation Deleted", "success");
            },error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("something went wrong!!", "error");
               }    
        });
    }

    function sendReminder(useremail, invitationId) {
        var emailValidation = validateEmail(useremail);
        var strconfirm = confirm('Are you sure you want to sent reminder?');
        if (!strconfirm) {
            return false;
        }

        if (useremail.length === 0) {
            showToastMessage('Please enter email!',"error");
            return false;
        }

        if (!emailValidation) {
            showToastMessage('Please enter valid email!',"error");
            return false;
        }

        $.ajax({
            type: "POST",
            url: __NVLI.context + '/user/sendReminder?useremail=' + useremail + "&invitationId=" + invitationId,
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                if (result === 1) {
                    showToastMessage('Reminder mail sent',"success");
                } else {
                    showToastMessage('Failed to sent reminder mail.Please check mailsettings!',"error");
                }
                fetchAndRenderInvitedUserData(1);
            },
            error: function () {
                showToastMessage("Some error came while updatating invitation!","error");
            }
        });
    }

    function checkEmailAvailable(useremail, selected_roles) {

        $.ajax({
            url: __NVLI.context + '/user/checkEmail',
            data: {
                useremail: useremail
            },
            type: 'GET',
            dataType: 'json',
            success: function (response) {

                if (response.isEmailExists) {
                    console.log(useremail);
                    // proceed for sending invites
                    inviteuser(useremail, selected_roles, false, "-1");
                } else {
                    $("#email-status").html('<div class="alert alert-danger">User already invited ...</div>').fadeIn();
                    $("#email-status").fadeOut(10000);
                }
            }
        });
    }

    function cancelInvite(invitationId) {
        $.ajax({
            url: __NVLI.context + '/user/cancelInvitaion?invitationId=' + invitationId,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (response === 1) {
                    $('#exampleModal').modal('hide');
                    console.log("inside");
                    showToastMessage('Invitation Cancelled',"success");
                    fetchAndRenderInvitedUserData(1);
                } else {
                    $('#exampleModal').modal('hide');
                    showToastMessage('Error is coming while cancel invitaion!',"error");
                }

            }
        });
    }

    function inviteuser(useremail, selectedRoles, isUpdate, invitationId) {

        var emailValidation = validateEmail(useremail);
        if (useremail.length === 0) {
            showToastMessage('Please enter email!',"error");
            return false;
        }

        if (!emailValidation) {
            showToastMessage('Please enter valid email!',"error");
            return false;
        }

        if (selectedRoles.length === 0) {
            showToastMessage('Please select atleast one role',"error");
            return false;
        }

        if (isUpdate) {
            var strconfirm = confirm('Are you sure you want to update invitation?');
            if (strconfirm) {
                $.ajax({
                    type: "POST",
                    url: __NVLI.context + '/user/invite?selected_roleid=' + selectedRoles + '&useremail=' + useremail + "&isUpdate=" + isUpdate + "&invitationId=" + invitationId,
                    //data:{selected_roleid:selected_roles},
                    dataType: "json",
                    contentType: "application/json",
                    success: function (result) {
                        if (result === 1) {
                            showToastMessage('Invitation Updated Successfully!',"success");
                        } else {
                            showToastMessage('Invitation could not be update.Please check mailsettings!',"error");
                        }
                        fetchAndRenderInvitedUserData(1);
                    },
                    error: function () {
                        showToastMessage("Some error came while updatating invitation!","error");
                    }
                });
            }
        } else {
            $("#alert-box").hide();
            var strconfirm = confirm('Are you sure you want to send invitation?');
            if (strconfirm) {
                $.ajax({
                    type: "POST",
                    url: __NVLI.context + '/user/invite?selected_roleid=' + selectedRoles + '&useremail=' + useremail + "&isUpdate=" + isUpdate + "&invitationId=-1",
                    dataType: "json",
                    contentType: "application/json",
                    success: function (result) {
                        if (result === 1) {
                            showToastMessage('Invitation Sent Successfully!',"success");
                        } else {
                            showToastMessage('Invitation could not be sent.Please check mailsettings!',"error");
                        }
                        fetchAndRenderInvitedUserData(1);
                    },
                    error: function () {
                        showToastMessage("Some error came while sending invitation!","error");
                    }
                });
            }
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $("#edit-invited-user-modal").ready(function () {
        $("#btnUpdateInvite").unbind().bind("click", function (e) {
            var selected_roles = [];
            $('#userRoles option:selected').each(function (e) {
                selected_roles.push($(this).val());
            });

            inviteuser($("#invited-user-email").val(), selected_roles, true, $("#invitation-id").val());
            $('#exampleModal').modal('hide');
        });
        $("#btnCancelInvite").unbind().bind("click", function (e) {
            cancelInvite($("#invitation-id").val());
        });
    });
</script>
