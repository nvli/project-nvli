<%-- 
    Document   : user-subscriptions
    Created on : Jun 6, 2016, 11:08:52 AM
    Author     : Bhumika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-envelope" style="color: #16987e;"></i><tags:message code="Subscription.heading"/></div>
        <div class="card-block">
            No Subscriptions available
        </div>
        <div class="card-footer"></div>
    </div>
</div>
