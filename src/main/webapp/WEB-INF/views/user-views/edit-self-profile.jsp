<%-- 
    Document   : edit-self-profile
    Created on :May 27, 2016, 3.28 PM
    Author     : Bhumika Gupta
    Author     : Gulafsha Khan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ page session="false"%>

<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
            <form:form  class="form-group required" role="form" id="user-edit-form" action="${context}/user/update" commandName="userDetObj" method="POST" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header"><i class="fa fa-edit"></i>Edit User Profile </div>

                    <%--<form:hidden path="id" id="userid"/>--%>
                    <form:hidden path="profilePicPath"/>
                    <div class="card-block">
                        <div class="row">
                            <div class="profile-image-container text-xs-center col-lg-5 col-lg-offset-0 col-md-6 col-md-offset-3">
                                <%--<spring:eval expression="@propertyConfigurer.getProperty('location.profile.picture')" var="dirPath"/>  --%>    
                                <img src="${exposedProps.userImageBase}/${userDetObj.profilePicPath}"
                                     id="imgProfileImage"
                                     width="100%"
                                     class="edit-user-profile-img"
                                     onerror="this.src='${context}/images/profile-picture.png'"/>
                                <br>
                                <form:input type="file" path="profileImage" id="profileImage" class="m-t-2 m-b-2"/>
                            </div>
                            <div class="profile-content col-lg-7 col-md-12">
                                <div class="col-lg-12 col-md-12 col-sm-12 ">
                                    <div class="">
                                        <div class="form-group row">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="edit.first.name"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:input  id="firstName" path="firstName" type="firstname" class="form-control" placeholder="First Name" required="required"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="edit.middle.name"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:input path="middleName" type="middlename" class="form-control" placeholder="Middle Name"/>
                                            </div>                               
                                        </div>
                                        <div class="form-group row">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="edit.last.name"/></label>                              
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:input path="lastName" type="lastname" class="form-control" placeholder="Last Name" required="required"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">      
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="edit.assigh.role"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <c:forEach items="${userDetObj.rolemap}" var="role" varStatus="loop">
                                                    ${role.value}<c:if test="${!loop.last}">, </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="edit.email"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:input  id="email"  path="email" type="email" class="form-control" placeholder="E-mail" required="required" readonly="true"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="edit.gender"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:select class="form-control" required="required" path="gender" >
                                                    <form:option value="male"><tags:message code="edit.gender.male"/></form:option>
                                                    <form:option value="female"><tags:message code="edit.gender.female"/></form:option>
                                                    <form:option value="other"><tags:message code="edit.gender.other"/></form:option>
                                                </form:select>
                                            </div>
                                        </div>  
                                        <div class="form-group row" id="countryLabel">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="label.country"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:select class="form-control" path="country" id="countries">
                                                </form:select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="stateLabel">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="label.state"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:select class="form-control" path="state" id="states">
                                                </form:select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="districtLabel">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="label.district"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <form:select class="form-control" path="district" id="district">
                                                </form:select>
                                            </div>
                                        </div>
                                        <div class="form-group row clearfix" id="cityLabel1">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="label.city"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12" id="cityValue1">
                                                <form:select class="form-control" path="city" id="city">
                                                </form:select>
                                            </div>
                                        </div>
                                        <div class="form-group row clearfix" id="cityLabel2">
                                            <label class="form-control-label col-lg-3 col-md-3 col-sm-3 text-right text-sm-left"><tags:message code="label.address"/></label>
                                            <div class="col-xl-8 col-lg-9 col-md-9 col-sm-9 col-xs-12" id="cityValue2">
                                                <form:textarea class="form-control"  path="address" id="address"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="card-footer text-center">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" >
                        <form:button type="submit" class="btn btn-primary btn-sm" id="btn-update"><tags:message code="edit.button.update"/></form:button>
                        </div>
                    </div>
            </form:form>

        </div>
    </div>
</div>
<script type="text/javascript">
    function  populateCity(district) {
        $("#city").empty();
        $("#city").append("<option value='' selected>Select</option>");
        $.ajax({
            url: __NVLI.context + "/api/fetch/city/" + parseInt(district),
            dataType: 'json',
            type: 'GET',
            success: function (city) {
                $.each(city, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.cityname);
                    $(opt).attr("value", option.cityname);
                    $(opt).attr("city-id", option.cityid);
                    $('#city').append(opt);
                });
                var userCity = "${userDetObj.city}";
                $('#city').val(userCity);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function  populateDistrict(state) {
        $("#district").empty();
        $("#city").empty();
        $("#district").append("<option value='' selected>Select</option>");
        $.ajax({
            url: __NVLI.context + "/api/fetch/district/" + parseInt(state),
            dataType: 'json',
            type: 'GET',
            success: function (district) {
                $.each(district, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.districtname);
                    $(opt).attr("value", option.districtname);
                    $(opt).attr("district-id", option.districtid);
                    $('#district').append(opt);
                });
                var userDistrict = "${userDetObj.district}";
                $('#district').val(userDistrict);
                var distt = document.getElementById('district');
                var id = distt.options[distt.selectedIndex].getAttribute('district-id');
                if (!_.isEmpty(id)) {
                    populateCity(id);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function  populateStates(country) {
        $("#states").empty();
        $("#district").empty();
        $("#city").empty();
        $("#states").append("<option value='' selected>Select</option>");
        $.ajax({
            url: __NVLI.context + "/api/fetch/states/" + parseInt(country),
            dataType: 'json',
            type: 'GET',
            success: function (states) {
                $.each(states, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.statename);
                    $(opt).attr("value", option.statename);
                    $(opt).attr("state-id", option.stateid);
                    $('#states').append(opt);
                });
                var userState = "${userDetObj.state}";
                $('#states').val(userState);
                var state = document.getElementById('states');
                var id = state.options[state.selectedIndex].getAttribute('state-id');
                if (!_.isEmpty(id)) {
                    populateDistrict(id);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function  populateCountries() {
        $("#countries").empty();
        $("#states").empty();
        $("#district").empty();
        $("#city").empty();
        $.ajax({
            url: __NVLI.context + "/api/fetch/countries",
            dataType: 'json',
            type: 'GET',
            success: function (countries) {
                $.each(countries, function (i, option) {
                    var opt = document.createElement('option');
                    $(opt).text(option.countryname);
                    $(opt).attr("value", option.countryname);
                    $(opt).attr("data-id", option.countryid);
                    $('#countries').append(opt);
                });
                var userCountry = "${userDetObj.country}";
                $('#countries').val(userCountry);
                var countryname = $('#countries').val();
                if (countryname !== 'INDIA')
                {
                    $("#stateLabel").hide();
                    $("#districtLabel").hide();
                    $("#cityLabel1").hide();
                    $("#cityLabel2").show();
                } else
                {
                    $("#stateLabel").show();
                    $("#districtLabel").show();
                    $("#cityLabel2").hide();
                    $("#cityLabel1").show();
                    var country = document.getElementById('countries');
                    var id = country.options[country.selectedIndex].getAttribute('data-id');
                    if (!_.isEmpty(id)) {
                        populateStates(id);
                    }
                }
            }
            ,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function showMessage(message) {
        $("#alert-box").html(message);
        $("#alert-box").show();
    }
    function validateProfile() {
        console.log("validate profile");
        var firstName = $('#firstName').val();
        var username = $('#username').val();
        if (firstName.length == 0) {
            showMessage('Please enter first name!')
            return false;
        }
        //check whether browser fully supports all File API
        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            //get the file size and file type from file input field
            var fsize = $('#profileImage')[0].files[0].size;
            var ftype = $("#profileImage")[0].files[0].type;
            switch (ftype)
            {
                case 'image/png':
                case 'image/jpeg':
                    break;
                default:
                    showMessage('Unsupported File Type.Only JPEG,PNG are allowed!')
                    return false;
            }
            if (fsize > 1048576) //alert if file size more than 1 mb (1024x1024)
            {
                showMessage('File can not be greater than 1 mb!');
                return false;
            }
        } else {
            showMessage("Please upgrade your browser, because your current browser lacks some new features we need!");
        }
        return true;
    }
    $(document).ready(function () {
        populateCountries();
        $("#alert-box").hide();
        $("#user-edit-form").submit(function (e) {
            if (validateProfile()) {
                return true;
            } else {
                e.preventDefault();
                return false;
            }
        });
        $('#countries').change(function (e) {
            var country = $('#countries').val();
            if (country !== 'INDIA')
            {
                $("#states").val('');
                $("#district").val('');
                $("#city").val('');
                $("#address").val('');
                $("#stateLabel").hide();
                $("#districtLabel").hide();
                $("#cityLabel1").hide();
                $("#cityLabel2").show();
            } else
            {
                $("#states").val('');
                $("#district").val('');
                $("#city").val('');
                $("#stateLabel").show();
                $("#districtLabel").show();
                $("#cityLabel2").hide();
                $("#cityLabel1").show();
                populateStates(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("data-id"));
            }
        });
        $('#states').change(function (e) {
            populateDistrict(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("state-id"));
        });
        $('#district').change(function (e) {
            populateCity(e.currentTarget.options[e.currentTarget.selectedIndex].getAttribute("district-id"));
        });

    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgProfileImage').attr('src', e.target.result);
                $('#imgProfileImage').hide();
                $('#imgProfileImage').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profileImage").change(function () {
        readURL(this);
    });
</script>
