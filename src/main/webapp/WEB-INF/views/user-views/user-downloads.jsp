<%-- 
    Document   : user-downloads
    Created on : Jun 14, 2016, 9:27:05 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-download" style="color: #16987e;"></i><tags:message code="dashboard.downloads.heading"/></div>
        <div class="card-block">
           <tags:message code="download.desc"/>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
