<%-- 
    Document   : user-professions
    Created on : Jun 2, 2016, 1:58:10 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />

<div class="col-md-4">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New Profession</h5>
                </div>
                <div class="ibox-content">
                    <form:form commandName="profession" action="${context}/user/manage/professions" method="POST">
                        <p class="small text-muted"><tags:message code="text.profession"/></p>
                        <div class="form-group">
                            <label for="add-profession-name"><tags:message code="label.profession.name"/></label>
                            <form:input cssClass="form-control" id="add-profession-name" path="professionName"/>
                            <form:errors cssClass="text-danger" path="professionName"/>
                        </div>
                        <div class="form-group">
                            <label for="add-profession-desc"><tags:message code="label.profession.desc"/></label>
                            <form:input cssClass="form-control" id="add-profession-desc" path="porfessionDescription"/>
                            <form:errors cssClass="text-danger" path="porfessionDescription"/>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                            <button class="btn btn-primary btn-sm"><tags:message code="label.profession.submit"/></button>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
        <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><tags:message code="heading.profession.list"/></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

        </div>
    </div>
</div>