<%--
    Document   : user-role
    Created on : Apr 22, 2016, 10:29:54 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
    Author     : Ruturaj Powar<ruturajp@cdac.in>
    Author     : Vivek Buagle<bvivek@cdac.in>
    Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<link   href="${context}/themes/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link   href="${context}/components/semantic/dropdown.min.css" rel="stylesheet" type="text/css" />
<link   href="${context}/components/semantic/transition.min.css" rel="stylesheet" type="text/css" />
<script src="${context}/scripts/libs/moment-with-locales.js" type="text/javascript"></script>
<script src="${context}/components/semantic/dropdown.min.js"></script>
<script src="${context}/components/semantic/transition.min.js"></script>
<script src="${context}/themes/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<style>
    .select-container > div {
        width:70%;
    }
    ul.pagination{
        display: inline-block;
        clear: both;
        padding: 4px;
    }
    ul.pagination li {
        list-style: none;
        float: left;
        padding: 8px 15px;
        border: 1px solid #ddd;
        border-radius: 4px;
        cursor: pointer;
        background: #fff;
        margin: 0 2px;
        line-height: 15px;
        text-align: center;
        vertical-align: middle;
    }
    ul.pagination li:hover{
        /*font-weight: bold;*/
        background: #eee;
    }
    ul.pagination li.active {

        background: #0048AB;
    }
    ul.pagination li.active{
        color: #fff;
        font-weight: bold;
    }
    ul.pagination li.disabled{
        cursor: not-allowed;
        background: #ddd;
    }
    .perPage{
        width:450px;
        float: right;
    }
    .pageInfo{
        float: right;
        padding: 10px;
        line-height: 20px;
    }
    .sort-by-th{
        cursor: pointer;
        cursor: hand;
    }

</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin">  <!-- header changes for responsive -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">${roleName}</div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-2">
                    <input id="searchString" class="form-control form-control-md" placeholder="Search By Name" autocomplete="off">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 col-xl-1">
                    <select id="limit" class="form-control form-control-sm" onChange="changeInPerPageValue(this.value)">
                        <option value="10" selected="selected">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" data-page-size="15">
                <thead>
                    <tr id="sort-by-tr">
                        <th>Profile Pic</th>
                        <th class="sort-by-th" id="firstName"><!-- id should be column name of pojo-->
                            <i class="fa fa-user"></i>
                            <tags:message code="user.info"/>
                            <span class="fa fa-sort-asc"></span>
                        </th>
                        <th>
                            <i class="fa fa-users"></i>
                            <tags:message code="user.roles"/>
                        </th>
                        <th>
                            <i class="fa fa-info-circle"></i>
                            <tags:message code="user.status"/>
                        </th>
                        <th style="text-align:center;" data-sort-ignore="true">
                            <i class="fa fa-gear"></i> <tags:message code="user.action"/>
                        </th>
                    </tr>
                </thead>
                <tbody id="userListDiv">
                </tbody>
            </table>
        </div>

        <div class="card-footer" id="pagination">
            <nav aria-label="">
                <ul class="pagination pagination-sm" id="pagination-type-ul">
                    <li class="pagination-type-li first disabled" pageno="first">
                        <span aria-hidden="true">First</span></li><li 
                        class="pagination-type-li first disabled" pageno="prev">
                        <span aria-hidden="true"><span class="fa fa-arrow-left"></span></span>
                    </li><li class="pagination-type-li active" pageno="1">1</li>
                    <li class="pagination-type-li last disabled" pageno="next">
                        <span aria-hidden="true"><span class="fa fa-arrow-right"></span></span>
                    </li><li class="pagination-type-li last disabled" pageno="last">
                        <span aria-hidden="true">Last</span></li></ul>
                <div id="pageInfo" class="pageInfo">Showing <strong>1</strong> out of <strong>1</strong> pages
                </div>
            </nav>
        </div>
    </div>

</div>

<!--Modal for assign resources to the user start-->
<div id="assign-resource-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.assign.resources"/> to <span id="userNameSpan" style="text-transform: capitalize; color: blue"></span></h4>
            </div>
            <div class="modal-body" id="assign-resource-modal-body"></div>
            <div class="modal-footer">
                <button id="saveResourcesBtn" type="button" class="btn btn-primary btn-sm pull-right" disabled="disabled" data-dismiss="modal"><tags:message code="label.save"/></button>
            </div>
        </div>
    </div>
</div>
<!--Modal for assign resources to the user end-->

<!--Modal for assign organization to the user start-->
<div id="assign-organizations-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.assign.organizations"/> to <span id="assign-org-user-name-span" style="text-transform: capitalize; color: yellow"></span></h4>
            </div>
            <div class="modal-body" id="assign-organization-modal-body"></div>
            <div class="modal-footer">
                <button id="save-organization-btn" type="button" class="btn btn-primary pull-right" data-dismiss="modal" disabled="disabled"><tags:message code="label.save"/></button>
            </div>
        </div>
    </div>
</div>
<!--Modal for assign organization to the user end-->

<!--Modal for show work statistics start-->
<div id="show-work-statistics-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <tags:message code="label.statistics"/> of <span id="StatisticsUserNameSpan" style="text-transform: capitalize; color: #09283e"></span></h4>
            </div>
            <div class="">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%"><tags:message code="label.serial.no"/></th>
                            <th width="40%"><tags:message code="label.resource"/></th>
                            <th width="15%"><tags:message code="label.assigned.record"/></th>
                            <th width="15%"><tags:message code="label.curated.records"/></th>
                            <th width="15%"><tags:message code="label.uncurated.records"/></th>
                        </tr>
                    </thead>
                    <tbody id="show-work-statistics-body"></tbody>
                </table>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!--Modal for show work statistics end-->

<!--Modal for download work statistics start-->
<div class="modal fade" id="download-work-statistics-pdf" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="closeButton" onclick="clearDownloadWorkStatisticsModal()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> <tags:message code="label.downloadStatistics"/> of <span id="DownloadStatisticsUserNameSpan" style="text-transform: capitalize; color: #09283e"></span></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="text-right col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label"><b>From <em class="text-danger">*</em> :</b></label>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="input-group date" id="datePicker1">
                            <input type="text" name="fromDate" id="fromDate" required="" class="form-control" />
                            <span class="input-group-addon" id="fromDateCal">
                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </span>
                        </div>
                        <div class="form-group row text-danger"><span class="form-control-feedback clearfix margin10" id="fromDateSpan"></span></div>
                    </div>
                    <label class="text-right col-lg-2 col-md-2 col-sm-2 col-xs-12 control-label"><b>To <em class="text-danger">*</em> :</b></label>
                    <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                        <div class="input-group date" id="datePicker2">
                            <input type="text" name="toDate" id="toDate" required="" class="form-control"/>
                            <span class="input-group-addon" id="toDateCal">
                                <span><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            </span>
                        </div>
                        <div class="form-group row text-danger"><span class="form-control-feedback clearfix margin10" id="toDateSpan"></span></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" id="resetButton"  class="btn btn-primary" onclick="clearDownloadWorkStatisticsModal()">Reset</button>
                <button type="button" id="download-report-button"  class="btn btn-primary" onclick="downloadWorkStatistics()" formtarget="_blank">Download</button>
            </div>
        </div>
    </div>
</div>
<!--Modal for download work statistics end-->

<script type="text/template" id="tpl-user-role-item">
    <a href="${context}/user/manage/role/{{=role_roleId}}">
    <span class="btn btn-sm btn-secondary m-b-1">{{=role_name}}</span>
    </a>
</script>

<script type="text/template" id="tpl-user-list">
    <tr>
    <td style="align:center">
    <a href="${context}/user/profile/view?userId={{=user_id}}&roleId={{=role_id}}">
    <img src="${exposedProps.userImageBase}/{{=user_profilePicPath}}" width="80" class="media-object img-small" alt="{{=user_firstName}} {{=user_lastName}}" onerror="this.src='${context}/images/profile-picture.png'"/>
    </a>
    </td>
    <td>
    <a href="${context}/user/profile/view?userId={{=user_id}}&roleId={{=role_id}}">
    Name : <b>{{=user_firstName}} {{=user_lastName}}</b>
    </a>
    <br>Email : <b>{{=user_email}}</b>
    <br>Username : <b>{{=user_name}}</b>
    </td>
    <td style='width:50%;' id='user-{{=user_id}}-role'></td>
    <td>
    {{ if(user_deleted === 1){ }}
    <span class="label label-primary"><tags:message code="user.action.deleted"/></span>
    {{ }else if(user_enabled === 1){ }}
    <span class="label label-success"><tags:message code="user.action.active"/></span>
    {{ }else if(user_enabled === 0){ }}
    <span class="label label-warning"><tags:message code="user.action.inactive"/></span>
    {{ } }}
    </td>
    <td style="align:center;" class="btn_group_width">
    <div class="btn-group btn_group_width">
    <a class="btn-secondary btn btn-sm m-b-1" href="${context}/user/profile/view?userId={{=user_id}}&roleId={{=role_id}}"  title="View">
    <span class="fa fa-eye" aria-hidden="true"></span>
    <a class="btn-secondary btn btn-sm m-b-1" href="${context}/user/profile/update?userId={{=user_id}}&roleId={{=role_id}}" title="Edit">
    <span class="fa fa-pencil" aria-hidden="true"></span>
    </a>
    {{ if(is_admin_access_allowed) { }}
    {{ if(selected_role_code === "ORG_ADMIN") { }}
    <a class="btn-secondary btn btn-sm m-b-1 assign-organizations" data-user-name="{{=user_firstName}} {{=user_lastName}}" data-user-id="{{=user_id}}"><i class="fa fa-tasks"></i></a>
    {{ } }}
    {{ } }}
    {{ if(is_access_allowed) { }}
    {{ if(selected_role_code === "CURATOR_USER" || selected_role_code === "LIB_USER") { }}
    <a class="btn-secondary btn btn-sm m-b-1 assign-resources" data-user-name="{{=user_firstName}} {{=user_lastName}}" data-user-id="{{=user_id}}" title="Assign Resources"><span><img src="${context}/images/icons/assign-org.png"></span></a>
    <a class="btn-secondary btn btn-sm m-b-1 show-work-statistics" data-stat-user-name="{{=user_firstName}} {{=user_lastName}}" data-stat-user-id="{{=user_id}}" title="Show Work Statistics"><i class="fa fa-file-text-o"></i></a>
    <a class="btn-secondary btn btn-sm m-b-1 download-work-statistics" data-stat-user-name="{{=user_firstName}} {{=user_lastName}}" data-stat-user-id="{{=user_id}}" data-stat-user-role={{=selected_role_code}}  title="Download Work Statistics in PDF"><i class="fa fa-file-pdf-o"></i></a>
    </div>
    {{ } }}
    {{ } }}
    </td>
    </tr>
</script>

<script type="text/template" id="tpl-assign-resource">
    <div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
    <label><b><tags:message code="label.resourcetype"/> :</b></label>
    <select class="resource-type-box form-control">
    {{ _.each(resourceTypeList, function (resourceType) { }}
    <option value="{{=resourceType.id}}">{{=resourceType.resourceTypeName}}</option>
    {{ }); }}
    </select>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
    <label><b><tags:message code="label.resource"/> :</b></label>
    <div id="resourceSelectDiv"></div>
    </div>
    </div><br>
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="card">
    <div class="card-header">
    <strong class="header-title"><tags:message code="label.assigned.resources"/></strong>
    </div>
    <table class="table table-bordered table-striped">
    <thead>
    <tr>
    <th> <tags:message code="label.resource"/></th>
    <th> <tags:message code="label.resourcetype"/></th>
    <th style="text-align:center;"> <tags:message code="label.action"/></th>
    </tr>
    </thead>
    <tbody id="userResourceListTR">
    {{ if(_.isEmpty(userResourceList)) { }}
    <tr id="emptyMsgTR">
    <td colspan="3" class="alert-warning" style="text-align:center"> Resource not assigned.</td>
    </tr>
    {{ } else { }}
    <tr id="emptyMsgTR" style="display: none;">
    <td colspan="3" class="alert-warning" style="text-align:center"> Resource not assigned.</td>
    </tr>
    {{ _.each(userResourceList, function (resource) { }}
    <tr id="{{=resource.id}}">
    <td> {{=resource.resourceName}}</td>
    <td> {{=resource.resourceType}}</td>
    <td style="text-align:center;"> <a class="btn-secondary btn remove-resource" data-resource-id="{{=resource.id}}"><i class="fa fa-trash"></i></a></td>
    </tr>
    {{ }); }}
    {{ } }}
    </tbody>
    </table>
    </div>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-assign-organizations">
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 select-container">
    <select class="ui search selection dropdown" id="org-select">
    <option value="">Select Organization</option>
    {{ _.each(organizationJSONArray, function (organizationJSONObj) { }}
    <option value="{{=organizationJSONObj.organizationId}}">{{=organizationJSONObj.organizationName}}</option>
    {{ }); }}
    </select>
    </div>
    </div>
    <br/>
    <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="card">
    <div class="card-header">
    <strong class="header-title"><tags:message code="label.assigned.organizations"/></strong>
    </div>
    <table class="table table-bordered table-striped assign-organization">
    <thead>
    <tr>
    <th> <tags:message code="label.organization"/></th>
    <th style="text-align:center;"> <tags:message code="label.action"/></th>
    </tr>
    </thead>
    <tbody id="user-assigned-organizations">
    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-assigned-org-table-body">
    {{ if(_.isEmpty(userAssignedOrganizations)) { }}
    <tr id="org-alert">
    <td colspan="2" class="alert-warning" style="text-align:center"> Organization not assigned.</td>
    </tr>
    {{ } else { }}
    <tr id="org-alert" style="display: none;">
    <td colspan="2" class="alert-warning" style="text-align:center"> Organization not assigned.</td>
    </tr>
    {{ _.each(userAssignedOrganizations, function (org) { $(".menu div[data-value='"+org.oraganizationId+"']").hide(); }}
    <tr id="org-{{=org.oraganizationId}}">
    <td> {{=org.organizationName}}</td>
    <td style="text-align:center;">
    <a class="btn-secondary btn remove-organization" data-organization-id="{{=org.oraganizationId}}"><i class="fa fa-trash"></i></a>
    </td>
    </tr>
    {{ }); }}
    {{ } }}
</script>

<script type="text/template" id="tpl-assigned-org-table-row">
    <tr id="org-{{=organizationId}}">
    <td>{{=organizationName}}</td>
    <td style="text-align:center;">
    <a class="btn-secondary btn remove-organization" data-organization-id="{{=organizationId}}"><i class="fa fa-trash"></i></a>
    </td>
    </tr>
</script>

<script type="text/template" id="tpl-select-resource">
    <select class="selectpicker show-tick resource-box-select resource-box" multiple title="Select resource"  data-size="5">
    {{ _.each(resourceList, function (resource) { }}
    {{ if(_.contains(assignedResourceIds, resource.id)){ }}
    <option id="option_{{=resource.id}}" value="{{=resource.id}}" selected>{{=resource.resourceName}}</option>
    {{ } else { }}
    <option id="option_{{=resource.id}}" value="{{=resource.id}}">{{=resource.resourceName}}</option>
    {{ } }}
    {{ }); }}
    </select>
</script>

<script type="text/template" id="tpl-assign-resource-row">
    <tr id="{{=resourceId}}">
    <td> {{=resourceName}}</td>
    <td> {{=resourceType}}</td>
    <td style="text-align:center;"> <a class="btn-secondary btn remove-resource" data-resource-id="{{=resourceId}}"><i class="fa fa-trash"></i></a></td>
    </tr>
</script>

<script type="text/template" id="invited_user_tpl">
    <tr>
    <td>{{=uEmail}}</td>
    <td>{{=invitedRoles}}</td>
    <td>{{=inviteDate}}</td>
    <td>{{=inviteAcceptedStatus}}</td>
    </tr>
</script>

<script type="text/template" id="work_statistics_row_tpl">
    <tr>
    <td>{{=rowCount+1}}</td>
    <td>{{=resourceName}}</td>
    <td id="{{=resourceName}}-assignedCount" class="text-right">
    {{=assignedCount}}
    </td>
    <td id="{{=resourceName}}-curatedCount" class="text-right">
    {{=curatedCount}}
    </td>
    <td id="{{=resourceName}}-uncuratedCount" class="text-right">
    {{=uncuratedCount}}
    </td>
    </tr>
</script>

<script type="text/javascript">
    var roleId, pageWindow, pageNumber, totalPages, jsonObj;
    var sortBy = "firstName";//default sorting will be by firstName
    var sortOrder = "asc";//default order will be ascending
    var _organizations = [];
    var _assignedResourceJSON;
    var _assignedOrganizationIds, _lastSavedOrganizationIds;
    var _assignedResourceIds, _previousResourceIds;
    var _selectedUserId, _lastSavedResourceIds;
    var _userId, _userRole;
    if (localStorage.getItem('pageWindow')) {
        pageWindow = parseInt(localStorage.getItem('pageWindow'));
        $("#limit").val(pageWindow);
    } else {
        pageWindow = 10;
    }

    function renderUsers(allUsers, selectedRoleCode, roleCodes) {
        $("#userListDiv").empty();
        var isAccessAllowed = _.contains(roleCodes, "METADATA_ADMIN");
        var isAdminAccessAllowed = _.contains(roleCodes, "ADMIN_USER");
        if (_.isEmpty(allUsers)) {
            $("#userListDiv").html('<h4>No users present<h4>');
            document.getElementById("pagination").style.display = 'none';
        } else {
            _.each(allUsers, function (user) {
                var userTpl = _.template($("#tpl-user-list").html());

                var opts = {
                    "user_id": user.userId,
                    "user_profilePicPath": user.userProfilePicPath,
                    "user_firstName": user.userFirstName,
                    "user_lastName": user.userLastName,
                    "user_email": user.userEmail,
                    "user_deleted": user.userDeleted,
                    "user_enabled": user.userEnabled,
                    "selected_role_code": selectedRoleCode,
                    "is_access_allowed": isAccessAllowed,
                    "is_admin_access_allowed": isAdminAccessAllowed,
                    "user_name": user.userName,
                    "role_id": roleId
                };

                var el = userTpl(opts);
                $("#userListDiv").append(el);
                var roles = _.sortBy(user.roleArray, 'roleName');

                _.each(roles, function (item) {
                    roleTpl = _.template($("#tpl-user-role-item").html());

                    $role = roleTpl({
                        role_roleId: item.roleId,
                        role_name: item.roleName
                    });

                    $("#user-" + user.userId + "-role").append($role);
                });
            });
        }

        $(".assign-resources").unbind().bind("click", function (e) {
            var userId = e.currentTarget.getAttribute("data-user-id");
            var userName = e.currentTarget.getAttribute("data-user-name");
            if (typeof _assignedResourceJSON === "undefined") {
                $.ajax({
                    url: "${context}/curation-activity/getResourcesByFiltersWithLimit",
                    data: {searchString: "", resourceTypeFilter: 0, dataLimit: 0, pageNo: 0},
                    success: function (jsonObject) {
                        if (jsonObject !== null) {
                            _assignedResourceJSON = jsonObject;
                            openAssignedResourcesModal(jsonObject, userId, userName);
                        } else {
                            alertErrorMsg();
                        }
                    },
                    error: function () {
                        alertErrorMsg();
                    }
                });
            } else {
                openAssignedResourcesModal(_assignedResourceJSON, userId, userName);
            }
        });

        $(".assign-organizations").unbind().bind("click", function (e) {
            var userId = e.currentTarget.getAttribute("data-user-id");
            var userName = e.currentTarget.getAttribute("data-user-name");
            if (_.isEmpty(_organizations)) {
                $.ajax({
                    url: "${context}/curation-activity/fetch-all-organizations",
                    dataType: 'json',
                    success: function (response) {
                        _organizations = response.organizationJSONArray;
                        showAssignOrganizationModal(_organizations, userId, userName);
                    }
                });
            } else {
                showAssignOrganizationModal(_organizations, userId, userName);
            }
        });

        $('.show-work-statistics').on('click', function (e) {
            var userId = e.currentTarget.getAttribute("data-stat-user-id");
            var userName = e.currentTarget.getAttribute("data-stat-user-name");
            openWorkStatisticsModal(userId, userName);
        });

        $('.download-work-statistics').on('click', function (e) {
            var userId = e.currentTarget.getAttribute("data-stat-user-id");
            var userName = e.currentTarget.getAttribute("data-stat-user-name");
            var userRole = e.currentTarget.getAttribute("data-stat-user-role");
            _userId = userId;
            _userRole = userRole;
            downloadWorkStatisticsModal(userName);
        });
    }

    $("#fromDate").on('click', function () {
        document.getElementById('fromDateSpan').innerHTML = " ";
    });

    $("#fromDateCal").on('click', function () {
        document.getElementById('fromDateSpan').innerHTML = " ";
    });

    $("#toDate").on('click', function () {
        document.getElementById('toDateSpan').innerHTML = " ";
    });

    $("#toDateCal").on('click', function () {
        document.getElementById('toDateSpan').innerHTML = " ";
    });

    function clearDownloadWorkStatisticsModal() {
        document.getElementById("fromDate").value = "";
        document.getElementById('fromDateSpan').innerHTML = " ";
        document.getElementById("toDate").value = "";
        document.getElementById('toDateSpan').innerHTML = " ";
    }

    function downloadWorkStatisticsModal(userName) {
        $("#DownloadStatisticsUserNameSpan").html(userName);
        $('#download-work-statistics-pdf').modal('show');
    }

    function downloadWorkStatistics() {
        if ($("#fromDate").val().length > 0 && $("#toDate").val().length > 0) {
            $('#download-work-statistics-pdf').modal('hide');
            if (_userRole === "LIB_USER") {
                window.open("${context}/curation-activity/library-expert-report/" + _userId + "?startDate=" + document.getElementById("fromDate").value + "&endDate=" + document.getElementById("toDate").value);
            }
            if (_userRole === "CURATOR_USER") {
                window.open("${context}/curation-activity/curator-report/" + _userId + "?startDate=" + document.getElementById("fromDate").value + "&endDate=" + document.getElementById("toDate").value);
            }
            clearDownloadWorkStatisticsModal();
        } else {
            if ($("#fromDate").val().length === 0) {
                document.getElementById('fromDateSpan').innerHTML = " Please,select the date.";
            }
            if ($("#toDate").val().length === 0) {
                document.getElementById('toDateSpan').innerHTML = " Please,select the date.";
            }
        }
    }

    function openAssignedResourcesModal(jsonObject, userId, userName) {
        _selectedUserId = userId;
        $.ajax({
            url: "${context}/curation-activity/getAssignedResourcesByUser/" + userId,
            success: function (resourceArray) {
                if (resourceArray !== null) {
                    var assignResourceTpl = _.template($("#tpl-assign-resource").html());

                    var Opts = {
                        "resourceTypeList": jsonObject.resourceTypeArray,
                        "userResourceList": resourceArray
                    };

                    var assignResourceModalBody = assignResourceTpl(Opts);
                    $("#assign-resource-modal-body").empty();
                    $("#assign-resource-modal-body").append(assignResourceModalBody);
                    $("#userNameSpan").html(userName);
                    $("#assign-resource-modal").modal("show");

                    _assignedResourceIds = _.pluck(resourceArray, 'id');
                    _lastSavedResourceIds = _.clone(_assignedResourceIds);
                    bindResourceRemoveBtn();
                    changeResourceType($(".resource-type-box").val());

                    $(".resource-type-box").unbind().bind("change", function () {
                        changeResourceType($(this).val());
                    });
                    $("#saveResourcesBtn").unbind().bind("click", function () {
                        $.ajax({
                            url: "${context}/curation-activity/saveAssignedResources/" + _selectedUserId,
                            data: "assignedResourceIds=" + _assignedResourceIds,
                            success: function (response) {
                                if (response) {
                                    $('#assign-resource-modal').modal("hide");
                                    $("#alert-box").html('<div class="alert alert-success">Assigned resources successfully saved.</div>').fadeIn();
                                    $("#alert-box").fadeOut(5000);
                                } else {
                                    alertErrorMsg();
                                }
                            },
                            error: function () {
                                alertErrorMsg();
                            }
                        });
                    });
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function showAssignOrganizationModal(organizationJSONArray, userId, userName) {
        var assign_org_tpl = _.template($('#tpl-assign-organizations').html());
        var opts = {
            organizationJSONArray: organizationJSONArray
        };
        var assign_org_body = assign_org_tpl(opts);
        $('#assign-organization-modal-body').empty();
        $('#assign-organization-modal-body').append(assign_org_body);
        $("#assign-org-user-name-span").html(userName);
        $('#org-select').dropdown();
        $.ajax({
            url: "${context}/curation-activity/fetch-assigned-organizations",
            data: {
                userId: userId
            },
            dataType: 'json',
            success: function (response) {
                _assignedOrganizationIds = _.pluck(response, 'oraganizationId');
                _lastSavedOrganizationIds = _.clone(_assignedOrganizationIds);
                var assign_org_tbl_body_tpl = _.template($('#tpl-assigned-org-table-body').html());
                var opts = {
                    userAssignedOrganizations: response
                };
                var assign_org_tbl_body = assign_org_tbl_body_tpl(opts);
                $('#user-assigned-organizations').empty();
                $('#user-assigned-organizations').append(assign_org_tbl_body);

                $(".remove-organization").unbind().bind("click", function (e) {
                    removeOrganization(e);
                });
            },
            error: function (xhr) {
                console.log("error ::", xhr);
            }
        });
        $('#assign-organizations-modal').modal('show');

        $('.item').unbind().bind("click", function (e) {

            var orgId = parseInt(e.currentTarget.getAttribute('data-value'));
            var orgName = $(this).text();

            if (_.isEmpty(_assignedOrganizationIds)) {
                $("#org-alert").hide();
            }

            if (_.contains(_assignedOrganizationIds, orgId)) {
                alert('already selected');
            } else {
                _assignedOrganizationIds.push(orgId);
                var assign_org_tbl_row_tpl = _.template($('#tpl-assigned-org-table-row').html());
                var opts = {
                    organizationId: orgId,
                    organizationName: orgName
                };
                var assign_org_tbl_row = assign_org_tbl_row_tpl(opts);
                $('.assign-organization').append(assign_org_tbl_row);

                $(this).hide();

                $(".remove-organization").unbind().bind("click", function (e) {
                    removeOrganization(e);
                });
            }

            if (_.isEmpty(_.difference(_assignedOrganizationIds, _lastSavedOrganizationIds)) && _.isEmpty(_.difference(_lastSavedOrganizationIds, _assignedOrganizationIds))) {
                $('#save-organization-btn').attr('disabled', 'disabled');
            } else {
                $('#save-organization-btn').removeAttr('disabled');
            }
        });

        $("#save-organization-btn").unbind().bind("click", function () {
            $.ajax({
                url: "${context}/curation-activity/save-assigned-organizations",
                data: "userId=" + userId + "&assignedOrganizationIds=" + _assignedOrganizationIds,
                success: function (response) {
                    if (response) {
                        $('#assign-organizations-modal').modal("hide");
                        $("#alert-box").html('<div class="alert alert-success">Assigned resources successfully saved.</div>').fadeIn();
                        $("#alert-box").fadeOut(5000);
                    } else {
                        alertErrorMsg();
                    }
                },
                error: function () {
                    alertErrorMsg();
                }
            });
        });
    }

    function removeOrganization(e) {
        var selectedId = e.currentTarget.getAttribute('data-organization-id');
        _assignedOrganizationIds.splice(_assignedOrganizationIds.indexOf(parseInt(selectedId)), 1);
        $("#org-" + selectedId).remove();
        $(".menu div[data-value='" + selectedId + "']").show();

        if (_.isEmpty(_assignedOrganizationIds)) {
            $("#org-alert").show();
        }

        if (_.isEmpty(_.difference(_assignedOrganizationIds, _lastSavedOrganizationIds)) && _.isEmpty(_.difference(_lastSavedOrganizationIds, _assignedOrganizationIds))) {
            $('#save-organization-btn').attr('disabled', 'disabled');
        } else {
            $('#save-organization-btn').removeAttr('disabled');
        }
    }

    function openWorkStatisticsModal(userId, userName) {
        $.ajax({
            url: "${context}/curation-activity/show-work-statistics/" + userId,
            success: function (response) {
                var curationStatistics = response["curationStatistics"];
                var resourceCodeNameMap = response["resourceCodeNameMap"];
                $('#show-work-statistics-body').empty();
                if (curationStatistics !== undefined && !_.isEmpty(curationStatistics['resourceWiseCount'])) {
                    var tbl_row_template = _.template($('#work_statistics_row_tpl').html());
                    var opts;
                    var tbl_row_template_el;
                    var resourceTypeCode, resourceTypeName, assignedCount, curatedCount, uncuratedCount, rowCount = 0;
                    _.each(curationStatistics['resourceWiseCount'], function (el) {
                        resourceTypeCode = el['resourceTypeCode'];
                        resourceTypeName = el['resourceTypeName'];
                        if (resourceTypeCode !== null && resourceTypeName !== null) {
                            $("#show-work-statistics-body").append('<tr id="resource-type-' + resourceTypeCode + '"><td colspan="5" class="alert-warning" style="text-align:center"><b>' + resourceTypeName + '</b></td></tr>');
                        }
                        _.each(el['userCurationCounts'], function (subEl, key) {
                            resourceName = resourceCodeNameMap[key];
                            assignedCount = subEl['assignedCount'];
                            curatedCount = subEl['curatedCount'];
                            uncuratedCount = subEl['assignedCount'] - subEl['curatedCount'];
                            opts = {
                                "rowCount": rowCount,
                                "resourceName": resourceName,
                                "assignedCount": assignedCount,
                                "curatedCount": curatedCount,
                                "uncuratedCount": uncuratedCount
                            };
                            tbl_row_template_el = tbl_row_template(opts);
                            rowCount++;
                            $('#show-work-statistics-body').append(tbl_row_template_el);
                        });
                    });
                } else {
                    $("#show-work-statistics-body").append('<tr id="resource-not-assigned"><td colspan="5" class="alert-warning" style="text-align:center"><b>No resource assigned.</b></td></tr>');
                }
                $("#StatisticsUserNameSpan").html(userName);
                $('#show-work-statistics-modal').modal('show');

            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function changeResourceType(resourceTypeId) {
        $("#resourceSelectDiv").empty();
        var selectResourceTpl = _.template($("#tpl-select-resource").html());
        _previousResourceIds = _.intersection(_assignedResourceIds, _.pluck(_assignedResourceJSON.resourceMap[resourceTypeId], 'id'));

        var selectOpts = {
            "resourceList": _assignedResourceJSON.resourceMap[resourceTypeId],
            "assignedResourceIds": _previousResourceIds
        };

        var selectResourceEl = selectResourceTpl(selectOpts);
        $("#resourceSelectDiv").append(selectResourceEl);

        $('.selectpicker').selectpicker('show');

        $(".resource-box").unbind().bind("change", function (e) {
            if (e.currentTarget.tagName === "SELECT") {
                var _currentResourceIds = [];
                _.each($(this).val(), function (item) {
                    _currentResourceIds.push(parseInt(item));
                });
                var selectedId;
                if (_.size(_previousResourceIds) >= _.size(_currentResourceIds)) {
                    selectedId = _.difference(_previousResourceIds, _currentResourceIds)[0];
                    _assignedResourceIds.splice(_assignedResourceIds.indexOf(parseInt(selectedId)), 1);
                    $("#" + selectedId).remove();
                    if (_.isEmpty(_assignedResourceIds)) {
                        $("#emptyMsgTR").show();
                    }
                } else {
                    selectedId = _.difference(_currentResourceIds, _previousResourceIds)[0];
                    if (_.isEmpty(_assignedResourceIds)) {
                        $("#emptyMsgTR").hide();
                    }

                    _assignedResourceIds.push(parseInt(selectedId));
                    var resourceRowTpl = _.template($("#tpl-assign-resource-row").html());
                    var resourceRowOpts = {
                        "resourceId": selectedId,
                        "resourceName": $("#option_" + selectedId)[0].innerHTML,
                        "resourceType": $(".resource-type-box option:selected").text()
                    };

                    var resourceRowEl = resourceRowTpl(resourceRowOpts);
                    $("#userResourceListTR").append(resourceRowEl);
                    bindResourceRemoveBtn();
                }
                _previousResourceIds = _.clone(_currentResourceIds);
                if (_.isEmpty(_.difference(_assignedResourceIds, _lastSavedResourceIds)) && _.isEmpty(_.difference(_lastSavedResourceIds, _assignedResourceIds))) {
                    $('#saveResourcesBtn').attr('disabled', 'disabled');
                } else {
                    $('#saveResourcesBtn').removeAttr('disabled');
                }
            }
        });
    }

    function bindResourceRemoveBtn() {
        $(".remove-resource").unbind().bind("click", function (e) {
            var selectedId = e.currentTarget.getAttribute('data-resource-id');
            _assignedResourceIds.splice(_assignedResourceIds.indexOf(parseInt(selectedId)), 1);
            $("#" + selectedId).remove();
            if (_.isEmpty(_assignedResourceIds)) {
                $("#emptyMsgTR").show();
            }
            if (_.isEmpty(_.difference(_assignedResourceIds, _lastSavedResourceIds)) && _.isEmpty(_.difference(_lastSavedResourceIds, _assignedResourceIds))) {
                $('#saveResourcesBtn').attr('disabled', 'disabled');
            } else {
                $('#saveResourcesBtn').removeAttr('disabled');
            }
            $('.dropdown-menu').remove();
            changeResourceType($(".resource-type-box").val());
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }

    function renderSelectedRole(jsonObj) {
        $(".header-title").text(jsonObj.selectedRole);
    }

    function getUsersAsPerPageNo(pageNo) {
        if (pageNo === "prev") {
            pageNumber = (pageNumber - 1) < 1 ? 1 : (pageNumber - 1);
        } else if (pageNo === "next") {
            pageNumber = (pageNumber + 1) > totalPages ? totalPages : (pageNumber + 1);
        } else if (pageNo === "first") {
            pageNumber = 1;
        } else if (pageNo === "last") {
            pageNumber = totalPages;
        } else {
            pageNumber = parseInt(pageNo);
        }
        fetchUserLists();
    }

    function populatePagination() {
        $("#pagination-type-ul").empty();
        $("#pagination-type-ul").append($("<li class='pagination-type-li first'  pageNo='first'><span  aria-hidden='true'>First</span></li>"));
        $("#pagination-type-ul").append($("<li class='pagination-type-li first'  pageNo='prev'><span  aria-hidden='true'><span class='fa fa-arrow-left'></span></span></li>"));
        var initialPage;
        var limitPage;
        if (totalPages > 5 && pageNumber > 3) {
            initialPage = pageNumber - 2;
            limitPage = (pageNumber + 2) > totalPages ? totalPages : (pageNumber + 2);
        } else {
            $("#pagination-type-ul").append($("<li class='active pagination-type-li' pageNo='1'>1</li>"));
            initialPage = 2;
            limitPage = 5 > totalPages ? totalPages : 5;
        }
        while (initialPage <= limitPage) {
            $("#pagination-type-ul").append($("<li class='pagination-type-li' pageNo='" + initialPage + "'>" + initialPage + "</li>"));
            initialPage++;
        }
        $("#pagination-type-ul").append($("<li class='pagination-type-li last'  pageNo='next'><span  aria-hidden='true'><span class='fa fa-arrow-right'></span></span></li>"));
        $("#pagination-type-ul").append($("<li class='pagination-type-li last'  pageNo='last'><span  aria-hidden='true'>Last</span></li>"));
    }

    function renderPagination() {
        populatePagination();
        //to make curruntly selected page active
        $("[pageNo='" + 1 + "']").removeClass('active');
        $("[pageNo='" + pageNumber + "']").addClass('active');
        //to bind click event to each page number
        $("#pagination-type-ul").ready(function (e) {
            $(".pagination-type-li").unbind("click").bind("click", function (e) {
                if ($(this).hasClass('disabled')) {
                    return false;
                }
                pageNo = e.currentTarget.getAttribute("pageNo");
                getUsersAsPerPageNo(pageNo);
            });
        });
        //prev and next button should be disabled if user reaches to first and last
        //page respectively
        if ((pageNumber - 1) < 1) {
            $(".first").addClass('disabled');
        }
        if ((pageNumber + 1) > totalPages) {
            $(".last").addClass('disabled');
        }
        //to show out of total pages,which page user is viewing
        $("#pageInfo").empty();
        $("#pageInfo").append("Showing <strong>" + pageNumber + "</strong> out of <strong>" + totalPages + "</strong> pages");
    }

    function renderUserList(jsonObj) {
        renderPagination();
        renderSelectedRole(jsonObj);
        renderUsers(jsonObj.allUsers, jsonObj.selectedRoleCode, jsonObj.roleCodes);
    }

    function fetchUserLists() {
        var url = "${pageContext.request.contextPath}/user/manage/roleAjax/" + roleId + "/" + pageNumber + "/" + pageWindow + "/" + sortBy + "/" + sortOrder;
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.onreadystatechange = function () {
            if (request.readyState != 4 || request.status != 200)
                return;
            jsonObj = JSON.parse(request.responseText);
            totalPages = jsonObj.totalPages;
            renderUserList(jsonObj);
        }
        history.pushState(pageNumber, "", "?pageNumber=" + pageNumber);
        request.send();
    }

    window.addEventListener("popstate", function (e) {
        if(!_.isNull(e.state)){
            pageNumber = e.state;
            fetchUserLists();
        }
        else{
            history.back();
            return true;
        }
    });
    //function to fetch query parameters from url
    function getParameterByName(name, url) {
        if (!url)
            url = window.location.href;
        name = name.replace("/[\[\]]/g", "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function changeInPerPageValue(perPage) {
        pageNumber = 1;
        pageWindow = perPage;
        localStorage.setItem("pageWindow", perPage);
        fetchUserLists();
    }

    //function to get sorted data when click on column
    $("#sort-by-tr").ready(function (e) {
        $(".sort-by-th").unbind("click").bind("click", function (e) {
            //if selected colum clicked again just toggle ascending and desending
            if (sortBy == e.currentTarget.getAttribute("id")) {
                if (sortOrder == "desc") {
                    sortOrder = "asc";
                    $(this).children('span').removeClass('fa-sort-desc');
                    $(this).children('span').addClass('fa-sort-asc');
                } else {
                    sortOrder = "desc";
                    $(this).children('span').removeClass('fa-sort-asc');
                    $(this).children('span').addClass('fa-sort-desc');
                }
                //else change the sortBy to id of clicked column and set default order ascending
            } else {
                if (sortOrder === "desc") {
                    $(this).parent('tr').children('th').children('span').removeClass('fa-sort-desc');
                    $(this).parent('tr').children('th').children('span').addClass('fa-sort');
                } else {
                    $(this).parent('tr').children('th').children('span').removeClass('fa-sort-asc');
                    $(this).parent('tr').children('th').children('span').addClass('fa-sort');
                }
                $(this).children('span').addClass('fa-sort-asc');
                sortBy = e.currentTarget.getAttribute("id");
                sortOrder = "asc";
            }
            fetchUserLists();
        });
    });

    function fetchSearchedUsers() {
        $('#searchString').devbridgeAutocomplete({
            paramName: 'username',
            minChars: 2,
            serviceUrl: __NVLI.context + '/search/user/usersSuggestionsBySearchedPhrase/' + roleId,
            scroll: true,
            scrollHeight: 100,
            showNoSuggestionNotice: false,
            onSearchComplete: function (query, suggestions) {
                $(".autocomplete-suggestions").hide();
                document.getElementById("pagination").style.display = 'none';
                if (suggestions.length === 0) {
                    $("#userListDiv").empty();
                    $("#userListDiv").html("<h4>No users present with this name<h4>");
                } else {
                    if (suggestions.length > 10) {
                        suggestions = suggestions.slice(0, 0 + 10);
                    }
                    renderUsers(suggestions, suggestions.selectedRole);
                }
            }
        });
    }
    $("#searchString").keyup(function () {
        if (!this.value) {
            fetchUserLists();
            document.getElementById("pagination").style.display = '';
        }
    });
    $(document).ready(function () {
        if (!_.isEmpty(getParameterByName("pageNumber", document.location))) {
            pageNumber = getParameterByName("pageNumber", document.location);
        } else
            pageNumber = 1;
        roleId = "${roleId}";
        fetchUserLists();
        fetchSearchedUsers();

        $('#datePicker1').datetimepicker({
            allowInputToggle: true,
            format: 'YYYY-MM-DD',
            maxDate: moment()
        });

        $('#datePicker2').datetimepicker({
            allowInputToggle: true,
            format: 'YYYY-MM-DD',
            maxDate: moment()
        });

        $("#datePicker1").on("dp.change", function (e) {
            $('#datePicker2').data("DateTimePicker").minDate(e.date);
        });

        $("#datePicker2").on("dp.change", function (e) {
            $('#datePicker1').data("DateTimePicker").maxDate(e.date);
        });

        document.getElementById("fromDate").value = "";
        document.getElementById("toDate").value = "";

        $('#assign-resource-modal').on('hidden.bs.modal', function () {
            $('#saveResourcesBtn').attr('disabled', 'disabled');
        });
    });
</script>
