<%-- 
    Document   : user-role-details
    Created on : Apr 28, 2016, 11:53:25 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        ${roleDetails.name}
                    </div>
                    <div class="card-block">
                        <h4><strong></strong> ${roleDetails.name}</h4>
                        <div><strong></strong> ${roleDetails.description}</div>
                        <small><strong>Role Code </strong> ${roleDetails.code}</small>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        User's with role ${roleDetails.name}
                    </div>
                    <div class="card-block">
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <th><tags:message code="label.uid"/></th>
                                    <th><tags:message code="label.name"/></th>
                                    <th><tags:message code="label.email"/></th>
                                </tr>
                            </thead>
                            <c:forEach items="${roleDetails.users}" var="user">
                                <tr>
                                    <td>${user.id}</td>
                                    <td>${user.firstName} ${user.lastName}</td>
                                    <td>${user.email}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>