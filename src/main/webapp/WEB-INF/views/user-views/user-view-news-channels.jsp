<%-- 
    Document   : user-view-news-channels
    Created on : May 16, 2017, 4:51:46 PM
    Author     : Vivek Bugale <bvivek@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request"/>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-rss"></i> <tags:message code="nvli.news.channles"/>
                <a class="nav-link pull-right" href="${context}/user/subscribe-news-channels" title="News Article Setting"><i class="fa fa-gears"></i></a>
            </div>
            <div class="card-block">
                <div class="row" id="news-feed-container">
                </div>
            </div>
        </div>
</div>
                            
<script type="text/template" id="tpl-news-article-block">
    <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-xs-12 news-block-container" data-paper-code="{{=paperCode}}">
        <div class="card bg-white m-b-2">
            <div class="card-header">
                <img src="${context}/news-agencies-logo/{{=paperCode}}.png" alt="{{=paperName}}" width="30" height="30"/>
                <b>{{=paperName}}</b>
            </div>
            <div id="news-feed-{{=paperCode}}" style="height:400px;">
                <div class="enews_loader"><img src="${context}/themes/images/processing.gif"></div>
            </div>
            
        </div>
    </div>
</script>

<script>

    function renderNewsArticleBlock(response) {
        $("#news-feed-container").empty();
        _.each(response, function (newsPaper) {
            var tpl = _.template($("#tpl-news-article-block").html());
            var opts = {
                paperCode: newsPaper.paperCode,
                paperName: newsPaper.paperName
            };
            var elem = tpl(opts);
            $("#news-feed-container").append(elem);
            fetchNewsArticleFeeds(newsPaper.paperCode);
            
            $(".news-block-container").mouseover(function (e) {
                $("#news-feed-controls-"+e.currentTarget.getAttribute("data-paper-code")).show();
            });

            $(".news-block-container").mouseout(function (e) {
                $("#news-feed-controls-"+e.currentTarget.getAttribute("data-paper-code")).hide();
            });
        });
    }
    
    function fetchUserSubscribedNewsArticle(callback) {
        $.ajax({
            url: __NVLI.context + "/nac/fetch-user-subscribed-news-channels",
            success: callback,
            error: function (err) {
                console.log(err);
            }
        });
    }
    
    function fetchNewsArticleFeeds(paperCode) {       
        $.ajax({
            url: __NVLI.context + "/nac/fetch-news-channels",
            data:{"paperCode": paperCode},
            success: function (response) {
            $("#news-feed-"+paperCode).empty();  
                $("#news-feed-"+paperCode).append(response);           
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
    
    $(document).ready(function () {
        fetchUserSubscribedNewsArticle(renderNewsArticleBlock);
    });
</script>