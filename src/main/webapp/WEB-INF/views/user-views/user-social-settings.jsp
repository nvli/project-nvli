<!-- Document   : user-social-settings.jsp
    Created on : Sep 29, 2016, 10:38:15 AM
    Author     : Administrator-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    .nav-tabs > li.active > a, .nav-tabs > li > a:hover{
        background: #FFF;
        border: none;
    }
    .nav-tabs > li > a{
        padding: 15px 30px;
        font-weight: bold;
    }
    input[type='text'], input[type='password'], input[type='email']{
        font-size: 20px;
        font-weight: normal;
    }
    /*    hr{
            margin: 40px 0;
        }*/
    .card-inner-block{
        background: #f3f3f3;
        padding: 30px 20px;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header">
            <i class="fa fa-cog"></i>  <tags:message code="social.connect.heading"/>
        </div>
        <div class="card-block" style="height: ">
            <div class="container">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div  class="panel">
                        <h4>
                            <c:choose>
                                <c:when test="${connections.get('facebook').getConnectedStatus() eq true}">
                                    <form id="social_facebook_disconnect" action="${context}/connect/facebook/${connections.get('facebook').getProviderUserId()}" method="post" novalidate="novalidate">
                                        <button type="submit" class="btn btn-facebook pull-right"   id="facebook-disconnect-button"><span class="fa fa-facebook "></span> Disconnect</button>
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form id="social_facebook" action="${context}/connect/facebook" method="POST" novalidate="novalidate">
                                        <input type="hidden" name="scope" value="user_posts,public_profile,email,user_location"/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <button type="submit" class="btn btn-facebook pull-right"   id="facebook-connect-button"><span class="fa fa-facebook "></span> Connect</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                            <div class="h4">
                                <tags:message code="social.connect.facebook"/>
                            </div>
                        </h4>
                    </div>
                    <p>
                        <tags:message code="facebook.connect.settings.desc"/>
                    </p>
                    <hr>
                    <%--<div  class="panel">
                        <h4>
                            <input type="hidden" value=${user.id} id="userid"/>
                            <c:choose>
                                <c:when test="${connections.get('twitter').getConnectedStatus()  eq true}">
                                    <form id="social_twitter_disconnect" action="${context}/connect/twitter/${connections.get('twitter').getProviderUserId()}" method="post" novalidate="novalidate">
                                        <button type="submit" class="btn btn-twitter pull-right"   id="twitter-disconnect-button"><span class="fa fa-twitter "></span> Disconnect</button>
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form id="social_twitter" action="${context}/connect/twitter" method="POST" novalidate="novalidate">
                                        <input type="hidden" name="scope" value="user_posts,public_profile,email,user_location"/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <button type="submit" class="btn btn-twitter pull-right"   id="twitter-connect-button"><span class="fa fa-twitter "></span></span> Connect</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                            <div class="h4">
                                <tags:message code="social.connect.twitter"/>
                            </div>
                        </h4>
                    </div>
                    <p>
                        <tags:message code="twitter.connect.settings.desc"/>
                    </p>
                    <hr>--%>
                    <div  class="panel">
                        <h4>
                            <input type="hidden" value=${user.id} id="userid"/>
                            <c:choose>
                                <c:when test="${connections.get('google').getConnectedStatus()  eq true}">
                                    <form id="social_disconnect_google" action="${context}/connect/google/${connections.get('google').getProviderUserId()}" method="post" novalidate="novalidate">
                                        <input type="hidden" name="_method" value="delete" />
                                        <button type="submit" class="btn btn-google pull-right"   id="google-disconnect-button"><span class="fa fa-google-plus"></span> Disconnect</button>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form id="social_google" action="${context}/connect/google" method="post" novalidate="novalidate">
                                        <input type="hidden" name="scope" value="https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <button type="submit" class="btn btn-google pull-right"   id="google-connect-button"><span class="fa fa-google-plus"></span> Connect</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                            <div class="h4">
                                <tags:message code="social.connect.google"/></div>
                        </h4>
                    </div>
                    <p>
                        <tags:message code="google.connect.settings.desc"/>
                    </p>
                    <hr>
                    <div  class="panel">
                        <h4>
                            <input type="hidden" value=${user.id} id="userid"/>
                            <c:choose>
                                <c:when test="${connections.get('linkedin').getConnectedStatus()  eq true}">
                                    <form id="social_disconnect_linkedin" action="${context}/connect/linkedin/${connections.get('linkedin').getProviderUserId()}" method="post" novalidate="novalidate">
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <button type="submit" class="btn btn-linkedin pull-right"   id="linkedin-disconnect-button"><span class="fa fa-linkedin"></span> Disconnect</button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form id="social_linkedin" action="${context}/connect/linkedin" method="POST" novalidate="novalidate">
                                        <input type="hidden" name="scope" value="r_basicprofile,r_emailaddress"/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <button type="submit" class="btn btn-linkedin pull-right"   id="linkedin-connect-button"><span class="fa fa-linkedin"></span> Connect</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                            <div class="h4">
                                <tags:message code="social.connect.linkedin"/>
                            </div>
                        </h4>
                    </div>
                    <p>
                        <tags:message code="linkedin.connect.settings.desc"/>
                    </p>
                    <hr>
                    <div  class="panel">
                        <h4>
                            <input type="hidden" value=${user.id} id="userid"/>

                            <c:choose>
                                <c:when test="${connections.get('github').getConnectedStatus()  eq true}">
                                    <form id="social_connect_github" action="${context}/connect/github/${connections.get('github').getProviderUserId()}" method="post" novalidate="novalidate">
                                        <input type="hidden" name="_method" value="delete" />
                                        <button type="submit" class="btn btn-github pull-right"   id="github-disconnect-button"><span class="fa fa-github "></span> Disconnect</button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form id="social_github" action="${context}/connect/github" method="POST" novalidate="novalidate">
                                        <input type="hidden" name="scope" value="user"/>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <button type="submit" class="btn btn-github pull-right"   id="github-connect-button"><span class="fa fa-github "></span> Connect</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                            <div class="h4">
                                <tags:message code="social.connect.github"/>
                            </div>
                        </h4>
                    </div>
                    <tags:message code="github.connect.settings.desc"/>
                </div>
            </div>
        </div>
    </div>
</div>