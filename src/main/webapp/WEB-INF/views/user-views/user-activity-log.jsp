<%--
    Document   : profile
    Created on : Apr 1, 2016, 12:53:17 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
    Author     : Ruturaj Powar<ruturajp@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    .adjust-left{
        padding-right:0px;
    }
    .adjust-right{
        padding-left:0px;
    }
    .inner{
        font-size:20px;
    }

    ul{
        list-style-type:none;
        padding:0px;
        margin:0px;
    }
    .activity-type-li>a{
        /*list-style:none;*/
        padding: 8px 15px;
    }
    .feed-activity-list .feed-element {
        border-bottom: 1px solid #e7eaec;
    }
    #feed-activity-list{
        background: #FFFFFF;
    }
    .feed-element:first-child {
        margin-top: 0;
    }
    .feed-element {
        padding-bottom: 15px;
    }
    .feed-element,
    .feed-element .media {
        margin-top: 15px;
    }
    .feed-element,
    .media-body {
        overflow: hidden;
    }
    .feed-element > .pull-left {
        margin-right: 10px;
    }
    .feed-element img.img-circle,
    .dropdown-messages-box img.img-circle {
        width: 38px;
        height: 38px;
    }
    .feed-element .well {
        border: 1px solid #e7eaec;
        box-shadow: none;
        margin-top: 10px;
        margin-bottom: 5px;
        padding: 10px 20px;
        font-size: 11px;
        line-height: 16px;
    }
    .feed-element .actions {
        margin-top: 10px;
    }
    .feed-element .photos {
        margin: 10px 0;
    }
</style>

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
        <div class="card">
            <div class="card-header"><i class="fa fa-book"></i>  <tags:message code="activity.log.heading"/>
                <a href='${context}/user/profile' class="pull-right"><i class="fa fa-compress"></i></a>
            </div>
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 adjust-left">
                    <div class="list-group activity-categories">
                        <ul style="padding:10px;" class="nav nav-tabs" id="activity-type-ul">
                            <li class="activity-type-li active" activity-type="allActivities"><a href="#" class="list-group-item"><span class="fa fa-users"></span> <strong><tags:message code="activity.all"/></strong></a></li>
                            <li class="activity-type-li" activity-type="searchActivities"><a href="#" class="list-group-item"><span class="fa fa-search-plus"></span> <strong><tags:message code="activity.search"/></strong></a></li>
                            <li class="activity-type-li" activity-type="visitActivities"><a href="#" class="list-group-item"><span class="fa fa-eye"></span> <strong><tags:message code="activity.visit"/></strong></a></li>
<!--                            <li class="activity-type-li" activity-type="saveActivities"><a href="#" class="list-group-item"><span class="fa fa-floppy-o"></span> <strong><tags:message code="activity.save.search"/></strong></a></li>-->
                            <li class="activity-type-li" activity-type="shareActivities"><a href="#" class="list-group-item"><span class="fa fa-share-square-o"></span> <strong><tags:message code="activity.share"/></strong></a></li>
                            <li class="activity-type-li" activity-type="likesActivities"><a href="#" class="list-group-item"><span class="fa fa-thumbs-o-up"></span> <strong><tags:message code="activity.like"/> </strong></a></li>
                            <li class="activity-type-li" activity-type="crowdSourceActivities"><a href="#" class="list-group-item"><span class="fa fa-tags"></span> <strong><tags:message code="activity.crowdsource"/></strong></a></li>
                            <li class="activity-type-li" activity-type="ratingActivities"><a href="#" class="list-group-item"><span class="fa fa-star-o"></span> <strong><tags:message code="activity.rating"/></strong></a></li>
                            <li class="activity-type-li" activity-type="listActivities"><a href="#" class="list-group-item"><span class="fa fa-folder-open-o"></span><strong> <tags:message code="activity.list"/></strong></a></li>
                            <li class="activity-type-li" activity-type="reviewActivities"><a href="#" class="list-group-item"><span class="fa fa-comment-o"></span> <strong> <tags:message code="activity.review"/></strong></a></li>
                            <li class="activity-type-li" activity-type="ebookActivities"><a href="#" class="list-group-item"><span class="fa fa-book"></span> <strong> <tags:message code="activity.ebook"/></strong></a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-lg-10 col-md-9 col-sm-8 col-xs-6 adjust-right">
                    <div class="feed-activity-list card-block" id="feed-activity-list"></div>
                </div>
            </div>
        </div>   
</div>
<script type="text/javascript">
    var pageNumber;
    var processing;
    var hasNext;
    var totalpages;
    var filter;
    var hasFilter = false;
    var selectedFilePath;
    $("#feed-activity-list").slimScroll({
        height: '700px',
        size: '8px',
        position: 'right',
        color: '#888',
        alwaysVisible: true,
        distance: '3px',
        railVisible: true,
        railColor: '#efefef',
        railOpacity: 0.3,
        wheelStep: 10,
        allowPageScroll: false,
        disableFadeOut: false
    });
    function checkIfObjEmpty(jsonObj) {
        if (!jsonObj.activityLogs.length) {
            var generateHere = document.getElementById("feed-activity-list");
            if (pageNumber == 1) {
                generateHere.innerHTML = '<div class="card-block inner">No Activities Found</div>';
            } else {
                $(".feed-activity-list").append('<div style="text-align:center;" class="card-block inner">End of Activities</div>');
            }
            hasNext = false;
        } else {
            hasNext = true;
        }
    }
    function renderActivityLogs(jsonObj) {
        processing = false;
        checkIfObjEmpty(jsonObj);
        showActivityLogs(jsonObj);
    }
    function showActivityLogWithImage(jsonObj, activity) {
        var tpl = _.template($("#tpl-visited-logs").html());
        var opts = {
            user_profilePicPath: jsonObj.userProfilePicPath,
            user_firstName: jsonObj.userFirstName,
            user_lastName: jsonObj.userLastName,
            user_activity_activityText: activity.activityText,
            user_activity_activityTime: activity.activityTime,
            recordId: activity.recordId,
            selectedPath: activity.recordImgPath
        };
        var el = tpl(opts);
        $(".feed-activity-list").children('div').detach(".inner");
        $(".feed-activity-list").append(el);
    }
//    function showPreview(jsonObj, activity, previewJsonObj) {
//        if (previewJsonObj.path.indexOf(".jpg") >= 0) {
//            showActivityLogWithImage(jsonObj, activity, previewJsonObj);
//        } else {
//            showNormalActivityLogs(jsonObj, activity);
//        }
//    }
//
//    function getTimelinePreview(jsonObj, activity) {
//        let activityText = activity.activityText;
//        text = activityText.substring(activityText.indexOf("<"), activityText.lastIndexOf(">") + 1);
//        el = document.createElement('div');
//        el.innerHTML = text;
//        var link = el.getElementsByTagName('a')[0].href;
//        var recordIndentyfire = link.substring(link.lastIndexOf("/") + 1);
//        var url = "${context}/search/previewForTimeline/" + recordIndentyfire;
//        if (recordIndentyfire !== "undefined") {
//            var request = new XMLHttpRequest();
//            request.open("GET", url, true);
//            request.onreadystatechange = function () {
//                if (request.readyState !== 4 || request.status !== 200)
//                    return;
//                showPreview(jsonObj, activity, JSON.parse(request.responseText));
//            };
//            request.send();
//        }
//    }

    function showNormalActivityLogs(jsonObj, activity) {
        var tpl = _.template($("#tpl-pagination-activityLogs").html());
        var opts = {
            user_profilePicPath: jsonObj.userProfilePicPath,
            user_firstName: jsonObj.userFirstName,
            user_lastName: jsonObj.userLastName,
            user_activity_activityText: activity.activityText,
            user_activity_activityTime: activity.activityTime
        };
        var el = tpl(opts);
        $(".feed-activity-list").children('div').detach(".inner");
        $(".feed-activity-list").append(el);
    }
    function showActivityLogs(jsonObj) {
        _.each(jsonObj.activityLogs, function (activity) {
            activity = JSON.parse(activity);
            console.log("dsbfidsbfjids " + activity);
            if (!_.isEmpty(activity.recordImgPath) && activity.recordImgPath.indexOf(".jpg") >= 0) {
                showActivityLogWithImage(jsonObj, activity);
            } else {
                showNormalActivityLogs(jsonObj, activity);
            }
        });
    }
    function fetchActivityLogs(userid) {
        var url = "${context}/user/filter/activity/log/" + pageNumber + "/" + 15 + "/" + filter + "/" + userid;

        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.onreadystatechange = function () {
            if (request.readyState !== 4 || request.status !== 200)
                return;
            renderActivityLogs(JSON.parse(request.responseText))

        }
        request.send();
    }
    function getFilteredActivityLogs(userid) {
        $(".feed-activity-list").empty();
        pageNumber = 1;
        fetchActivityLogs(userid);
    }
//ajax function only to fire request so that results can be catched.
    function getOtherFilteredLogsToStoreInCache(userid) {
        $('.activity-type-li').each(function () {
            var filter = $(this).attr("activity-type");
            var url = "${context}/user/filter/activity/log/" + 1 + "/" + 15 + "/" + filter + "/" + userid;

            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.onreadystatechange = function () {
                if (request.readyState !== 4 || request.status !== 200)
                    return;
            }
            request.send();
        })

    }
    $(document).ready(function () {
        var userid = "${user.id}";
        filter = "allActivities";
        pageNumber = 1;
        // Fetch Activity Log Data
        fetchActivityLogs(userid);
        getOtherFilteredLogsToStoreInCache(userid);
        //Fetch Next Set Of Activity Logs After Scrolling 80% Of Screen
        $("#feed-activity-list").scroll(function (e) {
            if (processing)
                return false;
            if (hasNext && $("#feed-activity-list").scrollTop() >= ($(window).height() - $("#feed-activity-list").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch Activity Log Data
                fetchActivityLogs(userid);
            }
        });
        //Fetch activity logs after click on particular filter
        $("#activity-type-ul").ready(function (e) {
            $(".activity-type-li").unbind("click").bind("click", function (e) {
                //Make currently selected tab active
                $(this).parent('ul').children('li').removeClass('active');
                $(this).addClass('active');
                //Get type of filter and set to filter-global variable
                filter = e.currentTarget.getAttribute("activity-type");
                getFilteredActivityLogs(userid);
            });
        });
        $("#activity-type-ul").height(700);
    });
</script>
<script type="text/template" id="tpl-pagination-activityLogs">
    <div class="feed-element">
    <a href="#" class="pull-left">
    <img src="${exposedProps.userImageBase}/{{=user_profilePicPath}}"
    height="40px"
    class="profile_img img-circle"
    onerror="this.src='${context}/images/profile-picture.png'"/>
    </a>
    <div class="media-body">
    <small class="pull-right text-navy">{{=user_activity_activityTime}}</small>
    <strong>
    {{=user_firstName}} {{=user_lastName}}
    </strong> {{=user_activity_activityText}} <br>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-visited-logs">
    <div class="feed-element">
    <a href="#" class="pull-left">
    <img src="${exposedProps.userImageBase}/{{=user_profilePicPath}}"
    height="40px"
    class="profile_img img-circle"
    onerror="this.src='${context}/images/profile-picture.png'"/>
    </a>
    <div class="media-body">
    <small class="pull-right text-navy">{{=user_activity_activityTime}}</small>
    <strong>
    {{=user_firstName}} {{=user_lastName}}
    </strong> {{=user_activity_activityText}} <br>
    </div>
    <div style="margin-left:48px;">
    <img style="margin:15px auto; height:200px !important; " id="cut_viewer_{{=recordId}}" src="{{=selectedPath}}" alt="visit-image"  onerror="this.src='${context}/themes/images/no-preview-available.jpg'" class="img img-thumbnail">
    </div>
    </div>
</script>