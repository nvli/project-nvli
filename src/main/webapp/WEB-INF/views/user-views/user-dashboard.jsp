<%--
    Document   : dashboard
    Created on : Apr 1, 2016, 12:48:53 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
    Author     : Ruturaj Powar<ruturajp@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
    .list-item {
        padding: 8px 10px;
        margin-bottom: 8px;
        margin-top: 8px;
        border-bottom: 1px solid #ccc;
    }
    
    .divclass{
        cursor: pointer;
    }
    .collapse > .card-block, .collapsing > .card-block{
        padding: 0;
    }
    .favoriteList{
        padding: 8px 15px;
        border-top: 1px solid #eee;
        background: #fefefe;
        font-size: 14px;
        line-height: 14px;
        display: inline-block;
        width: 100%;
        margin: -4px 0;
    }
    .favoriteList:hover{
        background: #efefef;
    }
    .favoriteList > a.btn{
        margin: 0;
    }
</style>
<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <c:if test="${user.active ne 1}">
                        <div class="alert alert-dismissable alert-warning">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h5 style="font-weight: bold;"><i class="fa fa-warning"></i> <tags:message code="dashboard.email.messege"/></h5>
                            <p><tags:message code="account.activation.resend"/> <a class="btn btn-sm btn-warning" onclick="resendMail()"><i class="fa fa-repeat"></i> <tags:message code="dashboard.email.resend"/></a></p>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>    
    </div>
    <div class="row">

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">

            <div class="">
                <div class="accordion-head">
                    <div class="divclass" data-toggle="collapse" data-target="#favorite_list_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <strong> <tags:message code="dashboard.favorite.resources"/></strong>
                        </span>
                        <span class="arrow-up">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                        </span>
                        <span class="arrow-down">
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="favorite_list_dashboard" class="collapse in border_line">
                            <div class="card-block">
                                <div id="resources_interest-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/saved/resource/interest" 
                                   id="resources_interest-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#interests_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <strong><tags:message code="dashboard.interest.heading"/></strong>
                        </span>
                        <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                        <span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="interests_dashboard" class="collapse in border_line">

                            <div class="card-block">
                                <div  id="interests-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/interests" 
                                   id="interests-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#professions_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span><i class="fa fa-user" aria-hidden="true"></i>
                            <strong><tags:message code="dashboard.profession.heading"/></strong></span><span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="professions_dashboard" class="collapse in border_line">

                            <div class="card-block">
                                <div  id="professions-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/profession" 
                                   id="professions-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#languages_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span><i class="fa fa-language" aria-hidden="true"></i>
                            <strong><tags:message code="dashboard.language.heading"/></strong></span>
                        <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
                        <span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="languages_dashboard" class="collapse in border_line">

                            <div class="card-block">
                                <div  id="languages-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/languages" 
                                   id="languages-container_show_all"
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--            <div class="">
                            <div class="accordion-head">
                                <div style="cursor: pointer;" data-toggle="collapse" data-target="#specializations_dashboard" aria-expanded="true" aria-controls="collapseExample">
                                    <span><i class="fa fa-hand-o-up" aria-hidden="true"></i>
                                        <strong> <tags:message code="dashboard.specialization.heading"/></strong></span><span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <div class="container">
                                <div class="form-group row">
                                    <div id="specializations_dashboard" class="collapse in border_line">
            
                                        <div class="card-block">
                                            <div id="specializations-container"></div>
            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->

            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#ebooks_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span><i class="fa fa-book" aria-hidden="true"></i>
                            <strong> <tags:message code="dashboard.ebooks.heading"/></strong></span><span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="ebooks_dashboard" class="collapse in border_line">

                            <div class="card-block">
                                <div id="ebooks-container" style="padding: 15px;"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/ebooks" 
                                   id="ebooks-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#bookmarkList_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span><i class="fa fa-edit" aria-hidden="true"></i>
                            <strong> <tags:message code="dashboard.article.heading"/></strong></span><span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="bookmarkList_dashboard" class="collapse in border_line">

                            <div class="card-block">
                                <div id="article-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/cs/show-articles" 
                                   id="article-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#bookmarks_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span><i class="fa fa-list" aria-hidden="true"></i>
                            <strong><tags:message code="dashboard.list.heading"/></strong></span><span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="bookmarks_dashboard" class="collapse in border_line">
                            <div class="card-block">
                                <div id="list-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/lists" 
                                   id="list-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="accordion-head">
                    <div style="cursor: pointer;" data-toggle="collapse" data-target="#wlists_dashboard" aria-expanded="true" aria-controls="collapseExample">
                        <span><i class="fa fa-heart" aria-hidden="true"></i>
                            <strong><tags:message code="dashboard.watchlist.heading"/></strong></span><span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="container">
                    <div class="form-group row">
                        <div id="wlists_dashboard" class="collapse in border_line">
                            <div class="card-block">
                                <div id="wlists-container"></div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-secondary bg-faded btn-block" 
                                   href="${context}/user/watch-lists" 
                                   id="wlists-container_show_all" 
                                   style="display: none;">
                                    <tags:message code="dashboard.show.all"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"></div>
    </div>
</div>

<div id="add_to_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="addToListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="add_to_list_link-title"> <tags:message code="dashboard.add.to.list"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix" id="add_to_list_link-body">

                </div>
            </div>
            <!--            <div class="modal-footer">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                            <input type="button" id="AddToExistingListBtn" class="btn btn-primary" value="Add to List"/>
                            <input type="button" id="resetBtn1" class="btn btn-secondary" value="Reset"/>
                        </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<div id="edit_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="editListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="edit_list_link-title">Update</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="listIdHidden"/>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right">List Name :</label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" id="listNameTxt" class="form-control"/>
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right">Visibility :</label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input type="radio" name="accessRadio" value="0"/> Public <input type="radio" name="accessRadio" value="1" /> Private
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" id="updateListBtn" class="btn btn-primary" value="Update"/>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


<script type="text/template" id="tpl-articles">
    <div class="favoriteList">
        <div class="pull-left" style="margin-top:6px;">
            <a href="${context}/cs/preview-article/{{=articleId}}">{{=articleTitle}}</a>
        </div>
        <div class="pull-right" style="margin:0; padding:0;">
            <a href="${context}/cs/edit-article/{{=articleId}}" class="btn btn-sm btn-secondary" data-tooltip="Edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a></span>
        </div>
    </div>
</script>


<script type="text/template" id="tpl-list">
    <div class="favoriteList" id="fav_{{=link}}">
        <div class="pull-left" style="margin-top:6px;">
            <a href="#"  id="{{=link}}" onclick="showMyList(this);" title="{{=title}}">{{=title}}</a>
            <div style="margin:10px 0; font-size:12px; color:#333;">
                <em>Posted {{=privacy}}ly </em><a href="#" title="{{=privacy}}" data-tooltip="{{=privacy}}"><i class="fa fa-globe" aria-hidden="true"></i></a> &nbsp; &nbsp; <i class="fa fa-clock-o"></i> {{=addedTime}} 
            </div>
        </div>
    </div>
</script>

<script type="text/template" id="tpl-wlist">
    <div class="favoriteList "> 
        <div class="pull-left" style="margin-top:6px;">
            <a href="#"  id="{{=title}}" onclick="showMyWatchList('{{=title}}','{{=createdById}}');" title="{{=title}}">{{=title}}</a><br/>   
            <i class="fa fa-clock-o"></i> {{=addedTime}}<br/>
            <i class="fa fa-user"></i>  created by - {{=createdByUsername}}<br/>
        </div>
    </div>
</script>

<script type="text/template" id="tpl-innerSearch">
    <p><span><a href="{{=link}}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i> {{=title}}</a></span></p>
</script>

<script type="text/template" id="tpl-resources_interest">
    <div class="favoriteList">
        <div class="pull-left" style="margin-top:6px;">
            <a href="${context}/{{=link}}">{{=title}}</a>
        </div>
        <div class="pull-right" style="margin:0; padding:0;">
            <a href="${context}/{{=link}}" data-tooltip="View" class="btn btn-sm btn-secondary"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
        </div>
    </div>
</script>
<script type="text/template" id="tpl-interests">
    <div class="favoriteList">
        <div class="pull-left"  style="margin-top:6px;">
            <a href="${context}/search/across/all?language=en&searchElement={{=link}}&ref=user-dashboard"> {{=title}}</a>
        </div>
        <div class="pull-right" style="margin:0; padding:0;">
            <a href="${context}/user/interests" class="btn btn-sm btn-secondary" data-tooltip="Edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
        </div>
    </div>
</script>
<script type="text/template" id="tpl-professions">
    <div class="favoriteList">
        <div class="pull-left"  style="margin-top:6px;">
            <a href="${context}/search/across/all?language=en&searchElement={{=link}}&ref=user-dashboard"> {{=title}}</a>
        </div>
        <div class="pull-right" style="margin:0; padding:0;">
            <a href="${context}/user/profession" class="btn btn-sm btn-secondary" data-tooltip="Edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit </a>
        </div>
    </div>
</script>
<script type="text/template" id="tpl-languages">
    <div class="favoriteList">
        <div class="pull-left"  style="margin-top:6px;">
            <a href="${context}/search/across/all?language=en&searchElement={{=link}}&ref=user-dashboard"> {{=title}}</a>
        </div>
        <div class="pull-right" style="margin:0; padding:0;">
            <a href="${context}/user/languages" data-tooltip="Edit" class="btn btn-sm btn-secondary"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
        </div>
    </div>
</script>
<script type="text/template" id="tpl-ebooks">
    <div class="favoriteList" id="fav_ebook_{{=identifier}}">
    <p><span>
    <a href="${context}/{{=link}}" title="{{=title}}">{{=title}}</a>
    <span class="pull-right"><a href="#" onclick="deleteEBook(this);" id="delete_ebook_{{=identifier}}" title="{{=title}}"><i class="fa fa-trash" aria-hidden="true"></i> </a></span>
    <br/><i class="fa fa-clock-o"></i> {{=addedTime}} <br/></a>
    </span>
    </p>
    </div> <hr/>
</script>

<script type="text/template" id="tpl-specializations">
    <div class="favoriteList">
    <p>
    <span><a>{{=specialization}}</a>
    <span class="pull-right"><a href="${context}/user/specialization"   title="Edit Specializations "><i class="fa fa-edit" aria-hidden="true"></i> Edit </a></span>
    </span>
    </p>
    </div> <hr/>
</script>
<script type="text/template" id="tpl-dashboard-block-unit">
    <div class="dashboard-b-u-c">
        <div class="d-b-u-c-head">
            <div class="d-b-u-c-title">
                <i class="fa fa-heart" aria-hidden="true"></i>
                <strong>{{=blockTitle}}</strong>
            </div>
            <div class="d-b-u-c-controls">
            </div>
        </div>
        <div class="d-b-u-c-container">
            <div class="form-group row">
                <div id="favorite_list_dashboard" class="collapse in border_line">
                    <div class="card-block">
                        <div id="{{=containerId}}"></div>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-secondary bg-faded btn-block" 
                           href="{{=showAllURL}}" 
                           id="wlists-container_show_all" 
                           style="display: none;">
                            Show All
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script>
    var _limit = 2;

    $(document).ready(function () {
        $('#file').awesomeCropper({
            width: 200, height: 200, debug: true
        });
        //min font size
        var min = 11;
        //max font size
        var max = 16;
        //grab the default font size
        var reset = $('body').css('fontSize');
        //font resize these elements
        var elm = $('body');
        //set the default font size and remove px from the value
        var size = str_replace(reset, 'px', '');
        //Increase font size
        $('a.fontSizePlus').click(function () {

            //if the font size is lower or equal than the max value
            if (size <= max) {

                //increase the size
                size++;
                //set the font size
                elm.css({'fontSize': size});
            }

            //cancel a click event
            return false;
        });
        $('a.fontSizeMinus').click(function () {
            //if the font size is greater or equal than min value
            if (size >= min) {
                //decrease the size
                size--;
                //set the font size
                elm.css({'fontSize': size});
            }

            //cancel a click event
            return false;
        });
        //Reset the font size
        $('a.fontReset').click(function () {

            //set the default font size
            elm.css({'fontSize': reset});
        });
        $(".arrow-up").hide();
        $(".arrow-down1").hide();
        $(".accordion-head").click(function () {
            $(this).find(".arrow-up, .arrow-down").toggle();
        });
        $(".accordion-head1").click(function () {
            $(this).find(".arrow-up1, .arrow-down1").toggle();
        });
        fetchAndPopulateArticles();
        fetchUserData(__NVLI.context + "/user/topLists", fetchAndPopulateList);
        fetchUserData(__NVLI.context + "/user/topInterestResources", fetchAndPopulateResourceInterest);
        fetchUserData(__NVLI.context + "/user/topInterests", fetchAndPopulateInterests);
        fetchUserData(__NVLI.context + "/user/topProfessions", fetchAndPopulateProfessions);
        fetchUserData(__NVLI.context + "/user/topLanguages", fetchAndPopulateLanguages);
        fetchUserData(__NVLI.context + "/search/user/watch-lists/results/1/5", fetchAndPopulateWatchList);
        fetchUserData(__NVLI.context + "/user/ebooks/results/1/3", fetchAndPopulateEBooks);
        fetchUserData(__NVLI.context + "/user/topSpecializations", fetchAndPopulateSpecializations);
    });
    function showMyList(list)
    {
        $('#add_to_list_link').modal('show');
        $('#add_to_list_link-title').html(list.title);
        var data = {};
        data["query"] = $("#query").val();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: __NVLI.context + "/user/mylists/" + list.title + "/" + 0,
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 100000,
            success: function (data) {

                var json = "";
                if (data.length === 1)
                {
                    json = json + "<div class='favoriteList' id='mylistcontents_" + data[0].link + "'><p><span><a href='${context}/search/preview/" + data[0].link + "'> " + data[0].title + " </a></span></p></div>";
                } else
                {
                    for (content of data) {

                        json = json + "<div class='favoriteList' id='mylistcontents_" + content.link + "'><p><span><i class='fa fa-dot-circle-o' aria-hidden='true'></i><a href='${context}/search/preview/" + content.link + "'> " + content.title + " </a></span></p></div>";
                    }
                }

                $('#add_to_list_link-body').html('<div class="card-block"> <div id="list-container-contents">' + json + "</div></div>");
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }
    function editMyList(listId, listname, listPrivacy)
    {
        var radioBtnVal;
        $("input[name=accessRadio]").attr('checked', false);
        $('#edit_list_link').modal('show');
        $('#listNameTxt').val(listname);
        $('#listIdHidden').val(listId);
        if (listPrivacy === 'private') {
            radioBtnVal = 1;
        } else if (listPrivacy === 'Public') {
            radioBtnVal = 0;
        }

        $("input[name=accessRadio][value=" + radioBtnVal + "]").prop('checked', true);
    }


    $("#updateListBtn").click(function () {
        var url = __NVLI.context + "/search/user/update/list/" + $('#listIdHidden').val() + "/" + $('#listNameTxt').val() + "/" + $("input:radio[name='accessRadio']:checked").val();
        console.log("url:");
        console.log(url);
        $.ajax({
            url: __NVLI.context + "/search/user/update/list/" + $('#listIdHidden').val() + "/" + $('#listNameTxt').val() + "/" + $("input:radio[name='accessRadio']:checked").val(),
            type: 'GET',
            success: function (res) {
                if (res === 2)
                {
                    alert("This List already Exists.");
                }
                fetchAndPopulateList();
                $('#edit_list_link').modal('hide');
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    });
    function showMyWatchList(list) {
        $('#add_to_list_link').modal('show');
        $('#add_to_list_link-title').html(list.title);
        var data = {};
        data["query"] = $("#query").val();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: __NVLI.context + "/user/mylists/" + list.id,
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 100000,
            success: function (data) {

                var json = "";
                json = json + "<div class='favoriteList' id='mylistcontents_" + data[0].link + "'><p><span><i class='fa fa-dot-circle-o' aria-hidden='true'></i><a href='${context}/search/preview/" + data[0].link + "'> " + data[0].title + " </a></span></p></div>";
                $('#add_to_list_link-body').html('<div class="card-block"> <div id="list-container-contents">' + json + "</div></div>");
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }



    function deleteMyListContents(content)
    {

        if (confirm("Do you want to Delete a Content from List"))
        {
            var data = {};
            data["query"] = $("#query").val();
            $.ajax({
                type: "DELETE",
                contentType: "application/json",
                url: __NVLI.context + "/user/mylistcontents/" + content.id.split("deleteMyListContents_")[1],
                data: data,
                timeout: 100000,
                success: function (data) {

                    if (data === "success")
                    {

                        alert("Content from a List ,Deleted Successfully...");
                        $('#mylistcontents_' + content.id.split("deleteMyListContents_")[1]).remove();
                        if ($('#ist-container-contents').find('.favoriteList').length === 0)
                        {
                            $("#ist-container-contents").empty();
                            $("#list-container").append("<div style='padding:20px; text-align:center;'>No favourite resources found. Please <a href='${context}/user/saved/resource/interest'>click here</a> to select some.</div>");
                        }

                    } else
                    {
                        alert("Error while Deleting....");
                    }

                },
                error: function (e) {
                    console.log("ERROR: ", e);
                    display(e);
                },
                done: function (e) {
                    console.log("DONE");
                }
            });
        }

    }


    function deleteMyList(list)
    {
        if (confirm("Do you want to Delete a List"))
        {
            var data = {};
            data["query"] = $("#query").val();
            $.ajax({
                type: "DELETE",
                contentType: "application/json",
                url: __NVLI.context + "/user/deleteMylists/" + list.id.split("delete_")[1],
                data: data,
                timeout: 100000,
                success: function (data) {

                    if (data === "success")
                    {

                        alert("List Deleted Successfully...");
                        $('#fav_' + list.id.split("delete_")[1]).remove();
                        if ($('#list-container').find('.favoriteList').length === 0)
                        {
                            $("#list-container").empty();
                            $("#list-container").append("<div style='padding:20px; text-align:center;'>No List's Found!</div>");
                            $("#list-container_show_all").hide();
                        }

                    } else
                    {
                        alert("Error while Deleting....");
                    }

                },
                error: function (e) {
                    console.log("ERROR: ", e);
                    display(e);
                },
                done: function (e) {
                    console.log("DONE");
                }
            });
        }
    }

    function deleteEBook(list)
    {
        var data = {};
        data["recordIdentifier"] = list.id.split("delete_ebook_")[1];
        data["title"] = list.title;
        data["query"] = $("#query").val();
        $.ajax({
            contentType: "application/json",
            url: __NVLI.context + "/user/d/ebook",
            data: data,
            timeout: 100000,
            success: function (data) {
                if (data === "success")
                {
                    //alert("eBook Deleted Successfully...");
                    showToastMessage("Your ebook deleted successfully", "success");
                    $("#ebooks-container").empty();
                    fetchUserData(__NVLI.context + "/user/ebooks/results/1/3", fetchAndPopulateEBooks);
                } else
                {
                    showToastMessage("Error while Deleting....");
                }
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });

    }

    function fetchAndPopulateSpecializations(specializations) {
        var Count = 0;
        _.each(specializations, function (specialization) {
            var template = _.template($("#tpl-specializations").html());
            Count++;
            var opts = {
                "specialization": specialization.title

            };
            var el = template(opts);
            $("#specializations-container").append(el);
        });
        if (Count === 0)
        {
            $("#specializations-container").empty();
            $("#specializations-container").append("<div style='padding:20px; text-align:center;'>There were no Specializations  chosen, Please  <a href='${context}/user/specialization' style='color:#3a84de'>click here</a> to choose</div>");
            $("#specializations-container_show_all").hide();
        }
    }

    function fetchAndPopulateInterests(interests) {
        var Count = 0;
        _.each(interests, function (interest) {
            var template = _.template($("#tpl-interests").html());
            Count++;
            var opts = {
                "link": interest.link,
                "title": interest.title

            };
            var el = template(opts);
            $("#interests-container").append(el);
        });
        if (Count === 0)
        {
            $("#interests-container").empty();
            $("#interests-container").append("<div style='padding:20px; text-align:center;'>Please  <a href='${context}/user/interests' style='color:#3a84de'>click here</a> to select your field of <b>Interests</b></div>");
            $("#interests-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#interests-container_show_all").show();
        }
    }

    function fetchAndPopulateProfessions(professions) {
        var Count = 0;
        _.each(professions, function (profession) {
            var template = _.template($("#tpl-professions").html());
            Count++;
            var opts = {
                "link": profession.link,
                "title": profession.title

            };
            var el = template(opts);
            $("#professions-container").append(el);
        });
        if (Count === 0)
        {
            $("#professions-container").empty();
            $("#professions-container").append("<div style='padding:20px; text-align:center;'>Please <a href='${context}/user/profession' style='color:#3a84de'>click here</a> to select your profession.</div>");
            $("#professions-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#professions-container_show_all").show();
        }
    }

    function fetchAndPopulateLanguages(languages) {
        var Count = 0;
        _.each(languages, function (language) {
            var template = _.template($("#tpl-languages").html());
            Count++;
            var opts = {
                "link": language.link,
                "title": language.title

            };
            var el = template(opts);
            $("#languages-container").append(el);
        });
        if (Count === 0) {
            $("#languages-container").empty();
            $("#languages-container").append("<div style='padding:20px; text-align:center;'>No languages chosen, please <a href='${context}/user/languages' style='color:#3a84de'>click here</a> to choose some known languages.</div>");
            $("#languages-container_show_all").hide();
        }
        if (Count > _limit) {
            $("#languages-container_show_all").show();
        }
    }

    function fetchAndPopulateResourceInterest(resources) {
        var Count = 0;
        _.each(resources, function (resource) {
            var template = _.template($("#tpl-resources_interest").html());
            Count++;
            var opts = {
                "link": resource.link,
                "title": resource.title

            };
            var el = template(opts);
            $("#resources_interest-container").append(el);
        });
        if (Count === 0) {
            $("#resources_interest-container").empty();
            $("#resources_interest-container").append("<div style='padding:20px; text-align:center;'>There were no prefered <b>resources</b> chosen, Please  <a href='${context}/user/saved/resource/interest' style='color:#3a84de;'>click here</a> to choose</div>");
            $("#resources_interest-container_show_all").hide();
        }
        if (Count > _limit) {
            $("#resources_interest-container_show_all").show();
        }
    }

    function fetchAndPopulateList(userLists) {
        $("#list-container").empty();
        var Count = 0;
        _.each(userLists, function (list) {
            var template = _.template($("#tpl-list").html());
            Count++;
            var opts = {
                "link": list.link,
                "title": list.title,
                "addedTime": list.addedTime,
                "privacy": list.privacy
            };
            var el = template(opts);
            $("#list-container").append(el);
        });
        if (Count === 0)
        {
            $("#list-container").empty();
            $("#list-container").append("<div style='padding:20px; text-align:center;'>You have not created any lists. Please create some.</div>");
            $("#list-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#list-container_show_all").show();
        }
    }

    function showMyWatchList(listName, creatorId) {
        $('#add_to_list_link').modal('show');
        $('#add_to_list_link-title').html(listName);
        var data = {}
        data["query"] = $("#query").val();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: __NVLI.context + "/user/mylists/" + listName + "/" + creatorId,
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 100000,
            success: function (data) {
                var json = "";
                {
                    for (content of data) {
                        json = json + "<div class='favoriteList' id='mylistcontents_" + content.link + "'><p><span><a href='${context}/search/preview/" + content.link + "'> " + content.title + " </a></span></p></div>";
                    }
                }

                $('#add_to_list_link-body').html('<div class="card-block"> <div id="list-container-contents">' + json + "</div></div>");
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }


    function fetchAndPopulateWatchList(userWLists) {
        var Count = 0;
        _.each(userWLists.watchLists, function (list) {
            var template = _.template($("#tpl-wlist").html());
            Count++;
            var opts = {
                "link": list.listTitle,
                "title": list.listTitle,
                "addedTime": list.addedTime,
                "createdByUsername": list.authorName,
                "createdById": list.authorId
            };
            var el = template(opts);
            $("#wlists-container").append(el);
        });
        if (Count === 0)
        {
            $("#wlists-container").empty();
            $("#wlists-container").append("<div style='padding:20px; text-align:center;'>There were no watch-list's Found!</div>");
            $("#wlists-container_show_all").hide();
        }
        if (Count > _limit) {
            $("#wlists-container_show_all").show();
        }
    }


    function fetchAndPopulateArticles() {
        $.ajax({
            url: __NVLI.context + "/cs/getArticlesByFiltersWithLimit",
            data: {searchString: '', statusTypeFilter: '', dataLimit: '3', pageNo: 1, isRequiredUserRestriction: true},
            type: 'POST',
            success: function (jsonObject) {
                var articleCount = 0;
                _.each(jsonObject.articleList, function (crowdSourceArticle) {
                    var article_template = _.template($("#tpl-articles").html());
                    articleCount++;
                    var opts = {
                        "articleTitle": crowdSourceArticle.articleTitle,
                        "articleId": crowdSourceArticle.id,
                        "articleCount": articleCount
                    };
                    var article_el = article_template(opts);
                    $("#article-container").append(article_el);
                });
                if (articleCount === 0) {
                    $("#article-container").empty();
                    $("#article-container").append("<div style='padding:20px; text-align:center;'>There were no Articles created, <a href='${context}/cs/create-article' style='color:#3a84de'>Click Here</a> to create</div>");
                }
                if (articleCount > _limit) {
                    $("#article-container_show_all").show();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
    function fetchAndPopulateLikes(userLikes) {
        var Count = 0;
        userLikes.reverse();
        userLikes.every(function (like) {
            var template = _.template($("#tpl-likes").html());
            Count++;
            var opts = {
                "link": like.link,
                "title": like.title,
                "addedTime": like.addedTime
            };
            var el = template(opts);
            $("#likes-container").append(el);
            return Count !== 3;
        });
        if (Count === 0)
        {
            $("#likes-container").empty();
            $("#likes-container").append("<div style='padding:20px; text-align:center;'>Oops ! Nothing to show. Please <b><i class='fa fa-like'></i> like</b> some records.</div>");
            $("#likes-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#likes-container_show_all").show();
        }
    }
    function fetchAndPopulateReviews(userReviews) {
        var Count = 0;
        userReviews.every(function (review) {
            var template = _.template($("#tpl-reviews").html());
            Count++;
            var opts = {
                "link": review.link,
                "title": review.title,
                "addedTime": review.addedTime,
                "review": review.review

            };
            var el = template(opts);
            $("#reviews-container").append(el);
            return Count !== 3;
        });
        if (Count === 0)
        {
            $("#reviews-container").empty();
            $("#reviews-container").append("<center>yet to Reviewed!</center>");
            $("#reviews-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#reviews-container_show_all").show();
        }
    }
    function fetchAndPopulateTags(userTags) {
        var Count = 0;
        _.each(userTags, function (tag) {
            var template = _.template($("#tpl-tags").html());
            Count++;
            var tags = tag.tags.split("&|&");
            var tempTag = "";
            for (var i = 0; i < tags.length; i++) {
                if (i > 2)
                    break;
                tempTag = tempTag + '<a href="#"> ' + tags[i] + ' <i class="fa fa-tags" aria-hidden="true"></i></a><br/>';
            }
            var opts = {
                "link": tag.link,
                "title": tag.title,
                "tags": tempTag
            };
            var el = template(opts);
            $("#tags-container").append(el);
        });
        if (Count === 0) {
            $("#tags-container").empty();
            $("#tags-container").append("<center>yet to Tag!</center>");
            $("#tags-container_show_all").hide();
        }
        if (Count > _limit) {
            $("#tags-container_show_all").show();
        }
    }

    function fetchAndPopulateshares(userShares) {
        var Count = 0;
        userShares.every(function (share) {
            var template = _.template($("#tpl-shares").html());
            Count++;
            var socialSiteSymbol = "";
            if (share.socialSiteName === "Facebook")
            {
                socialSiteSymbol = "fa fa-facebook";
            } else if (share.socialSiteName === "Twitter")
            {
                socialSiteSymbol = "fa fa-twitter";
            } else if (share.socialSiteName === "LinkedIn")
            {
                socialSiteSymbol = "fa fa-linkedin";
            } else if (share.socialSiteName === "Google+")
            {
                socialSiteSymbol = "fa fa-google-plus";
            }

            var opts = {
                "link": share.link,
                "title": share.title,
                "addedTime": share.addedTime,
                "socialSiteSymbol": socialSiteSymbol
            };
            var el = template(opts);
            $("#shares-container").append(el);
            return Count !== 3;
        });
        if (Count === 0)
        {
            $("#shares-container").empty();
            $("#shares-container").append("<center>yet to Share!</center>");
            $("#shares-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#shares-container_show_all").show();
        }
    }


    function fetchAndPopulateNvlishares(userNvliShares) {
        var Count = 0;
        userNvliShares.every(function (nvlishare) {
            var template = _.template($("#tpl-nvli-shares").html());
            Count++;

            var opts = {
                "link": nvlishare.link,
                "title": nvlishare.title,
                "addedTime": nvlishare.addedTime
            };
            var el = template(opts);
            $("#nvli-shares-container").append(el);
            return Count !== 3;
        });
        if (Count === 0)
        {
            $("#nvli-shares-container").empty();
            $("#nvli-shares-container").append("<center>yet to Share!</center>");
            $("#nvli-shares-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#nvli-shares-container_show_all").show();
        }
    }

    function fetchAndPopulateEBooks(jsonObj) {
        var Count = 0;
        _.each(jsonObj.suggestions, function (ebook) {
            var template = _.template($("#tpl-ebooks").html());
            Count++;
            var opts = {
                "link": ebook.link,
                "title": ebook.title,
                "addedTime": ebook.addedTime,
                "identifier": ebook.identifier
            };
            var el = template(opts);
            $("#ebooks-container").append(el);
            return Count !== 3;
        });
        if (Count === 0)
        {
            $("#ebooks-container").empty();
            $("#ebooks-container").append("<center>No Saved eBooks Found ! , to Save  <a href='" + __NVLI.context + "/user/suggestedebooks' style='color: #2a5cea;'>click here</a></center>");
            $("#ebooks-container_show_all").hide();
        }
        if (Count > _limit)
        {
            $("#ebooks-container_show_all").show();
        }
    }
//Single ajax function for all fetch request done on this page
//url-used in ajax call
//callBackFun-after success call this function
    function fetchUserData(url, callBackFun) {
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'GET',
            success: callBackFun
        });
    }

    //A string replace function
    function str_replace(haystack, needle, replacement) {
        var temp = haystack.split(needle);
        return temp.join(replacement);
    }

    //WOW Scroll Spy
    var wow = new WOW({
        //disabled for mobile
        mobile: false
    });
    wow.init();
</script>
