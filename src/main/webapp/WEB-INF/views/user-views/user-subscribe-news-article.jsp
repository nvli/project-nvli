<%-- 
    Document   : user-subscribe-news-article
    Created on : May 04, 2017, 4:52:50 PM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    #interest-tab-content .tab-content>.active{
        padding-right: 0px; 
        padding-left: 0px; 
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card newchanneloptions">
        <div class="card-header"><i class="fa fa-wrench"></i><tags:message code="profile.settings.heading"/></div>
        <div class="">
            <div class="tabs" style="margin-top: 0;padding:0 !important;">
                <ul id="myTab" class="nav nav-tabs font-weight-bold" style="margin-top: 0px;">
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/interests"><tags:message code="profile.setting.interest"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/profession"><tags:message code="profile.setting.profession"/></a> </li>
                        <%--<li class="nav-item"> <a class="nav-link" href="${context}/user/specialization"><tags:message code="profile.setting.specialization"/></a> </li>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/languages"><tags:message code="profile.setting.language"/></a> </li>
                        <%--<security:authorize access="hasAnyRole('LIB_USER','CURATOR_USER')"><li class="nav-item"> <a class="nav-link active"><tags:message code="profile.setting.resource.specialization"/></a> </li></security:authorize>--%>
                    <li class="nav-item"> <a class="nav-link active" href="#"><tags:message code="profile.setting.news.papers"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news-channels"><tags:message code="profile.setting.news.channels"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div id="lang-tab-content" class="tab active">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <tags:message code="profile.setting.subscribe.news.paper"/>
                                        <div class="pull-right">
                                            <select class="form-control form-control-sm" id="paper-lang">
                                                <option value="all" selected>All</option>
                                                <c:forEach items="${languageList}" var="language">
                                                    <option value="${language.languageCode}">${language.languageName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div id="news-paper-content-block" class="tab-content">
                                            <div class="row interest_tab" id="news-paper-container"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card newchanneloptions">
                                    <div class="card-header"><tags:message code="profile.setting.subscribed.news.paper"/>&nbsp;&nbsp;<span id="feedback"></span><a href="${context}/user/news-feed" class="btn btn-sm btn-success pull-right" style="margin-top: -3px;"><b><tags:message code="profile.setting.show.feed"/></b></a></div>
                                    <div class="card-block" style="padding: auto 5px !important;">
                                        <div id="subscribed-news-paper-container"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-news-paper">
    <div class="col-xl-4 col-sm-6 col-xs-12">
    <button class="btn btn-secondary btn-block subscribe-paper"
    data-paper-code="{{=paperCode}}" data-paper-display-name="{{=paperName}}">
    <img src="${context}/news-agencies-logo/{{=paperCode}}.png" alt="{{=paperName}}" width="28" height="28"/>
    <b>{{=paperName}}</b>
    </button>
    </div>
</script>

<script type="text/template" id="tpl-subscribed-news-paper">
        <li class="list-group-item" data-paper-code="{{=paperCode}}">
        <img src="${context}/news-agencies-logo/{{=paperCode}}.png" alt="{{=paperName}}" width="30" height="30"/>
        <b>{{=paperName}}</b>
        <button data-paper-code="{{=paperCode}}" title="Remove" class="pull-right btn btn-secondary unsubscribe-paper"><i class="fa fa-trash"></i></button>
        </li>
</script>

<script>
    function showFeedback() {
        $("#feedback").html('<span class="text-success animated fadeOut"><i class="fa fa-check-circle"> Saved</span>');
    }

    function renderNewsPaper(response) {
        $("#news-paper-container").empty();
        _.each(response, function (newsPaper) {
            var tpl = _.template($("#tpl-news-paper").html());
            var opts = {
                paperCode: newsPaper.paperCode,
                paperName: newsPaper.paperDisplayName
            };
            var elem = tpl(opts);
            $("#news-paper-container").append(elem);

        });
        $(".subscribe-paper").unbind().bind("click", function (e) {
            $(this).toggleClass("active");
            var isActive = $(this).hasClass("active");
            var action = isActive ? "add" : "remove";
            updateUserNewsPaper(e.currentTarget.getAttribute("data-paper-code"), e.currentTarget.getAttribute("data-paper-display-name"), action);
        });
        checkSelected();
    }

    function renderSubscribedNewsPaper(response) {
        __NVLI['subscribedNewsPaper'] = response;
        fetchNewsPapers(renderNewsPaper);
        $("#subscribed-news-paper-container").empty();
        _.each(_.sortBy(response, 'paperDisplayName'), function (newsPaper) {
            var tpl = _.template($("#tpl-subscribed-news-paper").html());
            var opts = {
                paperCode: newsPaper.paperCode,
                paperName: newsPaper.paperName
            };
            var elem = tpl(opts);
            $("#subscribed-news-paper-container").append(elem);
            $(".unsubscribe-paper").mouseover(function (e) {
                $(e.currentTarget).addClass("btn-danger");
                $(e.currentTarget).removeClass("btn-secondary");

            });
            $(".unsubscribe-paper").mouseout(function (e) {
                $(e.currentTarget).removeClass("btn-danger");
                $(e.currentTarget).addClass("btn-secondary");
            });
            $(".unsubscribe-paper").unbind().bind("click", function (e) {
                updateUserNewsPaper(e.currentTarget.getAttribute("data-paper-code"), e.currentTarget.getAttribute("data-paper-display-name"), "remove");
            });
        });
    }

    function fetchSubscribedNewsPapers(callback) {
        $.ajax({
            url: __NVLI.context + "/nac/fetch-user-subscribed-news-article",
            success: callback,
            error: function (err) {
                console.log(err);
            }
        });
    }

    function updateUserNewsPaper(paperCode, paperName, action) {
        $.ajax({
            url: window.__NVLI.context + "/nac/update-user-news-paper",
            data: {"action": action, "paperCode": paperCode},
            dataType: 'json',
            contentType: "application/json",
            success: function (responseData) {
                if (responseData.status === "success") {
                    showFeedback();
                }
                if (action === "add") {
                    prependToSubscribed(paperCode, paperName);
                } else {
                    removeFromSubscription(paperCode);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ajaz Error While Saving Professions", jqXHR, textStatus, errorThrown);
            }
        });
    }

    function prependToSubscribed(paperCode, paperName) {
        var isPresent = false;
        $(".list-group-item").each(function () {
            if ($(this).attr('data-paper-code') === paperCode) {
                isPresent = true;
            }
        });
        if (!isPresent) {
            var tpl = _.template($("#tpl-subscribed-news-paper").html());
            var opts = {
                paperCode: paperCode,
                paperName: paperName
            };
            var elem = tpl(opts);
            $("#subscribed-news-paper-container").prepend(elem);
            $(".unsubscribe-paper").unbind().bind("click", function (e) {
                updateUserNewsPaper(e.currentTarget.getAttribute("data-paper-code"), e.currentTarget.getAttribute("data-paper-display-name"), "remove");
            });
        }
    }

    function removeFromSubscription(paperCode) {
        $('li[data-paper-code="' + paperCode + '"]').remove();
        $('button[data-paper-code="' + paperCode + '"]').removeClass("active");
    }

    function activateSlimScroll(id, height) {
        $("#" + id).slimScroll({
            height: height,
            size: '8px',
            position: 'right',
            color: '#666',
            alwaysVisible: false,
            distance: '0px',
            railVisible: true,
            railColor: '#DDD',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: true
        });
    }

    function fetchNewsPapers(callback) {
        $.ajax({
            url: __NVLI.context + "/nac/fetch-all-news-papers",
            data: {"paperLang": $("#paper-lang").val()},
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function checkSelected() {
        _.each(__NVLI.subscribedNewsPaper, function (newsPaper) {
            var elems = $(".subscribe-paper[data-paper-code='" + newsPaper.paperCode + "']");
            _.each(elems, function (el) {
                $(el).addClass('active');
            });
        });
    }

    $(document).ready(function () {
        fetchSubscribedNewsPapers(renderSubscribedNewsPaper);

        activateSlimScroll("subscribed-news-paper-container", "550px");
        activateSlimScroll("news-paper-content-block", "550px");

        $('#paper-lang').unbind().bind('change', function () {
            fetchNewsPapers(renderNewsPaper);
        });
    });

</script>