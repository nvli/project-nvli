<%--
    Document   : user-role
    Created on : Apr 22, 2016, 10:29:54 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>

<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        All User Roles
                    </div>
                   <div class=" table-responsive">
                  <table class="footable table table-stripped table-bordered toggle-arrow-tiny" data-page-size="15">
                            <thead>
                                <tr>
                                    <th data-toggle="true"><tags:message code="user.roleid"/></th>
                                    <th data-hide="phone"><tags:message code="user.role.name"/></th>
                                    <th data-hide="phone"><tags:message code="user.noofuser"/></th>
                                    <th data-hide="phone,tablet"><tags:message code="user.desc"/></th>
                                    <th data-hide="phone"><tags:message code="user.role.code"/></th>
                                     <th data-sort-ignore="true"><tags:message code="user.action"/></th>
                                </tr>
                            </thead>
                            <tbody id="role-list-container"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--            <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            Create User Role
                                        </div>
                                        <div class="card-block">
                                             Form Goes Here, This form may be submitted using Ajax
            <form:form action="${context}/user/create/role" class="m-t" role="form" id="createRoleForm" commandName="roleModel">
                <div class="form-group">
                    <label for="role-name"><tags:message code="user.role.tittle"/></label>
                <form:input path="name" type="text" class="form-control" name="role-name" id="role-name" required="" placeholder="Role Title"/>
                <p class="help-block small"><tags:message code="user.role.desc"/></p>
            </div>
            <div class="form-group">
                <label for="role-code"><tags:message code="role.code"/></label>
                <form:input path="code" class="form-control" name="role-code" id="role-code" required="required" placeholder="Role Code"/>
                <p class="help-block small"><tags:message code="role.code.desc"/></p>
            </div>
            <div class="form-group">
                <label for="role-description"><tags:message code="user.desc"/></label>
                <form:textarea path="description" class="form-control" name="role-description" id="role-description" required="" placeholder="Description"></form:textarea>
                    <p class="help-block small"><tags:message code="role.desc.desc"/></p>
                </div>
                <div class="form-group">
                <form:button class="btn btn-sm btn-primary" type="submit"><tags:message code="role.create"/></form:button>
            </div>
            </form:form>
        </div>
    </div>
</div>
<div class="col-md-12 animated fadeInDown"></div>
<div class="col-md-12 animated fadeInDown"></div>
</div>
</div>-->
        </div>
    </div>
</div>
<script type="text/template" id="tpl-role">
    <tr class="">
    <td>{{=roleId}}</td>
    <td><strong><a href="{{=targetURL}}" >{{=name}}</a></strong></td>
    <td>{{=userCount}}</td>
    <td>{{=description}}</td>
    <td><strong>{{=code}}</strong></td>
    <td>
    <div class="btn-group">
    <a class="btn-default btn btn-sm btn-secondary action-view-role"
    data-role-id="{{=roleId}}" title="View" href="{{=targetURL}}">
    <i class="fa fa-eye"></i>
    </a>
    </div>
    </td>
    </tr>
</script>
<script>
    function populateRoles(roles) {
        // Empty Roles
        $("#role-list-container").empty();
        _.each(roles, function (role) {
            var role_template = _.template($("#tpl-role").html());
            var opts = {
                "roleId": role.roleId,
                "name": role.roleName,
                "description": role.roleDescription,
                "userCount": role.noOfUsers,
                "code": role.roleCode,
                targetURL: __NVLI.context + "/user/manage/role/" + role.roleId
            };
            var role_el = role_template(opts);
            $("#role-list-container").append(role_el);
            // User Role Page Event Binding
            $(".action-delete-role").unbind().bind("click", function (event) {
                deleteUserRole(event.currentTarget.getAttribute("data-role-id"));
            });
        });
    }
    function fetchRoles() {
        $.ajax({
            url: __NVLI.context + "/api/roles",
            dataType: 'json',
        type: 'GET',
            success: function (data) {
                populateRoles(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
    $(document).ready(function () {
        // Fetch Roles & Populate
        fetchRoles();
    });
</script>