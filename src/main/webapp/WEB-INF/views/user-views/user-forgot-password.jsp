<%-- 
    Document   : user-forgot-password
    Created on : Apr 26, 2016, 12:17:34 PM
    Author     : Bhumika
    Author     : Vivek Bugale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="container-fluid" id="main">
    <div class="clear"></div>
    <div class="col-lg-12">
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div style="margin-top:8%;">
            <div class="login_box">
                <div class="card-header">
                    <tags:message code="reset.password"/>
                </div>
                <div class="card-block" >
                    <div class="form-group clearfix" id="email-input-block">
                        <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right" for="inputEmail">
                            <tags:message code="forgot.registerd.email"/>
                        </label>
                        <div class="col-lg-8 col-md-12 col-sm-12">
                            <input id="emailAdd" name="emailAdd" type="email" class="form-control" placeholder="Enter registered email address" required="required" autofocus>
                        </div>                        
                    </div> 
                    <div class="form-group clearfix" id="email-signin-block">
                        <label class="form-control-label col-lg-4 col-md-12 col-sm-12"></label>
                        <div class="col-lg-8 col-md-12 col-sm-12">
                            <p>
                                <span class="light">
                                    Already have login and password?
                                    <b><a href="${context}/auth/login" style="color:#1b69b6">Sign in</a></b>
                                </span>
                            </p>
                        </div>
                    </div> 
                    <div id="message-block" style="display: none;"></div>
                </div>
                <div class="card-footer">
                    <div style="text-align: center;">                        
                        <button type="submit"  id="reset-email-btn" class="btn btn-primary center" onclick="sendMail()">
                            <tags:message code="button.send.reset.email"/>
                        </button>
                        <button type="submit" id="return-signin-btn" style="display: none;" class="btn btn-primary center" onclick="returnToSignIn()">
                            <tags:message code="button.return.to.signin"/>
                        </button>
                    </div>
                </div>      
            </div>            
        </div>
    </div>
</div>
<script type="text/javascript">
    
    $("#emailAdd").on("input", function () {
        $("#message-block").empty().removeClass().hide();
    });
    
    function sendMail() {
        var emailAdd = $("#emailAdd").val();
        if (isValidEmailId(emailAdd)) {
            $.ajax({
                url: window.__NVLI.context + "/auth/forgot-password-mail?emailAdd=" + emailAdd,
                cache: false,
                dataType: 'json',
                success: function (response) {
                    $("#message-block").empty().removeClass();                
                    Object.keys(response)[0] === "success" ? resetSuccess(response[Object.keys(response)[0]]) : resetError(response[Object.keys(response)[0]]);
                },
                error: function (xhr) {
                    console.error("error in sendMail ::",xhr);
                    $("#message-block").empty().removeClass();
                    $("#message-block").addClass("alert alert-danger").show();
                    $("#message-block").html("<b>Oops! Unable to send the reset email. Please check your Email Address.</b>");
                }
            });
        } else {
            $("#message-block").empty().removeClass();
            $("#message-block").addClass("alert alert-danger").show();
            $("#message-block").html("<strong>Enter a valid email.</strong>");
        }
    }

    function isValidEmailId(emailFieldID) {
        var regex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
        return regex.test(emailFieldID);
    }
    
    function resetSuccess(message){
        $("#email-input-block").remove();
        $("#email-signin-block").remove();
        $("#reset-email-btn").hide();     
        $("#return-signin-btn").show();  
        $("#message-block").addClass("alert alert-success").show();
        $("#message-block").html("<strong>"+ message +"</strong>");
    }
    
    function resetError(message){
        $("#message-block").addClass("alert alert-danger").show();
        $("#message-block").html("<strong>"+ message +"</strong>");
    }
    
    function returnToSignIn(){
        window.location = "${context}/auth/login";
    }

</script>
