<%-- 
    Document   : user-language-settings
    Created on : Jun 23, 2016, 4:52:50 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    #interest-tab-content .tab-content>.active{
        padding-right: 0px; 
        padding-left: 0px; 
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card newchanneloptions">
        <div class="card-header"><i class="fa fa-wrench"></i><tags:message code="profile.settings.heading"/></div>
        <div class="">
            <div class="tabs" style="margin-top: 0;padding:0 !important;">
                <ul id="myTab" class="nav nav-tabs font-weight-bold">
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/interests"><tags:message code="profile.setting.interest"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/profession"><tags:message code="profile.setting.profession"/></a> </li>
                        <%--<li class="nav-item"> <a class="nav-link" href="${context}/user/specialization"><tags:message code="profile.setting.specialization"/></a> </li>--%>
                    <li class="nav-item"> <a class="nav-link active" href="#"><tags:message code="profile.setting.language"/></a> </li>
                        <%--<security:authorize access="hasAnyRole('LIB_USER','CURATOR_USER')"><li class="nav-item"> <a class="nav-link" href="${context}/user/resource_specialization"><tags:message code="profile.setting.resource.specialization"/></a> </li></security:authorize>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news"><tags:message code="profile.setting.news.papers"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news-channels"><tags:message code="profile.setting.news.channels"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div id="lang-tab-content" class="tab active">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 col-lg-8">
                                <div class="card">
                                    <div class="card-header"><tags:message code="profile.setting.select.languages"/> <div id="feedback" class="pull-right"></div></div>
                                    <div class="card-block">
                                        <!--                                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 550px;"> -->
                                        <div id="lang-tab-content-block" class="tab-content" style="overflow: hidden; width: auto; height: 550px;">
                                            <div class="row interest_tab" id="language-container">
                                            </div>
                                        </div>
                                        <!--                                            <div class="slimScrollBar" style="background: rgb(102, 102, 102) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 0px; height: 550px;"></div>
                                                                                    <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: block; border-radius: 7px; background: rgb(221, 221, 221) none repeat scroll 0% 0%; opacity: 0.3; z-index: 90; right: 0px;"></div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4 col-lg-4">
                                <div class="card">
                                    <div class="card-header"><tags:message code="profile.setting.language"/></div>
                                    <div class="card-block">
                                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 550px;">
                                            <div id="selected-box"  style="overflow: hidden; width: auto; height: 550px;"></div>
                                            <!--                                            <div class="slimScrollBar" style="background: rgb(102, 102, 102) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 0px; height: 550px;"></div>
                                                                                        <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: block; border-radius: 7px; background: rgb(221, 221, 221) none repeat scroll 0% 0%; opacity: 0.3; z-index: 90; right: 0px;"></div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-language-item">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <button class="btn btn-secondary btn-lg btn-block language-item"
    style="margin:5px 0px 5px 0px; text-align:left !important;overflow: hidden;"
    data-language-code="{{=languageCode}}"
    data-language-id="{{=languageId}}"
    data-language-name="{{=languageName}}"
    data-is-indian-lang="{{=isIndianLanguage}}">
    {{=languageName}}
    </button>
    </div>
</script>

<script type="text/template" id="tpl-selected-item">
    <li class="list-group-item" >{{=languageName}} 
    <button data-language-id="{{=languageId}}" title="Remove" class="pull-right btn btn-xs btn-secondary delete-language">
    <i class="fa fa-trash"></i>
    </button>
    </li>
</script>
<script>
    function showFeedback() {
        $("#feedback").html("<span class=\"text-success animated fadeOut\"><i class=\"fa fa-check-circle\"> Saved</span>");
    }

    function renderLanguages(response) {
        $("#language-container").empty();
        _.each(response, function (language) {
            var tpl = _.template($("#tpl-language-item").html());
            var opts = {
                languageCode: language.languageCode,
                isIndianLanguage: language.isIndianLanguage,
                languageId: language.languageId,
                languageName: language.languageName
            };
            var elem = tpl(opts);
            $("#language-container").append(elem);

        });
        $(".language-item").unbind().bind("click", function (e) {
            $(this).toggleClass("active");
            var isActive = $(this).hasClass("active");
            var action = isActive ? "add" : "rm";
            updateLanguage(e.currentTarget.getAttribute("data-language-id"), action);
        });
        checkSelected();
    }

    function renderSelectedLanguageBlock(response) {
        __NVLI['selectedLanguages'] = response;
        fetchUDCLanguages(renderLanguages);
        $("#selected-box").empty();
        _.each(response, function (language) {
            var tpl = _.template($("#tpl-selected-item").html());
            var opts = {
                languageId: language.languageId,
                languageName: language.languageName
            };
            var elem = tpl(opts);
            $("#selected-box").append(elem);
            $(".delete-language").mouseover(function (e) {
                $(e.currentTarget).addClass("btn-danger");
                $(e.currentTarget).removeClass("btn-secondary");

            });
            $(".delete-language").mouseout(function (e) {
                $(e.currentTarget).removeClass("btn-danger");
                $(e.currentTarget).addClass("btn-secondary");
            });
            $(".delete-language").unbind().bind("click", function (e) {
                updateLanguage(e.currentTarget.getAttribute("data-language-id"), "rm");
            });
        });
    }

    function fetchAllSelectedLanguages(callback) {
        $.ajax({
            url: __NVLI.context + "/user/saved/languages",
            type: 'GET',
            dataType: 'JSON',
            contentType: 'application/json',
            success: callback,
            error: function (err) {
                console.log(err);
            }
        });
    }

    function updateLanguage(languageId, action) {
        $.ajax({
            url: window.__NVLI.context + "/user/" + action + "/language/" + languageId,
            type: "post",
            dataType: 'json',
            contentType: "application/json",
            success: function (responseData) {
                showFeedback();
                fetchAllSelectedLanguages(renderSelectedLanguageBlock);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ajaz Error While Saving Professions", jqXHR, textStatus, errorThrown);
            }
        });
    }

    function activateSlimScroll(id, height) {
        $("#" + id).slimScroll({
            height: height,
            size: '8px',
            position: 'right',
            color: '#666',
            alwaysVisible: true,
            distance: '0px',
            railVisible: true,
            railColor: '#DDD',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: true
        });
    }
    function fetchUDCLanguages(callback) {
        $.ajax({
            url: __NVLI.context + "/api/udc/languages",
            type: 'GET',
            dataType: 'JSON',
            contentType: 'application/json',
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function checkSelected() {
        _.each(__NVLI.selectedLanguages, function (lang) {
            var elems = $(".language-item[data-language-id='" + lang.languageId + "']");
            _.each(elems, function (el) {
                $(el).addClass('active');
            });
        });
    }
    $(document).ready(function () {
        fetchAllSelectedLanguages(renderSelectedLanguageBlock);

        activateSlimScroll("selected-box", "550px");
        activateSlimScroll("lang-tab-content-block", "550px");
    });

</script>