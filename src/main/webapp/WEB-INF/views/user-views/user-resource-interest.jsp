
<%@page import="java.util.List"%>
<%@page import="in.gov.nvli.domain.resource.ResourceType"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-heart"></i>&nbsp;<tags:message code="favorite.resource.heading"/></div>
        <!-- <form:form   commandName="user" action="${context}/user/save/resource/interest" method="post">
            <!--            <div class="card-block">
                            <div class="nav-tabs p-a-1 font-weight-bold">My Preferred Resources</div>
            <c:set var="userResourceInterest" value="${user.resourceInterest}"/>
            <ul class="card-block">
                <li>
                    <div class="form-group row">
            <c:set value="${fn:length(resourceTypes)/2}" var="sizeflag" />
            <fmt:parseNumber  type="number" integerOnly="true"  var ="intsizeflag" value="${sizeflag}" />
            <fmt:parseNumber  type="number" integerOnly="true"  var ="baseintsizeflag" value="${sizeflag}" />
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">  
            <c:forEach items="${resourceTypes}" var="resourceType" varStatus="rt"  >
                <c:if test ="${rt.index eq intsizeflag}">
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 ">  
                    <c:set value="${baseintsizeflag+intsizeflag}" var="intsizeflag" /> 
                </c:if>
                <c:set var="rtype" value="${resourceType.value}"/>
                <ul>
                    <li>
                <%
                    if (pageContext.getAttribute("userResourceInterest") != null) {
                        List<ResourceType> userResourceInterest = (List) pageContext.getAttribute("userResourceInterest");
                        ResourceType rtype = (ResourceType) pageContext.getAttribute("rtype");
                        if (userResourceInterest.contains(rtype)) {
                            pageContext.setAttribute("checked", "checked");
                        } else {
                            pageContext.setAttribute("checked", "");
                        }
                    }
                %>
                <input type="checkbox"  name="resourceInterest['${rt.index}']" value="${resourceType.value.id}" ${checked} id="${resourceType.value.id}">
                <span data-labelfor="${resourceType.value.id}">&nbsp;
                ${resourceType.value.resourceType}
            </span>
        </li>
    </ul>
            </c:forEach>
        </div>
    </div>
</li>
</ul> 
        </form:form> -->
        <div class="">
            <div class="tab-content">
                <div id="lang-tab-content" class="tab active">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            <div class="card">
                                <div class="card-header"><tags:message code="select.resources"/> <div id="feedback" class="pull-right"></div></div>
                                <div class="card-block">
                                    <div id="lang-tab-content-block" class="tab-content">
                                        <div class="row interest_tab" id="resource-container">

                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer clearfix"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="card-header"><tags:message code="resource.choose"/></div>
                                <div class="card-block"><div id="selected-box"></div></div>
                                <div class="card-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-resource-item">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <button class="btn btn-secondary btn-lg btn-block resource-item"
    style="margin:5px 0px 5px 0px; text-align:left !important;overflow: hidden;"
    data-resource-code="{{=resourceCode}}"
    data-resource-id="{{=resourceId}}"
    data-resource-name="{{=resourceName}}">
    {{=resourceName}}
    </button>
    </div>
</script>

<script type="text/template" id="tpl-selected-item">
    <li class="list-group-item" >{{=resourceName}} 
    <button data-resource-id="{{=resourceId}}" title="Remove" class="pull-right btn btn-xs btn-secondary delete-resource">
    <i class="fa fa-trash"></i>
    </button>
    </li>
</script>
<script>
    function showFeedback() {
        $("#feedback").html("<span class=\"text-success animated fadeOut\"><i class=\"fa fa-check-circle\"> Saved</span>");
    }

    function renderResources(response) {
        $("#resource-container").empty();
        _.each(response, function (resource) {
            var tpl = _.template($("#tpl-resource-item").html());
            var opts = {
                resourceCode: resource.resourceCode,
                // isIndianLanguage: language.isIndianLanguage,
                resourceId: resource.resourceId,
                resourceName: resource.resourceName
            };
            var elem = tpl(opts);
            $("#resource-container").append(elem);

        });
        $(".resource-item").unbind().bind("click", function (e) {
            $(this).toggleClass("active");
            var isActive = $(this).hasClass("active");
            var action = isActive ? "add" : "rm";
            updateResource(e.currentTarget.getAttribute("data-resource-id"), action);
        });
        checkSelected();
    }

    function renderSelectedResourcesBlock(response) {
        __NVLI['selectedResource'] = response;
        fetchAllResources(renderResources);
        $("#selected-box").empty();
        _.each(response, function (resource) {
            var tpl = _.template($("#tpl-selected-item").html());
            var opts = {
                resourceId: resource.resourceId,
                resourceName: resource.resourceName
            };
            var elem = tpl(opts);
            $("#selected-box").append(elem);
            $(".delete-resource").mouseover(function (e) {
                $(e.currentTarget).addClass("btn-danger");
                $(e.currentTarget).removeClass("btn-secondary");

            });
            $(".delete-resource").mouseout(function (e) {
                $(e.currentTarget).removeClass("btn-danger");
                $(e.currentTarget).addClass("btn-secondary");
            });
            $(".delete-resource").unbind().bind("click", function (e) {
                updateResource(e.currentTarget.getAttribute("data-resource-id"), "rm");
            });
        });
    }

    function fetchAllSelectedResources(callback) {
        console.log("asdasda");
        $.ajax({
            url: __NVLI.context + "/api/saved-resource-interest",
            type: 'GET',
            dataType: 'JSON',
            contentType: 'application/json',
            success: callback,
            error: function (err) {
                console.log(err);
            }
        });
    }

    function updateResource(resourceId, action) {
        $.ajax({
            url: window.__NVLI.context + "/api/" + action + "/save-resource-interest/" + resourceId,
            type: "post",
            dataType: 'json',
            contentType: "application/json",
            success: function (responseData) {
                showFeedback();
                fetchAllSelectedResources(renderSelectedResourcesBlock);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Ajaz Error While Saving Professions", jqXHR, textStatus, errorThrown);
            }
        });
    }

    function activateSlimScroll(id, height) {
        $("#" + id).slimScroll({
            height: height,
            size: '8px',
            position: 'right',
            color: '#666',
            alwaysVisible: true,
            distance: '0px',
            railVisible: true,
            railColor: '#DDD',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: true
        });
    }
    function fetchAllResources(callback) {
        $.ajax({
            url: __NVLI.context + "/api/allResources",
            type: 'GET',
            dataType: 'JSON',
            contentType: 'application/json',
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function checkSelected() {
        _.each(__NVLI.selectedResource, function (res) {
            var elems = $(".resource-item[data-resource-id='" + res.resourceId + "']");
            _.each(elems, function (el) {
                $(el).addClass('active');
            });
        });
    }
    $(document).ready(function () {
        console.log("dfsf");
        fetchAllSelectedResources(renderSelectedResourcesBlock);
        activateSlimScroll("selected-box", "550px");
        activateSlimScroll("lang-tab-content-block", "550px");
    });

</script>