<%-- 
    Document   : admin-manage-themes
    Created on : Jul 11, 2016, 6:39:26 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
    Author     : Ankita Dhongde <dankita@cdac.in>
    Author     : Savita Kakad
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request"/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="${context}/components/moment/moment.js"></script>
<style>
    ul.pagination{
        display: inline-block;
        clear: both;
        padding: 4px;
    }
    ul.pagination li {
        list-style: none;
        float: left;
        padding: 8px 15px;
        border: 1px solid #ddd;
        border-radius: 4px;
        cursor: pointer;
        background: #fff;
        margin: 0 2px;
        line-height: 15px;
        text-align: center;
        vertical-align: middle;
    }
    ul.pagination li:hover{
        /*font-weight: bold;*/
        background: #eee;
    }
    ul.pagination li.active {

        background: #0048AB;
    }
    ul.pagination li.active{
        color: #fff;
        font-weight: bold;
    }
    ul.pagination li.disabled{
        cursor: not-allowed;
        background: #ddd;
    }
    .perPage{
        width:300px;
        float: right;
    }
    .pageInfo{
        float: right;
        padding: 10px;
        line-height: 20px;
    }
    .sort-by-th{
        cursor: pointer;
    }

    .capitalize {
        text-transform: capitalize;
    }
    .font-bold{
        font-weight: bold;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-5"><i class="fa fa-image"></i> <tags:message code="theme.manage"/>
                    </div>
                    <div class="col-xs-12 col-sm-7 text-right-cust m-a-0">
                        <a class="btn btn-sm btn-blue"  href="${context}/widgets/themeCalWidget"><span class="fa fa-calendar"></span> <tags:message code="label.theme.widget"/></a>&nbsp;
                        <a data-toggle="modal" data-target="#uploadThemeBlock" class="btn btn-sm btn-blue"><i class="fa fa-upload"></i> <tags:message code="label.theme.install"/></a>
                    </div>
                </div>
            </div>
            <div class="">
                <div id="theme-list-container" class="themeListContainer"></div>
                 <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- Theme Modal -->
<div class="modal fade" id="uploadThemeBlock" tabindex="-1" role="dialog" aria-labelledby="installthemeFileLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-upload"></i> <tags:message code="theme.install"/>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="frm-upload-theme">
                <div class="modal-body">
                    <div class="container">
                        <div  style="padding: 50px auto;">
                            <div class="form-group">
                                <label for="" class="control-label"><strong></strong></label>
                                <input id="uploadfile" type="file" name="uploadfile" class="form-control" accept="application/x-zip-compressed" required="">
                                <p class="help-block text-muted">
                                    <tags:message code="theme.upload"/>
                                </p>
                            </div>
                            <output id="output"></output>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="button.close"/></button>
                    <input type="submit" value="Install Theme" class ="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>              
 <!-- Replace theme Bloc -->
 
 <div class="modal fade" id="replaceThemeBlock" tabindex="-1" role="dialog" aria-labelledby="replaceThemeFileLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <i class="fa fa-upload"></i> <tags:message code="theme.replace"/>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="frm-replace-theme">
                <div class="modal-body">
                    <div class="container">
                        <div  style="padding: 50px auto;">
                            <div class="form-group">
                                <label for="" class="control-label"><strong></strong></label>
                                <input id="replacefile" type="file" name="replacefile" class="form-control" accept="application/x-zip-compressed" required="">
                                <p class="help-block text-muted">
                                    <tags:message code="theme.upload"/>
                                </p>
                            </div>
                            <output id="output1"></output>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><tags:message code="button.close"/></button>
                    <input type="submit" id="replace" value="Replace Theme"  class ="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>              
 
                    
<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Parmanently</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure about this ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><tags:message code="button.cancel"/></button>
                <button type="button" class="btn btn-danger" id="confirm"><tags:message code="button.delete"/></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="configureTheme" role="dialog" aria-labelledby="configureTheme" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">configure Theme</h4>
            </div>
            <form id ="frm-configure-theme">
            <div class="modal-body" style="padding: 30px;">
                <div class="form-group form-horizontal">
                    <label class="mr-sm-2 control-label font-bold" for="theme-name"><tags:message code="theme.name"/></label>
                    <input type="text" id="theme-name" value="" class="form-control" required></input> 
                </div>
                <div class="form-group">
                    <label class="mr-sm-2 control-label font-bold" for="theme-desc"><tags:message code="theme.description"/></label>
                    <textarea id="theme-desc" value="" class="form-control" cols="" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <label class="mr-sm-2 control-label font-bold" for="inlineFormCustomThemeSelect" ><tags:message code="theme.type"/></label>
                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0 form-control" id="inlineFormCustomThemeSelect" required>
                        <option selected>Choose...</option>
                        <option value="global">Global</option>
                        <option value="personal">Personal</option>
                    </select>
                </div>
                <div id="date-container"></div>
                <p id="date-status" style="color: red; display: none;"></p>
                <div class="form-check form-check-inline" id="check-repeatable-theme">
                    <label class="form-check-label">
                        <tags:message code="theme.reapetable"/> <input class="form-check-input" type="checkbox" id="theme-repeat" value="false"> 
                    </label>
                </div>
            </div>   
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><tags:message code="button.cancel"/></button>
                <button type="button" class="btn btn-primary" id="btnUpdateTheme"><tags:message code="button.update"/></button>
            </div>
            </form>
        </div>
    </div>
</div>                                

<script type="text/template" id="tpl-date-field">
    <div class="form-group" id="theme-date-input-block">
    <label for="themeDateField">Select Date</label>
    <input type="text" name="themeDateField" id="themeDateField" size="12"  value="" data-datepick="showOtherMonths: true, firstDay: 1, dateFormat: 'yyyy-mm-dd', minDate: 'new Date()'">

    <p class="help-block">Please enter the date in which this theme should automatically applied globally.</p>
    <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
    </div>
</script>

<script type="text/template" id="tpl-theme-row">
<div data-theme-code="{{=themeCode}}">
 <div class="row m-a-0 p-b-1 p-t-1 border_t">
    
        <div class="col-xl-6 col-md-12 col-xs-12 ">   
             <img class="theme-image-preview pull-left" style="cursor:pointer" src="{{=thumb}}" 
               width="150" onerror="this.src='/project-nvli/images/icons/dummy.png'">   

            <div class="p-l-1 pull-left themeDesc">    
                <h3 class="m-b-1">{{=themeName}}</h3>
                <h5>{{=themeDescription}}</h5>
                {{ if(themeType === 'global'){ }} 
                <h6 class="m-b-1">Getting Activated on <b>{{=(new Date(themeActivationDate)).toDateString()}}</b></h6>
                {{ } }}
                <h6 class="m-b-1"> {{  if(themeRepeatable === true){ }} <span class="fa fa-repeat"></span> <tags:message code="theme.repeatYearly"/> {{ } }}</h6>
               <div class="capitalize btn btn-sm {{ if(themeType === 'personal'){ }} btn-primary-outline {{ } else { }} btn-success-outline {{ } }}" >{{=themeType}} Theme
               </div>
              
            </div>
        </div>
    
        <div class="col-xl-6 text-right-cust m-a-0 col-md-12 col-xs-12 themebuttons">      
            <a href="#" class="btn btn-sm btn-primary-outline" title="Configure" data-theme-repeatable={{=themeRepeatable}} data-theme-type="{{=themeType}}" data-theme-id="{{=id}}" data-theme-name="{{=themeName}}" data-theme-date="{{=themeActivationDate}}" data-theme-desc="{{=themeDescription}}" data-tooltip="Configure" data-toggle="modal" data-target="#configureTheme" ><i class="fa fa-gear"></i> Configure</a>
            {{ if(themeCode !== "default"){  }}
            <a href="#" class="btn btn-sm btn-danger-outline" title="Remove" data-theme-id="{{=id}}" data-tooltip="Remove" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Theme" data-message="Are you sure you want to remove this theme ?"><i class="fa fa-remove"></i> Delete</a>
            {{ } }}
            <a href="#" class="btn btn-sm btn-info-outline" title="Replace" data-theme-id="{{=id}}" data-tooltip="Replace" data-toggle="modal" data-target="#replaceThemeBlock" data-title="Replace Theme" data-message="Are you sure you want to replace this theme ?"><i class="fa fa-refresh"></i> Replace</a>
            <button class="btn btn-sm btn-warning-outline btn-theme-preview" data-theme-id="{{=id}}" >Preview</button>
        </div>
   </div>
  </div>
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

    var themeActivatedDate = null;
    var currentThemeid = ""; 
    
    function showMessage(message) {
        $("#alert-box").html(message);
        $("#alert-box").show();
    }

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object

        // files is a FileList of File objects. List some properties.
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
                    f.size, ' bytes, last modified: ',
                    f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                    '</li>');
        }
        document.getElementById('output').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    function fetchAllThemes() {
        $("#theme-list-container").empty();
        $.ajax({
            url: __NVLI.context + "/admin/get/themes",
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                _.each(data, function (theme, i) {
                    var tpl = _.template($("#tpl-theme-row").html());
                    theme['serial'] = i + 1;
                    theme['thumb'] = __NVLI.context + "/uploadedThemes/" + theme.themeCode + "_theme/images/" + theme.themeCode + "_theme_thumbnail.png";
                    var row = tpl(theme);
                    $("#theme-list-container").append(row);
                    
                });
                $(".btn-theme-preview").bind("click", function (e) {
                    showToastMessage("preview activated","info");
                    getThemeById(e.currentTarget.getAttribute('data-theme-id'), activateThemePreview, applyTheme);
                });
            }
        });
    }

    function globalThemeDateValidation(event) {
        var themeId = event.currentTarget.getAttribute('data-theme-id');
        var themeDate = event.currentTarget.getAttribute('data-theme-date');
        var themeType = event.currentTarget.getAttribute('data-theme-type');
        if(themeDate=== null ) {
            themeDate = $("#themeDateField").val();
        }
        if(themeType=== null ) {
            themeType=$("#inlineFormCustomThemeSelect").val();
        }
        
        $.ajax({
            url: __NVLI.context + "/admin/checkGlobalThemeDate",
            type: 'POST',
            data: {themeDate: themeDate,themeId: themeId},
            enctype: 'json',
            success: function (response) {
                if (response.status === "failed") {
                    $("#date-status").html('<div class="alert alert-danger">A global theme is already registered on this date. Please choose different date.</div>').fadeIn();
                } else {
                    let themeName = $("#theme-name").val();
                    let themeDesc = $("#theme-desc").val();
                    let themeRepeat = $('#theme-repeat').val();
                    updateTheme(themeId, themeType, themeDate, themeName,themeDesc,themeRepeat);
                    $('#configureTheme').modal('hide');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("Error while checking dates", "danger");
            }
        });
    }

    function uploadNewTheme(formData) {
        $.ajax({
            url: __NVLI.context + "/admin/themeFileUpload",
            type: 'POST',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                if (undefined !== response.status && response.status === 'Failed') {
                    showToastMessage("Theme could not be uploaded. " + response.message, "error");
                    $("#uploadThemeBlock").modal('hide');
                } else {
                    $("#frm-upload-theme")[0].reset();
                    showToastMessage("Theme Uploaded Successfully", "success");
                    document.getElementById('output').innerHTML = "";
                    $("#uploadThemeBlock").modal('hide');
                    document.getElementById('output').innerHTML = "";
                    fetchAllThemes();
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("Error while uploading theme", "danger");
            }
        });
    }
    
    function replaceTheme(formData, themeId) {
        $.ajax({
            url: __NVLI.context + "/admin/themeFileReplace?themeId="+themeId,
            type: 'POST',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (response) {
                if ( response.status === 'Failed') {
                    showToastMessage("Theme could not be uploaded. " + response.message, "error");
                    $("#replaceThemeBlock").modal('hide');
                }  
                else if ( response.status === 'success') {
                    $("#frm-replace-theme")[0].reset();
                    showToastMessage("Theme Replaced Successfully", "success");
                    $("#replaceThemeBlock").modal('hide');
                    
                    fetchAllThemes();
                } 
                else {
                    showToastMessage(response.message, "error");
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("Error while uploading theme", "danger");
            }
        });
    }
    
    function updateTheme(themeId, themeType, themeDate, themeName, themeDesc,themeRepeat) {
        
        if (themeId === null) {
            showMessage('Error');
            return false;
        }

        if (themeType === null) {
            showMessage('theme type is blank');
            return false;
        }

        if (themeDate === null) {
            showMessage('theme date is null');
            return false;
        }

        $.ajax({
            url: __NVLI.context + "/admin/configureThemeSettings",
            type: 'POST',
            data: {themeId: themeId, themeType: themeType, themeDate: themeDate ,themeName:themeName,themeDesc:themeDesc,themeRepeat:themeRepeat},
            enctype: 'json',
            success: function (response) {
                if (response.status === 'success') {
                    $("#frm-upload-theme")[0].reset();
                    $("#frm-configure-theme")[0].reset();
                    $("#theme-date-input-block").hide();
                    $("#check-repeatable-theme").hide();
                    $('#configureTheme').modal('hide');
                    showToastMessage("Theme updated Successfully", "success");
                    fetchAllThemes();
                    getPersonalThemes();
                } else {
                    showToastMessage("Theme could not be updated. " + response.message, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showToastMessage("Error while updating theme", "danger");
            }
        });
    }


    $(document).ready(function () {
        $("#date-status").hide();
        $("#check-repeatable-theme").hide();
        fetchAllThemes();
        document.getElementById('uploadfile').addEventListener('change', handleFileSelect, false);
        $("#frm-upload-theme").bind("submit", function (e) {
            e.preventDefault();
            // TODO : show upload progress bar.
            var formData = new FormData($("#frm-upload-theme")[0]);
            uploadNewTheme(formData);
        });
        
        
         $("#frm-replace-theme").bind("submit", function (e) {
            e.preventDefault();
            themeid = $(this).find('.modal-footer #replace')[0].getAttribute('data-theme-id');
            var formData = new FormData($("#frm-replace-theme")[0]);
            replaceTheme(formData,themeid);
        });
        
        $('#replaceThemeBlock').on('show.bs.modal', function (e) {
            var themeId = e.relatedTarget.getAttribute('data-theme-id');
            $(this).find('.modal-footer #replace')[0].setAttribute('data-theme-id', e.relatedTarget.getAttribute('data-theme-id'));
        });
       
        $("#theme-repeat").on('change', function() {
          if ($(this).is(':checked')) {
            $(this).attr('value', 'true');
          } else {
            $(this).attr('value', 'false');
          }
        });

        $('#confirmDelete').on('show.bs.modal', function (e) {

            $message = $(e.relatedTarget).attr('data-message');
            $(this).find('.modal-body p').text($message);
            $title = $(e.relatedTarget).attr('data-title');
            $(this).find('.modal-title').text($title);
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm')[0].setAttribute('data-theme-id', e.relatedTarget.getAttribute('data-theme-id'));
            $(this).find('.modal-footer #confirm').data('form', form);
        });

        $("#confirmDelete").find('.modal-footer #confirm').on('click', function (event) {
            var themeId = event.currentTarget.getAttribute('data-theme-id');
            $.ajax({
                url: __NVLI.context + "/admin/deleteTheme",
                type: 'POST',
                data: {themeId: themeId},
                dataType: 'json',
                success: function (response) {
                    if (response.statusCode) {
                        $('#confirmDelete').modal('hide');
                        showToastMessage("Theme Deleted Sucessfully", "success");
                        fetchAllThemes();
                        location.reload();
                    } else {
                        showToastMessage("something went wrong", "warning");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    showToastMessage("Error while deleteing theme", "danger");
                }
            });
        });
        $('#configureTheme').on('show.bs.modal', function (e) {
            var themeId = e.relatedTarget.getAttribute('data-theme-id');
            var themeName =  e.relatedTarget.getAttribute('data-theme-name');
            var themeDesc =  e.relatedTarget.getAttribute('data-theme-desc');
            var themeType = e.relatedTarget.getAttribute('data-theme-type');
            var themeRepeatable = e.relatedTarget.getAttribute('data-theme-repeatable');
            $("#theme-name").val(themeName);
            $("#theme-desc").val(e.relatedTarget.getAttribute('data-theme-desc'));
            $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-id', themeId);
            $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-name', themeName);
            $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-desc', themeDesc);
            console.log("Getting Activated on",e.relatedTarget.getAttribute('data-theme-date') );
            themeActivatedDate = e.relatedTarget.getAttribute('data-theme-date');
            if(themeType === "global") {
                var sel  = document.getElementById("inlineFormCustomThemeSelect");
                for(var i = 0, j = sel.options.length; i < j; ++i) {
                    if(sel.options[i].value === themeType) {
                       sel.selectedIndex = i;
                       $(sel).trigger("change");
                       $("#themeDateField").val(e.relatedTarget.getAttribute('data-theme-date'));
                       break;
                    }
                }
                if(themeRepeatable === "true"){
                    $("#theme-repeat").attr("checked","checked");
                }
            }
            if(themeType === "personal") {
                $("#inlineFormCustomThemeSelect").val(themeType);
                $("#theme-date-input-block").hide();
                $("#check-repeatable-theme").hide();
            }
        });

        $("#configureTheme").find('.modal-body #inlineFormCustomThemeSelect').on('change', function (event) {
            var themeType = event.currentTarget.options[event.currentTarget.selectedIndex].value;
           
            if (themeType === "global") { 
                $("#themeDateField").datepicker( "setDate" ,"2017-06-27" );
                var dateTpl = _.template($("#tpl-date-field").html());
                var el = dateTpl({});
                $("#date-container").empty();
                $("#configureTheme").find('.modal-body #date-container').append(el);
                var onSelectDate = function () {
                    var themeDate = $("#themeDateField").val();
                    $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-date', themeDate);
                    $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-type', themeType);
                    
                };
                $("#themeDateField").datepick({onSelect: onSelectDate});
                $("#check-repeatable-theme").show();
            }
            if (themeType === "personal") {
                var themeDate = new Date();
                $("#theme-date-input-block").hide();
                $("#check-repeatable-theme").hide();
                $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-date', themeDate.getFullYear() + "-" + themeDate.getMonth() + "-" + themeDate.getDate());
                $("#configureTheme").find('.modal-footer #btnUpdateTheme')[0].setAttribute('data-theme-type', themeType);
                
            }
        });

        $("#configureTheme").find('.modal-footer #btnUpdateTheme').on('click', function (event) {
            event.preventDefault();
            let themeName = $("#theme-name").val();
            let themeDesc = $("#theme-desc").val();
            var themeId = event.currentTarget.getAttribute('data-theme-id');
            var themeType = event.currentTarget.getAttribute('data-theme-type');
            if($("#inlineFormCustomThemeSelect").val() === "global") {
                 var themeDate = event.currentTarget.getAttribute('data-theme-date');
               // globalThemeDateValidation(event);
                    let themeRepeat = $('#theme-repeat').val();
                    updateTheme(themeId, themeType, themeDate, themeName,themeDesc,themeRepeat);
                    $('#configureTheme').modal('hide');
            } 
            
            if($("#inlineFormCustomThemeSelect").val() === "personal") {
                var themeDate = moment().format('YYYY-MM-DD');
                updateTheme(event.currentTarget.getAttribute('data-theme-id'),$("#inlineFormCustomThemeSelect").val(), themeDate, themeName,themeDesc,false);
            }
        });
    });
</script>
