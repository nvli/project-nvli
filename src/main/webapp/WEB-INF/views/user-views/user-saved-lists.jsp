<%--
    Document   : user-saved-lists
    Created on : Jun 6, 2016, 11:24:03 AM
    Author     : vootla
    Author     : Ruturaj Powar<ruturajp@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div id="add_to_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="addToListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="add_to_list_link-title"><tags:message code="dashboard.add.to.list"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix" id="add_to_list_link-body">

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<div id="edit_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="editListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="edit_list_link-title">Update</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="listIdHidden"/>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="label.list.name"/></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" id="listNameTxt" class="form-control"/>
                    </div>
                </div>             
            </div>
            <div class="modal-footer">
                <input type="submit" id="updateListBtn" class="btn btn-primary" value="Update"/>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><span class="fa fa-list"></span> <tags:message code="dashboard.saved.list"/></div>
        <div class="card-block" style="display: flow-root;">
            <div id="dashboard-fav-libs" class="row">
                <div class="slimScrollDiv" >
                    <div id="saved-lists" class="grid"> </div>
                </div>
            </div>
        </div>
    </div>
</div>
  
 <script type="text/template" id="tpl-saved-list">
    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="mylist-container grid-item card m-t-1 public_list_item">
            <div class="card-header">
                <h4 class="mylist-header-title text-uppercase">{{=listTitle}}</h4>
                <h6 class="mylist-header-author m-a-0 text-right-cust">
                    <span>&mdash; {{=authorName}}</span>
                    <img src="${exposedProps.userImageBase}/{{=profilePicUrl}}"
                        class="img img-circle" onerror="this.src='${context}/images/profile-picture.png'"
                        width="24" height="24">
                </h6>
            </div>
            <div class="mylist-body card-block">
                <ol class="p-l-2">
                {{ _.each(listItems, function(item){  }}
                <li class='favoriteList' id='mylistcontents_{{= item.itemUrl}}'>
                   <p><span><a href="${context}/search/preview/{{=item.itemUrl}}">  {{=item.itemName}} </a></p>
                 </li>
                {{ }); }}                           
                </ol>
            </div>           
            <div class="mylist-footer card-footer text-center">  
            <button class="btn btn-primary btn-sm" onclick="editMyList('{{=listTitle}}');"><i class="fa fa-pencil-square-o"></i> Edit</button>
            <button class="btn btn-primary btn-sm" onclick="deleteMyList(this);" id="delete_{{=listTitle}}"><i class="fa fa-trash"></i> Delete</button>                 
            </div>
        </div>
    </div>
</script>

<script type="text/javascript">
    var pageNumber;
    var pageWindow = 8;
    var processing = false;
    var hasNext = true;
    function populateLists() {
        $.ajax({
            url: __NVLI.context + "/search/user/lists/results/" + pageNumber + "/" + pageWindow,
            dataType: 'json',
            type: 'GET',
            success: function (jsonObj) {
                   if (jsonObj.savedLists.length === 0) {
                    if (pageNumber == 1)
                        $("#saved-lists").append("<center>No Saved Lists Found!</center>");
                    hasNext = false;
                    return false;
                } else {
                    _.each(jsonObj.savedLists, function (list) {             
                        var template = _.template($("#tpl-saved-list").html());
                        var opts = {
                            "listTitle": list.listTitle,
                            "authorId":list.authorId,
                            "authorName": list.authorName,
                            "profilePicUrl": list.profilePicUrl,
                            "listItems": list.itemArray,
                            "followersCount":list.followersCount
                        };
                        var el = template(opts);
                        $("#saved-lists").append(el);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    $(document).ready(function () {
        startup();
    });
    function startup() {
        pageNumber = 1;
        populateLists();
        $(window).scroll(function (e) {
            if (processing)
                return false;
            if (hasNext && $(window).scrollTop() >= ($("#saved-lists").height() - $("#saved-lists").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch notifications
                populateLists();
                processing = false;
            }
        });
    }
    function showMyList(list){
        $('#add_to_list_link').modal('show');
        $('#add_to_list_link-title').html(list.title);
        var data = {}
        data["query"] = $("#query").val();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: __NVLI.context + "/user/mylists/" + list.title+"/"+0,
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 100000,
            success: function (data) {              
                var json = "";
                {
                    for (content of data) {                    
                        json = json + "<div class='favoriteList' id='mylistcontents_" + content.link + "'><p><span><a href='${context}/search/preview/" + content.link + "'> " + content.title + " </a></span><span class='pull-right'><a href='#' onclick='deleteMyListContents(this);' id='" + list.title + "-split-" + content.link + "-split-" + content.title + "'><i class='fa fa-trash' aria-hidden='true' style='color:#000;'></i></a></span></p></div>";
                    }
                }

                $('#add_to_list_link-body').html('<div class="card-block"> <div id="list-container-contents">' + json + "</div></div>");
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }

    function showMyListAfterContentDeleted(listTitle){       
        $('#add_to_list_link-title').html(listTitle);
        var data = {}
        data["query"] = $("#query").val();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: __NVLI.context + "/user/mylists/" + listTitle+"/"+0,
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 100000,
            success: function (data) {             
                var json = "";
                for (content of data) {                   
                    json = json + "<div class='favoriteList' id='mylistcontents_" + content.link + "'><p><span><a href='${context}/search/preview/" + content.link + "'> " + content.title + " </a></span><span class='pull-right'><a href='#' onclick='deleteMyListContents(this);' id='" + listTitle + "-split-" + content.link + "-split-" + content.title + "'><i class='fa fa-trash' aria-hidden='true' style='color:#000;'></i></a></span></p></div>";
                }              
                $('#add_to_list_link-body').html('<div class="card-block"> <div id="list-container-contents">' + json + "</div></div>");            
            },
            error: function (e) {
                console.log("ERROR: ", e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }


    function deleteMyList(list) {
        if (confirm("Do you want to Delete a List"))
        {
            var data = {}
            data["query"] = $("#query").val();
            $.ajax({
                type: "DELETE",
                contentType: "application/json",
                url: __NVLI.context + "/user/deleteMylists/" + list.id.split("delete_")[1],
                data: data,
                timeout: 100000,
                success: function (data) {
                    if (data === 1)
                    {
                        showToastMessage("List Deleted Successfully...", "success");
                        $('#fav_' + list.id.split("delete_")[1]).remove();
                        if ($('#list-container').find('.favoriteList').length === 0)
                        {
                            $("#list-container").empty();
                            $("#list-container").append("<center>no List's Found!</center>");
                            $("#list-container_show_all").hide();
                        }
                        $("#saved-lists").empty();
                        startup();

                    } else
                    {
                        showToastMessage("Error while Deleting....", "error");
                    }

                },
                error: function (e) {
                    console.log("error");
                    console.log("ERROR: ", e);
                    display(e);
                },
                done: function (e) {
                    console.log("DONE");
                }
            });
        }
    }

    function deleteMyListContents(list) {
        if (confirm("Do you want to Delete a List"))
        {
            var data = {}
            data["query"] = $("#query").val();
            $.ajax({          
                contentType: "application/json",
                url: __NVLI.context + "/user/dMyListContent/" + list.id.split("-split-")[0] + "/" + list.id.split("-split-")[1] + "/" + list.id.split("-split-")[2],
                data: data,
                timeout: 100000,
                success: function (data) {
                    if (data === 1) {
                        showToastMessage("List Content Deleted Successfully...", "success");                     
                        showMyListAfterContentDeleted((list.id.split("-split-")[0]));
                    } else {
                        showToastMessage("Error while Deleting....", "error");
                    }
                },
                error: function (e) {
                    console.log("error");
                    console.log("ERROR: ", e);           
                },
                done: function (e) {
                    console.log("DONE");
                }
            });
        }
    }

    function editMyList(listname){
        var radioBtnVal;
        $("input[name=accessRadio]").attr('checked', false);
        $('#edit_list_link').modal('show');
        $('#listNameTxt').val(listname);
        $('#listIdHidden').val(listname);
    }


    $("#updateListBtn").click(function () {
        $.ajax({
            url: __NVLI.context + "/search/user/update/list/" + $('#listIdHidden').val() + "/" + $('#listNameTxt').val(),
            type: 'GET',
            success: function (res) {
                if (res == 2)
                {
                    showToastMessage("This List Name already Exists.Please choose another name.", "info");
                } else {
                    showToastMessage("List Updated", "success");
                    $('#edit_list_link').modal('hide');
                     $("#saved-lists").empty();
                    startup();
                }
               

            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    });
</script>