<%-- 
    Document   : user-profile-settings
    Created on : Jun 10, 2016, 4:56:06 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-wrench"></i><tags:message code="profile.settings.heading"/></div>
        <div class="tabs" style="margin-top: 0;">
            <ul id="myTab" class="nav nav-tabs font-weight-bold">
                <li class="nav-item"> <a data-toggle="tab" data-target="#tab1" class="nav-link active"><tags:message code="profile.setting.profession"/></a> </li>
                <li class="nav-item"> <a data-target="#tab3" data-toggle="tab" class="nav-link"><tags:message code="profile.setting.interest"/></a> </li>
                <li class="nav-item"> <a data-target="#tab4" data-toggle="tab" class="nav-link"><tags:message code="profile.setting.language"/></a> </li>
            </ul>
            <div class="tab-content">
                <div id="tab1" class="tab active">
                    <div class="col-sm-8 col-md-8 col-lg-9">
                        <div class="card">
                            <div class="card-header"><tags:message code="profile.setting.select.profession"/></div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">
                                        <form>
                                            <fieldset class="form-group">
                                                <label for="professionSelect"><strong>What is your Profession?</strong></label>
                                                <select class="form-control" id="professionSelect" onchange="profSelectCheck(this);">
                                                    <option>Select Option</option>
                                                    <option value="1" id="employed_1">Employed</option>
                                                    <option value="2" id="employed_2">Self Employed</option>
                                                </select>
                                            </fieldset>
                                            <div id="employed" style="display:none;"> 
                                                <fieldset class="form-group">
                                                    <label for="exampleSelect2"><strong>Select Core Function Area</strong></label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Sales & Marketing
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="open_engg" for="engineering">
                                                            <input type="checkbox" id="engineering"><span class="open_engg"> Engineering (Technology) </span>
                                                        </label>
                                                        <div id="engineering_select" class="col-lg-offset-1 card-block" style="display: none;">  
                                                            <select class="form-control col-lg-4 col-md-4">
                                                                <option>Select Option</option>
                                                                <option value="1">IT - Intern</option>
                                                                <option value="2">Computer Science Engineering</option>
                                                                <option value="3">Chemical Engineering</option>
                                                                <option value="4">Electrical Engineering</option>
                                                                <option value="5">Mechatronics Engineering</option>
                                                                <option value="5">Mechanical Engineering</option>
                                                                <option value="5">Electronics Engineering</option>
                                                            </select>
                                                        </div>    
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> HR
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Consulting
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Analytics
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Operations
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Finance
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Teaching & Training
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Project Management
                                                        </label>
                                                    </div>  
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Product Management
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Strategy
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Design / Art
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Media & Mass Communucation
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Government Service
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Research
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Social Work / Politics
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Legal
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox"> Performing
                                                        </label>
                                                    </div> 
                                                    <div class="checkbox">
                                                        <label class="othergroup">
                                                            <input type="checkbox"><span class="othergroup"> Other </span>
                                                        </label>
                                                        <div id="other_field" class="col-lg-offset-1 card-block" style="display: none;">  
                                                            <label for="other" class="col-sm-3 form-control-label">Other Profession</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control" id="other" placeholder="Other Profession">
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </fieldset>  
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>

                                        </form>
                                    </div>    
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <div class="card">
                            <div class="card-header">Your Profession</div>
                            <div class="card-block">
                                <form class="form">
                                    <div class="form-group row prof_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">Engineering - Computer Science</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteProfession"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab3" class="tab">
                    <div class="col-sm-8 col-md-8 col-lg-9">
                        <div class="card">
                            <div class="card-header">Select your interest(s)</div>
                            <div class="card-block">

                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-secondary">All</button>
                                    <button type="button" class="btn btn-secondary">A</button>
                                    <button type="button" class="btn btn-secondary">B</button>
                                    <button type="button" class="btn btn-secondary">C</button>
                                    <button type="button" class="btn btn-secondary">D</button>
                                    <button type="button" class="btn btn-secondary">E</button>
                                    <button type="button" class="btn btn-secondary">F</button>
                                    <button type="button" class="btn btn-secondary">G</button>
                                    <button type="button" class="btn btn-secondary">H</button>
                                    <button type="button" class="btn btn-secondary">I</button>
                                    <button type="button" class="btn btn-secondary">J</button>
                                    <button type="button" class="btn btn-secondary">K</button>
                                    <button type="button" class="btn btn-secondary">L</button>
                                    <button type="button" class="btn btn-secondary">M</button>
                                    <button type="button" class="btn btn-secondary">N</button>
                                    <button type="button" class="btn btn-secondary">O</button>
                                    <button type="button" class="btn btn-secondary">P</button>
                                    <button type="button" class="btn btn-secondary">Q</button>
                                    <button type="button" class="btn btn-secondary">R</button>
                                    <button type="button" class="btn btn-secondary">S</button>
                                    <button type="button" class="btn btn-secondary">T</button>
                                    <button type="button" class="btn btn-secondary">U</button>
                                    <button type="button" class="btn btn-secondary">V</button>
                                    <button type="button" class="btn btn-secondary">W</button>
                                    <button type="button" class="btn btn-secondary">X</button>
                                    <button type="button" class="btn btn-secondary">Y</button>
                                    <button type="button" class="btn btn-secondary">Z</button>
                                </div>
                                <div class="row" align="center">

                                    <div class="btn-group" data-toggle="buttons">
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;">
                                                <input type="checkbox" autocomplete="off" > Interest field 1
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 2
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 3
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 4
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 5
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 6
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 7
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 8
                                            </label>
                                        </div>

                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 9
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 10
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 11
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 12
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 13
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 14
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 15
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 16
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 17
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 18
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 19
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Interest field 20
                                            </label>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <div class="card">
                            <div class="card-header">Interested in</div>
                            <div class="card-block">
                                <form class="form">
                                    <div class="form-group row interest_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">1.interested field1</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteInterest"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                    <div class="form-group row interest_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">2.interested field2</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteInterest"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                    <div class="form-group row interest_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">3.interested field3</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteInterest"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tab4" class="tab">
                    <div class="col-sm-8 col-md-8 col-lg-9">
                        <div class="card">
                            <div class="card-header">Select Language(s) Known</div>
                            <div class="card-block">
                                <div class="row" align="center">

                                    <div class="btn-group" data-toggle="buttons" id="jas">
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;">
                                                <input type="checkbox" autocomplete="off" > Assamese
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Bengali
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Bodo
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Dogri
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > English
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Gujarati
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Hindi
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Kannada
                                            </label>
                                        </div>

                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Kashmiri
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Konkani
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Maithili
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Malayalam
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Manipuri
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Marathi
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Nepali
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Odia
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Punjabi
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Sanskrit
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Santali
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Sindhi
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Tamil
                                            </label>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input type="checkbox" autocomplete="off" > Telugu
                                            </label>
                                        </div> 
                                        <div class="col-lg-3">
                                            <label class="btn btn-secondary btn-lg btn-block" style="margin:5px 0px 5px 0px;" >
                                                <input class="checkbox1" type="checkbox" autocomplete="off" > Urdu
                                            </label>
                                        </div>

                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
                        <div class="card">
                            <div class="card-header">Languages Known</div>
                            <div class="card-block">
                                <form class="form">
                                    <div class="form-group row language_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">1.Marathi</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteLanguage"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                    <div class="form-group row language_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">2.Hindi</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteLanguage"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                    <div class="form-group row language_delet">
                                        <label class="control-label col-xs-9" for="inputEmail">3.English</label>
                                        <div class="col-xs-3">
                                            <input type="submit" class="btn btn-secondary deleteLanguage"  value="Delete" style="margin:0 0 0 5px;">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>

    </div> 
</div>    

<script>
    $(document).ready(function () {
        $('#file').awesomeCropper({width: 200, height: 200, debug: true});
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        //min font size
        var min = 11;

        //max font size
        var max = 16;

        //grab the default font size
        var reset = $('body').css('fontSize');

        //font resize these elements
        var elm = $('body');

        //set the default font size and remove px from the value
        var size = str_replace(reset, 'px', '');

        //Increase font size
        $('a.fontSizePlus').click(function () {

            //if the font size is lower or equal than the max value
            if (size <= max) {

                //increase the size
                size++;

                //set the font size
                elm.css({'fontSize': size});
            }

            //cancel a click event
            return false;

        });

        $('a.fontSizeMinus').click(function () {
            //if the font size is greater or equal than min value
            if (size >= min) {
                //decrease the size
                size--;

                //set the font size
                elm.css({'fontSize': size});
            }

            //cancel a click event
            return false;

        });

        //Reset the font size
        $('a.fontReset').click(function () {

            //set the default font size
            elm.css({'fontSize': reset});
        });
        /* $('.panel-heading a').on('click',function(e){
         if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
         e.stopPropagation();
         }*/
        /*******Chiatanya JS **************/
        /*$('#personal_information').click(function () {
         $('#image_change').css('display','block');
         });*/

        jQuery('.tabs .tab-links a').on('click', function (e) {
            var currentAttrValue = jQuery(this).attr('href');

            // Show/Hide Tabs
            jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

            // Change/remove current tab to active
            jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });
        /************Chaitanya JS end************/
    });

    //A string replace function
    function str_replace(haystack, needle, replacement) {
        var temp = haystack.split(needle);
        return temp.join(replacement);
    }


</script>
<script>
    $(document).ready(function () {

        $(".deleteProfession").on("click", function () {
            var div = $(this).closest('div.prof_delet');
            div.fadeOut(400, function () {
                div.remove();
            });
            return false;
        });
        $(".deleteLanguage").on("click", function () {
            var div = $(this).closest('div.language_delet');
            div.fadeOut(400, function () {
                div.remove();
            });
            return false;
        });
        $(".deleteInterest").on("click", function () {
            var div = $(this).closest('div.interest_delet');
            div.fadeOut(400, function () {
                div.remove();
            });
            return false;
        });

        $(".open_engg").click(function () {
            $("#engineering_select").toggle();
        });
        $(".othergroup").click(function () {
            $("#other_field").toggle();
        });

    });
</script>
<script>
    function profSelectCheck(nameSelect)
    {
        if (nameSelect) {
            admOptionValue = document.getElementById("employed_1").value;
            if (admOptionValue == nameSelect.value) {
                document.getElementById("employed").style.display = "block";
            }
            else {
                document.getElementById("employed").style.display = "none";
            }
        }
        else {
            document.getElementById("employed").style.display = "none";
        }
    }
</script>

<script type="text/javascript">


    //WOW Scroll Spy
    var wow = new WOW({
        //disabled for mobile
        mobile: false
    });
    wow.init();
</script>