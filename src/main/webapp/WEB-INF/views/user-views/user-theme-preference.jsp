<%--
    Document   : user-theme-preference
    Created on : Jul 11, 2016, 10:52:55 AM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request"/>
<style>
    .theme-item{
        border: 2px solid #efefef;
        padding-top: 20px;
        padding-bottom: 20px;
        margin-bottom: 20px;
    }
    .theme-item>label{
        cursor: pointer;
    }
    .theme-item.active{
        border: 2px solid #8BC34A;
        background: rgba(255, 152, 0, 0.12);

    }
    .theme-item:hover{
        border: 2px solid #ddd;
        background: rgba(255, 152, 0, 0.12);
    }

</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
<!--    <div class="col-md-12">-->
        <div class="card">
            <div class="card-header">
                <i class="fa fa-photo"></i> <tags:message code="nvli.theme"/>
            </div>
            <div class="card-block">
                <div class="row">
                    <c:forEach items="${themes}" var="theme">
                        <div class="col-md-3" >
                            <div class="theme-item <c:if test="${theme.id eq user.theme.id}">active</c:if>" data-target="#theme-${theme.id}" id="theme-${theme.id}" style="text-align: center;"  data-theme-id="${theme.id}">
                                    <label>
                                        <div class="img-preview img-thumbnail">
                                            <img src="${context}/uploadedThemes/${theme.themeCode}_theme/images/${theme.themeCode}_theme_thumbnail.png" height="200" width="200" onerror="this.src='${context}/images/icons/dummy.png'">
                                    </div>
                                    <div style="text-align: center;" >
                                        <input type="radio"
                                               name="user-theme"
                                               data-theme-name="${theme.themeName}"
                                               class="radio theme-item-radio"
                                               value="${theme.id}"
                                               data-theme-id="${theme.id}"
                                               data-target="#theme-${theme.id}"
                                               style="margin: 15px auto !important;"
                                               <c:if test="${theme.id eq user.theme.id}">checked</c:if>>
                                        </div>
                                        <div><h4>${theme.themeName}</h4></div>
                                    <div>${theme.themeDescription}</div>
                                </label>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
<!--            <div class="card-footer"></div>-->
        </div>
    </div>
</div>

<script>
    function applySelectedTheme(theme) {
//        var currenTheme = localStorage.getItem("selectedTheme");
        localStorage.setItem("selectedTheme", theme.themeCode);
        var src = "#current-theme-style";
        var target = __NVLI.context + "/uploadedThemes/" + theme.themeCode + "_theme/styles/" + theme.themeCode + ".css";

        $.ajax({
            type: 'HEAD',
            url: target,
            success: function () {
                $(src).attr("href", target);
            },
            error: function () {
                var uploadedTarget = __NVLI.context + "/uploadedThemes/default_theme/styles/" + theme.themeCode + ".css";
                $(src).attr("href", uploadedTarget);
            }
        });
    }

    function saveThemePref(themeId, target) {
        $.ajax({
            url: __NVLI.context + "/user/save/theme/" + themeId,
            type: 'post',
            success: function (response) {
                if (typeof response === "object") {
                    $(".theme-item").removeClass("active");
                    $(target).addClass("active");
                    applySelectedTheme(response);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $(document).ready(function () {
        $(".theme-item-radio").unbind().bind("click", function (e) {
            var themeId = e.currentTarget.getAttribute("data-theme-id");
            var target = e.currentTarget.getAttribute("data-target");
            saveThemePref(themeId, target);
        });
    });
</script>