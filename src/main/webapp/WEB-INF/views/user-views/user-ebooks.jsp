<%--
    Document   : my e-Books
    Created on : Jun 6, 2016, 11:24:03 AM
    Author     : vootla
    Author     : Ruturaj Powar<ruturajp@cdac.in>
--%>

<%@page import="in.gov.nvli.util.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<style>
    .ebook{
        padding: 8px 10px;
        margin-bottom: 8px;
        margin-top: 8px;
        border-bottom: 1px solid #ccc;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" style="display: none;"></div>
    <div class="card">
        <div class="card-header"><i class="fa fa-book"></i>  <tags:message code="ebook.my"/></div>
        <div class="card-block">
            <div id="dashboard-fav-libs">
                <div class="project-list">
                    <table class="table table-hover">
                        <tbody id="dashboard-ebooks" class="container">
                            
                        </tbody>
                    </table>
                    <!--<div id="dashboard-ebooks" class="container">-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script type="text/template" id="tpl-dash-ebooks">
    <tr id="tr_{{=identifier}}">
    <td class="lib-title">
    <i class="fa fa-book"></i>
    <strong><a href="${context}/{{=link}}" style="color: #2a5cea;"> {{=title}}</a></strong>&nbsp;&nbsp;<br>
    <small> <i class="fa fa-clock-o"></i>&nbsp;{{=addedTime}}</small>
    </td>
    <td class="lib-actions text-right">
    <a id="{{=identifier}}" data-tooltip="Delete" style="color: #2a5cea;" onClick="deleteEbook(this);" href="#"><i class="fa fa-trash"></i></a>
    </td>
    </tr>
</script>

<script type="text/javascript">
    var pageNumber;
    var pageWindow = 10;
    var processing = false;
    var hasNext = true;
    var isCacheCleared;
    function deleteEbook(tag) {
        var data = {};
        data["recordIdentifier"] = tag.id;
        data["title"] = tag.title;
        $.ajax({
            url: __NVLI.context + "/user/d/ebook",
            data: data,
            success: function (message) {
                if (message === "success")
                {
                    $('#tr_' + tag.id).remove();
                    $("#alert-box").html('<div class="alert alert-warning">Your ebook deleted successfully</div>').fadeIn();
                    $("#alert-box").fadeOut(3000);

                    if ($('#dashboard-ebooks').children().length === 0)
                    {
                        $("#dashboard-ebooks").append("<center>No Saved eBooks Found ! , to Save  <a href='" + __NVLI.context + "/user/suggestedebooks' style='color: #2a5cea;'>click here</a></center>");
                    }
                } else
                {
                    $("#alert-box").html('<div class="alert alert-danger">Problem While deleting, Please try again!</div>').fadeIn();
                    $("#alert-box").fadeOut(3000);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }


    function populateEbboks() {
        $.ajax({
            url: __NVLI.context + "/user/ebooks/results/" + pageNumber + "/" + pageWindow,
            dataType: 'json',
            type: 'GET',
            success: function (jsonObj) {
                if (jsonObj.suggestions.length === 0) {
                    if (pageNumber == 1)
                        $("#dashboard-ebooks").append("<center>No Saved eBooks Found ! , to Save  <a href='" + __NVLI.context + "/user/suggestedebooks' style='color: #2a5cea;'>click here</a></center>");
                    hasNext = false;
                    return false;
                }
                var Count = 0;
                _.each(jsonObj.suggestions, function (suggestion) {
                    var template = _.template($("#tpl-dash-ebooks").html());
                    Count++;
                    var opts = {
                        "link": suggestion.link,
                        "title": suggestion.title,
                        "identifier": suggestion.identifier,
                        "addedTime": suggestion.addedTime
                    };
                    var el = template(opts);
                    $("#dashboard-ebooks").append(el);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
//  function clearCache(){
//            if(!isCacheCleared){
//            $.ajax({
//                url: __NVLI.context + "/user/delete-redis-cache/" +'<%=Constants.FilterActivityLogs.EbookActivities%> ',
//                type: 'POST',
//                success: function (message) {
//                    console.log("cache cleared..");
//                    isCacheCleared=true;
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    console.error(textStatus, errorThrown, jqXHR);
//                }
//            });
//        }
//    }
    $(document).ready(function () {
        isCacheCleared = false;
        pageNumber = 1;
        populateEbboks();

        $("#dashboard-ebooks").scroll(function (e) {
            if (processing)
                return false;
            if (hasNext && $("#dashboard-ebooks").scrollTop() >= ($("#dashboard-ebooks").height() - $("#dashboard-ebooks").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch notifications
                populateEbboks();
                processing = false;
            }
        });
    });
</script>