<%-- 
    Document   : user-saved-libraries
    Created on : Jun 6, 2016, 11:35:02 AM
    Author     : Bhumika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><i class="fa fa-heart" style="color:#FF5722;"></i><tags:message code="dashboard.favorite.lib"/></div>
        <div class="card-block">
            <div id="dashboard-fav-libs">
                <div class="project-list">
                    <table class="table table-hover">
                        <tbody id="dashboard-fav-libs-body"></tbody>
                    </table>
                </div>
                
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-secondary btn-block"><i class="fa fa-arrow-down"></i><tags:message code="dashboard.show.more"/></button>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-dash-fav-lib-item">
    <tr>
    <td class="lib-icon"><i class="fa fa-link"></i></td>
    <td class="lib-title">
    <a href="#" class="">National Digital Library - IIT Kharagpur</a><br>
    <small>Added 14.08.2014</small><br>
    <small>
    <b>Privacy</b>
    <a href="#"  
    data-toggle="tooltip" 
    data-placement="top" 
    title="" 
    data-original-title="Public" 
    title="Public">
    <i class="fa fa-globe"></i>
    </a>
    </small>
    </td>
    <td class="lib-actions text-right">
    <a href="#" class=""><i class="fa fa-eye"></i> View </a>
    </td>
    </tr>
</script>
<script type="text/javascript">
    function populateFavLibs() {
        $("#dashboard-fav-libs-body").empty();
        for (var i = 0; i < 10; i++) {
            var tpl = _.template($("#tpl-dash-fav-lib-item").html());
            var opts = {};
            var $el = tpl(opts);
            $("#dashboard-fav-libs-body").append($el);
        }
    }

    $(document).ready(function () {
        $("#dashboard-fav-libs-container").slimScroll({
            height: '600px',
            size: '5px',
            position: 'right',
            color: '#888',
            alwaysVisible: false,
            distance: '0px',
            railVisible: false,
            railColor: '#efefef',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: false
        });
        populateFavLibs();
    });

</script>
