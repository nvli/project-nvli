<%-- 
    Document   : user-notfication-settings
    Created on : May 15, 2017, 2:18:08 PM
    Author     : Gulafsha Khan <gulafsha@cdac.in>
--%>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-gear"> Email Settings</i>   
            </div>
            <div class="card-block">
                <div class="container">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <div class="" role="tab" id="headingOne">
                                <div class="panel-title">
                                    <div>
                                        NVLI Link Share
                                        <div class="form-check pull-right">
                                            <label class="form-check-label pull-right">
                                                <input class="form-check-input pull-right" type="checkbox" id="nvli-link-share"> <span>  Send Email</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <i class="fa fa-gear"> Application Settings</i> 
            </div>
            <div class="card-block">
                <div class="container">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel">
                            <div class="" role="tab" id="headingOne">
                                <div class="panel-title">
                                    <div>
                                        NVLI Link Share
                                        <div class="form-check pull-right">
                                            <label class="form-check-label pull-right">
                                                <input class="form-check-input pull-right" type="checkbox" id="nvli-link-share"> <span>  Send Notification</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function checkNvliLinkShareStatus() {
        $.get(window.__NVLI.context + "/notificationSubscription/check/nvli-share-status", {}, function (response) {
            console.log(response);
            if (response)
                $("#nvli-link-share").attr("checked", true);
            else
                $("#nvli-link-share").attr("checked", false);
        });
    }
    $(document).ready(function () {
        checkNvliLinkShareStatus();
        $("#nvli-link-share").click(function () {
            var data;
            if (this.checked)
                data = 'checked';
            else
                data = 'unchecked';
            $.post(window.__NVLI.context + "/notificationSubscription/nvli/link/share/send/email/" + data, function (status) {
                if (status)
                    console.log("updated")
                else
                    console.log("not updated");
            })
        });
    });
</script>

