<%--
    Document   : user-Watch-Lists
    Created on : Oct 31, 2017, 11:24:03 AM
    Author     : Ruturaj<ruturajp@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>

<div id="add_to_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="addToListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="add_to_list_link-title"><tags:message code="dashboard.add.to.list"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix" id="add_to_list_link-body">

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><span class="fa fa-list"></span> <tags:message code="dashboard.watchlist.heading"/></div>
        <div class="card-block" style="display: flow-root;">
            <div id="dashboard-fav-libs" class="row">
                <div class="slimScrollDiv" >
                     <div id="watch-lists">
                     </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/template" id="tpl-dash-wlists">
      <div class="row list-item">
    <div class="col-md-9 lib-title">
    <strong><a href="#"  id="{{=title}}" onclick="showMyList('{{=title}}','{{=createdById}}');" title="{{=title}}">{{=title}}</a></strong><br>
    <small><i class="fa fa-clock-o"></i> {{=addedTime}}</small><br>
    <small><i class="fa fa-user"></i>  created by - {{=createdByUsername}}</small><br>
    </div>
    <div class="col-md-3 lib-actions text-right">
    <span class="pull-right"><button href="#" onclick="unSubscribeToList('{{=title}}','{{=createdById}}','{{=createdByUsername}}')" id="{{=title}}"><i class="fa fa-success" aria-hidden="true"></i> Un-Subscribe  </button></span>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-watch-list">
       <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <div class="mylist-container grid-item card m-t-1 public_list_item">          
            <div class="card-header">
                <h4 class="mylist-header-title text-uppercase">{{=listTitle}}</h4>
                <h6 class="mylist-header-author m-a-0 text-right-cust">
                    <span>&mdash; {{=authorName}}</span>
                    <img src="${exposedProps.userImageBase}/{{=profilePicUrl}}"
                        class="img img-circle" onerror="this.src='${context}/images/profile-picture.png'"
                        width="24" height="24"/>
                </h6>
            </div>
            <div class="mylist-body card-block">
                <ol class="p-l-2">
                {{ _.each(listItems, function(item){  }}
                <li class='favoriteList' id='mylistcontents_{{= item.itemUrl}}'>
                   <p><span><a href="${context}/search/preview/{{=item.itemUrl}}">  {{=item.itemName}} </a></p>
                 </li>
                {{ }); }}                           
                </ol>
            </div>           
            <div class="mylist-footer card-footer text-center">                
                <button onclick="unSubscribeToList('{{=listTitle}}','{{=authorId}}','{{=authorName}}')" id="{{=listTitle}}" class="btn btn-primary btn-sm"></i>Un-Subscribe</button>                     
                </button>                                   
            </div>
            </div>
        </div>
</script>

<script type="text/javascript">
     var pageNumber;
    var pageWindow = 8;
    var processing = false;
    var hasNext = true;                            
    function populateWLists() {
        $.ajax({
            url: __NVLI.context + "/search/user/watch-lists/results/"+pageNumber+"/"+pageWindow,
            dataType: 'json',
            type: 'GET',
            success: function (jsonObj) {
              if (jsonObj.watchLists.length === 0) {
                    if (pageNumber == 1)
                        $("#watch-lists").append("<center>No Watch Lists Found! Subscribe to Public lists</center>");
                    hasNext = false;
                    return false;
                } else {
                    _.each(jsonObj.watchLists, function (list) {             
                        var template = _.template($("#tpl-watch-list").html());
                        var opts = {
                            "listTitle": list.listTitle,
                            "authorId":list.authorId,
                            "authorName": list.authorName,
                            "profilePicUrl": list.profilePicUrl,
                            "listItems": list.itemArray                           
                        };
                        var el = template(opts);
                        $("#watch-lists").append(el);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
     function startup() {
        pageNumber = 1;
       populateWLists();
   }
   function showMyList(listName,creatorId){
        $('#add_to_list_link').modal('show');
        $('#add_to_list_link-title').html(listName);
        var data = {}
        data["query"] = $("#query").val();
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: __NVLI.context + "/user/mylists/" + listName+"/"+creatorId,
            data: JSON.stringify(data),
            dataType: 'json',
            timeout: 100000,
            success: function (data) {              
                var json = "";
                {
                    for (content of data) {                    
                        json = json + "<div class='favoriteList' id='mylistcontents_" + content.link + "'><p><span><a href='${context}/search/preview/" + content.link + "'> " + content.title + " </a></span></p></div>";
                    }
                }

                $('#add_to_list_link-body').html('<div class="card-block"> <div id="list-container-contents">' + json + "</div></div>");
            },
            error: function (e) {
                console.log("ERROR: ", e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }
    function unSubscribeToList(listname,createdById,createdByUsername){   
      $.ajax({
          url:__NVLI.context+"/search/user/unSubscribeToList/"+listname+"/"+createdById+"/"+createdByUsername,
          type:'POST',
          success:function(res){
               if (res == 2)
                {
                    showToastMessage("Un Subscribed Successfully.", "success");
                } else {
                    showToastMessage("Error...Please try again later", "info");           
                }
                 $("#watch-lists").empty();
                   startup();
        },
          error:function(e){
          console.log("ERROR: ", e);
                display(e);},
          done:function(e){
          console.log("DONE");}
                });
    }
    $(document).ready(function () {
        startup();
        $(window).scroll(function (e) {
            if (processing)
                return false;
            if (hasNext && $(window).scrollTop() >= ($("#watch-lists").height() - $("#watch-lists").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch notifications
                populateWLists();
                processing = false;
            }
        });
    });
</script>