<%-- 
    Document   : user-rewards
    Created on : 12 Feb, 2018, 10:48:58 AM
    Author     : Ankita Dhongde <dankita@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<script language="javascript">
    function goPage(page) {
        $('#pageNum').val(page);
        $("#sform").submit();
    }
</script>

<style>
    .feed-reward-list .feed-element {
        border-bottom: 1px solid #e7eaec;
    }
    .feed-element:first-child {
        margin-top: 0;
    }
    .feed-element {
        padding-bottom: 15px;
    }
    .feed-element,
    .feed-element .media {
        margin-top: 15px;
    }
    .feed-element,
    .media-body {
        overflow: hidden;
    }
    .feed-element > .pull-left {
        margin-right: 10px;
    }
    /*.feed-element img.img-circle,*/
    .record-link{
        color: #0275d8;
        border-bottom: 1px solid #0275d8;
    }
    span.badge{
        border-radius: 4px;
        background: #ddd;
        padding: 2px 8px;
        color: #333;
        font-weight: bold;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><tags:message code="label.myRewards"/></div>
        <div class="card-block" style="background:#FFFFFF;" id="feed-reward-list">
        </div>
        <div class="card-footer text-center">
            <div><button class="btn btn-secondary btn-block" id="showMore">Show More</button></div>
        </div>
    </div>
</div>              
<script type="text/javascript">
    var pageNO = 1;
    showMore(pageNO);
    $(document).ready(function () {
        $("#showMore").on('click', function (e) {
            pageNO = pageNO + 1;
            console.log(pageNO);
            showMore(pageNO);
        });
    });

    function showMore(pageNO) {
        $.ajax({
            url: "${context}/rewards/showRewardsPerPage",
            data: {pageNo: pageNO},
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                if(data.length !== 0 ) {
                    _.each(data, function (rh, index) {

                        var tpl = _.template($("#tpl-reward-history").html());
                        var opts = {
                            "date": new Date(rh.id.date).toLocaleString(),
                            "activityType": rh.activityType,
                            "recordTitle": rh.recordTitle,
                            "editNumer": rh.activity.editNumber,
                            "recordIdentifier":rh.recordIdentifier,
                            "points":rh.activity.tags
                        };
                        var el = tpl(opts);
                        $("#feed-reward-list").append(el);
                    }); 
                } else {
                    
                    $("#feed-reward-list").append("<div class='text-center'>No more activities</div>");
                    document.getElementById("showMore").disabled = true;
                    
                }    
            }, error: function (jqXHR, textStatus, errorThrown) {
                 $("#feed-reward-list").append("<div class='card-footer text-center'>No More Contents<div>");
            }
        });
    }

</script>
<script type="text/template" id="tpl-reward-history">
    <div class="feed-reward-list feed-element" >
        <a href="#" class="pull-left">
            <img src="${context}/images/profile-picture.png" height="40px" class="profile_img img-circle" onerror="this.src='${context}/images/profile-picture.png'">
        </a>
        <div class="media-body"> 
            <small class="pull-right text-navy">{{=date}}</small> 
            {{if( activityType === "addCustomTag" ) { }}
            <strong> You added custom tag for record <a href="${context}/search/preview/{{=recordIdentifier}}" target="_blank" class="record-link">{{=recordTitle}}</a></strong>          
            <div>Reward points earned <span class="badge">{{=points.length}}</span> </div>
            {{ } }} 
            {{if( activityType === "addUDCTag" ) { }}
            <strong> You added UDC tag for record <a href="${context}/search/preview/{{=recordIdentifier}}" target="_blank" class="record-link">{{=recordTitle}}</a></strong>          
            <div>Reward points earned <span class="badge">{{=points.length}}</span> </div>
            {{ } }}    
            {{if( activityType ==="editMetadata" ) { }}
            <strong> You have edited Metadata for record <a href="${context}/search/preview/{{=recordIdentifier}}" target="_blank" class="record-link">{{=recordTitle}}</a></strong>          
            <div>Reward points earned <span class="badge">{{=editNumer}}</span> </div>
            {{ } }}    
            <div class="clearfix"></div>
        </div>
    </div>
    <hr>
</script>
