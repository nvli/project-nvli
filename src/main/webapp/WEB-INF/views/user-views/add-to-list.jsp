<%--
    Document   : add-to-list
    Created on : Jun 20, 2016, 1:50:37 PM
    Author     : Bhumika
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div id="addToListDiv">
                    <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
                    <div class="card-header"><tags:message code="dashboard.add.to.list"/></div>
                    <div class="card-block" style="padding: 50px;">
                        <div class="container">
                            <%--<form  role="form" id="add-to-list-form" action="" commandName="" method="POST">--%>
                            <div class="form-group row clearfix">
                                <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"></label>
                                <div class="col-lg-4 col-md-4 col-sm-12"><select  id="addToListSelect" class="col-lg-4 col-md-4 col-sm-8">
                                        <option value="0"> <tags:message code="list.select"/>"</option>
                                        <c:forEach items="${userSavedLists}" var="savedList">
                                            <option value="${savedList.listId}">${savedList.listName}-<c:choose><c:when  test="${savedList.publicPrivate==1}"><span class="col-lg-4 col-md-4 col-sm-12">Private</span></c:when><c:otherwise><span class="col-lg-4 col-md-4 col-sm-12">Public</span></c:otherwise></c:choose></option>
                                        </c:forEach>
                                    </select></div>
                            </div>
                            <div class="form-group row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    <input type="button" class="btn btn-primary" value="Add to List" id="AddToExistingListBtn"/><input type="reset" class="btn btn-primary" value="Reset" id="resetBtn1"/><input type="button" class="btn btn-primary" value="Create New List" id="createListBtn">
                                </div>
                            </div>
                            <%--</form>--%>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div id="createNewListDiv">
                        <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
                        <div class="card-header">Create New List </div>
                        <div class="card-block" style="padding: 50px;">
                            <div class="container">
                                <!--<form:form  role="form" id="add-to-list-form" action="" commandName="" method="POST">-->
                                <div class="form-group row clearfix">
                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right">List Name:</label>
                                    <div class="col-lg-4 col-md-4 col-sm-8">
                                        <input type="text" id="newListTxt"/>
                                    </div>
                                </div>
                                <div class="form-group row clearfix">
                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"></label>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        Public : <input type="radio" name="accessRadio" value="0"/>Private : <input type="radio" name="accessRadio" value="1" checked/>
                                    </div>
                                </div>
                                <div class="form-group row clearfix">
                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right"></label>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <input type="submit" class="btn btn-primary" value="Add to List" id="addToNewListBtn"/><input type="reset" class="btn btn-primary" value="Reset" id="resetBtn2"/><input type="button" class="btn btn-primary" value="cancel" id="cancelBtn"/>
                                    </div>
                                </div>
                            </div>
                            <!--</form:form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${context}/scripts/user-module/add-to-list.js"></script>