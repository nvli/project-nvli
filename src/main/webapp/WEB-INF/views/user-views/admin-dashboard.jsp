<%-- 
    Document   : admin-dashboard
    Created on : 10 Apr, 2018, 2:29:35 PM
    Author     : Gulafsha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script src="${context}/components/jquery/jquery-dateFormat.min.js"></script>
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <input id="searchString" class="form-control" placeholder="Search title">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                <select id="limitFilter" class="form-control">
                    <option value="10" selected>10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="20%">S.NO</th>
                    <th width="40%">key</th>
                    <th width="40%">Name</th>
                </tr>
            </thead>
            <tbody id="keys-tbl-body"></tbody>
        </table>
        <div id="keys-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/template" id="tpl-keys-tbl-row">
    {{ _.each(keysList, function (value,i) { }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>{{=value.secretKey}}</td>
    <td>{{=value.name}}</td>
    </div>
    </td>
    </tr>
    {{ }); }}
</script>
<script type="text/javascript">
$(document).ready(function () {
    fetchAndRenderTableData(1);
    $("#searchString").unbind().bind("keyup", function () {
        fetchAndRenderTableData(1);
    });
    $("#limitFilter").unbind().bind("change", function () {
        fetchAndRenderTableData(1);
    });
});
function fetchAndRenderTableData(pageNo) {
    $.ajax({
        url: '${context}/admin/getUsersByFiltersWithLimit',
        data: {searchString: $('#searchString').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo},
        type: 'POST',
        success: function (jsonObject) {
            if (jsonObject !== null) {
                if (_.isEmpty(jsonObject.keysList)) {
                    $("#keys-tbl-body").html('<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                } else {
                    console.log(jsonObject);
                    var Tpl = _.template($("#tpl-keys-tbl-row").html());
                    var Opts = {
                        "keysList": jsonObject.keysList,
                        "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                    };
                    var TableBody = Tpl(Opts);
                    $("#keys-tbl-body").empty();
                    $("#keys-tbl-body").html(TableBody);
                    renderPagination(fetchAndRenderTableData, "keys-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                }
            } else {
                alertErrorMsg();
            }
        },
        error: function () {
            alertErrorMsg();
        }
    });
}
function alertErrorMsg() {
    $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
    $("#alert-box").fadeOut(5000);
}
</script>