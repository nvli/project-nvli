<%-- 
    Document   : user-message-page
    Created on : May 17, 2016, 3:10:48 PM
    Author     : Bhumika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="col-xs-12">
    <div class="container text-xs-center">
        <br>
        <div class="alert alert-info">
            <p>${userMsg}</p>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        window.location.href += "#";
        setTimeout("changeHashAgain()", "50");
    });

    function changeHashAgain() {
        window.location.href += "1";
    }

    var storedHash = window.location.hash;
    window.setInterval(function () {
        if (window.location.hash != storedHash) {
            window.location.hash = storedHash;
        }
    }, 50);

</script>

