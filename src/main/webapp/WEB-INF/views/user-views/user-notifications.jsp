<%--
    Document   : user-notifications
    Created on : Jul 27, 2016, 5:16:36 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
    Author     : Ruturaj Powar <ruturajp@cdac.in>

--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!--<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
    <div class="container" id="main" style="z-index: 1;">
        <div class="row">
            <div class="col-lg-12 notification">
                <div class="row">
                    <div class="card ">
<div class="card-header"><spring:message code="notif.heading"/> </div>
                        <div class="clearfix cabrd-block" id="notif-container">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var pageNumber;
    var pageWindow = 10;
    var processing = false;
    $("#notif-container").slimScroll({
        height: '650px',
        size: '8px',
        position: 'right',
        color: '#888',
        alwaysVisible: true,
        distance: '3px',
        railVisible: true,
        railColor: '#efefef',
        railOpacity: 0.3,
        wheelStep: 10,
        allowPageScroll: false,
        disableFadeOut: false
    });
    $(document).ready(function () {
        pageNumber = 1;
        fetchNotifs(pageNumber, pageWindow);


        $("#notif-container").scroll(function (e) {
            if (processing) {
                return false;
            }
            if ($("#notif-container").scrollTop() >= ($("#notif-container").height() - $("#notif-container").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch notifications
                fetchNotifs(pageNumber, pageWindow);
                processing = false;
            }
        });
    });
</script>-->
<div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
    <div class="container-fliud " id="main" style="z-index: 1;">
        <div class="row">
            <div class="col-lg-12 notification">             
                    <div class="card">
                        <div class="card-header"><i class=" fa fa-bell"></i>  <spring:message code="notif.heading"/> 
<!--                            <a class="fa fa-gear pull-right" href="${context}/notificationSubscription/notifications-subscription"> Notification Subscription</a>-->
                        </div>
                        <div class="clearfix card-block container">
                            <div id="notif-container" style="border: 1px solid #ddd;"></div>
                            <button id="btn-show-more-notifs" class="btn btn-secondary btn-block" data-target="#notif-container" data-current-page="1" data-max="10">
                                 <spring:message code="notif.show.more"/>
                            </button>
                        </div>
                        <div class="card-footer">

                        </div>
                    </div>              
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="tpl-notif-not-found">
    <div style="padding:20px; text-align:center; font-size:14px;">
        <div style="color:#ddd; font-size:100px;"><span class="fa fa-bell"></span></div>
        Notifications
    </div>
</script>
<script>

    $(document).ready(function () {
        fetchNotifs(1, 10);
    });
</script>
<!--<script type="text/template" id="tpl-notif-item">
    <div id="{{=notif_id}}">
    <div class="notif-item media {{ if(is_read){ }} noti_read {{ } }}"  data-notif-id="{{=notif_id}}" data-target-url="{{=target_url}}">
    <a class="pull-left" href="#">
    <img class="media-object" src="${exposedProps.userImageBase}/{{=notif_sender_profile_pic}}" alt="Media Object" onerror="this.src='${context}/images/profile-picture.png'">
    </a>

    <div>
    <p> {{=notif_message}} </p>
    <span class="text-muted"><i class="fa fa-clock-o"></i> {{=notif_time}} </span>
    </div>
    </div>
    </div>
</script>-->

