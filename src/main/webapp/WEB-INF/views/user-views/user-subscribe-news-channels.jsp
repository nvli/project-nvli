<%-- 
    Document   : user-subscribe-news-channels.jsp
    Created on : May 16, 2017, 2:21:37 PM
    Author     : Vivek Bugale <bvivek@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    #interest-tab-content .tab-content>.active{
        padding-right: 0px; 
        padding-left: 0px; 
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card newchanneloptions">
        <div class="card-header"><i class="fa fa-wrench"></i><tags:message code="profile.settings.heading"/></div>
        <div class="">
            <div class="tabs" style="margin-top: 0;padding:0 !important;">
                <ul id="myTab" class="nav nav-tabs font-weight-bold" style="margin-top: 0px;">
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/interests"><tags:message code="profile.setting.interest"/></a> </li>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/profession"><tags:message code="profile.setting.profession"/></a> </li>
                        <%--<li class="nav-item"> <a class="nav-link" href="${context}/user/specialization"><tags:message code="profile.setting.specialization"/></a> </li>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/languages"><tags:message code="profile.setting.language"/></a> </li>
                        <%--<security:authorize access="hasAnyRole('LIB_USER','CURATOR_USER')"><li class="nav-item"> <a class="nav-link active"><tags:message code="profile.setting.resource.specialization"/></a> </li></security:authorize>--%>
                    <li class="nav-item"> <a class="nav-link" href="${context}/user/subscribe-news"><tags:message code="profile.setting.news.papers"/></a> </li>
                    <li class="nav-item"> <a class="nav-link active" href="#"><tags:message code="profile.setting.news.channels"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div id="lang-tab-content" class="tab active">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <tags:message code="profile.setting.subscribe.news.channels"/>
                                        <div class="pull-right">
                                            <select class="form-control form-control-sm" id="channel-lang">
                                                <option value="all" selected>All</option>
                                                <c:forEach items="${languageList}" var="language">
                                                    <option value="${language.languageCode}">${language.languageName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="card-block">
                                        <div id="news-channel-content-block" class="tab-content">
                                            <div class="row interest_tab" id="news-channels-container"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="card newchanneloptions">
                                    <div class="card-header"><tags:message code="profile.setting.subscribed.news.channels"/>&nbsp;&nbsp;<span id="feedback"></span><a href="${context}/user/news-channel" class="btn btn-sm btn-success pull-right" style="margin-top: -3px;"><b><tags:message code="profile.subscribed.show.channels"/></b></a></div>
                                    <div class="card-block" style="padding: auto 5px !important;">
                                        <div id="subscribed-news-channels-container"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>

<script type="text/template" id="tpl-news-channel">
    <div class="col-xl-4 col-sm-6 col-xs-12">
    <button class="btn btn-secondary btn-block subscribe-channel"
    style="margin:5px 0px 5px 0px; text-align:left !important;overflow: hidden;"
    data-channel-code="{{=channelCode}}" data-channel-display-name="{{=channelName}}">
    <img src="${context}/news-agencies-logo/{{=channelCode}}.png" alt="{{=channelName}}" width="28" height="28"/>
    <b>{{=channelName}}</b>
    </button>
    </div>
</script>

<script type="text/template" id="tpl-subscribed-news-channels">
    <li class="list-group-item" data-channel-code="{{=channelCode}}">
    <img src="${context}/news-agencies-logo/{{=channelCode}}.png" alt="{{=channelName}}" width="30" height="30"/>
    <b>{{=channelName}}</b>
    <button data-channel-code="{{=channelCode}}" title="Remove" class="pull-right btn btn-secondary unsubscribe-channel"><i class="fa fa-trash"></i></button>
    </li>
</script>

<script type="text/javascript">
    function showFeedback() {
        $("#feedback").html('<span class="text-success animated fadeOut"><i class="fa fa-check-circle"> Saved</span>');
    }

    function renderNewsChannels(response) {
        $("#news-channels-container").empty();
        _.each(response, function (newsPaper) {
            var tpl = _.template($("#tpl-news-channel").html());
            var opts = {
                channelCode: newsPaper.paperCode,
                channelName: newsPaper.paperDisplayName
            };
            var elem = tpl(opts);
            $("#news-channels-container").append(elem);
        });
        $(".subscribe-channel").unbind().bind("click", function (e) {
            $(this).toggleClass("active");
            var isActive = $(this).hasClass("active");
            var action = isActive ? "add" : "remove";
            updateUserNewsChannels(e.currentTarget.getAttribute("data-channel-code"),e.currentTarget.getAttribute("data-channel-display-name"), action);
        });
        checkSelected();
    }

    function renderSubscribedNewsChannels(response) {
        __NVLI['subscribedNewsPaper'] = response;
        fetchNewsChannels(renderNewsChannels);
        $("#subscribed-news-channels-container").empty();
        _.each(_.sortBy(response,function(i){return i.paperName.toLowerCase();}), 
        function (newsPaper) {
            var tpl = _.template($("#tpl-subscribed-news-channels").html());
            var opts = {
                channelCode: newsPaper.paperCode,
                channelName: newsPaper.paperDisplayName
            };
            var elem = tpl(opts);
            $("#subscribed-news-channels-container").append(elem);
            $(".unsubscribe-channel").mouseover(function (e) {
                $(e.currentTarget).addClass("btn-danger");
                $(e.currentTarget).removeClass("btn-secondary");
            });
            $(".unsubscribe-channel").mouseout(function (e) {
                $(e.currentTarget).removeClass("btn-danger");
                $(e.currentTarget).addClass("btn-secondary");
            });
            $(".unsubscribe-channel").unbind().bind("click", function (e) {
                updateUserNewsChannels(e.currentTarget.getAttribute("data-channel-code"),e.currentTarget.getAttribute("data-channel-display-name"), "remove");
            });
        });
    }

    function fetchSubscribedNewsChannels(callback) {
        $.ajax({
            url: __NVLI.context + "/nac/fetch-user-subscribed-news-channels",
            success: callback,
            error: function (xhr) {
                console.log("error while fetching subscribed news channels ::",xhr);
            }
        });
    }

    function updateUserNewsChannels(channelCode,channelName,action) {
        $.ajax({
            url: window.__NVLI.context + "/nac/update-user-news-channel",
            data: {"action": action, "paperCode": channelCode},
            dataType: 'json',
            contentType: "application/json",
            success: function (responseData) {
                if (responseData.status === "success") {
                    showFeedback();
                }
                //fetchSubscribedNewsChannels(renderSubscribedNewsChannels);
                if(action==="add"){
                    prependToSubscribed(channelCode,channelName);
                }else{
                    removeFromSubscription(channelCode);
                }
            },
            error: function (xhr) {
                console.log("error while upadting news channels ::", xhr);
            }
        });
    }
    
    function prependToSubscribed(channelCode,channelName){
        var isPresent=false;
        $(".list-group-item").each(function (){
           if($(this).attr('data-channel-code')===channelCode){
               isPresent=true;
           }            
        });
        if(!isPresent){
            var tpl = _.template($("#tpl-subscribed-news-channels").html());
            var opts = {
                channelCode: channelCode,
                channelName: channelName
            };
            var elem = tpl(opts);
            $("#subscribed-news-channels-container").prepend(elem); 
            $(".unsubscribe-channel").unbind().bind("click", function (e) {
                updateUserNewsChannels(e.currentTarget.getAttribute("data-channel-code"),e.currentTarget.getAttribute("data-channel-display-name"), "remove");
            });
        }
    }
    
    function removeFromSubscription(channelCode){
        $('li[data-channel-code="' + channelCode +'"]').remove();
        $('button[data-channel-code="' + channelCode +'"]').removeClass("active");
    }

    function activateSlimScroll(id, height) {
        $("#" + id).slimScroll({
            height: height,
            size: '8px',
            position: 'right',
            color: '#666',
            alwaysVisible: false,
            distance: '0px',
            railVisible: true,
            railColor: '#DDD',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: true
        });
    }

    function fetchNewsChannels(callback) {        
        $.ajax({
            url: __NVLI.context + "/nac/fetch-all-news-channels",
            data: {"paperLang": $("#channel-lang").val()},
            success: callback,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function checkSelected() {
        _.each(__NVLI.subscribedNewsPaper, function (newsPaper) {
            var elems = $(".subscribe-channel[data-channel-code='" + newsPaper.paperCode + "']");
            _.each(elems, function (el) {
                $(el).addClass('active');
            });
        });
    }

    $(document).ready(function () {
        fetchSubscribedNewsChannels(renderSubscribedNewsChannels);

        activateSlimScroll("subscribed-news-channels-container", "550px");
        activateSlimScroll("news-channel-content-block", "550px");

        $('#channel-lang').unbind().bind('change', function () {
            fetchNewsChannels(renderNewsChannels);
        });
    });

</script>

