<%--
    Document   : public-Lists
    Created on : Oct 30, 2017, 11:24:03 AM
    Author     : Ruturaj Powar<ruturajp@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<div id="add_to_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="addToListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="add_to_list_link-title"><tags:message code="dashboard.add.to.list"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group row clearfix" id="add_to_list_link-body">

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<div id="edit_list_link" class="modal fade">
    <div class="modal-dialog" role="document">
        <div id="editListDiv" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="edit_list_link-title">Update</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="listIdHidden"/>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="label.list.name"/></label>
                    <div class="col-lg-4 col-md-4 col-sm-8">
                        <input type="text" id="listNameTxt" class="form-control"/>
                    </div>
                </div>
                <div class="form-group row clearfix">
                    <label class="form-control-label col-lg-2 col-md-4 col-sm-4 text-right"><tags:message code="label.visibility"/></label>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <input type="radio" name="accessRadio" value="1" checked/> Public <input type="radio" name="accessRadio" value="1" /> <tags:message code="label.private"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" id="updateListBtn" class="btn btn-primary" value="Update"/>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div class="card-header"><span class="fa fa-list"></span> <tags:message code="sb.public.lists"/></div>
        <div class="card-block" style="display: flow-root;">
            <div id="dashboard-fav-libs" class="row">
                <div class="slimScrollDiv" >
                    <div id="public-lists" class="grid"> </div>
                </div>
            </div>
        </div>
    </div>
</div>       
<script type="text/template" id="tpl-pub-list">
    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="mylist-container grid-item card m-t-1 public_list_item">
            <div class="card-header">
                <h4 class="mylist-header-title text-uppercase">{{=listTitle}}</h4>
                <h6 class="mylist-header-author m-a-0 text-right-cust">
                    <span>&mdash; {{=authorName}}</span>
                    <img src="${exposedProps.userImageBase}/{{=profilePicUrl}}"
                        class="img img-circle" onerror="this.src='${context}/images/profile-picture.png'"
                        width="24" height="24">
                </h6>
            </div>
            <div class="mylist-body card-block">
                <ol class="p-l-2">
                {{ _.each(listItems, function(item){  }}
                <li class='favoriteList' id='mylistcontents_{{= item.itemUrl}}'>
                   <p><span><a href="${context}/search/preview/{{=item.itemUrl}}">  {{=item.itemName}} </a></p>
                 </li>
                {{ }); }}                           
                </ol>
            </div>           
            <div class="mylist-footer card-footer text-center">            
                <button  onclick="subscribeToList('{{=listTitle}}', '{{=authorId}}', '{{=authorName}}')" id="{{=listTitle}}"  class="btn btn-primary btn-sm">
                <i class="fa fa-newspaper-o"></i> Subscribe <span class="badge">{{=followersCount}}</span></button>                                   
            </div>
        </div>
    </div>
</script>
        
<!--<script type="text/template" id="tpl-pub-list">
    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <div class="mylist-container grid-item card m-t-1 public_list_item">
            <div class="card-header">
                <h4 class="mylist-header-title text-uppercase">{{=listTitle}}</h4>
                <h6 class="mylist-header-author m-a-0 text-right-cust">
                    <span>&mdash; {{=authorName}}</span>
                    <img src="${exposedProps.userImageBase}/{{=profilePicUrl}}"
                        class="img img-circle" onerror="this.src='${context}/images/profile-picture.png'"
                        width="24" height="24">
                </h6>
            </div>
            <div class="mylist-body card-block">
                <ol class="p-l-2">
                {{ _.each(listItems, function(item){  }}
                <li class='favoriteList' id='mylistcontents_{{= item.itemUrl}}'>
                   <p><span><a href="${context}/search/preview/{{=item.itemUrl}}">  {{=item.itemName}} </a></p>
                 </li>
                {{ }); }}                           
                </ol>
            </div>           
            <div class="mylist-footer card-footer text-center">  
            <button class="btn btn-primary btn-sm"><i class="fa fa-share"></i> Share</button>
                <button class="btn btn-primary btn-sm"><i class="fa fa-heart"></i> Like</button>
                <button  onclick="subscribeToList('{{=listTitle}}', '{{=authorId}}', '{{=authorName}}')" id="{{=listTitle}}"  class="btn btn-primary btn-sm">
                <i class="fa fa-newspaper-o"></i> Subscribe <span class="badge">{{=followersCount}}</span></button>                   
            </div>
        </div>
    </div>
</script>-->

<script>
    var pageNumber;
    var pageWindow = 8;
    var processing = false;
    var hasNext = true;
    
    function populateLists() {
        $.ajax({
            url: __NVLI.context + "/search/user/get/publicLists/" + pageNumber + "/" + pageWindow,
            dataType: 'json',
            type: 'GET',
            success: function (jsonObj) {
                if (jsonObj.publicLists.length === 0) {
                    if (pageNumber == 1)
                        $("#public-lists").append("<center>No Public Lists Found!</center>");
                    hasNext = false;
                    return false;
                } else {
                    _.each(jsonObj.publicLists, function (list) {
                        var template = _.template($("#tpl-pub-list").html());
                        var opts = {
                            "listTitle": list.listTitle,
                            "authorId": list.authorId,
                            "authorName": list.authorName,
                            "profilePicUrl": list.profilePicUrl,
                            "listItems": list.itemArray,
                            "followersCount": list.followersCount
                        };
                        var el = template(opts);
                        $("#public-lists").append(el);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    $(document).ready(function () {
        startup();
    });
    function startup() {
        pageNumber = 1;
        populateLists();
        $(window).scroll(function (e) {
            if (processing)
                return false;
            if (hasNext && $(window).scrollTop() >= ($("#public-lists").height() - $("#public-lists").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch notifications
                populateLists();
                processing = false;
            }
        });
    }
    function subscribeToList(listname, createdById, createdByUsername) {
        $.ajax({
            url: __NVLI.context + "/search/user/subscribeToList/" + listname + "/" + createdById + "/" + createdByUsername,
            type: 'POST',
            success: function (res) {
                if (res == 2)
                {
                    showToastMessage("Subscribed Successfully..You can keep track of this list through 'My Watch List' option in 'My Stuff' section", "success");
                } else {
                    showToastMessage("Error...Please try again later...", "info");
                }
                $("#public-lists").empty();
                startup();
            },
            error: function (e) {
                console.log("ERROR: ", e);
                display(e);
            },
            done: function (e) {
                console.log("DONE");
            }
        });
    }
</script>
