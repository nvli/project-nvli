<%-- 
    Document   : user-reset-password
    Created on : Apr 26, 2016, 12:17:34 PM
    Author     : Bhumika
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="container-fluid" id="main">
    <div class="clear"></div>
    <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
    <div class="col-lg-12">
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div style="margin-top:8%;">
            <div class="login_box">
                <div class="card-header"><tags:message code="reset.password"/> </div>
                <div class="card-block">
                    <form:form  class="m-t" id="password-reset-form" action="${context}/auth/reset-password" commandName="PasswordResetBean" onSubmit="return validatePassword()">
                        <form:hidden path="emailId" id="uea"/>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="reset.label.passcode"/></strong></label>
                            <div class="col-lg-7 col-md-12 col-sm-12"><input name="code" id="passcode" type="text" class="form-control  text-center" /></div>
                        </div>
                        <div class="form-group row clearfix">
                            <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"></label>
                            <div class="col-lg-7 col-md-12 col-sm-12"><button type="button" class="btn btn-primary btn-block" onclick="matchPasscode()"><tags:message code="reset.button.next"/></button></div>
                            <div id="mismatchMsgDiv" class="card-block"><tags:message code="reset.msg.invalid"/></div>                     
                        </div>                 
                        <div id="newPasswordDiv">
                            <div class="form-group row clearfix">
                                <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="reset.label.password"/></strong></label>
                                <div class="col-lg-7 col-md-12 col-sm-12">
                                    <form:input id="newPassword" path="newPassword" type="password" class="form-control" placeholder="Password"/>
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"><strong><tags:message code="reset.label.confirm.password"/></strong></label>
                                <div class="col-lg-7 col-md-12 col-sm-12">
                                    <form:input id="confirmPassword" path="confirmPassword" name="re-password" type="password" class="form-control" placeholder="Repeat Password"/>                                  
                                </div>
                            </div>
                            <div class="form-group row clearfix">
                                <label class="form-control-label col-lg-4 col-md-12 col-sm-12 text-right"></label>
                                <div class="col-lg-7 col-md-12 col-sm-12"><form:button type="submit" class="btn btn-primary btn-block"><tags:message code="reset.label.reset.password"/></form:button>
                                    </div>
                                </div>
                            </div>
                    </form:form>
                </div>              
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function showMessage(message) {
        $("#alert-box").html(message);
        $("#alert-box").show();
    }

    $(document).ready(function () {
        $("#newPasswordDiv").hide();
        $("#mismatchMsgDiv").hide();

    });

    function validatePassword() {
        var pass = $('#newPassword').val();
        var conPass = $('#confirmPassword').val();
        if (pass.length == 0) {
            showMessage('Please enter password!');
            return false;
        }
        if (pass.length < 6 || pass.length > 15) {
            showMessage('Password must be between 6 and 15 characters!');
            return false;
        }
        if (pass != conPass) {
            showMessage('Passwords do not match!');
            return false;
        }
        return true;
    }


    function matchPasscode() {
        var passcode = $("#passcode").val();
        var userMail = $("#uea").val();
        $.ajax({
            url: window.__NVLI.context + "/auth/match-passcode?passcode=" + passcode + "&userMail=" + userMail,
            cache: false,
            success: function (matchStatus) {
                if (matchStatus == 1) {//success
                    $("#newPasswordDiv").show();
                    $("#mismatchMsgDiv").hide();
                } else if (matchStatus == 0) {//error
                    $("#newPasswordDiv").hide();
                    $("#mismatchMsgDiv").show();
                }
            },
            error: function () {
                showMessage("Something went wrong.Please try again later!!");
            }
        });
    }

</script>
