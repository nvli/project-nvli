<%--
    Document   : profile
    Created on : Apr 1, 2016, 12:53:17 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
    Author     : Ruturaj Powar<ruturajp@cdac.in>
    Author     : Ankita Dhongde<dankita@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<link rel="stylesheet" type="text/css" href="${context}/styles/css/profile.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
    <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-12">
            <div class="card">
                <div class="card-header">
                    <tags:message code="profile.details"/>
                </div>
                <div class="card-block" >
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center; position: relative;">
                            <div class="profile-image-container">
                                <div class="reward-badge"></div>
                                <img src="${exposedProps.userImageBase}/${user.profilePicPath}"
                                     class="img-rounded"
                                     alt="${user.firstName} ${user.lastName}"
                                     onerror="this.src='${context}/images/profile-picture.png'"/>
                            </div>
                        </div>
                        <div class="profile-content col-md-12 col-sm-12 col-xs-12">
                            <div class="user-name-container">
                                <h3 class=""><strong>${user.firstName} ${user.lastName}</strong></h3>
                            </div>
                            <div class="user-handle-conainer">
                                <a href="${context}/user/profile"><small>@${user.username}</small></a>
                            </div>
                            <div class="user-rewards-conainer" id="user-rewards-conainer"></div>
                            <div id="reward-points-pregress" style="height: 150px"></div>
                            <div class="user-details-container">
                                <table class="table table-stripped">
                                    <tbody>
                                        <c:if test="${!empty(user.email)}">
                                            <tr>
                                                <td class="desc-label"><a data-tooltip="<tags:message code="label.email"/>"><i class="fa fa-envelope-o"></i></a></td>
                                                <td>${user.email}</td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${!empty(user.contact)}">
                                            <tr>
                                                <td class="desc-label"><a data-tooltip="<tags:message code="profile.tooltip.contact"/>"><i class="fa fa-phone"></i></a></td>
                                                <td>${user.contact}</td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${!empty(user.country)}">
                                            <tr>
                                                <td class="desc-label"><a data-tooltip="<tags:message code="label.address"/>"><i class="fa fa-map-marker"></i></a></td>
                                                <td>${user.city} ${user.state} ${user.country}</td>
                                            </tr>
                                        </c:if>
                                        <c:if test="${!empty(user.roles)}">
                                            <tr>
                                                <td class="desc-label"><a data-tooltip="<tags:message code="profile.tooltip.roles"/>"><i class="fa fa-key"></i></a></td>
                                                <td>
                                                    <c:forEach items="${user.roles}" var="role">
                                                        <a class="role-underlined">${role.name}</a> &nbsp;
                                                    </c:forEach>
                                                </td>
                                            </tr>
                                        </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div  class="text-xs-center small">
                        <a href="${context}/user/update" class="btn btn-sm btn-secondary">
                            <i class="fa fa-edit"></i> <tags:message code="profile.edit.profile"/>
                        </a>
                        <a href="${context}/user/account/settings" class="btn btn-sm btn-secondary">
                            <i class="fa fa-gears"></i> <tags:message code="profile.account.settings"/>
                        </a>
                        <a href="${context}/user/activity/log" class="btn btn-sm btn-secondary">
                            <i class="fa fa-table"></i> <tags:message code="profile.activity.logs"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-7 col-md-12">
            <div class="card">
                <div class="card-header"><tags:message code="profile.activity.logs"/><a href='${context}/user/activity/log' class="pull-right"><i class="fa fa-arrows-alt"></i></a>
                </div>
                <div class="feed-activity-list card-block"  id="activity-container" style="background:#FFFFFF;">
                </div>
            </div>
        </div>
    </div>
</div>
<!--  Progress level Modal Box-->
<div class="modal fade" id="progressLevelStat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left" id="exampleModalLabel">Rewards</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 2px;">
                    <span aria-hidden="true" style="color: #000;">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row" style="text-align: center;">
                        <div class="circle-container">
                            <div class="level-container">
                                <div class="circle-level pull-left">
                                    <span id="levelImageId" class="level-text"></span>
                                </div>
                                <div id="levelName"></div>
                            </div>
                            <div class="badge-container">
                                <div class="point-circle circle-badge pull-left" id="badge" ></div>
                                <div><strong><tags:message code="label.badge"/></strong></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h4 style="margin-bottom: 20px; font-weight: bold; color: #333; text-align: center;"><tags:message code="label.levelUP"/></h4>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3"><strong><tags:message code="label.editedPoints"/></strong></div>
                        <div class="col-xs-8 col-sm-8 col-md-7 col-lg-7">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" id="progressbaredited" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-1 col-lg-2" id="totaleditpoint"><input type="text"></input></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3"><strong><tags:message code="label.approvedPoints"/></strong></div>
                        <div class="col-xs-8 col-sm-8 col-md-7 col-lg-7">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" id="progressbarapproved" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div>
                             <div class="col-xs-4 col-sm-4 col-md-1 col-lg-2" id="totalapprovedpoint"><input type="text"></input></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-bordered table-striped table-container" style="background: rgba(240, 173, 78, 0.28); border: 1px solid #ccc;">
                                <tbody>
                                    <tr>
                                        <td><i class="fa fa-check-circle" style="color:#0099ff"></i> &nbsp; <tags:message code="label.approvedPoints"/></td>
                                        <td id="approvedPoints"></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-edit" style="color:#0099ff"></i> &nbsp; <tags:message code="label.editedPoints"/></td>
                                        <td id="editPoints"></td>
                                    </tr>
                                    <tr>
                                        <td> <i class="fa fa-certificate" style="color:#0099ff"></i> &nbsp;<tags:message code="label.certificateAwarded"/> </td>
                                        <td id="certificateAwarded"></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-star" style="color:#0099ff"></i> &nbsp;<tags:message code="label.starRewarded"/></td>
                                        <td id="starAwarded"> <i class="fa fa-star"></i></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="${context}/rewards/showRewards" role="button"> Show Reward history </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <a href='' class="section-profile-stats-dialog-learn-more blue-link" id='rewards-help' data-toggle="modal" data-target="#rewardsPointHelp" > <tags:message code="msg.rewardPointhelp"/> </a>
                            <div><tags:message code="msg.rewardPointMsg"/></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal"><tags:message code="button.close"/></button>
            </div>
        </div>
    </div>
</div>
<!--reward point help-->
<div class="modal fade" id="rewardsPointHelp"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document"  style="width:900px;">
        <div class="modal-content" >
            <div class="modal-header">
                <h5 class="modal-title pull-left" id="exampleModalLongTitle"><tags:message code="label.rewardHelpModaltitle"/></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 30px 60px;">
                <div class="tab1desc"> <h6> <tags:message code="msg.rewardModalDesc1"/> </h6></div>
                <table class="table table-bordered table-stripped">
                    <thead>
                        <tr>
                            <th class="tddesc"><tags:message code="label.contribution"/></th>
                            <th class="tddesc"><tags:message code="label.earnedPoints"/></th>
                            <th class="tddesc"><tags:message code="label.accuracyRange"/></th>
                        </tr>
                    </thead>
                    <tbody id="feed-table-contribution">
                    </tbody>
                </table>
                <div class='tabfooter'> <tags:message code="msg.rewardModalDesc2"/> </div>
                <div class="tab2head"><h5 align="center" ><strong><tags:message code="title.tableLavel"/></strong></h5></div>

                <div class="tab1desc"> <h6><tags:message code="msg.rewardModalDesc3"/></h6></div>
                <table class="table  table-bordered table-stripped">
                    <thead>
                        <tr>
                            <th class="tddesc" width="100"><tags:message code="title.tableLavel"/></th>
                            <th class="tddesc"><tags:message code="label.pointRequired"/></th>
                            <th class="tddesc"><tags:message code="label.requiredEdits"/></th>
                            <th class="tddesc"><tags:message code="label.starRewarded1"/></th>
                            <th class="tddesc"><tags:message code="label.certificateAwarded"/></th>
                        </tr>
                    </thead>
                    <tbody id="feed-table-level">
                    </tbody>
                </table>
                <div class='tabfooter'> <tags:message code="msg.rewardModalDesc4"/></div>
                <tags:message code="msg.rewardModalDesc5"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal"><tags:message code="button.close"/></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var pageNumber;
    var processing;
    var hasNext;
    var totalpages;
    var empty = false;
    function showMessage(message) {
        $("#alert-box").html(message);
        $("#alert-box").show();
    }
    function checkIfObjEmpty(jsonObj) {
        if (!jsonObj.activityLogs.length) {
            var generateHere = document.getElementById("feed-activity-list");
            if (pageNumber === 1) {
                generateHere.innerHTML = '<div class="card-block inner">No Activities Found</div>';
            } else {
                if (!empty) {
                    $(".feed-activity-list").append('<div style="text-align:center;" class="card-block inner">End of Activities</div>');
                    empty = true;
                }
            }
            hasNext = false;
        } else {
            hasNext = true;
        }
    }
    function renderActivityLogs(jsonObj) {
        processing = false;
        checkIfObjEmpty(jsonObj);
        showActivityLogs(jsonObj);
    }
    function fetchActivityLogs(userid) {
        var filter = "allActivities";
        $.ajax({
            url: "${context}/user/filter/activity/log/" + pageNumber + "/" + 15 + "/" + filter + "/" + userid,
            cache: false,
            success: renderActivityLogs,
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }
    $(document).ready(function () {
        var userid = "${user.id}";
        pageNumber = 1;
        // Fetch Activity Log Data
        fetchActivityLogs(userid);

        $("#activity-container").slimScroll({
            height: '700px',
            size: '8px',
            position: 'right',
            color: '#888',
            alwaysVisible: true,
            distance: '3px',
            railVisible: true,
            railColor: '#efefef',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: false
        });
        //Fetch Next Set Of Activity Logs After Scrolling 80% Of Screen
        $("#activity-container").scroll(function (e) {
            if (processing) {
                return false;
            }
            var itemsPerPage = 15;
            var unitContentHeight = $(".feed-element").height() + 16;
            var totalContentHeight = unitContentHeight * pageNumber * itemsPerPage;
            //            if (hasNext && $("#activity-container").scrollTop() >= (totalContentHeight - $(".slimScrollDiv").height())) {
            if (hasNext && $("#activity-container").scrollTop() >= ($(window).height() - $("#activity-container").height()) * 0.8) {
                processing = true;
                //Increment Page Count
                pageNumber++;
                // Fetch Activity Log Data
                fetchActivityLogs(userid);
            }
        });

        $('#rewardsPointHelp').on('show.bs.modal', function (e) {
            showRewardPointsDetails();
            showRewardLevelDetails();
        });


        fetchUserRewardSummery();

        $('#progressLevelStat').on('show.bs.modal', function (e) {
            var levelName = __NVLI.userRewardSummery.levelName;
            var levelId = __NVLI.userRewardSummery.levelId !== null ? __NVLI.userRewardSummery.levelId : 1;
            var starAwarded = __NVLI.userRewardSummery.starAwarded;
            var editPoints = __NVLI.userRewardSummery.editPoints;
            var approvedPoints = __NVLI.userRewardSummery.approvedPoints;
            var maxEditPoint = __NVLI.userRewardSummery.maxEditPoint;
            var maxApprovedPoint = __NVLI.userRewardSummery.maxApprovedPoint;
            var userId = __NVLI.userRewardSummery.userId;
            var editedPercentage = (editPoints / maxEditPoint) * 100;
            var aprrovedPercentage = (approvedPoints / maxApprovedPoint) * 100;
            var certificateAwarded;
            var levelvalueImage;
            if (levelId === 1) {
                levelvalueImage = 1;
            } else if (levelId === 15) {
                levelvalueImage = 3;
            } else {
                levelvalueImage = 2;
            }
            var levelImage = document.createElement("img");
            levelImage.src = __NVLI.context + "/images/icons/level-" + levelvalueImage + ".png";
            levelImage.style.width = "128px";
            levelImage.style.height = "128px";
            levelImage.className = "level-image";
            $("#progressLevelStat").find(".level-image").remove();
            $("div.circle-level").append(levelImage);

            if (__NVLI.userRewardSummery.certificateAwarded !== null && __NVLI.userRewardSummery.certificateAwarded.badgeCode !== "NONE") {
                badgeName = __NVLI.userRewardSummery.certificateAwarded.badgeName;
                badgeCode = __NVLI.userRewardSummery.certificateAwarded.badgeCode;
                var badgeNameI = badgeName.toLowerCase();
                var badgeImage = document.createElement("img");
                badgeImage.src = __NVLI.context + "/images/icons/" + badgeNameI + "-coin128.png";
                badgeImage.style.width = "128px";
                badgeImage.style.height = "128px";
                $("div.circle-badge").html(badgeImage);
                badgeImage.onerror = function () {
                    $(".badge-container").remove();
                };
                $("#progressLevelStat").find('.modal-body #certificateAwarded').html(badgeName + '&nbsp; <a href="' + __NVLI.context + '/rewards/certi/' + userId + '?type=img&badgeCode=' + badgeCode + '" target="_blank" ><i class="fa fa-file-image-o"></i> click </a> ');
            } else {
                certificateAwarded = 'NONE';
                $(".badge-container").remove();
                $("#progressLevelStat").find('.modal-body #certificateAwarded').html(certificateAwarded);
            }
            $("#levelImageId").addClass("level-text-" + levelvalueImage);

            $("#progressLevelStat").find('.modal-body #levelImageId').html(levelId);
            var maxEditPoint = __NVLI.userRewardSummery.maxEditPoint;
            var maxApprovedPoint = __NVLI.userRewardSummery.maxApprovedPoint;
            $("#progressLevelStat").find('.modal-body #levelName').html("<strong> Level " + levelId + "</strong>");
            $("#progressLevelStat").find('.modal-body #starAwarded').html(starAwarded);
            $("#progressLevelStat").find('.modal-body #editPoints').html(editPoints + " points");
            $("#progressLevelStat").find('.modal-body #approvedPoints').html(approvedPoints + " points");

            if (editPoints > 0) {
                $("#progressLevelStat").find('.modal-body #totaleditpoint').html(editPoints + " / " + maxEditPoint);
            }
            if (approvedPoints > 0) {
                $("#progressLevelStat").find('.modal-body #totalapprovedpoint').html(approvedPoints + " / " + maxApprovedPoint);
            }
            document.getElementById("progressbaredited").style["width"] = editedPercentage.toFixed(0) + "%";
            document.getElementById("progressbarapproved").style["width"] = aprrovedPercentage.toFixed(0) + "%";
            $("#progressbaredited").attr('aria-valuenow', editedPercentage);
            $("#progressbaredited").attr('aria-valuemax', maxEditPoint);
            $("#progressbarapproved").attr('aria-valuenow', aprrovedPercentage);
            $("#progressbarapproved").attr('aria-valuemax', maxApprovedPoint);
        });



    });

    function showRewardLevelDetails() {
        $.ajax({
            url: "${context}/rewards/getRewardsLevelList/",
            dataType: 'JSON',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                //$("#rewardsPointHelp").modal('show');
                renderLevelDetails(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function fetchUserRewardSummery() {
        $.ajax({
            url: "${context}/rewards/getUserRewardsSummery/",
            dataType: "JSON",
            type: "GET",
            success: function (userRewardSummery, textStatus, jqXHR) {
                console.log(userRewardSummery);
                __NVLI['userRewardSummery'] = userRewardSummery;
                var badgeName = 'NONE';
                if (userRewardSummery.certificateAwarded !== null) {
                    badgeName = userRewardSummery.certificateAwarded.badgeName;
                    var badgeNameI = badgeName.toLowerCase();
                    if (badgeNameI !== 'none') {
                        $("div.reward-badge").css({background: "url('" + __NVLI.context + "/images/icons/" + badgeNameI + "-coin64.png')"});
                    }
                }
                var tpl = _.template($("#tpl-reward-link").html());
                var opts = {
                    "level": userRewardSummery.levelName !== null ? userRewardSummery.levelName : 'level 1',
                    "levelId": userRewardSummery.levelId !== null ? userRewardSummery.levelId : 1,
                    "starAwarded": userRewardSummery.starAwarded,
                    "editPoints": userRewardSummery.editPoints,
                    "approvedPoints": userRewardSummery.approvedPoints,
                    "certificateAwarded": badgeName
                };
                var el = tpl(opts);
                $("#user-rewards-conainer").append(el);
                if (userRewardSummery.editPoints === 0) {
                    $("#reward-points-pregress").hide();
                } else {
                    var chart = initRewardProgess();
                    chart.series[0].update({name: userRewardSummery.levelName}, false);
                    chart.series[0].setData([userRewardSummery.editPoints]);
                    chart.redraw();
                }

            }, error: function (jqXHR, textStatus, errorThrown) {

            }
        });
    }

    function renderLevelDetails(jsonObj) {
        $("#rewardsPointHelp").find('.modal-body #feed-table-level').empty();
        _.each(jsonObj, function (levelDetail, index) {

            var tpl = _.template($("#tpl-table-level").html());
            var opts = {
                "level": levelDetail.levelName,
                "pointRequired": levelDetail.editPoints,
                "requiredAcceptedEdits": levelDetail.approvedPoints,
                "starAwarded": parseInt(levelDetail.starsAwarded.starsCode),
                "certificatesAwarded": levelDetail.badgeAwarded.badgeName
            };
            var el = tpl(opts);
            $("#rewardsPointHelp").find('.modal-body #feed-table-level').append(el);
        });
    }

    function showRewardPointsDetails() {
        $.ajax({
            url: "${context}/rewards/getRewardsPoinDetails/",
            dataType: 'JSON',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {

                //$("#rewardsPointHelp").modal('show');
                renderRewardsDetails(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function renderRewardsDetails(jsonObj) {
        $("#rewardsPointHelp").find('.modal-body #feed-table-contribution').empty();
        _.each(jsonObj, function (rewardDetail, index) {

            var tpl = _.template($("#tpl-table-contribution").html());
            var opts = {
                "contribution": rewardDetail.activityDesc,
                "earnedPoints": rewardDetail.earnedPoints,
                "lowerBound": rewardDetail.lowerBoundAccuracy > 0 ? rewardDetail.lowerBoundAccuracy : '',
                "upperBound": rewardDetail.upperBoundAccuracy > 0 ? rewardDetail.upperBoundAccuracy : ''
            };
            var el = tpl(opts);
            $("#rewardsPointHelp").find('.modal-body #feed-table-contribution').append(el);
        });
    }

    function showActivityLogs(jsonObj) {
        _.each(jsonObj.activityLogs, function (activity) {
            activity = JSON.parse(activity);
            if (!_.isEmpty(activity.recordImgPath) && activity.recordImgPath.indexOf(".jpg") >= 0) {
                showActivityLogWithImage(jsonObj, activity);
            } else {
                showNormalActivityLogs(jsonObj, activity);
            }
        });
    }
    function showNormalActivityLogs(jsonObj, activity) {
        var tpl = _.template($("#tpl-pagination-activityLogs").html());
        var opts = {
            user_profilePicPath: jsonObj.userProfilePicPath,
            user_firstName: jsonObj.userFirstName,
            user_lastName: jsonObj.userLastName,
            user_activity_activityText: activity.activityText,
            user_activity_activityTime: activity.activityTime
        };
        var el = tpl(opts);
        $(".feed-activity-list").children('div').detach(".inner");
        $(".feed-activity-list").append(el);
    }

    function showActivityLogWithImage(jsonObj, activity) {
        var tpl = _.template($("#tpl-visited-logs").html());
        var opts = {
            user_profilePicPath: jsonObj.userProfilePicPath,
            user_firstName: jsonObj.userFirstName,
            user_lastName: jsonObj.userLastName,
            user_activity_activityText: activity.activityText,
            user_activity_activityTime: activity.activityTime,
            recordId: activity.recordId,
            selectedPath: activity.recordImgPath
        };
        var el = tpl(opts);
        $(".feed-activity-list").children('div').detach(".inner");
        $(".feed-activity-list").append(el);
    }
</script>
<script type="text/template" id="tpl-table-contribution">
    <tr>
    <td class="tddesc">{{=contribution}}</td>
    <td class="tddesc1">{{=earnedPoints}}</td>
    <td class="tddesc1">{{=lowerBound}} &mdash; {{=upperBound }}</td>

    </tr>
</script>

<script type="text/template" id="tpl-table-level">
    <tr>
    <td class="tddesc">{{=level}}</td>
    <td class="tddesc1">{{=pointRequired}}</td>
    <td class="tddesc1">{{=requiredAcceptedEdits}}</td>
    <td class="tddesc1">{{ for (var i=0;i<starAwarded; i++){ }} <i class="fa fa-star" style="color: #ff9800;"></i> {{ } }} </td>
    <td class="tddesc1">{{=certificatesAwarded}}</td>
    </tr>
</script>

<script type="text/template" id="tpl-pagination-activityLogs">
    <div class="feed-element">
    <a href="#" class="pull-left">
    <img src="${exposedProps.userImageBase}/{{=user_profilePicPath}}" height="40px" class="profile_img img-circle"    onerror="this.src='${context}/images/profile-picture.png'"/>
    </a>
    <div class="media-body">
    <small class="pull-right text-navy">{{=user_activity_activityTime}}</small>
    <strong>
    {{=user_firstName}} {{=user_lastName}}
    </strong> {{=user_activity_activityText}} <br>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-visited-logs">
    <div class="feed-element">
    <a href="#" class="pull-left">
    <img src="${exposedProps.userImageBase}/{{=user_profilePicPath}}" height="40px" class="profile_img img-circle" onerror="this.src='${context}/images/profile-picture.png'"/>
    </a>
    <div class="media-body">
    <small class="pull-right text-navy">{{=user_activity_activityTime}}</small>
    <strong>
    {{=user_firstName}} {{=user_lastName}}
    </strong> {{=user_activity_activityText}} <br>
    </div>
    <div style="margin-left:48px;">
    <img style="margin:15px auto; height:200px !important; " id="cut_viewer_{{=recordId}}" src="{{=selectedPath}}" alt="visit-image" onerror="this.src='${context}/themes/images/no-preview-available.jpg'" class="img img-thumbnail">
    </div>
    </div>
</script>
<script type="text/template" id="tpl-reward-link">
    <div id="progressLevel" data-toggle="modal" data-target="#progressLevelStat" class="profile-progress-level">
    <span class="progress-level"><i class="fa fa-level-up"></i> Level {{=levelId}}</span>
    <span class="progress-star">{{=starAwarded}} <i class="fa fa-star-o"></i></span>
    </div>
</script>
<script>
    function initRewardProgess() {
        var chart = Highcharts.chart('reward-points-pregress', {
            chart: {
                type: 'bar',
                marginLeft: 5,
                marginBottom: 50
            },
            title: {
                text: 'Reward Points Earned',
                style: {
//                    display: 'none'
                }
            },
            subtitle: {
                text: 'Points >',
                align: 'right'
            },
            xAxis: {
                visible: false, categories: ['Points']
            },
            yAxis: {
                title: {text: null}
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                    showInLegend: false,
                    name: 's',
                    data: [0]
                }]
        });
        return chart;
    }
</script>

