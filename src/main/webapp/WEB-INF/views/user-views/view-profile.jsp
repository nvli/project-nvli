<%--
    Document   : view-user-profile
    Created on :May 27, 2016, 3.28 PM
    Author     : Bhumika Gupta
    Author     : Savita Kakad
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"  %>
<%@ page session="false"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div id="alert-box" class="alert alert-warning" style="display: none;"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><i class="fa fa-user"></i> Profile Detail </div>
                <div class="card-block">
                    <div class="row">
                        <div class="profile-image-container text-xs-center col-md-5"> 
                            <img src="${exposedProps.userImageBase}/${userObj.profilePicPath}"
                                 width="100%"
                                 class="img-rounded profile-view-image" 
                                 alt="${user.firstName} ${user.lastName}"
                                 onerror="this.src='${context}/images/profile-picture.png'"/>
                        </div>                        
                        <div class="profile-content col-md-7">
                            <div class="col-lg-12 col-md-12 col-sm-12 ">
                                <div class="table-responsive">
                                    <table class="table text-left">
                                        <tbody>                              
                                            <tr>
                                                <th class="text-right" width="200">Name</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <strong>${userObj.firstName} ${userObj.middleName} ${userObj.lastName}</strong>
                                                    </div>
                                                </td>
                                            </tr>                            
                                            <tr>
                                                <th class="text-right">E-mail Address</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <c:if test="${!empty(userObj.email)}">
                                                            <p></i> <span>${userObj.email}</span></p>
                                                        </c:if>
                                                    </div>
                                                </td>
                                            </tr>                            
                                            <tr>
                                                <th class="text-right">User Name</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <strong>${userObj.username}</strong>
                                                    </div>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">Gender</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <strong>${userObj.gender}</strong>
                                                    </div>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">Country</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <strong>${userObj.country}</strong>
                                                    </div> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">State</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <strong>${userObj.state}</strong>
                                                    </div> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="text-right">District</th>
                                                <td align="center">:</td>
                                                <td>
                                                    <div>
                                                        <strong>${userObj.district}</strong>
                                                    </div> 
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div  class="text-xs-center small">
                        <a style="margin-top: 10px" href="${context}/user/profile/update?userId=${userObj.id}&roleId=${roleId}" class="btn btn-sm btn-secondary btn-lg">
                            <i class="fa fa-edit"></i> Edit User
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>