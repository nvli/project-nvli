<%-- 
    Document   : user-bookmarks
    Created on : May 10, 2016, 3:04:42 PM
    Author     : Bhumika
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div>
    <div class="ibox-title">
        <h5>Bookmarks List</h5>
    </div>
    <table border="1">

        <c:forEach items="${bookmarkList}" var="user_bookmark">
            <tr>
                <td width="70%"> <c:if test="${user_bookmark.uniqueResIdentifier eq 'rep#lib#res#1'}"><img 
                            alt="image" 
                            class="feed-photo" 
                            src="http://brainconnection.brainhq.com/wp-content/uploads/2013/02/tree-library.jpg">
                        <a class="btn btn-xs btn-white" onclick="likeActivity('${user.email}', '1')" > Like </a> <a class="btn btn-xs btn-white" onclick="removeBookmark('${user_bookmark.uniqueResIdentifier}', '${user_bookmark.id}')" ><tags:message code="bookmark.remove"/></a> 
                    </c:if>
                    <c:if test="${user_bookmark.uniqueResIdentifier eq 'rep#lib#res#2'}">

                        <img 
                            alt="image" 
                            class="feed-photo" 
                            src="http://www.digitalbookworld.com/wp-content/uploads/tablet-library.jpg">
                        <a class="btn btn-xs btn-white" onclick="likeActivity('${user.email}', '2')" > Like </a> <a class="btn btn-xs btn-white" onclick="removeBookmark('${user_bookmark.uniqueResIdentifier}', '${user_bookmark.id}')" ><tags:message code="bookmark.remove"/> </a>
                    </c:if>
                    <c:if test="${user_bookmark.uniqueResIdentifier eq 'rep#lib#res#3'}">
                        <img 
                            alt="image" 
                            class="feed-photo" 
                            src="http://s3.amazonaws.com/libapps/accounts/431/images/ebooks.jpg">
                        <a class="btn btn-xs btn-white" onclick="likeActivity('${user.email}', '3')" > Like </a><a class="btn btn-xs btn-white" onclick="removeBookmark('${user_bookmark.uniqueResIdentifier}', '${user_bookmark.id}')" ><tags:message code="bookmark.remove"/> </a>

                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>

</div>
<script type="text/javascript">
    function likeActivity(emailId, resid) {
        $.ajax({
            url: window.__NVLI.context + "/user/like?res=" + resid,
            cache: false,
            success: function (resid) {
            },
            error: function () {
                console.log('Unsuccessful like!');
            }
        });
    }

    function removeBookmark(resId, bookmarkid) {
        window.location = window.__NVLI.context + "/user/removebookmark?bookmarkid=" + bookmarkid + "&resId=" + encodeURIComponent(resId);
    }
</script>