<%--
    Document   : owl
    Created on : Mar 22, 2017, 10:22:42 AM
    Author     : Hemant Anjana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@include file="../../static/components/pagination/pagination.html" %>
<link rel="stylesheet" type="text/css" href="${context}/themes/css/font-awesome.css">
<!-- Added for OWL-->
<link rel="stylesheet" type="text/css" href="${context}/components/webowl/css/webvowl.css" />
<link rel="stylesheet" type="text/css" href="${context}/components/webowl/css/webvowl.app.css" />        
<style>
    .intro {
        /*white-space: nowrap;*/ 
        width: 25em; 
        overflow: hidden;
        text-overflow: ellipsis;
        display:inline-block;
        font-size: 1.17em;
    }

    #recordPreviewPId a{ color:#0275d8 }
    #recordPreviewPId a:hover{ color:#111111 }
</style>
<script type="text/javascript">
    try {
        var AG_onLoad = function (func) {
            if (document.readyState === "complete" || document.readyState === "interactive")
                func();
            else if (document.addEventListener)
                document.addEventListener("DOMContentLoaded", func);
            else if (document.attachEvent)
                document.attachEvent("DOMContentLoaded", func)
        };
        var AG_removeElementById = function (id) {
            var element = document.getElementById(id);
            if (element && element.parentNode) {
                element.parentNode.removeChild(element);
            }
        };
        var AG_removeElementBySelector = function (selector) {
            if (!document.querySelectorAll) {
                return;
            }
            var nodes = document.querySelectorAll(selector);
            if (nodes) {
                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i] && nodes[i].parentNode) {
                        nodes[i].parentNode.removeChild(nodes[i]);
                    }
                }
            }
        };
        var AG_each = function (selector, fn) {
            if (!document.querySelectorAll)
                return;
            var elements = document.querySelectorAll(selector);
            for (var i = 0; i < elements.length; i++) {
                fn(elements[i]);
            }
            ;
        };
        var AG_removeParent = function (el, fn) {
            while (el && el.parentNode) {
                if (fn(el)) {
                    el.parentNode.removeChild(el);
                    return;
                }
                el = el.parentNode;
            }
        };
        window.uabInject = function () {};
    } catch (ex) {
        console.error('Error executing AG js: ' + ex);
    }
</script>
<script>
    //please set this variable ..its is used in js of webvowl
    var myJSONDATAURL = "${context}/components/webowl/data/";
    var myUSERID = "${user.id}";
    var myCONTEXT = "${context}";
    var SELECTED_ONT_Id = "";
    $(document).ready(function () {
        var totalCount;
        var termId, ontId;
        var pageLimit = 20;
        var arrLblList = new Array();
        var arrLblListForRecords = new Array();
        var arrRecordList = new Array();
        var isTaggingEnabled = false;

        $('body').on('click', 'circle', function () {
            $(".vowlGraph").find("circle").css('pointer-events', 'none');

            $('#recordCountPId').text("");
            $('#recordPreviewPId').text("");
            //termId = $(this).children("title").text();
            termId = $(this).parent("g").attr("id");
            ontId = $(".selected-ontology").children("a").attr("id");
            //$('#selection-details-trigger').attr('aria-expanded', 'true');
            //$('#selection-details').attr('aria-expanded', 'true');

            $('#selection-details').attr('class', 'accordion-container collapse');
            $('#selection-details').attr('aria-expanded', 'false');
            $('#selection-details').css('height', '0px');

            $('#tag-labels').empty();
            if ($("#user-id") !== undefined) {
                $('#img_process_lbls').show();
                clickedClassId = $(this).parent('g').attr('id');

                //make disable tag-name, submit-tag-name, enable-tagging disabled before featching records.
                $('#tag-name').prop('disabled', true);
                $('#submit-tag-name').prop('disabled', true);
                $('#enable-tagging').prop('disabled', true);

                $('#mylabels').show();
            } else {
                $('#mylabels').hide();
            }

            fetchCount('/visualize/getjsonforjsCount', {q: termId, refOntId: ontId}, false);
            $('#selection-details').attr('class', 'accordion-container collapse in');
            $('#selection-details').attr('aria-expanded', 'true');
            $('#selection-details').css('height', 'auto');
        });

        // get records from fuseki endpoint for clicked class.
        function  fetchAndRenderTableData(pageNo) {
            $('#recordPreviewPId').text("");
            $('#article-tbl-footer').empty();
            if (totalCount !== undefined && totalCount > 0) {
                console.log("${context}/visualize/getjsonforjs/" + pageNo + "/" + pageLimit);
                $.ajax({
                    type: 'GET',
                    data: {q: termId, refOntId: ontId},
                    url: "${context}/visualize/getjsonforjs/" + pageNo + "/" + pageLimit,
                    success: function (jsonObj) {
                        renderRecords(jsonObj, fetchAndRenderTableData, pageNo);

                        if ($("#user-id") !== undefined && pageNo === 1) {
                            fetchUserTags($('#user-id').attr('data-value'));

                            //make enable tag-name, submit-tag-name, enable-tagging disabled after featching records.
                            $('#tag-name').prop('disabled', false);
                            $('#submit-tag-name').prop('disabled', false);
                            $('#enable-tagging').prop('disabled', false);
                            $('#img_process_lbls').hide();


                        }

                        // on hover make class circles clickeble when requested for records completed.
                        $(".vowlGraph").find("circle").css('pointer-events', 'visiblePainted');
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            }
        }

        function fetchCount(strUlr, strData, isCustomTaggedRecords) {
            $('#img_process_results').show();

            $('#tag-name').prop('disabled', true);
            $('#submit-tag-name').prop('disabled', true);
            $('#enable-tagging').prop('disabled', true);
            $('.check-lbl').prop('disabled', true);
            $(".lbl_delete").css('pointer-events', 'none');

            $.ajax({
                type: 'GET',
                //async: false, //kept intentially because need variable set in success case.
                data: strData,
                traditional: true,
                url: "${context}" + strUlr,
                success: function (response) {
                    $('#classSelectionInformation').show();
                    $('#recordCountPId').text("");
                    $('#recordCountPId').text(response);
                    totalCount = response;
                    //if record count is greater than 0 then ajax call for records.
                    if (totalCount > 0) {
                        if (isCustomTaggedRecords) {
                            getTaggedRecordIdOfSelectedLbl(1);
                        } else {
                            fetchAndRenderTableData(1);
                        }
                    } else {
                        $('#img_process_lbls').hide();
                        $('#img_process_results').hide();
                        $(".vowlGraph").find("circle").css('pointer-events', 'visiblePainted');
                        var valuea = "<div class='enews_loader'>No Record Found.</div>";
                        $('#recordPreviewPId').text("");
                        $('#recordPreviewPId').append(valuea);
                        if (!isCustomTaggedRecords) {
                            fetchUserTags($('#user-id').attr('data-value'));
                        }
                    }

                    $('#tag-name').prop('disabled', false);
                    $('#submit-tag-name').prop('disabled', false);
                    if (arrLblListForRecords <= 0)
                        $('#enable-tagging').prop('disabled', false);
                    $('.check-lbl').prop('disabled', false);
                    $(".lbl_delete").css('pointer-events', 'visiblePainted');
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }

        /**
         * get list of custom tags for clicked class
         
         * @param {type} userId
         * @returns {undefined}         */
        function  fetchUserTags(userId) {
            $('#enable-tagging').prop('disabled', true);
            //$('#save-tagging').prop('disabled', true);
            $('#mylabels').show();
            $('#tag-labels').empty();
            $.ajax({
                type: 'GET',
                data: {refOntId: ontId, className: termId},
                url: "${context}/visualize/labels/" + userId,
                success: function (jsonObj) {
                    showMyLbls(jsonObj);
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }

        //get and render the tagged records for clicked custom lables
        function getTaggedRecordIdOfSelectedLbl(pageNo) {
            $.ajax({
                type: 'GET',
                traditional: true,
                data: {userID: $('#user-id').attr('data-value'), custIds: arrLblListForRecords},
                url: "${context}/visualize/get_tagged_records/" + pageNo + "/" + pageLimit,
                success: function (jsonObj) {
                    renderRecords(jsonObj, getTaggedRecordIdOfSelectedLbl, 1);

                    // on hover make class circles clickeble when requested for records completed
                    $(".vowlGraph").find("circle").css('pointer-events', 'visiblePainted');
                },
                error: function (errorThrown) {
                    console.error(errorThrown);
                }
            });
        }

        function renderRecords(jsonObj, callThisFunction, pageNo) {
            $('#recordPreviewPId').text("");
            var recordCount = parseInt((pageNo - 1) * pageLimit);
            var index = 1;
            _.each(jsonObj, function (key, value) {
                var valuea = "<li class='list-group-item'><input type='checkbox' class='check-record' id='" + value + "' style='visibility: hidden'><a href='${context}/search/preview/" + value + "' target='_blank' class='intro' title='" + key + "'>" + (recordCount + index) + ".&nbsp;" + key + "</a></li>";
                $('#recordPreviewPId').append(valuea);
                $('.check-record').unbind().bind("click", function () {
                    if ($(this).is(":checked")) {
                        arrRecordList.push($(this).attr('id'))
                    } else {
                        var removeItem = $(this).attr('id');
                        arrRecordList = jQuery.grep(arrRecordList, function (myvalue) {
                            return myvalue != removeItem;
                        });
                    }

                    //set save tagging button enable/disable.
                    if (arrLblList.length > 0 && arrRecordList.length > 0 && isTaggingEnabled) {
                        $('#save-tagging').attr('disabled', false);
                    } else {
                        $('#save-tagging').attr('disabled', true);
                    }
                });
                index++;
            });
            console.log(totalCount + " pageNo" + pageNo);
            renderPagination(callThisFunction, "article-tbl-footer", "page-btn", pageNo, totalCount, pageLimit, true, 1, true);
            if (isTaggingEnabled)
                $('.check-record').css('visibility', 'visible');
            $('#img_process_results').hide();

        }

        function addCustomLabel(userId, refOntClassId, customLabel) {
            console.log(userId + " " + refOntClassId + " " + customLabel);
            //must have some text for custom lable
            if (customLabel !== "") {
                $.ajax({
                    type: 'POST',
                    data: {userId: userId, refOntClassId: refOntClassId,
                        customLabel: customLabel, ontID: ontId, className: termId, lanuageCode: 'en'},
                    url: "${context}/visualize/label",
                    success: function (jsonObj) {
                        $("#tag-name").val('');
                        showMyLbls(jsonObj);
                        $('#' + termId).children('circle').css({"stroke": "green", "stroke-width": "5", "fill": "#80ffaa"});
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            }
        }

        //save request for generate tag
        $("#submit-tag-name").click(function () {
            addCustomLabel($('#user-id').attr('data-value'), clickedClassId, $("#tag-name").val());
        });

        //enabling tagging 
        $("#enable-tagging").click(function () {
            if ($('.check-lbl') !== undefined && $('.check-lbl').css('visibility') === 'hidden') {
                isTaggingEnabled = true;
                $(".lbl_delete").css('pointer-events', 'none');

                $('#enable-tagging').text("Disable Tagging");
                $('.check-lbl').css('visibility', 'visible');
                arrLblListForRecords = new Array();
            } else {
                isTaggingEnabled = false;
                $(".lbl_delete").css('pointer-events', 'visiblePainted');

                $('#enable-tagging').text("Enable Tagging");
                $('.check-lbl').css('visibility', 'hidden');

                $('.check-lbl').attr('checked', false);
                arrLblList = new Array();

                $('.check-record').attr('checked', false);
                arrRecordList = new Array();
            }

            if ($('.check-record') !== undefined && $('.check-record').css('visibility') === 'hidden') {
                $('.check-record').css('visibility', 'visible');
            } else {
                $('.check-record').css('visibility', 'hidden');
            }
        });

        //save custome tag with records.
        $("#save-tagging").click(function () {//bookmarks/multi
            saveTagging($('#user-id').attr('data-value'));
        });

        function saveTagging(uID) {
            $.ajax({
                type: 'POST',
                traditional: true,
                data: {userId: uID, custLblIds: arrLblList, recordIds: arrRecordList},
                url: "${context}/visualize/bookmarks/multi",
                success: function (data, textStatus, jqXHR) {
                    if (textStatus === "success") {

                        $('.check-lbl').attr('checked', false);
                        arrLblList = new Array();

                        $('.check-record').attr('checked', false);
                        arrRecordList = new Array();

                        $('#record-tag-msg').html("Record tagged successfully.");
                        $('#record-tag-msg').show('slow');
                        setTimeout(function () {
                            $('#record-tag-msg').hide('slow');
                        }, 3000);
                        $('.check-lbl').css('visibility', 'hidden');
                        $('.check-record').css('visibility', 'hidden');
                        $('#save-tagging').attr('disabled', true);
                        $('#enable-tagging').text("Enable Tagging");
                        isTaggingEnabled = false;
                    }
                }
            });
        }

        function deleteLbl(clicked_id) {
            //alert(clicked_id.substr(4));
            $.ajax({
                type: 'DELETE',
                url: "${context}/visualize/delete_my_custom_balel/" + $('#user-id').attr('data-value') + "/" + clicked_id.substr(4),
                success: function (data) {
                    $('#chk_lbl_' + clicked_id.substr(4)).parent().remove();

                    //console.log(clicked_id.substr(4) + "****** " + arrLblListForRecords.indexOf(clicked_id.substr(4)));

                    if (arrLblListForRecords.indexOf(clicked_id.substr(4)) > -1) {
                        arrLblListForRecords = jQuery.grep(arrLblListForRecords, function (value) {
                            return value != clicked_id.substr(4);
                        });
                    }

                    $('#record-tag-msg').html("Custom tag deleted successfully.");
                    $('#record-tag-msg').show('slow');
                    setTimeout(function () {
                        $('#record-tag-msg').hide('slow');
                    }, 3000);

                    //console.log('>>>>>>>>>>>>>>>>>>>>>>>>%%%%%%%%%%%%% ' + arrLblListForRecords);
                    //if no custom lbl is selected then get records for clicled class
                    if (arrLblListForRecords.length > 0) {
                        fetchCount('/visualize/get_tagged_record_count', {userID: $('#user-id').attr('data-value'), custIds: arrLblListForRecords}, true);
                        //getTaggedRecordIdOfSelectedLbl(1);
                    } else {
                        fetchCount('/visualize/getjsonforjsCount', {q: termId, refOntId: ontId}, false);
                        //fetchAndRenderTableData(1);
                    }

                }
            });
        }

        function showMyLbls(jsonObj) {
            $('#mylabels').show();
            $('#tag-labels').empty();
            var tagLists = '';
            _.each(jsonObj, function (value) {
                console.log(JSON.stringify(value));
                $('#enable-tagging').prop('disabled', false);
                tagLists = '<label class="label label-info" ><input type="checkbox" id="chk_lbl_' + value.customLabelId + '" class="check-lbl" style="visibility: hidden"> ' + value.customLabel + '&nbsp; &nbsp;<a href="#" id="lbl_' + value.customLabelId + '" class="lbl_delete" ><i class="fa fa-times" ></i></a></label>&nbsp; &nbsp;';
                $('#tag-labels').append(tagLists);
                $('#lbl_' + value.customLabelId).unbind().bind("click", function () {
                    deleteLbl($(this).attr('id'));
                });

                $('.check-lbl').unbind().bind("click", function () {
                    if (isTaggingEnabled) {
                        // collect checked custome lable ids
                        if ($(this).is(":checked")) {
                            arrLblList.push($(this).attr('id').substr(8));
                        } else {
                            var removeItem = $(this).attr('id').substr(8);
                            arrLblList = jQuery.grep(arrLblList, function (myvalue) {
                                return myvalue != removeItem;
                            });
                        }

                        //set save tagging button enable/disable.
                        if (arrLblList.length > 0 && arrRecordList.length > 0 && isTaggingEnabled) {
                            $('#save-tagging').attr('disabled', false);
                        } else {
                            $('#save-tagging').attr('disabled', true);
                        }
                    } else {
                        // collect checked custome lable ids for tagged record
                        if ($(this).is(":checked")) {
                            arrLblListForRecords.push($(this).attr('id').substr(8));
                        } else {
                            var removeItem = $(this).attr('id').substr(8);
                            arrLblListForRecords = jQuery.grep(arrLblListForRecords, function (myvalue) {
                                return myvalue != removeItem;
                            });
                        }

                        //change background-color of custom lbl on click
                        if (arrLblListForRecords.indexOf($(this).attr('id').substr(8)) > -1) {
                            $(this).parent().css("background-color", "red");
                        } else {
                            $(this).parent().css("background-color", "#5bc0de");
                        }

                        //if no custom lbl is selected then get records for clicled class
                        console.log('%%%%%%%%%%%%%%%%%%%% ' + arrLblListForRecords);
                        if (arrLblListForRecords.length > 0) {
                            fetchCount('/visualize/get_tagged_record_count', {userID: $('#user-id').attr('data-value'), custIds: arrLblListForRecords}, true);
                            //getTaggedRecordIdOfSelectedLbl(1);
                        } else {
                            fetchCount('/visualize/getjsonforjsCount', {q: termId, refOntId: ontId}, false);
                            //fetchAndRenderTableData(1);
                        }
                    }
                });
            });
        }

    });

</script>

<div id="main" class="container-fluid">
    <div class="clearfix" style="margin-bottom:20px;"></div>
    <div class="col-lg-12">

        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="card" style="width: 100%;">
                <div class="card-header ontoheader clearfix">
                    <nav id="optionsArea">
                        <ul id="optionsMenu">
                            <li id="select" class="${selectedOnt}"><a href="#">   <tags:message code="label.nvli.ontology"/></a>
                                <ul class="toolTipMenu select"  id="uid">
                                    <c:forEach items="${domainOnts}" var="domainOnt">
                                        <li id="${domainOnt.id}">
                                            <c:set var = "xmpFile" value = "${domainOnt.fileName}"/>
                                            <c:set var = "ontName" value = "${domainOnt.displayName}"/>
                                            <a href="#${fn:replace(xmpFile,'.xml','.json')}" id="${domainOnt.id}">${fn:toUpperCase(ontName)}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                            <li id="gravityOption" ><a href="">Gravity</a>
                                <ul class="toolTipMenu gravity">
                                    <li class="slideOption" id="classSliderOption"></li>
                                    <li class="slideOption" id="datatypeSliderOption"></li>
                                </ul>
                            </li>
                            <li id="filterOption" ><a href=""><tags:message code="label.Filters"/></a>
                                <ul class="toolTipMenu filter">
                                    <li class="toggleOption" id="datatypeFilteringOption"></li>
                                    <li class="toggleOption" id="objectPropertyFilteringOption"></li>
                                    <li class="toggleOption" id="subclassFilteringOption"></li>
                                    <li class="toggleOption" id="disjointFilteringOption"></li>
                                    <li class="toggleOption" id="setOperatorFilteringOption"></li>
                                    <li class="slideOption" id="nodeDegreeFilteringOption"></li>
                                </ul>
                            </li>

                            <li id="moduleOption" ><a href="#"><tags:message code="label.mode"/></a>
                                <ul class="toolTipMenu module">
                                    <li class="toggleOption" id="pickAndPinOption"></li>
                                    <li class="toggleOption" id="nodeScalingOption"></li>
                                    <li class="toggleOption" id="compactNotationOption"></li>
                                    <li class="toggleOption" id="colorExternalsOption"></li>
                                </ul>
                            </li>

                            <li id="pauseOption" ><a id="pause-button" href="#"><tags:message code="label.pause"/></a></li>
                            <li id="resetOption" ><a id="reset-button" href="#" type="reset"><tags:message code="button.Reset"/></a></li>
                            <li class="searchMenu pull-right" id="searchMenuId">
                                <input class="searchInputText form-control" type="text" id="search-input-text" placeholder='<tags:message code="label.search"/>'>
                                <ul class="searchMenuEntry" id="searchEntryContainer">
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
                <div class="onto_content">
                    <!--please don't change id of this div (for showing name of ontology)..its is used in js of webvowl-->
                    <div class="name_ontology" id="idOntologyName"></div>
                    <section id="canvasArea" >
                        <div id="loading-info">
                            <div id="loading-error" class="hidden">
                                <div id="error-info"></div>
                                <div id="error-description-button" class="hidden">Show error details</div>
                                <div id="error-description-container" class="hidden">
                                    <pre id="error-description"></pre>
                                </div>
                            </div>
                            <div id="loading-progress" class="hidden">
                                <span>Loading ontology... </span>
                                <div class="spin">&#8635;</div><br>
                                <div id="myProgress">
                                    <div id="myBar"></div>
                                </div>
                            </div>
                        </div>
                        <div id="graph" ></div>
                    </section>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="card ontologycontentacc">
                <aside id="detailsArea">
                    <section id="generalDetails">
                        <div  id="accordion1">
                            <!--<div id="ontology-metadata" class="accordion-container"></div>-->
                            <c:if test="${not empty user}">
                                <div id ="user-specific-tag" >
                                    <input type="hidden" id="user-id" data-value="${user.id}" />
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#mylabels" aria-expanded="true" aria-controls="Statistics" class="card-header accordion-toggle">My Labels<img id="img_process_lbls" src='${context}/themes/images/processing.gif' style="display: none"></a>
                                    <div class="accordion-container card-block" id="mylabels" style="display: none">
                                        <div class="row">
                                            <div class="col-md-9 m-b-1">
                                                <input type="text" id="tag-name" placeholder="My custom label" class="form-control">
                                            </div>
                                            <button type="button" id="submit-tag-name" class="btn btn-blue"><i class="fa fa-plus"></i></button>
                                        </div>

                                        <div class="clearfix m-t-1" id="tag-labels"></div>
                                        <button id ="enable-tagging" class="btn btn-sm btn-primary m-r-1" disabled="true"><tags:message code="label.enable.tagging.ontology"/></button>
                                        <button id="save-tagging" class="btn btn-sm btn-primary" disabled="true">Save</button>                  
                                    </div>
                                </div>
                                <span id="record-tag-msg" style="width: 100%;color: green;display:  none; text-align: center;background-color: #eee;padding: 5px;margin-bottom: 5px;">Record tagged successfully.</span>
                            </c:if>

                            <!--<a data-toggle="collapse" data-parent="#accordion1" href="#selectcount" aria-expanded="true" aria-controls="Statistics" class="card-header accordion-toggle">Caption Goes here..</a>
                            <div class="accordion-container card-block" id="selectcount">
                                <div class="row">
                                    <label class="form-control-label col-md-3 text-right"><strong>Caption</strong></label>
                                    <div class="col-md-9">
                                        <select class="form-control">
                                            <option>Option (20)</option>
                                            <option>Option (20)</option>
                                            <option>Option (20)</option>
                                            <option>Option (20)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>-->

                            <a data-toggle="collapse" data-parent="#accordion1" href="#Statistics" aria-expanded="false" aria-controls="Statistics" class="card-header accordion-toggle collapsed"><tags:message code="label.statistics"/></a>
                            <div class="accordion-container card-block collapse" id="Statistics" aria-expanded="false">
                                <table width="100%">
                                    <tr>
                                        <td width="25%">Classes: </td>
                                        <td width="25%"><span id="classCount"></span></td>
                                        <td width="25%">Object prop.:</td>
                                        <td width="25%"><span id="objectPropertyCount"></span></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Datatype prop.:</td>
                                        <td width="25%"><span id="datatypePropertyCount"></span></td>
                                        <td width="25%">Individuals:</td>
                                        <td width="25%"><span id="individualCount"></span></td>
                                    </tr>
                                    <tr>
                                        <td width="25%">Nodes:</td>
                                        <td width="25%"><span id="nodeCount"></span></td>
                                        <td width="25%">Edges:</td>
                                        <td width="25%"><span id="edgeCount"></span></td>
                                    </tr>                        

                                </table>
                            </div>
                            <a data-toggle="collapse" data-parent="#accordion1" href="#selection-details" id="selection-details-trigger" aria-controls="selection-details" class="card-header accordion-toggle collapsed"><tags:message code="label.results"/><img id="img_process_results" src='${context}/themes/images/processing.gif' style="display: none"></a>
                            <div class="accordion-container collapse" id="selection-details" aria-expanded="false">
                                <div id="classSelectionInformation" class="card-block" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td width="25%">Name:</td>
                                            <td width="25%"><span id="name"></span></td>
                                            <td width="25%">Type:</td>
                                            <td width="25%"><span id="typeNode"></span></td>
                                        </tr>

                                        <!--                                        <tr>
                                                                                    <td width="25%">Equiv.:</td>
                                                                                    <td width="25%"><span id="classEquivUri"></span></td>
                                                                                    <td width="25%">Disjoint:</td>
                                                                                    <td width="25%"><span id="disjointNodes"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="25%">Charac.:</td>
                                                                                    <td width="25%"><span id="classAttributes"></span></td>
                                                                                    <td width="25%">Description:</td>
                                                                                    <td width="25%"><span id="nodeDescription"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="25%">Comment.:</td>
                                                                                    <td width="25%"><span id="nodeComment"></span></td>
                                                                                    <td width="25%">Individuals:</td>
                                                                                    <td width="25%"><span id="individuals"></span></td>
                                                                                </tr>-->
                                        <tr>
                                            <td width="25%">Total Count:</td>
                                            <td width="25%"><span id="recordCountPId"></span></td>
                                        </tr>
                                    </table>
                                </div>

                                <div style="max-height: 670px !important;overflow-y: auto;overflow-x: hidden;height: 670px !important">
                                    <ul class="list-group" id="recordPreviewPId"></ul>
                                </div>

                                <div id="article-tbl-footer" class="card-footer clearfix"></div>
                            </div>
                            <p class="text-center clickforresukt"><br/><b>Click on any class to see result</b><br/><br/></p>
                        </div>
                    </section>
                </aside>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>
<script src="${context}/components/webowl/js/d3.min.js"></script>
<script src="${context}/components/webowl/js/webvowl.js"></script>
<script src="${context}/components/webowl/js/webvowl.app.js"></script>

<script>
    /*$.ajax({
     type: 'GET',
     url: "${context}/visualize/getjsonforDomainOnts",
     async: false,
     success: function (jsonObjData) {
     _.each(jsonObjData, function (key) {
     var varl = "<li><a href='#" + key['fileName'].replace("xml", "json") + "' id='" + key['namespaceURI'] + "#'>" + key['displayName'] + "</a></li>"
     $('#uid').append(varl);
     });
     },
     error: function (err) {
     console.log(err);
     }
     });*/
    window.onload = webvowl.app().initialize;
</script>
<script src="${context}/components/underscore/underscore.js"></script>

