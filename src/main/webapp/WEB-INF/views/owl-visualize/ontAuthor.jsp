<%-- 
    Document   : ontAuthor
    Created on : Jul 19, 2017, 10:52:51 AM
    Author     : Suhas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@include file="../../static/components/pagination/pagination.html" %>
<link rel="stylesheet" type="text/css" href="${context}/themes/css/font-awesome.css">
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<!-- Added for OWL-->
<link rel="stylesheet" type="text/css" href="${context}/components/webowl/css/webvowl.css" />
<link rel="stylesheet" type="text/css" href="${context}/components/webowl/css/webvowl.app.css" />  
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script>
    $(document).ready(function () {
        var totalCount, totalCoCount, recordCount;
        var pageLimit = 20;
        var alfaSort = 'all';
        var resType = 'all';
        var orgName = 'all';
        var publisherName = 'all';
        var authorName = '';
        var searchAuthor = 'all';
        var authorArr = '';
        getAuthorsCount('all', false);
        getAuthors(1);

        /**
         * Get Author count.
         * 
         * @param {type} auther
         * @param {type} isSearch
         * @returns {undefined}
         */
        function getAuthorsCount(auther, isSearch) {
            $.ajax({
                type: 'GET',
                //async: false, //kept intentially because need variable set in success case.
                data: {resType: resType, organization: orgName, publisher: publisherName, auther: auther, strAlfa: alfaSort},
                url: "${context}/visualize/getcountforallauthors",
                success: function (response) {
                    if (auther === 'all' || isSearch === true) {
                        totalCount = response;
                    } else {
                        totalCoCount = response;
                    }
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }

        function getAuthors(pageNo) {
            $('#author-list').html("<div class='enews_loader'><img src='${context}/themes/images/processing.gif'></div>");
            $('#co-author-list').empty();
            $('#co-author-tbl-footer').empty();
            $('#author-tbl-footer').empty();
            $.ajax({
                type: 'GET',
                data: {resType: resType, organization: orgName, publisher: publisherName, strAlfa: alfaSort, srchAuthor: searchAuthor},
                url: "${context}/visualize/getjsonforallauthors/" + pageNo + "/" + pageLimit,
                success: function (jsonObj) {
                    $('#author-list').empty();
                    if (JSON.parse(jsonObj).length === 0) {
                        $('#author-list').html("<div class='enews_loader'>No record found.</div>");
                    } else {
                        _.each(JSON.parse(jsonObj), function (value) {
                            var valuea = "<li><a href='#' class='onto_authorname' title='" + value + "'>" + value + "</a></li>";
                            $('#author-list').append(valuea);
                            $('.onto_authorname').unbind().bind("click", function () {//
                                $('#record-list').empty();
                                $('#record-tbl-footer').empty();
                                authorName = $(this).attr('title');
                                getAuthorsCount($(this).attr('title'), false);
                                getCoAuthors(1);
                            });
                            //index++;
                        });
                        renderPagination(getAuthors, "author-tbl-footer", "page-btn2", pageNo, totalCount, pageLimit, true, 1, true);
                    }
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }

        function getRecordCount(authors) {
            $.ajax({
                type: 'GET',
                async: false, //kept intentially because need variable set in success case.
                data: {authorIRI: authors, organizationIRI: 'ontId'},
                url: "${context}/visualize/getrecordcountforauthor",
                success: function (response) {
                    recordCount = response;
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }

        function getCoAuthors(pageNo) {
            $('#co-author-list').html("<div class='enews_loader'><img src='${context}/themes/images/processing.gif'></div>")
            $('#co-author-tbl-footer').empty();
            $('#co-authors-of').text('Co-author of ' + authorName);
            $.ajax({
                type: 'GET',
                data: {authorIRI: authorName},
                url: "${context}/visualize/getjsonforcoauthors/" + pageNo + "/" + pageLimit,
                success: function (jsonObj) {
                    var index = 0;
                    $('#co-author-list').empty();
                    $('#co-author-tbl-footer').empty();
                    $('#record-list').empty();
                    $('#record-tbl-footer').empty();
                    _.each(JSON.parse(jsonObj), function (value) {
                        var valuea = "";
                        index++;
                        if (index === 1) {
                            //Set first Co-Author name Checkbox Btdefault Checked. 
                            valuea = "<li><label for='author1' class='onto_co_authorname'><input id='authorIRI_" + index + "' name='authorIRI' type='checkbox' checked='true' class='co-author-check' value='" + value + "'><a title='" + value + "'>" + value + "</a></label></li>";
                        } else {
                            valuea = "<li><label for='author1' class='onto_co_authorname'><input id='authorIRI_" + index + "' name='authorIRI' type='checkbox' class='co-author-check' value='" + value + "'><a  title='" + value + "'>" + value + "</a></label></li>";
                        }
                        $('#co-author-list').append(valuea);
                        $('.co-author-check').unbind().bind("click", function () {
                            authorArr = "";
                            $('input:checkbox:checked.co-author-check').map(function () {
                                authorArr += "&authorIRI=" + this.value + "&";
                            }).get();
                            if (authorArr.length > 0) {
                                getRecordCount(authorArr.slice(0, -1));
                                fetchAndRenderTableData(1, authorArr);
                            } else {
                                $('#record-list').html("");
                            }
                        });

                        /*$('.onto_co_authorname').unbind().bind("click", function () {
                         $('#' + $(this).children().attr('id')).click();
                         });*/
                    });
                    renderPagination(getCoAuthors, "co-author-tbl-footer", "page-btn1", pageNo, totalCoCount, pageLimit, true, 1, true);
                    authorArr = "";
                    authorArr += "&authorIRI=" + authorName + "&";
                    getRecordCount(authorArr.slice(0, -1));
                    fetchAndRenderTableData(1, authorArr);
                },
                error: function (err) {
                    console.error(err);
                }
            });
        }

        function  fetchAndRenderTableData(pageNo) {
            $('#record-list').html("<div class='enews_loader'><img src='${context}/themes/images/processing.gif'></div>");
            $('#record-tbl-footer').empty(); //record-tbl-footer
            if (recordCount !== undefined && recordCount > 0) {
                console.log("${context}/visualize/getrecordsforauthor/" + pageNo + "/" + pageLimit);
                $.ajax({
                    type: 'GET',
                    data: {authorIRI: authorArr, organizationIRI: 'ontId'},
                    url: "${context}/visualize/getrecordsforauthor/" + pageNo + "/" + pageLimit,
                    success: function (jsonObj) {
                        $('#record-list').empty();
                        $('#record-tbl-footer').empty();
                        var rCount = parseInt((pageNo - 1) * pageLimit);
                        var index = 1;
                        _.each(jsonObj, function (key, value) {
                            var valuea = "<li class='list-group-item'><a href='${context}/search/preview/" + value + "' target='_blank' class='intro' title='" + key + "'>" + (rCount + index) + ".&nbsp;" + key + "</a></li>";
                            $('#record-list').append(valuea);
                            index++;
                        });
                        console.log(recordCount + " pageNo" + pageNo);
                        renderPagination(fetchAndRenderTableData, "record-tbl-footer", "page-btn3", pageNo, recordCount, pageLimit, true, 1, true);
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            }
        }

        //Load Organizations and Authors list on selection of Rsource type.
        $("#resourceType").change(function () {
            populateOrgName();
            $('#co-authors-of').text('Co-authors');
            $('#co-author-list').empty();
            $('#co-author-tbl-footer').empty();
            $('#record-list').empty();
            $('#record-tbl-footer').empty();
            resType = $('#resourceType :selected').text();
            if (resType === 'Select')
                resType = 'all';
            orgName = 'all';
            publisherName = 'all';
            getAuthorsCount('all', false);
            searchAuthor = 'all';
            getAuthors(1);
        });

        //Load Authors list on selection of Rsource type.
        $("#resourceCode").change(function () {
            $('#co-author-list').empty();
            $('#co-author-tbl-footer').empty();
            $('#co-authors-of').text('Co-authors');
            $('#record-list').empty();
            $('#record-tbl-footer').empty();
            resType = $('#resourceType :selected').attr('value');
            if (resType === 'select')
                resType = 'all';
            orgName = $('#resourceCode :selected').attr('value');
            if (orgName === 'select')
                orgName = 'all';
            publisherName = 'all';
            getAuthorsCount('all', false);
            searchAuthor = 'all';
            getAuthors(1);
        });

        $("#alfa-sort").change(function () {
            $('#co-author-list').empty();
            $('#co-author-tbl-footer').empty();
            $('#record-list').empty();
            $('#record-tbl-footer').empty();
            alfaSort = $('#alfa-sort :selected').text();
            if (alfaSort === 'Select')
                alfaSort = 'all';
            publisherName = 'all';
            getAuthorsCount('all', false);
            searchAuthor = 'all';
            getAuthors(1);
        });

        $("#srch-authors").devbridgeAutocomplete({
            paramName: 'subStr',
            serviceUrl: '${context}/visualize/getsuggestions/10',
            maxHeight: 190,
            minChars: 3,
            showNoSuggestionNotice: true,
            noSuggestionNotice: "Sorry,no matches found...",
            triggerSelectOnValidInput: false,
            onSelect: function (suggestion) {
                $('#author-list').empty();
                $('#co-author-list').empty();
                $('#co-author-tbl-footer').empty();
                $('#record-list').empty();
                $('#record-tbl-footer').empty();
                totalCount = 1;
                var valuea = "<li><a href='#' class='onto_authorname' title='" + suggestion.data + "'>" + suggestion.data + "</a></li>";
                $('#author-list').append(valuea);
                $('.onto_authorname').unbind().bind("click", function () {
                    $('#record-list').empty();
                    $('#record-tbl-footer').empty();
                    authorName = $(this).attr('title');
                    getAuthorsCount($(this).attr('title'), false);
                    getCoAuthors(1);
                });
                renderPagination(getAuthors, "author-tbl-footer", "page-btn2", 1, totalCount, pageLimit, true, 1, true);
            }
        });

        $("#publisherName").devbridgeAutocomplete({
            paramName: 'subStr',
            serviceUrl: '${context}/visualize/getPublishers/10',
            maxHeight: 190,
            minChars: 3,
            showNoSuggestionNotice: true,
            noSuggestionNotice: "Sorry,no matches found...",
            triggerSelectOnValidInput: false,
            onSelect: function (suggestion) {
                $('#resourceType').val('select');
                //populateOrgName();
                $('#co-author-list').empty();
                $('#co-author-tbl-footer').empty();
                $('#record-list').empty();
                $('#record-tbl-footer').empty();
                $('#co-authors-of').text('Co-authors');
                resType = 'all';
                orgName = 'all';
                publisherName = suggestion.data;
                getAuthorsCount('all', false);
                searchAuthor = 'all';
                getAuthors(1);
            }
        });

        function populateOrgName() {
            if ($('#resourceType :selected').text() !== "Select")
            {
                $.ajax({
                    url: "${context}/visualize/orglist/" + $('#resourceType :selected').attr('value'),
                    success: function (result) {
                        $('#resourceCode').find('option').remove().end();
                        $('#resourceCode').append("<option value='select'>Select</option>");
                        result.forEach(function (item, index) {
                            $('#resourceCode').append("<option value=" + item.orgCode + ">" + item.orgName + "</option>");
                        });
                    }});
            } else
            {
                $('#resourceCode').find('option').remove().end();
                $('#resourceCode').append("<option value='select'>Select</option>");
            }
        }

    });
</script>
<div class="container-fluid">
    <div id="main" class="m-t-3 m-l-3 m-r-3">

        <div class="col-md-12">
            <div class="col-lg-12">
                <div class="paper m-b-2 p-a-1 author_searchonto">
                    <div class="row">
                        <label class="col-lg-1 col-md-2 form-control-label text-right"><strong><tags:message code="label.resource-type"/></strong></label>
                        <div class="col-lg-2 col-md-4">
                            <select class="form-control" id="resourceType">
                                <option value="select">Select</option>
                                <c:forEach items="${resourceType}" var="resourceT">
                                    <option value="${resourceT}">${resourceT}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <label class="col-lg-1 col-md-2 form-control-label text-right"><strong><tags:message code="label.organization"/></strong></label>
                        <div class="col-lg-2 col-md-4">
                            <select  id="resourceCode" class="form-control">
                                <option value="select">Select</option>
                            </select>
                        </div>
                        <label class="col-lg-1 col-md-2 form-control-label text-right"><strong><tags:message code="label.publisher"/></strong></label>
                        <div class="col-lg-2 col-md-4">
                            <input type="text" id="publisherName" class="form-control" placeholder="Publishers..." autofocus="true">
                        </div>
                        <label class="col-lg-1 col-md-2 form-control-label text-right"><strong><tags:message code="label.search-author"/></strong></label>
                        <div class="col-lg-2 col-md-4">
                            <div class="input-group">
                                <input type="text" id="srch-authors" class="form-control" placeholder="Authors..." autofocus="true">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div><!-- /input-group -->
                        </div><!-- end of paper -->
                    </div>                 
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-9">
                <div class="row"> 
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <!-- Main Author list goes hear -->
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-5">
                                        <tags:message code="label.author"/>
                                    </div>                
                                    <label class="col-xs-4 txt-right m-a-0"><small><tags:message code="label.sort-by"/></small></label>
                                    <div class="col-xs-4 col-sm-3">
                                        <select id='alfa-sort' class="form-control form-control-sm">
                                            <option>Select</option>
                                            <option>A</option>
                                            <option>B</option>
                                            <option>C</option>
                                            <option>D</option>
                                            <option>E</option>
                                            <option>F</option>
                                            <option>G</option>
                                            <option>H</option>
                                            <option>I</option>
                                            <option>J</option>
                                            <option>K</option>
                                            <option>L</option>
                                            <option>M</option>
                                            <option>N</option>
                                            <option>O</option>
                                            <option>P</option>
                                            <option>Q</option>
                                            <option>R</option>
                                            <option>S</option>
                                            <option>T</option>
                                            <option>U</option>
                                            <option>V</option>
                                            <option>W</option>
                                            <option>X</option>
                                            <option>Y</option>
                                            <option>Z</option>                 
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div style="max-height: 785px !important;overflow-y: auto;overflow-x: hidden;height: 785px !important" class="card-block select_ontoauthor authoroverflow777">
                                <ul id="author-list"></ul>
                            </div>
                            <div id="author-tbl-footer" class="card-footer clearfix m-t-1"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <!-- Co-Author list goes hear -->
                        <div class="card">
                            <div class="card-header"><span id="co-authors-of"><tags:message code="label.co-authors"/></span></div>
                            <div style="max-height: 785px !important;overflow-y: auto;overflow-x: hidden;height: 785px !important" class="card-block select_ontoauthor authoroverflow777">
                                <ul id="co-author-list"></ul>
                            </div>   
                            <div id="co-author-tbl-footer" class="card-footer clearfix m-t-1"></div>
                        </div>
                    </div>

                </div>
            </div>



            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="card ontologycontentacc">
                    <aside id="detailsArea">
                        <section id="generalDetails">
                            <div  id="accordion1">                         
                                <a data-toggle="collapse" data-parent="#accordion1" href="#selection-details" aria-controls="selection-details" class="card-header accordion-toggle collapsed"><tags:message code="label.records"/></a>
                                <div class="accordion-container" id="selection-details">
                                    <div style="max-height: 785px !important;overflow-y: auto;overflow-x: hidden;height: 785px !important">
                                        <ul class="list-group" id="record-list">

                                        </ul>
                                    </div>
                                    <div id="record-tbl-footer" class="card-footer clearfix"></div>
                                </div>

                            </div>
                        </section>
                    </aside>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="clear22"></div>
    </div>
</div>