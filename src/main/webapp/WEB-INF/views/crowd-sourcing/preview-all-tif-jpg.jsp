<%--
    Document   : preview-all-tif-jpg
    Created on : Jul 13, 2016, 4:33:56 PM
    Author     : Doppa Srinivas
--%>

<%@page import="in.gov.nvli.util.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<c:set value="<%=Constants.ContentType.JPEG%>" var="jpeg"></c:set>
<c:set value="<%=Constants.ContentType.TIFF%>" var="tif"></c:set>
    <!DOCTYPE html>
    <div class="card m-a-1">
        <div class="card-header">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <c:choose>
                <c:when test="${not empty record.title && record.title ne null}">
                    ${record.title[0]}
                </c:when>
                <c:otherwise>
                    ${record.recordIdentifier}
                </c:otherwise>
            </c:choose>
        </a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
        <div class="card-block">
            <c:if test="${totalMedia eq 0}">
                <center>Content Not Avaliable</center>
                </c:if>
                <c:if test="${totalMedia ne 0}">
                    <c:choose>
                        <c:when test="${contentType eq jpeg}">
                        <div id="bookDiv">
                            <ul id="lstImages" class="imagesSource"></ul>
                            <div class="canvasHolder" id="canvasHolder"></div>
                            <div class="canvasHolder" id="canvasHolder1"></div>
                            <div id="fb-btn_div">
                                <div id="fb-prev" class="nxt-prev-btn iviewer_btn black"><img src="${context}/themes/images/BookReader/prev.jpg">
                                </div>
                                <div id="fb-nxt" class="nxt-prev-btn  iviewer_btn black"><img src="${context}/themes/images/BookReader/next.jpg" alt="" />
                                </div>
                                <div id="fb-zoom" class="nxt-prev-btn  iviewer_btn black"><img src="${context}/themes/images/BookReader/zoom-in.jpg">
                                </div>
                                <div id="fb-play-pause" class="nxt-prev-btn  iviewer_btn black"><img src="${context}/themes/images/BookReader/play-big.jpg" alt="" />
                                </div>
                                <span class="fontClass"><tags:message code="label.Image"/></span>
                                <input type="text" id="fb-pg" size="7" maxlength="7" value="1" />
                                <span class="fontClass" id="totalCnt"></span>
                                <input type="submit" id="fb-go" class="btn_disabled iviewer_btn btn-default" value="GO" disabled="disabled" />
                                <div id="errmsg"></div>
                            </div>
                            <!--  padding15 ends here -->
                            <!--  content-outer ends here -->
                        </div>
                    </c:when>
                    <c:when test="${contentType eq tif}">
                        <div class="wrapper">
                            <div id="viewer" class="viewer"></div>
                        </div>
                        <div class="row m-t-1" id="viewer-sidebar">
                            <div class=" marginauto">
                                <span id="in" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_in.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="out" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_out.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="orig" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_zero.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="fit" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.zoom_fit.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="rotateleft" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.rotate_left.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="rotateright" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/iviewer.rotate_right.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="prev" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/prev.png" width="24" height="24" alt="NVLI"> </span>
                                <span id="next" href="#" class="iviewer_button"><img src="${context}/themes/images/iviewer/next.png" width="24" height="24" alt="NVLI"> </span>
                            </div>
                        </div>
                    </c:when>
                </c:choose>
            </c:if>
        </div>
    </div>
</div>
