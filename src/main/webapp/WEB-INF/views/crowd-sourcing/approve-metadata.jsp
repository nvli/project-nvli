<%--
    Document   : approve-metadata
    Created on : May 12, 2016, 3:20:08 PM
    Author     : Doppa Srinivas
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="in.gov.nvli.util.Constants"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/customTag.tld" prefix="customTag"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set value="<%=Constants.MetadataStandardType.DUBLIN_CORE%>" var="dcType"></c:set>
<c:set value="<%=Constants.MetadataStandardType.MARC21%>" var="marc21Type"></c:set>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<link href="${context}/themes/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--canvasFlipbook css start-->
<link rel="stylesheet" href="${context}/themes/css/BookReader/canvasFlipbook.css" />
<!--canvasFlipbook css end-->
<!--iviewer css start-->
<link rel="stylesheet" href="${context}/themes/css/iviewer/jquery.iviewer.css">
<!--iviewer css end-->

<!--iviewer and canvasFlipBook js start-->
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery-migrate-1.2.1.js" ></script>
<script type="text/javascript" src="${context}/themes/js/jquery-ui.js"></script>
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery.mousewheel.js"></script>
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery.iviewer.js"></script>
<script type="text/javascript" src="${context}/themes/BookReader/canvasFlipbook.js"></script>
<!--iviewer and canvasFlipBook js end-->
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script src="${context}/components/viaf/src/jquery.viafauto.js" type="text/javascript"></script>
<script src="${context}/themes/js/clipboard/clipboard.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var recordIdentifier = '${crowdSourceBean.recordIdentifier}';
    var contentType = '${contentType}';
    $(document).ready(function () {
        getRecordContent();
        $('#leftTab').show();
        
        //repeated code start
        marc21TagSuggestions();
        marc21DescriptionSuggestions();
        //repeated code end

    });    
    function selectEditVersion(isOriginal, crowdSourceId)
    {
        $.ajax({
            url: '${context}/cs/getMetadata',
            data: 'recordIdentifier=${crowdSourceBean.recordIdentifier}' + '&isOriginal=' + isOriginal + '&crowdSourceId=' + crowdSourceId + '&metadataStandardType=${crowdSourceBean.originalMetadata.metadataType}&userType=expert&isEditFirstTime=false',
            type: 'POST',
            beforeSend: function () {
               $("#approve-metadata-loader").show();
            },
            complete: function ( ) {
               $("#approve-metadata-loader").hide();
            },
            success: function (response)
            {
                document.getElementById('tabId').innerHTML = response;
                addClassToDiscription();
            },
            error: function ()
            {
                alert('Service not available.Try after some time');
            }
        });
    }
    var selectedFilePath, currentPageNo, totalPages, tempTiffFile;
    function renderTiff(tiffFile, no, numPages) {
        selectedFilePath = "${pageContext.request.contextPath}/search/preview/all/renderTiff/" + recordIdentifier + "/" + no;
        tempTiffFile = tiffFile;
        currentPageNo = no;
        totalPages = numPages;
        $(function () {
            $('#viewer').show();
            console.log(numPages + " in tif" + tiffFile + " " + no);
            var iv1 = $("#viewer").iviewer({
                src: selectedFilePath
            });
            console.log("path.. " + selectedFilePath);
            iv1.iviewer('loadImage', selectedFilePath);
        });
    }
    function getRecordContent() {
        $.ajax({
            url: '${context}/cs/getRecordContent',
            data: 'recordIdentifier=' + recordIdentifier,
            type: 'POST',
            success: function (response)
            {
                document.getElementById('accordion').innerHTML = response;
                if (contentType === '<%=Constants.ContentType.TIFF%>') {
                    $(function () {
                        $.ajax(
                                {
                                    type: "POST",
                                    url: "${pageContext.request.contextPath}/search/preview/all/getTiffImagesCount/" + recordIdentifier,
                                    success: function (response) {
                                        renderTiff(recordIdentifier, 1, response);
                                    },
                                    error: function (e) {
                                        alert('Error while retreiving number of pages in a tiff file: ' + e + '<br/>Tiff File: ' + recordIdentifier);
                                    }
                                });
                    });
                    /*Preview of previous page*/
                    $("#prev").click(function () {
                        if (currentPageNo <= 1) {
                            alert('Previous page is not available.');
                        } else {
                            renderTiff(tempTiffFile, (currentPageNo - 1), totalPages);
                        }
                    });
                    /*Preview of next page*/
                    $("#next").click(function () {
                        if (currentPageNo >= (totalPages - 1)) {
                            alert('Next page is not available.');
                        } else {
                            renderTiff(tempTiffFile, (currentPageNo + 1), totalPages);
                        }
                    });
                    $("#viewer").on('mousewheel', function (e) {
                        //console.log("Wheel");
                        e.preventDefault();
                    });
                    /*
                     *Zoom in functionality
                     */
                    $("#in").click(function () {

                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('zoom_by', 1);
                    });
                    /*
                     *Zoom out functionality
                     */
                    $("#out").click(function () {
                        console.log("in click");
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('zoom_by', -1);
                    });
                    /*
                     *fit to screen functionality
                     */
                    $("#fit").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('fit');
                    });

                    /*
                     *original view functionality
                     */
                    $("#orig").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('set_zoom', 100);
                    });

                    /*
                     *Rotate Left functionality
                     */
                    $("#rotateleft").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('angle', -90);
                    });

                    /*
                     *Rotate Right functionality
                     */
                    $("#rotateright").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('angle', 90);
                    });
                } else {
                    function getWindowHeight() {
                        var myHeight = 0;
                        if (typeof (window.innerWidth) == 'number') {
                            //Non-IE
                            myHeight = window.innerHeight;
                        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                            //IE 6+ in 'standards compliant mode'
                            myHeight = document.documentElement.clientHeight;
                        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                            //IE 4 compatible
                            myHeight = document.body.clientHeight;
                        }
                        return myHeight;
                    }
                    var recordPathList = '${recordPathList}';
                    recordPathList = recordPathList.replace("[", "").replace("]", "").split(",");
                    for (var i = 0; i < recordPathList.length; i++) {
                        imgPathArr.push(recordPathList[i]);
                    }

                    imgCounter = imgPathArr.length;
                    if (imgCounter % 2 != 0) {
                        imgPathArr.push(recordPathList[0]);
                        imgCounter = imgCounter + 1;
                    }
                    imagePathJsonObject["play-big"] = "${context}/themes/images/BookReader/play-big.jpg";
                    imagePathJsonObject["pause-big"] = "${context}/themes/images/BookReader/pause-big.jpg";
                    imagePathJsonObject["closedhand"] = "${context}/themes/images/BookReader/closedhand.cur";
                    imagePathJsonObject["openhand"] = "${context}/themes/images/BookReader/openhand.cur";
                    $('#fb-pg').val("1");
                    $("#totalCnt").text("/ " + imgCounter);
                    load_Initial_Pages();
                }
            },
            error: function ()
            {
                alert('Error in showing the content');
            }
        });
    }
    function setEditStatusValue(checkBox) {
        if (checkBox.checked)
            $('#editStatus').val('yes');
        else
            $('#editStatus').val('no');
    }
    function openVideoMarking() {
        console.trace();
        top.location.href = __NVLI.context + "/cs/audioVideoMarking/${recordIdentifier}";
    }    
    
//    repeat same code in edit metadata start   
    function addClassToDiscription(){   
        $(".metadata-tag-description").each(function (){
            if($(this).data("tag")===100 && $(this).data("subfield")==="a"){
                $(this).addClass("author-name ui-autocomplete-input");
                bindAuthorClickEvent();
            }
            if($(this).data("tag-type")==="dublin-core" && $(this).data("tag")==="creator"){
                $(this).addClass("author-name ui-autocomplete-input");
                bindAuthorClickEvent();
            }
        });
    }
    
    function bindAuthorClickEvent(){
        $(".author-name").viafautox();
    }

    function addMarcTag(flag) {
        $('#addMarc21TagModal').modal('hide');
        if (flag) {
            var metadata_template = _.template($("#tpl-dataField-metadataFormRow").html());
            var recordStatusIndex = parseInt($("#recordStatusIndex").val());
            var datafieldStatusIndex;
            if (datafieldStatusIndex === 0) {
                datafieldStatusIndex = parseInt($("#datafieldStatusIndex").val());
            } else {
                datafieldStatusIndex = parseInt($("#datafieldStatusIndex").val()) + 1;
            }
            var subfieldStatusIndex = parseInt($("#subfieldStatusIndex").val());
            var opts = {
                recordStatusIndex: recordStatusIndex,
                datafieldStatusIndex: datafieldStatusIndex,
                subfieldStatusIndex: subfieldStatusIndex,
                tagCode: $("#tagCode").val(),
                subfieldCode: $("#subfieldCode").val(),
                tagValue: $("#tagValue").val(),
                description: $("#tagDescription").val()
            };
            var metadata_el = metadata_template(opts);
            $("#metadataDiv").append(metadata_el);
            $("#recordStatusIndex").val(recordStatusIndex);
            $("#datafieldStatusIndex").val(datafieldStatusIndex);
            $("#subfieldStatusIndex").val(subfieldStatusIndex);
        } else {
            var metadata_template = _.template($("#tpl-controlField-metadataFormRow").html());
            var recordStatusIndex = parseInt($("#recordStatusIndex").val());
            var controlfieldStatusIndex;
            if (controlfieldStatusIndex === 0) {
                controlfieldStatusIndex = parseInt($("#controlfieldStatusIndex").val());
            } else {
                controlfieldStatusIndex = parseInt($("#controlfieldStatusIndex").val()) + 1;
            }
            var opts = {
                recordStatusIndex: recordStatusIndex,
                controlfieldStatusIndex: controlfieldStatusIndex,
                tagCode: $("#tagCode").val(),
                tagValue: $("#tagValue").val(),
                description: $("#tagDescription").val()
            };
            var metadata_el = metadata_template(opts);
            $("#metadataDiv").append(metadata_el);
            $("#recordStatusIndex").val(recordStatusIndex);
            $("#controlfieldStatusIndex").val(controlfieldStatusIndex);
        }
        addClassToDiscription();
        marc21CloseRefresh();
    }

    function addDoublinCoreTag(tagName, tagValue) {
        $('#addDCTagModal').modal('hide');
        var metadata_dc_template = _.template($("#tpl-dc-metadataFormRow").html());
        var filedIndex = getDCFieldIndex(tagName.toLowerCase());      
        var opts = {
            tagName: tagName,
            fieldName: tagName.toLowerCase(),
            filedIndex: filedIndex,
            tagValue: tagValue
        };
        var metadata_dc_el = metadata_dc_template(opts);
        $("#metadataDiv").append(metadata_dc_el);
        addClassToDiscription();
        dcCloseRefresh();
    }
    
    function getDCFieldIndex(tagName){
        var counter=0;
        $('#metadataDiv > div').each(function () {
            if($(this).hasClass("dc-"+tagName)){
                ++counter;
            }
        });
        return counter;
    }

    function addMarc21Tag() {
        var dataFiled = true;
        var tagCodeValue = parseInt($("#tagCode").val());
        var tagDescriptionAttr = document.getElementById("tagDescription").hasAttribute("readonly");
        var tagCodeAttr = document.getElementById("tagCode").hasAttribute("readonly");
        var subfieldCodeAttr = document.getElementById("subfieldCode").hasAttribute("readonly");
        if ($("#tagDescription").val().length === 0)
        {
            alert("Select the description.");
        } else if (isNaN($("#tagCode").val()) && $("#tagCode").val().length === 0) {
            alert("Select valid tagcode.");
        } else if ($("#tagValue").val() === "") {
            alert('Please enter the tag value.');
        } else if ((tagDescriptionAttr && tagCodeAttr && subfieldCodeAttr) && (tagCodeValue < 10 && $("#subfieldCode").val().length === 0)) {
            addMarcTag(!dataFiled);
        } else if ((tagDescriptionAttr && tagCodeAttr && subfieldCodeAttr) && (tagCodeValue >= 10 && $("#subfieldCode").val().length !== 0)) {
            addMarcTag(dataFiled);
        } else {
            alert("Something going wrong,please reset");
        }
    }

    function addDCTag() {
        var e = document.getElementById("dc-tag-name");
        var tagName = e.options[e.selectedIndex].value;
        if (tagName.length === 0) {
            alert("Select the tag");
        } else if ($("#dcTagValue").val() === "") {
            alert('Enter the tag value');
        } else if (tagName.length !== 0 && $("#dcTagValue").val() !== "") {
            addDoublinCoreTag(tagName, $("#dcTagValue").val());
        } else {
            alert("Something going wrong,please reset.");
        }
    }  
    
    function isbnSearch(){
        $("#recordMetadata").hide();
        $("#isbn-search-metadata").show();
        $("#isbn-search-input").val(${isbn_no});
        validateISBN($("#isbn-search-input").val());
    }
    
    function returnToRecordMetadata(){
        $("#recordMetadata").show();
        $("#isbn-search-metadata").hide();
        $("#metadata-source-tabs").hide();
        $("#metadata-source-tabs-content").hide();
        $("#isbn-message").hide();       
    }
    
    function getMetadataKey(key){      
        key=key.replace(/_/g, " ");
        return key.charAt(0).toUpperCase()+key.slice(1);
    }
    
    function getMetadataValue(value){
        return typeof value[0]=== "string"?value[0].replace(/("|')/g, "") : value;
    }   
    
    function isbnMetadataSearch(){
        $("#metadata-source-tabs").hide();
        $("#metadata-source-tabs-content").hide();
        $("#isbn-message").hide();
        validateISBN($("#isbn-search-input").val());
    }
    
    function validateISBN(isbn){
        !_.isEmpty(isbn) && checkISBNLength(isbn) ? fetchMetadata(isbn) : enterValidISBN();
    }
    
    function enterValidISBN(){
        $("#isbn-message").removeClass().addClass("alert alert-warning m-a-1 text-center");
        $("#isbn-message").html("Enter 10 or 13 characters ISBN.");
        $("#isbn-message").show();
    }
    
    function checkISBNLength(isbn){
        isbn=isbn.replace(/-/g,"");
        return (isbn.length === 10) || (isbn.length === 13) ? true : false;
    }
    
    function beforeFetchMetadata(){
        // hide tabs
        $("#metadata-source-tabs").hide();
        //hide tabs content
        $("#metadata-source-tabs-content").hide();
        //show loader
        $("#isbn-metadata-loader").show();
        //hide message
        $("#isbn-message").hide();
    }
    
    function renderMetadataTabs(response){
        //maintain index to active first tab
        var index=0;
        //tabs template start here
        $("#metadata-source-tabs").empty();               
        var metadata_tab_template = _.template($("#tpl-metadata-source-tabs").html());                
        _.each(response,function (values,key){                    
            var opts = {
                index: ++index,
                key : key
            };
            var metadata_tab_el = metadata_tab_template(opts);
            $("#metadata-source-tabs").append(metadata_tab_el);
        });   
    }
    
    function renderMetadataTabsContent(response){
        //maintain index to active first tab
        var index=0;
        //tab content template start here
        $("#metadata-source-tabs-content").empty();
        var metadata_tab_content_template = _.template($("#tpl_metadata_tab_content").html());
        _.each(response,function (values,key){
            var opts = {
                index: ++index,
                key : key,
                values : values,
                imageURL : values.imageUrl                   
            };
            var metadata_tab_content_template_el = metadata_tab_content_template(opts);
            $("#metadata-source-tabs-content").append(metadata_tab_content_template_el);
        });    
    }
    
    function fetchMetadata(isbn){
        $.ajax({
            url: __NVLI.context+"/cs/fetchMetadata",
            data: {isbn:isbn},
            dataType: 'json',
            beforeSend: function(){
                // Handle the beforeSend event
                beforeFetchMetadata();
            },
            complete: function(){
                // Handle the complete event
                //hide loader
                $("#isbn-metadata-loader").hide();
            }
        })
            
            .done(function(response) {       
                if(!_.isEmpty(response)){
                    //render tabs                    
                    renderMetadataTabs(response);
                    //rendet tabs content
                    renderMetadataTabsContent(response);     
                    //show tabs
                    $("#metadata-source-tabs").show(); 
                    //show tab content
                    $("#metadata-source-tabs-content").show();
                    //instantiate clipboard.js
                    new Clipboard(".copy-metadata-value");
                }else{
                    $("#isbn-message").removeClass().addClass("alert alert-warning m-a-1 text-center");
                    $("#isbn-message").html("Sorry, we could not find any information for this book. Please try a different book.");
                    $("#isbn-message").show();
                }
            })
            .fail(function(xhr){
                $("#isbn-message").removeClass().addClass("alert alert-danger m-a-1 text-center");
                $("#isbn-message").html("Sorry,something went wrong. Please try again.");
                $("#isbn-message").show();
                console.log("error in fetchMetadata ::",xhr);        
            });        
    }
    
    function marc21TagSuggestions() {
        $('#tagCode').devbridgeAutocomplete({
            paramName: 'tagField',
            serviceUrl: '${context}/cs/getMarc21TagsSuggestions',
            maxHeight: 190,
            showNoSuggestionNotice: true,
            noSuggestionNotice: "Sorry,no matches found...",
            triggerSelectOnValidInput: false,
            formatResult: function (suggestion, currentValue) {
                return suggestion.data !== null ? (suggestion.value + "  " + suggestion.data + " [ " + suggestion.tagLibrarianTitle + " ] ") : (suggestion.value + " [ " + suggestion.tagLibrarianTitle + " ] ");
            },
            onSelect: function (suggestion) {
                document.getElementById('tagDescription').value = suggestion.tagLibrarianTitle;
                document.getElementById('subfieldCode').value = suggestion.data;
                $('#tagDescription').attr("readonly", "true");
                $('#tagCode').attr("readonly", "true");
            }
        });
    }
    $("#tagCode").ready(function () {
        $("#tagCode").on('input', function () {
            document.getElementById('tagDescription').value = "";
            document.getElementById('subfieldCode').value = "";
        });
    });
    function marc21DescriptionSuggestions() {
        $('#tagDescription').devbridgeAutocomplete({
            paramName: 'librarianTitle',
            serviceUrl: __NVLI.context + '/cs/getMarc21DescriptionSuggestions',
            maxHeight: 252,
            showNoSuggestionNotice: true,
            triggerSelectOnValidInput: false,
            noSuggestionNotice: "Sorry,no matches found...",
            onSelect: function (suggestion) {
                document.getElementById('tagCode').value = suggestion.data;
                document.getElementById('subfieldCode').value = suggestion.tagSubfield;
                $('#tagDescription').attr("readonly", "true");
                $('#tagCode').attr("readonly", "true");
            },
            formatResult: function (suggestion, currentValue) {
                return suggestion.tagSubfield != null ? (suggestion.value + " [ " + suggestion.data + "  " + suggestion.tagSubfield + " ] ") : (suggestion.value + " [ " + suggestion.data + " ] ");
            }
        });
    }
    $("#tagDescription").ready(function () {
        $("#tagDescription").on('input', function () {
            document.getElementById('tagCode').value = "";
            document.getElementById('subfieldCode').value = "";
        });
    });
    function marc21CloseRefresh() {
        $('#tagDescription').removeAttr("readonly");
        $('#tagCode').removeAttr("readonly");
        $('#subfieldCode').attr("readonly", "true");
        $("#tagCode").val("");
        $("#tagDescription").val("");
        $("#subfieldCode").val("");
        $("#tagValue").val("");
    }

    function dcCloseRefresh(){        
        $("#dcTagValue").val("");
    }

//    repeat same code in edit metadata end



</script>
<!--repeated Add Tag start-->
<script type="text/template" id="tpl-dataField-metadataFormRow">
    <div class="form-group row clearfix">
    <input type="hidden" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].tag" value="{{=tagCode}}"/>
    <input type="hidden" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].subfield[{{=subfieldStatusIndex}}].code" value="{{=subfieldCode}}"/>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12" style="text-transform: capitalize">{{=tagCode}}</label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12 ">{{=subfieldCode}}</label>    
    {{ if(tagCode==="100" && subfieldCode==="a"){ }}
    <label class="form-control-label col-lg-4 col-md-1 col-sm-12 ">{{=description}} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
    <div class="col-lg-6 col-md-6 col-sm-12">
    <input type="text" data-tag="{{=tagCode}}" data-subfield="{{=subfieldCode}}" class="form-control metadata-tag-description" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].subfield[{{=subfieldStatusIndex}}].value" value="{{=tagValue}}"/>
    {{ }else{ }}
    <label class="form-control-label col-lg-4 col-md-1 col-sm-12 ">{{=description}}</label>
    <div class="col-lg-6 col-md-6 col-sm-12">
    <input type="text" class="form-control" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].subfield[{{=subfieldStatusIndex}}].value" value="{{=tagValue}}"/>
    {{ } }}
    </div>
    </div>
</script>
<script type="text/template" id="tpl-controlField-metadataFormRow">
    <div class="form-group row clearfix">
    <input type="hidden" name="collectionType.record[{{=recordStatusIndex}}].controlfield[{{=controlfieldStatusIndex}}].tag" value="{{=tagCode}}"/>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12" style="text-transform: capitalize">{{=tagCode}}</label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12 ">---</label>
    <label class="form-control-label col-lg-4 col-md-1 col-sm-12 ">{{=description}}</label>
    <div class="col-lg-6 col-md-6 col-sm-12">
    <input type="text" class="form-control" name="collectionType.record[{{=recordStatusIndex}}].controlfield[{{=controlfieldStatusIndex}}].value" value="{{=tagValue}}"/>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-dc-metadataFormRow">
    <div class="form-group row clearfix dc-{{=fieldName}}" data-dc-tag="{{=fieldName}}">
        {{ if( fieldName === "creator"){ }}
            <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="{{=fieldName}}" style="text-transform: capitalize">{{=tagName}} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
        {{ }else{ }}
            <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="{{=fieldName}}" style="text-transform: capitalize">{{=tagName}}</label>
        {{ } }}
        <div class="col-lg-10 col-md-7 col-sm-12">
            <input type="text" class="form-control metadata-tag-description" data-tag="{{=fieldName}}" name="dcMetadata.{{=fieldName}}[{{=filedIndex}}]" value="{{=tagValue}}"/>
        </div>
    </div>   
</script>
<script type="text/template" id="tpl-metadata-source-tabs">
    <li class="nav-item">
        {{ if(index === 1){ }}
            <a class="nav-link active" data-target="\#{{=key}}-tab" data-toggle="tab">{{=key}}</a> 
        {{ } else { }}
            <a class="nav-link" data-target="\#{{=key}}-tab" data-toggle="tab">{{=key}}</a> 
        {{ } }}
    </li>
</script>
<script type="text/template" id="tpl_metadata_tab_content">
    {{ if(index === 1){ }}
        <div class="tab-pane nopad active" id="{{=key}}-tab">
    {{ } else { }}
        <div class="tab-pane nopad" id="{{=key}}-tab">
    {{ } }}  
        <div class="overflow685">            
            <div class="card-block">                
                {{ _.each(values,function(value,key){ key=getMetadataKey(key);}}
                    {{ if(key ==="ImageUrl"){ }}
                        <div class="m-a-1 text-center">
                            <img src="{{=value}}" height="200px"/>
                        </div>
                    {{ } }}
                {{ }); }}                
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Description</th>
                        <th>Value</th>
                        <th>Copy</th>
                      </tr>
                    </thead>
                    <tbody>
                        {{ _.each(values,function(value,key){ key=getMetadataKey(key); value=getMetadataValue(value); }}                   
                            {{ if(key !=="ImageUrl"){ }}
                                <tr>
                                  <td width="30%">{{=key}}</td>
                                  <td>{{=value}}</td>
                                  <td width="10%" align="center">
                                      <button class="btn btn-secondary copy-metadata-value clipboard" data-clipboard-text="{{=value}}" data-toggle="tooltip" data-placement="bottom" title="Copy" type="button">
                                          <i class="fa fa-clipboard"></i>
                                      </button>
                                  </td>
                                </tr>
                            {{ } }}
                        {{ }); }}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</script>
<!--modal to add marc21 tags start-->
<div class="modal fade" id="addMarc21TagModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="marc21CloseRefresh()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.Add.Marc21.Tag"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.description"/> <input  type="text" id="tagDescription" class="form-control" required/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.Tag"/>  : <input  type="text" id="tagCode" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.SubField"/> : <input type="text" readonly class="form-control" id="subfieldCode"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.TagValue"/> : <textarea name="textarea" id="tagValue" class="form-control" required="required" maxlength="100" placeholder="Maximum 100 characters allowed"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-primary" onclick="marc21CloseRefresh();"><tags:message code="button.Reset"/></button>
                <button type="button" class="btn btn-primary" onclick="addMarc21Tag();"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<!--modal to add marc21 tags start end-->
<!--modal to add dublin core tags start-->
<div class="modal fade" id="addDCTagModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="dcCloseRefresh()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.Add.DC.Tag"/></h4>
            </div>
            <div class="modal-body">                
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.Tag"/>  :                         
                        <select class="form-control" id="dc-tag-name">
                            <option  value="Contributors">Contributors</option>
                            <option  value="Coverage">Coverage</option>
                            <option  value="Creator">Creator</option>                           
                            <option  value="Date">Date</option>
                            <option  value="Description">Description</option>
                            <option  value="Format">Format</option>
                            <option  value="Identifier">Identifier</option>
                            <option  value="Language">Language</option>
                            <option  value="Publisher">Publisher</option>
                            <option  value="Relation">Relation</option>
                            <option  value="Rights">Rights</option>   
                            <option  value="Subject">Subject</option>
                            <option  value="Source">Source</option>
                            <option  value="Title">Title</option>
                            <option  value="Type">Type</option>                            
                        </select>
                    </div>
                </div>               
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.Value"/> : <textarea name="textarea" id="dcTagValue" class="form-control" required="required" maxlength="100" placeholder="Maximum 100 characters allowed"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-primary" onclick="dcCloseRefresh();"><tags:message code="button.Reset"/></button>
                <button type="button" class="btn btn-primary" onclick="addDCTag();"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<!--modal to add dublin core tags end-->
<!--repeated Add Tag end-->

<!--place of div changed for scroll in popup start-->
<div id="light" class="white_content">
    <div id="close_btn" style="background:url(${context}/themes/images/BookReader/noti_close.png)"></div>
    <!--next and previous zoom start-->
    <div id="next_btn" style="background:url(${context}/themes/images/iviewer/nextPopup.png)"></div>
    <div id="prev_btn" style="background:url(${context}/themes/images/iviewer/prevPopup.png)"></div>
    <!--next and previous zoom-->
</div>
<div id="fade" class="black_overlay"></div>
<!--place of div changed for scroll in popup end-->
<!-- Horizontal breadcrumbs -->
<ol class="breadcrumb">
    <li><a href="${context}/home"><i class="fa fa-home" aria-hidden="true"> </i></a></li>
    <security:authorize access="hasAnyRole('LIB_USER')">
        <li><a href="${context}/cs/metadata-curated-record-register/1/15"><tags:message code="sb.curated.records"/></a></li>
    </security:authorize>
</ol>
<div style="margin-bottom:0px;" class="clearfix"></div>
<!-- search result content starts here -->
<div class="container-fluid" id="main">
    <div style="margin-bottom:20px;" class="clearfix"></div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!--Breadcrumb-->
        <!--show hide panel-->
        <div id="accordion"></div>
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div >
            <div id="viewMetadata">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="card panel-default clearfix" id="recordMetadata">
                        <div class="card-header cardHeaderInputMargin">
                            <div class="row">
                                <div class="col-xl-6 col-lg-12">
                                    <tags:message code="label.existing.metadata"/>
                                </div>
                                <div class="col-xl-6 col-lg-12 text-right-cust m-a-0">
                                    <button class="btn btn-sm btn-primary" id="isbn-search-btn" onclick="isbnSearch()">
                                        <i class="fa fa-search"></i>
                                        <tags:message code="label.search.isbn"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <c:set var="underScore" value="_"/>
                            <ul class="nav nav-tabs" id="leftTab">
                                <li class="nav-item"> <a class="nav-link active" data-target="#originalTab" data-toggle="tab"><tags:message code="label.source"/></a> </li>
                                    <c:forEach items="${crowdSourceBean.crowdSourceEditBeanList}" var="crowdSourceEditBean" >
                                        <c:if test="${crowdSourceEditBean.isCuratedVersion eq 'true'}">
                                        <li class="nav-item"> <a class="nav-link" style="color:blue;" data-target="#${crowdSourceEditBean.crowdSourceEditId}" data-toggle="tab"><tags:message code="label.Curated"/></a> </li>
                                        </c:if>
                                        <c:if test="${crowdSourceEditBean.isCuratedVersion eq 'false'}">
                                        <li class="nav-item"> <a class="nav-link" data-target="#${crowdSourceEditBean.crowdSourceEditId}" data-toggle="tab"><tags:message code="label.crowdsource"/> V${crowdSourceEditBean.subEditNumber}</a> </li>
                                        </c:if>
                                    </c:forEach>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane nopad active" id="originalTab">
                                    <div class="overflow685">
                                        <div class="m-a-3"> <c:choose>
                                                <c:when test="${crowdSourceBean.originalMetadata.metadataType eq dcType}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold " ><tags:message code="label.TagName"/></label>
                                                        <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="dcMetadata" value="${crowdSourceBean.originalMetadata.dcMetadata}" />
                                                    <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                        <c:if test="${field.name ne 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="metadata">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                    <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${metadata}</span> </div>
                                                                </div>
                                                            </c:forEach>
                                                            <c:forEach items="${crowdSourceBean.originalMetadata.dcMetadata.others}" var="childMetadata">
                                                                <c:if test="${childMetadata.parentName eq field.name}">
                                                                    <c:forEach items="${childMetadata.tagValues}" var="otherMetadata">
                                                                        <div class="form-group row clearfix">
                                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${childMetadata.tagName}" style="text-transform: capitalize">${childMetadata.tagName}</label>
                                                                            <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                        </div>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${field.name eq 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="other">
                                                                <c:if test="${other.parentName eq 'NA'}">
                                                                    <c:forEach items="${other.tagValues}" var="otherMetadata">
                                                                        <div class="form-group row clearfix">
                                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                            <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                        </div>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:when>
                                                <c:when test="${crowdSourceBean.originalMetadata.metadataType eq marc21Type}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                        <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="collectionType" value="${crowdSourceBean.originalMetadata.collectionType}" />
                                                    <c:forEach items="${collectionType.record}" var="record">
                                                        <c:forEach items="${record.controlfield}" var="controlfield">
                                                            <div class="form-group row clearfix">
                                                                <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                                <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                                <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(controlfield.tag)}</label>
                                                                <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${controlfield.value}</span> </div>
                                                            </div>
                                                        </c:forEach>
                                                        <c:forEach items="${record.datafield}" var="datafield">
                                                            <c:forEach items="${datafield.subfield}" var="subfield">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${subfield.code}">${subfield.code}</label>
                                                                    <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${subfield.value}</span> </div>
                                                                </div>
                                                            </c:forEach>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                    <div class="card-footer clearfix" align="right">
                                        <div class="col-xs-offset-2 col-lg-10 col-sm-12">
                                            <input type="button" class="btn btn-primary" onclick="selectEditVersion(true, '${crowdSourceBean.crowdSourceId}');" value="Edit"/>
                                        </div>
                                    </div>
                                </div>
                                <c:forEach items="${crowdSourceBean.crowdSourceEditBeanList}" var="crowdSourceEditBean" >
                                    <div id="${crowdSourceEditBean.crowdSourceEditId}" class="tab-pane nopad">
                                        <div class="overflow685">
                                            <div class="m-a-3">
                                                <c:choose>
                                                    <c:when test="${crowdSourceEditBean.editedMetadata.metadataType eq dcType}">
                                                        <div class="form-group row clearfix">
                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.TagName"/></label>
                                                            <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                        </div>
                                                        <c:set var="dcMetadata" value="${crowdSourceEditBean.editedMetadata.dcMetadata}" />
                                                        <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                            <c:if test="${field.name ne 'others'}">
                                                                <c:forEach items="${dcMetadata[field.name]}" var="metadata" varStatus="loopCounter">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                        <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.dcMetadata[field.name][loopCounter.index]}" editedValue="${metadata}"/></span> </div>
                                                                    </div>
                                                                </c:forEach>
                                                                <c:forEach items="${crowdSourceEditBean.editedMetadata.dcMetadata.others}" var="childMetadata">
                                                                    <c:if test="${childMetadata.parentName eq field.name}">
                                                                        <c:forEach items="${childMetadata.tagValues}" var="otherMetadata" varStatus="loopCounter">
                                                                            <div class="form-group row clearfix">
                                                                                <label style="text-transform: capitalize" class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${childMetadata.tagName}">${childMetadata.tagName}</label>
                                                                                <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.dcMetadata.others.tagValues[loopCounter.index]}" editedValue="${otherMetadata}"/></span> </div>
                                                                            </div>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test="${field.name eq 'others'}">
                                                                <c:forEach items="${dcMetadata[field.name]}" var="other">
                                                                    <c:if test="${other.parentName eq 'NA'}">
                                                                        <c:forEach items="${other.tagValues}" var="otherMetadata" varStatus="loopCounter">
                                                                            <div class="form-group row clearfix">
                                                                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                                <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.dcMetadata[field.name].tagValues[loopCounter.index]}" editedValue="${otherMetadata}"/></span> </div>
                                                                            </div>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:when test="${crowdSourceEditBean.editedMetadata.metadataType eq marc21Type}">
                                                        <div class="form-group row clearfix">
                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                            <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                            <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                        </div>
                                                        <c:set var="collectionType" value="${crowdSourceEditBean.editedMetadata.collectionType}" />
                                                        <c:forEach items="${collectionType.record}" var="record" varStatus="outerCount1">
                                                            <c:forEach items="${record.controlfield}" var="controlfield" varStatus="count">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(controlfield.tag)}</label>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.collectionType.record[outerCount1.index].controlfield[count.index].value}" editedValue="${controlfield.value}"/></span> </div>
                                                                </div>
                                                            </c:forEach>
                                                            <c:forEach items="${record.datafield}" var="datafield" varStatus="outerCount2">
                                                                <c:forEach items="${datafield.subfield}" var="subfield" varStatus="count">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${subfield.code}" >${subfield.code}</label>
                                                                        <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                        <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.collectionType.record[outerCount1.index].datafield[outerCount2.index].subfield[count.index].value}" editedValue="${subfield.value}"/></span> </div>
                                                                    </div>
                                                                </c:forEach>
                                                            </c:forEach>
                                                        </c:forEach>
                                                    </c:when>
                                                </c:choose>
                                            </div>
                                        </div>
                                        <div class="card-footer clearfix" align="right">
                                            <div class="col-xs-offset-2 col-lg-10 col-sm-12">
                                                <input type="button"  class="btn btn-primary" onclick="selectEditVersion(false, '${crowdSourceEditBean.crowdSourceEditId}');" value="Edit"/>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <!--repeated code start-->
                    <div class="card panel-default clearfix" id="isbn-search-metadata" style="display: none;">
                        <div class="card-header cardHeaderInputMargin">
                            <div class="row">
                                <div class="col-xl-4 col-lg-12">
                                    <tags:message code="label.search.isbn"/>
                                </div>
                                <div class="col-xl-4">
                                    <div class="input-group" style="width:100%">
                                        <input class="form-control form-control-sm" placeholder="Enter ISBN" value="${isbn_no}" id="isbn-search-input">
                                        <a id="btn-isbn-search" href="javascript:isbnMetadataSearch();" class="input-group-addon" >
                                            <i class="fa fa-search"></i>                                   
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-12 text-right-cust m-a-0">
                                    <button class="btn btn-sm btn-primary" id="return-record-metadata" onclick="returnToRecordMetadata()">
                                        <i class="fa fa-mail-reply"></i> 
                                        <tags:message code="label.Recordmetadata"/>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div>                       
                            <ul class="nav nav-tabs" id="metadata-source-tabs" style="display: none;"></ul>                        
                            <div class="tab-content" id="metadata-source-tabs-content" style="display: none;"></div>
                            <div id="isbn-metadata-loader" style="display: none;">
                                <span style="text-align: center;display: block;">
                                    <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
                                </span>
                            </div>
                            <div id="isbn-message" style="display: none;"></div>
                        </div>
                    </div>
                    <!--repeated code end-->
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6" id="tabId">
                    <div id="approve-metadata-loader" style="display: none;">
                        <span style="text-align: center;display: block;">
                                <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- s result content container -->
<div style="margin-bottom:20px;" class="clearfix"></div>
