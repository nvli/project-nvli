<%--
    Document   : assigned-record-register
    Created on : Apr 25, 2016, 12:04:26 PM
    Author     : Ritesh Malviya
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script src="${context}/components/jquery/jquery-dateFormat.min.js"></script>
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title"><tags:message code="label.articles"/></strong>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <input id="searchString" class="form-control" placeholder="Search title">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                <select id="statusTypeFilter" class="form-control">
                    <option value="pending" selected><tags:message code="label.Pending"/></option>
                    <option value="published"><tags:message code="label.option.Published"/></option>
                </select>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                <select id="limitFilter" class="form-control">
                    <option value="10" selected>10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%"><tags:message code="label.serial.no"/></th>
                    <th><tags:message code="label.article.title"/></th>
                    <th width="25%"><tags:message code="label.author"/></th>
                    <th width="15%"><tags:message code="label.publishDate"/></th>
                </tr>
            </thead>
            <tbody id="article-tbl-body"></tbody>
        </table>
        <div id="article-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/template" id="tpl-article-tbl-row">
    {{ _.each(articleList, function (article,i) { }}
    <tr class="article-row" data-article-id="{{=article.id}}" style="cursor:pointer;">
    <td>{{=i+1+recordCount}}</td>
    <td>{{=article.articleTitle}}</td>
    <td>{{=article.user.firstName}} {{=article.user.lastName}}</td>
    <td>{{=$.format.date(new Date(article.publishDate), "dd MMM yyyy hh:mm:ss")}}</td>
    </tr>
    {{ }); }}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#statusTypeFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/cs/getArticlesByFiltersWithLimit',
            data: {searchString: $('#searchString').val(), statusTypeFilter: $('#statusTypeFilter').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo, isRequiredUserRestriction: false},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.articleList)) {
                        $("#article-tbl-body").html('<tr><td colspan="4" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-article-tbl-row").html());

                        var Opts = {
                            "articleList": jsonObject.articleList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };

                        var articleTableBody = articlesTpl(Opts);
                        $("#article-tbl-body").empty();
                        $("#article-tbl-body").html(articleTableBody);
                        $(".article-row").unbind().bind("click", function (e) {
                            window.location.href = "${context}/cs/manage/preview-article/" + e.currentTarget.getAttribute("data-article-id");
                        });
                        renderPagination(fetchAndRenderTableData, "article-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>