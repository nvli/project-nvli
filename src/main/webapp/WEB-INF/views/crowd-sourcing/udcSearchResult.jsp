<%--
    Document   : udcSearchResult
    Created on : Jul 2, 2016, 1:29:46 PM
    Author     : Suman Behara <sumanb@cdac.in>
--%>

<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<% ArrayList list = new ArrayList();%>
<div class="card-header clearfix">
    <tags:message code="label.udc.classification.results"/> (${webserviceSearchForm.resultSize})
    <span class="pull-right font120 font-weight-normal" id="pagelimitDiv">
        <span class="m-l-1 d_i_block">
            <tags:message code="label.per.page"/> <select id="limit">
                <option >25</option>
                <option>50</option>
                <option>100</option>
                <option>125</option>
            </select>
        </span>
    </span>
</div>
<div class="index_bg p-a-4 clearfix">
                <div class="font120 font-weight-normal" id="resourceTypelimitDiv">
                    <label class="col-xl-2 col-lg-3 col-xl-offset-7 col-lg-offset-4 col-md-3 col-sm-3" style="text-align: right;"> <tags:message code="label.resource"/></label>
                    <div class="col-xl-3 col-lg-5 col-md-9 col-sm-9">
                        <select id="resourceTypelimit" class="form-control">
                            <option>All</option>
                            <c:forEach items="${resourceTypeSearchResultMap}" var="resultsMap" varStatus="cc">
                                <option id="resourceTypeId">${resultsMap.key}(${resultsMap.value})</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
</div>
<div class="card-block overflow750" id="recordresultDiv">
    <c:choose>
        <c:when test="${webserviceSearchForm.resultSize ne 0}">
            <c:forEach items="${webserviceSearchForm.results}" var="resultsMap" varStatus="cc">
                <c:set value="${resultsMap.value}" var="searchResultResponse" />
                <c:if test="${searchResultResponse.resultSize ne 0}">
                    <c:forEach items="${searchResultResponse.listOfResult}" var="searchResult">
                        <div class="brdr_alltab " id="RecordResultId">
                            <table cellpadding="0" cellspacing="0" class="table table-bordered  bg-white tr_hand" id="tableId">
                                <tbody id="tbodyId">
                                    <tr class="col_head" id="trTitleId">
                                        <th width="140" class="text-right" id="thtitleId"><spring:message code="label.title"/></th>
                                        <td>
                                            <a href="${context}/search/preview/${searchResult.recordIdentifier}?language=${pageContext.response.locale}" target="_blank" class="link2">${searchResult.nameToView}</a>
                                        </td>
                                    </tr>
                                    <c:if test="${not empty searchResult.subject && searchResult.subject ne null}">
                                        <tr class="col_head" id="trSubjectId">
                                            <th class="text-right" id="thSubjectId"><spring:message code="label.subject"/></th>
                                                <td id="tdSubjectId">
                                                    <div class="overflow41hdn">
                                                        <c:forEach items="${searchResult.subject}" var="subject" varStatus="increment">
                                                            ${fn:trim(subject)}<c:if test="${increment.count ne searchResult.subject.size()}">&#44;</c:if>
                                                        </c:forEach>
                                                    </div>
                                                </td>
                                             </tr>
                                    </c:if>
                                    <c:set var="desc" value="${searchResult.description}" scope="request"/>
                                        <%
                                            list = (ArrayList) request.getAttribute("desc");
                                            if (list != null) {
                                                list.removeAll(Arrays.asList(null, ""));
                                            }
                                        %>
                                    <c:if test="${not empty searchResult.description && searchResult.description ne null}">
                                        <tr class="col_head" id="trDescId">
                                            <th class="text-right" id="thDescId"><spring:message code="label.description"/> </th>
                                            <td id="tdDescId">
                                                <div class="overflow41hdn">
                                                    <c:forEach items="${searchResult.description}" var="description" varStatus="increment">
                                                        ${description}
                                                    </c:forEach>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr class="col_head" id="trResourceTypeId">
                                        <th class="text-right" id="thResourceTypeId" width="40"><spring:message code="label.resource-type"/></th>
                                        <td id="tdResourceTypeId"><img src="${rTIconSearchLocation}/${mapOfResourceType[searchResult.resourceType].resourceTypeSearchIcon}" alt="${mapOfResourceType[searchResult.resourceType].resourceTypeSearchIcon}" id="resourceTypeIconId" height="26"/>${mapOfResourceType[searchResult.resourceType].resourceType}</td>
                                    </tr>
                                </tbody>
                            </table>
                                <span class="text-right font-bold"  id="searchResultCountId"> </span>
                            </div>
                            <div style="margin-bottom:15px;" class="clearfix" id="clearDivId"></div>
                    </c:forEach>
                </c:if>
            </c:forEach>
            </c:when>
            <c:otherwise>
                <center><tags:message code="label.no.found"/><br><tags:message code="label.Please.select.other.udc"/></center>
        </c:otherwise>
    </c:choose>
</div>
  
<div class="card-footer" id="cardfooterDIV">
    <c:if test="${webserviceSearchForm.resultSize ne 0}">
        <nav class="marginauto">
            <ul class="pagination " id="paginationId">
                <%--For displaying Previous link except for the 1st page --%>
                <c:choose>
                    <c:when test="${pageNum ne 1}">
                        <li class="page-item"> <a class="page-link pl"  data-page-num="1" data-page-window="${pageWin}" href="#">[1] </a> </li>
                        <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${pageNum-1}" data-page-window="${pageWin}" href="#"> <span aria-hidden="true">&laquo;</span> <span class="sr-only"><tags:message code="label.previous"/></span> </a> </li>
                        </c:when>
                        <c:otherwise>
                            <%-- <c:if test="${totalpages ne 0}">
                                 <li class="page-item disabled"> <a class="page-link"   aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Previous</span> </a> </li>
                             </c:if>--%>
                        </c:otherwise>
                    </c:choose>
                    <%--For displaying Page numbers. --%>
                    <c:choose>
                        <c:when test="${totalpages eq 0}">
                        <li class="page-item active"> <a class="page-link" > 1 <span class="sr-only">(current)</span></a> </li>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${totalpages lt 10}">
                                    <c:forEach begin="1" end="${totalpages}" var="i">
                                        <c:set var="lastCount" value="${i}"/>
                                        <c:choose>
                                            <c:when test="${pageNum eq i}">
                                            <li class="active page-item"> <a class="page-link" >${i} <span class="sr-only">(current)</span></a> </li>
                                            </c:when >
                                            <c:otherwise>
                                            <li class="page-item">
                                                <a class="page-link pl" data-page-num="${i}" data-page-window="${pageWin}" href="#">${i}</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <c:if test="${lastCount ne pageNum}">
                                    <li class="page-item"> <a class="page-link pl" href="#" aria-label="Next" data-page-num="${pageNum+1}" data-page-window="${pageWin}"> <span aria-hidden="true">»</span> <span class="sr-only"><tags:message code="label.next"/></span> </a></li>
                                    <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${totalpages}" data-page-window="${pageWin}" href="#">[${totalpages}] </a> </li>
                                    </c:if>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="begin" value="1"/>
                                    <c:set var="end" value="10"/>
                                    <c:if test="${(pageNum ge 5)}">
                                        <c:set var="begin" value="${pageNum-4}"/>
                                        <c:set var="end" value="${pageNum+4}"/>
                                    </c:if>
                                    <c:if test="${(totalpages-4) lt pageNum}">
                                        <c:set var="end" value="${totalpages}"/>
                                    </c:if>
                                    <c:forEach begin="${begin}" end="${end}" var="i">
                                        <c:set var="lastCount" value="${i}"/>
                                        <c:choose>
                                            <c:when test="${pageNum eq i}">
                                            <li class="active page-item"> <a class="page-link" >${i} <span class="sr-only">(current)</span></a> </li>
                                            </c:when >
                                            <c:otherwise>
                                            <li class="page-item">
                                                <a class="page-link pl" data-page-num="${i}" data-page-window="${pageWin}" href="#">${i}</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <c:if test="${lastCount ne pageNum}">
                                    <li class="page-item"> <a class="page-link pl" href="#" aria-label="Next" data-page-num="${pageNum+1}" data-page-window="${pageWin}"> <span aria-hidden="true">»</span> <span class="sr-only"><tags:message code="label.next"/></span> </a></li>
                                    <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${totalpages}" data-page-window="${pageWin}" href="#">[${totalpages}] </a> </li>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
            </ul>
        </nav>
        </div></c:if>
<script type="text/javascript">
    $(".pl").unbind().bind("click", function (e) {
        var requestURL = null;
        var data = null;
        var udcnotation = $("#udcnotationId").val();
        var graphicalUDCNotationValue = $("#graphicalUDCUnotation").val();
        var pageNum = e.currentTarget.getAttribute("data-page-num");
        var pageWin = e.currentTarget.getAttribute("data-page-window");
        var selectBox = document.getElementById("resourceTypelimit");
        var resource = selectBox.options[selectBox.selectedIndex].value;
        var udcDisplayTag = document.getElementById("udctagDispay");
        var udcDisplayView = udcDisplayTag.options[udcDisplayTag.selectedIndex].value;


        if (resource === 'All' && udcDisplayView === 'UDC Visualization')
        {
            requestURL = '${context}/search/across/all/udctagRecordSearch';
            data = {udcnotation: graphicalUDCNotationValue, pageNum: pageNum, pageWin: pageWin};
        } else if (resource === 'All' && udcDisplayView === 'UDC catalogue')
        {
            requestURL = '${context}/search/across/all/udctagRecordSearch';
            data = {udcnotation: udcnotation, pageNum: pageNum, pageWin: pageWin};
        } else if (resource !== 'All' && udcDisplayView === 'UDC Visualization')
        {
            requestURL = '${context}/search/specificresource/udctagRecordSearch';
            data = {udcnotation: graphicalUDCNotationValue, resource: resource, pageNum: pageNum, pageWin: pageWin};
        } else if (resource !== 'All' && udcDisplayView === 'UDC catalogue') {
            requestURL = '${context}/search/specificresource/udctagRecordSearch';
            data = {udcnotation: udcnotation, resource: resource, pageNum: pageNum, pageWin: pageWin};
        }
        $.ajax({
            type: 'GET',
            url: requestURL,
            data: data,
            cache: false,
            success: function (data) {
                $("#udcSearchResultDiv").html(data);
                $("#limit").val(pageWin);
                $("#resourceTypelimit").val(resource);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                (errorThrown);
            }
        });
    });
    $("#limit").change(function () {
        var pageWin = $("#limit").val();
        var udcnotation = $("#udcnotationId").val();
        var graphicalUDCNotationValue = $("#graphicalUDCUnotation").val();
        var selectBox = document.getElementById("resourceTypelimit");
        var resource = selectBox.options[selectBox.selectedIndex].value;
        var udcDisplayTag = document.getElementById("udctagDispay");
        var udcDisplayView = udcDisplayTag.options[udcDisplayTag.selectedIndex].value;
        if (graphicalUDCNotationValue.length === 0) {
            graphicalUDCNotationValue = document.getElementById("defaultParentId").value;

        }
        if (udcnotation.length === 0) {
            udcnotation = document.getElementById("defaultParentId").value;
        }
        $('#recordresultDiv').empty();
        if (resource === 'All' && udcDisplayView === 'UDC Visualization') {
            callRecordResult(graphicalUDCNotationValue, 'All', '1', pageWin);
        } else if (resource === 'All' && udcDisplayView === 'UDC catalogue') {
            callRecordResult(udcnotation, 'All', '1', pageWin);
        } else if (resource !== 'All' && udcDisplayView === 'UDC Visualization') {
            callSpecificResourceRecordResult(graphicalUDCNotationValue, resource, '1', pageWin);
        } else if (resource !== 'All' && udcDisplayView === 'UDC catalogue') {
            callSpecificResourceRecordResult(udcnotation, resource, '1', pageWin);
        }
    });
    $("#resourceTypelimit").change(function () {
        var pageWin = $("#limit").val();
        var udcnotation = $("#udcnotationId").val();
        var graphicalUDCNotationValue = $("#graphicalUDCUnotation").val();
        var selectBox = document.getElementById("resourceTypelimit");
        var resource = selectBox.options[selectBox.selectedIndex].value;
        var udcDisplayTag = document.getElementById("udctagDispay");
        var udcDisplayView = udcDisplayTag.options[udcDisplayTag.selectedIndex].value;
        if (graphicalUDCNotationValue.length === 0) {
            graphicalUDCNotationValue = document.getElementById("defaultParentId").value;

        }
        if (udcnotation.length === 0) {
            udcnotation = document.getElementById("defaultParentId").value;
        }
        $('#recordresultDiv').empty();
        if (resource === 'All' && udcDisplayView === 'UDC Visualization')
        {
            callRecordResult(graphicalUDCNotationValue, 'All', '1', pageWin);
        } else if (resource === 'All' && udcDisplayView === 'UDC catalogue')
        {
            callRecordResult(udcnotation, 'All', '1', pageWin);
        } else if (resource !== 'All' && udcDisplayView === 'UDC Visualization')
        {
            callSpecificResourceRecordResult(graphicalUDCNotationValue, resource, '1', pageWin);
        } else if (resource !== 'All' && udcDisplayView === 'UDC catalogue') {
            callSpecificResourceRecordResult(udcnotation, resource, '1', pageWin);
        }
    });
</script>