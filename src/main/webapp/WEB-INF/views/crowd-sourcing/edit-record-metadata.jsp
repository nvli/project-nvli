<%--
    Document   : edit-record-metadata
    Created on : Apr 25, 2016, 12:04:26 PM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="in.gov.nvli.util.Constants"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/customTag.tld" prefix="customTag"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@include file="../../static/components/pagination/pagination.html" %>
<c:set value="<%=Constants.MetadataStandardType.DUBLIN_CORE%>" var="dcType"></c:set>
<c:set value="<%=Constants.MetadataStandardType.MARC21%>" var="marc21Type"></c:set>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<meta name="referrer" content="origin-when-crossorigin">
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<link href="${context}/themes/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<!--iviewer css start-->
<link rel="stylesheet" href="${context}/themes/css/iviewer/jquery.iviewer.css">
<link rel="stylesheet" href="${context}/themes/css/BookReader/canvasFlipbook.css" />
<!--iviewer css end-->

<!--iviewer and canvasFlipBook js start-->
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery-migrate-1.2.1.js" ></script>
<!--<script type="text/javascript" src="${context}/themes/js/iviewer/jqueryui.js"></script>-->
<script type="text/javascript" src="${context}/themes/js/jquery-ui.js"></script>
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery.mousewheel.js"></script>
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery.iviewer.js"></script>
<script type="text/javascript" src="${context}/themes/BookReader/canvasFlipbook.js"></script>
<!--iviewer and canvasFlipBook js end-->
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src="${context}/themes/js/language/pramukhindic.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/languageData.js" charset="utf-8"></script>
<script type='text/javascript' src="${context}/themes/js/language/guessLanguage.js" charset="utf-8"></script>
<script src="${context}/components/viaf/src/jquery.viafauto.js" type="text/javascript"></script>
<script src="${context}/themes/js/clipboard/clipboard.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var nodeIds;
    var notation;
    var notationText;
    var i;
    var userType = '${userType.trim()}';
    var udcJson = {};
    var customTagsJson = {};
    var existingUDCOriginalTags = '${crowdSourceBean.crowdSourceUDCOriginalTags}';
    var existingUDCTagsAdded = '${crowdSourceBean.crowdSourceUDCTagsAdded}';
    var existingCustomTags = '${crowdSourceBean.crowdSourceCustomTags}';
    var recordIdentifier = '${crowdSourceBean.recordIdentifier}';
    var existingCustomTagsJsonObject = {};
    var existingUDCTagsJsonObject = {};
    var modifiedMetadataJson = {};
    var languageId = '40';
    var udcTagNotionsAdded = '';
    var contentType = '${contentType}';
    var _editMetadataFlag = false;
    var crowdSourceId = '${crowdSourceBean.crowdSourceId}';
    var crowdSourceEditId = '${crowdSourceEditBean.crowdSourceEditId}';
    var _allowedLanguagesArray = '${languageArray}'.split('&|&');
    if (existingUDCTagsAdded !== '' && existingUDCTagsAdded.length !== 0) {
        if (existingUDCOriginalTags !== '' && existingUDCOriginalTags.length !== 0)
            existingUDCOriginalTags = existingUDCOriginalTags + '&|&' + existingUDCTagsAdded;
        else
            existingUDCOriginalTags = existingUDCTagsAdded;
    }
    var _libraryDetails =${libraryDetailsMap};
    var _parameterMap = {};
    var _z3950Results = [];
    var _pageNo = 1;
    $(document).ready(function () {
        var version = '${version}';
        if (version !== '') {
            $("#alert-box").html('<div class="alert alert-danger">sorry!! the metedata version already present in ' + version + '</div>').fadeIn();
            $("#alert-box").fadeOut(30000);
        }
        setLanguage("selectCustomLanguage", "customAutocomplete");
        setLanguage("selectUDCLanguage", "udcAutocomplete");

        getRecordContent();
        $('#leftTab').show();
        stringToJSON(existingUDCOriginalTags, 'udc');
        stringToJSON(existingCustomTags, 'custom');
        addExistingTags(existingUDCTagsJsonObject, 'existingUDCTags', 'udc');
        addExistingTags(existingCustomTagsJsonObject, 'existingCustomTags', 'custom');
        getUDCTreeviewData();
        marc21TagSuggestions();
        marc21DescriptionSuggestions();
        if ("${crowdSourceBean.isRecordEditable}" === 'false') {
            if ("${curationStatus}" === 'curated') {
                $("#alert-box").html('<div class="alert alert-success">You have already curated this record.</div>').fadeIn();
                $("#alert-box").fadeOut(30000);
            } else {
                $("#alert-box").html('<div class="alert alert-danger">Maximum crowdsource edit limit is reached..please wait for Expert aproval.</div>').fadeIn();
                $("#alert-box").fadeOut(30000);
            }
        }
        if ('${status}' === 'true') {
            if ("${curationStatus}" === 'curated') {
                $("#alert-box").html('<div class="alert alert-success">You have successfully curated the record.</div>').fadeIn();
                $("#alert-box").fadeOut(30000);
            } else {
                $("#alert-box").html('<div class="alert alert-success">You have successfully crowdsourced the record.</div>').fadeIn();
                $("#alert-box").fadeOut(30000);
            }
        } else if ('${status}' === 'false') {
            $("#alert-box").html('<div class="alert alert-danger">Failed crowdsource/curated the record.</div>').fadeIn();
            $("#alert-box").fadeOut(30000);
        }

        $("#zSearch-modal").on("hidden.bs.modal", function () {
            $(".search-parameter-value").val("");
            $("#msg-div").empty();
            $("#z3950-search").show();
            $("#btn-z-search").show();
            $("#z3950-results").hide();
            $("#marc-detail").hide();
            $("#btn-back-z-search").hide();
            $("#btn-back-z-search-results").hide();
            actionAllTargets(false);
        });
    });
    function addClassToDiscription() {
        $(".metadata-tag-description").each(function () {
            if ($(this).data("tag") === 100 && $(this).data("subfield") === "a") {
                $(this).addClass("author-name ui-autocomplete-input");
                bindAuthorClickEvent();
            }
            if ($(this).data("tag-type") === "dublin-core" && $(this).data("tag") === "creator") {
                $(this).addClass("author-name ui-autocomplete-input");
                bindAuthorClickEvent();
            }
        });
    }

    function bindAuthorClickEvent() {
        $(".author-name").viafautox();
    }

    function openVideoMarking() {
        console.trace();
        top.location.href = __NVLI.context + "/cs/audioVideoMarking/" + recordIdentifier;
    }
    function selectEditVersion(isOriginal, crowdSourceId)
    {
        $.ajax({
            url: '${context}/cs/getMetadata',
            data: 'recordIdentifier=' + recordIdentifier + '&crowdSourceId=' + crowdSourceId + '&metadataStandardType=${crowdSourceBean.originalMetadata.metadataType}&userType=${userType}&isEditFirstTime=${isEditFirstTime}' + '&isOriginal=' + isOriginal,
            type: 'POST',
            beforeSend: function (xhr) {
                $("#edit-metadata-loader").show();
            },
            complete: function () {
                $("#edit-metadata-loader").hide();
            },
            success: function (response, textStatus, responseHeader)
            {
                var status = responseHeader.getResponseHeader('status');
                if (status === 'success') {
                    document.getElementById('tabId').innerHTML = response;
                    setLanguageAdvancedSearch("selectCurationLanguage");
                    _editMetadataFlag = true;
                    addClassToDiscription();
//                    uncomment below code to restrict paste into input from other language
//                    $("input").bind("paste", function (e) {
//                        // access the clipboard using the api
//                        var pastedData = e.originalEvent.clipboardData.getData('text');
//                        guessLanguage.info(pastedData, function (language) {
//                            if (!_.contains(_allowedLanguagesArray, language[0]) && language[0] !== "unknown")
//                            {
//                                alert(language[2] + " not allowed for this record.");
//                                e.preventDefault();
//                            } else
//                            {
//                                guessLanguage.info(e.currentTarget.value + pastedData, function (lang) {
//                                    if (!_.contains(_allowedLanguagesArray, lang[0]) && lang[0] !== "unknown") {
//                                        alert(lang[2] + " not allowed for this record.");
//                                        e.preventDefault();
//                                    }
//                                });
//                            }
//                        });
//                    });
                    modifiedField();
                } else
                    alert('Service not available.Try after some time');
            },
            error: function ()
            {
                alert('Service not available.Try after some time');
            }
        });
    }

    function addTags(tagsJson, div, tagType)
    {
        document.getElementById(div).innerHTML = '';
        if (!checkJsonIsEmpty(tagsJson))
        {
            var divHtml = '';
            $.each(tagsJson, function (keyName, value) {
                if (tagType === 'custom')
                    divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" onclick="deleteTag(\'' + keyName + '\',\'' + div + '\',\'' + tagType + '\');" title="' + value + '"> <span class="fa fa-remove"></span>  ' + value + ' </button>';
                else
                    divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" onclick="deleteTag(\'' + keyName + '\',\'' + div + '\',\'' + tagType + '\');" title="' + value + '"> <span class="fa fa-remove"></span>  ' + keyName + '   ' + value + ' </button>';
            });
            if (tagType === 'custom')
            {
                document.getElementById('customSaveButton').innerHTML = '<input type="button" class="btn btn-primary" onclick="saveTags(\'' + tagType + '\');" value="Save"/>';
                if (checkJsonIsEmpty(existingCustomTagsJsonObject))
                    document.getElementById('existingCustomTags').innerHTML = '';
            } else
            {
                if (userType === 'normal') {
                    document.getElementById('udcSaveButton').innerHTML = '<input type="button" class="btn btn-primary" onclick="saveTags(\'' + tagType + '\');" value="Save"/>';
                }
                if (checkJsonIsEmpty(existingUDCTagsJsonObject))
                    document.getElementById('existingUDCTags').innerHTML = '';
            }
            document.getElementById(div).innerHTML = divHtml;
        } else
        {
            if (tagType === 'custom')
            {
                if (checkJsonIsEmpty(existingCustomTagsJsonObject))
                    document.getElementById('existingCustomTags').innerHTML = 'No Tags Available';
                document.getElementById('customSaveButton').innerHTML = '';
            } else
            {
                if (checkJsonIsEmpty(existingUDCTagsJsonObject))
                    document.getElementById('existingUDCTags').innerHTML = 'No Tags Available';
                if (userType === 'normal') {
                    document.getElementById('udcSaveButton').innerHTML = '';
                }
            }
        }
    }

    function deleteTag(key, div, tagType)
    {
        if (tagType === 'udc')
        {
            if (div === 'existingUDCTags') {
                delete existingUDCTagsJsonObject[key];
                addExistingTags(existingUDCTagsJsonObject, div, tagType);
            } else {
                delete udcJson[key];
                addTags(udcJson, div, tagType);
            }
            var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[key]);
            $('#udcTreeviewCheckable').treeview('uncheckNode', [nodeId, {silent: true}]);
        } else
        {
            delete customTagsJson[key];
            addTags(customTagsJson, div, tagType);
        }
    }

    function saveTags(tagType)
    {
        var tags = '';
        if ((tagType === 'udc'))
        {
            if (existingUDCTagsAdded !== '' && existingUDCTagsAdded.length !== 0)
            {
                tags = existingUDCTagsAdded;
            }
            $.each(udcJson, function (keyName, value)
            {
                if (tags === '')
                    tags = keyName;
                else
                    tags = tags + '&|&' + keyName;
            });
            document.getElementById("modifiedJson").value = JSON.stringify(udcJson);
        } else if (tagType === 'custom')
        {
            $.each(customTagsJson, function (keyName, value)
            {
                if (tags === '')
                    tags = keyName;
                else
                    tags = tags + '&|&' + keyName;
            });
            document.getElementById("modifiedJson").value = JSON.stringify(customTagsJson);
        }
        document.getElementById("recordIdentifier").value = recordIdentifier;
        document.getElementById("crowdSourceTagType").value = tagType;
        document.getElementById("crowdSourceTags").value = tags;
        $('#tagForm').submit();
    }

    function addExistingTags(tagsJson, div, tagType)
    {
        document.getElementById(div).innerHTML = '';
        if (!checkJsonIsEmpty(tagsJson))
        {
            var divHtml = '';
            if (div === 'existingCustomTags') {
                $.each(tagsJson, function (keyName, value)
                {
                    divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" title="' + value + '">   ' + value + ' </button>';
                });
                document.getElementById(div).innerHTML = divHtml;
            } else {
                var tags;
                $.each(tagsJson, function (keyName, value)
                {
                    if (tags === '')
                        tags = keyName;
                    else
                        tags = tags + '&|&' + keyName;
                });
                $.ajax({
                    url: '${context}/cs/getUDCTagDescription',
                    dataType: 'json',
                    type: "post",
                    data: {
                        'udcNotations': tags,
                        'languageId': languageId
                    },
                    success: function (response)
                    {
                        for (var key in response) {
                            if (userType === 'normal') {
                                divHtml = divHtml + '<button type="button" class="btn btn-secondar tag_ellipsis" style="margin:0 0 10px 10px;" title="' + response[key] + '">   ' + key + '   ' + [response[key]] + ' </button>';
                            } else {
                                divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" onclick="deleteTag(\'' + key + '\',\'' + div + '\',\'' + tagType + '\');" title="' + [response[key]] + '"> <span class="fa fa-remove"></span>  ' + key + '   ' + [response[key]] + ' </button>';
                            }
                        }
                        document.getElementById(div).innerHTML = divHtml;
                    },
                    error: function ()
                    {

                    }
                });
            }
        } else if (checkJsonIsEmpty(udcJson))
            document.getElementById(div).innerHTML = 'No Tags Available';
    }

    function checkJsonIsEmpty(tagsJson)
    {
        return (Object.keys(tagsJson).length === 0 && JSON.stringify(tagsJson) === JSON.stringify({}));
    }

    function addCustomTags(tagValue, div, tagType) {
        if (tagValue.trim().length !== 0) {
            if (tagValue.trim().length < 100) {
                tagValue = tagValue.trim().replace(/[_,\\\/!@#$%^&*\[\]{}\'\";:]/gm, '');
                languageId = document.getElementById('selectCustomLanguage').value;
                if ((!checkJsonIsEmpty(existingCustomTagsJsonObject) && (typeof (existingCustomTagsJsonObject[tagValue]) !== "undefined")) || (!checkJsonIsEmpty(customTagsJson) && (typeof (customTagsJson[tagValue + '_' + languageId]) !== "undefined"))) {
                    alert('Tag already exist!');
                } else {
                    if (tagValue !== '' && tagValue.length !== 0) {
                        customTagsJson[tagValue + '_' + languageId] = tagValue;
                        addTags(customTagsJson, div, tagType);
                        document.getElementById('customAutocomplete').value = '';
                    }
                }
            } else {
                alert("Maximum 100 characters allowed in custom tag value !");
            }
        } else {
            alert("Enter custom tag value!");
        }
    }

    function selectLanguage(languageDiv)
    {
        languageId = document.getElementById(languageDiv).value;
        document.getElementById('selectUDCLanguage').value = languageId;
        document.getElementById('selectLanguageModel').value = languageId;
        addExistingTags(existingUDCTagsJsonObject, 'existingUDCTags', 'udc');
        if (!checkJsonIsEmpty(udcJson))
        {
            udcTagNotionsAdded = '';
            $.each(udcJson, function (keyName, value)
            {
                if (udcTagNotionsAdded === '')
                    udcTagNotionsAdded = keyName;
                else
                    udcTagNotionsAdded = udcTagNotionsAdded + "&|&" + keyName;
            });
            var jsonObject = {};
            $.ajax({
                url: '${context}/cs/getUDCTagDescription',
                dataType: 'json',
                type: "post",
                data: {
                    'udcNotations': udcTagNotionsAdded,
                    'languageId': languageId
                },
                success: function (response)
                {
                    for (var key in response)
                        jsonObject[key] = response[key];
                    if (!checkJsonIsEmpty(jsonObject)) {
                        udcJson = jsonObject;
                        addTags(udcJson, 'addUDCTags', 'udc');
                    }
                },
                error: function ()
                {

                }
            });
        }
        suggestions();
        getUDCTreeviewData();
    }

    function suggestions()
    {
        languageId = document.getElementById('selectUDCLanguage').value;
        $('#udcAutocomplete').devbridgeAutocomplete({
            paramName: 'tagPrefix',
            serviceUrl: '${context}/cs/getUDCTagsSuggestions?languageId=' + languageId,
            maxHeight: 120,
            onSelect: function (suggestion) {
                if (!checkJsonIsEmpty(existingUDCTagsJsonObject) && (existingUDCTagsJsonObject[suggestion.data] != null) || (!checkJsonIsEmpty(udcJson) && (udcJson[suggestion.data] != null)))
                    alert('Tag already exist!');
                else
                {
                    udcJson[suggestion.data] = suggestion.value;
                    addTags(udcJson, 'addUDCTags', 'udc');
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[suggestion.data]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                }
                document.getElementById('udcAutocomplete').value = '';
            }
        });
    }

    function stringToJSON(tags, type)
    {
        var jsonObject = {};
        if (tags !== '' && tags.length !== 0)
        {
            tags = tags.split('&|&');
            for (var i = 0; i < tags.length; i++)
                jsonObject[tags[i]] = tags[i];
        }
        if (type === 'udc')
            existingUDCTagsJsonObject = jsonObject;
        else
            existingCustomTagsJsonObject = jsonObject;
    }

    function selectCustomLanguage()
    {
        languageId = document.getElementById('selectCustomLanguage').value;
        customTAgSuggestions();
    }

    function customTAgSuggestions()
    {
        languageId = document.getElementById('selectCustomLanguage').value;
        $('#customAutocomplete').devbridgeAutocomplete({
            paramName: 'tagPrefix',
            serviceUrl: '${context}/cs/getCustomTagsSuggestions?languageId=' + languageId
        });
    }

    function getUDCTreeviewData()
    {
        var tags = existingUDCOriginalTags;
        var isDisable = 'true';
        if (userType === 'expert') {
            isDisable = 'false';
        }
        $.ajax({
            url: '${context}/cs/getUDCTreeviewData',
            data: {languageId: languageId, tags: tags, isDisable: isDisable},
            type: 'POST',
            success: function (response)
            {
                nodeIds = response["nodeIds"];
                $('#udcTreeviewCheckable').treeview({
                    data: response["udcTreeViewData"],
                    showIcon: false,
                    showCheckbox: true,
                    onNodeChecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        notationText = node.text.split("]")[1].trim();
                        udcJson[notation] = notationText;
                    },
                    onNodeUnchecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        if (udcJson[notation] !== undefined) {
                            delete udcJson[notation];
                        }
                        if (existingUDCTagsJsonObject[notation] !== undefined) {
                            delete existingUDCTagsJsonObject[notation];
                        }
                    }
                });
                applySlimScroll("udcTreeviewCheckable");
                $.each(udcJson, function (keyName, value)
                {
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[keyName]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                });
                $.each(existingUDCTagsJsonObject, function (keyName, value)
                {
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[keyName]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                });
            },
            error: function ()
            {
            }
        });
    }

    function addTreeviewSelectionList()
    {
        addTags(udcJson, 'addUDCTags', 'udc');
        addExistingTags(existingUDCTagsJsonObject, 'existingUDCTags', 'udc');
    }

    $(".radio-cs-block").ready(function () {
        $(".radio-cs").unbind().bind("click", function (e) {
            var toggleTab = e.currentTarget.getAttribute("data-value");
            if (toggleTab === 'udc')
            {
                $('#udcTagDiv').prop('hidden', false);
                $('#customTagDiv').prop('hidden', true);
                $('#viewMetadata').prop('hidden', true);
                $("#tabId").hide();
                changeLanguage(get_related_lang_val($("#selectUDCLanguage").val()));
            } else if (toggleTab === 'custom')
            {
                $('#customTagDiv').prop('hidden', false);
                $('#udcTagDiv').prop('hidden', true);
                $('#viewMetadata').prop('hidden', true);
                $("#tabId").hide();
                changeLanguage(get_related_lang_val($("#selectCustomLanguage").val()));
            } else
            {
                $('#viewMetadata').prop('hidden', false);
                $('#udcTagDiv').prop('hidden', true);
                $('#customTagDiv').prop('hidden', true);
                $("#tabId").show();
                if (_editMetadataFlag) {
                    changeLanguage(get_related_lang_val($("#selectCurationLanguage").val()));
                }
            }
        });
    });
    var selectedFilePath, currentPageNo, totalPages, tempTiffFile;
    function renderTiff(tiffFile, no, numPages) {
        selectedFilePath = "${pageContext.request.contextPath}/search/preview/all/renderTiff/" + recordIdentifier + "/" + no;
        tempTiffFile = tiffFile;
        currentPageNo = no;
        totalPages = numPages;
        $(function () {
            $('#viewer').show();
            console.log(numPages + " in tif" + tiffFile + " " + no);
            var iv1 = $("#viewer").iviewer({
                src: selectedFilePath
            });
            console.log("path.. " + selectedFilePath);
            iv1.iviewer('loadImage', selectedFilePath);
        });
    }
    function getRecordContent() {
        $.ajax({
            url: '${context}/cs/getRecordContent',
            data: 'recordIdentifier=' + recordIdentifier,
            type: 'POST',
            success: function (response)
            {
                document.getElementById('accordion').innerHTML = response;
                if (contentType === '<%=Constants.ContentType.TIFF%>') {
                    $(function () {
                        $.ajax(
                                {
                                    type: "POST",
                                    url: "${pageContext.request.contextPath}/search/preview/all/getTiffImagesCount/" + recordIdentifier,
                                    success: function (response) {
                                        renderTiff(recordIdentifier, 1, response);
                                    },
                                    error: function (e) {
                                        alert('Error while retreiving number of pages in a tiff file: ' + e + '<br/>Tiff File: ' + recordIdentifier);
                                    }
                                });
                    });
                    /*Preview of previous page*/
                    $("#prev").click(function () {
                        if (currentPageNo <= 1) {
                            alert('Previous page is not available.');
                        } else {
                            renderTiff(tempTiffFile, (currentPageNo - 1), totalPages);
                        }
                    });
                    /*Preview of next page*/
                    $("#next").click(function () {
                        if (currentPageNo >= (totalPages - 1)) {
                            alert('Next page is not available.');
                        } else {
                            renderTiff(tempTiffFile, (currentPageNo + 1), totalPages);
                        }
                    });
                    $("#viewer").on('mousewheel', function (e) {
                        e.preventDefault();
                    });
                    /*
                     *Zoom in functionality
                     */
                    $("#in").click(function () {

                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('zoom_by', 1);
                    });
                    /*
                     *Zoom out functionality
                     */
                    $("#out").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('zoom_by', -1);
                    });
                    /*
                     *fit to screen functionality
                     */
                    $("#fit").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('fit');
                    });
                    /*
                     *original view functionality
                     */
                    $("#orig").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('set_zoom', 100);
                    });
                    /*
                     *Rotate Left functionality
                     */
                    $("#rotateleft").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('angle', -90);
                    });
                    /*
                     *Rotate Right functionality
                     */
                    $("#rotateright").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('angle', 90);
                    });
                } else if (contentType === '<%=Constants.ContentType.JPEG%>') {
                    function getWindowHeight() {
                        var myHeight = 0;
                        if (typeof (window.innerWidth) == 'number') {
                            //Non-IE
                            myHeight = window.innerHeight;
                        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                            //IE 6+ in 'standards compliant mode'
                            myHeight = document.documentElement.clientHeight;
                        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                            //IE 4 compatible
                            myHeight = document.body.clientHeight;
                        }
                        return myHeight;
                    }
                    var recordPathList = '${recordPathList}';
                    recordPathList = recordPathList.replace("[", "").replace("]", "").split(",");
                    for (var i = 0; i < recordPathList.length; i++) {
                        imgPathArr.push(recordPathList[i]);
                    }

                    imgCounter = imgPathArr.length;
                    if (imgCounter % 2 != 0) {
                        imgPathArr.push(recordPathList[0]);
                        imgCounter = imgCounter + 1;
                    }
                    imagePathJsonObject["play-big"] = "${context}/themes/images/BookReader/play-big.jpg";
                    imagePathJsonObject["pause-big"] = "${context}/themes/images/BookReader/pause-big.jpg";
                    imagePathJsonObject["closedhand"] = "${context}/themes/images/BookReader/closedhand.cur";
                    imagePathJsonObject["openhand"] = "${context}/themes/images/BookReader/openhand.cur";
                    $('#fb-pg').val("1");
                    $("#totalCnt").text("/ " + imgCounter);
                    load_Initial_Pages();
                }
            },
            error: function ()
            {
                alert('Error in showing the content');
            }
        });
    }

    function addMarcTag(flag) {
        $('#addMarc21TagModal').modal('hide');
        var oldValue = "";
        if (flag) {
            var metadata_template = _.template($("#tpl-dataField-metadataFormRow").html());
            var recordStatusIndex = parseInt($("#recordStatusIndex").val());
            var datafieldStatusIndex;
            if (datafieldStatusIndex === 0) {
                datafieldStatusIndex = parseInt($("#datafieldStatusIndex").val());
            } else {
                datafieldStatusIndex = parseInt($("#datafieldStatusIndex").val()) + 1;
            }
            var subfieldStatusIndex = parseInt($("#subfieldStatusIndex").val());
            var opts = {
                recordStatusIndex: recordStatusIndex,
                datafieldStatusIndex: datafieldStatusIndex,
                subfieldStatusIndex: subfieldStatusIndex,
                tagCode: $("#tagCode").val(),
                subfieldCode: $("#subfieldCode").val(),
                tagValue: $("#tagValue").val(),
                description: $("#tagDescription").val(),
                oldValue: oldValue
            };
            var metadata_el = metadata_template(opts);
            $("#metadataDiv").append(metadata_el);
            $("#recordStatusIndex").val(recordStatusIndex);
            $("#datafieldStatusIndex").val(datafieldStatusIndex);
            $("#subfieldStatusIndex").val(subfieldStatusIndex);
        } else {
            var metadata_template = _.template($("#tpl-controlField-metadataFormRow").html());
            var recordStatusIndex = parseInt($("#recordStatusIndex").val());
            var controlfieldStatusIndex;
            if (controlfieldStatusIndex === 0) {
                controlfieldStatusIndex = parseInt($("#controlfieldStatusIndex").val());
            } else {
                controlfieldStatusIndex = parseInt($("#controlfieldStatusIndex").val()) + 1;
            }
            var opts = {
                recordStatusIndex: recordStatusIndex,
                controlfieldStatusIndex: controlfieldStatusIndex,
                tagCode: $("#tagCode").val(),
                tagValue: $("#tagValue").val(),
                description: $("#tagDescription").val(),
                oldValue: oldValue
            };
            var metadata_el = metadata_template(opts);
            $("#metadataDiv").append(metadata_el);
            $("#recordStatusIndex").val(recordStatusIndex);
            $("#controlfieldStatusIndex").val(controlfieldStatusIndex);
        }
        addClassToDiscription();
        marc21CloseRefresh();
    }

    function addDoublinCoreTag(tagName, tagValue) {
        $('#addDCTagModal').modal('hide');
        var metadata_dc_template = _.template($("#tpl-dc-metadataFormRow").html());
        var filedIndex = getDCFieldIndex(tagName.toLowerCase());
        var oldValue = "";
        var opts = {
            tagName: tagName,
            fieldName: tagName.toLowerCase(),
            filedIndex: filedIndex,
            tagValue: tagValue,
            oldValue: oldValue
        };
        var metadata_dc_el = metadata_dc_template(opts);
        $("#metadataDiv").append(metadata_dc_el);
        creatJsonRewardpointsForEditMetadata(tagName.toLowerCase(), oldValue, tagValue);
        addClassToDiscription();
        dcCloseRefresh();
    }

    function getDCFieldIndex(tagName) {
        var counter = 0;
        $('#metadataDiv > div').each(function () {
            if ($(this).hasClass("dc-" + tagName)) {
                ++counter;
            }
        });
        return counter;
    }

    function addMarc21Tag() {
        var dataFiled = true;
        var tagCodeValue = parseInt($("#tagCode").val());
        var tagDescriptionAttr = document.getElementById("tagDescription").hasAttribute("readonly");
        var tagCodeAttr = document.getElementById("tagCode").hasAttribute("readonly");
        var subfieldCodeAttr = document.getElementById("subfieldCode").hasAttribute("readonly");
        if ($("#tagDescription").val().length === 0)
        {
            alert("Select the description.");
        } else if (isNaN($("#tagCode").val()) && $("#tagCode").val().length === 0) {
            alert("Select valid tagcode.");
        } else if ($("#tagValue").val() === "") {
            alert('Please enter the tag value.');
        } else if ((tagDescriptionAttr && tagCodeAttr && subfieldCodeAttr) && (tagCodeValue < 10 && $("#subfieldCode").val().length === 0)) {
            addMarcTag(!dataFiled);
        } else if ((tagDescriptionAttr && tagCodeAttr && subfieldCodeAttr) && (tagCodeValue >= 10 && $("#subfieldCode").val().length !== 0)) {
            addMarcTag(dataFiled);
        } else {
            alert("Something going wrong,please reset");
        }
    }

    function addDCTag() {
        var e = document.getElementById("dc-tag-name");
        var tagName = e.options[e.selectedIndex].value;
        if (tagName.length === 0) {
            alert("Select the tag");
        } else if ($("#dcTagValue").val() === "") {
            alert('Enter the tag value');
        } else if (tagName.length !== 0 && $("#dcTagValue").val() !== "") {
            addDoublinCoreTag(tagName, $("#dcTagValue").val());
        } else {
            alert("Something going wrong,please reset.");
        }
    }

    function isbnSearch() {
        $("#recordMetadata").hide();
        $("#isbn-search-metadata").show();
        $("#isbn-search-input").val(${isbn_no});
        validateISBN($("#isbn-search-input").val());
    }

    function returnToRecordMetadata() {
        $("#recordMetadata").show();
        $("#isbn-search-metadata").hide();
        $("#metadata-source-tabs").hide();
        $("#metadata-source-tabs-content").hide();
        $("#isbn-message").hide();
    }

    function getMetadataKey(key) {
        key = key.replace(/_/g, " ");
        return key.charAt(0).toUpperCase() + key.slice(1);
    }

    function getMetadataValue(value) {
        return typeof value[0] === "string" ? value[0].replace(/("|')/g, "") : value;
    }

    function isbnMetadataSearch() {
        $("#metadata-source-tabs").hide();
        $("#metadata-source-tabs-content").hide();
        $("#isbn-message").hide();
        validateISBN($("#isbn-search-input").val());
    }

    function validateISBN(isbn) {
        !_.isEmpty(isbn) && checkISBNLength(isbn) ? fetchMetadata(isbn) : enterValidISBN();
    }

    function enterValidISBN() {
        $("#isbn-message").removeClass().addClass("alert alert-warning m-a-1 text-center");
        $("#isbn-message").html("Enter 10 or 13 characters ISBN.");
        $("#isbn-message").show();
    }

    function checkISBNLength(isbn) {
        isbn = isbn.replace(/-/g, "");
        return (isbn.length === 10) || (isbn.length === 13) ? true : false;
    }

    function beforeFetchMetadata() {
        // hide tabs
        $("#metadata-source-tabs").hide();
        //hide tabs content
        $("#metadata-source-tabs-content").hide();
        //show loader
        $("#isbn-metadata-loader").show();
        //hide message
        $("#isbn-message").hide();
    }

    function renderMetadataTabs(response) {
        //maintain index to active first tab
        var index = 0;
        //tabs template start here
        $("#metadata-source-tabs").empty();
        var metadata_tab_template = _.template($("#tpl-metadata-source-tabs").html());
        _.each(response, function (values, key) {
            var opts = {
                index: ++index,
                key: key
            };
            var metadata_tab_el = metadata_tab_template(opts);
            $("#metadata-source-tabs").append(metadata_tab_el);
        });
    }

    function renderMetadataTabsContent(response) {
        //maintain index to active first tab
        var index = 0;
        //tab content template start here
        $("#metadata-source-tabs-content").empty();
        var metadata_tab_content_template = _.template($("#tpl_metadata_tab_content").html());
        _.each(response, function (values, key) {
            var opts = {
                index: ++index,
                key: key,
                values: values,
                imageURL: values.imageUrl
            };
            var metadata_tab_content_template_el = metadata_tab_content_template(opts);
            $("#metadata-source-tabs-content").append(metadata_tab_content_template_el);
        });
    }

    function fetchMetadata(isbn) {
        $.ajax({
            url: __NVLI.context + "/cs/fetchMetadata",
            data: {isbn: isbn},
            dataType: 'json',
            beforeSend: function () {
                // Handle the beforeSend event
                beforeFetchMetadata();
            },
            complete: function () {
                // Handle the complete event
                //hide loader
                $("#isbn-metadata-loader").hide();
            }
        })
                .done(function (response) {
                    if (!_.isEmpty(response)) {
                        //render tabs
                        renderMetadataTabs(response);
                        //rendet tabs content
                        renderMetadataTabsContent(response);
                        //show tabs
                        $("#metadata-source-tabs").show();
                        //show tab content
                        $("#metadata-source-tabs-content").show();
                        //instantiate clipboard.js
                        new Clipboard(".copy-metadata-value");
                    } else {
                        $("#isbn-message").removeClass().addClass("alert alert-warning m-a-1 text-center");
                        $("#isbn-message").html("Sorry, we could not find any information for this book. Please try a different book.");
                        $("#isbn-message").show();
                    }
                })
                .fail(function (xhr) {
                    $("#isbn-message").removeClass().addClass("alert alert-danger m-a-1 text-center");
                    $("#isbn-message").html("Sorry,something went wrong. Please try again.");
                    $("#isbn-message").show();
                    console.log("error in fetchMetadata ::", xhr);
                });
    }

    function marc21TagSuggestions() {
        $('#tagCode').devbridgeAutocomplete({
            paramName: 'tagField',
            serviceUrl: '${context}/cs/getMarc21TagsSuggestions',
            maxHeight: 190,
            showNoSuggestionNotice: true,
            noSuggestionNotice: "Sorry,no matches found...",
            triggerSelectOnValidInput: false,
            formatResult: function (suggestion, currentValue) {
                return suggestion.data !== null ? (suggestion.value + "  " + suggestion.data + " [ " + suggestion.tagLibrarianTitle + " ] ") : (suggestion.value + " [ " + suggestion.tagLibrarianTitle + " ] ");
            },
            onSelect: function (suggestion) {
                document.getElementById('tagDescription').value = suggestion.tagLibrarianTitle;
                document.getElementById('subfieldCode').value = suggestion.data;
                $('#tagDescription').attr("readonly", "true");
                $('#tagCode').attr("readonly", "true");
            }
        });
    }
    $("#tagCode").ready(function () {
        $("#tagCode").on('input', function () {
            document.getElementById('tagDescription').value = "";
            document.getElementById('subfieldCode').value = "";
        });
    });
    function marc21DescriptionSuggestions() {
        $('#tagDescription').devbridgeAutocomplete({
            paramName: 'librarianTitle',
            serviceUrl: __NVLI.context + '/cs/getMarc21DescriptionSuggestions',
            maxHeight: 252,
            showNoSuggestionNotice: true,
            triggerSelectOnValidInput: false,
            noSuggestionNotice: "Sorry,no matches found...",
            onSelect: function (suggestion) {
                document.getElementById('tagCode').value = suggestion.data;
                document.getElementById('subfieldCode').value = suggestion.tagSubfield;
                $('#tagDescription').attr("readonly", "true");
                $('#tagCode').attr("readonly", "true");
            },
            formatResult: function (suggestion, currentValue) {
                return suggestion.tagSubfield != null ? (suggestion.value + " [ " + suggestion.data + "  " + suggestion.tagSubfield + " ] ") : (suggestion.value + " [ " + suggestion.data + " ] ");
            }
        });
    }
    $("#tagDescription").ready(function () {
        $("#tagDescription").on('input', function () {
            document.getElementById('tagCode').value = "";
            document.getElementById('subfieldCode').value = "";
        });
    });
    function marc21CloseRefresh() {
        $('#tagDescription').removeAttr("readonly");
        $('#tagCode').removeAttr("readonly");
        $('#subfieldCode').attr("readonly", "true");
        $("#tagCode").val("");
        $("#tagDescription").val("");
        $("#subfieldCode").val("");
        $("#tagValue").val("");
    }

    function dcCloseRefresh() {
        $("#dcTagValue").val("");
    }

    function renderFile(path) {
        var fileName = path.toString().split("/")[path.toString().split("/").length - 1];
        if (fileName.indexOf(".mp4") != -1) {
            document.getElementById('timecodeMarkingDiv').innerHTML = '<button class="btn btn-primary" type="button" data-toggle="modal"  onclick="openVideoMarking();">Time Code Marking</button>';
            var displayHTML = "<div class='videop marginauto'><div class='video_innerbox1'> <video oncontextmenu='return false;' controls controlsList='nodownload'><source src='" + path + "' type='video/mp4'>Your browser does not support the video tag.</video></div></div>";
            $('#viewall').html(displayHTML);
        } else if (fileName.indexOf(".mp3") != -1) {
            document.getElementById('timecodeMarkingDiv').innerHTML = '<button class="btn btn-primary" type="button" data-toggle="modal"  onclick="openVideoMarking();">Time Code Marking</button>';
            var displayHTML = " <div class='audiop marginauto'><div class='audio_innerbox'><audio controls><source src='" + path + "' type='audio/mpeg'></audio></div></div>";
            $('#viewall').html(displayHTML);
        }
        if (fileName.indexOf(".pdf") != -1) {
            var displayHTML = "<object class='videop object' data='" + path + "' type='application/pdf' width='100%' height='100%'></object>";
            $('#viewall').html(displayHTML);
        }
    }
    function approveTags()
    {
        var tags = '';
        $.each(existingUDCTagsJsonObject, function (keyName, value)
        {
            if (tags === '')
                tags = keyName;
            else
                tags = tags + "&|&" + keyName;
        });
        $.each(udcJson, function (keyName, value)
        {
            if (tags === '')
                tags = keyName;
            else
                tags = tags + "&|&" + keyName;
        });
        document.getElementById("crowdSourceTags").value = tags;
        document.getElementById("isEdit").value = 'yes';
        document.getElementById("recordIdentifier").value = recordIdentifier;
        $('#udcTagForm').submit();
    }
    function applySlimScroll(id) {
        $("#" + id).slimScroll({
            height: '550px',
            size: '5px',
            position: 'right',
            color: '#888',
            alwaysVisible: false,
            distance: '0px',
            railVisible: false,
            railColor: '#efefef',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: false
        });
    }
    function actionAllTargets(action) {
        $(".search-target-checkbox").prop('checked', action);
    }


    function getParameterMap() {
        var parameterMap = {};
        $(".search-parameter-value").each(function () {
            if ($(this).val()) {
                parameterMap[$(this).attr("name")] = $(this).val();
            }
        });
        _parameterMap = parameterMap;
        return Object.keys(parameterMap).length !== 0 ? parameterMap : false;
    }

    function getSelectedLibraries() {
        var selectedLibraries = [];
        $('#search-target-checkboxes label>input[type=checkbox]').each(function () {
            if ($(this).is(":checked")) {
                selectedLibraries.push($(this).attr('value'));
            }
        });
        return selectedLibraries.length !== 0 ? selectedLibraries : false;
    }

    function zSearch(parameterMap, selectedLibraries) {
        if (parameterMap !== false && selectedLibraries !== false) {
            callZ3950WS(parameterMap, 0, false);
        } else {
            alert("Select parameter or targets.");
        }
    }

    function showZsearch() {
        $("#search-target-checkboxes").empty();
        var library_list_checkbox_template = _.template($("#tpl-library-list-checkbox").html());
        var opts = {
            selectedLibraries: _libraryDetails
        };
        var library_list_checkbox_template_el = library_list_checkbox_template(opts);
        $("#search-target-checkboxes").append(library_list_checkbox_template_el);
        $("#zSearch-modal").modal("show");

    }
    function loadSelectedLibraries(selectedLibraries, action) {
        if (!action) {
            $("#library-filters").empty();
            var library_filters_template = _.template($("#tpl-library-filters").html());
            var opts = {
                selectedLibraries: selectedLibraries
            };
            var library_filters_template_el = library_filters_template(opts);
            $("#library-filters").append(library_filters_template_el);
            LibraryFilterChange();
        }
    }

    function callZ3950WS(parameterMap, index, action) {
        var selectedLibraries = getSelectedLibraries();
        $.ajax({
            url: __NVLI.context + "/cs/z3950WS-sru",
            cache: false,
            type: "POST",
            data: {
                parameterMap: JSON.stringify(parameterMap),
                selectedLibrary: selectedLibraries[index],
                pageNo: _pageNo,
                dataLimit: 10
            },
            beforeSend: function () {
                $("#z3950-search").hide();
                $("#btn-z-search").hide();
                $("#btn-back-z-search").show();
                $("#z3950-results").show();
                $("#z3950-results-table-body").empty();
                appendLoader(_libraryDetails[selectedLibraries[index]]);
                $("#z3950-search-footer").hide();
                $("#resultSize-count").text(0);
                loadSelectedLibraries(selectedLibraries, action);
            }
        })
                .done(function (response) {
                    $("#z3950-search-loader").hide();
                    if (!_.isEmpty(response.Z3950Search)) {
                        $("#msg-div").empty();
                        _z3950Results = response.Z3950Search;
                        $("#resultSize-count").text(response.resultSize);
                        $("#z3950-results-table-body").empty();
                        var z3950_search_results_template = _.template($("#tpl-z3950-search-results").html());
                        var opts = {
                            resultList: response.Z3950Search
                        };
                        var z3950_search_results_template_el = z3950_search_results_template(opts);
                        $("#z3950-results-table-body").append(z3950_search_results_template_el);
                        applySlimScroll("tbl-slim-scroll");
                        renderPagination(z3950ResultsPagination, "z3950-search-footer", "page-btn", _pageNo, response.resultSize, 10, true, 5, true);
                        $("#z3950-search-footer").show();
                    }
                    else {
                        appendNoResultFound();
                        if ((typeof selectedLibraries === "object") && (selectedLibraries.indexOf($("#library-filters option:selected").val()) !== selectedLibraries.length - 1)) {
                            callZ3950WS(parameterMap, selectedLibraries.indexOf($("#library-filters option:selected").val()) + 1, true);
                            document.getElementById("library-filters").selectedIndex = selectedLibraries.indexOf($("#library-filters option:selected").val()) + 1;
                        }
                        $("#z3950-search-footer").hide();
                    }
                })
                .fail(function (xhr) {
                    console.error("Error in callZ3950WS ::", xhr);
                });
    }

    function getSubfieldValue(code, subfieldsArr, val) {
        _.each(subfieldsArr, function (subfield) {
            if (subfield.code === code) {
                val !== "" ? val += " | " : val;
                val += subfield.value;
            }
        });
        return val;
    }

    function getRecordDetatils(result) {
        var recordJson = {
            title: "",
            author: "",
            isbn: "",
            lccn: "",
            personalName: ""

        };
        _.each(result.record[0].datafield, function (el) {
            if (el.tag === "245") {
                recordJson["title"] = getSubfieldValue("a", el.subfield, recordJson["title"]);
            }
            if (el.tag === "100") {
                recordJson["author"] = getSubfieldValue("a", el.subfield, recordJson["author"]);
            }
            if (el.tag === "020") {
                recordJson["isbn"] = getSubfieldValue("a", el.subfield, recordJson["isbn"]);
            }
            if (el.tag === "010") {
                recordJson["lccn"] = getSubfieldValue("a", el.subfield, recordJson["lccn"]);
            }
            if (el.tag === "700") {
                recordJson["personalName"] = getSubfieldValue("a", el.subfield, recordJson["personalName"]);
            }
        });
        return recordJson;
    }

    function backToZSearch() {
        _pageNo = 1;
        $("#z3950-search").show();
        $("#btn-z-search").show();
        $("#z3950-results").hide();
        $("#btn-back-z-search").hide();
        $("#msg-div").empty();
    }

    function backToZSearchResults() {
        $("#z3950-results").show();
        $("#btn-back-z-search").show();
        $("#marc-detail").hide();
        $("#btn-back-z-search-results").hide();
        $("#marc-detail").parent().hide();
        $("#msg-div").empty();
    }

    function LibraryFilterChange() {
        $("#library-filters").change(function () {
            $("#z3950-results-table-body").empty();
            _pageNo = 1;
            callZ3950WS(_parameterMap, document.getElementById("library-filters").selectedIndex, true);
        });
    }

    function showMarcDetails(i) {
        $("#z3950-results").hide();
        $("#btn-back-z-search").hide();
        $("#btn-back-z-search-results").show();
        $("#marc-detail").empty();
        $("#marc-detail").text(_z3950Results[i].marcString);
        applySlimScroll("marc-detail");
        $("#marc-detail").parent().show();
        $("#marc-detail").show();
    }

    function importMarcDetails(i) {
        var msg = "After import metadata wiil be replaced by selected metadata. Do you want to import?";
        if (confirm(msg)) {
            $("#zSearch-modal").modal("hide");
            var collectionType = _z3950Results[i].collectionType;
            $("#edit-metadata-table").empty();
            var import_marc_template = _.template($("#tpl-edit-metadata-table").html());
            var opts = {
                collectionType: collectionType,
                marc21DescriptionMap: ${marc21DescriptionMap}
            };
            var import_marc_template_el = import_marc_template(opts);
            $("#edit-metadata-table").append(import_marc_template_el);
            addClassToDiscription();
        }
    }

    function z3950ResultsPagination(pageNO) {
        _pageNo = pageNO;
        callZ3950WS(getParameterMap(), document.getElementById("library-filters").selectedIndex, true);
    }

    function appendLoader(library) {
        if (library) {
            $("#msg-div>#z3950-search-loader").remove();
            var loader_template = _.template($("#tpl-loader").html());
            var opts = {
                library: library
            };
            var loader_template_el = loader_template(opts);
            $("#msg-div").append(loader_template_el);
        }
    }

    function appendNoResultFound() {
        $("#msg-div>#z3950-no-result-found").remove();
        var no_result_found_template = _.template($("#tpl-no-result-found").html());
        var opts = {
            library: $("#library-filters option:selected").text()
        };
        var no_result_found_template_el = no_result_found_template(opts);
        $("#msg-div").append(no_result_found_template_el);
    }

    function creatJsonRewardpointsForEditMetadata(fieldName, oldValue, newValue) {
        var objectArray = [];
        if (_.has(modifiedMetadataJson, fieldName)) {
            var metaData = modifiedMetadataJson[fieldName];
            var isNew = true;
            _.each(metaData, function (innerObject) {
                if (innerObject.oldValue === oldValue) {
                    innerObject.newValue = newValue;
                    if (innerObject.oldValue === innerObject.newValue) {
                        objectArray = _.filter(metaData, function (o) {
                            return o.newValue !== oldValue;
                        });
                        isNew = false;
                    } else {
                        metaData = [];
                        modifiedMetadataJson[fieldName] = metaData;
                    }
                }
            });
            if (isNew) {
                metaData.push({
                    "oldValue": oldValue,
                    "newValue": newValue
                });
                objectArray = metaData;
            }
        } else {
            objectArray = [{
                    "oldValue": oldValue,
                    "newValue": newValue
                }
            ];
        }

        if ((objectArray.length === 0) || (objectArray === undefined)) {
            delete modifiedMetadataJson[fieldName];
        } else {
            modifiedMetadataJson[fieldName] = objectArray;
        }

        document.getElementById("modifiedMetadataJson").value = JSON.stringify(modifiedMetadataJson);
        console.log(modifiedMetadataJson);
    }

    function modifiedField() {
        $(".metadata-tag-description").change(function (e) {
            var fieldName;
            var oldValue = e.currentTarget.getAttribute("data-old-value");
            var newValue = $(this).val();
            if ($("#metadataType").val() === "marc21") {
                fieldName = e.currentTarget.getAttribute("data-tag") + "_" + e.currentTarget.getAttribute("data-subfield");
            } else {
                fieldName = e.currentTarget.getAttribute("data-tag");
            }
            creatJsonRewardpointsForEditMetadata(fieldName, oldValue, newValue);
        });
    }
</script>
<script type="text/template" id="tpl-dataField-metadataFormRow">
    <div class="form-group row clearfix">
    <input type="hidden" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].tag" value="{{=tagCode}}"/>
    <input type="hidden" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].subfield[{{=subfieldStatusIndex}}].code" value="{{=subfieldCode}}"/>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12" style="text-transform: capitalize">{{=tagCode}}</label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12 ">{{=subfieldCode}}</label>
    {{ if(tagCode==="100" && subfieldCode==="a"){ }}
    <label class="form-control-label col-lg-4 col-md-1 col-sm-12 ">{{=description}} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
    {{ }else{ }}
    <label class="form-control-label col-lg-4 col-md-1 col-sm-12 ">{{=description}}</label>
    {{ } }}
    <div class="col-lg-6 col-md-6 col-sm-12">
    <input type="text" class="form-control metadata-tag-description" data-tag="{{=tagCode}}" data-subfield="{{=subfieldCode}}" name="collectionType.record[{{=recordStatusIndex}}].datafield[{{=datafieldStatusIndex}}].subfield[{{=subfieldStatusIndex}}].value" data-old-value="{{=oldValue}}" value="{{=tagValue}}"/>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-controlField-metadataFormRow">
    <div class="form-group row clearfix">
    <input type="hidden" name="collectionType.record[{{=recordStatusIndex}}].controlfield[{{=controlfieldStatusIndex}}].tag" value="{{=tagCode}}"/>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12" style="text-transform: capitalize">{{=tagCode}}</label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12 ">---</label>
    <label class="form-control-label col-lg-4 col-md-1 col-sm-12 ">{{=description}}</label>
    <div class="col-lg-6 col-md-6 col-sm-12">
    <input type="text" class="form-control metadata-tag-description" data-tag="{{=tagCode}}" data-subfield="" name="collectionType.record[{{=recordStatusIndex}}].controlfield[{{=controlfieldStatusIndex}}].value" data-old-value="{{=oldValue}}" value="{{=tagValue}}"/>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-dc-metadataFormRow">
    <div class="form-group row clearfix dc-{{=fieldName}}" data-dc-tag="{{=fieldName}}">
    {{ if( fieldName === "creator"){ }}
    <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="{{=fieldName}}" style="text-transform: capitalize">{{=tagName}} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
    {{ }else{ }}
    <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="{{=fieldName}}" style="text-transform: capitalize">{{=tagName}}</label>
    {{ } }}
    <div class="col-lg-10 col-md-7 col-sm-12">
    <input type="text" class="form-control metadata-tag-description" data-tag="{{=fieldName}}"  name="dcMetadata.{{=fieldName}}[{{=filedIndex}}]" data-old-value="{{=oldValue}}" value="{{=tagValue}}"/>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-metadata-source-tabs">
    <li class="nav-item">
    {{ if(index === 1){ }}
    <a class="nav-link active" data-target="\#{{=key}}-tab" data-toggle="tab">
    {{ } else { }}
    <a class="nav-link" data-target="\#{{=key}}-tab" data-toggle="tab">
    {{ } }}
    {{=key}}
    </a>
    </li>
</script>
<script type="text/template" id="tpl_metadata_tab_content">
    {{ if(index === 1){ }}
    <div class="tab-pane nopad active" id="{{=key}}-tab">
    {{ } else { }}
    <div class="tab-pane nopad" id="{{=key}}-tab">
    {{ } }}
    <div class="overflow685">
    <div class="card-block">
    {{ _.each(values,function(value,key){ key=getMetadataKey(key);}}
    {{ if(key ==="ImageUrl"){ }}
    <div class="m-a-1 text-center">
    <img src="{{=value}}" height="200px"/>
    </div>
    {{ } }}
    {{ }); }}
    <table class="table table-hover">
    <thead>
    <tr>
    <th>Description</th>
    <th>Value</th>
    <th>Copy</th>
    </tr>
    </thead>
    <tbody>
    {{ _.each(values,function(value,key){ key=getMetadataKey(key); value=getMetadataValue(value); }}
    {{ if(key !=="ImageUrl"){ }}
    <tr>
    <td width="30%">{{=key}}</td>
    <td>{{=value}}</td>
    <td width="10%" align="center">
    <button class="btn btn-secondary copy-metadata-value clipboard" data-clipboard-text="{{=value}}" data-toggle="tooltip" data-placement="bottom" title="Copy" type="button">
    <i class="fa fa-clipboard"></i>
    </button>
    </td>
    </tr>
    {{ } }}
    {{ }); }}
    </tbody>
    </table>
    </div>
    </div>
    </div>
</script>
<script type="text/template" id="tpl-z3950-search-results">
    {{ _.each(resultList,function(result,i){ var recordJson= getRecordDetatils(result.collectionType);}}
    <tr>
    <td>{{=recordJson.title}}</td>
    <td>{{=recordJson.author}}</td>
    <td>{{=recordJson.isbn}}</td>
    <td>{{=recordJson.lccn}}</td>
    <td>{{=recordJson.personalName}}</td>
    <td>
    <a href="javascript:;" onClick="showMarcDetails({{=i}});">
    <tags:message code="label.marc"/>
    </a> |
    <a href="javascript:;" onClick="importMarcDetails({{=i}});">
    <tags:message code="label.import"/>
    </a>
    </td>

    </tr>
    {{ }); }}
</script>
<script type="text/template" id="tpl-library-filters">
    {{ _.each(selectedLibraries,function(library){ }}
    <option value="{{=library}}">{{=_libraryDetails[library]}}</option>
    {{ }); }}
</script>
<script type="text/template" id="tpl-library-list-checkbox">
    {{ _.each(selectedLibraries,function(library,libraryCode){ }}
    <label style="text-transform: capitalize;">
    <input class="search-target-checkbox" name="{{=library}}" type="checkbox" value="{{=libraryCode}}">
    {{= library }}
    </label>
    {{ }); }}
</script>
<script type="text/template" id="tpl-edit-metadata-table">
    <table class="table">
    <tbody>
    <div class="form-group row clearfix">
    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" style="text-transform: capitalize">
    <tags:message code="label.Tag" />
    </label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold">
    <tags:message code="label.Subfield" />
    </label>
    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold">
    <tags:message code="label.description" />
    </label>
    <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold">
    <tags:message code="label.Value" />
    </label>
    </div>
    <input type="hidden" name="collectionType.id" value="{{=collectionType.id}}" />
    {{ var controlfieldStatusIndex=0; var datafieldStatusIndex=0; var recordStatusIndex=0; }}
    {{ _.each(collectionType.record, function(record,recordIndex){ }}
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].leader.value" value="{{=record.leader.value}}" />
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].leader.id" value="{{=record.leader.id}" />
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].type" value="{{=record.type}}" />
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].id" value="{{=record.id}}" />
    {{ _.each(record.controlfield, function(controlfield,controlfieldIndex){  }}
    <div class="form-group row clearfix">
    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="{{=controlfield.tag}}" style="text-transform: capitalize">{{=controlfield.tag}}</label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 ">---</label>
    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 ">{{= marc21DescriptionMap[controlfield.tag] }}</label>
    <div class="col-lg-6 col-md-6 col-sm-12">
    <input type="text" class="form-control" name="collectionType.record[{{=recordIndex}}].controlfield[{{=controlfieldIndex}}].value"
    value="${fn:escapeXml('{{=controlfield.value}}')}" />
    </div>
    </div>
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].controlfield[{{=controlfieldIndex}}].id" value="{{=controlfield.id}}"
    />
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].controlfield[{{=controlfieldIndex}}].tag" value="{{=controlfield.tag}}"
    />
    {{ controlfieldStatusIndex=controlfieldIndex; }}
    {{ }); }}
    {{ _.each(record.datafield, function(datafield,datafieldIndex){ }}
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].id" value="{{=datafield.id}}"/>
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].tag" value="{{=datafield.tag}}"/>
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].ind1" value="{{=datafield.ind1}}"/>
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].ind2" value="{{=datafield.ind2}}"/>
    {{ _.each(datafield.subfield, function(subfield,subfieldIndex){ }}
    <div class="form-group row clearfix">
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12" for="{{=datafield.tag}}" style="text-transform: capitalize">{{=datafield.tag}}</label>
    <label class="form-control-label col-lg-1 col-md-1 col-sm-12 " for="{{=subfield.code}}">{{=subfield.code}}</label>
    <%--<c:choose>--%>
    <%--<c:when test="${('{{=datafield.tag}}' eq '100') and ('{{=subfield.code}}' eq 'a')}">--%>
    {{ if(datafield.tag === "100" && subfield.code === "a"){}}
    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 ">{{= marc21DescriptionMap[datafield.tag+"_"+subfield.code] }}
    <a href="javascript:;" style="color:#0275d8;">
    <label>[VIAF]</label>
    </a>
    </label>
    {{ }else{ }}
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 ">{{= marc21DescriptionMap[datafield.tag+"_"+subfield.code] }}</label>
    <%--</c:otherwise>--%>
    {{ } }}
    <%--</c:choose>--%>
    <div class="col-lg-6 col-md-6 col-sm-12 ui-widget">
    <input type="text" data-tag="{{=datafield.tag}}" data-subfield="{{=subfield.code}}" class="form-control metadata-tag-description"
    name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].subfield[{{=subfieldIndex}}].value"
    value="${fn:escapeXml('{{=subfield.value}}')}" />
    </div>
    </div>
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].subfield[{{=subfieldIndex}}].id"  value="{{=subfield.id}}" />
    <input type="hidden" name="collectionType.record[{{=recordIndex}}].datafield[{{=datafieldIndex}}].subfield[{{=subfieldIndex}}].code" value="{{=subfield.code}}" />
    {{ }); }}
    {{ datafieldStatusIndex=datafieldIndex; }}
    {{ }); }}
    {{ recordStatusIndex=recordIndex; }}
    {{ }); }}
    <input type="hidden" id="subfieldStatusIndex" value="0" />
    <input type="hidden" id="datafieldStatusIndex" value="{{=datafieldStatusIndex}}" />
    <input type="hidden" id="recordStatusIndex" value="{{=recordStatusIndex}}" />
    <input type="hidden" id="controlfieldStatusIndex" value="{{=controlfieldStatusIndex}}" />
    </tbody>
    </table>
</script>
<script  type="text/template" id="tpl-loader">
    <div id="z3950-search-loader">
    <span style="text-align: center;display: block;">
    <img src="${context}/themes/images/processing.gif" alt="Loading...">
    Loading results of {{=library}}...
    </span>
    </div>
</script>
<script  type="text/template" id="tpl-no-result-found">
    <div id="z3950-no-result-found" class="alert alert-warning">
    <span style="text-align: center;display: block;">
    <b>Did not match any record of {{=library}}.</b>
    </span>
    </div>
</script>
<!--place of div changed for scroll in popup start-->
<div id="light" class="white_content">
    <div id="close_btn" style="background:url(${context}/themes/images/BookReader/noti_close.png)"></div>
    <!--<div id="next_btn" style="background:url(images/noti_close.png)"></div>
    <div id="prev_btn" style="background:url(images/noti_close.png)"></div>-->
    <!--next and previous zoom start-->
    <div id="next_btn" style="background:url(${context}/themes/images/iviewer/next.png)"></div>
    <div id="prev_btn" style="background:url(${context}/themes/images/iviewer/prev.png)"></div>
    <!--next and previous zoom-->
</div>
<div id="fade" class="black_overlay"></div>
<!--place of div changed for scroll in popup end-->
<!-- Horizontal breadcrumbs -->
<ol class="breadcrumb">
    <li><a href="${context}"><i class="fa fa-home" aria-hidden="true"> </i></a></li>
        <security:authorize access="hasAnyRole('CURATOR_USER')">
        <li><a href="${context}/curation-activity/curationRegister"><tags:message code="sb.record.register"/></a></li>
        </security:authorize>
</ol>
<div style="margin-bottom:0px;" class="clearfix"></div>
<!-- search result content starts here -->
<div class="container-fluid" id="main">
    <div class="clear"></div>
    <div class="col-lg-12">
        <!--Breadcrumb-->
        <!--show hide panel-->
        <div id="alert-box" style="display: none;"></div>
        <div id="accordion">
        </div>
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div id="toolbar" class="clearfix">
            <div class="text-center center-block  ">
                <div class="btn-group btn-group-lg radio-cs-block" data-toggle="buttons">
                    <label class="btn btn-secondary active radio-cs" data-value="metadata">
                        <input type="radio" name="options" id="option_1" checked autocomplete="off" >
                        <tags:message code="label.MetadataCuration"/> </label>
                    <label class="btn btn-secondary  radio-cs" data-value="udc">
                        <input type="radio" name="options" id="option_2" autocomplete="off" >
                        <tags:message code="label.udctagging"/> </label>
                    <label class="btn btn-secondary  radio-cs" data-value="custom">
                        <input type="radio" name="options" id="option_3" autocomplete="off" >
                        <tags:message code="label.Customtagging"/> </label>
                </div>
            </div>
        </div>
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div id="viewMetadata" >
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card panel-default clearfix" id="recordMetadata">
                    <div class="card-header cardHeaderInputMargin">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12">
                                <tags:message code="label.existing.metadata"/>
                            </div>
                            <div class="col-xl-6 col-lg-12 text-right-cust m-a-0">
                                <button class="btn btn-sm btn-primary" id="isbn-search-btn" onclick="isbnSearch()">
                                    <i class="fa fa-search"></i>
                                    <tags:message code="label.search.isbn"/>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <c:set var="underScore" value="_"/>
                        <c:if test="${!isEditFirstTime}">
                            <ul class="nav nav-tabs" id="leftTab">
                                <li class="nav-item"> <a class="nav-link active" data-target="#originalTab" data-toggle="tab"><tags:message code="label.source"/></a> </li>
                                    <c:forEach items="${crowdSourceBean.crowdSourceEditBeanList}" var="crowdSourceEditBean" >
                                        <c:if test="${crowdSourceEditBean.isCuratedVersion eq 'true'}">
                                        <li class="nav-item"> <a class="nav-link" style="color:blue;" data-target="#${crowdSourceEditBean.crowdSourceEditId}" data-toggle="tab"><tags:message code="label.Curated"/></a> </li>
                                        </c:if>
                                        <c:if test="${crowdSourceEditBean.isCuratedVersion eq 'false'}">
                                        <li class="nav-item"> <a class="nav-link" data-target="#${crowdSourceEditBean.crowdSourceEditId}" data-toggle="tab"><tags:message code="label.version"/> ${crowdSourceEditBean.subEditNumber}</a> </li>
                                        </c:if>
                                    </c:forEach>
                            </ul>
                        </c:if>
                        <div class="tab-content">
                            <c:if test="${isEditFirstTime}">
                                <div class="tab-pane nopad active" id="originalMetadata">
                                    <div class="overflow685">
                                        <div class="m-a-3">
                                            <c:choose>
                                                <c:when test="${crowdSourceBean.originalMetadata.metadataType eq dcType}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                        <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="dcMetadata" value="${crowdSourceBean.originalMetadata.dcMetadata}" />
                                                    <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                        <c:if test="${field.name ne 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="metadata">
                                                                <c:if test="${not empty metadata}">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                        <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${metadata}</span> </div>
                                                                    </div>
                                                                </c:if>
                                                            </c:forEach>
                                                            <c:forEach items="${crowdSourceBean.originalMetadata.dcMetadata.others}" var="childMetadata">
                                                                <c:if test="${childMetadata.parentName eq field.name}">
                                                                    <c:forEach items="${childMetadata.tagValues}" var="otherMetadata">
                                                                        <c:if test="${not empty otherMetadata}">
                                                                            <div class="form-group row clearfix">
                                                                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${childMetadata.tagName}" style="text-transform: capitalize">${childMetadata.tagName}</label>
                                                                                <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                            </div>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${field.name eq 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="other">
                                                                <c:if test="${other.parentName eq 'NA'}">
                                                                    <c:forEach items="${other.tagValues}" var="otherMetadata">
                                                                        <c:if test="${not empty otherMetadata}">
                                                                            <div class="form-group row clearfix">
                                                                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                                <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                            </div>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:when>
                                                <c:when test="${crowdSourceBean.originalMetadata.metadataType eq marc21Type}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                        <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="collectionType" value="${crowdSourceBean.originalMetadata.collectionType}" />
                                                    <c:forEach items="${collectionType.record}" var="record">
                                                        <c:forEach items="${record.controlfield}" var="controlfield">
                                                            <c:if test="${not empty controlfield.value}">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                                    <c:set var="key" value="${controlfield.tag}"/>
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${controlfield.value}</span> </div>
                                                                </div>
                                                            </c:if>
                                                        </c:forEach>
                                                        <c:forEach items="${record.datafield}" var="datafield">
                                                            <c:forEach items="${datafield.subfield}" var="subfield">
                                                                <c:if test="${not empty subfield.value}">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${subfield.code}">${subfield.code}</label>
                                                                        <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                        <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${subfield.value}</span> </div>
                                                                        <c:if test="${(datafield.tag eq '020' ) and (subfield.code eq 'a')}">
                                                                            <c:set var="isbn_no" value="${subfield.value}"/>
                                                                        </c:if>
                                                                    </div>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                    <div class="card-footer clearfix" align="right">
                                        <div class="col-xs-offset-2 col-lg-10 col-sm-12">
                                            <c:if test="${crowdSourceBean.isRecordEditable}">
                                                <input type="button" class="btn btn-primary" onclick="selectEditVersion(false, '${crowdSourceBean.crowdSourceId}');" value="Edit"/>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${!isEditFirstTime}">
                                <div class="tab-pane nopad active" id="originalTab">
                                    <div class="overflow685">
                                        <div class="m-a-3">
                                            <c:choose>
                                                <c:when test="${crowdSourceBean.originalMetadata.metadataType eq dcType}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                        <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="dcMetadata" value="${crowdSourceBean.originalMetadata.dcMetadata}" />
                                                    <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                        <c:if test="${field.name ne 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="metadata">
                                                                <c:if test="${not empty metadata}">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                        <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${metadata}</span> </div>
                                                                    </div>
                                                                </c:if>
                                                            </c:forEach>
                                                            <c:forEach items="${crowdSourceBean.originalMetadata.dcMetadata.others}" var="childMetadata">
                                                                <c:if test="${childMetadata.parentName eq field.name}">
                                                                    <c:forEach items="${childMetadata.tagValues}" var="otherMetadata">
                                                                        <c:if test="${not empty otherMetadata}">
                                                                            <div class="form-group row clearfix">
                                                                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${childMetadata.tagName}" style="text-transform: capitalize">${childMetadata.tagName}</label>
                                                                                <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                            </div>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${field.name eq 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="other">
                                                                <c:if test="${other.parentName eq 'NA'}">
                                                                    <c:forEach items="${other.tagValues}" var="otherMetadata">
                                                                        <c:if test="${not empty otherMetadata}">
                                                                            <div class="form-group row clearfix">
                                                                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                                <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                            </div>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:when>
                                                <c:when test="${crowdSourceBean.originalMetadata.metadataType eq marc21Type}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                        <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="collectionType" value="${crowdSourceBean.originalMetadata.collectionType}" />
                                                    <c:forEach items="${collectionType.record}" var="record">
                                                        <c:forEach items="${record.controlfield}" var="controlfield">
                                                            <c:if test="${not empty controlfield.value}">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(controlfield.tag)}</label>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${controlfield.value}</span> </div>
                                                                </div>
                                                            </c:if>
                                                        </c:forEach>
                                                        <c:forEach items="${record.datafield}" var="datafield">
                                                            <c:forEach items="${datafield.subfield}" var="subfield">
                                                                <c:if test="${not empty subfield.value}">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${subfield.code}" >${subfield.code}</label>
                                                                        <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                        <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${subfield.value}</span> </div>
                                                                    </div>
                                                                    <c:if test="${(datafield.tag eq '020' ) and (subfield.code eq 'a')}">
                                                                        <c:set var="isbn_no" value="${subfield.value}"/>
                                                                    </c:if>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                    <div class="card-footer clearfix" align="right">
                                        <div class="col-xs-offset-2 col-lg-10 col-sm-12">
                                            <c:if test="${crowdSourceBean.isRecordEditable}">
                                                <input type="button" class="btn btn-primary" onclick="selectEditVersion(true, '${crowdSourceBean.crowdSourceId}');" value="Edit"/>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                                <c:forEach items="${crowdSourceBean.crowdSourceEditBeanList}" var="crowdSourceEditBean" >
                                    <div id="${crowdSourceEditBean.crowdSourceEditId}" class="tab-pane nopad">
                                        <div class="overflow685">
                                            <div class="m-a-3">
                                                <c:choose>
                                                    <c:when test="${crowdSourceEditBean.editedMetadata.metadataType eq dcType}">
                                                        <div class="form-group row clearfix">
                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                            <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                        </div>
                                                        <c:set var="dcMetadata" value="${crowdSourceEditBean.editedMetadata.dcMetadata}" />
                                                        <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                            <c:if test="${field.name ne 'others'}">
                                                                <c:forEach items="${dcMetadata[field.name]}" var="metadata" varStatus="loopCounter">
                                                                    <c:if test="${not empty crowdSourceBean.originalMetadata.dcMetadata[field.name][loopCounter.index] or not empty metadata}">
                                                                        <div class="form-group row clearfix">
                                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                            <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.dcMetadata[field.name][loopCounter.index]}" editedValue="${metadata}"/></span> </div>
                                                                        </div>
                                                                    </c:if>
                                                                </c:forEach>
                                                                <c:forEach items="${crowdSourceEditBean.editedMetadata.dcMetadata.others}" var="childMetadata">
                                                                    <c:if test="${childMetadata.parentName eq field.name}">
                                                                        <c:forEach items="${childMetadata.tagValues}" var="otherMetadata" varStatus="loopCounter">
                                                                            <c:if test="${not empty crowdSourceBean.originalMetadata.dcMetadata.others.tagValues[loopCounter.index] or not empty otherMetadata}">
                                                                                <div class="form-group row clearfix">
                                                                                    <label style="text-transform: capitalize" class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${childMetadata.tagName}">${childMetadata.tagName}</label>
                                                                                    <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.dcMetadata.others.tagValues[loopCounter.index]}" editedValue="${otherMetadata}"/></span> </div>
                                                                                </div>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test="${field.name eq 'others'}">
                                                                <c:forEach items="${dcMetadata[field.name]}" var="other">
                                                                    <c:if test="${other.parentName eq 'NA'}">
                                                                        <c:forEach items="${other.tagValues}" var="otherMetadata" varStatus="loopCounter">
                                                                            <c:if test="${not empty crowdSourceBean.originalMetadata.dcMetadata[field.name].tagValues[loopCounter.index] or not empty otherMetadata}">
                                                                                <div class="form-group row clearfix">
                                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                                    <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.dcMetadata[field.name].tagValues[loopCounter.index]}" editedValue="${otherMetadata}"/></span> </div>
                                                                                </div>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:when>
                                                    <c:when test="${crowdSourceEditBean.editedMetadata.metadataType eq marc21Type}">
                                                        <div class="form-group row clearfix">
                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold"><tags:message code="label.Tag"/></label>
                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                            <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                            <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                        </div>
                                                        <c:set var="collectionType" value="${crowdSourceEditBean.editedMetadata.collectionType}" />
                                                        <c:forEach items="${collectionType.record}" var="record" varStatus="outerCount1">
                                                            <c:forEach items="${record.controlfield}" var="controlfield" varStatus="count">
                                                                <c:if test="${not empty crowdSourceBean.originalMetadata.collectionType.record[outerCount1.index].controlfield[count.index].value or not empty controlfield.value}">
                                                                    <div class="form-group row clearfix">
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(controlfield.tag)}</label>
                                                                        <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.collectionType.record[outerCount1.index].controlfield[count.index].value}" editedValue="${controlfield.value}"/></span> </div>
                                                                    </div>
                                                                </c:if>
                                                            </c:forEach>
                                                            <c:forEach items="${record.datafield}" var="datafield" varStatus="outerCount2">
                                                                <c:forEach items="${datafield.subfield}" var="subfield" varStatus="count">
                                                                    <c:if test="${not empty crowdSourceBean.originalMetadata.collectionType.record[outerCount1.index].datafield[outerCount2.index].subfield[count.index].value or not empty subfield.value}">
                                                                        <div class="form-group row clearfix">
                                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${subfield.code}">${subfield.code}</label>
                                                                            <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                            <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                            <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text"><customTag:stringDiffereneHTML originalValue="${crowdSourceBean.originalMetadata.collectionType.record[outerCount1.index].datafield[outerCount2.index].subfield[count.index].value}" editedValue="${subfield.value}"/></span> </div>
                                                                        </div>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:forEach>
                                                        </c:forEach>
                                                    </c:when>
                                                </c:choose>
                                            </div>
                                        </div>
                                        <div class="card-footer clearfix" align="right">
                                            <div class="col-xs-offset-2 col-lg-10 col-sm-12">
                                                <c:if test="${crowdSourceBean.isRecordEditable}">
                                                    <input type="button"  class="btn btn-primary" onclick="selectEditVersion(false, '${crowdSourceEditBean.crowdSourceEditId}');" value="Edit"/>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="card panel-default clearfix" id="isbn-search-metadata" style="display: none;">
                    <div class="card-header cardHeaderInputMargin">
                        <div class="row">
                            <div class="col-xl-4 col-lg-12">
                                <tags:message code="label.search.isbn"/>
                            </div>
                            <div class="col-xl-4">
                                <div class="input-group" style="width:100%">
                                    <input class="form-control form-control-sm" placeholder="Enter ISBN" value="${isbn_no}" id="isbn-search-input">
                                    <a id="btn-isbn-search" href="javascript:isbnMetadataSearch();" class="input-group-addon" >
                                        <i class="fa fa-search"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-12 text-right-cust m-a-0">
                                <button class="btn btn-sm btn-primary" id="return-record-metadata" onclick="returnToRecordMetadata()">
                                    <i class="fa fa-mail-reply"></i>
                                    <tags:message code="label.Recordmetadata"/>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <ul class="nav nav-tabs" id="metadata-source-tabs" style="display: none;"></ul>
                        <div class="tab-content" id="metadata-source-tabs-content" style="display: none;"></div>
                        <div id="isbn-metadata-loader" style="display: none;">
                            <span style="text-align: center;display: block;">
                                <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
                            </span>
                        </div>
                        <div id="isbn-message" style="display: none;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6" id="tabId">
            <div id="edit-metadata-loader" style="display: none;">
                <span style="text-align: center;display: block;">
                    <img src="${context}/themes/images/processing.gif" alt="Loading..."/>
                </span>
            </div>
        </div>
    </div>

    <div id="udcTagDiv" hidden style="margin-bottom: 50px;">
        <div class="tab-content clearfix">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="card panel-default">
                        <div class="card-header"><tags:message code="label.Addudc.tags"/></div>
                        <div class="card-block clearfix">
                            <div class="form-group row">
                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left" ><tags:message code="label.select.language"/></label>
                                <div class="col-lg-5 col-md-7 col-sm-12">
                                    <select class="form-control" id="selectUDCLanguage" onchange="selectLanguage('selectUDCLanguage');">
                                        <c:forEach items="${languageList}" var="language">
                                            <c:if test="${language.id eq 40}">
                                                <option value="${language.id}" selected="true">${language.languageName}</option>
                                            </c:if>
                                            <c:if test="${language.id ne 40}">
                                                <option value="${language.id}">${language.languageName}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"></label>
                                <div class="col-lg-10 col-md-7 col-sm-12">
                                    <div class="input-group">
                                        <input type="text" placeholder="Enter UDC Tags..." name="udcTags" id="udcAutocomplete" class="form-control"/>
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#udcTreeViewModel"><tags:message code="button.TreeView.Selection"/></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="card panel-default">
                        <div class="card-header"><tags:message code="label.UDCTags"/></div>
                        <div class="card-block">
                            <form class="form-horizontal">
                                <div class="form-group" id="existingUDCTags"></div>
                                <div class="form-group" id="addUDCTags"></div>
                            </form>
                        </div>
                        <div class="card-footer clearfix" >
                            <div class="col-xs-offset-2 col-lg-10 col-sm-12" id="udcSaveButton">
                                <c:if test="${userType.trim() eq 'expert'}">
                                    <input type="button" class="btn btn-primary" onclick="approveTags()" value="Approve" />
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-bottom:6px;" class="clearfix" ></div>
            </div>
        </div>
    </div>
    <div id="customTagDiv" hidden>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card panel-default">
                <div class="card-header"><tags:message code="label.Addcustom.tags"/></div>
                <div class="card-block clearfix">
                    <div class="form-group row">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left" ><tags:message code="label.select.language"/></label>
                        <div class="col-lg-5 col-md-7 col-sm-12">
                            <select class="form-control" id="selectCustomLanguage" onchange="selectCustomLanguage();">
                                <c:forEach items="${languageList}" var="language">
                                    <c:if test="${language.id eq 40}">
                                        <option value="${language.id}" selected="true">${language.languageName}</option>
                                    </c:if>
                                    <c:if test="${language.id ne 40}">
                                        <option value="${language.id}">${language.languageName}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"  ></label>
                        <div class="col-lg-5 col-md-7 col-sm-12">
                            <input type="text" placeholder="Enter Custom Tags..." name="customTags" class="form-control"  id="customAutocomplete"/>
                        </div>
                    </div>
                </div>
                <div class="card-footer clearfix">
                    <div class="col-xs-offset-2 col-lg-10 col-sm-12">
                        <input type="button" class="btn btn-primary"  onclick="addCustomTags(document.getElementById('customAutocomplete').value, 'addCustomTags', 'custom');" value="Add"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card panel-default">
                <div class="card-header"><tags:message code="label.CustomTags"/></div>
                <div class="card-block">
                    <form class="form-horizontal">
                        <div class="form-group" id="existingCustomTags">
                        </div>
                        <div class="form-group" id="addCustomTags">
                        </div>
                    </form>
                </div>
                <div class="card-footer clearfix" >
                    <div class="col-xs-offset-2 col-lg-10 col-sm-12" id="customSaveButton">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<div class="clear22"></div>
<!-- s result content container -->
<div style="margin-bottom:20px;" class="clearfix"></div>
<div style="margin-bottom:0px;" class="clearfix"></div>
<div class="modal fade" id="udcTreeViewModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.UDCTags"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 form-group">
                        <select class="form-control" id="selectLanguageModel" onchange="selectLanguage('selectLanguageModel');">
                            <c:forEach items="${languageList}" var="language">
                                <c:if test="${language.id eq 40}">
                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                </c:if>
                                <c:if test="${language.id ne 40}">
                                    <option value="${language.id}">${language.languageName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                </div><br>
                <div id="udcTreeviewCheckable"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addTreeviewSelectionList();" data-dismiss="modal"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<!--modal to add marc21 tags start-->
<div class="modal fade" id="addMarc21TagModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="marc21CloseRefresh()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.Add.Marc21.Tag"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.description"/> <input  type="text" id="tagDescription" class="form-control" required/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.Tag"/>  : <input  type="text" id="tagCode" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.SubField"/> : <input type="text" readonly class="form-control" id="subfieldCode"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.TagValue"/> : <textarea name="textarea" id="tagValue" class="form-control" required="required"  maxlength="100" placeholder="Maximum 100 characters allowed"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-primary" onclick="marc21CloseRefresh();"><tags:message code="button.Reset"/></button>
                <button type="button" class="btn btn-primary" onclick="addMarc21Tag();"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<!--modal to add marc21 tags start end-->
<!--modal to add dublin core tags start-->
<div class="modal fade" id="addDCTagModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="dcCloseRefresh()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.Add.DC.Tag"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.Tag"/>  :
                        <select class="form-control" id="dc-tag-name">
                            <option  value="Contributors">Contributors</option>
                            <option  value="Coverage">Coverage</option>
                            <option  value="Creator">Creator</option>
                            <option  value="Date">Date</option>
                            <option  value="Description">Description</option>
                            <option  value="Format">Format</option>
                            <option  value="Identifier">Identifier</option>
                            <option  value="Language">Language</option>
                            <option  value="Publisher">Publisher</option>
                            <option  value="Relation">Relation</option>
                            <option  value="Rights">Rights</option>
                            <option  value="Subject">Subject</option>
                            <option  value="Source">Source</option>
                            <option  value="Title">Title</option>
                            <option  value="Type">Type</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        <tags:message code="label.Value"/> : <textarea name="textarea" id="dcTagValue" class="form-control" required="required" maxlength="100" placeholder="Maximum 100 characters allowed"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-primary" onclick="dcCloseRefresh();"><tags:message code="button.Reset"/></button>
                <button type="button" class="btn btn-primary" onclick="addDCTag();"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<!--modal to add dublin core tags end-->
<!--modal for Z39.50/SRU start-->
<div class="modal fade" id="zSearch-modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="closeButton" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    <tags:message code="label.z39.50/SRU"/>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row" id="z3950-search">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="card-header">Search Parameters</div>
                            <div class="card-block border-box">
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">Title</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="title" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">Author</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="author" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">ISBN</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="isbn" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">ISSN</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="issn" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">Keyword (any)</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="any" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">LCCN</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="lccn" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">Personal Name</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="personalName" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">Publisher</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="publisher" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="form-control-label col-lg-4">Subject Heading</label>
                                    <div class="col-lg-8">
                                        <input class="search-parameter-value" name="subject" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-header">
                                Search Targets
                                <div style="float: right;font-size: medium;font-weight: initial;">
                                    <a href="javascript:;" onClick="actionAllTargets(true);"><i class="fa fa-check"></i> Select All</a> | <a href="javascript:;" onClick="actionAllTargets(false);"><i class="fa fa-remove"></i> clear All</a>
                                </div>
                            </div>
                            <div class="card-block border-box">
                                <div class="checklistMeta" id="search-target-checkboxes"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row" id="z3950-results" style="display: none;">
                    <div class="card">
                        <div class="card-header row" style="display: block; margin: 0">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <strong class="header-title">
                                    <tags:message code="label.results"/>(<strong id="resultSize-count"></strong>)
                                </strong>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 pull-right">
                                <select id="library-filters" class="form-control"></select>
                            </div>
                        </div>
                        <div id="tbl-slim-scroll">
                            <table id="z3950-results-tbl" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="">
                                            <tags:message code="label.title"/>
                                        </th>
                                        <th width="20%">
                                            <tags:message code="label.author"/>
                                        </th>
                                        <th width="20%">
                                            <tags:message code="label.isbn.short"/>
                                        </th>
                                        <th width="10%">
                                            <tags:message code="label.lccn.short"/>
                                        </th>
                                        <th width="15%">
                                            <tags:message code="label.personalName"/>
                                        </th>
                                        <th width="10%">
                                            <tags:message code="label.action"/>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="z3950-results-table-body" ></tbody>
                            </table>
                            <div id="msg-div"></div>
                        </div>
                        <div class="card-footer clearfix" id="z3950-search-footer"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <pre width="100" id="marc-detail" style="display: none;font-weight: 550;font-size: medium;color: black;background-color: beige;"></pre>
            </div>
            <div class="modal-footer text-center">
                <button type="button" id="btn-z-search" onclick="zSearch(getParameterMap(), getSelectedLibraries());" class="btn btn-primary">Search</button>
                <button type="button" id="btn-back-z-search" onclick="backToZSearch();" class="btn btn-primary" style="display: none;">
                    <i class="fa fa-mail-reply"></i>
                    <tags:message code="label.search.z39.50/SRU"/>
                </button>
                <button type="button" id="btn-back-z-search-results" onclick="backToZSearchResults();" class="btn btn-primary" style="display: none;">
                    <i class="fa fa-mail-reply"></i>
                    <tags:message code="label.z39.50/SRU"/><tags:message code="label.results"/>
                </button>
                <button type="button" id="btnClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!--modal for Z39.50/SRU end-->
<c:if test="${userType.trim() ne 'expert'}">
    <form:form id="tagForm" method="post" action="${context}/cs/saveTags" commandName="crowdSourceTagBean">
        <form:input type="hidden" path="crowdSourceTags" id="crowdSourceTags"/>
        <form:input type="hidden" path="crowdSourceTagType" id="crowdSourceTagType"/>
        <form:input type="hidden" path="recordIdentifier" id="recordIdentifier"/>
        <input type="hidden" name="modifiedJson" id="modifiedJson" />
    </form:form>
</c:if>
<c:if test="${userType.trim() eq 'expert'}">
    <form:form id="udcTagForm" method="post" action="${context}/cs/approveUDCTags" commandName="crowdSourceUDCTagBean">
        <form:input type="hidden" path="crowdSourceTags" id="crowdSourceTags"/>
        <form:input type="hidden" path="isEdit" id="isEdit"/>
        <form:input type="hidden" path="recordIdentifier" id="recordIdentifier"/>
    </form:form>
</c:if>
