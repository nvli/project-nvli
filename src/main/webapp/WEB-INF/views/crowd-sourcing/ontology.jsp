<%-- 
    Document   : ontology
    Created on : 28 Jun, 2017, 10:04:48 AM
    Author     : Darshana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script type="text/javascript">
    $(document).ready(function () {
        getSelectedLanguageId('${pageContext.response.locale}');
    });
    function getSelectedLanguageId(languageCode) {
        $.ajax({
            url: "${context}/visualize/getLanguageCode/" + languageCode
        })
                .done(function (data) {
                    getUDCClassification(data);
                    getOntologyList();
                })
                .fail(function (xhr) {
                    console.log('error :: ', xhr);
                });
    }

    function getUDCClassification(languageId) {
        $.ajax({
            url: '${context}/search/getUDCClassification',
            data: {'languageId': languageId},
            type: 'POST',
            success: function (response)
            {
                $('#udc-categories-ul').empty();
                _.each(response, function (category) {
                    var tpl_udc_categories_ul = _.template($("#tpl-udc-categories-li").html());
                    var opts = {
                        categoryId: category.id,
                        languageId: languageId,
                        description: category.description
                    };
                    var el_udc_categories_li = tpl_udc_categories_ul(opts);
                    $("#udc-categories-ul").append(el_udc_categories_li);
                });
            },
            error: function (xhr) {
                console.error(xhr);
            }
        });
    }
    function getOntologyList() {
        $.ajax({
            url: '${context}/visualize/getjsonforDomainOnts',
            type: 'GET',
            success: function (response)
            {
                $('#ontology-categories-ul').empty();
                _.each(response, function (record) {
                    var tpl_ontology_categories_ul = _.template($("#tpl-ontology-categories-li").html());
                    var opts = {
                        ontId: record.id,
                        description: record.displayName.toUpperCase()
                    };
                    var el_ontology_categories_li = tpl_ontology_categories_ul(opts);
                    $("#ontology-categories-ul").append(el_ontology_categories_li);
                });
            },
            error: function (xhr) {
                console.error(xhr);
            }
        });
    }
</script>
<ol class="breadcrumb">
    <li><a href="${context}/home">Home</a></li>
    <li class="active">Ontology</li>
</ol>

<div id="main" class="container-fluid">
    <div class="clearfix" style="margin-bottom:20px;"></div>
    <div class="col-lg-12">
        <div class="col-xl-12">
            <div class="card bg-white">
                <div class="card-header"><tags:message code="label.titleUDC"/></div>
                <div class="col-xl-4">
                    <div class="card-block font14" style="text-align: justify;">
                        The Universal Decimal Classification (UDC) is a bibliographic and library classification representing the systematic arrangement of all branches of human knowledge organized as a coherent system in which knowledge fields are related and inter-linked. The UDC is an analytico-synthetic and faceted classification system featuring detailed vocabulary and syntax that enables powerful content indexing and information retrieval in large collections. In NVLI, diverse types of knowledge resources are automatically classified as per the UDC tags, which are helpful in subject wise structured browsing of the information. The crowdsourcing module of NVLI allows the users to correct and add UDC tags. The UDC Translation Module also allows the users to translate / localize the UDC classification labels in Indian languages.
                        <br/><br/><b>UDC Consortium</b><br/><a class="ullilinks" href="http://www.udcc.org" style="color: #003eff"><i class="fa fa-link" ></i> http://www.udcc.org</a>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="card-block ullilinks">
                        <ul id="udc-categories-ul"></ul>
                    </div>
                </div><div class="clearfix"></div>
            </div>
        </div>

        <div class="col-xl-12 m-t-1">
            <div class="card bg-white">
                <div class="card-header"><tags:message code="label.domainontology"/></div>
                <div class="col-xl-4">
                    <div class="card-block font14" style="text-align: justify;">
                        An ontology is a formal naming and definition of the types, properties, and interrelationships of the entities with a particular domain of knowledge. Ontology provides a common data model for expressing the knowledge in a semantic, systematic, structured and machine readable format. NVLI ontology module provides semantic access to various resources of knowledge using domain specific ontology. It also provides an interesting visualization of the entities within an ontology.
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="card-block ullilinks">
                        <ul id = "ontology-categories-ul">
                            <!--                            <li>
                                                            <a href="${context}/visualize/owl-ontology"><i class="fa fa-link"></i>PHYSICS ONTOLOGY</a>
                                                        </li>-->
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xl-12 m-t-1">
            <div class="card bg-white">
                <div class="card-header"><tags:message code="label.authorontology"/></div>
                <div class="col-xl-4">
                    <div class="card-block font14" style="text-align: justify;">
                        This module aims at creating the author ontology based on the relations between Authors, Co-Authors, Publishers and Resources. It allows you to find the authors through author index or by searching the name of author. After selecting a particular author, the system provides a list of co-authors and their publications. It is also possible to find authors, co-athors publication wise and organization wise. This module can be used by the researchers to find the research collaborators working in their areas of interest and organizations.
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="card-block ullilinks">
                        <ul id = "authors-ontology">
                            <li>
                                <a href="${context}/visualize/owl-authors"><i class="fa fa-link"></i>AUTHORS ONTOLOGY</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clear22"></div>
</div>

<script type="text/template" id="tpl-udc-categories-li">
    <li id="category_{{=categoryId}}">
    <a href="${context}/search/udc-tags-searchRecord/{{=categoryId}}/{{=languageId}}"><i class="fa fa-link"></i>{{=description}}</a>
    </li>
</script>
<script type="text/template" id="tpl-ontology-categories-li">
    <li id="category_{{=ontId}}">
    <a href="${context}/visualize/owl-ontology/{{=ontId}}"><i class="fa fa-link"></i>{{=description}}</a>
    </li>
</script>


