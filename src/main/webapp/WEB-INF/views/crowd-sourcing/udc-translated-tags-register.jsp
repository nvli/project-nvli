<%-- 
    Document   : udc-translated-tags-register
    Created on : Jul 14, 2017, 12:38:54 PM
    Author     : Vivek Bugale <bvivek@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request"/>
<%@include file="../../static/components/pagination/pagination.html" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:message code="label.search.placeholder" var="searchPlaceholder" /> 

<div id="alert-box"></div>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">    
    <div class="card">        
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title"><tags:message code="label.udc.translated.tags.Register"/></strong>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <input id="searchString" class="form-control" placeholder="${searchPlaceholder}">
            </div>            
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1 pull-right">
                <select id="limitFilter" class="form-control">
                    <option value="10" selected>10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="2%"><tags:message code="label.sno"/></th>
                    <th width="10%"><tags:message code="label.udc.notation"/></th>
                    <th width=""><tags:message code="label.description"/></th>
                    <th width="15%"><tags:message code="label.translation.language"/></th>
                    <th width="15%"><tags:message code="label.view.translation"/></th>
                </tr>
            </thead>
            <tbody id="translated-udc-tags-table-body"></tbody>
        </table>
        <div id="translated-udc-tags-footer" class="card-footer clearfix"></div>
    </div>
</div>

<!-- udc-tags-translation Modal start-->
<div id="udc-tags-translation-modal" tabindex="-1" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="sb.udc.translated.tags"/></h4>
            </div>
            <div class="modal-body" id="udc-tags-translation-modal-body">

            </div>
            <div class="modal-footer">
                <span class="pull-right">
                    <button type="button" class="btn btn-success" onclick="updateTranslation('accept')">Accept</button>
                    <button type="button" class="btn btn-danger"  onclick="updateTranslation('reject-all')">Reject All</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </span>
            </div>
        </div>
    </div>
</div>
<!-- udc-tags-translation Modal end-->
<script type="text/template" id="tpl-translated-udc-tags-row">
    {{ _.each(tagsJsonArray, function (tagJson,i) { }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>{{=tagJson.udcNotation}}</td>    
    <td>{{=tagJson.description}}</td>
    <td>{{=tagJson.udcLanguageName}}</td>
    <td>
        <a class="btn btn-primary see-translations" data-toggle="modal" data-target="#udc-tags-translation-modal" translation-language-id="{{=tagJson.udcLanguageId}}" data-udc-concept-id="{{=tagJson.udcConceptId}}" data-udc-description="{{=tagJson.description}}">
        Click here
        </a>
    </td> 
    </tr>
    {{ }); }}
</script>
<script type="text/template" id="tpl_tags_translation_modal_body">
    <div id="modal-alert-box"></div>
    <div class="clearfix"></div>
    <div class="alert alert-info" role="alert">
        <strong>Original Description : </strong> {{=description}}
    </div>
    <div class="form-group">
        {{_.each(seeUDCTranslatedTagsJSONArray,function(obj){ }}
            <div class="radio clearfix">
                <label for="opt-{{=obj.udcConceptId}}" class="col-lg-6">
                    <input type="radio" class="selected-new-translation" value="{{=obj.udcTagsCrowdsourceId}}" name="translation-opt" id="opt-{{=obj.udcTagsCrowdsourceId}}">
                        {{=obj.translatedDescription}}
                </label>
                <div class="col-lg-5"><progress class="progress" value="{{=votingMap[obj.udcTagsCrowdsourceId]}}" max="100"></progress></div>
                <div class="col-lg-1">{{=votingMap[obj.udcTagsCrowdsourceId]}}%</div>
            </div>
        {{ }); }}
    </div>
</script>
<script type="text/javascript">
    var _translationJSONArray=[];
    var _pageNo;
    function fetchTranslatedTags(pageNo) {
        _pageNo=pageNo;
        $.ajax({
            url: '${context}/cs/udc-translated-tag-register',
            data: {searchString: $('#searchString').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo, isRequiredUserRestriction: true},
            type: 'POST'
        })
                .done(function (response) {
                    if (!_.isEmpty(response)) {
                        if (_.isEmpty(response.tagsJsonObject.uDCConceptDescriptionJSONArray)) {
                            $("#translated-udc-tags-table-body").html('<tr><td colspan="5" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                        } else {
                            var translated_tags_Tpl = _.template($("#tpl-translated-udc-tags-row").html());
                            var Opts = {
                                "tagsJsonArray": response.tagsJsonObject.uDCConceptDescriptionJSONArray,
                                "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                            };
                            var translated_tags_el = translated_tags_Tpl(Opts);
                            $("#translated-udc-tags-table-body").empty();
                            $("#translated-udc-tags-table-body").html(translated_tags_el);
                            bindSeeTranslation();
                        }
                        renderPagination(fetchTranslatedTags, "translated-udc-tags-footer","page-btn", pageNo, response.totalCount, $('#limitFilter').val(), true, 5, true);
                    } else {
                        alertErrorMsg();
                        $("#translated-udc-tags-table-body").html('<tr><td colspan="5" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    }
                })
                .fail(function (xhr) {
                    console.log("error while fetching Translated Tags::", xhr);
                    $("#translated-udc-tags-table-body").html('<tr><td colspan="5" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    alertErrorMsg();
                });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }

    function bindSeeTranslation() {
        $(".see-translations").unbind().bind('click', function (e) {
            $.ajax({
                url: "${context}/cs/see-udc-translated-tag",
                data: {
                    conceptId: e.currentTarget.getAttribute('data-udc-concept-id'),
                    languageId: e.currentTarget.getAttribute('translation-language-id')
                },
                type: 'POST',
                dataType: 'json'
            })
            .done(function (response) {
                if(!_.isEmpty(response)){
                    _translationJSONArray=response.seeUDCTranslatedTagsJSONArray;
                    var see_translations_Tpl = _.template($("#tpl_tags_translation_modal_body").html());
                    var Opts = {
                        description: e.currentTarget.getAttribute("data-udc-description"),
                        seeUDCTranslatedTagsJSONArray: response.seeUDCTranslatedTagsJSONArray,
                        votingMap: response.votingMap
                    };
                    var see_translations_el = see_translations_Tpl(Opts);
                    $("#udc-tags-translation-modal-body").empty();
                    $("#udc-tags-translation-modal-body").html(see_translations_el);                    
                }
            })
            .fail(function (xhr) {
                console.log("error while fetching translated tag ::", xhr)
            });
        });
    }
    
    function updateTranslation(action){    
            var rejectedTransalationsArr=[];
            var selectedTranslationArr=[];
            var finalJsonObject = {};
            if(action==='accept'){        
                if(!$('input[name=translation-opt]:checked').length<=0) {                    
                    var selectedTagId=$('input[name="translation-opt"]:checked').val();
                    selectedTranslationArr= $.grep(_translationJSONArray, function(obj) {
                        return obj.udcTagsCrowdsourceId == selectedTagId;
                    });
                    rejectedTransalationsArr=$.grep(_translationJSONArray, function(obj) {
                        return obj.udcTagsCrowdsourceId != selectedTagId;
                    });
                    finalJsonObject["selectedTranslationArr"]=JSON.stringify(selectedTranslationArr);
                    finalJsonObject["rejectedTransalationsArr"]=JSON.stringify(rejectedTransalationsArr);                    
                    updateTagTranslation(finalJsonObject);
                    $("#udc-tags-translation-modal").modal("hide");
                }else{           
                    alertBoxMessage("modal-alert-box","Translation not selected!","alert alert-warning",alertAnimation);
                }
            }else{  
                rejectedTransalationsArr=_translationJSONArray;
                finalJsonObject["selectedTranslationArr"]=JSON.stringify(selectedTranslationArr);
                finalJsonObject["rejectedTransalationsArr"]=JSON.stringify(rejectedTransalationsArr);
                updateTagTranslation(finalJsonObject);
                $("#udc-tags-translation-modal").modal("hide");
            }    
    } 
    
    function updateTagTranslation(finalJsonObject){
        $.ajax({
            url: "${context}/cs/update-udc-translation",
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            processData:false,
            data : JSON.stringify(finalJsonObject)
        })
        .done(function (response){            
            if(response){
//                action==="accept"?
//                alertBoxMessage("alert-box","Translation saved successfully!","alert alert-success",alertAnimation):
//                alertBoxMessage("alert-box","All translations are rejected !","alert alert-success",alertAnimation);
                fetchTranslatedTags(_pageNo);
            }else{
               // alertBoxMessage("alert-box","Something went wrong,please try again!","alert alert-danger",alertAnimation);
            }            
        })
        .fail(function (xhr){
            console.log("error while updating Tag ::",xhr);
           // alertBoxMessage("alert-box","Service not available,try after some time!","alert alert-danger",alertAnimation);
        });     
    }
    
    function alertBoxMessage(id,text,classname,callback){
        $("#"+id).text(text);
        $("#"+id).removeClass();
        $("#"+id).addClass("col-sm-12 col-md-12 col-lg-12 ").addClass(classname);
        callback(id);
    }

    function alertAnimation(id){
        $("#"+id)
            .first().hide().fadeIn(200)
            .delay(2000)
            .fadeOut(1000);
    }

    $(document).ready(function () {
        fetchTranslatedTags(1);
        $("#searchString").unbind().bind("keyup", function () {
            _pageNo=1;
            fetchTranslatedTags(_pageNo);
        });
        $("#statusTypeFilter").unbind().bind("change", function () {
            _pageNo=1;
            fetchTranslatedTags(_pageNo);
        });
        $("#limitFilter").unbind().bind("change", function () {
            _pageNo=1;
            fetchTranslatedTags(_pageNo);
        });
    });

</script>