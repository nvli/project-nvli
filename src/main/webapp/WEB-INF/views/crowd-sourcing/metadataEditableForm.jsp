<%--
    Document   : metadataEditableForm
    Created on : May 23, 2016, 3:47:26 PM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="in.gov.nvli.util.Constants"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set value="<%=Constants.MetadataStandardType.DUBLIN_CORE%>" var="dcType"></c:set>
<c:set value="<%=Constants.MetadataStandardType.MARC21%>" var="marc21Type"></c:set>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<div class="card panel-default clearfix" >
    <div class="card-header bg-warning cardHeaderInputMargin">        
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-12 col-lg-6 col-xl-6">
                <tags:message code="label.edit.metadata"/>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-12 col-lg-4 col-xl-4 text-right-cust m-a-0"> 
                <c:if test="${metadataBeanForm.metadataType eq marc21Type}">
                    <a href="javascript:;" class="btn btn-primary btn-sm" onclick="showZsearch();">
                        <i class="fa fa-search"></i>
                        <tags:message code="label.search.z39.50/SRU"/>
                    </a>    
                </c:if>  
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 col-xl-2">
                <select class="form-control form-control-sm" id="selectCurationLanguage">            
                    <option value="40">English</option>
                    <option value="63">हिंदी</option>
                    <option value="106">मराठी</option>
                    <option value="19">বাংলা</option>
                    <option value="158">తెలుగు</option>
                    <option value="156">தமிழ்</option>
                    <option value="58">ગુજરાતી</option>
                    <option value="80">ಕನ್ನಡ</option>
                    <option value="104">മലയാളം</option>
                    <option value="126">ਪੰਜਾਬੀ</option>
                </select>
            </div>
        </div>
    </div>
    <div class="card-block">
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane nopad fade in active" >
                <div class="overflow685">
                    <form:form id="metadataForm" method="post" action="${context}/cs/saveMetaData" commandName="metadataBeanForm" modelAttribute="metadataBeanForm" class="m-a-1">
                        <div class="panel-body" id="metadataDiv">
                            <input type="hidden" name="crowdSourceId" value="${metadataBeanForm.crowdSourceId}"/>
                            <input type="hidden" name="recordIdentifier" value="${metadataBeanForm.recordIdentifier}"/>
                            <input type="hidden" id="metadataType" name="metadataType" value="${metadataBeanForm.metadataType}"/>
                            <input type="hidden" name="userType" value="${metadataBeanForm.userType}"/>
                            <input type="hidden" name="isEditFirstTime" value="${metadataBeanForm.isEditFirstTime}"/>
                            <input type="hidden" id="editStatus" name="editStatus" value="no"/>
                            <input type="hidden" id="approved" name="approved" value="false"/>
                            <input type="hidden" id="isEdit" name="isEdit" value="false"/>
                            <div id="edit-metadata-table" >
                                <table class="table">
                                    <tbody>
                                        <c:set var="underScore" value="_"/>
                                        <c:choose>
                                            <c:when test="${metadataBeanForm.metadataType eq dcType}">
                                            <div class="form-group row clearfix">
                                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                            </div>
                                            <c:set var="dcMetadata" value="${metadataBeanForm.dcMetadata}" />
                                            <c:set var="parentList" value="" />
                                            <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                <c:if test="${field.name ne 'others'}">
                                                    <c:set var="parentList" value="${parentList},${field.name}" />
                                                    <c:if test="${fn:length(dcMetadata[field.name]) eq 0}">
                                                        <div class="form-group row clearfix dc-${field.name}" data-dc-tag="${field.name}">
                                                            <c:choose>
                                                                <c:when test="${field.name eq 'creator'}">
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                </c:otherwise>
                                                            </c:choose>                                                        
                                                            <div class="col-lg-10 col-md-7 col-sm-12">
                                                                <input type="text" class="form-control metadata-tag-description" data-tag="${field.name}" name="dcMetadata.${field.name}[0]" data-old-value=""/>
                                                            </div>
                                                        </div>
                                                    </c:if>
                                                    <c:forEach items="${dcMetadata[field.name]}" var="metadata" varStatus="metadataStatus">
                                                        <div class="form-group row clearfix dc-${field.name}" data-dc-tag="${field.name}">
                                                            <c:choose>
                                                                <c:when test="${field.name eq 'creator'}">
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                </c:otherwise>
                                                            </c:choose>                                                                                                                
                                                            <div class="col-lg-10 col-md-7 col-sm-12">
                                                                <input type="text" class="form-control metadata-tag-description" data-tag="${field.name}" name="dcMetadata.${field.name}[${metadataStatus.index}]" value="${fn:trim(fn:escapeXml(metadata))}" data-old-value="${fn:trim(fn:escapeXml(metadata))}"/>
                                                            </div>
                                                        </div>
                                                    </c:forEach>
                                                    <c:forEach items="${metadataBeanForm.dcMetadata.others}" var="other" varStatus="otherStatus">
                                                        <c:if test="${other.parentName eq field.name}">
                                                            <c:forEach items="${other.tagValues}" var="otherMetadata" varStatus="otherMetadataStatus">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                    <div class="col-lg-10 col-md-7 col-sm-12">
                                                                        <input type="text" class="form-control metadata-tag-description" data-tag="${other.tagName}" name="dcMetadata.others[${otherStatus.index}].tagValues[${otherMetadataStatus.index}]" value="${fn:trim(fn:escapeXml(otherMetadata))}" data-old-value="${fn:trim(fn:escapeXml(otherMetadata))}"/>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="dcMetadata.others[${otherStatus.index}].parentName" value="${other.parentName}"/>
                                                                <input type="hidden" name="dcMetadata.others[${otherStatus.index}].tagName" value="${other.tagName}"/>
                                                            </c:forEach>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                            </c:forEach>
                                            <c:forEach items="${dcMetadata.others}" var="other" varStatus="otherStatus">
                                                <c:if test="${other.parentName eq 'NA' || !fn:contains(parentList, other.parentName)}">
                                                    <c:forEach items="${other.tagValues}" var="otherMetadata" varStatus="otherMetadataStatus">
                                                        <div class="form-group row clearfix">
                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                            <div class="col-lg-10 col-md-7 col-sm-12">
                                                                <input type="text" class="form-control metadata-tag-description" data-tag="${other.tagName}" name="dcMetadata.others[${otherStatus.index}].tagValues[${otherMetadataStatus.index}]" value="${fn:trim(fn:escapeXml(otherMetadata))}" data-old-value="${fn:trim(fn:escapeXml(otherMetadata))}"/>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="dcMetadata.others[${otherStatus.index}].parentName" value="${other.parentName}"/>
                                                        <input type="hidden" name="dcMetadata.others[${otherStatus.index}].tagName" value="${other.tagName}"/>
                                                    </c:forEach>
                                                </c:if>
                                            </c:forEach>
                                            <input type="hidden" id="dcRecordStatusIndex" value="${recordStatusIndex}"/>
                                        </c:when>
                                        <c:when test="${metadataBeanForm.metadataType eq marc21Type}">
                                            <div class="form-group row clearfix">
                                                <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" style="text-transform: capitalize"><tags:message code="label.Tag"/></label>
                                                <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                            </div>
                                            <c:set var="collectionType" value="${metadataBeanForm.collectionType}" />
                                            <input type="hidden" name="collectionType.id" value="${collectionType.id}"/>
                                            <c:set var="controlfieldStatusIndex" value="0"/>
                                            <c:set var="datafieldStatusIndex" value="0"/>
                                            <c:forEach items="${collectionType.record}" var="record" varStatus="recordStatus">
                                                <input type="hidden" name="collectionType.record[${recordStatus.index}].leader.value" value="${record.leader.value}"/>
                                                <input type="hidden" name="collectionType.record[${recordStatus.index}].leader.id" value="${record.leader.id}"/>
                                                <input type="hidden" name="collectionType.record[${recordStatus.index}].type" value="${record.type}"/>
                                                <input type="hidden" name="collectionType.record[${recordStatus.index}].id" value="${record.id}"/>
                                                <c:forEach items="${record.controlfield}" var="controlfield" varStatus="controlfieldStatus">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(controlfield.tag)}</label>
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <input type="text" class="form-control metadata-tag-description" data-tag="${controlfield.tag}" data-subfield="" name="collectionType.record[${recordStatus.index}].controlfield[${controlfieldStatus.index}].value" value="${fn:trim(fn:escapeXml(controlfield.value))}" data-old-value="${fn:trim(fn:escapeXml(controlfield.value))}" />
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="collectionType.record[${recordStatus.index}].controlfield[${controlfieldStatus.index}].id" value="${controlfield.id}"/>
                                                    <input type="hidden" name="collectionType.record[${recordStatus.index}].controlfield[${controlfieldStatus.index}].tag" value="${controlfield.tag}"/>
                                                    <c:set var="controlfieldStatusIndex" value="${controlfieldStatus.index}"/>
                                                </c:forEach>
                                                <c:forEach items="${record.datafield}" var="datafield" varStatus="datafieldStatus">
                                                    <input type="hidden" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].id" value="${datafield.id}"/>
                                                    <input type="hidden" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].tag" value="${datafield.tag}"/>
                                                    <input type="hidden" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].ind1" value="${datafield.ind1}"/>
                                                    <input type="hidden" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].ind2" value="${datafield.ind2}"/>
                                                    <c:forEach items="${datafield.subfield}" var="subfield" varStatus="subfieldStatus">
                                                        <div class="form-group row clearfix">
                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-12" for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                            <label class="form-control-label col-lg-1 col-md-1 col-sm-12 " for="${subfield.code}">${subfield.code}</label>
                                                            <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                            <c:choose>
                                                                <c:when test="${(datafield.tag eq '100') and (subfield.code eq 'a')}">
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)} <a href="javascript:;" style="color:#0275d8;"><label>[VIAF]</label></a></label>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                </c:otherwise>
                                                            </c:choose>                                                                                                                  
                                                            <div class="col-lg-6 col-md-6 col-sm-12 ui-widget">
                                                                <input type="text" class="form-control metadata-tag-description" data-tag="${datafield.tag}" data-subfield="${subfield.code}" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].subfield[${subfieldStatus.index}].value" value="${fn:trim(fn:escapeXml(subfield.value))}" data-old-value="${fn:trim(fn:escapeXml(subfield.value))}" />
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].subfield[${subfieldStatus.index}].id" value="${subfield.id}"/>
                                                        <input type="hidden" name="collectionType.record[${recordStatus.index}].datafield[${datafieldStatus.index}].subfield[${subfieldStatus.index}].code" value="${subfield.code}"/>
                                                    </c:forEach>
                                                    <c:set var="datafieldStatusIndex" value="${datafieldStatus.index}"/>
                                                </c:forEach>
                                                <c:set var="recordStatusIndex" value="${recordStatus.index}"/>
                                            </c:forEach>
                                            <input type="hidden" id="subfieldStatusIndex" value="0"/>
                                            <input type="hidden" id="datafieldStatusIndex" value="${datafieldStatusIndex}"/>
                                            <input type="hidden" id="recordStatusIndex" value="${recordStatusIndex}"/>
                                            <input type="hidden" id="controlfieldStatusIndex" value="${controlfieldStatusIndex}"/>
                                            </tbody>
                                        </c:when>
                                    </c:choose>
                                </table>
                            </div>
                        </div>
                        <input type="hidden" name="modifiedMetadataJson" id="modifiedMetadataJson" />
                    </form:form>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer clearfix">
        <div class="col-lg-5 col-sm-12">
            <a href="javascript:;" style="color:#0275d8;"><label style="margin-top: 5px;"> Virtual International Authority File [VIAF] Supported </label></a>
        </div>
        <div class="col-lg-7 col-sm-12" align="right">
            <c:if test="${metadataBeanForm.userType eq 'normal'}">
                <c:if test="${metadataBeanForm.metadataType eq marc21Type}">
                    <input type="button" class="btn btn-primary" value="Add Tag" data-toggle="modal" data-target="#addMarc21TagModal"/>                    
                </c:if>
                <c:if test="${metadataBeanForm.metadataType eq dcType}">
                    <input type="button" class="btn btn-primary" value="Add Tag" data-toggle="modal" data-target="#addDCTagModal"/>
                </c:if>
                <input type="button" class="btn btn-primary" value="Submit" onclick="$('#approved').val(false);
                        $('#metadataForm').submit();" />
            </c:if>
            <c:if test="${metadataBeanForm.userType ne 'normal'}">
                <input type="checkbox" class="btn btn-primary"  onclick="setEditStatusValue(this);"/>
                <label><tags:message code="label.Close.crowdsourcing"/></label>
                &nbsp;&nbsp;
                <c:if test="${metadataBeanForm.metadataType eq marc21Type}">
                    <input type="button" class="btn btn-primary" value="Add Tag" data-toggle="modal" data-target="#addMarc21TagModal"/>                    
                </c:if>
                <c:if test="${metadataBeanForm.metadataType eq dcType}">
                    <input type="button" class="btn btn-primary" value="Add Tag" data-toggle="modal" data-target="#addDCTagModal"/>
                </c:if>
                <input type="button" class="btn btn-primary" value="Approve" onclick="$('#approved').val(true);
                        $('#metadataForm').submit();" />
            </c:if>
        </div>
    </div>
</div>