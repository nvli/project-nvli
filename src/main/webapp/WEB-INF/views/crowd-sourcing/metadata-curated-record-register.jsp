<%--
    Document   : assigned-record-register
    Created on : Apr 25, 2016, 12:04:26 PM
    Author     : Ritesh Malviya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<c:if test="${not empty status}">
    <c:if test="${status}">
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong><tags:message code="label.Approved.successfully"/></strong>
        </div>
    </c:if>
    <c:if test="${!status}">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong><tags:message code="label.button.Failed.approve"/></strong> <tags:message code="label.Try"/>
        </div>
    </c:if>
</c:if>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="card">
            <div class="card-header"><tags:message code="label.Metadata.Curated.Record"/>
                <div class="pull-rights font12"><tags:message code="label.per.page"/>
                    <select id="limit">
                        <option selected="selected" value="15">15</option>
                        <option value="30">30</option>
                        <option value="45">45</option>
                        <option value="60">60</option>
                    </select>
                </div>
            </div>
            <div class="card-blojck">
                <div class="table-responsive">
                    <table class="table table-bordered tr_hand">
                        <thead>
                            <tr>
                                <th width="5%"><tags:message code="label.sno"/></th>
                                <th><tags:message code="label.Record.Identifier"/></th>
                                <th><tags:message code="label.Record.Title"/></th>
                                <th><tags:message code="label.Edit"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${not empty crowdSourceRecords}">
                                <c:set var="crowdSourceRecordIndex" value="${((pageNumber-1)*pageWindow)+1}"/>
                                <c:forEach items="${crowdSourceRecords}" var="crowdSourceRecord">
                                    <tr onclick='gotoEditMetadata("${context}/cs/approve-record-metadata/${crowdSourceRecord.id}")'>
                                        <td>${crowdSourceRecordIndex}</td>
                                        <td>${crowdSourceRecord.recordIdentifier}</td>
                                        <td>${crowdSourceRecord.recordTitle}</td>
                                        <td>${crowdSourceRecord.editNumber}</td>
                                    </tr>
                                    <c:set var="crowdSourceRecordIndex" value="${crowdSourceRecordIndex+1}"/>
                                </c:forEach>
                            </c:if>
                            <c:if test="${empty crowdSourceRecords}">
                                <tr><td colspan="3"><center><h1><tags:message code="label.data"/></h1></center></td></tr>
                            </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer clearfix">
                <span class="pull-right">
                    <ul class="pagination pagination-sm">
                        <%--For displaying Page numbers. --%>
                        <c:choose>
                            <c:when test="${totalPages eq 0}">
                                <li class="page-item active"> <a class="page-link" > 1 <span class="sr-only">(current)</span></a> </li>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${totalPages lt 10}">
                                            <c:if test="${pageNumber ne 1}">
                                            <li class="page-item"> <a class="page-link pl"  data-page-num="1" data-page-window="${pageWindow}" href="#">[1] </a> </li>
                                            <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${pageNumber-1}" data-page-window="${pageWindow}" href="#"> <span aria-hidden="true">&laquo;</span> <span class="sr-only"><tags:message code="label.previous"/></span> </a> </li>
                                            </c:if>
                                            <c:forEach begin="1" end="${totalPages}" var="i">
                                                <c:set var="lastCount" value="${i}"/>
                                                <c:choose>
                                                    <c:when test="${pageNumber eq i}">
                                                    <li class="active page-item"> <a class="page-link" >${i} <span class="sr-only">(current)</span></a> </li>
                                                    </c:when >
                                                    <c:otherwise>
                                                    <li class="page-item">
                                                        <a class="page-link pl" data-page-num="${i}" data-page-window="${pageWindow}" href="#">${i}</a>
                                                    </li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                        <c:if test="${lastCount ne pageNumber}">
                                            <li class="page-item"> <a class="page-link pl" href="#" aria-label="Next" data-page-num="${pageNumber+1}" data-page-window="${pageWindow}"> <span aria-hidden="true">»</span> <span class="sr-only"><tags:message code="label.next"/></span> </a></li>
                                            <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${totalPages}" data-page-window="${pageWindow}" href="#">[${totalPages}] </a> </li>
                                            </c:if>
                                        </c:when>
                                        <c:otherwise>
                                            <c:if test="${pageNumber ne 1}">
                                            <li class="page-item"> <a class="page-link pl"  data-page-num="1" data-page-window="${pageWindow}" href="#">[1] </a> </li>
                                            <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${pageNumber-1}" data-page-window="${pageWindow}" href="#"> <span aria-hidden="true">&laquo;</span> <span class="sr-only"><tags:message code="label.previous"/></span> </a> </li>
                                            </c:if>
                                            <c:set var="begin" value="${pageNumber-4}"/>
                                            <c:set var="end" value="${pageNumber+4}"/>
                                            <c:if test="${(pageNumber-4) lt 1}">
                                                <c:set var="begin" value="1"/>
                                            </c:if>
                                            <c:if test="${(totalPages-4) lt pageNumber}">
                                                <c:set var="end" value="${totalPages}"/>
                                            </c:if>
                                            <c:forEach begin="${begin}" end="${end}" var="i">
                                                <c:set var="lastCount" value="${i}"/>
                                                <c:choose>
                                                    <c:when test="${pageNumber eq i}">
                                                    <li class="active page-item"> <a class="page-link" >${i} <span class="sr-only">(current)</span></a> </li>
                                                    </c:when >
                                                    <c:otherwise>
                                                    <li class="page-item">
                                                        <a class="page-link pl" data-page-num="${i}" data-page-window="${pageWindow}" href="#">${i}</a>
                                                    </li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                        <c:if test="${lastCount ne pageNumber}">
                                            <li class="page-item"> <a class="page-link pl" href="#" aria-label="Next" data-page-num="${pageNumber+1}" data-page-window="${pageWindow}"> <span aria-hidden="true">»</span> <span class="sr-only"><tags:message code="label.next"/></span> </a></li>
                                            <li class="page-item"> <a class="page-link pl" aria-label="Previous" data-page-num="${totalPages}" data-page-window="${pageWindow}" href="#">[${totalPages}] </a> </li>
                                            </c:if>
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function gotoEditMetadata(url)
    {
        window.location.href = url;
    }
    $(document).ready(function () {
        $("#limit option[value='${pageWindow}']").prop('selected', true);
        $("#limit").unbind().bind("change", function (e) {
            var action = "${context}/cs/metadata-curated-record-register/1/" + $("#limit").val();
            var form = document.createElement("form");
            form.method = "GET";
            form.action = action;
            form.submit();
        });
        $(".pl").unbind().bind("click", function (e) {
            var pageNumber = e.currentTarget.getAttribute("data-page-num");
            var pageWindow = e.currentTarget.getAttribute("data-page-window");
            var action = "${context}/cs/metadata-curated-record-register/" + pageNumber + "/" + pageWindow;
            var form = document.createElement("form");
            form.method = "GET";
            form.action = action;
            form.submit();
        });
    });
</script>