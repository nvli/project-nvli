<%--
    Document   : approve-tags
    Created on : May 12, 2016, 5:46:51 PM
    Author     : Doppa Srinivas
--%>

<%@page import="in.gov.nvli.util.Constants"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<c:set value="<%=Constants.MetadataStandardType.DUBLIN_CORE%>" var="dcType"></c:set>
<c:set value="<%=Constants.MetadataStandardType.MARC21%>" var="marc21Type"></c:set>
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<!--iviewer css start-->
<link rel="stylesheet" href="${context}/themes/css/iviewer/jquery.iviewer.css">
<link rel="stylesheet" href="${context}/themes/css/BookReader/canvasFlipbook.css" />
<!--iviewer css end-->

<!--iviewer and canvasFlipBook js start-->
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery-migrate-1.2.1.js" ></script>
<!--<script type="text/javascript" src="${context}/themes/js/iviewer/jqueryui.js"></script>-->
<script type="text/javascript" src="${context}/themes/js/jquery-ui.js"></script>
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery.mousewheel.js"></script>
<script type="text/javascript" src="${context}/themes/js/iviewer/jquery.iviewer.js"></script>
<script type="text/javascript" src="${context}/themes/BookReader/canvasFlipbook.js"></script>
<!--iviewer and canvasFlipBook js end-->
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src="${context}/themes/js/language/pramukhindic.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>
<script type="text/javascript">
    var nodeIds;
    var notation;
    var notationText;
    var udcJson = {};
    var existingUDCOriginalTags = '${existingUDCOriginalTags}';
    var existingUDCTagsAdded = '${existingUDCTagsAdded}';
    var crowdSourceUDCTagId = '${crowdSourceUDCTagId}';
    var recordIdentifier = '${recordIdentifier}';
    var existingUDCTagsJsonObject = {};
    var languageId = '40';
    var value;
    var contentType = '${contentType}';

    $(document).ready(function () {
        setLanguage("selectLanguage", "udcAutocomplete");
        getRecordContent();
        stringToJSON(existingUDCOriginalTags, 'existing');
        stringToJSON(existingUDCTagsAdded, 'tagsAdded');
        addTags(existingUDCTagsJsonObject, 'existingUDCTags');
        addTags(udcJson, 'addUDCTags');
        getUDCTreeviewData();
    });

    function addTags(tagsJson, div)
    {
        document.getElementById(div).innerHTML = '';
        if (!checkJsonIsEmpty(tagsJson))
        {
            var divHtml = '';
            $.each(tagsJson, function (keyName, value)
            {
                divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" onclick="deleteTag(\'' + keyName + '\',\'' + div + '\');" title="' + value + '"> <span class="fa fa-remove"></span>  ' + keyName + '   ' + [value] + ' </button>';
            });
            document.getElementById(div).innerHTML = divHtml;
            //document.getElementById('udcApproveButton').innerHTML = '<input type="button" class="btn btn-primary" onclick="approveTags()" value="Approve"/>';
        }
//        else
//        {
//            document.getElementById('udcApproveButton').innerHTML = '';
//        }
    }

    function deleteTag(key, div)
    {
        if (div === 'existingUDCTags')
        {
            delete existingUDCTagsJsonObject[key];
            addTags(existingUDCTagsJsonObject, div);
        } else
        {
            delete udcJson[key];
            addTags(udcJson, div);
        }
        var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[key]);
        $('#udcTreeviewCheckable').treeview('uncheckNode', [nodeId, {silent: true}]);
//        if (checkJsonIsEmpty(existingUDCTagsJsonObject) || checkJsonIsEmpty(existingUDCTagsJsonObject))
//            document.getElementById('udcApproveButton').innerHTML = '';
    }

    function approveTags()
    {
        var tags = '';
        $.each(existingUDCTagsJsonObject, function (keyName, value)
        {
            if (tags === '')
                tags = keyName;
            else
                tags = tags + "&|&" + keyName;
        });

        $.each(udcJson, function (keyName, value)
        {
            if (tags === '')
                tags = keyName;
            else
                tags = tags + "&|&" + keyName;
        });
        document.getElementById("crowdSourceUDCTagId").value = crowdSourceUDCTagId;
        document.getElementById("crowdSourceTags").value = tags;
        document.getElementById("isEdit").value = 'no';
        $('#udcTagForm').submit();
    }

//    function addExistingTags(tagsJson, div)
//    {
//        document.getElementById(div).innerHTML = '';
//        if (!checkJsonIsEmpty(tagsJson))
//        {
//            var divHtml = '';
//            $.ajax({
//                url: '${context}/cs/getUDCTagDescription',
//                dataType: 'json',
//                type: "post",
//                data: {
//                    'udcNotations': existingUDCOriginalTags,
//                    'languageId': languageId
//                },
//                success: function (response)
//                {
//                    for (var key in response)
//                        divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" title="' + response[key] + '">   ' + key + '   ' + [response[key]] + ' </button>';
//                    document.getElementById(div).innerHTML = divHtml;
//                },
//                error: function ()
//                {
//
//                }
//            });
//        }
//    }

    function checkJsonIsEmpty(tagsJson)
    {
        return (Object.keys(tagsJson).length === 0 && JSON.stringify(tagsJson) === JSON.stringify({}));
    }

    function selectLanguage(languageDiv)
    {
        languageId = document.getElementById(languageDiv).value;
        document.getElementById('selectLanguage').value = languageId;
        document.getElementById('selectLanguageModel').value = languageId;
        addTags(existingUDCTagsJsonObject, 'existingUDCTags');
        if (!checkJsonIsEmpty(udcJson))
        {
            existingUDCTagsAdded = '';
            $.each(udcJson, function (keyName, value)
            {
                if (existingUDCTagsAdded === '')
                    existingUDCTagsAdded = keyName;
                else
                    existingUDCTagsAdded = existingUDCTagsAdded + "&|&" + keyName;
            });
            var jsonObject = {};
            $.ajax({
                url: '${context}/cs/getUDCTagDescription',
                dataType: 'json',
                type: "post",
                data: {
                    'udcNotations': existingUDCTagsAdded,
                    'languageId': languageId
                },
                success: function (response)
                {
                    for (var key in response)
                        jsonObject[key] = response[key];
                    if (!checkJsonIsEmpty(jsonObject)) {
                        udcJson = jsonObject;
                        addTags(udcJson, 'addUDCTags', 'udc');
                    }
                },
                error: function ()
                {

                }
            });
        }
        suggestions();
        getUDCTreeviewData();
    }

    function suggestions()
    {
        $('#udcAutocomplete').devbridgeAutocomplete({
            paramName: 'tagPrefix',
            serviceUrl: '${context}/cs/getUDCTagsSuggestions?languageId=' + languageId,
            maxHeight: 120,
            onSelect: function (suggestion) {
                if ((!checkJsonIsEmpty(existingUDCTagsJsonObject) && (existingUDCTagsJsonObject[suggestion.data] != null)) || (!checkJsonIsEmpty(udcJson) && (udcJson[suggestion.data] != null)))
                    alert('Tag already exist!');
                else
                {
                    udcJson[suggestion.data] = suggestion.value;
                    addTags(udcJson, 'addUDCTags', 'udc');
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[suggestion.data]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                }
                document.getElementById('udcAutocomplete').value = '';
            }
        });
    }

    function stringToJSON(tags, type)
    {
        if (tags !== '' && tags.length !== 0)
        {
            var jsonObject = {};
            $.ajax({
                url: '${context}/cs/getUDCTagDescription',
                dataType: 'json',
                type: "post",
                data: {
                    'udcNotations': tags,
                    'languageId': languageId
                },
                success: function (response)
                {
                    for (var key in response)
                        jsonObject[key] = response[key];
                    if (type === 'existing') {
                        existingUDCTagsJsonObject = jsonObject;
                        addTags(existingUDCTagsJsonObject, 'existingUDCTags');
                    } else {
                        udcJson = jsonObject;
                        addTags(udcJson, 'addUDCTags');
                    }
//                    if (!checkJsonIsEmpty(existingUDCTagsJsonObject))
//                        document.getElementById('udcApproveButton').innerHTML = '<input type="button" class="btn btn-primary" onclick="approveTags()" value="Approve"/>';
                },
                error: function ()
                {

                }
            });
        }

    }

    function getUDCTreeviewData()
    {
        var tags = existingUDCOriginalTags;
        $.ajax({
            url: '${context}/cs/getUDCTreeviewData',
            data: 'languageId=' + languageId + '&tags=' + tags + '&isDisable=true',
            type: 'POST',
            success: function (response)
            {
                nodeIds = response["nodeIds"];
                $('#udcTreeviewCheckable').treeview({
                    data: response["udcTreeViewData"],
                    showIcon: false,
                    showCheckbox: true,
                    onNodeChecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        notationText = node.text.split("]")[1].trim();
                        udcJson[notation] = notationText;
                    },
                    onNodeUnchecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        delete udcJson[notation];
                    }
                });
                $.each(udcJson, function (keyName, value)
                {
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[keyName]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                });
            },
            error: function ()
            {
            }
        });
    }

    function addTreeviewSelectionList()
    {
        addTags(udcJson, 'addUDCTags', 'udc');
    }
    var selectedFilePath, currentPageNo, totalPages, tempTiffFile;
    function renderTiff(tiffFile, no, numPages) {
        selectedFilePath = "${pageContext.request.contextPath}/search/preview/all/renderTiff/" + recordIdentifier + "/" + no;
        tempTiffFile = tiffFile;
        currentPageNo = no;
        totalPages = numPages;
        $(function () {
            $('#viewer').show();
            console.log(numPages + " in tif" + tiffFile + " " + no);
            var iv1 = $("#viewer").iviewer({
                src: selectedFilePath
            });
            console.log("path.. " + selectedFilePath);
            iv1.iviewer('loadImage', selectedFilePath);
        });
    }
    function getRecordContent() {
        $.ajax({
            url: '${context}/cs/getRecordContent',
            data: 'recordIdentifier=' + recordIdentifier,
            type: 'POST',
            success: function (response)
            {
                document.getElementById('accordion').innerHTML = response;
                if (contentType === '<%=Constants.ContentType.TIFF%>') {
                    $(function () {
                        $.ajax(
                                {
                                    type: "POST",
                                    url: "${pageContext.request.contextPath}/search/preview/all/getTiffImagesCount/" + recordIdentifier,
                                    success: function (response) {
                                        renderTiff(recordIdentifier, 1, response);
                                    },
                                    error: function (e) {
                                        alert('Error while retreiving number of pages in a tiff file: ' + e + '<br/>Tiff File: ' + recordIdentifier);
                                    }
                                });
                    });
                    /*Preview of previous page*/
                    $("#prev").click(function () {
                        if (currentPageNo <= 1) {
                            alert('Previous page is not available.');
                        } else {
                            renderTiff(tempTiffFile, (currentPageNo - 1), totalPages);
                        }
                    });
                    /*Preview of next page*/
                    $("#next").click(function () {
                        if (currentPageNo >= (totalPages - 1)) {
                            alert('Next page is not available.');
                        } else {
                            renderTiff(tempTiffFile, (currentPageNo + 1), totalPages);
                        }
                    });
                    $("#viewer").on('mousewheel', function (e) {
                        //console.log("Wheel");
                        e.preventDefault();
                    });
                    /*
                     *Zoom in functionality
                     */
                    $("#in").click(function () {

                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('zoom_by', 1);
                    });
                    /*
                     *Zoom out functionality
                     */
                    $("#out").click(function () {
                        console.log("in click");
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('zoom_by', -1);
                    });
                    /*
                     *fit to screen functionality
                     */
                    $("#fit").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('fit');
                    });

                    /*
                     *original view functionality
                     */
                    $("#orig").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('set_zoom', 100);
                    });

                    /*
                     *Rotate Left functionality
                     */
                    $("#rotateleft").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath

                        });
                        iv1.iviewer('angle', -90);
                    });

                    /*
                     *Rotate Right functionality
                     */
                    $("#rotateright").click(function () {
                        $('#viewer').show();
                        var iv1 = $("#viewer").iviewer({
                            src: selectedFilePath
                        });
                        iv1.iviewer('angle', 90);
                    });
                } else {
                    function getWindowHeight() {
                        var myHeight = 0;
                        if (typeof (window.innerWidth) == 'number') {
                            //Non-IE
                            myHeight = window.innerHeight;
                        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                            //IE 6+ in 'standards compliant mode'
                            myHeight = document.documentElement.clientHeight;
                        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                            //IE 4 compatible
                            myHeight = document.body.clientHeight;
                        }
                        return myHeight;
                    }
                    var recordPathList = '${recordPathList}';
                    recordPathList = recordPathList.replace("[", "").replace("]", "").split(",");
                    for (var i = 0; i < recordPathList.length; i++) {
                        imgPathArr.push(recordPathList[i]);
                    }

                    imgCounter = imgPathArr.length;
                    if (imgCounter % 2 != 0) {
                        imgPathArr.push(recordPathList[0]);
                        imgCounter = imgCounter + 1;
                    }
                    imagePathJsonObject["play-big"] = "${context}/themes/images/BookReader/play-big.jpg";
                    imagePathJsonObject["pause-big"] = "${context}/themes/images/BookReader/pause-big.jpg";
                    imagePathJsonObject["closedhand"] = "${context}/themes/images/BookReader/closedhand.cur";
                    imagePathJsonObject["openhand"] = "${context}/themes/images/BookReader/openhand.cur";
                    $('#fb-pg').val("1");
                    $("#totalCnt").text("/ " + imgCounter);
                    load_Initial_Pages();
                }
            },
            error: function ()
            {
                alert('Error in showing the content');
            }
        });
    }
</script>
<!--place of div changed for scroll in popup start-->
<div id="light" class="white_content">
    <div id="close_btn" style="background:url(${context}/themes/images/BookReader/noti_close.png)"></div>
    <!--next and previous zoom start-->
    <div id="next_btn" style="background:url(${context}/themes/images/iviewer/nextPopup.png)"></div>
    <div id="prev_btn" style="background:url(${context}/themes/images/iviewer/prevPopup.png)"></div>
    <!--next and previous zoom-->
</div>
<div id="fade" class="black_overlay"></div>
<!--place of div changed for scroll in popup end-->
<!-- Horizontal breadcrumbs -->
<ol class="breadcrumb">
    <li><a href="${context}/home"><i class="fa fa-home" aria-hidden="true"> </i></a></li>
    <security:authorize access="hasAnyRole('LIB_USER')">
        <li><a href="${context}/cs/udc-tagged-record-register/1/15"><tags:message code="sb.udc.tagged.records"/></a></li>
    </security:authorize>
</ol>
<div style="margin-bottom:0px;" class="clearfix"></div>
<!-- search result content starts here -->
<div class="container-fluid" id="main">
    <div style="margin-bottom:20px;" class="clearfix"></div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!--Breadcrumb-->
        <!--show hide panel-->
        <div id="accordion"></div>
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div >
            <div id="viewMetadata">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="card panel-default clearfix">
                        <div class="card-header"><tags:message code="label.Recordmetadata"/></div>
                        <div>
                            <c:set var="underScore" value="_"/>
                            <div class="tab-content">
                                <div class="tab-pane nopad active" >
                                    <div class="overflow685">
                                        <div class="m-a-3">
                                            <c:choose>
                                                <c:when test="${metadata.metadataType eq dcType}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-2 col-md-4 col-sm-12 font-weight-bold " ><tags:message code="label.TagName"/></label>
                                                        <label class="form-control-label col-lg-10 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="dcMetadata" value="${metadata.dcMetadata}" />
                                                    <c:forEach var="field" items="${dcMetadata['class'].declaredFields}">
                                                        <c:if test="${field.name ne 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="metadata">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${field.name}" style="text-transform: capitalize">${field.name}</label>
                                                                    <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${metadata}</span> </div>
                                                                </div>
                                                            </c:forEach>
                                                            <c:forEach items="${metadata.dcMetadata.others}" var="childMetadata">
                                                                <c:if test="${childMetadata.parentName eq field.name}">
                                                                    <c:forEach items="${childMetadata.tagValues}" var="otherMetadata">
                                                                        <div class="form-group row clearfix">
                                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12 " for="${childMetadata.tagName}" style="text-transform: capitalize">${childMetadata.tagName}</label>
                                                                            <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                        </div>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${field.name eq 'others'}">
                                                            <c:forEach items="${dcMetadata[field.name]}" var="other">
                                                                <c:if test="${other.parentName eq 'NA'}">
                                                                    <c:forEach items="${other.tagValues}" var="otherMetadata">
                                                                        <div class="form-group row clearfix">
                                                                            <label class="form-control-label col-lg-2 col-md-4 col-sm-12" for="${other.tagName}" style="text-transform: capitalize">${other.tagName}</label>
                                                                            <div class="col-lg-10 col-md-7 col-sm-12"> <span class="fc-text">${otherMetadata}</span> </div>
                                                                        </div>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:when>
                                                <c:when test="${metadata.metadataType eq marc21Type}">
                                                    <div class="form-group row clearfix">
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Tag"/></label>
                                                        <label class="form-control-label col-lg-1 col-md-1 col-sm-6 font-weight-bold" ><tags:message code="label.Subfield"/></label>
                                                        <label class="form-control-label col-lg-4 col-md-4 col-sm-12 font-weight-bold" ><tags:message code="label.description"/></label>
                                                        <label class="form-control-label col-lg-6 col-md-6 col-sm-12 font-weight-bold" ><tags:message code="label.Value"/></label>
                                                    </div>
                                                    <c:set var="collectionType" value="${metadata.collectionType}" />
                                                    <c:forEach items="${collectionType.record}" var="record">
                                                        <c:forEach items="${record.controlfield}" var="controlfield">
                                                            <div class="form-group row clearfix">
                                                                <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${controlfield.tag}" style="text-transform: capitalize">${controlfield.tag}</label>
                                                                <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " >---</label>
                                                                <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(controlfield.tag)}</label>
                                                                <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${controlfield.value}</span> </div>
                                                            </div>
                                                        </c:forEach>
                                                        <c:forEach items="${record.datafield}" var="datafield">
                                                            <c:forEach items="${datafield.subfield}" var="subfield">
                                                                <div class="form-group row clearfix">
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${datafield.tag}" style="text-transform: capitalize">${datafield.tag}</label>
                                                                    <label class="form-control-label col-lg-1 col-md-1 col-sm-6 " for="${subfield.code}">${subfield.code}</label>
                                                                    <c:set var="key" value="${datafield.tag}${underScore}${subfield.code}"/>
                                                                    <label class="form-control-label col-lg-4 col-md-4 col-sm-12 " >${marc21TitleMap.get(key)}</label>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12"> <span class="fc-text">${subfield.value}</span> </div>
                                                                </div>
                                                            </c:forEach>
                                                        </c:forEach>
                                                    </c:forEach>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6" >
                    <div class="card panel-default">
                        <div class="card-header"> <tags:message code="label.Addudc.tags"/></div>
                        <div class="card-block clearfix">
                            <div class="form-group row">
                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left" ><tags:message code="label.select.language"/></label>
                                <div class="col-lg-10 col-md-7 col-sm-12">
                                    <select class="form-control" id="selectLanguage" onchange="selectLanguage('selectLanguage');">
                                        <c:forEach items="${languageList}" var="language">
                                            <c:if test="${language.id eq 40}">
                                                <option value="${language.id}" selected="true">${language.languageName}</option>
                                            </c:if>
                                            <c:if test="${language.id ne 40}">
                                                <option value="${language.id}">${language.languageName}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="form-control-label col-lg-2 col-md-4 col-sm-12 text-right text-sm-left"></label>
                                <div class="col-lg-10 col-md-7 col-sm-12">
                                    <div class="input-group">
                                        <input type="text" placeholder="Enter UDC Tags..." name="udcTags" id="udcAutocomplete" class="form-control"/>
                                        <span class="input-group-btn">
                                            <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#udcTreeViewModel"><tags:message code="button.TreeView.Selection"/></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom:20px;" class="clearfix"></div>
                    <div class="card panel-default">
                        <div class="card-header"><tags:message code="label.UDCTags"/></div>
                        <div class="card-block">
                            <form class="form-horizontal">
                                <div class="form-group" id="existingUDCTags"></div>
                                <div class="form-group" id="addUDCTags"></div>
                            </form>
                        </div>
                        <div class="card-footer clearfix" >
                            <div class="col-xs-offset-2 col-lg-10 col-sm-12" id="udcApproveButton" align="center">
                                <input type="button" class="btn btn-primary" onclick="approveTags()" value="Approve" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- s result content container -->
<div style="margin-bottom:20px;" class="clearfix"></div>
<div class="modal fade" id="udcTreeViewModel" role="dialog">
    <div class="modal-dialog modal-xlg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.UDCTags"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 form-group">
                        <h3><tags:message code="label.select.language"/></h3>
                        <select class="form-control" id="selectLanguageModel" onchange="selectLanguage('selectLanguageModel');">
                            <c:forEach items="${languageList}" var="language">
                                <c:if test="${language.id eq 40}">
                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                </c:if>
                                <c:if test="${language.id ne 40}">
                                    <option value="${language.id}">${language.languageName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                </div><br>
                <div id="udcTreeviewCheckable" class=""></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addTreeviewSelectionList();" data-dismiss="modal"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<form:form id="udcTagForm" method="post" action="${context}/cs/approveUDCTags" commandName="crowdSourceUDCTagBean">
    <form:input type="hidden" path="crowdSourceUDCTagId" id="crowdSourceUDCTagId"/>
    <form:input type="hidden" path="crowdSourceTags" id="crowdSourceTags"/>
    <form:input type="hidden" path="isEdit" id="isEdit"/>
</form:form>
