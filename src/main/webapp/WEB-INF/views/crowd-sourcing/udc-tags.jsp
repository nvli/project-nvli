<%--
    Document   : udc-tags
    Created on : Jun 24, 2016, 11:46:04 AM
    Author     : Suman Behara <sumanb@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<!-- search result content starts here -->
<div class="m-l-3 m-r-3 m-t-3" id="main">
    <div class="row udc_tag_drarchrecord">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="clearfix"></div>
                <div class="card panel-default">
                        <div class="card-header clearfix"><tags:message code="Select.UDC.tags.Representation"/>
                            <span class="m-l-1 d_i_block">
                                <select id="udctagDispay" class="udctagDispay">
                                    <option><tags:message code="label.udc.catalogue"/></option>
                                    <option><tags:message code="label.udc.visualization"/> </option>
                                </select>
                            </span>
                            <span class="pull-right font14" id="treeViewExpandDiv">
                                <a href="#" class="label label-default" id="btn-collapse-all"> <tags:message code="profile.setting.Specialization.collapse.all"/> </a>
                                <a href="#" class="label label-default m-l-1" id="btn-expand-all"> <tags:message code="profile.setting.Specialization.expand.all"/> </a>
                            </span>
                        </div>
                        <div class="index_bg p-a-4 clearfix" id="treeviewheaderDiv">
                            <div class="pull-left font14" id="languageDiv">
                                <div role="group" aria-label="Basic example">
                                    <select class="form-control" id="selectLanguage" onchange="selectLanguage();">
                                        <c:forEach items="${languageList}" var="language">
                                            <c:if test="${language.id eq udcLanguageId}">
                                                <option value="${language.id}" selected="true">${language.languageName}</option>
                                            </c:if>
                                            <c:if test="${language.id ne udcLanguageId}">
                                                <option value="${language.id}">${language.languageName}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select
                                </div>
                            </div>
                        </div>
                        <div class="pull-right font14" id="udcParentIdDiv">
                            <div class="btn-group marginauto" role="group" aria-label="Basic example">
                                <c:forEach items="${udcParentMap}" var="udcParent" varStatus="udcCount">
                                    <c:if test="${udcCount.index eq 0}">
                                        <button type="button" id="${udcParent.key}" class="btn btn-secondary treeview-pagination-btn active" data-parent-id="${udcParent.key}" data-parent-value="${udcParent.value}">${udcParent.value}</button>
                                    </c:if>
                                    <c:if test="${udcCount.index ne 0}">
                                        <button type="button" id="${udcParent.key}" class="btn btn-secondary treeview-pagination-btn" data-parent-id="${udcParent.key}" data-parent-value="${udcParent.value}">${udcParent.value}</button>
                                    </c:if>
                                </c:forEach>
                                <input type="hidden" id="udcnotationId" name="udcnotationId" />
                                <input type="hidden" id="graphicalUDCUnotation" name="graphicalUDCUnotationId" />
                                <input type="hidden" id="defaultParentId" value="${defaultParentId}" />
                                <input type="hidden" id="defaultParentValue" value="${defaultParentValue}" />
                            </div>
                        </div>
                    </div>
                    <!--  Treeview content goes here-->
                    <div class="clearfix">
                        <div class="clear"></div>
                        <div id="udc-scroll-container">
                            <div id="udc-treeview-container" class="treeview">
                            </div>
                        </div>
                    </div>
                <!--  Treeview content ends here-->
            </div>
          </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card bg-greye9">
                <div id="udcSearchResultDiv"></div>
            </div>
        </div>
</div>
</div>
<div class="clear22"></div>
<script type="text/javascript">
    // -----------------------------------------------------------------------------
// Globals
// Major version of Flash required
    var requiredMajorVersion = 8;
// Minor version of Flash required
    var requiredMinorVersion = 0;
// Revision of Flash required
    var requiredRevision = 0;
// the version of javascript supported
    var jsVersion = 1.0;
// -----------------------------------------------------------------------------
    var _parentId;
    var _parentValue;
    var _defaultParentValueFlag = false;
    $(document).ready(function () {
        _parentId = $(".treeview-pagination-btn.active").attr("data-parent-id");

        $('#recordresultDiv').hide();
        $('#udcTotalResultDiv').hide();
        $('#resourceTypelimitDiv').hide();


        _parentValue = $(".treeview-pagination-btn.active").attr("data-parent-value");

        $(".treeview-pagination-btn").unbind().bind("click", function (e) {
            _parentId = e.currentTarget.getAttribute("data-parent-id");
            _parentValue = e.currentTarget.getAttribute("data-parent-value");

            $(".treeview-pagination-btn.active").removeClass("active");
            $(e.currentTarget).addClass("active");

            getUDCTreeviewData(_parentId);
            document.getElementById("defaultParentId").value = _parentId;
        });

//        $('#treeViewExpandDiv').hide();
//        $('#treeviewheaderDiv').hide();
//        window.history.replaceState('object or string', 'SWFtitle', '${context}/swf/RelationBrowser.swf');
//        getUDCGraphicalView();
        window.history.replaceState('object or string', 'Title1', '${context}/search/udc-tags-searchRecord/' + _parentId + '/' + document.getElementById("selectLanguage").value);
        $('#treeViewExpandDiv').show();
        $('#treeviewheaderDiv').show();
        selectLanguage();
        callRecordResult(document.getElementById("defaultParentId").value, 'All', '1', '25');
    });
    
    function applySlimScroll(id) {                                           
        $("#" + id).slimScroll({
            height: '550px',
            size: '5px',
            position: 'right',
            color: '#888',
            alwaysVisible: false,
            distance: '0px',
            railVisible: false,
            railColor: '#efefef',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: false
        });
    }
    
    function getUDCTreeviewData(parentId)
    {
        var languageId = document.getElementById("selectLanguage").value;
        $(".treeview-pagination-btn.active").removeClass("active");
        $("#" + parentId).addClass("active");
        $("#udcnotationId").val(_parentValue);
        callRecordResult($("#" + parentId).attr("data-parent-value"), 'All', '1', '25');
        $.ajax({
            url: '${context}/search/getUDCTreeviewDataWithPagination',
            data: {parentId: parentId, languageId: languageId},
            type: 'POST',
            dataType: "text",
            success: function (response)
            {
                $('#udc-treeview-container').treeview({
                    data: response,
                    levels: 1,
                    onNodeSelected: function (event, node) {
                        var udcnotation = (node.text.substring(1)).split("]")[0];
                        $("#udcnotationId").val(udcnotation);
                        $('#recordresultDiv').empty();
                        $('#searchResultCountId').hide();
                        $('#cardPanelId').hide();
                        callRecordResult(udcnotation, 'All', '1', '25');
                        $('#recordresultDiv').hide();
                    }
                });
                applySlimScroll("udc-scroll-container");
                if (!_defaultParentValueFlag) {
                    var notationValue = "[" + document.getElementById("defaultParentValue").value + "]";
                    var nodeArray = $('#udc-treeview-container').treeview('search', [notationValue, {
                            exactMatch: false
                        }]);
                    $('#udc-treeview-container').treeview('clearSearch');
                    if (nodeArray[0] !== undefined)
                    {
                        $('#udc-treeview-container').treeview('expandNode', [nodeArray[0].nodeId, {silent: true, levels: 1}]);
                    }
                    _defaultParentValueFlag = true;
                } else {
                    var notationValue = "[" + _parentValue + "]";
                    var nodeArray = $('#udc-treeview-container').treeview('search', [notationValue, {
                            exactMatch: false
                        }]);
                    $('#udc-treeview-container').treeview('clearSearch');
                    if (nodeArray[0] !== undefined)
                    {
                        $('#udc-treeview-container').treeview('expandNode', [nodeArray[0].nodeId, {silent: true, levels: 1}]);
                    }
                }

                $('#btn-expand-all').on('click', function (e) {
                    $('#udc-treeview-container').treeview('expandAll');
                });

                $('#btn-collapse-all').on('click', function (e) {
                    $('#udc-treeview-container').treeview('collapseAll');
                });
                window.history.replaceState('object or string', 'Title', '${context}/search/udc-tags-searchRecord/' + parentId + '/' + languageId);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
        });
    }
    function selectLanguage()
    {
        getUDCTreeviewData(document.getElementById("defaultParentId").value);
    }

    function callRecordResult(udcnotation, resource, pageNum, pageWin) {
        var hPageWin = pageWin;
        var hResource = resource;
        $.ajax({
            type: 'GET',
            url: '${context}/search/across/all/udctagRecordSearch',
            data: {udcnotation: udcnotation, pageNum: pageNum, pageWin: pageWin},
            cache: false,
            success: function (data) {
                $("#udcSearchResultDiv").html(data);
                $("#limit").val(hPageWin);
                $("#resourceTypelimit").val(hResource);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    }
    function callSpecificResourceRecordResult(udcnotation, resource, pageNum, pageWin) {
        var hPageWin = pageWin;
        var hResource = resource;
        var selectBox = document.getElementById("resourceTypelimit");
        var resourceList = new Array();
        for (var i = 0; i < selectBox.length; i++) {
            resourceList.push(selectBox[i].value);
        }
        $.ajax({
            type: 'GET',
            url: '${context}/search/specificresource/udctagRecordSearch',
            data: {udcnotation: udcnotation, resource: resource, pageNum: pageNum, pageWin: pageWin},
            cache: false,
            success: function (data) {
                
                $("#udcSearchResultDiv").html(data);
                $("#limit").val(hPageWin);
                $('#resourceTypelimit').empty();
                $.each(resourceList, function (key, value)
                {
                    $('#resourceTypelimit').append('<option>' + value + '</option>');
                });
                $("#resourceTypelimit").val(hResource);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    }

    function getUDCGraphicalView() {
        var hasRightVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);
        if (hasRightVersion) {  // if we've detected an acceptable version
            var oeTags = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'
                    + 'width="100%" height="100%"'
                    + 'codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab">'
                    + '<param name="movie" value="${context}/swf/RelationBrowser.swf" /><param name="quality" value="high" /><param name="bgcolor" value="#ffffff" />'
                    + '<embed src="${context}/swf/RelationBrowser.swf" quality="high" bgcolor="#ffffff"'
                    + 'width="100%" height="800px" name="RelationBrowser" align="middle"'
                    + 'play="true"'
                    + 'loop="false"'
                    + 'quality="high"'
                    + 'allowScriptAccess="always"'
                    + 'type="application/x-shockwave-flash"'
                    + 'pluginspage="http://www.macromedia.com/go/getflashplayer">'
                    + '<\/embed>'
                    + '<\/object>';
            document.getElementById("udc-treeview-container").innerHTML = oeTags;
            //document.write(oeTags);   // embed the flash movie
        } else {  // flash is too old or we can't detect the plugin
            var alternateContent = 'Alternate HTML content should be placed here.'
                    + 'This content requires the Macromedia Flash Player.'
                    + '<a href=http://www.macromedia.com/go/getflash/>Get Flash</a>';
            // document.write(alternateContent);  // insert non-flash content
            document.getElementById("udc-treeview-container").innerHTML = alternateContent;
        }
        setTimeout(waitFunction, 3000);
    }

    function waitFunction() {
        window.history.replaceState(null, null, '${context}/search/udc-tags-searchRecord/' + _parentId + '/' + document.getElementById("selectLanguage").value);
    }
    function getDesc(value) {
        var udcnotation = value.match(/\[(.*)\]/);
        callRecordResult(udcnotation[1], 'All', '1', '25');
        $("#graphicalUDCUnotation").val(udcnotation[1]);
    }
    $("#udctagDispay").change(function () {
        var udcDisplayTag = document.getElementById("udctagDispay");
        var udcDisplayView = udcDisplayTag.options[udcDisplayTag.selectedIndex].value;
        if (udcDisplayView === 'UDC catalogue') {
            window.history.replaceState('object or string', 'Title1', '${context}/search/udc-tags-searchRecord/' + _parentId + '/' + document.getElementById("selectLanguage").value);
            $('#treeViewExpandDiv').show();
            $('#treeviewheaderDiv').show();
            selectLanguage();
        } else {
            $('#treeViewExpandDiv').hide();
            $('#treeviewheaderDiv').hide();
            window.history.replaceState('object or string', 'Title', '${context}/swf/RelationBrowser.swf');
            getUDCGraphicalView();
        }

    });
</script>
<script language="VBScript" type="text/vbscript">
    <!-- // Visual basic helper required to detect Flash Player ActiveX control version information
    Function VBGetSwfVer(i)
    on error resume next
    Dim swControl, swVersion
    swVersion = 0

    set swControl = CreateObject("ShockwaveFlash.ShockwaveFlash." + CStr(i))
    if (IsObject(swControl)) then
    swVersion = swControl.GetVariable("$version")
    end if
    VBGetSwfVer = swVersion
    End Function
    // -->
</script>
<script language="JavaScript1.1" type="text/javascript">
<!-- // Detect Client Browser type
    var isIE = (navigator.appVersion.indexOf("MSIE") !== -1) ? true : false;
    var isWin = (navigator.appVersion.toLowerCase().indexOf("win") !== -1) ? true : false;
    var isOpera = (navigator.userAgent.indexOf("Opera") !== -1) ? true : false;
    jsVersion = 1.1;
// JavaScript helper required to detect Flash Player PlugIn version information
    function JSGetSwfVer(i) {
        // NS/Opera version >= 3 check for Flash plugin in plugin array
        if (navigator.plugins !== null && navigator.plugins.length > 0) {
            if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
                var swVer2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
                var flashDescription = navigator.plugins["Shockwave Flash" + swVer2].description;
                descArray = flashDescription.split(" ");
                tempArrayMajor = descArray[2].split(".");
                versionMajor = tempArrayMajor[0];
                versionMinor = tempArrayMajor[1];
                if (descArray[3] !== "") {
                    tempArrayMinor = descArray[3].split("r");
                } else {
                    tempArrayMinor = descArray[4].split("r");
                }
                versionRevision = tempArrayMinor[1] > 0 ? tempArrayMinor[1] : 0;
                flashVer = versionMajor + "." + versionMinor + "." + versionRevision;
            } else {
                flashVer = -1;
            }
        }
        // MSN/WebTV 2.6 supports Flash 4
        else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") !== -1)
            flashVer = 4;
        // WebTV 2.5 supports Flash 3
        else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") !== -1)
            flashVer = 3;
        // older WebTV supports Flash 2
        else if (navigator.userAgent.toLowerCase().indexOf("webtv") !== -1)
            flashVer = 2;
        // Can't detect in all other cases
        else {

            flashVer = -1;
        }
        return flashVer;
    }
// If called with no parameters this function returns a floating point value
// which should be the version of the Flash Player or 0.0
// ex: Flash Player 7r14 returns 7.14
// If called with reqMajorVer, reqMinorVer, reqRevision returns true if that version or greater is available
    function DetectFlashVer(reqMajorVer, reqMinorVer, reqRevision)
    {
        reqVer = parseFloat(reqMajorVer + "." + reqRevision);
        // loop backwards through the versions until we find the newest version
        for (i = 25; i > 0; i--) {
            if (isIE && isWin && !isOpera) {
                versionStr = VBGetSwfVer(i);
            } else {
                versionStr = JSGetSwfVer(i);
            }
            if (versionStr === -1) {
                return false;
            } else if (versionStr !== 0) {
                if (isIE && isWin && !isOpera) {
                    tempArray = versionStr.split(" ");
                    tempString = tempArray[1];
                    versionArray = tempString.split(",");
                } else {
                    versionArray = versionStr.split(".");
                }
                versionMajor = versionArray[0];
                versionMinor = versionArray[1];
                versionRevision = versionArray[2];

                versionString = versionMajor + "." + versionRevision;   // 7.0r24 == 7.24
                versionNum = parseFloat(versionString);
                // is the major.revision >= requested major.revision AND the minor version >= requested minor
                if ((versionMajor > reqMajorVer) && (versionNum >= reqVer)) {
                    return true;
                } else {
                    return ((versionNum >= reqVer && versionMinor >= reqMinorVer) ? true : false);
                }
            }
        }
        return (reqVer ? false : 0.0);
    }
// -->
</script>
