<%--
    Document   : user-articles-register
    Created on : 9 Feb, 2017, 11:59:03 AM
    Author     : Vivek Bugale<bvivek@cdac.in>
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script src="${context}/components/jquery/jquery-dateFormat.min.js"></script>
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header cardHeaderInputMargin">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 col-xl-8">
                    <strong class="header-title"><tags:message code="dashboard.article.heading"/></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="${context}/cs/create-article" class="btn btn-success"><tags:message code="label.article.create.new"/></a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-xl-2">
                    <input id="searchString" class="form-control form-control-sm" placeholder="Search title">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 col-xl-1">
                    <select id="statusTypeFilter" class="form-control form-control-sm">
                        <option value="all" selected><tags:message code="label.all"/></option>
                        <option value="draft"><tags:message code="label.Draft"/></option>
                        <option value="pending"><tags:message code="label.Pending"/></option>
                        <option value="published"><tags:message code="label.option.Published"/></option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 col-xl-1">
                    <select id="limitFilter" class="form-control form-control-sm">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"><tags:message code="label.serial.no"/></th>
                        <th width="35%"><tags:message code="label.article.title"/></th>
                        <th width="15%"><tags:message code="label.lastModifiedDate"/></th>
                        <th width="15%"><tags:message code="label.publishDate"/></th>
                        <th width="15%"><tags:message code="label.status"/></th>
                        <th style="text-align:center"><tags:message code="label.action"/></th>
                    </tr>
                </thead>
                <tbody id="article-tbl-body"></tbody>
            </table>
        </div>
        <div id="article-tbl-footer" class="card-footer clearfix"></div>
    </div>
</div>
<script type="text/template" id="tpl-article-tbl-row">
    {{ _.each(articleList, function (article,i) { }}
        <tr>
            <td>{{=i+1+recordCount}}</td>
            <td>{{=article.articleTitle}}</td>
            <td>{{=$.format.date(new Date(article.lastModifiedDate), "dd MMM yyyy hh:mm:ss")}}</td>
            <td>
                {{ if(article.isPublished) { }}
                    {{=$.format.date(new Date(article.publishDate), "dd MMM yyyy hh:mm:ss")}}
                {{ }else{ }}
                    NA
                {{ } }}
            </td>
            <td>
                {{ if(article.isPublished && article.isApproved) { }}
                    Published
                {{ }else if(article.isPublished && !article.isApproved){ }}
                    Pending for approval
                {{ }else{ }}
                    Draft
                {{ } }}
            </td>
            <td class="text-right" style="text-align:center">
                <div class="btn-group" title="Preview Article">
                    <a class="btn-secondary btn" href="${context}/cs/preview-article/{{=article.id}}"><i class="fa fa-eye"></i></a>
                </div>
                <div class="btn-group" title="Edit Article">
                    <a class="btn-secondary btn" href="${context}/cs/edit-article/{{=article.id}}"><i class="fa fa-pencil"></i></a>
                </div>
            </td>
        </tr>
    {{ }); }}
</script>
<script type="text/javascript">
    $(document).ready(function () {
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });

        $("#statusTypeFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });

        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });

    function fetchAndRenderTableData(pageNo) {
        $.ajax({
            url: '${context}/cs/getArticlesByFiltersWithLimit',
            data: {searchString: $('#searchString').val(), statusTypeFilter: $('#statusTypeFilter').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo, isRequiredUserRestriction: true},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.articleList)) {
                        $("#article-tbl-body").html('<tr><td colspan="6" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-article-tbl-row").html());

                        var Opts = {
                            "articleList": jsonObject.articleList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };

                        var articleTableBody = articlesTpl(Opts);
                        $("#article-tbl-body").empty();
                        $("#article-tbl-body").html(articleTableBody);
                        renderPagination(fetchAndRenderTableData, "article-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }

    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>
