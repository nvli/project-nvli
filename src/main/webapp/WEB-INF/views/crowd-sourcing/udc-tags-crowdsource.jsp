<%--
    Document   : udc-tags-crowdsource
    Created on : Jun 23, 2016, 12:04:26 PM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style type="text/css" id="udc-treeview-container-style">
    .treeview .list-group-item{font-size: 15px;cursor:pointer}
    .treeview span.indent{margin-left:10px;margin-right:10px}
    .treeview span.icon{width:12px;margin-right:5px}
    .treeview .node-disabled{color:silver;cursor:not-allowed}
    .node-udc-treeview-container{}
    .node-udc-treeview-container:not(.node-disabled):hover{background-color:#F5F5F5;}
    table > tbody {display: inline-table;width: 100%;}    
</style>
<div class="m-l-3 m-r-3 m-t-3 udc_tag_crodsource" id="main" style="z-index: 1;">
    <div class="row">
        <div id="alert-box"></div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card panel-default">
                <ul class="nav nav-tabs">
                    <li class="nav-item"> <a class="nav-link udc-tag-tab active" data-target="#udcTagsTab" data-toggle="tab"><tags:message code="label.UDCTags"/> </a> </li>
                    <li class="nav-item"> <a class="nav-link udc-tag-tab" data-target="#newlyTranslatedTagTab" data-toggle="tab"><tags:message code="label.Newly.Translated.Tags"/></a> </li>
                    <li class="nav-item croud_tab_trans"> <a class="nav-link udc-tag-tab" data-target="#viewUntranslatedTab" data-toggle="tab"><tags:message code="label.View.Untranslated"/></a> </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="udcTagsTab">
                        <div class="index_bg p-a-4 clearfix">
                            <div class="pull-right font14 croud_lang">
                                <div role="group" aria-label="languages">
                                    <select class="form-control" id="translation-udc-language" onchange="changeTranslationUDCLanguage();">
                                        <c:forEach items="${languageList}" var="language">
                                            <c:if test="${language.id eq 40}">
                                                <option value="${language.id}" selected>${language.languageName}</option>
                                            </c:if>
                                            <c:if test="${language.id ne 40}">
                                                <option value="${language.id}">${language.languageName}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="btn-group marginauto pull-left udc-pagination-btn" role="group" aria-label="Basic example">
                                <c:forEach items="${udcParentMap}" var="udcParent" varStatus="udcCount">
                                    <c:if test="${udcCount.index eq 0}">
                                        <button type="button" id="treeviewPagination-${udcParent.key}" class="btn btn-secondary treeview-pagination-btn active" data-parent-id="${udcParent.key}" data-parent-value="">${udcParent.value}</button>
                                    </c:if>
                                    <c:if test="${udcCount.index ne 0}">
                                        <button type="button" id="treeviewPagination-${udcParent.key}" class="btn btn-secondary treeview-pagination-btn" data-parent-id="${udcParent.key}" data-parent-value="${udcParent.value}">${udcParent.value}</button>
                                    </c:if>
                                </c:forEach>              
                            </div>
                            <!--collapse and expand all buttons-->
                            <!--<span class="udc_crowd_link">-->
                            <!--<a href="#" class="label label-default" id="btn-collapse-all">-->
                            <%--<tags:message code="profile.setting.Specialization.collapse.all"/>--%>
                            <!--</a>-->
                            <!--<a href="#" class="label label-default m-l-1" id="btn-expand-all">-->
                            <%--<tags:message code="profile.setting.Specialization.expand.all"/>--%>
                            <!--</a>-->
                            <!--</span>-->
                            <!--Responsive view untranslated button-->
                            <span class="right">
                                <a href="#" class="label label-default udc-tag-tab" id="btn-view-untranslated-tag" data-target="#viewUntranslatedTab">
                                    <b><tags:message code="label.View.Untranslated"/></b>
                                </a>
                            </span>
                        </div>          
                        <div id="udc-container">
                            <div id="udc-treeview-container"></div>
                        </div>
                        <div id="udc-table-container" class="clearfix"></div>
                    </div>
                    <div class="tab-pane" id="newlyTranslatedTagTab">
                        <div id="newly-translated-container">
                            <div id="newly-translated-tag-container" class="clearfix"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="viewUntranslatedTab">  
                        <div id="untranslated-container">
                            <div id="view-untranslated-tag-container" class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card panel-default">
                <div class="card-header"><tags:message code="label.Translation.Panel"/></div>
                <div class="card-blfock">
                    <div id="udc-crowdsource-container"> 
                        <div class="m-b-1">
                            <table class="table table-md tr_hand table-responsive">
                                <tr>
                                    <td><center><tags:message code="label.Select.UDC.notation"/></center></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src='${context}/themes/js/language/pramukhindic.js'></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>
<script type="text/javascript">
    var _treeviewUDCLanguageId = "40";
    var _translationUDCLanguageId;
    var _parentId;
    var _parentValue;
    var _untranslatedTagCheckbox = false;
    var _selectedUDCConceptId = "";
    var _translationList;
    var _isTreeviewTabSelected = true;
    var _udcConceptId = '${udcConceptId}';

    $(document).ready(function () {
        _parentId = $(".treeview-pagination-btn.active").attr("data-parent-id");
        _parentValue = $(".treeview-pagination-btn.active").attr("data-parent-value");
        _translationUDCLanguageId = document.getElementById("translation-udc-language").value;
        $(".udc-tag-tab").unbind().bind("click", function (e) {
            $(".udc-tag-tab").removeClass("active");
            $(this).addClass("active");
            if (e.currentTarget.getAttribute("data-target") === '#udcTagsTab') {
                $(".udc-pagination-btn").show();
                $("#udcTagsTab").show();
                $("#newlyTranslatedTagTab").hide();
                $("#viewUntranslatedTab").hide();
                _isTreeviewTabSelected = true;
                getUDCTreeviewData();
                _selectedUDCConceptId = "";
                $('#udc-crowdsource-container').empty();
                $('#udc-crowdsource-container').append('<div><table class="table table-md tr_hand table-responsive"><tr><td><center>Select UDC notation for translation from left side</center></td></tr></table></div>');
            } else if (e.currentTarget.getAttribute("data-target") === '#newlyTranslatedTagTab') {
                $(".udc-pagination-btn").hide();
                $("#udcTagsTab").hide();
                $("#newlyTranslatedTagTab").show();
                $("#viewUntranslatedTab").hide();
                _isTreeviewTabSelected = false;
                getNewlyTranslatedTags();
                _selectedUDCConceptId = "";
                $('#udc-crowdsource-container').empty();
                $('#udc-crowdsource-container').append('<div><table class="table table-md tr_hand table-responsive"><tr><td><center>Select UDC notation for translation from left side</center></td></tr></table></div>');
            } else if (e.currentTarget.getAttribute("data-target") === "#viewUntranslatedTab") {
                $(".udc-pagination-btn").hide();
                $("#udcTagsTab").hide();
                $("#newlyTranslatedTagTab").hide();
                $("#viewUntranslatedTab").show();
    //                                            collapse and expand all buttons
    //                                            $("#btn-collapse-all").hide();
    //                                            $("#btn-expand-all").hide();
                getUDCTableData();
                $('#udc-crowdsource-container').empty();
                $('#udc-crowdsource-container').append('<div><table class="table table-md tr_hand table-responsive"><tr><td><center>Select UDC notation for translation from left side</center></td></tr></table></div>');
            }
        });

        $(".treeview-pagination-btn").unbind().bind("click", function (e) {
            _parentId = e.currentTarget.getAttribute("data-parent-id");
            _parentValue = e.currentTarget.getAttribute("data-parent-value");
            $(".treeview-pagination-btn.active").removeClass("active");
            $(e.currentTarget).addClass("active");
            $(".newly-translated-pagination-btn.active").removeClass("active");
            $("#newlyTranslatedPagination-" + _parentId).addClass("active");
            getUDCTreeviewData();
        });

//                                    $(".newly-translated-pagination-btn").unbind().bind("click", function (e) {
//                                        _parentId = e.currentTarget.getAttribute("data-parent-id");
//                                        _parentValue = e.currentTarget.getAttribute("data-parent-value");
//                                        $(".newly-translated-pagination-btn.active").removeClass("active");
//                                        $(e.currentTarget).addClass("active");
//                                        $(".treeview-pagination-btn.active").removeClass("active");
//                                        $("#treeviewPagination-" + _parentId).addClass("active");
//                                        getNewlyTranslatedTags();
//                                    });

        setLanguage("translation-udc-language", "translation-text");
        if (_udcConceptId !== "") {
            window.history.replaceState('object or string', 'Title', '${context}/cs/udc-tags-crowdsource');
            getUDCTranslation(_udcConceptId);
        }
    });

    function applySlimScroll(id) {
        $("#" + id).slimScroll({
            height: '550px',
            size: '5px',
            position: 'right',
            color: '#888',
            alwaysVisible: false,
            distance: '0px',
            railVisible: false,
            railColor: '#efefef',
            railOpacity: 0.3,
            wheelStep: 10,
            allowPageScroll: false,
            disableFadeOut: false
        });
    }

    function changeTranslationUDCLanguage() {
        _translationUDCLanguageId = document.getElementById("translation-udc-language").value;
        if (_isTreeviewTabSelected) {
            getUDCTreeviewData();
            if (_selectedUDCConceptId !== "") {
                getUDCTranslation(_selectedUDCConceptId);
            }
        } else {
            getNewlyTranslatedTags();
        }
    }

    function getUDCTreeviewData() {
        $.ajax({
            url: '${context}/cs/getUDCTreeviewDataWithPagination',
            data: {parentId: _parentId, languageId: _treeviewUDCLanguageId, translationUDCLanguageId: _translationUDCLanguageId},
            type: 'POST',
            success: function (response) {
                $('#udc-table-container').empty();
                $('#udc-treeview-container').empty();
                $('#udc-treeview-container').treeview({
                    data: response,
                    onNodeSelected: function (event, node) {
                        getUDCTranslation(node.href);
                    }
                });
                if (_parentId === "2450") {
                    $('#udc-treeview-container').treeview('collapseAll');
                }
                applySlimScroll("udc-container");
    //                                            $('#btn-expand-all').on('click', function (e) {
    //                                                $('#udc-treeview-container').treeview('expandAll');
    //                                            });
    //
    //                                            $('#btn-collapse-all').on('click', function (e) {
    //                                                $('#udc-treeview-container').treeview('collapseAll');
    //                                            });
            },
            error: function () {
            }
        });
    }

    function getUDCTableData() {
        $.ajax({
            url: '${context}/cs/getUDCTableDataWithPagination',
            data: {parentId: _parentValue, languageId: _treeviewUDCLanguageId, translationUDCLanguageId: _translationUDCLanguageId},
            type: 'POST',
            success: function (response) {
                $('#udc-treeview-container').empty();
                $('#view-untranslated-tag-container').empty();
                $('#view-untranslated-tag-container').append('<ul style="margin-top: 0px;" class="list-group nobr">');
                var count = 0;
                var udc_data_template = _.template($("#tpl-udcData").html());
                var opts;
                var udc_data_el;
                _.each(response, function (udcConceptDescription) {
                    count++;
                    opts = {
                        "udcTagId": udcConceptDescription.id,
                        "udcTaxt": udcConceptDescription.description
                    };
                    udc_data_el = udc_data_template(opts);
                    $("#view-untranslated-tag-container").append(udc_data_el);
                });
                applySlimScroll("untranslated-container");
                if (count === 0) {
                    $('#view-untranslated-tag-container').append('<li style="color:#2418DE;background-color:#428bca;" data-nodeid="0" class="list-group-item node-udc-treeview-container"><center>Data Not Found</center></li>');
                }

                $('#view-untranslated-tag-container').append("</ul>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function getNewlyTranslatedTags() {
        $.ajax({
            url: '${context}/cs/getNewlyTranslatedTags',
            data: {parentId: _parentValue, languageId: _treeviewUDCLanguageId, translationUDCLanguageId: _translationUDCLanguageId},
            type: 'POST',
            success: function (response) {
                $('#newly-translated-tag-container').empty();
                $('#newly-translated-tag-container').append('<ul style="margin-top: 0px;" class="list-group nobr">');
                var count = 0;
                var udc_data_template = _.template($("#tpl-udcData").html());
                var opts;
                var udc_data_el;
                _.each(response, function (udcConceptDescription) {
                    count++;
                    opts = {
                        "udcTagId": udcConceptDescription.id,
                        "udcTaxt": udcConceptDescription.description
                    };
                    udc_data_el = udc_data_template(opts);
                    $("#newly-translated-tag-container").append(udc_data_el);
                });
                applySlimScroll("newly-translated-container");
                if (count === 0) {
                    $('#newly-translated-tag-container').append('<li style="color:#2418DE;background-color:#428bca;" data-nodeid="0" class="list-group-item node-udc-treeview-container"><center>Data Not Found</center></li>');
                }

                $('#newly-translated-tag-container').append("</ul>");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function getUDCTranslation(udcConceptId) {
        _selectedUDCConceptId = udcConceptId;
        $.ajax({
            url: '${context}/cs/getUDCTranslation',
            data: {udcConceptId: udcConceptId, translationUDCLanguageId: _translationUDCLanguageId},
            type: 'POST',
            success: function (udcTranslationBean) {
                $('#udc-crowdsource-container').empty();

                var otherTranslation_template = _.template($("#tpl-otherTranslation").html());
                var otherUsersTranslation_html = '';
                var opts;
                var otherTranslation_template_el;
                var isNotEmpty = false;
                var voteResultMap = udcTranslationBean.votingMap;

                _translationList = _.pluck(udcTranslationBean.otherUsersTranslation, 'description');

                _.each(udcTranslationBean.otherUsersTranslation, function (translation) {
                    isNotEmpty = true;
                    opts = {
                        "translation": translation.description,
                        "translationId": translation.id,
                        "userName": translation.user.firstName,
                        "isVoted": udcTranslationBean.isVoted,
                        "voteResult": voteResultMap[translation.id]
                    };
                    otherTranslation_template_el = otherTranslation_template(opts);
                    otherUsersTranslation_html = otherUsersTranslation_html + otherTranslation_template_el;
                });

                var translation_template = _.template($("#tpl-translation").html());
                opts = {
                    "selectedNotation": udcTranslationBean.selectedNotation,
                    "originalTranslation": udcTranslationBean.originalTranslation,
                    "otherUsersTranslation": otherUsersTranslation_html,
                    "ownTranslation": udcTranslationBean.ownTranslation,
                    "isVoted": udcTranslationBean.isVoted,
                    "isNotEmpty": isNotEmpty
                };
                var translation_template_el = translation_template(opts);

                $('#udc-crowdsource-container').append(translation_template_el);

                changeLanguage(get_related_lang_val(_translationUDCLanguageId));
                bindSaveTranslation(udcConceptId);
                bindTranslationCopy();
                bindTranslationVote(udcConceptId);
            },
            error: function (xhr) {
                console.log("error while fetching UDC Translation ::", xhr);
            }
        });
    }

    function bindSaveTranslation(udcConceptId) {
        $("#save-translation").click(function () {
            if (document.getElementById("translation-text").value === '') {
                alertBoxMessage("alert-box", "Enter translation text!", "alert alert-warning", alertAnimation);
            } else if (_.contains(_translationList, document.getElementById("translation-text").value)) {
                alertBoxMessage("alert-box", "Entered translation text alresdy present!", "alert alert-warning", alertAnimation);
            } else {
                $.ajax({
                    url: '${context}/cs/saveUDCTranslation',
                    data: {udcConceptId: udcConceptId, translation: document.getElementById("translation-text").value, translationUDCLanguageId: _translationUDCLanguageId},
                    type: 'POST',
                    success: function (response) {
                        if (response) {
                            getUDCTranslation(udcConceptId);
                            alertBoxMessage("alert-box", "Translation saved successfully!", "alert alert-success", alertAnimation);
                        } else {
                            alertBoxMessage("alert-box", "Service not available,try after some time!", "alert alert-danger", alertAnimation);
                        }
                    },
                    error: function (xhr) {
                        console.log("error while saving translation ::", xhr);
                    }
                });
            }
        });
    }

    function bindTranslationCopy() {
        $(".translation-copy-btn").unbind().bind("click", function (e) {
            document.getElementById("translation-text").value = e.currentTarget.getAttribute("data-translation-value");
        });
    }

    function bindTranslationVote(udcConceptId) {
        $(".vote-btn").unbind().bind("click", function (e) {
            $.ajax({
                url: '${context}/cs/voteUDCTranslation',
                data: {udcTagsCrowdsourceId: e.currentTarget.getAttribute("data-translation-id")},
                type: 'POST',
                success: function (response) {
                    if (response) {
                        getUDCTranslation(udcConceptId);
                        alert("You have successfully cast your vote.");
                    } else {
                        alert("service not available.try after some time");
                    }
                },
                error: function () {
                }
            });
        });
    }

    function alertBoxMessage(id, text, classname, callback) {
        $("#" + id).text(text);
        $("#" + id).removeClass();
        $("#" + id).addClass("col-sm-12 col-md-12 col-lg-12 ").addClass(classname);
        callback(id);
    }

    function alertAnimation(id) {
        $("#" + id)
                .first().hide().fadeIn(200)
                .delay(2000)
                .fadeOut(1000);
    }
</script>
<script type="text/template" id="tpl-udcData">
    <a href="#">
    <li style="color:undefined;background-color:undefined;" data-nodeid="0"
    class="list-group-item node-udc-treeview-container"
    onclick='getUDCTranslation("{{=udcTagId}}");'>
    {{=udcTaxt}}
    </li>
    </a>
</script>
<script type="text/template" id="tpl-translation">
    <div>
    <table class="table table-md tr_hand table-responsive">
    <tr class="table-warning font140">
    <th width="24%" class="text-right">Selected Notation</th>
    <td>{{=selectedNotation}}</td>
    </tr>
    <tr class="table-success font140">
    <th class="text-right">Original Translation</th>
    <td>{{=originalTranslation}}</td>
    </tr>
    </table>
    </div>
    <div>
    {{ if(isNotEmpty){ }}
    <table class="table table-bordered table-strfiped  bg-white tr_hand table-responsive" >
    <tbody>
    <tr class="bg-greye9">
    <th>Translations</th>
    <th width="15%">Translated By</th>
    <th width="15%">Copy</th>
    <th width="20%">{{ if(isVoted){ }} Votes {{ }else{ }} Vote {{ } }}</th>
    </tr>
    {{=otherUsersTranslation}}
    </tbody>
    </table>
    {{ } }}
    <div>
    <div class="card-header clearfix"> Input your translation </div>
    <div class="card-block bg-white ">
    <textarea class="form-control" id="translation-text" rows="3">{{=ownTranslation}}</textarea>
    <div class="clear"></div>
    </div>
    <div class="card-footer clearfix">
    <span class="marginauto">
    <input id="save-translation" value="Save" type="button" class="btn btn-primary">
    </span>
    </div>
    </div>
    </div>
</script>

<script type="text/template" id="tpl-otherTranslation">
    <tr>
    <td>{{=translation}}</td>
    <td>{{=userName}}</td>
    <td>
    <a class="btn btn-block btn-secondary btn-xs translation-copy-btn" data-translation-value="{{=translation}}">
    <i class="fa fa-copy"></i>
    <span class="font12"> Copy</span>
    </a>
    </td>
    <td>
    {{ if(isVoted){ }}
    <progress class="progress" value="{{=voteResult}}" max="100"></progress>{{=voteResult}}%
    {{ }else{ }}
    <a class="btn btn-block btn-secondary btn-xs vote-btn" data-translation-id="{{=translationId}}">
    <i class="fa fa-thumbs-up text-success "></i>
    <span class="font12"> Vote</span>
    </a>
    {{ } }}
    </td>
    </tr>
</script>