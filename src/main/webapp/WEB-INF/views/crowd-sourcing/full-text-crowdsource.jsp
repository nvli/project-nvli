<%-- 
    Document   : full_text_crowdsource
    Created on : Nov 10, 2017, 5:09:11 PM
    Author     : Milind
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Full Text Crowdsource</title>
    <!--    <link rel="stylesheet" href="${context}/themes/css/full_text_crowdsource_fonts.css"/>
        <link rel="stylesheet" href="${context}/themes/css/full_text_crowdsource_main.css"/>
        <link rel="stylesheet" href="${context}/themes/css/full_text_crowdsource_hocr-proofreader.css"/>-->
        <link rel="stylesheet" href="${context}/themes/css/bootstrap.css">
        <link rel="stylesheet" href="${context}/themes/css/style.css" >
        <link rel="stylesheet" href="${context}/themes/css/default.css" >
        <link rel="stylesheet" href="${context}/themes/css/font-awesome.css" >
        <link rel="stylesheet" href="${context}/themes/css/full_text_crowdsource_main.css"/>

        <script type="text/javascript">
            var con = '${context}';
            var currentPage = parseInt('${currentPage}');
            var totalPages = parseInt('${totalPages}');
            var recordName = '${recordName}';
            var hocrFileName = '${recordName}-' + currentPage + '.html';
            var recordLocation = '${recordLocation}';
            var originalName = recordLocation.split('/').reverse()[0];
            function displayNextPage()
            {
                if (currentPage === totalPages) {
                    alert("You are on last page");
                }
                else {
                    currentPage++;
                    window.location = "${context}/cs/full-text-crowdsource/" + recordName + "/" + currentPage + "/" + totalPages + "?recordLocation=" + recordLocation+"&recordTitle=${recordTitle}";
                }
            }

            function displayPrevPage()
            {
                if (currentPage === 1) {
                    alert("You are on first page");
                }
                else {
                    currentPage--;
                    window.location = "${context}/cs/full-text-crowdsource/" + recordName + "/" + currentPage + "/" + totalPages + "?recordLocation=" + recordLocation+"&recordTitle=${recordTitle}";

                }
            }
        </script>
        <script src="${context}/themes/js/full_text_crowdsource_hocr-proofreader.js"></script>
        <script type="text/javascript">
            var hocrBase = '${cdn}/' + '${recordLocation}/';
            var hOCRName = "hocr/" + originalName + "-" + currentPage + ".html";
            var ImgName = originalName + "-" + currentPage + ".jpg";
            var htmlName = originalName + "-" + currentPage + ".html";
        </script>
        <script src="${context}/themes/js/full_text_crowdsource_main.js"></script>
    </head>
    <body>
        <div id="lheader" class="valign-middle">
            <div class="lheader_logo  columns"> <img src="${context}/themes/images/logo/NVLI-LOGO-header.png"  alt="National Virtual Library of India (NVLI)" class="pull-left" > </div>
            <h2 class="text-center columns">${recordTitle}</h2>
            <div class="columns">
                <div class="pull-right m-r-1"> <span id="page-status" class="font14 font-weight-bold">6/47</span>
                    <button id="button-prev"  class="btn btn-sm  btn-primary" onClick="displayPrevPage();"><i class="fa fa-2x  fa-caret-left" title="Previous"></i> Prev</button>
                    <button id="button-next"  class="btn btn-sm  btn-primary" onClick="displayNextPage();">Next <i class="fa fa-2x fa-caret-right " title="Next"></i> </button>
                </div>
            </div>
        </div>
        <div class="main_container">
            <div class="card toolbar">
                <div class="col-container">     

                    <div id="layout-container" class="col-lg-6 col-sm-6 col-xs-6 col-xl-6 col-md-6 container col  layout"></div>
                    <div id="editor-container" class="col-lg-6 col-sm-6 col-xs-6 col-xl-6 col-md-6 col container"></div>
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <footer class="lfooter">
            <div class="text-center" >
                <button id="zoom-page-full" class="btn btn-sm btn-primary"><i class="fa  fa-expand "></i> Fit to Page</button>
                <button id="zoom-original"  class="btn btn-sm btn-primary"><i class="fa  fa-picture-o "></i> Original</button>
                <button id="zoom-page-width"  class="btn btn-sm btn-primary"><i class="fa  fa-arrows-h "></i> Page Width</button>
                <button id="button-save"  class="btn btn-sm btn-success"><i class="fa fa-save" title="Save"> </i> Save</button>
            </div>
        </footer>  
        <script type="text/javascript">
            document.getElementById("page-status").innerHTML = currentPage + "/" + totalPages;
        </script>
    </body>
</html>
