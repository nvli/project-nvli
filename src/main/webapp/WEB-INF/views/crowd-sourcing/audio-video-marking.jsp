<%--
    Document   : content
    Created on : Jun 7, 2016, 3:24:01 PM
    Author     : Doppa Srinivas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<script src="${context}/themes/js/masonry.pkgd.min.js"></script>
<script src="${context}/themes/js/custom.js"></script>
<script language="JavaScript">
    var audioVideoFileList = "${previewFileList.toString()}";
    var audioVideoList = [];
    var audioVideoMarkingArray = ${audioVideoMarkingMap};
    var markerArray = {};
    var markerChanges = {};
    var selectedFileName;
    var selectedFileNameWithPath;
    var sourceObjectType = "${sourceObjectType}";
    var markerArrayDiv = "savedMarkerList";
    var markerChangesDiv = "currentMarkerList";
    var recordIdentifier = "${recordIdentifier}";
    var markerChangesArray = {};

    audioVideoFileList = audioVideoFileList.replace("[", "").replace("]", "").split(",");

    for (var i = 0; i < audioVideoFileList.length; i++)
    {
        audioVideoList[audioVideoList.length] = audioVideoFileList[i].toString().trim();
    }

    $(document).ready(function ()
    {
        //get subtitle data
        getSubtiltes('all');
        var fileName = audioVideoList[0];
        selectedFileNameWithPath = fileName;
        selectedFileName = fileName.toString().split("/")[fileName.toString().split("/").length - 1];
        markerArray = audioVideoMarkingArray[selectedFileName];
        if (typeof markerArray === "undefined")
            markerArray = {};
        markerChangesArray[selectedFileName] = markerChanges;
        var option = '<option value="' + selectedFileNameWithPath + '" selected>' + selectedFileName + '</option>';

        for (var i = 1; i < audioVideoList.length; i++) {
            fileName = audioVideoList[i];
            option += '<option value="' + fileName + '">' + fileName.toString().split("/")[fileName.toString().split("/").length - 1] + '</option>';
            markerChanges = {};
            markerChangesArray[fileName.toString().split("/")[fileName.toString().split("/").length - 1]] = markerChanges;
        }
        markerChanges = markerChangesArray[selectedFileName];
        $('#fileSelectBox').append(option);
        addMarkingTags();
        addContent();
        $("#fileSelectBox").change(function ()
        {
            markerChangesArray[selectedFileName] = markerChanges;
            selectedFileNameWithPath = $(this).val();
            selectedFileName = selectedFileNameWithPath.toString().split("/")[selectedFileNameWithPath.toString().split("/").length - 1];
            markerArray = audioVideoMarkingArray[selectedFileName];

            if (typeof markerArray === "undefined")
                markerArray = {};
            markerChanges = markerChangesArray[selectedFileName];
            addMarkingTags();
            addContent();
            disableSaveBtn();
        });

        $(".arrow-down").hide();
        $(".arrow-up1").hide();
        $(".accordion-head1").click(function () {
            // $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
        });
        $(".accordion-head2").click(function () {
            // $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up1, .arrow-down1").toggle();
        });
        $('#tblGrid tr').click(function (event) {
            alert($(this).attr('id')); //trying to alert id of the clicked row

        });
    });

    //A string replace function
    function str_replace(haystack, needle, replacement) {
        var temp = haystack.split(needle);
        return temp.join(replacement);
    }
    function markAudioVideo()
    {
        var audioVideoObj = document.getElementsByTagName(sourceObjectType)[0];
        audioVideoObj.pause();
        var audioVideoCurrentTime = audioVideoObj.currentTime;
        if ((typeof markerChanges[audioVideoCurrentTime] === "undefined") && (typeof markerArray[audioVideoCurrentTime] === "undefined"))
        {
            $('#markTime').val(msToTimecode(audioVideoCurrentTime * 1000));
            $('#markAudioVideo').html('<input type="text" id="markString" name="markString" class="form-control" maxlength="150" /><input type="button"  class="btn btn-secondary" onclick="doneMarking(' + audioVideoCurrentTime + ');" value="Add"/><input type="button"  class="btn btn-secondary" onclick="cancelEvent();" value="cancel"/>');

            $('#markString').keypress(function (event) {
                var regex = new RegExp("^[a-zA-Z0-9 \b-_]");
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (!regex.test(key)) {
                    event.preventDefault();
                    return false;
                }
            });

            $('#markString').keyup(function (event) {
                if ($('#markString').val().length >= 15)
                {
                    openTextArea();
                    return true;
                }
            });
        } else
        {
            audioVideoObj.play();
            alert("marker time already present.");
        }
    }

    function openTextArea()
    {
        $("#textAreaButtonSpan").html('<input type="button"  class="btn btn-secondary" id="buttonDone" value="Done"/>');
        $('#textAreaDiv').modal('show');
        $("#textAreaValue").focus().val("").val($('#markString').val());

        $("#buttonDone").click(function () {
            $('#markString').val($("#textAreaValue").val());
            $('#textAreaDiv').modal('hide');
            ;
        });

        $('#textAreaValue').keypress(function (event) {
            var regex = new RegExp("^[a-zA-Z0-9 \b-_]");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });
    }

    function cancelEvent()
    {
        var audioVideoObj = document.getElementsByTagName(sourceObjectType)[0];
        $('#markAudioVideo').html(' <button class="btn btn-secondary" type="button" onClick="markAudioVideo();">Mark</button>');
        $('#markTime').val("");
        audioVideoObj.play();
    }

    function doneMarking(audioVideoCurrentTime)
    {
        if ($('#markString').val() === "")
        {
            alert("Please enter marker name.");
        } else
        {
            var audioVideoObj = document.getElementsByTagName(sourceObjectType)[0];
            markerChanges[audioVideoCurrentTime] = $('#markString').val() + "," + msToTimecode(audioVideoCurrentTime * 1000);
            addMarkingTags();
            $('#markAudioVideo').html(' <button class="btn btn-secondary" type="button" onClick="markAudioVideo();">Mark</button>');
            $('#markTime').val("");
            audioVideoObj.play();
        }
        disableSaveBtn();
    }

    function deleteMarker(audioVideoCurrentTime, id)
    {
        delete markerChanges[audioVideoCurrentTime];
        addMarkingTags(markerChanges, markerChangesDiv);
        disableSaveBtn();
    }

    function playWithOffset(offset)
    {
        var audioVideoObj = document.getElementsByTagName(sourceObjectType)[0];
        audioVideoObj.currentTime = offset;
        audioVideoObj.play();
    }

    function msToTimecode(duration)
    {
        var milliseconds = parseInt(duration % 1000)
                , seconds = parseInt((duration / 1000) % 60)
                , minutes = parseInt((duration / (1000 * 60)) % 60)
                , hours = parseInt((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
    }

    function saveAudioVideoMarking()
    {
        audioVideoMarkingArray[selectedFileName] = markerChanges;
        $.ajax(
                {
                    url: '${context}/cs/saveAudioVideoMarking',
                    data: 'audioVideoMarkingMap=' + JSON.stringify(audioVideoMarkingArray) + '&recordIdentifier=' + recordIdentifier,
                    type: 'POST',
                    success: function (response)
                    {
                        if (response === 'failed')
                            alert("Failed to save marked tags. Please try again");
                        else
                        {
                            alert("Successfully saved..");
                            for (var key in markerChanges)
                            {
                                markerArray[key] = markerChanges[key];
                            }
                            audioVideoMarkingArray[selectedFileName] = markerArray;
                            markerChanges = {};
                            markerChangesArray[selectedFileName] = markerChanges;
                            addMarkingTags();
                            disableSaveBtn();
                        }
                    },
                    error: function ()
                    {
                        alert("XML generation failed. Try again");
                    }
                });
    }
    function disableSaveBtn()
    {
        if (jQuery.isEmptyObject(markerChanges))
            $('#save-audio-video-marking').attr('disabled', 'disabled');
        else
            $('#save-audio-video-marking').removeAttr('disabled', 'disabled');
    }
    function addMarkingTags() {
        var divInnerHTML = '';
        var mapKeys = Object.keys(markerArray);
        mapKeys.sort(function (a, b) {
            return a - b;
        });
        for (var i = 0; i < mapKeys.length; i++) {
            divInnerHTML = divInnerHTML + ' <tr ><td onClick="playWithOffset(' + mapKeys[i] + ');" >' + markerArray[mapKeys[i]].toString().split(",")[1] + '</td><td>' + markerArray[mapKeys[i]].toString().split(",")[0] + '</td></tr>';
        }
        mapKeys = Object.keys(markerChanges);
        mapKeys.sort(function (a, b) {
            return a - b;
        });
        for (var i = 0; i < mapKeys.length; i++) {
            divInnerHTML = divInnerHTML + ' <tr id="' + i + '"><td onClick="playWithOffset(' + mapKeys[i] + ');" >' + markerChanges[mapKeys[i]].toString().split(",")[1] + '</td><td>' + markerChanges[mapKeys[i]].toString().split(",")[0] + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-secondary" title="Delete" onClick="deleteMarker(' + mapKeys[i] + ',' + i + ');"><span class="fa fa-trash" aria-hidden="true"></span></button></td></tr>';
        }
        document.getElementById('markerTableBody').innerHTML = divInnerHTML;
    }
    function addContent() {
        if (sourceObjectType === 'video')
        {
            $("#audioVideoId").removeClass("audio").addClass("video");
            $("#audioVideoId").html('<video controls controlsList="nodownload"><source src="' + selectedFileNameWithPath + '">Your browser does not support the video tag.</video>');
        } else if (sourceObjectType === 'audio')
        {
            $("#audioVideoId").removeClass("video").addClass("audio");
            $("#audioVideoId").html('<audio controls><source src="' + selectedFileNameWithPath + '">Your browser does not support the audio element.</audio>');
        }
    }
    function getSubtiltes(filterString) {
        if (filterString.trim().length === 0)
            filterString = 'all';
        $.ajax(
                {
                    url: '${context}/cs/getSubtiltes',
                    data: 'filterString=' + filterString,
                    type: 'POST',
                    success: function (response) {
                        var divInnerHtML = ' <tr><td colspan="3"><center><h3>No Data Found</h3></center></td></tr>';
                        if (Object.keys(response).length > 0)
                            divInnerHtML = '';
                        for (var key in response) {
                            divInnerHtML = divInnerHtML + '<tr onclick="playWithOffset(' + (key / 1000) + ');">';
                            divInnerHtML = divInnerHtML + '<td>' + (response[key].split("&|&")[1]) + '</td>';
                            divInnerHtML = divInnerHtML + '<td>' + (response[key].split("&|&")[0]) + '</td></tr>';
                        }
                        document.getElementById('subTitleBody').innerHTML = divInnerHtML;
                    },
                    error: function () {

                    }
                });
    }
</script>
<!-- Horizontal breadcrumbs -->
<ol class="breadcrumb">
    <li><a href="${context}/home"><i class="fa fa-home" aria-hidden="true"> </i> <tags:message code="label.home"/></a></li>
    <li><a href="#"><tags:message code="label.Marking"/></a></li>
</ol>
<div style="margin-bottom:0px;" class="clearfix"></div>

<div class="container-fluid">
    <!-- content starts here -->
    <div class="container-fluid" id="main">
        <div class="clear"></div>
        <div class="col-lg-12">
            <!--Breadcrumb-->
            <!--show hide panel-->
            <div style="margin-bottom:20px;" class="clearfix"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form role="form">
                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <div class="row">
                            <div class="article_writing">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <div class="accordion-head1 tr_hand">
                                                <div type="button" data-toggle="collapse" data-target="#markContent" aria-expanded="true" aria-controls="collapseExample">
                                                    <span>
                                                        <strong><tags:message code="label.MarkContent"/></strong>
                                                    </span>
                                                    <span class="arrow-up">
                                                        <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                                    </span>
                                                    <span class="arrow-down">
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div id="markContent" class="collapse in border_line1">
                                                <div class="card-block">
                                                    <fieldset class="form-group">
                                                        <div class="col-lg-7 col-md-7 col-sm-12">
                                                            <div class="input-group" style="float: left;">
                                                                <input type="text" class="form-control" disabled id="markTime">
                                                                <span class="input-group-btn" id="markAudioVideo">
                                                                    <button class="btn btn-secondary" type="button" onClick="markAudioVideo();"><tags:message code="button.mark"/></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="" id="markerTable">
                                                    <table class="table table-striped table-bordered table-hover tr_hand">
                                                        <thead class="">
                                                            <tr>
                                                                <th style="width: 30%;"><tags:message code="label.TimeCode"/></th>
                                                                <th><tags:message code="label.MarkedText"/></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="markerTableBody">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="card-footer clearfix">
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-lg-offset-3" >
                                                        <button class="btn btn-primary" type="button" id="save-audio-video-marking" onclick="saveAudioVideoMarking();" disabled><tags:message code="label.save"/></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear" style="margin-bottom: 20px;"></div>
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="accordion-head1 tr_hand">
                                                <div  type="button" data-toggle="collapse" data-target="#searchResult" aria-controls="collapseExample">
                                                    <span>
                                                        <strong><tags:message code="label.Subtitles"/></strong>
                                                    </span>
                                                    <span class="arrow-up1">
                                                        <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                                    </span>
                                                    <span class="arrow-down1">
                                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div id="searchResult" class="collapse border_line1">
                                                <div class="card-block">
                                                    <fieldset class="form-group">
                                                        <label class="form-control-label col-lg-3 col-md-3 col-sm-12 text-right "><tags:message code="label.SearchValue"/></label>
                                                        <div class="col-lg-5 col-md-8 col-sm-12">
                                                            <input type="text" class="form-control" name="search" placeholder="Search for sub title" onkeyup="getSubtiltes(this.value);">
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <div class="overflow330 table-responsive">
                                                    <table class="table table-striped table-bordered table-hover tr_hand">
                                                        <thead class="">
                                                            <tr>
                                                                <th><tags:message code="label.time"/></th>
                                                                <th><tags:message code="label.SubTitle"/></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="subTitleBody">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7">
                        <div class="row">
                            <div class="card-block">
                                <div class="card-header4">
                                    <div class="form-group row" style="margin-bottom: 0;">
                                        <label class="form-control-label col-lg-2 col-md-3 col-sm-12 text-right "  for="videoSelect"><tags:message code="label.SelectVideo"/></label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <fieldset class="form-group" style="margin-bottom: 0;">
                                                <select class="form-control" id="fileSelectBox">
                                                </select>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-lg-offset-2" id="audioVideoId">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="clear22"></div>
<!-- s result content container -->
<div style="margin-bottom:20px;" class="clearfix"></div>
<div style="margin-bottom:0px;" class="clearfix"></div>

<!-- dialog box for textArea starts here -->
<div class="modal fade" id="textAreaDiv" role="dialog">
    <div class="modal-dialog modal-xlg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.EnterMarker.Text"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 form-group">
                        <textarea name="textarea" cols="78" rows="5" id="textAreaValue" maxlength="150" ></textarea>
                    </div>
                </div><br>
            </div>
            <div class="modal-footer" id="textAreaButtonSpan">
            </div>
        </div>
    </div>
</div>
<!-- Dialog Box for textArea End here-->