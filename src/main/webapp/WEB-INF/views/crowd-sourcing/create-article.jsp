<%--
    Document   : create-article
    Created on : Jun 15, 2016, 7:11:59 PM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@include file="../../static/components/pagination/pagination.html" %>
<style>

    .img-preview > img {
        max-width: 100%;

    }

    .docs-preview {
        margin-bottom: 1rem;
        background: #efefef;
        height: 600px;
    }

    .img-preview {
        /*float: left;*/
        margin-right: .5rem;
        margin-bottom: .5rem;
        overflow: hidden;
    }

    .img-preview > img {
        max-width: 100%;
    }

    .preview-lg {
        width: 16rem;
        height: 9rem;
    }

    .preview-md {
        width: 8rem;
        height: 4.5rem;
    }

    .preview-sm {
        width: 4rem;
        height: 2.25rem;
    }

    .preview-xs {
        width: 4rem;
        height: 1.125rem;
        margin-right: 0;
    }
    .help-text{
        color : #666;
    }
    .uploaded-image{
        position: relative;
    }
    .overlay {
        position: absolute;
        bottom:0;
        left: 0;
        right: 0;
        background-color: rgba(255,255,255,0.6);
        overflow: hidden;
        width: 100%;
        height: 0;
        transition: height 1s ease-in-out;
        display: none;
        text-align: center;
        padding: 10px;
    }

    .uploaded-image:hover .overlay {
        height: 50px;
        display: block;
    }

    /*source window size*/
    textarea.cke_source{
        max-height: 100%
    }
</style>
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.css" />
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
<script type='text/javascript' src="${context}/themes/js/language/pramukhindic.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
<script type="text/javascript" src="${context}/themes/js/language/switchLanguage_crowdSource.js"></script>
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script src="${context}/components/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.js"></script>
<script src="${context}/scripts/image-browser.js"/>
<script src="${context}/components/jquery/jquery-dateFormat.min.js"></script>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <form:form id="articleForm" method="post" action="${context}/cs/save-article" commandName="createArticleFormBean">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="article_writing card">
                    <div class="card-header"><tags:message code="label.WriteArticle"/></div>
                    <div id="write-article" class="collapse in">
                        <div class="card-block row">
                            <div class="col-sm-12 col-md-12 col-lg-8">
                                <div  class="form-group row">
                                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 m-b-lg-down10">
                                        <form:input type="text" class="form-control" id="articleTitle" path="crowdSourceArticle.articleTitle" placeholder="Article Title" autocomplete="off"/>
                                    </div>
                                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 m-b-lg-down10">
                                        <form:select id="article-category" class="form-control" path="crowdSourceArticle.articleCategory.id" >
                                            <option value="-1"> --<tags:message code="label.SelectCategory"/>-- </option>
                                            <c:forEach items="${resourcesList}" var="resource">
                                                <c:if test="${createArticleFormBean.crowdSourceArticle.articleCategory.id eq resource.id}">
                                                    <option value="${resource.id}" selected> ${resource.resourceName} </option>
                                                </c:if>
                                                <c:if test="${createArticleFormBean.crowdSourceArticle.articleCategory.id ne resource.id}">
                                                    <option value="${resource.id}"> ${resource.resourceName} </option>
                                                </c:if>
                                            </c:forEach>
                                        </form:select>
                                    </div>
                                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12">
                                        <c:choose>
                                            <c:when test="${createArticleFormBean.crowdSourceArticle.isPublished eq 'false' || createArticleFormBean.crowdSourceArticle.id eq '-1' }">
                                                <button class="btn btn-sm btn-primary" id="save-article" onclick="saveArticle();" type="button" >
                                                    <i class="fa fa-save" aria-hidden="true"></i> 
                                                    <tags:message code="label.save"/>
                                                </button>
                                            </c:when>
                                            <c:otherwise>
                                                <button class="btn btn-sm btn-primary" id="save-article" onclick="saveArticle();" type="button" style="display: none;">
                                                    <i class="fa fa-save" aria-hidden="true" ></i>
                                                    <tags:message code="label.save"/>
                                                </button>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:choose>
                                            <c:when test="${createArticleFormBean.crowdSourceArticle.isPublished eq 'false' && createArticleFormBean.crowdSourceArticle.id ne '-1'}">
                                                <button type="button" id="publish-article" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="bottom" title="Article last saved will be published.">
                                                    <tags:message code="button.Publish"/>
                                                </button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="button" id="publish-article" class="btn btn-sm btn-primary" style="display: none;">
                                                    <tags:message code="button.Publish"/>
                                                </button>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:choose>
                                            <c:when test="${createArticleFormBean.crowdSourceArticle.isPublished eq 'true'}">
                                                <button type="button" id="withdraw-article" class="btn btn-sm btn-primary" onclick="withdrawArticle(${createArticleFormBean.crowdSourceArticle.id});" >
                                                    <tags:message code="button.Withdraw"/>
                                                </button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="button" id="withdraw-article" class="btn btn-sm btn-primary" onclick="withdrawArticle(${createArticleFormBean.crowdSourceArticle.id});" style="display: none;">
                                                    <tags:message code="button.Withdraw"/>
                                                </button>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:if test="${createArticleFormBean.crowdSourceArticle.id ne '-1'}">
                                            <a class="btn btn-sm btn-primary"  href="${context}/cs/preview-article/${createArticleFormBean.crowdSourceArticle.id}">
                                                <tags:message code="label.Preview"/>
                                            </a>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12 col-md-9 col-sm-12">
                                        <form:textarea path="crowdSourceArticle.articleText" class="form-control" id="articleText"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12">
                                <div class="panel card ">
                                    <div class="article_writing">
                                        <div class="accordion-head1 tr_hand collapsed">
                                            <div  type="button" data-toggle="collapse" data-target="#udc-tags" aria-expanded="true" aria-controls="collapseExample">
                                                <span>
                                                    <strong><tags:message code="label.UDCTags"/></strong>
                                                </span>
                                                <span class="arrow-down">
                                                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                                </span>
                                                <span class="arrow-up">
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div id="udc-tags" class="collapsed border_line collapse in">
                                            <div class="form-group">
                                                <div class="bg-greye9" style="border-bottom:1px solid #ccc;">
                                                    <div class="m_lab udt_tagEllipsis" id="addUDCTags"></div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset class="form-group">
                                                    <div class="col-lg-12 col-md-9 col-sm-12">
                                                        <select class="form-control" id="selectLanguage" onchange="changeUDCLanguage('selectLanguage');" >
                                                            <c:forEach items="${languageList}" var="language">
                                                                <c:if test="${language.id eq 40}">
                                                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                                                </c:if>
                                                                <c:if test="${language.id ne 40}">
                                                                    <option value="${language.id}">${language.languageName}</option>
                                                                </c:if>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <div class="col-lg-12 col-md-7 col-sm-12 " style="margin-left:0%">
                                                        <div class="input-group" style="width:100%">
                                                            <span class="input-group-btn" style="width:40%">
                                                                <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#udcTreeViewModel"><tags:message code="button.TreeView.Selection"/></button>
                                                            </span>
                                                            <input type="text" placeholder="Enter UDC Tags..." name="udcTags" id="udcAutocomplete" class="form-control"/>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel card m-t-1">
                                    <div class="article_writing">
                                        <div class="accordion-head1 tr_hand">
                                            <div type="button" data-toggle="collapse" data-target="#custom-tags" aria-expanded="true" aria-controls="collapseExample">
                                                <span>
                                                    <strong><tags:message code="label.CustomTags"/></strong>
                                                </span>
                                                <span class="arrow-down">
                                                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                                </span>
                                                <span class="arrow-up">
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div id="custom-tags" class="collapsed border_line collapse in">
                                            <div class="form-group">
                                                <div class="bg-greye9" style="border-bottom:1px solid #ccc;">
                                                    <div class="m_lab udt_tagEllipsis" id="addCustomTags"></div>
                                                </div>
                                            </div>
                                            <div>
                                                <fieldset class="form-group">
                                                    <div class="col-lg-12 col-md-9 col-sm-12">
                                                        <select class="form-control" id="selectCustomLanguage" onchange="changeCustomLanguage();">
                                                            <c:forEach items="${languageList}" var="language">
                                                                <c:if test="${language.id eq 40}">
                                                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                                                </c:if>
                                                                <c:if test="${language.id ne 40}">
                                                                    <option value="${language.id}">${language.languageName}</option>
                                                                </c:if>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </fieldset>
                                                <fieldset class="form-group">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <div class=" col-xl-8 col-lg-12 col-md-9 col-sm-9 ">
                                                                <input type="text" placeholder="Enter Custom Tags" name="customTags" class="form-control"  id="customAutocomplete" />
                                                            </div>
                                                            <div class="col-xl-4 col-lg-12 col-md-3 col-sm-3">
                                                                <button class="btn btn-primary"
                                                                        onclick="addCustomTags(document.getElementById('customAutocomplete').value, 'addCustomTags', 'custom');"
                                                                        type="button"><tags:message code="button.AddTag"/>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel card m-t-1">
                                    <div class="article_writing">
                                        <div class="accordion-head1 tr_hand">
                                            <div  type="button" data-toggle="collapse" data-target="#related-article" aria-expanded="true" aria-controls="collapseExample">
                                                <span>
                                                    <strong><tags:message code="label.RelatedArticles"/></strong>
                                                </span>
                                                <span class="arrow-down">
                                                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                                </span>
                                                <span class="arrow-up">
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div id="related-article" class="collapsed border_line collapse in">
                                            <div class="card-block">
                                                <fieldset class="form-group " >
                                                    <button type="button" class="btn btn-secondary" id="pick-articles"><tags:message code="label.PickArticle"/></button>
                                                </fieldset>
                                            </div>
                                            <div id="bookmarkList_dashboard">
                                                <div id="show-article-container"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel card m-t-1">
                                    <div class="article_writing">
                                        <div class="accordion-head1 tr_hand">
                                            <div  type="button" data-toggle="collapse" data-target="#related-resources" aria-expanded="true" aria-controls="collapseExample">
                                                <span>
                                                    <strong><tags:message code="label.RelatedResources"/></strong>
                                                </span>
                                                <span class="arrow-down">
                                                    <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                                </span>
                                                <span class="arrow-up">
                                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div id="related-resources" class="collapsed border_line collapse in">
                                            <div class="card-block">
                                                <div class="form-group row">
                                                    <label class="form-control-label col-lg-4 col-md-3 col-sm-12 " for="resourceUrl">Resource URL:</label>
                                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                                        <input type="text" class="form-control" placeholder="Paste Resource URL" id="related-resource-url"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="form-control-label col-lg-4 col-md-3 col-sm-12 " for="resouceLabel">Label Name: </label>
                                                    <div class="col-lg-8 col-md-9 col-sm-12">
                                                        <input type="text" class="form-control" placeholder="Enter Label for Resource"  id="related-resource-label-url"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-8 col-md-7 col-sm-12 col-lg-offset-4">
                                                        <button class="btn btn-primary" id="add-resource-url" type="button"><tags:message code="button.AddResource"/></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="bookmarkList_dashboard">
                                                <div id="resource-container">
                                                    <c:set value="false" var="flag" />
                                                    <c:forEach items="${createArticleFormBean.crowdSourceArticleAttachedResources}" var="crowdSourceArticleAttachedResource" varStatus="resourceCount">
                                                        <form:input type="hidden" path="crowdSourceArticleAttachedResources[${resourceCount.index}].id"  />
                                                        <div id="resource-${resourceCount.index}">
                                                            <form:input type="hidden" path="crowdSourceArticleAttachedResources[${resourceCount.index}].resourceLabel" />
                                                            <form:input type="hidden" path="crowdSourceArticleAttachedResources[${resourceCount.index}].resourceURL"  />
                                                            <hr/>
                                                            <div class="favoriteList">
                                                                <p>
                                                                    <span>
                                                                        <a href="${crowdSourceArticleAttachedResource.resourceURL}">${crowdSourceArticleAttachedResource.resourceLabel}</a> <a href="#"><i class="fa fa-trash pull-right" aria-hidden="true" onClick='removeRelatedResources("${resourceCount.index}")'></i></a>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <c:set value="${resourceCount.index}" var="resourceCountSet" />
                                                        <c:set value="true" var="flag" />
                                                    </c:forEach>
                                                </div>
                                                <c:if test="${flag}">
                                                    <input type="hidden" value="<c:out value="${resourceCountSet+1}"/>" id="resourceCountValue"/>
                                                </c:if>
                                                <c:if test="${!flag}">
                                                    <input type="hidden" value="0" id="resourceCountValue"/>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <form:input type="hidden" path="crowdSourceArticle.id" id="articleId"/>
        <form:input type="hidden" path="crowdSourceArticle.user.id" id="userId"/>
        <form:input type="hidden" path="crowdSourceArticle.isPublished" id="isPublished"/>
        <form:input type="hidden" path="crowdSourceArticle.isApproved" id="isApproved"/>
        <form:input type="hidden" path="crowdSourceArticle.udcTags" id="udcTags"/>
        <form:input type="hidden" path="crowdSourceArticle.customTags" id="customTags"/>
        <form:input type="hidden" path="crowdSourceArticle.publishDate" id="publishDate"/>
        <form:input type="hidden" path="crowdSourceArticle.lastModifiedDate" id="lastModifiedDate"/>
        <form:input type="hidden" path="crowdSourceArticle.relatedArticles" id="relatedArticles"/>
    </form:form>
</div>
<div class="modal fade" id="udcTreeViewModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><tags:message code="label.UDCTags"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-3 form-group">
                        <select class="form-control" id="selectLanguageModel" onchange="changeUDCLanguage('selectLanguageModel');">
                            <c:forEach items="${languageList}" var="language">
                                <c:if test="${language.id eq 40}">
                                    <option value="${language.id}" selected="true">${language.languageName}</option>
                                </c:if>
                                <c:if test="${language.id ne 40}">
                                    <option value="${language.id}">${language.languageName}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div id="udcTreeviewCheckable" class="treeview"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addTreeviewSelectionList();" data-dismiss="modal"><tags:message code="label.add"/></button>
            </div>
        </div>
    </div>
</div>
<div id="pickArticleModel" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="card-header row" style="display: block; margin: 0">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                    <strong class="header-title"><tags:message code="label.articles"/></strong>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <input id="searchString" class="form-control" placeholder="Search title">
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%"></th>
                        <th width="5%"><tags:message code="label.serial.no"/></th>
                        <th width="60%"><tags:message code="label.article.title"/></th>
                        <th width="15%"><tags:message code="label.publishDate"/></th>
                        <th width="15%"><tags:message code="label.author"/></th>
                    </tr>
                </thead>
                <tbody id="article-tbl-body"></tbody>
            </table>
            <div id="article-tbl-footer" class="card-footer"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    CKEDITOR.replace('articleText', {
        extraPlugins: 'timestamp,akinzacropify,tableresize,tableselection,tabletools',
        language: '${pageContext.response.locale}',
        tabSpaces: 4,
        height: 750,
        allowedContent: true,
        // The following options are not necessary and are used here for presentation purposes only.
        // They configure the Styles drop-down list and widgets to use classes.

        stylesSet: [
            {name: 'Narrow image', type: 'widget', widget: 'image', attributes: {'class': 'image-narrow'}},
            {name: 'Wide image', type: 'widget', widget: 'image', attributes: {'class': 'image-wide'}},
            {name: 'Narrow media', type: 'widget', widget: 'embed', attributes: {'class': 'embed-narrow'}},
            {name: 'Centered media', type: 'widget', widget: 'embed', attributes: {'class': 'embed-align-center'}}
        ],
        // Load the default contents.css file plus customizations for this sample.
        contentsCss: [CKEDITOR.basePath + 'contents.css', 'http://sdk.ckeditor.com/samples/assets/css/widgetstyles.css'],
        // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
        // resizer (because image size is controlled by widget styles or the image takes maximum
// 100% of the editor width).
        image2_alignClasses: ['image-align-left', 'image-align-center', 'image-align-right'],
        image2_disableResizer: true,
        cropperCSS: "https://fengyuanchen.github.io/cropper/css/cropper.css",
        cropperJS: "https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.js"
    });
    CKEDITOR.config.scayt_autoStartup = true;
</script>
<script type="text/javascript">
    var nodeIds;
    var notation;
    var notationText;
    var udcJson = {};
    var customTagsJson = {};
    var existingUDCTags = '${createArticleFormBean.crowdSourceArticle.udcTags}';
    var existingCustomTags = '${createArticleFormBean.crowdSourceArticle.customTags}';
    var languageId = '40';
    var _relatedArticlesArr = [];
    var _relatedArticlesMap = {};
    $(document).ready(function () {

        /*******Chiatanya JS **************/

        $(".arrow-down").hide();
        $(".arrow-down1").hide();
        $(".accordion-head1").click(function () {
// $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
        });
        $(".accordion-head").click(function () {
// $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up1, .arrow-down1").toggle();
        });
        $('#tblGrid tr').click(function (event) {
            alert($(this).attr('id')); //trying to alert id of the clicked row

        });
        /************Chaitanya JS end************/

        suggestions();
        customTagSuggestions();
        stringToJSON(existingUDCTags, 'udc');
        stringToJSON(existingCustomTags, 'custom');
        getUDCTreeviewData();
        setLanguage("lanSelect", "udcAutocomplete");
        setLanguage("lanSelect", "customAutocomplete");
        if (!_.isEmpty(document.getElementById("relatedArticles").value)) {
            var tempRelatedArticleMap = {};
            _relatedArticlesArr = document.getElementById("relatedArticles").value.split('&|&');
            var relatedArticleArray =${relatedArticleJsonArray};
            for (var key in relatedArticleArray) {
                if (relatedArticleArray.hasOwnProperty(key)) {
                    tempRelatedArticleMap[relatedArticleArray[key].id] = relatedArticleArray[key].title + "&|&" + relatedArticleArray[key].author;
                }
            }
            _relatedArticlesMap = tempRelatedArticleMap;
            addArticles();
        }
        $('#pick-articles').unbind().bind("click", function (e) {
            fetchAndRenderTableData(1);
            $("#pickArticleModel").modal('show');
        });
        $('#add-resource-url').unbind().bind("click", function (e) {
            addResources();
        });

        $('#articleTitle').keyup(function () {
            var re = /"/;
            if (re.test($('#articleTitle').val())) {
                var no_double_quote = $('#articleTitle').val().replace(/"/, '');
                $('#articleTitle').val(no_double_quote);
            }
        });

        $('#publish-article').unbind().bind("click", function (e) {
            var articleId = document.getElementById("articleId").value;
            $.ajax({
                url: __NVLI.context + "/cs/publish-article",
                dataType: 'json',
                type: 'post',
                data: {
                    'articleId': articleId
                },
                success: function (returnFlag) {
                    if (returnFlag)
                    {
                        document.getElementById("isPublished").value = returnFlag;
                        alert("Article Sent to the expert for publish.");
                        $('#save-article').hide();
                        $('#publish-article').hide();
                        $('#withdraw-article').show();
                    } else
                        alert("Service is not available,please try after some time!");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown, jqXHR);
                    alert("try after some time");
                }
            });
        });
    });

    //A string replace function
    function str_replace(haystack, needle, replacement) {
        var temp = haystack.split(needle);
        return temp.join(replacement);
    }

    function addTags(tagsJson, div, tagType)
    {
        document.getElementById(div).innerHTML = '';
        if (!checkJsonIsEmpty(tagsJson))
        {
            var divHtml = '';
            $.each(tagsJson, function (keyName, value) {
                if (tagType === 'custom')
                    divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" onclick="deleteUDCTag(\'' + keyName + '\',\'' + div + '\',\'' + tagType + '\');"> <span class="fa fa-remove"></span>  ' + value + ' </button>';
                else
                    divHtml = divHtml + '<button type="button" class="btn btn-secondary tag_ellipsis" style="margin:0 0 10px 10px;" onclick="deleteUDCTag(\'' + keyName + '\',\'' + div + '\',\'' + tagType + '\');"> <span class="fa fa-remove"></span>  ' + keyName + '   ' + value + ' </button>';
            });
            document.getElementById(div).innerHTML = divHtml;
        }
    }

    function deleteUDCTag(key, div, tagType)
    {
        if (tagType === 'udc')
        {
            delete udcJson[key];
            addTags(udcJson, div, tagType);
            var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[key]);
            $('#udcTreeviewCheckable').treeview('uncheckNode', [nodeId, {silent: true}]);
        } else
        {
            delete customTagsJson[key];
            addTags(customTagsJson, div, tagType);
        }
    }

    function checkJsonIsEmpty(tagsJson)
    {
        return (Object.keys(tagsJson).length === 0 && JSON.stringify(tagsJson) === JSON.stringify({}));
    }

    function addCustomTags(tagValue, div, tagType)
    {
        tagValue = tagValue.trim().replace(/[_,\\\/!@#$%^&*\[\]{}\'\";:]/gm, '');
        languageId = document.getElementById('selectCustomLanguage').value;
        if (!checkJsonIsEmpty(customTagsJson) && (customTagsJson[tagValue + '_' + languageId] !== null))
            alert('Tag already exist!');
        else
        {
            if (tagValue !== '' && tagValue.length !== 0)
            {
                customTagsJson[tagValue + '_' + languageId] = tagValue;
                addTags(customTagsJson, div, tagType);
                document.getElementById('customAutocomplete').value = '';
            }
        }
    }

    function changeUDCLanguage(languageDiv)
    {
        languageId = document.getElementById(languageDiv).value;
        document.getElementById('selectLanguage').value = languageId;
        document.getElementById('selectLanguageModel').value = languageId;
        if (!checkJsonIsEmpty(udcJson))
        {
            var jsonObject = {};
            var udcTagNotionsAdded = '';
            $.each(udcJson, function (keyName, value) {
                if (udcTagNotionsAdded === '')
                    udcTagNotionsAdded = keyName;
                else
                    udcTagNotionsAdded = udcTagNotionsAdded + "&|&" + keyName;
            });
            $.ajax({
                url: '${context}/cs/getUDCTagDescription',
                dataType: 'json',
                type: "post",
                data: {
                    'udcNotations': udcTagNotionsAdded,
                    'languageId': languageId
                },
                success: function (response)
                {
                    for (var key in response)
                        jsonObject[key] = response[key];
                    if (!checkJsonIsEmpty(jsonObject)) {
                        udcJson = jsonObject;
                        addTags(udcJson, 'addUDCTags', 'udc');
                    }
                },
                error: function ()
                {

                }
            });
        }
        suggestions();
        getUDCTreeviewData();
    }

    function suggestions()
    {
        languageId = document.getElementById('selectLanguage').value;
        $('#udcAutocomplete').devbridgeAutocomplete({
            paramName: 'tagPrefix',
            serviceUrl: '${context}/cs/getUDCTagsSuggestions?languageId=' + languageId,
            onSelect: function (suggestion) {
                if (!checkJsonIsEmpty(udcJson) && (udcJson[suggestion.data] !== null))
                    alert('Tag already exist!');
                else
                {
                    udcJson[suggestion.data] = suggestion.value;
                    addTags(udcJson, 'addUDCTags', 'udc');
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[suggestion.data]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                }
                document.getElementById('udcAutocomplete').value = '';
            }
        });
    }

    function stringToJSON(tags, type)
    {
        if (type === 'udc')
        {
            $.ajax({
                url: '${context}/cs/getUDCTagDescription',
                dataType: 'json',
                type: "post",
                data: {
                    'udcNotations': tags,
                    'languageId': languageId
                },
                success: function (response)
                {
                    for (var key in response)
                        udcJson[key] = response[key];
                    addTags(udcJson, 'addUDCTags', 'udc');
                },
                error: function ()
                {

                }
            });
        } else
        {
            if (tags !== '' && tags.length !== 0)
            {
                tags = tags.split('&|&');
                for (var i = 0; i < tags.length; i++) {
                    customTagsJson[tags[i]] = tags[i];
                }
                addTags(customTagsJson, 'addCustomTags', 'custom');
            }
        }
    }

    function changeCustomLanguage()
    {
        languageId = document.getElementById('selectCustomLanguage').value;
        customTagSuggestions();
    }

    function customTagSuggestions()
    {
        languageId = document.getElementById('selectCustomLanguage').value;
        $('#customAutocomplete').devbridgeAutocomplete({
            paramName: 'tagPrefix',
            serviceUrl: '${context}/cs/getCustomTagsSuggestions?languageId=' + languageId
        });
    }

    function getUDCTreeviewData()
    {
        var tags = existingUDCTags;
        var isDisable = 'false';
        $.ajax({
            url: '${context}/cs/getUDCTreeviewData',
            data: {languageId: languageId, tags: tags, isDisable: isDisable},
            type: 'POST',
            success: function (response)
            {
                nodeIds = response["nodeIds"];
                $('#udcTreeviewCheckable').treeview({
                    data: response["udcTreeViewData"],
                    showIcon: false,
                    showCheckbox: true,
                    onNodeChecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        notationText = node.text.split("]")[1].trim();
                        udcJson[notation] = notationText;
                    },
                    onNodeUnchecked: function (event, node) {
                        notation = (node.text.substring(1)).split("]")[0];
                        delete udcJson[notation];
                    }
                });
                $.each(udcJson, function (keyName, value)
                {
                    var nodeId = $('#udcTreeviewCheckable').treeview('getNode', nodeIds[keyName]);
                    $('#udcTreeviewCheckable').treeview('checkNode', [nodeId, {silent: true}]);
                });
            },
            error: function ()
            {
            }
        });
    }

    function fetchAndRenderTableData(pageNo)
    {
        var dataLimit = 15;
        $.ajax({
            url: '${context}/cs/getArticlesByFiltersWithLimit',
            data: {searchString: $('#searchString').val(), statusTypeFilter: "published", dataLimit: dataLimit, pageNo: pageNo, isRequiredUserRestriction: false},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.articleList)) {
                        $("#article-tbl-body").html('<tr><td colspan="7" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var articlesTpl = _.template($("#tpl-article-tbl-row").html());
                        var Opts = {
                            "articleList": jsonObject.articleList,
                            "relatedArticlesArr": _relatedArticlesArr,
                            "recordCount": ((pageNo - 1) * dataLimit)
                        };
                        var articleTableBody = articlesTpl(Opts);
                        $("#article-tbl-body").empty();
                        $("#article-tbl-body").html(articleTableBody);
                        $("#searchString").unbind().bind("keyup", function () {
                            fetchAndRenderTableData(1);
                        });
                        renderPagination(fetchAndRenderTableData, "article-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, dataLimit, true, 3, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
            }
        });
    }

    function addTreeviewSelectionList()
    {
        addTags(udcJson, 'addUDCTags', 'udc');
    }

    function saveArticle() {
        if ($('#articleTitle').val().length === 0) {
            alert('Article title required.');
        } else if ($('#article-category').val() === "-1") {
            alert('Please select the article category.');
        } else {
            var tags = '';
            $.each(udcJson, function (keyName, value)
            {
                if (tags === '')
                    tags = keyName;
                else
                    tags = tags + '&|&' + keyName;
            });
            document.getElementById("udcTags").value = tags;

            tags = '';
            $.each(customTagsJson, function (keyName, value)
            {
                if (tags === '')
                    tags = value;
                else
                    tags = tags + '&|&' + value;
            });
            document.getElementById("customTags").value = tags;
            $('#articleForm').submit();
        }
    }

    function withdrawArticle(articleId) {
        $.ajax({
            url: __NVLI.context + "/cs/withdraw-article",
            dataType: 'json',
            type: 'post',
            data: {
                'articleId': articleId
            },
            success: function (returnFlag) {
                if (returnFlag)
                {
                    document.getElementById("isPublished").value = false;
                    alert("Article withdraw successfully.");
                    $('#save-article').show();
                    $('#publish-article').show();
                    $('#withdraw-article').hide();
                } else
                    alert("Service is not available,please try after some time!");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(textStatus, errorThrown, jqXHR);
                alert("try after some time");
            }
        });

    }

    function addArticles() {
        var articleTitle;
        var articleId;
        var tags = "";
        $("#show-article-container").empty();
        $.each(_relatedArticlesMap, function (key, value) {
            articleId = key;
            articleTitle = value.split('&|&')[0];
            articleAuthor = value.split('&|&')[1];
            if (tags === '')
                tags = articleId;
            else
                tags = tags + '&|&' + articleId;
            var article_template = _.template($("#tpl-show-articles").html());
            var opts = {
                "articleTitle": articleTitle,
                "articleId": articleId,
                "articleAuthor": articleAuthor
            };
            var article_el = article_template(opts);
            $("#show-article-container").append(article_el);
        });
        document.getElementById("relatedArticles").value = tags;
    }

    function deleteArticles()
    {

        addArticles();
    }

    function addResources()
    {
        var resourceURL = document.getElementById("related-resource-url").value;
        var resourceLabel = document.getElementById("related-resource-label-url").value;
        var resourceCountValue = parseInt(document.getElementById("resourceCountValue").value);
        if (!(resourceURL.indexOf(__NVLI.context) >= 0))
        {
            alert("This is outside url");
            return;
        }
        if (resourceLabel === '')
            alert("Please add resource label");
        else {
            $.ajax({
                url: __NVLI.context + "/cs/is-active-url",
                dataType: 'json',
                timeout: 10000,
                data: {
                    'activeURL': resourceURL
                },
                type: 'post',
                success: function (response) {
                    if (response)
                    {
                        var article_template = _.template($("#tpl-show-resources").html());
                        var opts = {
                            "resourceLabel": resourceLabel,
                            "resourceURL": resourceURL,
                            "resourceCount": resourceCountValue
                        };
                        document.getElementById("resourceCountValue").value = resourceCountValue + 1;
                        document.getElementById("related-resource-url").value = '';
                        document.getElementById("related-resource-label-url").value = '';
                        var article_el = article_template(opts);
                        $("#resource-container").append(article_el);
                    } else
                        alert("URL Not Active");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus === 'timeout')
                        alert("try after some time");
                }
            });
        }
    }

    function handleClick(currentArticle) {
        var articleId = currentArticle.value.split('&|&')[0];
        if (currentArticle.checked) {
            _relatedArticlesArr.push(articleId);
            _relatedArticlesMap[articleId] = currentArticle.value.split('&|&')[1] + "&|&" + currentArticle.value.split('&|&')[2];
        } else {
            var i = _relatedArticlesArr.indexOf(articleId);
            if (i !== -1) {
                _relatedArticlesArr.splice(i, 1);
            }
            delete _relatedArticlesMap[articleId];
        }
        addArticles();
    }

    function removeRelatedArticle(articleId) {
        $("#article-" + articleId).remove();
        var i = _relatedArticlesArr.indexOf(articleId);
        if (i !== -1) {
            _relatedArticlesArr.splice(i, 1);
        }
        delete _relatedArticlesMap[articleId];
        addArticles();
    }

    function removeRelatedResources(resourceCount) {
        $("#resource-" + resourceCount).remove();
    }
</script>
<script type="text/template" id="tpl-article-tbl-row">
    {{ _.each(articleList, function (article,i) { }}
    {{ var checked = _.contains(relatedArticlesArr, article.id + ""); }}
    <tr>
    <td><input type="checkbox" {{if(checked){ }} checked {{ } }} onclick='handleClick(this)' value="{{=article.id}}&|&{{=article.articleTitle}}&|&{{=article.user.firstName}}" name="pick-article-checkbox" />
    <td>{{=i+recordCount+1}}</td>
    <td>{{=article.articleTitle}}</td>
    <td>
    {{ if(article.isPublished) { }}
    {{=$.format.date(new Date(article.publishDate), "dd MMM yyyy hh:mm:ss")}}
    {{ }else{ }}
    NA
    {{ } }}
    </td>
    <td>
    {{=article.user.firstName}}
    </td>
    </tr>
    {{ }); }}
</script>
<script type="text/template" id="tpl-show-articles">
    <div id="article-{{=articleId}}"><hr/>
    <div class="favoriteList">
    <p><span>
    <a href="${context}/cs/preview-article/{{=articleId}}">{{=articleTitle}}</a> by <b>{{=articleAuthor}}</b> <a href="#"><i class="fa fa-trash pull-right" aria-hidden="true" onClick='removeRelatedArticle("{{=articleId}}")'></i></a>
    </span>
    <%--        <span class="pull-right"><a href="${context}/cs/preview-article/{{=articleId}}"><i class="fa fa-remove" aria-hidden="true"></i> Remove </a></span> --%>
    </p>
    </div></div>
</script>
<script type="text/template" id="tpl-show-resources">
    <div id="resource-{{=resourceCount}}">
    <input type="hidden" name="crowdSourceArticleAttachedResources[{{=resourceCount}}].resourceLabel" value="{{=resourceLabel}}" />
    <input type="hidden" name="crowdSourceArticleAttachedResources[{{=resourceCount}}].resourceURL" value="{{=resourceURL}}" />
    <hr/>
    <div  class="favoriteList">
    <p>
    <span>
    <a href="{{=resourceURL}}">{{=resourceLabel}}</a> <a href="#"><i class="fa fa-trash pull-right" aria-hidden="true" onClick='removeRelatedResources("{{=resourceCount}}")'></i></a>
    </span>
    </p>
    </div>
    </div>
</script>

