<%--
    Document   : preview-article
    Created on : Jun 16, 2016, 11:13:15 AM
    Author     : Ritesh Malviya
    Author     : Vivek Bugale
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<style>
    img{
        max-width: 100%;
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-sm-12 col-md-7 col-lg-9">
            <div class="article_writing border_line">
                <div class="accordion-head1 tr_hand">
                    <div  type="button" data-toggle="" data-target="#name_Article" aria-expanded="true" aria-controls="collapseExample">
                        <span>
                            <strong> ${crowdSourceArticle.articleTitle} </strong>
                        </span>
                        <c:if test="${crowdSourceArticle.user.id eq currentUserId && crowdSourceArticle.isPublished ne 'true'}">
                            <span><a class="pull-right" title="Edit article" href="${context}/cs/edit-article/${crowdSourceArticle.id}"><i class="fa fa-pencil"></i></a></span>
                                </c:if>
                    </div>
                </div>
                <div id="name_Article" class="collapse in">
                    <div class="card-block">
                        <div>
                            ${crowdSourceArticle.articleText}
                        </div>
                        <c:if test="${crowdSourceArticle.isPublished eq 'true'}">
                            <span><tags:message code="label.Published"/> <b><fmt:formatDate pattern="dd-MMM-yyyy HH:mm:ss" value="${crowdSourceArticle.publishDate}" /></b> by <a href="#"><b>${crowdSourceArticle.user.firstName} ${crowdSourceArticle.user.lastName} </b></a></span>
                        </c:if>
                    </div>
                    <security:authorize access="hasAnyRole('ADMIN_USER')">
                        <c:if test="${crowdSourceArticle.isPublished eq 'true' && crowdSourceArticle.isApproved eq 'false'}">
                            <hr/>
                            <div class="form-group row">
                                <div class="col-lg-12 col-md-12 col-sm-12" align="center">
                                    <button id="approve-article" class="btn btn-primary" type="button"><tags:message code="button.Approve"/></button>
                                </div>
                            </div>
                        </c:if>
                    </security:authorize>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-3">
            <div class="panel card">
                <div class="article_writing border_line">
                    <div class="accordion-head1 tr_hand">
                        <div  type="button" data-toggle="collapse" data-target="#relatedUDCTags" aria-expanded="true" aria-controls="collapseExample">
                            <span>
                                <strong> <tags:message code="label.UDCTags"/> </strong>
                            </span>
                            <span class="arrow-up">
                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            </span>
                            <span class="arrow-down">
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div id="relatedUDCTags" class="collapse in">
                        <div class="card-block">
                            <div class="p-a-1 custom_paddingarticle">
                                <c:forEach items="${crowdSourceArticleUDCTags}" var="crowdSourceArticleUDCTag">
                                    <a href="#" class="label label-default">${crowdSourceArticleUDCTag.key} ${crowdSourceArticleUDCTag.value}</a>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel card m-t-1">
                <div class="article_writing border_line">
                    <div class="accordion-head1 tr_hand">
                        <div  type="button" data-toggle="collapse" data-target="#relatedCustomTags" aria-expanded="true" aria-controls="collapseExample">
                            <span>
                                <strong> <tags:message code="label.CustomTags"/> </strong>
                            </span>
                            <span class="arrow-up">
                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            </span>
                            <span class="arrow-down">
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div id="relatedCustomTags" class="collapse in">
                        <div class="card-block">
                            <div class="p-a-1 custom_paddingarticle">
                                <c:forEach items="${crowdSourceArticleCustomTags}" var="crowdSourceArticleCustomTag">
                                    <a href="#" class="label label-default">${crowdSourceArticleCustomTag}</a>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel card m-t-1">
                <div class="article_writing border_line">
                    <div class="accordion-head1 tr_hand">
                        <div  type="button" data-toggle="collapse" data-target="#relatedArticles" aria-expanded="true" aria-controls="collapseExample">
                            <span>
                                <strong> <tags:message code="label.RelatedArticles"/> </strong>
                            </span>
                            <span class="arrow-up">
                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            </span>
                            <span class="arrow-down">
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div id="relatedArticles" class="collapse in">
                        <div id="bookmarkList_dashboard" class="collapse in">
                            <div class="card-block">
                                <c:forEach items="${crowdSourceAttachedArticleList}" var="crowdSourceAttachedArticle">
                                    <hr/>
                                    <div class="favoriteList">
                                        <p>
                                            <span>
                                                <a href="${context}/cs/preview-article/${crowdSourceAttachedArticle.id}">${crowdSourceAttachedArticle.articleTitle}</a> by <b>${crowdSourceAttachedArticle.user.firstName} ${crowdSourceAttachedArticle.user.lastName}</b>
                                            </span>
                                        </p>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel card m-t-1">
                <div class="article_writing border_line">
                    <div class="accordion-head1 tr_hand">
                        <div  type="button" data-toggle="collapse" data-target="#relatedResources" aria-expanded="true" aria-controls="collapseExample">
                            <span>
                                <strong><tags:message code="label.RelatedResources"/></strong>
                            </span>
                            <span class="arrow-up">
                                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                            </span>
                            <span class="arrow-down">
                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                    <div id="relatedResources" class="collapse in">
                        <div id="bookmarkList_dashboard" class="collapse in">
                            <div class="card-block">
                                <c:forEach items="${crowdSourceArticle.crowdSourceArticleAttachedResources}" var="crowdSourceArticleAttachedResource">
                                    <hr/>
                                    <div class="favoriteList">
                                        <p>
                                            <span>
                                                <a href="${crowdSourceArticleAttachedResource.resourceURL}">${crowdSourceArticleAttachedResource.resourceLabel}</a>
                                            </span>
                                        </p>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="${crowdSourceArticle.id}" id="articleId"/>
</div>
<script>
    $(document).ready(function () {

        /*******Chiatanya JS **************/

        $(".arrow-down").hide();
        $(".arrow-down1").hide();
        $(".accordion-head1").click(function () {
            // $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
        });
        $(".accordion-head").click(function () {
            // $(this).next(".option-content").slideToggle(500);
            $(this).find(".arrow-up1, .arrow-down1").toggle();
        });
        $('#tblGrid tr').click(function (event) {
            alert($(this).attr('id')); //trying to alert id of the clicked row

        });
        /************Chaitanya JS end************/

        $('#approve-article').unbind().bind("click", function (e) {
            var articleId = document.getElementById("articleId").value;
            $.ajax({
                url: __NVLI.context + "/cs/approve-article",
                type: 'post',
                data: {
                    'articleId': articleId
                },
                success: function (returnFlag) {
                    if (returnFlag)
                    {
                        $('#approve-article').remove();
                        alert("Article Approved Successfully.");
                    } else
                        alert("Service is not available,please try after some time!");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(textStatus, errorThrown, jqXHR);
                    alert("try after some time");
                }
            });
        });
    });

    //A string replace function
    function str_replace(haystack, needle, replacement) {
        var temp = haystack.split(needle);
        return temp.join(replacement);
    }
</script>