<%-- 
    Document   : create-privacy-policy
    Created on : Aug 29, 2017, 11:48:10 AM
    Author     : Gulafsha Khan <gulafsha@cdac.in>
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@include file="../../static/components/pagination/pagination.html" %>
<style>

    .img-preview > img {
        max-width: 100%;

    }

    .docs-preview {
        margin-bottom: 1rem;
        background: #efefef;
        height: 600px;
    }

    .img-preview {
        /*float: left;*/
        margin-right: .5rem;
        margin-bottom: .5rem;
        overflow: hidden;
    }

    .img-preview > img {
        max-width: 100%;
    }

    .preview-lg {
        width: 16rem;
        height: 9rem;
    }

    .preview-md {
        width: 8rem;
        height: 4.5rem;
    }

    .preview-sm {
        width: 4rem;
        height: 2.25rem;
    }

    .preview-xs {
        width: 4rem;
        height: 1.125rem;
        margin-right: 0;
    }
    .help-text{
        color : #666;
    }
    .uploaded-image{
        position: relative;
    }
    .overlay {
        position: absolute;
        bottom:0;
        left: 0;
        right: 0;
        background-color: rgba(255,255,255,0.6);
        overflow: hidden;
        width: 100%;
        height: 0;
        transition: height 1s ease-in-out;
        display: none;
        text-align: center;
        padding: 10px;
    }

    .uploaded-image:hover .overlay {
        height: 50px;
        display: block;
    }
    textarea.cke_source{
        max-height: 100%
    }
</style>
<link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.css" />
<script src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
<script src="${context}/components/bootstrap-treeview/bootstrap-treeview.js"></script>
<script src="${context}/themes/js/ckeditor/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.js"></script>
<script src="${context}/scripts/image-browser.js">
    < script src = "${context}/components/jquery/jquery-dateFormat.min.js" ></script>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="pp_writing">
                <div class="accordion-head1 tr_hand">
                    <div  type="button" data-toggle="" data-target="#write_pp" aria-expanded="true" aria-controls="collapseExample">
                        <span>
                            <strong class="editor-policy">Write Policy</strong>
                        </span>
                    </div>
                </div>
                <div id="write-article" class="collapse in border_line1">
                    <div class="card-block">
                        <div class="col-sm-12 col-md-7 col-lg-12">
                            <div  class="form-group row">
                                <div class="col-lg-6 col-md-8 col-sm-12">
                                    <input  type="text" class="form-control" id="ppTittle" placeholder="Enter Policy Title" autocomplete="off"/>
                                </div>
                                <div class="col-lg-6 col-md-8 col-sm-12">
                                    <button class="btn btn-primary" id="save-article" type="button" ><i class="fa fa-save" aria-hidden="true"></i> <tags:message code="label.save"/></button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12 col-md-9 col-sm-12">
                                    <textarea id="ppEditor">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
            CKEDITOR.replace('ppEditor', {
                extraPlugins: 'embed,autoembed,image2,timestamp,akinzacropify',
                height: 750,
                allowedContent: true,
                // The following options are not necessary and are used here for presentation purposes only.
                // They configure the Styles drop-down list and widgets to use classes.

                stylesSet: [
                    {name: 'Narrow image', type: 'widget', widget: 'image', attributes: {'class': 'image-narrow'}},
                    {name: 'Wide image', type: 'widget', widget: 'image', attributes: {'class': 'image-wide'}},
                    {name: 'Narrow media', type: 'widget', widget: 'embed', attributes: {'class': 'embed-narrow'}},
                    {name: 'Centered media', type: 'widget', widget: 'embed', attributes: {'class': 'embed-align-center'}}
                ],
                // Load the default contents.css file plus customizations for this sample.
                contentsCss: [CKEDITOR.basePath + 'contents.css', 'http://sdk.ckeditor.com/samples/assets/css/widgetstyles.css'],
                // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
                // resizer (because image size is controlled by widget styles or the image takes maximum
                // 100% of the editor width).
                image2_alignClasses: ['image-align-left', 'image-align-center', 'image-align-right'],
                image2_disableResizer: true,
                cropperCSS: "https://fengyuanchen.github.io/cropper/css/cropper.css",
                cropperJS: "https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.js"
            });
</script>
<script>
    $(document).ready(function () {
        var policyId =${id};
        checkPolicyType(policyId);
        isNewPrivacy(policyId);
        $("#save-article").click(function () {
            if ($("#ppTittle").val() === "") {
                alert('please give some Policy Tittle');
                return;
            }
            if (CKEDITOR.instances['ppEditor'].getData() === "") {
                alert('please give some content in privacy policy editor');
                return;
            }
            var data = {};
            data ['tittle'] = $("#ppTittle").val();
            data['text'] = CKEDITOR.instances['ppEditor'].getData();
            data['policyId'] = policyId;
            var id = policyId;
            $.post(window.__NVLI.context + "/policy/policy", data, function (response) {
                if (response.status === 1) {
                    // sendNotifications(response.notificationActivityType);
                    window.location.href = __NVLI.context + "/policy/manage/policy-types/" + id;
                } else if (response.status === 0) {
                    window.location.href = __NVLI.context + "/policy/manage/policy-types/" + id;
                } else if (response.status === 2) {
                    //sendNotifications();
                    window.location.href = __NVLI.context + "/policy/manage/policy-types/" + id;
                } else if (response.status === 3) {
                    //sendNotifications();
                    window.location.href = __NVLI.context + "/policy/manage/policy-types/" + id;
                }
            });
        });
    });
    function checkPolicyType(typeId) {
        var data = {};
        data['typeId'] = typeId;
        $.post(window.__NVLI.context + "/policy/check/policy-type", data, function (object) {
            if (object.status === 1)
            {
                //$("#ppTittle").attr('placeholder', "Enter Privacy Policy Tittle");
                $(".editor-policy").text("Write Privacy Policy");
            } else if (object.status === 2)
            {
                //$("#ppTittle").attr('placeholder', "Enter Terms and Service Policy Tittle");
                $(".editor-policy").text("Write Terms and Services");
            }
        });
    }
    function sendNotifications(notificationActivityType, message) {
        console.log("send notifs");
        $.ajax({
            url: "${pageContext.request.contextPath}/search/user/notification/" + notificationActivityType + "/" + message,
            cache: false,
            success: function (jsonObj) {
            },
            error: function () {
                alert("Notification Service not available.Please try after some time.");
            }
        });
    }
    function isNewPrivacy(policyId) {
        var data = {};
        data['typeId'] = policyId;
        $.post(window.__NVLI.context + "/policy/check/policy", data, function (response) {
            if (response) {
                CKEDITOR.instances['ppEditor'].setData('');
            } else {
                $.post(window.__NVLI.context + "/policy/find/maximum-version", data, function (jsonObject) {
                    if (jsonObject !== null)
                    {
                        $("#ppTittle").val(jsonObject.policyObject.tittle);
                        CKEDITOR.instances['ppEditor'].setData(jsonObject.policyObject.description);
                    }
                });
            }
        });
    }
</script>
