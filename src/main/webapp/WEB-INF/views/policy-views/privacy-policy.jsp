<%-- 
    Document   : privacy-policy
    Created on : Aug 14, 2017, 5:29:10 PM
    Author     : Gulafsha Khan <gulafsha@cdac.in>
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<script src="${context}/components/jquery/jquery-dateFormat.min.js"></script>
<%@include file="../../static/components/pagination/pagination.html" %>
<div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
    <div class="card">
        <div id="alert-box" style="display: none;"></div>
        <div class="card-header row" style="display: block; margin: 0">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
                <strong class="header-title policy-tittle"></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="${context}/privacy/create-privacy-policy" class="btn btn-success" id="create-edit-button"></a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <input id="searchString" class="form-control" placeholder="Search title">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-1">
                <select id="limitFilter" class="form-control">
                    <option value="10" selected>10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                </select>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%">S.NO</th>
                    <th width="20%">Policy Tittle</th>
                    <th width="15%">Last Modified Date</th>
                    <th width="15%">Publish Date</th>
                    <th width="15%">Created By</th>
                    <th width="15%">Last Updated By</th>
                    <th width="5%">Version</th>
                    <th style="text-align:center">View Policy</th>
                </tr>
            </thead>
            <tbody id="policy-tbl-body"></tbody>
        </table>
        <div id="policy-tbl-footer" class="card-footer clearfix"></div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="ppTittle"></h4>
                    </div>
                    <div class="modal-body" style="font-weight: normal" id="ppText">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="tpl-policy-tbl-row">
    {{ _.each(policyList, function (policy,i) { }}
    <tr>
    <td>{{=i+1+recordCount}}</td>
    <td>{{=policy.tittle}}</td>
    <td>{{=$.format.date(new Date(policy.lastModifiedDate), "dd MMM yyyy hh:mm:ss")}}</td>
    <td>{{=$.format.date(new Date(policy.publishDate), "dd MMM yyyy hh:mm:ss")}}</td>
    <td>{{=policy.createdBy}}</td>
    <td>{{=policy.updatedBy}}</td>
    <td>{{=policy.version}}</td>
    <td class="text-right" style="text-align:center">
    <div class="btn-group" title="Preview Policy">
    <button class="btn-secondary btn btn-view-policy" data-policy-id="{{=policy.policyId}}"><i class="fa fa-eye"></i></button>
    </div>
    </td>
    </tr>
    {{ }); }}
</script>
<script type="text/javascript">

    $(document).ready(function () {
        var typeId = ${typeId};
        checkPolicyType(typeId);
        isNewPrivacy(typeId);
        fetchAndRenderTableData(1);
        $("#searchString").unbind().bind("keyup", function () {
            fetchAndRenderTableData(1);
        });
        $("#limitFilter").unbind().bind("change", function () {
            fetchAndRenderTableData(1);
        });
    });
    function isNewPrivacy(typeId) {
        var data = {};
        data['typeId'] = typeId;
        $.post(window.__NVLI.context + "/policy/check/policy", data, function (response) {
            if (response)
            {
                $("#create-edit-button").text("Create Policy");
                $("#create-edit-button").attr("href", "${context}/policy/create-privacy-policy/" + typeId);
            } else
            {
                $("#create-edit-button").text("Edit Policy");
                $("#create-edit-button").attr("href", "${context}/policy/create-privacy-policy/" + typeId);
            }
        });
    }
    function checkPolicyType(typeId)
    {
        var data = {};
        data['typeId'] = typeId;
        $.post(window.__NVLI.context + "/policy/check/policy-type", data, function (object) {
            if (object !== null)
            {
                $(".policy-tittle").text(object.policyName);
            }

        });
    }
    function fetchAndRenderTableData(pageNo) {
        var typeId = ${typeId};
        $.ajax({
            url: '${context}/policy/getPoliciesByFiltersWithLimit',
            data: {searchString: $('#searchString').val(), dataLimit: $('#limitFilter').val(), pageNo: pageNo, typeId: typeId},
            type: 'POST',
            success: function (jsonObject) {
                if (jsonObject !== null) {
                    if (_.isEmpty(jsonObject.policyList)) {
                        $("#policy-tbl-body").html('<tr><td colspan="8" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                    } else {
                        var policesTpl = _.template($("#tpl-policy-tbl-row").html());
                        var Opts = {
                            "policyList": jsonObject.policyList,
                            "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                        };
                        var articleTableBody = policesTpl(Opts);
                        $("#policy-tbl-body").empty();
                        $("#policy-tbl-body").html(articleTableBody);
                        $(".btn-view-policy").unbind().bind("click", function (e) {
                            e.preventDefault();
                            $("#myModal").modal("show");
                            viewPolicy(e.currentTarget.getAttribute("data-policy-id"));
                        });
                        renderPagination(fetchAndRenderTableData, "policy-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                    }
                } else {
                    alertErrorMsg();
                }
            },
            error: function () {
                alertErrorMsg();
            }
        });
    }
    function viewPolicy(privacyId) {
        var data = {};
        data['privacyId'] = privacyId;
        $.post(window.__NVLI.context + "/policy/preview-policy", data, function (jsonObject) {
            if (jsonObject !== null)
            {
                $("#ppTittle").text(jsonObject.policyTittle);
                $("#ppText").html(jsonObject.policyDesc);
            }
        });
    }
    function alertErrorMsg() {
        $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
        $("#alert-box").fadeOut(5000);
    }
</script>

