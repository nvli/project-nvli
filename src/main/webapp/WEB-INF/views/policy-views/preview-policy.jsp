<%-- 
    Document   : preview-policy
    Created on : Nov 1, 2017, 2:59:16 PM
    Author     : Gulafsha Khan <gulafsha@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="tags" uri="http://www.springframework.org/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<div class="container">
    <div>
        <c:if test="${IsPolicyExist != 0}">  
            ${policyDesc}
        </c:if>
        </
    </div>
</div>
</div>
