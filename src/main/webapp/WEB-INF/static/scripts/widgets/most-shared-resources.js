function plotHighchart(containerId, title, times, series) {
    var text = 'Most Shared Links';
    if (_.isEmpty(series)) {
        text = "Nothing Shared in last 24 hours";
    }
    $(containerId).highcharts({
        title: {
            text: text
        },
        yAxis: {
            title: {
                text: 'Share Counts'
            },
            tickInterval: 1
        },
        xAxis: {
            title: {
                text: title
            },
            categories: times

        },
        chart: {
            type: 'spline'
        },
        series: series
    });
    $(".highcharts-credits").hide();
}

function plotDailyResourceChart() {
    $.ajax({
        url: __NVLI.context + "/analytics/ws/top/shared/records/d",
        type: "GET", //This is what you should chage
        dataType: "json",
        processData: false,
        success: function (response) {

            var timeData, times = [], title = "Time";
            if (!_.isEmpty(response.data)) {
                var timeData = response.trendsData[response.data[0].recordIdentifier].time;
                _.each(timeData, function (mili) {
                    var d = new Date(mili);
                    var am_pm = (d.getHours() < 12) ? ":00 AM" : ":00 PM";
                    var hour = d.getHours() % 12 || 12;
                    times.push(hour + am_pm);
                });
            }

            var series = [];
            $("#top-resource-items").empty();
            _.each(response.data, function (item, i) {
                var obj = {};
                // Render Resource Names
                var tpl = _.template($("#tpl-top-resource-item").html());
                var el = tpl({
                    resourceName: response.info[item.recordIdentifier],
                    link: __NVLI.context + "/search/preview/" + item.recordIdentifier,
                    i: (++i)
                });
                $("#top-resource-items").append(el);
                obj['name'] = response.info[item.recordIdentifier];
                obj['data'] = response.trendsData[item.recordIdentifier].trends;
                series.push(obj);
            });
            plotHighchart("#graph-container", title, times, series);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function plotResourceWeekly() {
    $.ajax({
        url: __NVLI.context + "/analytics/ws/top/shared/records/w",
        type: "GET", //This is what you should chage
        dataType: "json",
        processData: false,
        success: function (response) {
            var timeData, times = [];
            var title = "Day";
            var weekDays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            if (!_.isEmpty(response.data)) {
                var timeData = response.trendsData[response.data[0].recordIdentifier].time;
                _.each(timeData, function (mili) {
                    var d = new Date(mili);
                    times.push(weekDays[ d.getDay() ] + ", " + d.toLocaleDateString());
                });
            }
            var series = [];
            $("#top-resource-items").empty();
            _.each(response.data, function (item, i) {
                var obj = {};
                // Render Resource Names
                var tpl = _.template($("#tpl-top-resource-item").html());
                var el = tpl({
                    resourceName: response.info[item.recordIdentifier],
                    link: __NVLI.context + "/search/preview/" + item.recordIdentifier,
                    i: (++i)
                });
                $("#top-resource-items").append(el);
                obj['name'] = response.info[item.recordIdentifier];
                obj['data'] = response.trendsData[item.recordIdentifier].trends;
                series.push(obj);
            });
            plotHighchart("#graph-container", title, times, series);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        }
    });
}


function plotResourceMonthly() {
    $.ajax({
        url: __NVLI.context + "/analytics/ws/top/shared/records/m",
        type: "GET", //This is what you should chage
        dataType: "json",
        processData: false,
        success: function (response) {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var timeData, times = [], title, startingMonth, EndingMonth;
            if (!_.isEmpty(response.data)) {
                var timeData = response.trendsData[response.data[0].recordIdentifier].time;
                var d1 = new Date(timeData[0]);
                var d2 = new Date(timeData[_.size(timeData) - 1]);
                startingMonth = monthNames[(d1).getMonth()] + " " + d1.getDate() + ", " + d1.getFullYear();
                EndingMonth = monthNames[(d2).getMonth()] + " " + d2.getDate() + ", " + d2.getFullYear();
                title = startingMonth + " - " + EndingMonth;
                _.each(timeData, function (mili) {
                    var d = new Date(mili);
                    times.push(d.getDate());
                });
            }
            var series = [];
            $("#top-resource-items").empty();
            _.each(response.data, function (item, i) {
                var obj = {};
                // Render Resource Names
                var tpl = _.template($("#tpl-top-resource-item").html());
                var el = tpl({
                    resourceName: response.info[item.recordIdentifier],
                    link: __NVLI.context + "/search/preview/" + item.recordIdentifier,
                    i: (++i)
                });
                $("#top-resource-items").append(el);
                obj['name'] = response.info[item.recordIdentifier];
                obj['data'] = response.trendsData[item.recordIdentifier].trends;
                series.push(obj);
            });
//            $('#graph-container').css({height: "400px", width: "1000px"});
            plotHighchart("#graph-container", title, times, series);

        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}

function plotResourceyearly() {
    $.ajax({
        url: __NVLI.context + "/analytics/ws/top/shared/records/y",
        type: "GET", //This is what you should chage
        dataType: "json",
        processData: false,
        success: function (response) {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var timeData, times = [], title, startingYear, endingYear;
            if (!_.isEmpty(response.data)) {
                var timeData = response.trendsData[response.data[0].recordIdentifier].time;
                var d1 = new Date(timeData[0]);
                var d2 = new Date(timeData[_.size(timeData) - 1]);
                startingYear = monthNames[(d1).getMonth()] + ", " + d1.getFullYear();
                endingYear = monthNames[(d2).getMonth()] + ", " + d2.getFullYear();
                title = startingYear + " - " + endingYear;
                _.each(timeData, function (mili) {
                    var d = new Date(mili);
                    times.push(monthNames[d.getMonth()]);
                });
            }
            var series = [];
            $("#top-resource-items").empty();
            _.each(response.data, function (item, i) {
                var obj = {};
                // Render Resource Names
                var tpl = _.template($("#tpl-top-resource-item").html());
                var el = tpl({
                    resourceName: response.info[item.recordIdentifier],
                    link: __NVLI.context + "/search/preview/" + item.recordIdentifier,
                    i: (++i)
                });
                $("#top-resource-items").append(el);
                obj['name'] = response.info[item.recordIdentifier];
                obj['data'] = response.trendsData[item.recordIdentifier].trends;
                series.push(obj);
            });
//            $('#graph-container').css({height: "400px", width: "1000px"});
            console.log(series, monthNames);
            plotHighchart("#graph-container", title, times, series);

        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}

function toggleButtonResourceType(el) {

}

$(document).ready(function () {
    //plotDailyResourceChart();
    plotResourceyearly();
    $("#daily").unbind().bind("click", function (e) {
        $(".chart-type-btn").removeClass('active');
        $(this).addClass('active');
        plotDailyResourceChart();
    })

    $("#weekly").unbind().bind("click", function (e) {
        $(".chart-type-btn").removeClass('active');
        $(this).addClass('active');
        plotResourceWeekly();
    })

    $("#monthly").unbind().bind("click", function (e) {
        $(".chart-type-btn").removeClass('active');
        $(this).addClass('active');
        plotResourceMonthly();
    })

    $("#yearly").unbind().bind("click", function (e) {
        $(".chart-type-btn").removeClass('active');
        $(this).addClass('active');
        plotResourceyearly();
    })
});