$(document).ready(function () {
    var url = __NVLI.context + "/analytics/get/most/searched/keywords";
    var source = {
        datatype: "json",
        datafields: [
            {name: 'score'},
            {name: 'value'}
        ],
        url: url
    };
    var dataAdapter = new $.jqx.dataAdapter(source, {});
    $('#tagCloud').jqxTagCloud({
        source: dataAdapter,
        fontSizeUnit: 'px',
        minFontSize: 14,
        maxFontSize: 24,
        displayMember: 'value',
        valueMember: 'score',
        minColor: '#00AA99',
        maxColor: '#FF0000'
    });
});