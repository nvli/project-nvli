function getResourceTypes() {
    $.ajax({
        url: __NVLI.context + "/analytics/ws/get/resourceTypes",
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            $("#resource-type-select").empty();
            response = _.sortBy(response, 'resourceType');
            _.each(response, function (item) {
                var tpl = _.template($("#tpl-resource-type").html());
                var option = tpl(item);
                $("#resource-type-select").append(option);
            });
            $("#resource-type-select").change(function (e) {
                var url = __NVLI.context + "/analytics/getRecordInfos/" + e.currentTarget.options[e.currentTarget.selectedIndex].value
                getDataFromServer(url);
            });
            var resourceTypeCode = $("#resource-type-select").val();
            var weburl = __NVLI.context + "/analytics/getRecordInfos/" + resourceTypeCode;
            getDataFromServer(weburl);
//            console.log(response);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

$(document).ready(function () {
    $('.allResourcesFilters label').removeClass("active");
    $('.allResourcesFilters').click(function () {
        $('.allResourcesFilters label').removeClass("active");
    });
    getResourceTypes();
//
//    $('#graph-container-widget2').html("Loading data...");
//


});

function getDataFromServer(weburl) {

//    console.log(weburl);
    $.ajax({
        url: weburl,
        type: "GET", //This is what you should chage
        dataType: "json",
        processData: false,
        success: function (response) {
//            console.log(response);
            var r = response;

            $("#resource-items-widget2").empty();
            _.each(response, function (item, i) {
                var tpl = _.template($("#tpl-top-viewd-resource").html());
                var el = tpl({
                    url: __NVLI.context + "/search/preview/" + item.recordIdentifier + "?language=" + __NVLI.locale,
                    serial: (i + 1),
                    resourceName: item.nameToView
                });
                $("#resource-items-widget2").append(el);
            });

            var color = ['#00FF00', '#FF00FF', '#FAEBD7', '#FFD700', '#FF7F50', '#CD5C5C', '#D3D3D3', '#ADD8E6', '#FAFAD2', '#48D1CC', '#48D1CC'];

            var data = new Array();

            for (p in r) {
                data.push({name: r[p].nameToView, y: parseInt(r[p].recordViewCount), color: color[p]});
            }

            var options = {
                loading: {
                    hideDuration: 1000,
                    showDuration: 1000
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Most Viewed Links',
                    align: 'center',
                },
                xAxis: {
                    title: {
                        text: 'Resource Seriese'
                    },
                    categories: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'View count'
                    }
                },
                chart: {
                    renderTo: 'container',
                    type: 'column'
                },
                series: [{}]
            };

            options.series[0].data = data;
            options.series[0].showInLegend = false;
            $('#graph-container-widget2').highcharts(options);
        },
        error: function (xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            console.log(xhr.status, xhr.responseText, xhr, ajaxOptions, thrownError);
            $('#graph-container-widget2').html("Resourse not found");
        }
    });

}
function fetchData() {

    $('.allResourcesFilters').click(function () {
        $('.toggle').hide();
        $('#widget' + $(this).attr('id')).show();
    });
}