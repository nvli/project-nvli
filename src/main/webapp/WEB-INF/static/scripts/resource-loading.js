var _resourceTypeId;
var resourceListG;
var _pageNo;
var _resourceTypeCategory;
function eventHnadlerForResource(resourceId, event, resourceName) {
    var eventMessage;
    var url;
    if (event === "publish" || event === "unpublish" || event === "deactivate") {
        url = __NVLI.context + '/resource/' + event + "/action/" + resourceId;
    } else if (event === "activate") {
        url = __NVLI.context + '/resource/record/processing/' + resourceId
    }
    $.ajax({
        url: url,
        type: 'POST',
        success: function (isPublished) {
            var eventType;
            console.log(isPublished + "isPublished")
            if (isPublished) {
                if (event === "publish") {
                    eventType = "published"
                    $("#" + resourceId + "-publish").html('<a class="event-btn btn btn-secondary" data-tooltip="Unpublish Resource" data-resource-id="' + resourceId + '" data-event="unpublish" data-resource-name="' + resourceName + '"> <img src="' + __NVLI.context + '/images/withdraw.png" width="10"></a>');
                } else if (event === "unpublish") {
                    eventType = "unpublished"
                    $("#" + resourceId + "-publish").html('<a class="event-btn btn btn-secondary" data-tooltip="Publish Resource" data-resource-id="' + resourceId + '" data-event="publish" data-resource-name="' + resourceName + '"> <img src="' + __NVLI.context + '/images/publish.png" width="19"></a>');
                } else if (event === "activate") {
                    eventType = "activated"
                    $("#" + resourceId + "-active").html('<a class="event-btn btn btn-secondary" data-tooltip="Stop Record Processing" data-resource-id="' + resourceId + '" data-event="deactivate" data-resource-name="' + resourceName + '"> <img src="' + __NVLI.context + '/images/disable.png" width="19"></a>');
//                    if (_resourceTypeCategory === "Harvest") {
//
//                        $("#" + resourceId + "-status").html('<span class="label label-success">Active</span>');
//                        $("#" + resourceId + "-action").html(' <button data-tooltip="Start Harvesting of Repository" class="action-btn btn btn-secondary" data-resource-id="{{=resource.id}}" data-event="harvest" data-resource-name="{{=resource.resourceName}}"><img src="' + __NVLI.context + '/images/start-process.png"/></button>');
//                    }
                } else if (event === "deactivate") {
                    eventType = "deactivated"
                    $("#" + resourceId + "-active").html('<a class="event-btn btn btn-secondary" data-tooltip="Start Record Processing" data-resource-id="' + resourceId + '" data-event="activate" data-resource-name="' + resourceName + '"> <img src="' + __NVLI.context + '/images/enable.png" width="19"></a>');

//                    if (_resourceTypeCategory === "Harvest") {
//
//                        $("#" + resourceId + "-status").html('<span class="label label-warning">Not Active</span>');
//                        $("#" + resourceId + "-action").html('');
//                    }
                }
                eventMessage = resourceName + " resource " + " " + eventType + " " + "successfully." + "  ";
            } else {
                eventMessage = resourceName + " resource " + " " + event + " " + "failed " + ". Try after some time.";
            }
            bindEvent();
            $("#eventMsg").html('<div class="alert alert-success"> <span id="enewEventMsg">' + eventMessage + '</span></div>');

        }
        ,
        error: function () {
            alertErrorMsg();
        }

    });
}

function fetchAndRenderTableData(pageNo) {
    $.ajax({
        url: __NVLI.context + '/resource/async/register/list',
        data: {searchString: $('#searchString').val(), pageWin: $('#limitFilter').val(), pageNum: pageNo, resourceTypeId: _resourceTypeId},
        type: 'POST',
        success: function (jsonObject) {
            if (jsonObject !== null) {
                if (_.isEmpty(jsonObject.resourceList)) {
                    $("#resource-tbl-body").html('<tr><td colspan="9" class="alert-warning" style="text-align:center"><b>No Data Found</b></td></tr>');
                } else {
                    var articlesTpl = _.template($("#tpl-resource-list").html());

                    var Opts = {
                        "resourceList": jsonObject.resourceList,
                        "recordCount": ((pageNo - 1) * $('#limitFilter').val())
                    };
                    _pageNo = pageNo;
                    resourceListG = jsonObject.resourceList;
                    console.log(Opts);
                    var articleTableBody = articlesTpl(Opts);
                    $("#resource-tbl-body").empty();
                    $("#resource-tbl-body").html(articleTableBody);
                    renderPagination(fetchAndRenderTableData, "resource-tbl-footer", "page-btn", pageNo, jsonObject.totalCount, $('#limitFilter').val(), true, 5, true);
                }
            } else {
                alertErrorMsg();
            }
            bindEvent();
        },
        error: function () {
            alertErrorMsg();
        }
    });
}
function alertErrorMsg() {
    $("#alert-box").html('<div class="alert alert-danger">Something went wrong,try after some time.</div>').fadeIn();
    $("#alert-box").fadeOut(5000);
}

function deleteResource(resourceCode, resourceName) {
    if (confirm("Are you sure you want to delete " + resourceName + " resource?")) {
        var eventMessage;
        $.ajax({
            url: __NVLI.context + '/resource/remove/' + resourceCode,
            type: 'GET',
            success: function (isDeleted) {
                console.log(isDeleted + "isDeleted")
                if (isDeleted) {

                    fetchAndRenderTableData(_pageNo);
                    eventMessage = resourceName + " resource is deleted successfully.";

                } else {
                    eventMessage = resourceName + " resource is not Possible to delete. Try after some time.";
                }
                $("#eventMsg").html('<div class="alert alert-success"> <span id="enewEventMsg">' + eventMessage + '</span></div>');
            }
            ,
            error: function () {
                alertErrorMsg();
            }
        });
    } else {
        return false;
    }
    bindEvent();
}

function eventHnadlerForOR(resourceId, event, resourceName) {
    var eventMessage;
    $.ajax({
        url: __NVLI.context + '/resource/oar/' + event + "/" + resourceId,
        type: 'POST',
        success: function (status) {

            var statusTemp = status.toLowerCase();
            if (statusTemp.indexOf("error") != -1) {
                $("#" + resourceId + "-status btn-group").html('<span class="label label-danger">' + status + '</span>');
            } else {
                $("#" + resourceId + "-info").html('<span class="label label-info">' + status + '</span>');
                $("#" + resourceId + "-action").html('<span data-tooltip="Repository Harvesting Processing..."><img  width="19" src="' + __NVLI.context + '/themes/images/processing.gif"></span>');
            }



            $("#eventMsg").html('');

        }
        ,
        error: function () {
            alertErrorMsg();
        }
    });
    bindEvent();
}

function bindEvent() {
    if (_resourceTypeCategory === "Harvest") {
        $(".action-btn").unbind().bind("click", function (e) {
            eventHnadlerForOR(e.currentTarget.getAttribute("data-resource-id"), e.currentTarget.getAttribute("data-event"), e.currentTarget.getAttribute("data-resource-name"));
        });
    }
    $(".event-btn").unbind().bind("click", function (e) {
        eventHnadlerForResource(e.currentTarget.getAttribute("data-resource-id"), e.currentTarget.getAttribute("data-event"), e.currentTarget.getAttribute("data-resource-name"));
    });
    $(".delete-btn").unbind().bind("click", function (e) {
        deleteResource(e.currentTarget.getAttribute("data-resource-code"), e.currentTarget.getAttribute("data-resource-name"));
    });
}