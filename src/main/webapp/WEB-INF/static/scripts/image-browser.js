/* 
 * Image-Browser Plugin 
 */
var myCropper;
function renderCropper(uploadedImageURL) {
    var $image = document.getElementById('image');
    var croppable = false;
    var cropper = new Cropper($image, {
        preview: '.img-preview',
        ready: function () {
            croppable = true;
        }
    });

    $("#btn-crop").click(function (e) {
        var croppedCanvas;
        var croppedImage;
        if (!croppable) {
            return;
        }
        // Crop
        croppedCanvas = cropper.getCroppedCanvas();
        croppedCanvas.toBlob(function (blob) {
            var formData = new FormData();
            formData.append("uploadfile", blob, $("#btn-crop").attr("data-img-name"));
            uploadCroppedImage(formData, $("#btn-crop").attr("data-img-id"), top.location.origin + $("#btn-crop").attr("data-img-url"));
        });
        // Show
        croppedImage = document.createElement('img');
        croppedImage.src = croppedCanvas.toDataURL();
    });

    $(".button-crop").unbind().bind("click", function (event) {
        $("#btn-crop").attr("data-img-url", event.currentTarget.getAttribute("data-img-url"));
        $("#btn-crop").attr("data-img-id", event.currentTarget.getAttribute("data-img-id"));
        $("#btn-crop").attr("data-img-name", event.currentTarget.getAttribute("data-img-name"));
        cropper.replace(event.currentTarget.getAttribute("data-img-url"));
    });

    return cropper;
}

function uploadImage(data) {
    console.log("uploading");
    $.ajax({
        url: __NVLI.context + "/uploader/image",
        data: data,
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data, textStatus, jqXHR) {
            console.log("success", data);
            fetchAndRenderImages();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("errrr while uploading");
        }
    });
}

function uploadCroppedImage(data, id, imgSrc) {
    console.log("uploading");
    $.ajax({
        url: __NVLI.context + "/uploader/image/r/" + id,
        data: data,
        type: 'POST',
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function (data, textStatus, jqXHR) {
            CKEDITOR.instances['articleText'].insertHtml('<img alt="Image" src="' + imgSrc + '">');
            $('#default-modal-img').modal('hide');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("errrr while uploading");
        }
    });
}

function imageBrowser(options) {

    var settings = {
        id: "default-modal-img",
        title: "Image Picker and Uploader"
    };

    if ($("#" + settings.id).length) {
        $("#" + settings.id).modal("show");
    } else {

        var tpl = _.template($("#tpl-image-browser").html());
        var el = tpl(settings);

        $(el).modal('show');

        // When Modal is visible
        $("#default-modal-img").on('shown.bs.modal', function () {
            fetchAndRenderImages();
        });


        $("#input-new-image").unbind().bind("change", function (e) {
            var formData = new FormData();
            formData.append("uploadfile", $("#input-new-image")[0].files[0]);
            var fileStatus = validateFile($("#input-new-image")[0].files[0]);
            if (fileStatus.isValid) {
                uploadImage(formData);
            } else {
                createAlertModal({title: "Alert", message: fileStatus.message});

            }

        });

        $("#default-modal-img").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
            $("#default-modal-img").remove();
        });

        fetchAndRenderImages();
    }
}

function fetchAndRenderImages() {
    $.ajax({
        url: __NVLI.context + "/api/my/images",
        dataType: 'json',
        type: 'GET',
        success: function (data, textStatus, jqXHR) {
            $("#uploaded-image-grid").empty();
            _.each(data, function (item) {
                var tpl = _.template($("#tpl-uploaded-image").html());
                // User CDN Image Url Instead
                var url = __NVLI.context + "/user-images/" + item.createdBy + "/images/" + item.fileName;
                var opt = $.extend({imgUrl: url}, item);
                var el = tpl(opt);
                $("#uploaded-image-grid").append(el);

            });
            $(".button-insert").click(function (e) {
                insertImage(e.currentTarget.getAttribute("data-img-url"));
                $('#default-modal-img').modal('hide');
            });
            if (_.size(data)) {
                var uploadedImageURL = __NVLI.context + "/user-images/" + data[0].createdBy + "/images/" + data[0].fileName;
                $("#image").attr("src", uploadedImageURL);
                if (!$("#image").hasClass('cropper-hidden')) {
                    $("#btn-crop").attr("data-img-url", uploadedImageURL);
                    $("#btn-crop").attr("data-img-id", data[0].id);
                    $("#btn-crop").attr("data-img-name", data[0].fileName);
                    myCropper = renderCropper();
                } else {
                    myCropper.replace(uploadedImageURL);
                    $(".button-crop").unbind().bind("click", function (event) {
                        $("#btn-crop").attr("data-img-url", event.currentTarget.getAttribute("data-img-url"));
                        $("#btn-crop").attr("data-img-id", event.currentTarget.getAttribute("data-img-id"));
                        $("#btn-crop").attr("data-img-name", event.currentTarget.getAttribute("data-img-name"));
                        myCropper.replace(event.currentTarget.getAttribute("data-img-url"));
                    });
                }
            }

        }
    });
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0)
        return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function validateFile(formData) {
    var supported_media_types = ["image/jpeg", "image/png", "image/gif"];
    if (supported_media_types.indexOf(formData.type) < 0) {
        return {
            "message": "Unsupported media type. Allowed media types are jpeg, png & gif",
            "isValid": false
        }
    } else if (formData.size > 1024000) {
        return {
            "message": "File is too large. Maximum allowed size is 1MB",
            "isValid": false
        }
    }
    return {isValid: true};
}

function insertImage(imgUrl) {
    console.log("Inserting");
//    var imageToInsertURL = $("#result").find('img')[0].src;
    CKEDITOR.instances['articleText'].insertHtml('<img alt="Image" src="' + imgUrl + '">');
    return true;
}

function loadImageForCrop(imgUrl) {
    $("#image").attr("src", imgUrl);
}

$(document).ready(function () {
    $("#btn-browse-image").unbind().bind("click", function (e) {
        imageBrowser();
    });
});