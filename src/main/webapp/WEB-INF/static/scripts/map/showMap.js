/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//on click on list item show marker

$(document).ready(function(){
  
  $(document).on('click', '.list-group .list-group-item', function(e){
    $("#myMap").css("display","none");
    $("#mapLoading").css("display","block");
    $(".list-group a").removeClass("active");
    $(this).addClass('active');
    
    var locationName=$(this).text();
    showMapByLocationName(locationName);
    
    //showMapByLocationLatLon(latitude,longitude);
  });

  try{
    $( "#libraryList a:first-child" ).trigger('click');
    $( "#libraryList a:first-child" ).addClass('active');
  }catch(err){
    console.log("Exe",err);
  }  
      
  $('#myMap').load(function(){
    console.log("Map loaded");
    $("#mapLoading").fadeOut("slow");
    $("#myMap").css("display","block");
    $("#mapLoading").css("display","none");
  });
  
  function showMapByLocationName(locationName){
    var re = /,/gi;
    var para1=locationName.replace(re,"%20");
    var url="https://www.google.com/maps/embed/v1/search?key=AIzaSyBqFbNyBlu9i5DXYu9Mn9T135DtKqMf-eA&q="+para1;
    var $iframe = $('#myMap');
    if ( $iframe.length ) {
      $iframe.attr('src',url);
      return false;
    }
  }
  
  function showMapByLocationLatLon(latitude,longitude){
    var url="https://www.google.com/maps/embed/v1/search?key=AIzaSyBqFbNyBlu9i5DXYu9Mn9T135DtKqMf-eA&q="+latitude+longitude;
    var $iframe = $('#myMap');
    if ( $iframe.length ) {
      $iframe.attr('src',url);
      return false;
    }
  }

});
      

