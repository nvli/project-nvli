/* global _ */

var MIN_SELECTED_CATEGORY = 2;
/**
 * This function resizes the page wrapper container dynamically with window
 * height by setiing the min-height style property.
 *
 * @returns {undefined}
 */
function resizeWrapper() {
    var wrapper_height = window.innerHeight - 150;
    $("#wrapper").css('min-height', wrapper_height);
    $("#wrapper").css('overflow', "hidden");
}

$(document).ready(function () {
    // Invoking Auto Resizing of Wrapper
    //    resizeWrapper();


    // User Role Page Event Binding
    $(".action-delete-role").unbind().bind("click", function (event) {
        deleteUserRole(event.currentTarget.getAttribute("data-role-id"));
    });
    // Fetch available themes
});
//$(window).resize(function () {
//    resizeWrapper();
//});

/////////////////////// USER ROLE /////////////////////////////////////////////

function deleteUserRole(roleId) {
    // Post Ajax Call for deleting record
    $.ajax({
        url: window.__NVLI.context + "/user/delete/role/" + roleId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            handleDeleteUserRole(response);
            fetchRoles();
        }
    });
}

function handleDeleteUserRole(response) {
    if (response.success) {
        showToastMessage(response.message, "success");
    } else {
        showToastMessage(response.message, "error");
    }

}

///////////////////  PERSISTANT MENU  /////////////////////////////////////////

function maintainMenuState() {
    var url_pathname = top.location.pathname;

    if (url_pathname.indexOf("/manage/") < 0 && url_pathname.indexOf("article") >= 0) {
        url_pathname = __NVLI.context + "/cs/show-articles";
    } else if (url_pathname.indexOf("/manage/") >= 0 && url_pathname.indexOf("article") >= 0) {
        url_pathname = __NVLI.context + "/cs/manage/articles";
    }

    var sidebar = $("#sidebar-menus");
    var selected_menu_link, collapse_level_0;

    selected_menu_link = $(sidebar).find(".menu_link[href='" + url_pathname + "']");

    parent = selected_menu_link.parent();
    collapse_level_0 = selected_menu_link.parent().parent();
    $(parent).addClass('active');

    $(collapse_level_0).attr('aria-expanded', true);

    if (parseInt($(collapse_level_0).parent().attr('data-level'))) {
        $(collapse_level_0).parent().addClass('in');
        $(collapse_level_0).parent().parent().parent().addClass('in');
        var heading1 = $(sidebar).find("a[href='#" + $(collapse_level_0).parent().attr('id') + "']");
        var heading2 = $(sidebar).find("div[href='#" + $(collapse_level_0).parent().parent().parent().attr('id') + "']");

        $(heading1).removeClass('collapsed');
        $(heading2).removeClass('collapsed');
    } else {
        var heading = $(sidebar).find("div[href='#" + $(collapse_level_0).parent().parent().attr('id') + "']");
        $(heading).removeClass('collapsed');
        $(collapse_level_0).parent().parent().addClass('in');
    }

}

/////////////////////////   UTIL   ////////////////////////////////////////////

/**
 * Method to show toast message
 * @param {String} message
 * @param {String} toastType (success|warning|error|info)
 * @returns {undefined}
 */
function showToastMessage(message, toastType) {
    setTimeout(function () {
        toastr.options = {
            closeButton: true,
            progressBar: false,
            showMethod: 'slideDown',
            timeOut: 5000
        };
        toastr[toastType]("NVLI", message);
    }, 300);
}

function getMyTheme(callback) {
    $.ajax({
        url: __NVLI.context + "/api/me/theme",
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            callback(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function setCurrentTheme() {
    $.ajax({
        url: __NVLI.context + "/api/me/theme",
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            applyTheme(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getDateString() {
    var now = new Date();
    return now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
}

function renderThemeDateInfo(theme) {
    var themeName = theme.themeName;
    if (themeName !== "Default Theme") {
        var tplThemeInfo = _.template($("#tpl-theme-date-info").html());
        var dd = new Date(theme.themeActivationDate);
        var dateString = dd.toDateString();
        var opts = {
            themeName: theme.themeName,
            themeDate: dateString
        };
        var $el = tplThemeInfo(opts);
        $("#select_text").hover(function () {
            $("#hoverText").html($el);
        }, function () {
            $('#hoverText').empty();
        });
    } else {
        $("#select_text").unbind('mouseenter').unbind('mouseleave');
    }
}

function applyTheme(theme) {
    var currenTheme = localStorage.getItem("selectedTheme");
    localStorage.setItem("selectedTheme", theme.themeCode);
    var src = "#current-theme-style";
    var target = __NVLI.context + "/uploadedThemes/" + theme.themeCode + "_theme/styles/" + theme.themeCode + ".css";
    $(src).attr("href", target);
    $("#selected-theme").html('<span class="fa fa-image" aria-hidden="true"></span> &nbsp;' + theme.themeName);
    if ($("#select_text").length) {
        renderThemeDateInfo(theme);
    }
}

function saveTheme(themeId) {
    $.ajax({
        url: __NVLI.context + "/user/save/theme/" + themeId,
        type: 'post',
        success: function (response) {
            if (typeof response === "object") {
                applyTheme(response);
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}
// Fetch User Notifications
function fetchNotifsHeader(page, max) {
    $.ajax({
        type: 'GET',
        url: __NVLI.context + "/user/notifs/" + page + "/" + max,
        success: function (response) {
            renderNotifs(response, "#notif-items");
        },
        error: function (err) {
            console.log(err);
        }
    });
}
// Fetch User Notifications
function fetchNotifs(page, max) {
    $.ajax({
        type: 'GET',
        url: __NVLI.context + "/user/notifs/" + page + "/" + max,
        success: function (response) {
            if (response.length === 0) {
                if (page === 1)
                    $("#notif-container").append($("#tpl-notif-not-found").html());
                $("#btn-show-more-notifs").hide();
                return false;
            }
            if (!_.isEmpty($("#notif-container"))) {
                var target = "#notif-container";
                $("#notifNotFound").remove;
                renderNotifs(response, target);
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

// Fetch Unread Notifs count
function fetchUnreadNotifCount() {
    $.ajax({
        type: 'GET',
        url: __NVLI.context + "/user/notifs/unread",
        success: function (response) {
            if (response.unread_count) {
                $(".notification-count").show();
                $(".notification-count").html(response.unread_count);
            } else {
                $(".notification-count").hide();
            }
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function loadTarget(targetURL) {
    if (!_.isEmpty(targetURL)) {
        top.location.href = targetURL;
    }

}

// Render User Notification of Header
function renderNotifs(notifs, target) {
    _.each(notifs, function (notif) {
        var notifDateTime = new Date(notif.notifactionDateTime);

        var tpl = _.template($("#tpl-notif-item").html());
        var opts = {
            notif_message: notif.notificationMessage,
            notif_time: notifDateTime.toLocaleString(),
            is_read: notif.isReadByReceiver,
            notif_id: notif.objectId,
            target_url: __NVLI.context + "" + notif.notificationTargetURL
                    // notif_sender_profile_pic: notif.senderProfilePicPath
        };
        var el = tpl(opts);
        $(target).append(el);
    });
    $(".notif-item").unbind('click').bind("click", function (e) {
        if (!$(e.currentTarget).hasClass("noti_read")) {
            // ajax call to mark this notification as read.
            var notif_id = e.currentTarget.getAttribute("data-notif-id");
            $.ajax({
                url: __NVLI.context + "/user/notifs/read/" + notif_id,
                type: 'GET',
                success: function (isRead) {
                    fetchUnreadNotifCount();
                    $(".notif-item[data-notif-id='" + notif_id + "']").addClass("noti_read");
                    loadTarget(e.currentTarget.getAttribute("data-target-url"));
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        } else {
            loadTarget(e.currentTarget.getAttribute("data-target-url"));
        }
    });
    $("#btn-show-more-notifs").unbind().bind("click", function (e) {
        var currentPage = parseInt(e.currentTarget.getAttribute('data-current-page'));
        $("#btn-show-more-notifs").attr('data-current-page', currentPage + 1);
        fetchNotifs(currentPage + 1, parseInt(e.currentTarget.getAttribute('data-max')));
    })
}

function connect(uid) {
    var socket = new SockJS(__NVLI.context + '/nvliNotificationsEndPoint');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/notifs/user-notifications/' + uid, function (messageTemplate) {
            showToastMessage(messageTemplate.body, "success");
            fetchUnreadNotifCount();
        });
    });
}

function checkLoginStatus(callback) {
    $.ajax({
        url: __NVLI.context + "/api/check-login-status",
        success: function (response) {
            callback(response);
        }
    });
}

function fetchThemeByID(themeId, callback) {
    $.ajax({
        url: __NVLI.context + "/api/get/theme/" + themeId,
        success: function (response) {
            callback(response);
        }
    });
}

function checkRequiredProfileItems() {
    $.ajax({
        url: __NVLI.context + "/api/check-required-profile-settings",
        success: function (response, textStatus, jqXHR) {
            console.log(response);
            if (response.data.hasLanguageExpertise || response.data.hasResourceExpertise) {
                showRequiredProfileInfo(response.data);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // Do Nothing
        }

    });
}

function showRequiredProfileInfo(data) {
    var requestQueryArray = {};
    var a = document.createElement('a');
    a.href = top.location.href;
    var query = a.search.substring(1, a.search.length).split('&');
    _.each(query, function (item) {
        var itemArray = item.split('=');
        requestQueryArray[itemArray[0]] = itemArray[1];
    });
    var forceUser = false;
    _.each(data, function (value, key) {
        if (!value) {
            forceUser = true;
        }
    });

    if (!_.isEmpty(requestQueryArray['progress']) && parseInt(requestQueryArray.progress)) {
        forceUser = false;
    }
    if (forceUser) {
        var tpl = _.template($("#tpl-profile-check").html());
        var opts = data;
        var overlay = tpl(opts);
        $('body').append(overlay);
    }
}

function populateRolesMenu() {
    var request = new XMLHttpRequest();
    request.open("GET", __NVLI.context + "/api/roles", true);
    request.onreadystatechange = function () {
        if (request.readyState != 4 || request.status != 200)
            return;
        var response = JSON.parse(request.responseText)
        _.each(response, function (role) {
            var li = document.createElement('li');
            li.className = "panel-headli";
            li.style.paddingLeft = '15px';
            var link = document.createElement('a');
            link.className = "menu_link";
            link.href = __NVLI.context + "/user/manage/role/" + role.roleId
            link.text = role.roleName;
            li.appendChild(link);
            $("#sidebar-usermanagement").append($(li));
        });
        maintainMenuState();
    }
    request.send();
}

function _showAlertBox(type, id, title, message) {
    var template = _.template($("#tpl-alert").html());
    var opts = {
        modalId: id,
        modalType: type,
        title: title,
        message: message
    };

    var el = template(opts);
    $('body').append(el);
    $("#" + id).modal('show');

}

//////////////////// WIDGET Manager and Management  ////////////////////////////

function showWidgetUploaderForm() {

}

/////////////////// ./WIDGET Manager and Management  ///////////////////////////

//check Resources Without Lib User
function checkResourcesWithoutLibUser(roleId) {
    $.ajax({
        url: __NVLI.context + "/curation-activity/check-Resources-Without-LibUser-Register",
        dataType: 'json',
        success: function (response) {
            if (!response.status) {
                //if register is not empty then show pop-up
                var template = _.template($("#tpl-resources-without-lib-user").html());
                var opts = {
                    resourcesWithoutLibUsersJSONArray: response.resourcesWithoutLibUsersJSONArray
                };
                var el = template(opts);
                $('body').append(el);

                //go to Register for assign library user
                $("#gotoRegisterBtn").unbind().bind("click", function () {
                    window.location.href = __NVLI.context + "/user/manage/role/" + roleId;
                    localStorage.setItem("resourcesWithoutLibUserRemindMeLater", true);
                });

                //remind me later for assign library user
                $("#remindMeLaterBtn").unbind().bind("click", function (e) {
                    localStorage.setItem("resourcesWithoutLibUserRemindMeLater", true);
                    $("#resourcesWithoutLibUser").remove();
                });
            }
        }
    });
}

function __initMain(response) {
    if (response.status === true) {
        localStorage.setItem("logged_in_status", response);
        connect(response.uid);
        fetchUnreadNotifCount();
        fetchNotifsHeader(1, 10);
        $(".header-notif-btn").unbind().bind("click", function (e) {
            $("#notif-items").empty();
            fetchNotifsHeader(1, 10);
        });
        getPersonalThemes();
        var theme = localStorage.getItem('previewTheme');
        if (null !== theme) {
            activateThemePreview(theme, applyPreviewTheme);
        }

        if ($("#select_text").length) {
            getMyTheme(renderThemeDateInfo);
        }
    } else {
        __initThemeTimer();
        connectThemeScheduler();
    }
    bindThemeSelection();
}

function bindThemeSelection() {
    $(".theme-item-link").click(function (e) {
        var themeId = e.currentTarget.getAttribute("data-theme-id");
        saveTheme(themeId);
        fetchThemeByID(themeId, applyTheme);
    });
}

function uploadWidget(formData) {
    $.ajax({
        url: __NVLI.context + "/widgets/upload",
        type: 'POST',
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function (response) {
            if (undefined !== response.status && response.status === 'Failed') {
                var tplErrMsg = _.template($("#tpl-widget-msg-err").html());
                var elErr = tplErrMsg({
                    message: response.message
                });
                showToastMessage(elErr, "error");
            } else {
                $("#frm-widget-uploader")[0].reset();
                var tplSuccessMsg = _.template($("#tpl-widget-msg-success").html());
                showToastMessage(tplSuccessMsg(), "success");
                $("#widget-uploader-container").hide();
                $("#btn-show-widget-uploader").show();
                fetchAllWidgets();
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            var tplErrMsg = _.template($("#tpl-widget-msg-err").html());
            var elErr = tplErrMsg();
            showToastMessage(elErr, "error");
        }
    });
}

function fetchAllWidgets() {
    $.get(__NVLI.context + "/widgets/list", {}, function (res) {
        populateWidgets(res);
        $("#widgetId").val(_.size(res) + 1 + "_" + (new Date()).getTime());
    });
}

function populateWidgets(data) {
    _.each(data, function (widgetData, i) {
        var tpl = _.template($("#tpl-widget-item").html());
        widgetData['count'] = i + 1;
        var el = tpl(widgetData);
        $("#widget-list-container").append(el);
    });
}

function getPersonalThemes() {

    $.ajax({
        url: __NVLI.context + "/api/get/personal/themes",
        type: 'GET',
        dataType: 'json',
        success: function (data, i) {
            renderThemeList(data);
        }
    });
}

function getFeturedTheme() {
    var now = new Date();
    var dateString = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
    $.ajax({
        url: __NVLI.context + "/api/get/featured/themes",
        data: {
            themeDate: dateString
        },
        type: 'GET',
        dataType: 'json',
        success: function (themes) {
            if (themes.length > 1) {
                __NVLI['featuredThemes'] = themes;
                var themeTimer = setInterval(handleMultipleTheme, 3600000);
            } else {
                if (!_.isEmpty(theme[0])) {
                    __NVLI['featuredThemes'] = theme[0];
                    applyTheme(theme[0]);
                } else {
                    __NVLI['featuredThemes'] = {};
                }
            }

        },
        error: function (err) {
            console.log(err);
        }
    });
}

function getThemeById(themeId, callback, applyThemeCallback) {
    $.ajax({
        url: __NVLI.context + "/api/get/theme/" + themeId,
        type: 'GET',
        dataType: 'json',
        success: function (theme) {
            callback(theme, applyThemeCallback);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function renderClosePreview() {
    var el = document.createElement('div');
    el.style.position = "absolute";
    el.style.top = "5px";
    el.style.left = "120px";
    el.style.display = "block";
//    el.style.width = "100%";
    el.style.textAlign = "center";
    el.id = "btn-close-preview";
    var button = document.createElement('button');
    button.className = "btn btn-sm btn-primary-outline";
    button.innerHTML = "Close theme Preview <span class='fa fa-close'></span>";
    el.appendChild(button);
    $('body').append(el);
    $(button).click(function () {
        localStorage.removeItem('previewTheme');
        setCurrentTheme();
        $("#btn-close-preview").remove();
    });
}


function activateThemePreview(theme, applyThemeCallback) {
    // if previewTheme does not exist in LocalStorage Save the theme info with key and remember older theme
    if (typeof theme !== "string") {
        localStorage.setItem("previewTheme", JSON.stringify(theme));
    } else {
        localStorage.setItem("previewTheme", theme);
    }

    applyThemeCallback(theme);
    renderClosePreview();
    // or retirieve the theme with the key
    // call applyTheme(retievedTheme)
    // render a "close theme button on page"
    // bind event to the button to deactivate preview and delete the corresponding theme from localstorage and re apply previous theme from selectedTheme key from localstorage
}

function applyPreviewTheme(previewTheme) {
    var src = "current-theme-style";
    var theme = JSON.parse(previewTheme);
    var target = __NVLI.context + "/uploadedThemes/" + theme.themeCode + "_theme/styles/" + theme.themeCode + ".css";
    document.getElementById(src).setAttribute("href", target);

}

function renderThemeList(data) {
    $("#drop_down_menu").empty();

    _.each(data, function (theme) {
        var tpl = _.template($("#tpl-header-theme-list").html());
        var el = tpl(theme);
        $("#drop_down_menu").append(el);
    });
    bindThemeSelection();
}

function loadTodaysGlobalTheme() {
    getFeturedTheme();
}


function connectThemeScheduler() {
    var socket = new SockJS(__NVLI.context + '/nvliThemeChangerEndPoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/themeBroker/themepush', function (messageTemplate) {
            loadTodaysGlobalTheme();
        });
    });
}

function manualSubscribe() {
    $.ajax({
        url: __NVLI.context + "/api/subscribeTheme",
        type: 'GET',
        dataType: 'json',
        success: function () {
            // Do Nothing
        }
    });
}

function __bindEvents() {
    // Show or Hide Widget Uploader Section
    if ($("#btn-show-widget-uploader").length) {
        fetchAllWidgets();
        $("#btn-show-widget-uploader").unbind().bind('click', function (e) {
            $(this).hide();
            $("#widget-uploader-container").show();
        });
    }

    // Binding Submit Event
    if ($("#frm-widget-uploader").length) {
        $("#frm-widget-uploader").bind("submit", function (e) {
            e.preventDefault();
            // TODO : show upload progress bar.
            var formData = new FormData($("#frm-widget-uploader")[0]);
            uploadWidget(formData);
        });
    }

    if ($("#select_text").length) {
        var themeString = localStorage.getItem(getDateString());
        if (null !== themeString) {
            var theme = JSON.parse(themeString);
            renderThemeDateInfo(theme);
        }
    }
}

function __initThemeTimer() {
    var now = new Date();
    var millisTillMidNight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 24, 0, 0, 0) - now;
    if (millisTillMidNight < 0) {
        millisTillMidNight += 86400000; // it's after 24:00 Hours.
    }
    setTimeout(function () {
        manualSubscribe();

        setInterval(function () {
            manualSubscribe();
        }, 86400000);
    }, millisTillMidNight);
}
function initLocalization() {
    $("#lanSelect").html(localStorage.getItem('clicked_prev_lang'));
    $(".localisation").click(function () {
        localStorage.setItem('clicked_prev_lang', $(this).text());
        $("#lanSelect").html($(this).text());
        document.getElementById($(this).attr("id")).href = "?language=" + $(this).attr("id");
    });
}

$(document).ready(function () {
    // Checking in logged in status of user
    checkLoginStatus(__initMain);
    __bindEvents();
    if ($("#sidebar-usermanagement").length) {
        populateRolesMenu();
    }

    if ($("#sidebar-menus").length) {
        maintainMenuState();
    }

    initLocalization();
});
