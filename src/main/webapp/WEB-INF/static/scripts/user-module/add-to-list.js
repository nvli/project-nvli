/*
 * This js file adds functionalities to add favourite searched items / records to user list
 *
 */
(function ($, APP) {

    $("#createListBtn").click(function () {
        $("#createNewListDiv").show();
        $("#addToListDiv").hide();
    });

    $("#resetBtn1").click(function () {
        resetAddListDiv();
    });

    $("#resetBtn2").click(function () {
        resetNewListDiv();
    });

    function resetAddListDiv() {
        $("#addToListSelect").val('0');
    }

    function resetNewListDiv() {
        $("input[name=accessRadio][value=1]").prop('checked', true);
        $("#newListTxt").val("");
    }

    $("#cancelBtn").click(function () {
        resetNewListDiv();
        resetAddListDiv();
        $("#createNewListDiv").hide();
        $("#addToListDiv").show();
    });


    $("#AddToExistingListBtn").click(function () {
        var listId = $("#addToListSelect").val();
        if (listId != 0) {
            $.ajax({
                url: APP.context + "/search/user/add/to/existing/list/" + '${uniqueObjectIdentifier}' + "/" + listId,
                cache: false,
                success: function (data) {
                    if (data == 0) {
                        alert("some error occured while saving");
                    } else {
                        alert("Added to list");
                        window.close();
                    }
                },
                error: function () {
                    alert("error");
                }
            });
        } else {
            alert("Please Select Any List");
        }

    });

    $("#addToNewListBtn").click(function () {
        var listname = $("#newListTxt").val();
        var listAccess = -1;
        var selectedRaio = $("input[type='radio'][name='accessRadio']:checked");
        if (selectedRaio.length <= 0) {
            alert("Please Select Access Type");
        } else if (listname.length <= 0) {
            alert("Please Provide List Name");
        } else {
            listAccess = selectedRaio.val();
            $.ajax({
                url: APP.context + "/search/user/add/to/new/list/" + '${uniqueObjectIdentifier}' + "/" + listname + "/" + listAccess,
                cache: false,
                success: function (data) {
                    if (data == 0) {
                        alert("some error occured while saving");
                    } else {
                        alert("Added to list");
                        window.close();
                    }
                },
                error: function () {
                    alert("error");
                }
            });

        }

    });

    $(document).ready(function () {
        $("#createNewListDiv").hide();
        $("#addToListDiv").show();
    });
}(jQuery, __NVLI));