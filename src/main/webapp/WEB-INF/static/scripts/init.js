function applyFeaturedTheme(featuredThemes) {
    var src = "current-theme-style";

    if (typeof featuredThemes == "string") {
        // converting themes JSON string into JSON Object
        featuredThemes = JSON.parse(featuredThemes);
    }
    __NVLI['featuredThemes'] = featuredThemes;
    if (featuredThemes.length > 1) {
        localStorage.setItem("featuredThemes", JSON.stringify(featuredThemes)); // String
        var themeTimer = setInterval(handleMultipleThemeInINIT, 3600000);
    }
    if (featuredThemes.length > 0 && null !== featuredThemes[0]) {
        var now = new Date();
        var indexOfTheme = now.getHours() % featuredThemes.length;
        var theme = featuredThemes[indexOfTheme];
        localStorage.removeItem(getDateString());
        localStorage.setItem(getDateString(), JSON.stringify(theme));
        var target = __NVLI.context + "/uploadedThemes/" + theme.themeCode + "_theme/styles/" + theme.themeCode + ".css";
        document.getElementById(src).setAttribute("href", target);
    } else {
        localStorage.removeItem(getDateString());
        var target = __NVLI.context + "/uploadedThemes/default_theme/styles/default.css";
        document.getElementById(src).setAttribute("href", target);
    }

}

function handleMultipleThemeInINIT() {
    var now = new Date();
    if (__NVLI.featuredThemes !== undefined && __NVLI.featuredThemes.length > 0)
        var themes = __NVLI.featuredThemes;
    if (undefined !== themes) {
        var indexOfTheme = now.getHours() % themes.length;
        var themeToActivate = themes[indexOfTheme];
        localStorage.setItem("selectedTheme", JSON.stringify(themeToActivate));
        applyTheme(themes[indexOfTheme]);
    }
}

function getDateString() {
    var now = new Date();
    return now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
}

function featuredThemeExists(url, callback) {
    var dateString = getDateString();
    var http = new XMLHttpRequest();
    http.open('GET', url + "?themeDate=" + dateString, true);
    http.onreadystatechange = function () {
        if (http.response !== "" && this.readyState == this.DONE) {
            callback(http.response);
        }
    };
    http.send();
}

featuredThemeExists(__NVLI.context + "/api/get/featured/themes", applyFeaturedTheme);
