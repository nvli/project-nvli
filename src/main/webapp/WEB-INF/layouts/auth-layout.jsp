<%--
    Document   : default-user-home
    Created on : Apr 1, 2016, 1:01:49 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>
        <link href="${context}/themes/css/bootstrap.css" rel="stylesheet"/>
        <link href="${context}/themes/css/bootstrap-social.css" rel="stylesheet"/>
        <link href="${context}/themes/css/style.css" rel="stylesheet"/>
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link href="${context}/themes/css/font-awesome.css" rel="stylesheet"/>
        <link href="${context}/themes/css/animate.css" rel="stylesheet"/>
        <link href="${context}/components/tooltip/tooltip.css" rel="stylesheet"/>

        <script src='https://www.google.com/recaptcha/api.js'></script>

        <script src="${context}/themes/js/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap.min.js"></script>
        <script src="${context}/themes/js/custom.js"></script>
        <script src="${context}/components/jquery/jquery.slimscroll.min.js"></script>
        <script src="${context}/components/underscore/underscore.min.js"></script>
        <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body>
        <div class="container-fluid">
            <tiles:insertAttribute name="header" ignore="true" />
            <tiles:insertAttribute name="body" ignore="true" />
        </div>
        <div class="clear22"></div>
        <!-- Footer -->
        <tiles:insertAttribute name="footer" ignore="true" />
        <!-- ./Footer -->
        <!-- s result content container -->
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div style="margin-bottom:0px;" class="clearfix"></div>
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>
    </body>
</html>