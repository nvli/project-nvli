<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <title><tiles:insertAttribute name="title"/></title>
        <link href="${context}/themes/css/bootstrap.css" rel="stylesheet"/>
        <link href="${context}/themes/css/style.css" rel="stylesheet"/>
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link href="${context}/themes/css/font-awesome.css" rel="stylesheet"/>
        <link href="${context}/themes/css/animate.css" rel="stylesheet"/>

        <script src="${context}/themes/js/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="${context}/themes/css/sly/horizontal2.css">
        <script src="${context}/themes/js/sly/modernizr.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/assamese.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/bengali.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/gujarati.js"></script>
        <script type="text/javascript"src="${context}/themes/js/language/hindi.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/marathi.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/oriya.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/punjabi.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/tamil.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/switchLanguage.js"></script>
        <script type="text/javascript" src="${context}/themes/js/sly/plugins.js"></script>
        <script type="text/javascript" src="${context}/themes/js/sly/sly-unminified-displayInfo.js"></script>
        <!--<script type="text/javascript" src="${context}/themes/js/sly/horizontal.js"></script>-->

        <script src="${context}/themes/js/custom.js"></script>
        <script type="text/javascript">
            <%--   $(document).ready(function(){
                   $("#black_theme").click(function(){
                       $('link[href="${context}/themes/css/light.css"]').attr('href','${context}/themes/css/dark.css');
                   });

                $("#default_theme").click(function(){
                    $('link[href="${context}/themes/css/dark.css"]').attr('href','${context}/themes/css/light.css');
                });

            });--%>
        </script>
       <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="body_bg">
        <div class="container-fluid">
            <tiles:insertAttribute name="header"/> 
            <div class="m-a-2 container-fluid"> 

                <div class="m-t-2" id="main">
                    <div class="clear"></div>
                    <div class="row">
                        <div class="clearfix equalheight">  

                            <form:form  class="form-group"  id="searchForm" commandName="searchForm" name="searchForm">


                                <tiles:insertAttribute name="search"/>

                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <tiles:insertAttribute name="footer"/>
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>
    </body>
</html>