<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<c:set var="appURL" value="${exposedProps.applicationUrl}" />
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<!-- Update your html tag to include the itemscope and itemtype attributes. -->
<html itemscope itemtype="http://schema.org/Article">
    <head>
        <title><tiles:insertAttribute name="title"/></title>
        <!-- Schema.org markup for Google+ -->
        <!-- Twitter Card data -->
        <!-- Open Graph data for facebook-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Checkout this article on National Virtual Library of India." />
        <c:if test="${not empty record.title}">
            <meta property="og:title" content="${record.title[0]}"/>
            <meta name="twitter:title" content="${record.title[0]}">
            <meta itemprop="name" content="${record.title[0]}">
        </c:if>
        <c:if test="${not empty record.description}">
            <meta property="og:description" content="${fn:replace(record.description[0], newLineChar, "")}"/>
            <meta name="twitter:description" content="${fn:replace(record.description[0], newLineChar, "")}">
            <meta itemprop="description" content="Th${fn:replace(record.description[0], newLineChar, "")}">
        </c:if> 
        <c:set var="paths" value="${record.paths}"/>
        <c:forEach items="${paths}" var="path" end="0">            
            <c:set var="testpath" value="${fn:toLowerCase(path)}"/>
            <c:choose>
                <c:when test="${fn:endsWith(testpath, '.jpg') || fn:endsWith(testpath, '.jpeg' || fn:endsWith(testpath, '.png')) }">
                    <meta property="og:image" content="${fn:replace(path, " ","%20")}" />
                    <meta name="twitter:image" content="${fn:replace(path, " ","%20")}">
                    <meta itemprop="image" content="${fn:replace(path, " ","%20")}">
                </c:when>
                <c:otherwise>
                    <meta property="og:image" content="${appURL}/themes/images/logo/Nvli-logo-home.png"/>
                    <meta name="twitter:image" content="${appURL}/themes/images/logo/Nvli-logo-home.png">
                    <meta itemprop="image" content="${fn:replace(path, " ","%20")}">
                </c:otherwise>
            </c:choose>
        </c:forEach>        
        <meta property="og:site_name" content="National Virtual Library of India" />
        <meta name="twitter:site" content="@National Virtual Library of India" />                    
        <meta name="twitter:card" content="${record.publisher[0]}" />        
        <!--<meta name="twitter:creator" content="${record.creator[0]}"/>-->                    
        <meta property="og:image:alt" content="${record.title[0]}"/>
        <meta property="og:url" content="${appURL}/search/preview/${record.recordIdentifier}">
        <meta property="fb:app_id" content="${exposedProps.fbAppID}" />
        <meta property="og:type" content="Article"/>        
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <link href="${context}/themes/css/bootstrap.css" rel="stylesheet"/>
        <link href="${context}/themes/css/style.css" rel="stylesheet"/>
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link href="${context}/themes/css/font-awesome.css" rel="stylesheet"/>
        <link href="${context}/themes/css/animate.css" rel="stylesheet"/>
        <link href="${context}/themes/css/bootstrap-social.css" rel="stylesheet"/>
        <link rel="stylesheet" href="${context}/themes/css/bootstrap-datatable/datatable.pagination.css">
        <link rel="stylesheet" href="${context}/themes/css/bootstrap-datatable/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="${context}/themes/css/map.css">
        <link rel="stylesheet" type="text/css" href="${context}/themes/css/iviewer/jquery.iviewer.css"/>
        <link rel="stylesheet" type="text/css" href="${context}/styles/css/jssocials.css"/>
        <link rel="stylesheet" type="text/css" href="${context}/styles/css/rateit.css"/>

        <link href="${context}/themes/cropper/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="${context}/themes/cropper/css/jquery.awesome-cropper.css">
        <script src="${context}/themes/js/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/map.js"></script>
        <script type="text/javascript" src="${context}/themes/js/jquery-ui.js"></script>
        <script type="text/javascript" src="${context}/themes/js/iviewer/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="${context}/themes/js/iviewer/jquery.iviewer.js"></script>
        <script type="text/javascript" src="${context}/themes/js/iviewer/lightbox_iviewer.js" ></script>
         <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="body_bg">
        <div class="container-fluid" id="box">
            <tiles:insertAttribute name="header"/>
            <tiles:insertAttribute name="breadcrump"/>
            <!--            <div class="m-t-3" style="z-index:-1;">-->
            <!-- search result content starts here -->
            <div class="container-fluid m-l-3 m-r-3 m-t-3" id="main">
                <div class="clear" style="margin-bottom:20px"></div>
                <!--                <div class="row clearfix card">-->
                <tiles:insertAttribute name="content"/>
                <!--                </div>-->
            </div>
            <!--            </div>-->
            <!-- s result content container -->
        </div>
        <div class="clear22"></div>
        <div style="margin-bottom:20px;" class="clearfix"></div>
        <div style="margin-bottom:0px;" class="clearfix"></div>
        <tiles:insertAttribute name="footer"/>



        <script src="${context}/themes/js/bootstrap-datatable/jquery.dataTables.min.js"></script>
        <script src="${context}/themes/js/bootstrap-datatable/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" src="${context}/scripts/libs/jssocials.js"></script>
        <script type="text/javascript" src="${context}/scripts/libs/jquery.rateit.js"></script>
        <script src="${context}/themes/js/clipboard/clipboard.min.js"></script>
        <script src="${context}/themes/js/masonry.pkgd.min.js"></script>
        <script src="${context}/themes/cropper/components/imgareaselect/scripts/jquery.imgareaselect.js"></script>
        <script src="${context}/themes/cropper/build/jquery.awesome-cropper.js"></script>
        <script src="${context}/themes/js/custom.js"></script>
        <script type="text/javascript" src="${context}/themes/js/iviewer/jquery-migrate-1.2.1.js" ></script>

        <script src="${context}/components/underscore/underscore.min.js"></script>
        
        <!--<script type="text/javascript">
    
            $("#black_theme").click(function(){
                $('link[href="${context}/themes/css/light.css"]').attr('href','${context}/themes/css/dark.css');
            });
    
            $("#default_theme").click(function(){
                $('link[href="${context}/themes/css/dark.css"]').attr('href','${context}/themes/css/light.css');
            });
            $('#file').awesomeCropper(
            { width: 200, height: 200, debug: true }
        );
        </script>-->
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>
    </body>
</html>

