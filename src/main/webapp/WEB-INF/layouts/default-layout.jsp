<%--
    Document   : default-layout
    Created on : Apr 1, 2016, 5:27:19 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
    Author     : Priya Bhalerao
    Author     : Madhuri Dhone
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><tiles:insertAttribute name="title"/></title>
        <link rel="stylesheet" href="${context}/themes/css/bootstrap.css"/>
        <link rel="stylesheet" href="${context}/themes/css/style.css"/>
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link href="${context}/components/tooltip/tooltip.css" rel="stylesheet"/>
        <link rel="stylesheet" href="${context}/themes/css/font-awesome.css"/>
        <link href='https://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="${context}/components/toastr/toastr.min.css">
        <link rel="stylesheet" href="${context}/themes/css/animate.css"/>
        <link rel="stylesheet" href="${context}/themes/css/bootstrap-datatable/datatable.pagination.css">
        <link rel="stylesheet" href="${context}/themes/css/bootstrap-datatable/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="${context}/themes/css/map.css">
        <link href="${context}/themes/cropper/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="${context}/themes/cropper/css/jquery.awesome-cropper.css">
        <script src="${context}/themes/js/sly/modernizr.js"></script>
        <script src="${context}/themes/js/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap.min.js"></script>
        <script src="${context}/themes/js/bootstrap-datatable/jquery.dataTables.min.js"></script>
        <script src="${context}/themes/js/bootstrap-datatable/dataTables.bootstrap.min.js"></script>
        <script src="${context}/themes/js/map.js"></script>
        <script src="${context}/themes/js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/custom.js"></script>
        <script src="${context}/themes/js/news-ticker/jquery.easing.min.js"></script>
        <script src="${context}/themes/js/news-ticker/jquery.easy-ticker.min.js"></script>
        <script src="${context}/themes/cropper/components/imgareaselect/scripts/jquery.imgareaselect.js"></script>
        <script src="${context}/themes/cropper/build/jquery.awesome-cropper.js"></script>
        <%-- NOTE! Please DO Not Remove following Component js --%>
        <script src="${context}/components/underscore/underscore.min.js"></script>
        <script src="${context}/components/jquery/jquery.slimscroll.min.js"></script>
        <script src="${context}/components/toastr/toastr.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/themes/js/ssi-uploader.js"></script>
       <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="home_bg">
        <div class="container-fluid">
            <tiles:insertAttribute name="header" ignore="true"/>
            <tiles:insertAttribute name="body" ignore="true"/>
        </div>
        <tiles:insertAttribute name="footer"  ignore="true"/>
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>        
    </body>
</html>