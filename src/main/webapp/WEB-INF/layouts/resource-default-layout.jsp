<%-- 
    Document   : resource-default-layout
    Created on : Apr 25, 2016, 12:48:27 PM
    Author     : Sujata Aher
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="${context}/themes/css/bootstrap.css" rel="stylesheet"/>
        <!--<link href="css/bootstrap-social.css" rel="stylesheet"/>-->
        <link href="${context}/themes/css/style.css" rel="stylesheet"/>
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link href="${context}/themes/css/font-awesome.css" rel="stylesheet"/>
        <link href="${context}/themes/css/animate.css" rel="stylesheet"/>

        <script src="${context}/themes/js/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap.min.js"></script>

        <script src="${context}/themes/js/custom.js"></script>
        <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="body_bg">
        <div class="container-fluid">
            <tiles:insertAttribute name="header" ignore="true"/>
            <div class="m-a-3 container-fluid">

                <tiles:insertAttribute name="body" ignore="true" />    
                <!-- Footer -->

            </div>
        </div>
        <tiles:insertAttribute name="footer" ignore="true" />
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>
    </body>
</html>

