<%--
    Document   : default-user-home
    Created on : Apr 1, 2016, 1:01:49 PM
    Author     : Sanjay Rabidas<sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${context}/themes/css/bootstrap.css" rel="stylesheet"/>
        <link href="${context}/themes/css/bootstrap-social.css" rel="stylesheet"/>
        <link href="${context}/themes/css/style.css" rel="stylesheet"/>
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link href="${context}/components/tooltip/tooltip.css" rel="stylesheet"/>
        <link href="${context}/themes/css/font-awesome.css" rel="stylesheet"/>
        <link href="${context}/themes/css/animate.css" rel="stylesheet"/>
        <link href="${context}/themes/cropper/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="${context}/themes/cropper/css/jquery.awesome-cropper.css">
        <link rel="stylesheet" href="${context}/styles/css/jquery.rateyo.min.css">
        <link rel="stylesheet" href="${context}/components/toastr/toastr.min.css">
        <link rel="stylesheet" href="${context}/components/jquery.datepick/css/jquery.datepick.css">
        <link rel="stylesheet" href="${context}/themes/css/bootstrap-select.min.css">
        <script src="${context}/themes/js/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <script src="${context}/components/jquery/jquery.metisMenu.js"></script>
        <script src="${context}/themes/cropper/components/imgareaselect/scripts/jquery.imgareaselect.js"></script>
        <script src="${context}/themes/cropper/build/jquery.awesome-cropper.js"></script>
        <script src="${context}/themes/js/masonry.pkgd.min.js"></script>
        <script src="${context}/themes/js/custom.js"></script>
        <script src="${context}/themes/js/wow.js"></script>


        <%-- NOTE! Please DO Not Remove following Component js --%>
        <script src="${context}/components/underscore/underscore.min.js"></script>
        <script src="${context}/components/jquery/jquery.slimscroll.min.js"></script>
        <script src="${context}/themes/js/bootstrap-select.min.js"></script>
        <script src="${context}/components/jquery.datepick/js/jquery.plugin.min.js"></script>
        <script src="${context}/components/jquery.datepick/js/jquery.datepick.min.js"></script>
        <script src="${context}/components/toastr/toastr.min.js"></script>
        <script src="${context}/scripts/libs/jquery.rateyo.min.js"></script>
         <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="body_bg">
        <div class="container-fluid">
            <tiles:insertAttribute name="header" ignore="true" />
            <tiles:insertAttribute name="breadcrumb" ignore="true" />
            <div style="margin-bottom:0px;" class="clearfix"></div>
            <div class="container-fluid" id="main">
                <div class="clear"></div>
                <div class="col-lg-12">
                    <div style="margin-bottom:20px;" class="clearfix"></div>
                    <tiles:insertAttribute name="sidebar" ignore="true" />
                    <!-- Dashboard Main Contents -->
                    <tiles:insertAttribute name="body" ignore="true" />
                </div>
            </div>

        </div>
        <tiles:insertAttribute name="footer" ignore="true" />
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>        
    </body>
</html>