<%-- 
    Document   : default-layout
    Created on : Apr 1, 2016, 5:27:19 PM
    Author     : Sanjay Rabidas <sanjayr@cdac.in>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<script>
    window.__NVLI = window.__NVLI || {};
    window.__NVLI['context'] = "${context}";
</script>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="${context}/apple-touch-icon.png">
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link rel="stylesheet" href="${context}/components/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="${context}/components/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="${context}/components/animate.css/animate.min.css">
        <link rel="stylesheet" href="${context}/styles/css/production.min.css">
        <link rel="stylesheet" href="${context}/styles/css/dashboard.css">

        <script src="${context}/components/jquery/jquery.min.js"></script>
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/components/bootstrap/js/bootstrap.min.js"></script>
       <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center animated fadeIn">
            <h1><tiles:insertAttribute name="error-code" ignore="false"/></h1>
            <h3 class="font-bold"><tiles:insertAttribute name="error-message" ignore="false"/></h3>

            <div class="error-desc">
                <tiles:insertAttribute name="error-desc" ignore="false"/>
                <br/>
                You can go back to main page: 
                <br/>
                <a href="${context}/user/dashboard" class="btn btn-primary m-t">Dashboard</a>
            </div>
        </div>
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>
    </body>
</html>
