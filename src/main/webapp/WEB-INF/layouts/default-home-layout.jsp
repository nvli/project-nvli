<%-- 
    Document   : default-home-layout
    Created on : Jul 5, 2016, 9:49:16 AM
    Author     : Priya More<mpriya@cdac.in>
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <link rel="icon" href="${context}/themes/images/logo/nvli_orange.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><tiles:insertAttribute name="title"/></title>
        <link rel="stylesheet" href="${context}/themes/css/bootstrap.css"/>
        <link rel="stylesheet" href="${context}/themes/css/style.css"/>
        <link rel="stylesheet" href="${context}/themes/css/hover-min.css"/>
        
        <security:authorize access="isAuthenticated()">
            <c:if test="${not empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/${user.theme.themeCode}.css" rel="stylesheet"/>
            </c:if>
            <c:if test="${empty user.theme.themeCode}">
                <link id="current-theme-style" href="${context}/uploadedThemes/${user.theme.themeCode}_theme/styles/default.css" rel="stylesheet"/>
            </c:if>
        </security:authorize>
        <script>
            window.__NVLI = window.__NVLI || {};
            window.__NVLI['context'] = "${context}";
        </script>
        <security:authorize access="!isAuthenticated()">
            <link id="current-theme-style" href="${context}/uploadedThemes/default_theme/styles/default.css" rel="stylesheet"/>
            <script src="${context}/scripts/init.js"></script>
        </security:authorize>
        <link rel="stylesheet" href="${context}/themes/css/font-awesome.css"/>
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Yatra+One" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Galada" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Timmana" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Baloo+Thambi" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Mogra" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Baloo+Tamma" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Baloo+Paaji" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Teko" rel="stylesheet"> 
        <script type="text/javascript" src="${context}/components/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/pramukhime.js"></script>
        <script type='text/javascript' src='${context}/themes/js/language/pramukhindic.js'></script>
        <script type="text/javascript" src="${context}/themes/js/language/pramukhime-common.js"></script>
        <script type="text/javascript" src="${context}/themes/js/language/switchLanguage.js"></script>
        <link rel="stylesheet" href="${context}/components/jquery-autocomplete/autocomplete.css" />
        <script type="text/javascript" src="${context}/components/jquery-autocomplete/jquery.autocomplete.js"></script>
        <script type="text/javascript" src="${context}/themes/js/scrolling.js"></script>
        <script src="${context}/components/toastr/toastr.min.js"></script>
        <script src="${context}/components/websocket/sockjs-0.3.4.js"></script>
        <script src="${context}/components/websocket/stomp.js"></script>
        
        <link rel="stylesheet" href="${context}/themes/css/animate.css"/>
        <link rel="stylesheet" href="${context}/themes/css/map.css">
        <!--<script src="${context}/themes/js/jquery.min.js"></script>-->
        <script src="${context}/components/tether/js/tether.min.js"></script>
        <script src="${context}/themes/js/bootstrap3.2.min.js"></script>
        <script type="text/javascript" src="${context}/themes/js/custom.js"></script>

        <%-- NOTE! Please DO Not Remove following Component js --%>
        <script src="${context}/components/underscore/underscore.min.js"></script>
        <!--<script src="${context}/components/jquery/jquery.slimscroll.min.js"></script>-->
        <!--<script src="${context}/components/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>-->
         <!-- Piwik -->
        <script type="text/javascript">
            var _paq = _paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function () {
                var u = "//static1.nvli.in/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', '1']);
                var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.async = true;
                g.defer = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Piwik Code -->
    </head>
    <body class="home_bg">
        <div class="container-fluid">
            <tiles:insertAttribute name="body" ignore="true"/>
        </div>
        <tiles:insertAttribute name="footer"  ignore="true"/>
        <%@include file="../views/default-views/underscore-templates.jsp" %>
        <script src="${context}/scripts/main.js"></script>
    </body>
</html>