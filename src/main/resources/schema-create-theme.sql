CREATE TABLE IF NOT EXISTS `user_theme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bg_image_location` varchar(255) DEFAULT NULL,
  `is_custom` tinyint(1) DEFAULT NULL,
  `header_image_location` varchar(255) DEFAULT NULL,
  `theme_code` varchar(255) DEFAULT NULL,
  `theme_description` varchar(255) DEFAULT NULL,
  `theme_name` varchar(255) NOT NULL,
  `theme_repeatable` TINYINT(1) NOT NULL DEFAULT '0',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_theme`
--

-- INSERT IGNORE INTO `user_theme` (`id`, `bg_image_location`, `is_custom`, `header_image_location`, `theme_code`, `theme_description`, `theme_name`,`theme_type`,`theme_activation_date`,`theme_directory`) VALUES
-- (1, NULL, 0, NULL, 'default', 'Default Theme', 'Default','personal','2017-04-20','default'),
-- (2, NULL, 0, NULL, 'dark', 'Dark Theme', 'Dark','personal','2017-04-20','dark'),
-- (3, NULL, 0, NULL, 'light', 'Light Theme', 'Light','personal','2017-04-20','light'),
-- (4, NULL, 0, NULL, 'republic_day', 'Republic Day Theme', 'Republic Day','personal','2017-04-20','republic_day'),
-- (5, NULL, 0, NULL, 'mahatma_gandhi', 'Gandhi Jayanti Theme', 'Mahatma Gandhi Jayanti','personal','2017-04-20','mahatma_gandhi'),
-- (6, NULL, 0, NULL, 'independence', 'Independence Day Theme', 'Independence Day','personal','2017-04-20','independence'),
-- (7, NULL, 0, NULL, 'olympic', 'Rio Olympic 2016 Theme', 'Rio 2016 Olympic','personal','2017-04-20','olympic'),
-- (8, NULL, 0, NULL, 'yoga_day', 'Yoga_Day Theme', 'Yoga_Day','personal','2017-04-20','yoga_day');
-- (9, NULL, 0, NULL, 'nag_panchmi', 'Nag_Panchmi Theme', 'Nag_Panchmi','personal','2017-04-20','nag_panchmi');
-- (10, NULL, 0, NULL, 'mahavir_jayanti', 'Mahavir_Jayanti Theme', 'Mahavir_Jayanti','personal','2017-04-20','mahavir_jayanti');