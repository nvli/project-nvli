package in.gov.nvli.api;

import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.resource.ThematicType;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.service.UserThemeService;
import in.gov.nvli.util.Constants;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Controller API for helping Async XML HTTP Requests to serve data related
 * to Resources of the application.
 *
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@RestController
@RequestMapping(value = "api")
public class ResourceAPI {

    @Autowired
    ResourceService resourceService;

    @Autowired
    UserThemeService themeService;

    @Autowired
    UserService userService;

    /**
     * Method serves List of Thematic Types which actually works as thematic
     * area for User's Interest Selection.
     *
     * @return list of Map
     */
    @RequestMapping(value = "/thematic-types")
    public List<Map<String, Object>> getThematicTypes() {
        List<ThematicType> testList = resourceService.getThematicTypeList();
        List<Map<String, Object>> thematicTypes = new LinkedList<Map<String, Object>>();
        for (Iterator<ThematicType> iterator = testList.iterator(); iterator.hasNext();) {
            ThematicType thematicType = iterator.next();
            Map<String, Object> map = new HashMap<>();
            map.put("thematicTypeId", thematicType.getId());
            map.put("thematicType", thematicType.getThematicType());
            thematicTypes.add(map);
        }
        return thematicTypes;
    }

    /**
     * This controller method provides list of Indian Languages from Universal
     * Decimal Class (UDC) Languages with <i>languageCode</i>,
     * <i>languageName</i>,<i>languageId</i>,<i>isIndianLanguage</i> fields
     *
     * @return {@link JSONArray} of {@link JSONObject}
     */
    @RequestMapping(value = "/udc/languages")
    public JSONArray getUDCLanguages() throws Exception {
        // Fetching Indian Languages
        List<UDCLanguage> udcLanguages = resourceService.getIndianLanguageList();
        JSONArray array = new JSONArray();

        for (UDCLanguage udcLanguage : udcLanguages) {
            JSONObject object = new JSONObject();
            object.put("languageCode", udcLanguage.getLanguageCode());
            object.put("languageName", udcLanguage.getLanguageName());
            object.put("languageId", udcLanguage.getId());
            object.put("isIndianLanguage", udcLanguage.getIsIndianLanguage());
            array.add(object);
        }
        return array;
    }

    /**
     * Method to find all resource types
     *
     * @return JSONArray
     */
    @RequestMapping(value = "/allResources")
    public JSONArray getAllResources() throws Exception {
        JSONArray array = new JSONArray();
        List<ResourceType> ResourceList = resourceService.getListOfAllResourceType();
        for (ResourceType resourceType : ResourceList) {
            JSONObject object = new JSONObject();
            object.put("resourceCode", resourceType.getResourceTypeCode());
            object.put("resourceName", resourceType.getResourceType());
            object.put("resourceId", resourceType.getId());
            array.add(object);
        }
        return array;
    }

    /**
     * Method to serve list of all saved resource interests with
     * <i>resourceCode</i>, <i>resourceName</i>, <i>resourceId</i>.
     *
     * @return JSONArray
     */
    @RequestMapping(value = "/saved-resource-interest")
    public JSONArray getAllSavedResources() throws Exception {
        JSONArray array = new JSONArray();
        List<ResourceType> ResourceList = userService.getCurrentUser().getResourceInterest();
        for (ResourceType resourceType : ResourceList) {
            JSONObject object = new JSONObject();
            object.put("resourceCode", resourceType.getResourceTypeCode());
            object.put("resourceName", resourceType.getResourceType());
            object.put("resourceId", resourceType.getId());
            array.add(object);
        }
        return array;
    }

    /**
     * Method to allow logged in user to add or remove resource interest.
     * Method accepts <b>action</b> ('rm' | 'add') and resourceId as <b>id</b>
     * 
     * 
     * @param action
     * @param id
     * @return JSONObject
     * @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{action}/save-resource-interest/{id}", method = RequestMethod.POST)
    public JSONObject addOrRemoveResourceInterest(
            @PathVariable(value = "action") String action,
            @PathVariable(value = "id") long id
    ) throws Exception {
        User user = userService.getCurrentUser();
        List<ResourceType> resourceTypes = user.getResourceInterest();
        if (action.equalsIgnoreCase("add")) {
            resourceTypes.add(resourceService.getResourceTypeById(id));
        } else if (action.equalsIgnoreCase("rm")) {
            resourceTypes.remove(resourceService.getResourceTypeById(id));
        }
        user.setResourceInterest(resourceTypes);
        userService.updateUser(user);
        JSONObject object = new JSONObject();
        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    /**
     * This controller method returns all themes
     * 
     * @return List
     */
    @RequestMapping(value = "/get-themes", method = RequestMethod.GET)
    public List<HashMap<String, Object>> getThemes() {
        return themeService.listProjectedThemes();
    }
}
