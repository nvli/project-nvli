package in.gov.nvli.api;

import in.gov.nvli.beans.UserDetailsBean;
import in.gov.nvli.domain.user.Cities;
import in.gov.nvli.domain.user.Country;
import in.gov.nvli.domain.user.DeactivationFeedbackBase;
import in.gov.nvli.domain.user.District;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.States;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserFeedback;
import in.gov.nvli.domain.user.UserInterest;
import in.gov.nvli.domain.user.UserOtherFeedback;
import in.gov.nvli.domain.user.UserProfession;
import in.gov.nvli.domain.user.UserTheme;
import in.gov.nvli.domain.user.UserUploadedFile;
import in.gov.nvli.dto.ProfessionDTO;
import in.gov.nvli.dto.RoleDTO;
import in.gov.nvli.dto.UserThemeDTO;
import in.gov.nvli.service.NotificationSubscriptionService;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserProfessionService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.service.UserThemeService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.UserInterestComparatorByID;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is an User API Controller to serve only JSON data in response to any
 * Rest API Call
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
// TODO: Provide access control wherever required while serving sensitive data
@RestController
@RequestMapping("/api")
public class UserAPI {

    @Autowired
    UserService userService;

    @Autowired
    UserThemeService themeService;

    @Autowired
    UserProfessionService userProfessionService;

    @Autowired
    UserThemeService userThemeService;

    @Autowired
    UserActivityService userActivityService;

    @Autowired
    NotificationSubscriptionService notificationSubscriptionService;

    @Autowired
    private UserActivityService activityServiceObj;

    /**
     * Method to return all projected Roles as a list of {@link RoleDTO}
     *
     * @return List of Roles
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = {"/roles"})
    public List<RoleDTO> getRoles() {
        return userService.listRoles();
    }

    /**
     * Method to get {@link Role} by <b>roleId</b>
     *
     * @param roleId
     * @return an object of {@link Role}
     */
    @RequestMapping(value = {"/role/{roleId}"})
    public Role getRole(@PathVariable("roleId") Long roleId) {
        return userService.getRoleByID(roleId);
    }

    /**
     * This controller is to fetch instance of {@link UserDetailsBean } of
     * currently logged in {@link User}
     *
     * @return UserDetailsBean
     */
    @RequestMapping(value = {"/me"})
    public UserDetailsBean me() {
        UserDetailsBean userDetailsBean = null;

        User user = userService.getCurrentUser();
        userDetailsBean = new UserDetailsBean(
                user.getId(),
                user.getEnabled(),
                user.getUsername(),
                user.getEmail(),
                user.getFirstName(),
                user.getMiddleName(),
                user.getLastName(),
                user.getContact(),
                user.getCity(),
                user.getAddress(),
                user.getDistrict(),
                user.getState(),
                user.getCountry(),
                user.getGender(),
                userService.listRoleIdsOfUser(user.getId()),
                user.getProfilePicPath()
        );
        return userDetailsBean;
    }

    /**
     * Get list of all personal themes available
     *
     * @return List of {@link UserThemeDTO}
     */
    @RequestMapping(value = "/get/personal/themes")
    public List<UserThemeDTO> getPersonalThemes() {
        List<UserTheme> themes = themeService.listPersonalThemes();
        List<UserThemeDTO> themesDTO = new LinkedList<UserThemeDTO>();
        for (UserTheme userTheme : themes) {

            UserThemeDTO userThemeDTO = new UserThemeDTO();
            userThemeDTO.setId(userTheme.getId());
            userThemeDTO.setBgImageLocation(userTheme.getBgImageLocation());
            userThemeDTO.setCustom(false);
            userThemeDTO.setHeaderImageLocation(userTheme.getHeaderImageLocation());
            userThemeDTO.setThemeName(userTheme.getThemeName());
            userThemeDTO.setThemeCode(userTheme.getThemeCode());
            userThemeDTO.setThemeDescription(userTheme.getThemeDescription());
            userThemeDTO.setThemeActivationDate(userTheme.getThemeActivationDate());
            userThemeDTO.setThemeType(userTheme.getThemeType());
            themesDTO.add(userThemeDTO);
        }
        return themesDTO;
    }

    /**
     *
     * This method checks if <b>username</b> exists i.e. no user should be
     * registered desired username while allowing user to create or change
     * username. o
     *
     * @param username
     * @return boolean value
     */
    @RequestMapping(value = {"/username/exists"})
    public boolean isUsernameAvailable(
            @RequestParam(value = "username") String username
    ) {
        return userService.usernameExists(username);
    }

    /**
     * Checks if Email is available for allowing user registration.
     *
     * @param useremail
     * @return boolean
     */
    @RequestMapping(value = {"/useremail/exists"})
    public boolean isUserEmailAvailable(
            @RequestParam(value = "useremail") String useremail
    ) {
        return userService.useremailExists(useremail);
    }

    /**
     * Method to get username suggestions based on user's input username,
     * firstName, lastName for registration page
     *
     * @param username
     * @param firstName
     * @param lastName
     *
     * @return {@link JSONArray} of {@link String} suggestedUsername
     */
    @RequestMapping(value = {"/username/suggestions/registration"}, method = RequestMethod.GET)
    public JSONArray usernameSuggestionForRegistration(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName) {

        JSONArray array = new JSONArray();
        List<String> sugegstedUsernameList = userService.suggestedUsername(username, firstName, lastName);
        for (String sugegstedUsername : sugegstedUsernameList) {
            array.add(sugegstedUsername);
        }
        return array;
    }

    /**
     * Method to suggest usernames by taking only username as an input
     *
     * @param username
     * @return {@link JSONArray} of {@link String}
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = {"/username/suggestions"}, method = RequestMethod.GET)
    public JSONArray usernameSuggestion(
            @RequestParam(value = "username") String username) {
        String firstName = userService.getCurrentUser().getFirstName();
        String lastName = userService.getCurrentUser().getLastName();
        JSONArray array = new JSONArray();
        List<String> sugegstedUsernameList = userService.suggestedUsername(username, firstName, lastName);
        for (String sugegstedUsername : sugegstedUsernameList) {
            array.add(sugegstedUsername);
        }
        return array;
    }

    /**
     * Method to find logged in user's interests
     *
     * @return {@link JSONArray}. Also see {@link UserInterest}
     */
    @RequestMapping(value = "/my/interests")
    public JSONArray myInterests() {
        JSONArray array = new JSONArray();
        List<UserInterest> interests = userService.getCurrentUser().getInterests();
        Collections.sort(interests, new UserInterestComparatorByID());
        for (UserInterest interest : interests) {
            JSONObject object = new JSONObject();
            object.put("id", interest.getThematicType().getId());
            object.put("interestId", interest.getId());
            object.put("thematicType", interest.getThematicType().getThematicType());
            object.put("keywords", interest.getKeywords());
            array.add(object);
        }
        return array;
    }

    /**
     * Method to save the Profession data. We accept List of
     * {@link ProfessionDTO}
     *
     *
     * @param list of {@link ProfessionDTO}
     * @return boolean
     */
    @RequestMapping(value = "/save/profession", method = RequestMethod.POST)
    public boolean saveProfessionsFromJSON(@RequestBody List<ProfessionDTO> list
    ) {
        boolean saved = false;
        for (ProfessionDTO p : list) {
            if (null != p.getParentProfession()) {
                saved = userProfessionService.addNew(new UserProfession(p.getProfessionName(), p.getProfessionDescription(), p.getUdcTags(), userProfessionService.findProfessionByID(p.getParentProfession())));
            } else {
                saved = userProfessionService.addNew(new UserProfession(p.getProfessionName(), p.getProfessionDescription(), p.getUdcTags(), null));
            }

        }
        return saved;
    }

    /**
     * This method is to find currently logged in user's selected theme.
     *
     *
     * @return {@link UserThemeDTO}
     */
    @RequestMapping(value = "/me/theme", method = RequestMethod.GET)
    public UserThemeDTO myTheme() {
        User user = userService.getCurrentUser();
        if (null != user && user.getTheme() != null) {
            return new UserThemeDTO(user.getTheme());
        } else {
            UserTheme defaultTheme = themeService.getDefaultTheme();
            if (null != defaultTheme) {
                return new UserThemeDTO(defaultTheme);
            } else {
                return new UserThemeDTO();
            }
        }
    }

    /**
     * Method to find {@link UserTheme} by themeId
     *
     * @param themeId
     * @return {@link UserThemeDTO}
     */
    @RequestMapping(value = "/get/theme/{themeId}", method = RequestMethod.GET)
    public UserThemeDTO getThemeByID(
            @PathVariable(value = "themeId") long themeId
    ) {
        UserTheme theme = themeService.findById(themeId);
        if (null != theme) {
            return new UserThemeDTO(theme);
        } else {
            return new UserThemeDTO();
        }
    }

    /**
     * This method checks the user's logged in status.
     *
     * @return
     */
    @RequestMapping(value = "/check-login-status")
    public JSONObject checkUserLoggedInStatus() {
        User loggedInUser = userService.getCurrentUser();
        JSONObject obj = new JSONObject();
        if (null == loggedInUser) {
            obj.put("status", false);
            obj.put("message", "User not logged in");
            return obj;
        } else {
            obj.put("status", true);
            obj.put("message", "User is logged in");
            obj.put("uid", loggedInUser.getId());
            obj.put("email", loggedInUser.getEmail());
            obj.put("first_name", loggedInUser.getFirstName());
            obj.put("last_name", loggedInUser.getLastName());
            JSONObject roleObj;
            JSONArray roleCodes = new JSONArray();
            for (Role role : userService.getAllRoles()) {
                roleObj = new JSONObject();
                roleObj.put("roleId", role.getRoleId());
                roleObj.put("roleCode", role.getCode());
                roleCodes.add(roleObj);
            }
            obj.put("roles", roleCodes);
            JSONArray loggedInUserRoleCodes = new JSONArray();
            loggedInUser.getRoles().stream().forEach((role) -> {
                loggedInUserRoleCodes.add(role.getCode());
            });
            obj.put("loggedInUserRoles", loggedInUserRoleCodes);

            return obj;
        }
    }

    /**
     * Method to check mandatory profile settings of current user
     *
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "','" + Constants.UserRole.CURATOR_USER + "')")
    @RequestMapping(value = "/check-required-profile-settings", method = RequestMethod.GET)
    public JSONObject checkRequiredProfileSettings() {
        JSONObject obj = new JSONObject();
        User loggedInUser = userService.getCurrentUser();
        if (null == loggedInUser) {
            obj.put("status", false);
            obj.put("message", "Unauthorized Access, User not logged in");
            return obj;
        } else {
            obj.put("status", true);
            obj.put("message", Constants.AjaxResponseCode.SUCCESS);
            JSONObject jsono = new JSONObject();
            jsono.put("hasLanguageExpertise", userService.hasLanguageExpertise(loggedInUser));
            jsono.put("hasResourceExpertise", userService.hasResourceExpertise(loggedInUser));
            obj.put("data", jsono);
            return obj;
        }
    }

    /**
     * controller method use to fetch all countries
     *
     * @return {@link JSONArray} Json array of country id ,country name
     */
    @RequestMapping(value = "fetch/countries", method = RequestMethod.GET)
    public JSONArray fetchCountries() {
        JSONArray array = new JSONArray();
        List<Country> countrylist = userService.getAllCountries();
        for (Country country : countrylist) {
            JSONObject object = new JSONObject();
            object.put("countryid", country.getCountryId());
            object.put("countryname", country.getCountryName());
            array.add(object);
        }
        return array;
    }

    /**
     * controller method use to fetch all states
     *
     * @param countryId that uses to find all states
     * @return {@link JSONArray} Json array of state id ,state name ,country id
     * of corresponding state
     */
    @RequestMapping(value = "/fetch/states/{country}", method = RequestMethod.GET)
    public JSONArray fetchStates(@PathVariable(value = "country") Long countryId) {
        JSONArray array = new JSONArray();
        List<States> statesList = userService.getAllStates(countryId);
        for (States state : statesList) {
            JSONObject object = new JSONObject();
            object.put("stateid", state.getStateId());
            object.put("statename", state.getStateName());
            object.put("countryid", state.getCountries().getCountryId());
            array.add(object);
        }
        return array;
    }

    /**
     * controller method use to get all district
     *
     * @param stateId that uses to find all district
     * @return {@link JSONArray} Json array of district id ,district name ,state
     * id of corresponding district
     */
    @RequestMapping(value = "/fetch/district/{state}", method = RequestMethod.GET)
    public JSONArray fetchDistrict(@PathVariable(value = "state") Long stateId) {
        JSONArray array = new JSONArray();
        List<District> disttList = userService.getAllDistrict(stateId);
        for (District distt : disttList) {
            JSONObject object = new JSONObject();
            object.put("districtid", distt.getDistrictId());
            object.put("districtname", distt.getDistrictName());
            object.put("stateid", distt.getState().getStateId());
            array.add(object);
        }
        return array;
    }

    /**
     * controller method use to get all cities
     *
     * @param districtId that uses to find all cities
     * @return {@link JSONArray} Json array of city id ,city name ,district id
     * of corresponding city
     */
    @RequestMapping(value = "/fetch/city/{district}", method = RequestMethod.GET)
    public JSONArray fetchCities(@PathVariable(value = "district") Long districtId) {
        JSONArray array = new JSONArray();
        List<Cities> cityList = userService.getAllCities(districtId);
        for (Cities city : cityList) {
            JSONObject object = new JSONObject();
            object.put("cityid", city.getCityId());
            object.put("cityname", city.getCityName());
            object.put("districtid", city.getDistrictId().getDistrictId());
            array.add(object);
        }
        return array;
    }

    /**
     * Method to subscribe to global theme scheduler
     *
     * @return
     */
    @RequestMapping(value = "/subscribeTheme", method = RequestMethod.GET)
    public JSONObject subscribeTheme() {
        userThemeService.scheduleGlobalThemeLoad();
        return new JSONObject();
    }

    /**
     * Method to identify global theme applicable for the current date return
     * {@link UserThemeDTO}
     *
     * @param themeDate
     * @return {@link UserThemeDTO}
     */
    @RequestMapping(value = "/get/featured/themes", method = RequestMethod.GET)
    public List<UserThemeDTO> loadTodaysThemes(@RequestParam(value = "themeDate") Date themeDate) {
        List<UserTheme> userThemes = userThemeService.fetchThemesByDate(themeDate);
        List<UserThemeDTO> uTD = new ArrayList<>();
        for (UserTheme ut : userThemes) {
            UserThemeDTO userThemeDTO = new UserThemeDTO();
            userThemeDTO.setId(ut.getId());
            userThemeDTO.setBgImageLocation(ut.getBgImageLocation());
            userThemeDTO.setCustom(false);
            userThemeDTO.setHeaderImageLocation(ut.getHeaderImageLocation());
            userThemeDTO.setThemeName(ut.getThemeName());
            userThemeDTO.setThemeCode(ut.getThemeCode());
            userThemeDTO.setThemeDescription(ut.getThemeDescription());
            userThemeDTO.setThemeActivationDate(ut.getThemeActivationDate());
            userThemeDTO.setThemeType(ut.getThemeType());
            uTD.add(userThemeDTO);

        }
        return uTD;
    }

    /**
     * controller method use to get feedback list
     *
     * @return {@link JSONArray} Json array of feedback id ,feedback label
     * ,feedback description
     */
    @RequestMapping(value = "/fetch/feedback", method = RequestMethod.GET)
    public JSONArray fetchFeedback() {
        JSONArray array = new JSONArray();
        List<DeactivationFeedbackBase> feedbackList = userService.getFeedbackList();
        for (DeactivationFeedbackBase flist : feedbackList) {
            JSONObject object = new JSONObject();
            object.put("feedbackid", flist.getFeedbackId());
            object.put("feedbacklabel", flist.getFeedbackLabel());
            object.put("feedbackdesc", flist.getFeedbackDescription());
            array.add(object);
        }
        return array;
    }

    /**
     * controller method use to Deactivate Account by user
     *
     * @param request
     * @param response
     * @param feedbackId
     * @param feedbackMsg
     * @return {@link JSONObject} Json object use to check status whether
     * account is deactivated successfully or not
     */
    @RequestMapping(value = "/deactivate/account", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject deactivateAccount(HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "fId") String feedbackId,
            @RequestParam(value = "fMessage") String feedbackMsg) {
        JSONObject object = new JSONObject();
        User user = userService.getCurrentUser();
        if (user != null) {
            if (feedbackMsg.isEmpty()) {
                UserFeedback userFeedback = new UserFeedback();
                userFeedback.setDeactivationFeedbackBase(userService.getFeedbackBaseByFID(Long.parseLong(feedbackId)));
                userFeedback.setUser(user);
                userService.saveUserFeedback(userFeedback);
            } else {
                UserOtherFeedback userOtherFeedback = new UserOtherFeedback();
                userOtherFeedback.setUser(user);
                userOtherFeedback.setFeedbackMessage(feedbackMsg);
                userService.saveUserOtherFeedback(userOtherFeedback);
            }
            user.setEnabled(Constants.USER_DEACTIVATED);
            userService.updateUser(user);
            object.put("status", Constants.AjaxResponseCode.SUCCESS);
        } else {
            object.put("status", Constants.AjaxResponseCode.ERROR);
        }
        return object;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/my/images")
    public List<UserUploadedFile> getMyImages() {
        return userService.fetchUserUploadedFiles(userService.getCurrentUser());
    }
}
