package in.gov.nvli.application.listeners;

import in.gov.nvli.dao.crowdsource.IMarc21TagSubfieldStructureDetailDAO;
import in.gov.nvli.dao.crowdsource.IUDCConceptDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.crowdsource.Marc21TagSubfieldStructureDetail;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.mongodb.service.NotificationService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Madhuri
 */
public class ContextLoaderListener implements ServletContextListener {

    private ServletContext ctx;
    private WebApplicationContext springContext;
    private IResourceTypeDAO resourceTypeDAOImpl;
    private IMarc21TagSubfieldStructureDetailDAO marc21TagSubfieldStructureDetailDAO;
    private IUDCConceptDAO udcConceptDAO;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ctx = sce.getServletContext();
        springContext = WebApplicationContextUtils.getWebApplicationContext(ctx);
        resourceTypeDAOImpl = (IResourceTypeDAO) springContext.getBean("resourceTypeDAOImpl");
        udcConceptDAO = (IUDCConceptDAO) springContext.getBean("UDCConceptDAO");
        marc21TagSubfieldStructureDetailDAO = (IMarc21TagSubfieldStructureDetailDAO) springContext.getBean("marc21TagSubfieldStructureDetailDAO");
        List<Marc21TagSubfieldStructureDetail> marc21TagSubfieldStructureDetails = marc21TagSubfieldStructureDetailDAO.list();
        Map<String, String> marc21TitleMap = new HashMap<>();
        for (Marc21TagSubfieldStructureDetail marc21TagSubfieldStructureDetail : marc21TagSubfieldStructureDetails) {
            marc21TitleMap.put(marc21TagSubfieldStructureDetail.getTagSubfield() != null ? (marc21TagSubfieldStructureDetail.getTagField() + "_" + marc21TagSubfieldStructureDetail.getTagSubfield()) : marc21TagSubfieldStructureDetail.getTagField(), marc21TagSubfieldStructureDetail.getLibrarianTitle());
        }
        sce.getServletContext().setAttribute("marc21TitleMap", marc21TitleMap);
        List<ResourceType> resourceTypes = new ArrayList<>();
        try {
            /**
             * getResourceTypes() method changed to
             * getResourceTypesForLocalization() because eager fetch in resource
             * type cause repeat entries of resource type info in all over.
             */
            resourceTypes = resourceTypeDAOImpl.getResourceTypesForLocalization();
        } catch (Exception ex) {
            Logger.getLogger(ContextLoaderListener.class).error(ex.getMessage(), ex);
        }
        Map<String, ResourceType> resourceTypesMap = new LinkedHashMap<>();
        for (ResourceType resourceType : resourceTypes) {
            resourceTypesMap.put(resourceType.getResourceTypeCode(), resourceType);
        }
        sce.getServletContext().setAttribute("resourceTypes", resourceTypesMap);
        sce.getServletContext().setAttribute("jsonLocalisation", createJSONLocalisation());
        sce.getServletContext().setAttribute("udcClassificationMap", udcConceptDAO.getUDCClassification());
    }

    private JSONObject createJSONLocalisation() {
        JSONObject jsonObject = new JSONObject();
        URL url = this.getClass().getClassLoader().getResource("/localisation");
        File file;
        Properties prop;
        JSONObject jsonFileObject;
        try {
            file = new File(url.toURI());
            for (File jsonFile : file.listFiles(pathname -> pathname.getAbsolutePath().endsWith(".properties"))) {
                prop = new Properties();
                prop.load(new InputStreamReader(new FileInputStream(jsonFile), "UTF-8"));
                jsonFileObject = new JSONObject();
                for (Object key : prop.keySet()) {
                    jsonFileObject.put((String) key, prop.get((String) key));
                }
                jsonObject.put(jsonFile.getName().split("_")[1].split("\\.")[0], jsonFileObject);
            }
        } catch (URISyntaxException | IOException ex) {
            Logger.getLogger(ContextLoaderListener.class).error(ex.getMessage(), ex);
        }
        return jsonObject;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
