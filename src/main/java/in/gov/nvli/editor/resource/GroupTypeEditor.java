/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.resource.IGroupTypeDAO;
import in.gov.nvli.domain.resource.GroupType;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for group type {@link GroupType}
 *
 * @author Suman Behara<sumanb@cdac.in>
 * @since 1
 * @version 1
 */
public class GroupTypeEditor extends PropertyEditorSupport {

    /**
     * The name of the property groupTypeDAO used to holds {@link IGroupTypeDAO}
     * object reference.
     */
    private IGroupTypeDAO groupTypeDAO;

    /**
     * Parameterized constructor of GroupTypeEditor
     *
     * @param groupTypeDAO {@link IGroupTypeDAO}
     */
    public GroupTypeEditor(IGroupTypeDAO groupTypeDAO) {
        this.groupTypeDAO = groupTypeDAO;
    }

    /**
     * Gets the property value as a string suitable for presentation.
     *
     * @return
     */
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    /**
     * Sets the property value by parsing a given String.
     *
     * @param text
     * @throws IllegalArgumentException
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        GroupType groupType = null;
        try {
            groupType = groupTypeDAO.get(Long.parseLong(text));
            setValue(groupType);
        } catch (Exception e) {
            setValue(null);
        }
    }
}
