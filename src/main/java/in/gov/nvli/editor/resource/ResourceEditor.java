/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.domain.resource.Resource;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for Resource {@link Resource}
 *
 * @author Suman Behara
 * @version 1
 * @since 1
 */
public class ResourceEditor extends PropertyEditorSupport {

    /**
     * The name of the property resourceDAO used to holds
     * {@link IResourceDAO} object reference.
     */
    private IResourceDAO resourceDAO;

    /**
     * Parameterized Constructor of ResourceEditor
     *
     * @param resourceDAO {@link IResourceDAO}
     */
    public ResourceEditor(IResourceDAO resourceDAO) {
        this.resourceDAO = resourceDAO;
    }

    /**
     * Gets the property value as a string suitable for presentation.
     *
     * @return
     */
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    /**
     * Sets the property value by parsing a given String.
     *
     * @param text
     * @throws IllegalArgumentException
     */
    @Override
    public void setAsText(String text) {
        Resource resource = null;
        try {
            resource = resourceDAO.get(Long.parseLong(text));
            setValue(resource);
        } catch (Exception e) {
            e.printStackTrace();
            setValue(null);
        }
    }
}
