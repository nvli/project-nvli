package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.resource.IOrganizationDAO;
import in.gov.nvli.domain.resource.Organization;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for organization{@link Organization}
 *
 * @author Sujata Aher
 */
public class OrganizationEditor extends PropertyEditorSupport {

    /**
     * The name of the property organizationDAO used to holds
     * {@link IOrganizationDAO} object reference.
     */
    private IOrganizationDAO organizationDAO;

    /**
     * Parameterized Constructor of OrganizationEditor
     *
     * @param organizationDAO {@link IOrganizationDAO}
     */
    public OrganizationEditor(IOrganizationDAO organizationDAO) {
        this.organizationDAO = organizationDAO;
    }

    /**
     * Gets the property value as a string suitable for presentation.
     *
     * @return
     */
    @Override
    public String getAsText() {
        return super.getAsText(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Sets the property value by parsing a given String.
     *
     * @param text
     * @throws IllegalArgumentException
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Organization organization = null;
        try {
            organization = organizationDAO.get(Long.parseLong(text));
            setValue(organization);
        } catch (Exception e) {
            setValue(null);
        }
    }

}
