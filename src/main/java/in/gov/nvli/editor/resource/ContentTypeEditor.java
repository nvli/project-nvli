package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.resource.IContentTypeDAO;
import in.gov.nvli.domain.resource.ContentType;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for content type {@link ContentType}
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public class ContentTypeEditor extends PropertyEditorSupport {

    /**
     * The name of the property contentTypeDAO used to holds
     * {@link IContentTypeDAO} object reference.
     */
    private IContentTypeDAO contentTypeDAO;

    /**
     * Parameterized constructor of ContentTypeEditor
     *
     * @param contentTypeDAO {@link IContentTypeDAO}
     */
    public ContentTypeEditor(IContentTypeDAO contentTypeDAO) {
        this.contentTypeDAO = contentTypeDAO;
    }

    /**
     * Gets the property value as a string suitable for presentation.
     *
     * @return 
     */
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    /**
     * Sets the property value by parsing a given String.
     *
     * @param text
     * @throws IllegalArgumentException
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        ContentType contentType = null;
        try {
            contentType = contentTypeDAO.get(Long.parseLong(text));
            setValue(contentType);
        } catch (Exception e) {
            setValue(null);
        }
    }
}
