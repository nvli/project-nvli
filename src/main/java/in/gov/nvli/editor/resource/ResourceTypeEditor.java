package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.resource.ResourceType;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for ResourceType {@link ResourceType}
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public class ResourceTypeEditor extends PropertyEditorSupport {

    /**
     * The name of the property resourceTypeDAO used to holds
     * {@link IResourceTypeDAO} object reference.
     */
    private IResourceTypeDAO resourceTypeDAO;

    /**
     * Parameterized Constructor of ResourceTypeEditor
     *
     * @param resourceTypeDAO {@link IResourceTypeDAO}
     */
    public ResourceTypeEditor(IResourceTypeDAO resourceTypeDAO) {
        this.resourceTypeDAO = resourceTypeDAO;
    }

    /**
     * Gets the property value as a string suitable for presentation.
     *
     * @return
     */
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    /**
     * Sets the property value by parsing a given String.
     *
     * @param text
     * @throws IllegalArgumentException
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {

        ResourceType resourceType = null;
        try {
            resourceType = resourceTypeDAO.get(Long.parseLong(text));
            setValue(resourceType);
        } catch (Exception e) {
            setValue(null);
        }
    }
}
