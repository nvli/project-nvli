package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.crowdsource.ILanguageDAO;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for Language type {@link Language}
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public class LanguageEditor extends PropertyEditorSupport {

    /**
     * The name of the property languageDAO used to holds {@link ILanguageDAO}
     * object reference.
     */
    private ILanguageDAO languageDAO;

    /**
     * Parameterized constructor of LanguageEditor
     *
     * @param languageDAO {@link ILanguageDAO}
     */
    public LanguageEditor(ILanguageDAO languageDAO) {
        this.languageDAO = languageDAO;
    }

    /**
     * Gets the property value as a string suitable for presentation.
     *
     * @return
     */
    @Override
    public String getAsText() {
        return super.getAsText();
    }

    /**
     * Sets the property value by parsing a given String.
     *
     * @param text
     * @throws IllegalArgumentException
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        UDCLanguage language = null;
        try {
            language = languageDAO.get(Long.parseLong(text));
            setValue(language);
        } catch (Exception e) {
            setValue(null);
        }
    }
}
