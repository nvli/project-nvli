package in.gov.nvli.editor.resource;

import in.gov.nvli.dao.resource.IThematicTypeDAO;
import in.gov.nvli.domain.resource.ThematicType;
import java.beans.PropertyEditorSupport;

/**
 * Property Editor for Language type {@link Language}
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public class ThematicTypeEditor extends PropertyEditorSupport {

    /**
     * The name of the property thematicTypeDAO used to holds
     * {@link IThematicTypeDAO} object reference.
     */
    private IThematicTypeDAO thematicTypeDAO;

    /**
     * Parameterized Constructor of ThematicTypeEditor
     *
     * @param thematicTypeDAO {@link IThematicTypeDAO}
     */
    public ThematicTypeEditor(IThematicTypeDAO thematicTypeDAO) {
        this.thematicTypeDAO = thematicTypeDAO;
    }

    @Override
    public String getAsText() {
        return super.getAsText();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        ThematicType thematicType = null;
        try {
            thematicType = thematicTypeDAO.get(Long.parseLong(text));
            setValue(thematicType);
        } catch (Exception e) {
            setValue(null);
        }
    }
}
