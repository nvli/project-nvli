/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.artist;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ArtistProfile;
import java.util.List;

/**
 *
 * @author admin
 */
public interface ArtistProfileDao extends IGenericDAO<ArtistProfile, Integer> {

    public List<ArtistProfile> getArtistProfileByName(String name);

    public List<ArtistProfile> getAllArtistForGallary(String gallaryURL);

    public List<ArtistProfile> getAllArtistForGallary(Integer id);

}
