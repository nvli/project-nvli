/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.artist;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ArtisticResourceType;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gajraj Singh Tanwar
 */
@Repository
public class ArtistSiteDaoImpl extends GenericDAOImpl<ArtisticResourceType, Integer> implements ArtistSiteDao {

    @Override
    public List<ArtisticResourceType> getAllArtistSites() {
        return list();

    }

}
