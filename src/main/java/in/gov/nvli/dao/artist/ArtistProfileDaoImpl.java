/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.artist;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ArtistProfile;
import in.gov.nvli.domain.resource.ArtisticResourceType;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author admin
 */
@Repository
public class ArtistProfileDaoImpl extends GenericDAOImpl<ArtistProfile, Integer> implements ArtistProfileDao {

    @Override
    public List<ArtistProfile> getArtistProfileByName(String name) {
        Criteria criteria = currentSession().createCriteria(ArtistProfile.class);
        criteria.add(Restrictions.eq("artistName", name));
        return (List<ArtistProfile>) criteria.list();
    }

    @Override
    public List<ArtistProfile> getAllArtistForGallary(String gallaryURL) {
        Criteria criteria = currentSession().createCriteria(ArtistProfile.class);
        criteria.add(Restrictions.eq("artistSiteID", gallaryURL));
        return (List<ArtistProfile>) criteria.list();
    }

    @Override
    public List<ArtistProfile> getAllArtistForGallary(Integer id) {
        ArtisticResourceType at = new ArtisticResourceType();
        at.setArtisticResourseId(id);
        Criteria criteria = currentSession().createCriteria(ArtistProfile.class);
        criteria.add(Restrictions.eq("artistResourceID", at));
        return (List<ArtistProfile>) criteria.list();
    }

}
