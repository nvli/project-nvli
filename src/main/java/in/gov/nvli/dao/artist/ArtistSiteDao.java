/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.artist;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ArtisticResourceType;
import java.util.List;

/**
 *
 * @author Gajraj Singh Tanwar
 */
public interface ArtistSiteDao extends IGenericDAO<ArtisticResourceType, Integer> {

    public List<ArtisticResourceType> getAllArtistSites();

}
