package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;

/**
 * Resource DAO interface.
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
public interface IResourceDAO extends IGenericDAO<Resource, Long> {

    /**
     * Find last inserted Resource.
     *
     * @return last inserted Resource object
     */
    public Resource getLastInsertedResource(Long resourceTypeId);

    /**
     * Delete resource using resourceCode.
     *
     * @param resourceCode {@link Resource} code
     */
    public void deleteByResourceCode(String resourceCode);

    /**
     * Returns list of used resource type
     *
     * @return list of {@link RseourceType}
     */
    public List<ResourceType> getListOfUsedResourceType();

    /**
     * Get the List of resource type. size.
     *
     * @return {@link ResourceType} list
     */
    public List<ResourceType> getListOfAllResourceType();

    /**
     * Returns resource by resource type
     *
     * @param resourceType {@link ResourceType}
     * @return list of {@link Resource}
     */
    public List<Resource> getResourceByResourceType(final ResourceType resourceType);

    /**
     * Returns resource by type and group
     *
     * @param grouptype {@link GroupType}
     * @param resourceType {@link ResourceType}
     * @return list of {@link Resource}
     */
    public List<Resource> getResourceByGroupAndResourceType(final GroupType grouptype, final ResourceType resourceType);

    /**
     * Returns total record count by resource type within resource
     *
     * @param resourceType {@link ResourceType}
     * @return total record count
     */
    public Long getRecordCountByResourceType(ResourceType resourceType);

    /**
     * Get the draft resources.(Failed to save through web service)
     *
     * @param resourceType {@link ResourceType}
     * @return list of {@link Resource}
     */
    public List<Resource> getDraftResource(ResourceType resourceType);

    /**
     * Get the List of {@link Resource} of particular {@link ResourceType}. As
     * per the page number number and page window size.
     *
     * @param searchString resource name to search
     * @param pageNumber page number
     * @param pageWindow window size
     * @param resourceType {@link ResourceType}
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateResource(String searchString, int pageNumber, int pageWindow, ResourceType resourceType);

  /**
     * Get the List of {@link Resource} of particular {@link ResourceType}. As
     * per the page number number and page window size.
     *
     * @param searchString resource name to search
     * @param pageNumber page number
     * @param pageWindow window size
     * @param resourceType {@link ResourceType}
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateDraftResource(String searchString, int pageNumber, int pageWindow, ResourceType resourceType);

    /**
     *
     * @param resourceIdList
     * @return
     * @throws Exception
     */
    public List<Resource> getListOfResourceByResourceIds(List<Long> resourceIdList) throws Exception;

    /**
     * 
     * @param searchString
     * @param resourceTypeFilter
     * @param dataLimit
     * @param pageNo
     * @return
     * @throws Exception
     */
    public List<Resource> getResourcesByFiltersWithLimit(String searchString, long resourceTypeFilter, int dataLimit, int pageNo) throws Exception;

    /**
     * Get Resource by Resource name
     *
     * @param resourceName resource name
     * @return {@link Resource}
     */
    public Resource getResourceByName(String resourceName);

    /**
     * Get Resource by Resource code
     *
     * @param resourceCode resource code
     * @return {@link Resource}
     */
    public Resource getResourceByCode(String resourceCode);

   /**
    * Get Resource by Resource name and Resource Type
    * @param resourceName name of resource
    * @param resourceType {@link ResourceType}
    * @return {@link Resource}
    */
    public Resource getResourceByNameAndRT(String resourceName, ResourceType resourceType) ;
}
