package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ResourceTypeInfo;
import java.io.Serializable;

/**
 *
 * @author Sujata Aher
 */
public interface IResourceTypeInfoDAO extends IGenericDAO<ResourceTypeInfo, Long>{
    
}
