package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctResultTransformer;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * ResourceType DAO interface definition
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Repository
public class ResourceTypeDAOImpl extends GenericDAOImpl<ResourceType, Long> implements IResourceTypeDAO {

    private static final Logger LOG = Logger.getLogger(ResourceTypeDAOImpl.class);

    @Override
    public ResourceType getResourceObjectFromMappingResourceType(String mappingResType) {
        ResourceType resType = null;
        try {
            resType = (ResourceType) super.currentSession().createCriteria(ResourceType.class).add(Restrictions.eq("resourceType", mappingResType)).uniqueResult();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return resType;
    }

    /**
     * doPaginateResourceType method used to do Resource type Register
     * pagination.
     *
     * @param pageNumber
     * @param pageWindow
     * @return resultHolder {@link ResultHolder}
     */
    @Override
    @Transactional
    public ResultHolder doPaginateResourceType(String searchString, int pageNum, int pageWin) {
        List list = null;
        long resultCount = 0L;
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        if (searchString != null && !searchString.equals("")) {
            list = currentSession().createCriteria(ResourceType.class).add(Restrictions.like("resourceType", searchString, MatchMode.ANYWHERE)).addOrder(Order.asc("resourceType")).setFirstResult(from).setMaxResults(to).list();
        } else {
            list = currentSession().createCriteria(ResourceType.class).addOrder(Order.asc("resourceType")).setFirstResult(from).setMaxResults(to).list();
        }
        resultCount = (Long) currentSession().createCriteria(ResourceType.class).setProjection(Projections.count("id")).uniqueResult();
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    /**
     * Returns list of resource type according to desc total record count
     *
     * @return list
     */
    @Override
    public List<ResourceType> getListOfResourceTypeAccToTotalRecordCount() {
        Criteria resourceCriteria = currentSession().createCriteria(ResourceType.class).add(Restrictions.eq("publishFlag", Boolean.TRUE)).addOrder(Order.desc("totalRecordCount"));
        /**
         * Distinct Root Entity is added because resource types repeating on
         * common search jsp.
         */
        return resourceCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public ResourceType getResourceTypeById(final long id) {
        ResourceType resourceType = null;
        Criteria resourceCriteria = currentSession().createCriteria(ResourceType.class).add(Restrictions.eq("id", id)).setFetchMode("resources", FetchMode.JOIN);
        List<ResourceType> list = resourceCriteria.list();
        if (!list.isEmpty()) {
            resourceType = list.get(0);
        }
        return resourceType;
    }

    @Override
    public List<Resource> getResourceByResourceTypeCode(String resourceTypeCode) {
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class, "resource");
        resourceCriteria.createAlias("resource.resourceType", "resourceType").add(Restrictions.eq("resourceType.resourceTypeCode", resourceTypeCode));
        resourceCriteria.setProjection(Projections.projectionList()
                .add(Projections.property("resourceName"), "resourceName")
                .add(Projections.property("resourceCode"), "resourceCode"))
                .setResultTransformer(Transformers.aliasToBean(Resource.class));
        resourceCriteria.addOrder(Order.asc("resourceName"));
        List<Resource> resources = (List<Resource>) resourceCriteria.list();
        return resources;
    }

    @Override
    public List<ResourceType> getResourceTypes() throws Exception {
        Criteria resourceCriteria = currentSession().createCriteria(ResourceType.class).addOrder(Order.asc("resourceType"));
        resourceCriteria.addOrder(Order.asc("resourceType"));
        return resourceCriteria.list();
    }

    /**
     * Get all resource types and also eagerly load resource type info for
     * localization.
     *
     * @return list of resource type object
     * @throws Exception
     */
    @Override
    public List<ResourceType> getResourceTypesForLocalization() throws Exception {
        Criteria resourceCriteria = currentSession().createCriteria(ResourceType.class).add(Restrictions.eq("publishFlag", true)).addOrder(Order.asc("resourceType")).setFetchMode("resourceTypeInfo", FetchMode.JOIN);
        resourceCriteria.addOrder(Order.asc("resourceType"));
        return resourceCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public ResourceType getResourceTypeByName(String resourceTypeName) {
        ResourceType resType = null;
        try {
            resType = (ResourceType) super.currentSession().createCriteria(ResourceType.class).add(Restrictions.eq("resourceType", resourceTypeName)).uniqueResult();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return resType;
    }

    @Override
    public ResourceType getResourceTypeByCode(String resourceTypeCode) {
        ResourceType resType = null;
        try {
            resType = (ResourceType) super.currentSession().createCriteria(ResourceType.class).add(Restrictions.eq("resourceTypeCode", resourceTypeCode)).uniqueResult();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return resType;
    }
}
