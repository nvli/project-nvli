package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.OpenRepository;
import in.gov.nvli.domain.resource.ResourceStatus;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Open Repository DAO interface definition
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Repository
public class OpenRepositoryDAOImpl extends GenericDAOImpl<OpenRepository, Long> implements IOpenRepositoryDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); 
    }

    /**
     * Get {@link ResourceStatus} wise list of {@link OpenRepository}
     * @param resourceStatus {@link ResourceStatus}
     * @return 
     */
    @Override
    public List<OpenRepository> getOpenRepoByStatus(ResourceStatus resourceStatus) {
        Criteria resourceCriteria = currentSession().createCriteria(OpenRepository.class).add(Restrictions.eq("resourceStatus", resourceStatus));
        return resourceCriteria.list();
    }

   

}
