package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Resource DAO interface definition
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Repository
public class ResourceDAOImpl extends GenericDAOImpl<Resource, Long> implements IResourceDAO {

    private ResultHolder resultHolder;

    @Override
    @Transactional(readOnly = true)
    public Resource getLastInsertedResource(Long resourceTypeId) {
        Resource resource = (Resource) currentSession().createQuery("FROM Resource where resourceType.id=" + resourceTypeId + " ORDER BY id DESC").setMaxResults(1).uniqueResult();
        return resource;
    }

    @Override
    @Transactional
    public void deleteByResourceCode(String resourceCode) {
        List<Resource> resourceList = findbyQuery("findByResourceCode", resourceCode);
        if (resourceList != null && !resourceList.isEmpty()) {
            Resource resource = resourceList.get(0);
            if (resource.getLanguages() != null) {
                resource.getLanguages().removeAll(resource.getLanguages());
            }
            if (resource.getContentTypes() != null) {
                resource.getContentTypes().removeAll(resource.getContentTypes());
            }
            if (resource.getThematicTypes() != null) {
                resource.getThematicTypes().removeAll(resource.getThematicTypes());
            }
            delete(resource);
            flush();
        }
    }

    @Override
    public List<ResourceType> getListOfUsedResourceType() {
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class).addOrder(Order.asc("resourceType")).setProjection(Projections.distinct(Projections.property("resourceType")));;
        return resourceCriteria.list();
    }

    @Override
    public List<ResourceType> getListOfAllResourceType() {
        Criteria resourceCriteria = currentSession().createCriteria(ResourceType.class);
        return resourceCriteria.list();

    }

    @Override
    public List<Resource> getResourceByResourceType(final ResourceType resourceType) {
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class).add(Restrictions.eq("resourceType", resourceType)).addOrder(Order.asc("groupType.id"));
        return resourceCriteria.list();
    }

    @Override
    public List<Resource> getResourceByGroupAndResourceType(GroupType grouptype, ResourceType resourceType) {
        List list = null;
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class).add(Restrictions.eq("resourceType", resourceType)).add(Restrictions.eq("groupType", grouptype)).addOrder(Order.asc("resourceName"));
        return resourceCriteria.list();
    }

    @Override
    public Long getRecordCountByResourceType(ResourceType resourceType) {
        long totalCount = 0;
        Query totalRecordCountQuery = currentSession().createQuery("select sum(recordCount) from Resource where resourceType=:resourceType");
        totalRecordCountQuery.setParameter("resourceType", resourceType);
        Object object = totalRecordCountQuery.uniqueResult();
        if (object != null) {
            totalCount = Long.valueOf(totalRecordCountQuery.uniqueResult().toString());
        }
        return totalCount;
    }

    @Override
    public List<Resource> getDraftResource(ResourceType resourceType) {
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType));//.add(Restrictions.eq("resourceStatus.statusCode",(short)8)).add(Restrictions.or("resourceStatus.statusCode",(short)9));
        Criterion rest1 = Restrictions.and(Restrictions.eq("resourceStatus.statusCode", (short) 8));
        Criterion rest2 = Restrictions.and(Restrictions.eq("resourceStatus.statusCode", (short) 9));
        resourceCriteria.add(Restrictions.or(rest1, rest2));
        return resourceCriteria.list();
    }

    @Override
    @Transactional
    public ResultHolder doPaginateResource(String searchString, int pageNum, int pageWin, ResourceType resourceType) {
        long resultCount;
        List list;
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType)).addOrder(org.hibernate.criterion.Property.forName("createdOn").desc());
        Criterion rest1 = Restrictions.and(Restrictions.ne("resourceStatus.statusCode", (short) 8));
        Criterion rest2 = Restrictions.and(Restrictions.ne("resourceStatus.statusCode", (short) 9));
        if (searchString != null && !searchString.equals("")) {
            Criterion searchCriterion = Restrictions.and(Restrictions.like("resourceName", searchString, MatchMode.ANYWHERE));
            list = resourceCriteria.add(Restrictions.and(rest1, rest2, searchCriterion)).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType)).add(Restrictions.or(rest1, rest2)).add(Restrictions.and(searchCriterion)).setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();
        } else {
            list = resourceCriteria.add(Restrictions.and(rest1, rest2)).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType)).add(Restrictions.or(rest1, rest2)).setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();//list.size();
        }
        resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    @Override
    @Transactional(readOnly = true)
    public ResultHolder doPaginateDraftResource(String searchString, int pageNum, int pageWin, ResourceType resourceType) {
       long resultCount;
        List list;
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        Criteria resourceCriteria = currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType)).addOrder(org.hibernate.criterion.Property.forName("createdOn").desc());
        Criterion rest1 = Restrictions.and(Restrictions.eq("resourceStatus.statusCode", (short) 8));
        Criterion rest2 = Restrictions.and(Restrictions.eq("resourceStatus.statusCode", (short) 9));
        if (searchString != null && !searchString.equals("")) {
            Criterion searchCriterion = Restrictions.and(Restrictions.like("resourceName", searchString, MatchMode.ANYWHERE));
            list = resourceCriteria.add(Restrictions.or(rest1, rest2)).add(Restrictions.and(searchCriterion)).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType)).add(Restrictions.or(rest1, rest2)).add(Restrictions.and(searchCriterion)).setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();
        }else{
            list = resourceCriteria.add(Restrictions.or(rest1, rest2)).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(Resource.class).createAlias("resourceStatus", "resourceStatus").add(Restrictions.eq("resourceType", resourceType)).add(Restrictions.or(rest1, rest2)).setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();//list.size();
            
        }
        resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    @Override
    public List<Resource> getListOfResourceByResourceIds(List<Long> resourceIdList) throws Exception {
        return currentSession().createCriteria(Resource.class).add(Restrictions.in("id", resourceIdList)).list();
    }

    @Override
    public List<Resource> getResourcesByFiltersWithLimit(String searchString, long resourceTypeFilter, int dataLimit, int pageNo) throws Exception {
        Criteria criteria = currentSession().createCriteria(Resource.class).createAlias("resourceType", "resourceType");
        criteria.add(Restrictions.like("resourceName", searchString, MatchMode.ANYWHERE));
        if (resourceTypeFilter != 0) {
            criteria.add(Restrictions.eq("resourceType.id", resourceTypeFilter));
        }
        criteria.addOrder(Order.asc("resourceType.resourceType"));
        criteria.addOrder(Order.asc("resourceName"));
        if (dataLimit > 0) {
            criteria.setFirstResult((pageNo - 1) * dataLimit);
            criteria.setMaxResults(dataLimit);
        }
        return criteria.list();
    }

    @Override
    public Resource getResourceByName(String resourceName) {
        Resource resource = null;
        resource = (Resource) super.currentSession().createCriteria(Resource.class).add(Restrictions.eq("resourceName", resourceName)).uniqueResult();
        return resource;
    }

    @Override
    public Resource getResourceByCode(String resourceCode) {
        Resource resource = null;
        resource = (Resource) super.currentSession().createCriteria(Resource.class).add(Restrictions.eq("resourceCode", resourceCode)).uniqueResult();
        return resource;
    }

    @Override
    public Resource getResourceByNameAndRT(String resourceName, ResourceType resourceType) {
        Resource resource = null;
        System.out.println("resourceName getResourceByNameAndRT " + resourceName);
        resource = (Resource) super.currentSession().createCriteria(Resource.class).add(Restrictions.eq("resourceName", resourceName)).add(Restrictions.eq("resourceType", resourceType)).uniqueResult();
        return resource;
    }
}
