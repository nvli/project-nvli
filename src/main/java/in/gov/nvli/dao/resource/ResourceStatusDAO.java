package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ResourceStatus;
import org.springframework.stereotype.Repository;

/**
 * ResourceStatus DAO interface definition
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Repository
public class ResourceStatusDAO extends GenericDAOImpl<ResourceStatus, Long> implements IResourceStatusDAO{
    
}
