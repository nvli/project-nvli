package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ContentType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * ContentType DAO interface definition
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Repository
public class ContentTypeDAOImpl extends GenericDAOImpl<ContentType, Long> implements IContentTypeDAO {

    @Override
    @Transactional
    public ResultHolder doPaginateContentType(String searchString, int pageNumber, int pageWindow) {
        List list = null;
        long resultCount = 0L;
        int from = ((pageNumber - 1) * pageWindow);
        int to = pageWindow;
        if (searchString != null && !searchString.equals("")) {
            list = currentSession().createCriteria(ContentType.class).add(Restrictions.like("contentType", searchString, MatchMode.ANYWHERE)).addOrder(Order.asc("contentType")).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(ContentType.class).add(Restrictions.like("contentType", searchString, MatchMode.ANYWHERE)).setProjection(Projections.count("id")).uniqueResult();
        } else {
            list = currentSession().createCriteria(ContentType.class).addOrder(Order.asc("contentType")).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(ContentType.class).setProjection(Projections.count("id")).uniqueResult();
        }
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    @Transactional
    @Override
    public ContentType getContentTypeByContentName(String contentName) {
        Criteria criteria = currentSession().createCriteria(ContentType.class);
        criteria.add(Restrictions.eq("contentType", contentName));
        return (ContentType) criteria.uniqueResult();
    }
}
