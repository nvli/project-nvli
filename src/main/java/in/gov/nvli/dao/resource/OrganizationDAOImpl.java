/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Organization DAO interface definition.
 *
 * @author Sujata Aher
 * @author Vivek Bugale
 */
@Repository
public class OrganizationDAOImpl extends GenericDAOImpl<Organization, Long> implements IOrganizationDAO {

    /**
     * doPaginateOrganizationList method used for
     * Organization{@link Organization} List pagination
     *
     * @param pageNumber
     * @param pageWindow
     * @return resultHolder {@link ResultHolder}
     */
    @Override
    public ResultHolder doPaginateOrganizationList(String pageNumber, String pageWindow) {
        List list = null;
        long resultCount = 0L;
        int pageNum = Integer.parseInt(pageNumber);
        int pageWin = Integer.parseInt(pageWindow);
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        list = currentSession().createCriteria(Organization.class).addOrder(Order.desc("id")).setFirstResult(from).setMaxResults(to).list();
        resultCount = (Long) currentSession().createCriteria(Organization.class).setProjection(Projections.count("id")).uniqueResult();
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    /**
     * This method is used to get List of {@link organization} by using the
     * organization id's list.
     *
     * @param organizationsIdsList List of organization id's.
     * @return List of Organizations.
     */
    @Override
    public List<Organization> getListOfOrganizationsByOrganizationIds(List<Long> organizationsIdsList) {
        return currentSession().createCriteria(Organization.class).add(Restrictions.in("id", organizationsIdsList)).list();
    }

    @Override
    @Transactional
    public ResultHolder doPaginateOrganization(String searchString, int pageNumber, int pageWindow) {
        List list = null;
        long resultCount = 0L;
        int from = ((pageNumber - 1) * pageWindow);
        int to = pageWindow;
        if (searchString != null && !searchString.equals("")) {
            list = currentSession().createCriteria(Organization.class).add(Restrictions.like("name", searchString, MatchMode.ANYWHERE)).addOrder(Order.asc("name")).setFirstResult(from).setMaxResults(to).list();
       resultCount=(Long)currentSession().createCriteria(Organization.class).add(Restrictions.like("name", searchString, MatchMode.ANYWHERE)).setProjection(Projections.count("id")).uniqueResult();
        } else {
            list = currentSession().createCriteria(Organization.class).addOrder(Order.asc("name")).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(Organization.class).setProjection(Projections.count("id")).uniqueResult();
        }
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }
   
    
    @Override
    @Transactional
    public Organization getOrganizationByOrganizationName(String organizationName){
        Criteria criteria = currentSession().createCriteria(Organization.class)   ;
        criteria.add(Restrictions.eq("name", organizationName));
       return (Organization) criteria.uniqueResult();
}
}
