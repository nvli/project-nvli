package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;

/**
 * GroupType DAO interface definition
 *
 * @author Suman Behara<sumanb@cdac.in>
 * @author Sujata Aher
 * @since 1
 * @version 1
 */
public interface IGroupTypeDAO extends IGenericDAO<GroupType, Long> {

    /**
     * Returns list of group type by resource type
     *
     * @param resourceType {@link ResourceType}
     * @return list of {@link GroupType}
     */
    public List<GroupType> getGroupByResourceType(final ResourceType resourceType);

    /**
     * Get the List of group type. As per the page number number and page window
     * size.
     *
     * @param pageNumber page number
     * @param pageWindow window size
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateGroupList(final String pageNumber, final String pageWindow);

    /**
     * Returns GroupType by groupName
     *
     * @param groupName {@link GroupType} name
     * @return {@link GroupType}
     */
    public GroupType getGroupTypeByGroupName(String groupName);

    /**
     * Get the List of group type. As per the page number number and page window
     * size.
     *
     * @param searchString
     * @param pageNumber page number
     * @param pageWindow window size
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateGroupType(String searchString, int pageNumber, int pageWindow);
}
