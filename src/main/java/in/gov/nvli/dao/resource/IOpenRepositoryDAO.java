package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.OpenRepository;
import in.gov.nvli.domain.resource.ResourceStatus;
import java.util.List;

/**
 * Open Repository DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IOpenRepositoryDAO extends IGenericDAO<OpenRepository, Long> {

    /**
     * Get the List of {@link OpenRepository}
     *
     * @param resourceStatus {@link ResourceStatus}
     * @return List of {@link OpenRepository}
     */
    public List<OpenRepository> getOpenRepoByStatus(ResourceStatus resourceStatus);

}
