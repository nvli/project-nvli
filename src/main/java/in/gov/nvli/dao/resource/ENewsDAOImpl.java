package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ENews;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * ENews DAO interface definition
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Repository
public class ENewsDAOImpl extends GenericDAOImpl<ENews, Long> implements IENewsDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); 
    }
}
