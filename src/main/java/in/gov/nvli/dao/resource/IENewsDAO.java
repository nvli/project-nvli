package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ENews;

/**
 * ENews DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IENewsDAO extends IGenericDAO<ENews, Long> {

}
