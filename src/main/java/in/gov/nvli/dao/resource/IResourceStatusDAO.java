package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ResourceStatus;

/**
 * ResourceStatus DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IResourceStatusDAO extends IGenericDAO<ResourceStatus, Long>{
    
}
