package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ContentType;
import in.gov.nvli.util.ResultHolder;

/**
 * ContentType DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IContentTypeDAO extends IGenericDAO<ContentType, Long> {

    /**
     * Get the List of content type. As per the page number number and page
     * window size.
     *
     * @param searchString
     * @param pageNumber page number
     * @param pageWindow window size
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateContentType(String searchString, int pageNumber, int pageWindow);

    /**
     * Returns ContentType by contentName
     *
     * @param contentName
     * @return ContentType
     */
    public ContentType getContentTypeByContentName(String contentName);
}
