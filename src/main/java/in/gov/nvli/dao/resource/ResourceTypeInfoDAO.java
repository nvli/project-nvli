package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ResourceTypeInfo;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sujata Aher
 */
@Repository
public class ResourceTypeInfoDAO extends GenericDAOImpl<ResourceTypeInfo, Long> implements IResourceTypeInfoDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
