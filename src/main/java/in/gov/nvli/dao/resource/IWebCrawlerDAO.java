package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.WebCrawler;

/**
 * Web Crawler DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IWebCrawlerDAO extends IGenericDAO<WebCrawler, Long>{
    
}
