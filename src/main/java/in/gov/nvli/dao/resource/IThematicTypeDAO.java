package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.ThematicType;
import in.gov.nvli.util.ResultHolder;

/**
 * ThematicType DAO interface
 *
 * @author Suman
 * @version 1
 * @since 1
 */
public interface IThematicTypeDAO extends IGenericDAO<ThematicType, Long> {

    /**
     * Get the List of {@link ThematicType}. As per the page number number and
     * page window size.
     *
     * @param pageNumber page number
     * @param pageWindow window size
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateThematic(final String pageNumber, final String pageWindow);

    /**
     * Returns ThematicType by thematicName
     *
     * @param thematicName thematic name
     * @return {@link ThematicType}
     */
    public ThematicType getThematicTypeByThematicName(String thematicName);

    public ResultHolder doPaginateThematicType(String searchString, int pageNum, int pageWin);
}
