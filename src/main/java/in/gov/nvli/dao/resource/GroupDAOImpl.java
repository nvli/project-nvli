package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * GroupDAOImpl DAO interface definition
 *
 * @author Suman Behara<sumanb@cdac.in>
 * @since 1
 * @version 1
 */
@Repository
public class GroupDAOImpl extends GenericDAOImpl<GroupType, Long> implements IGroupTypeDAO {

    /**
     * Returns list of group type by resource type
     *
     * @param resourceType {@link ResourceType}
     * @return list of {@link GroupType}
     */
    @Override
    public List<GroupType> getGroupByResourceType(final ResourceType resourceType) {
       Criteria groupCriteria = currentSession().createCriteria(GroupType.class).createCriteria("resourceTypes").add(Restrictions.eq("resourceType", resourceType.getResourceType()));
       List<GroupType> grpList=groupCriteria.list();
       grpList.sort(GroupType.GroupTypeComparator);
       return grpList;
    }

    /**
     * doPaginateGroupList method used for group List pagination
     *
     * @param pageNumber
     * @param pageWindow
     * @return resultHolder {@link ResultHolder}
     */
    @Override
    @Transactional
    public ResultHolder doPaginateGroupList(String pageNumber, String pageWindow) {
        List list = null;
        long resultCount = 0L;
        int pageNum = Integer.parseInt(pageNumber);
        int pageWin = Integer.parseInt(pageWindow);
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        list = currentSession().createCriteria(GroupType.class).addOrder(Order.asc("groupType")).setFirstResult(from).setMaxResults(to).list();
        resultCount = (Long) currentSession().createCriteria(GroupType.class).setProjection(Projections.count("id")).uniqueResult();
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    /**
     * Returns GroupType by groupName
     *
     * @param groupName {@link GroupType} name
     * @return {@link GroupType}
     */
    @Transactional
    @Override
    public GroupType getGroupTypeByGroupName(String groupName) {
        Criteria criteria = currentSession().createCriteria(GroupType.class);
        criteria.add(Restrictions.eq("groupType", groupName));
        return (GroupType) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public ResultHolder doPaginateGroupType(String searchString, int pageNumber, int pageWindow) {
        List list = null;
        long resultCount = 0L;
        int from = ((pageNumber - 1) * pageWindow);
        int to = pageWindow;
        if (searchString != null && !searchString.equals("")) {
            list = currentSession().createCriteria(GroupType.class).add(Restrictions.like("groupType", searchString, MatchMode.ANYWHERE)).addOrder(Order.asc("groupType")).setFirstResult(from).setMaxResults(to).list();
       resultCount = (Long) currentSession().createCriteria(GroupType.class).add(Restrictions.like("groupType", searchString, MatchMode.ANYWHERE)).setProjection(Projections.count("id")).uniqueResult();
        } else {
            list = currentSession().createCriteria(GroupType.class).addOrder(Order.asc("groupType")).setFirstResult(from).setMaxResults(to).list();
            resultCount = (Long) currentSession().createCriteria(GroupType.class).setProjection(Projections.count("id")).uniqueResult();
        }
        
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }
}
