package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.util.ResultHolder;
import java.util.List;

/**
 * Organization DAO interface.
 *
 * @author Sujata Aher
 * @author Vivek Bugale
 */
public interface IOrganizationDAO extends IGenericDAO<Organization, Long> {

    /**
     * Get the List of {@link organization}. As per the page number number and
     * page window size.
     *
     * @param pageNumber page number
     * @param pageWindow window size
     * @return {@link ResultHolder}
     */
    public ResultHolder doPaginateOrganizationList(String pageNumber, String pageWindow);

    /**
     * To get List of {@link organization}.
     *
     * @param organizationsIdsList List of organization id's.
     * @return List of Organizations.
     */
    public List<Organization> getListOfOrganizationsByOrganizationIds(List<Long> organizationsIdsList);

    public ResultHolder doPaginateOrganization(String searchString, int pageNumber, int pageWindow);

   /**
     * This method used to fetch Organization by given Organization name.
     *
     * @param organizationName Set organization name
     * @return {@link Organization}
     */
    public Organization getOrganizationByOrganizationName(String organizationName);

}
