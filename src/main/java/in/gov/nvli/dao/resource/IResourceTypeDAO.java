package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;

/**
 * ResourceType DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IResourceTypeDAO extends IGenericDAO<ResourceType, Long> {

    /**
     * doPaginateResourceType method used to do Resource type Register
     * pagination.
     *
     * @param searchString resource type name to search
     * @param pageNumber page number
     * @param pageWindow window size
     * @return resultHolder {@link ResultHolder}
     */
    public ResultHolder doPaginateResourceType(String searchString, int pageNumber, int pageWindow);

    ResourceType getResourceObjectFromMappingResourceType(String mappingResType);

    /**
     * Returns list of resource type according to desc total record count
     *
     * @return list
     */
    public List<ResourceType> getListOfResourceTypeAccToTotalRecordCount();

    /**
     * Get {@link ResourceType} by its primary key/Id
     *
     * @param id {@link ResourceType} primary key/Id
     * @return {@link ResourceType}
     */
    public ResourceType getResourceTypeById(final long id);

    /**
     * Get resource List using resource type code
     *
     * @param resourceTypeCode resource type code
     * @return list of {@link Resource}
     */
    public List<Resource> getResourceByResourceTypeCode(String resourceTypeCode);

    /**
     * get all resource types
     *
     * @return list of resource type object
     * @throws Exception
     */
    public List<ResourceType> getResourceTypes() throws Exception;

    /**
     * Get all resource types and also eagerly load resource type info for
     * localization.
     *
     * @return list of resource type object
     * @throws Exception
     */
    public List<ResourceType> getResourceTypesForLocalization() throws Exception;

    /**
     * Get ResourceType by Resource Type name
     *
     * @param resourceTypeName resource type name
     * @return {@link ResourceType}
     */
    public ResourceType getResourceTypeByName(String resourceTypeName);

    /**
     * Get ResourceType by Resource Type code
     *
     * @param resourceTypeCode resource type code
     * @return {@link ResourceType}
     */
    public ResourceType getResourceTypeByCode(String resourceTypeCode);
}
