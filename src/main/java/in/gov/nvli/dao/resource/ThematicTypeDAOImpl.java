package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.resource.ThematicType;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * ThematicType DAO interface definition
 *
 * @author Suman
 * @version 1
 * @since 1
 */
@Repository
public class ThematicTypeDAOImpl extends GenericDAOImpl<ThematicType, Long> implements IThematicTypeDAO {

    /**
     *
     * @param pageNumber
     * @param pageWindow
     * @return
     */
    @Override
    @Transactional
    public ResultHolder doPaginateThematic(final String pageNumber, final String pageWindow) {
        List list = null;
        long resultCount = 0L;
        int pageNum = Integer.parseInt(pageNumber);
        int pageWin = Integer.parseInt(pageWindow);
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        list = currentSession().createCriteria(ThematicType.class).setFirstResult(from).setMaxResults(to).list();
        resultCount = (Long) currentSession().createCriteria(ThematicType.class).setProjection(Projections.count("id")).uniqueResult();
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    /**
     *
     * @param thematicName
     * @return ThematicType
     */
    @Transactional
    @Override
    public ThematicType getThematicTypeByThematicName(String thematicName) {
        Criteria criteria = currentSession().createCriteria(ThematicType.class);
        criteria.add(Restrictions.eq("thematicType", thematicName));
        return (ThematicType) criteria.uniqueResult();
    }

    /**
     *
     * @param pageNumber
     * @param pageWindow
     * @return
     */
    @Override
    @Transactional
    public ResultHolder doPaginateThematicType(String searchString, int pageNumber, int pageWindow) {
        List list = null;
        long resultCount = 0L;
        int from = ((pageNumber - 1) * pageWindow);
        int to = pageWindow;
        if (searchString != null && !searchString.equals("")) {
            list = currentSession().createCriteria(ThematicType.class).add(Restrictions.like("thematicType", searchString, MatchMode.ANYWHERE)).addOrder(Order.asc("thematicType")).setFirstResult(from).setMaxResults(to).list();
             resultCount = (Long) currentSession().createCriteria(ThematicType.class).add(Restrictions.like("thematicType", searchString, MatchMode.ANYWHERE)).setProjection(Projections.count("id")).uniqueResult();
        } else {
            list = currentSession().createCriteria(ThematicType.class).addOrder(Order.asc("thematicType")).setFirstResult(from).setMaxResults(to).list();
             resultCount = (Long) currentSession().createCriteria(ThematicType.class).setProjection(Projections.count("id")).uniqueResult();
        }
       
        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }
}
