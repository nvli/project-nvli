package in.gov.nvli.dao.resource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.resource.MetadataIntegrator;

/**
 * MetadataIntegrator DAO interface
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface IMetadataIntegratorDAO extends IGenericDAO<MetadataIntegrator, Long> {
    
}
