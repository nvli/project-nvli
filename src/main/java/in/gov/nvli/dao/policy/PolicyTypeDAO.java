/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.policy;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.policy.PolicyType;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public interface PolicyTypeDAO extends IGenericDAO<PolicyType, Long> {
}
