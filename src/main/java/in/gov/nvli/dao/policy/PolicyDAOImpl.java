/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.policy;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.policy.PolicyDocument;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Gulafsha khan <gulafsha@cdac.in>
 */
@Repository
public class PolicyDAOImpl extends GenericDAOImpl<PolicyDocument, Long> implements PolicyDAO {

    private static final Logger LOG = Logger.getLogger(PolicyDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public boolean checkPolicy(Long typeId) {
        Criteria criteria = super.currentSession().createCriteria(PolicyDocument.class);
        criteria.add(Restrictions.eq("policy.typeId", typeId));
        return criteria.list().isEmpty();
    }

    @Override
    public PolicyDocument findMaximumVersion(Long typeId) {
        Criteria criteria = super.currentSession().createCriteria(PolicyDocument.class);
        criteria.add(Restrictions.eq("policy.typeId", typeId));
        criteria.addOrder(Order.desc("policyId"));
        criteria.setMaxResults(1);
        return (PolicyDocument) criteria.uniqueResult();
    }

    @Override
    public List<PolicyDocument> getPoliciesByFiltersWithLimit(String searchString, int dataLimit, int pageNo, Long typeId) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(PolicyDocument.class);
        criteria.add(Restrictions.eq("policy.typeId", typeId));
        criteria.add(Restrictions.like("policyTittle", searchString, MatchMode.ANYWHERE));
        criteria.addOrder(Order.desc("lastModifiedDate"));
        criteria.setFirstResult((pageNo - 1) * dataLimit);
        criteria.setMaxResults(dataLimit);
        return criteria.list();
    }

    @Override
    public long getPoliciesTotalCountByFilters(String searchString, Long typeId) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(PolicyDocument.class);
        criteria.add(Restrictions.eq("policy.typeId", typeId));
        criteria.add(Restrictions.like("policyTittle", searchString, MatchMode.ANYWHERE));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }
}
