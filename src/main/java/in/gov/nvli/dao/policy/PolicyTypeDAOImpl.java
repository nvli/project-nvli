/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.policy;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.policy.PolicyType;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Repository
public class PolicyTypeDAOImpl extends GenericDAOImpl<PolicyType, Long> implements PolicyTypeDAO {

    private static final Logger LOG = Logger.getLogger(PolicyType.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
