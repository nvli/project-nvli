/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.policy;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.policy.PolicyDocument;
import java.util.List;

/**
 * This interface used for privacy and terms and conditions releted policies
 *
 * @author Gulafsha khan<gulafsha@cdac.in>
 */
public interface PolicyDAO extends IGenericDAO<PolicyDocument, Long> {
    /**
     * method used for checking whether policy is terms and condition type of privacy policy type
     * @param typeId
     * @return
     */

    public boolean checkPolicy(Long typeId);

    /**
     * It will give last uploaded policy document
     * @param typeId
     * @return
     */
    public PolicyDocument findMaximumVersion(Long typeId);

    /**
     * this method provide policy according to pagination and given search string
     * @param searchString
     * @param dataLimit
     * @param pageNo
     * @param typeId
     * @return
     * @throws Exception
     */
    public List<PolicyDocument> getPoliciesByFiltersWithLimit(String searchString, int dataLimit, int pageNo, Long typeId) throws Exception;

    /**
     * this method return total count of policies being uploaded according to given policy type 
     * @param searchString
     * @param typeId
     * @return
     * @throws Exception
     */
    public long getPoliciesTotalCountByFilters(String searchString, Long typeId) throws Exception;

}
