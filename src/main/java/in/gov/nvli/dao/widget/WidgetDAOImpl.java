package in.gov.nvli.dao.widget;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.widget.Widget;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sanjay Rabidas <rabidassanjay@gmail.com>
 */
@Repository
public class WidgetDAOImpl extends GenericDAOImpl<Widget, Long> implements WidgetDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Widget getWidgetByName(String widgetName) {
        Criteria c = super.currentSession().createCriteria(Widget.class);
        c.add(Restrictions.eq("widgetName", widgetName));
        return (Widget) c.uniqueResult();
    }
}
