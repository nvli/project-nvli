/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.widget;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.widget.CalenderEvent;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Repository
public class CalenderEventDAOImpl extends GenericDAOImpl<CalenderEvent, Long> implements CalenderEventDAO {

    @Override
    public List<CalenderEvent> getEventByFiltersWithLimit(String searchString, int dataLimit, int pageNo) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CalenderEvent.class);
        if (!"null".equals(searchString)) {
            criteria.add(Restrictions.like("title", searchString, MatchMode.ANYWHERE));
        }
        criteria.addOrder(Order.asc("startDate"));
        criteria.setFirstResult((pageNo - 1) * dataLimit);
        criteria.setMaxResults(dataLimit);
        return criteria.list();
    }

    @Override
    public long getEventsTotalCountByFilters(String searchString) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CalenderEvent.class);
        if (!"null".equals(searchString)) {
            criteria.add(Restrictions.like("title", searchString, MatchMode.ANYWHERE));
        }
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }
}
