package in.gov.nvli.dao.widget;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.widget.Widget;

/**
 *
 * @author Sanjay Rabidas <rabidassanjay@gmail.com>
 */

public interface WidgetDAO extends IGenericDAO<Widget, Long>  {
    public Widget getWidgetByName(String widgetName);
}
