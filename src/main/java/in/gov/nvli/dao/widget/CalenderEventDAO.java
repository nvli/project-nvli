/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.widget;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.widget.CalenderEvent;
import java.util.List;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
public interface CalenderEventDAO extends IGenericDAO<CalenderEvent, Long> {

    public List<CalenderEvent> getEventByFiltersWithLimit(String searchString, int dataLimit, int pageNo) throws Exception;

    public long getEventsTotalCountByFilters(String searchString) throws Exception;
}
