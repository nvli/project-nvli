package in.gov.nvli.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Generic DAO interface definition
 *
 * @param <T> The class of the pojo being persisted.
 * @param <ID> the class of the pojo's id property
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Transactional
public class GenericDAOImpl<T, ID extends Serializable> implements IGenericDAO<T, ID> {

    /**
     * The class of the pojo being persisted.
     */
    protected Class<T> persistentClass;
    protected SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Get the current session from the seesionFactory.
     *
     * @return Session{@link Session}
     */
    public Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Constructor: Checking for the right type since we are using cglib proxy
     * if catch block's code modified it will throw ClassCastException due to
     * Parameterized Type Conversion of proxy.
     *
     */
    public GenericDAOImpl() {
        try {
            this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0];
        } catch (Exception e) {
            try {
                this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            } catch (Exception ex) {
                this.persistentClass = (Class<T>) ((TypeVariable) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getGenericDeclaration().getTypeParameters()[0].getBounds()[0];
            }
        }
    }

    /**
     * Get the Persistent class
     *
     * @return
     */
    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    /**
     * Create New entity
     *
     * @param entity
     * @return boolean
     * @throws DataIntegrityViolationException
     */
    @Override
    @Transactional(rollbackFor = {DataIntegrityViolationException.class}, readOnly = false)
    public boolean createNew(T entity) throws DataIntegrityViolationException {
        Session session = currentSession();
        boolean isCreated = session.save(entity) != null;
        return isCreated;
    }

    /**
     * @see in.gov.nvli.dao.generic.IGenericDAO#get(java.io.Serializable)
     */
    @Override
    @Transactional(readOnly = true)
    public T get(ID idd) {
        return (T) currentSession().get(persistentClass, idd);
    }

    /**
     * @return @see in.gov.nvli.dao.generic.IGenericDAO#merge()
     */
    @Override
    @Transactional(readOnly = false)
    public T merge(T entity) {
        return (T) currentSession().merge(entity);
    }

    /**
     * @return @see in.gov.nvli.dao.generic.IGenericDAO#merge()
     */
    @Override
    @Transactional(readOnly = false)
    public void update(T entity) {
        currentSession().update(entity);
    }

    /**
     * @see in.gov.nvli.dao.generic.IGenericDAO#list()
     */
    @Override
    @Transactional(readOnly = true)
    public List<T> list() {
        return currentSession().createCriteria(persistentClass).list();
    }

    /**
     * @see in.gov.nvli.dao.generic.IGenericDAO#saveOrUpdate(java.lang.Object)
     *
     */
    @Override
    public void saveOrUpdate(T entity) {
        currentSession().saveOrUpdate(entity);
    }

    /**
     * @see in.gov.nvli.dao.generic.IGenericDAO#delete(java.lang.Object)
     */
    @Override
    @Transactional
    public void delete(T entity) {
        System.out.println("entity " + entity.toString());
        currentSession().delete(entity);
    }

    /**
     * @see in.gov.nvli.dao.generic.IGenericDAO#exist(java.io.Serializable)
     */
    @Override
    @Transactional(readOnly = true)
    public boolean exist(ID idd) {
        return currentSession().get(persistentClass, idd) != null;
    }

    /**
     * @see in.gov.nvli.dao.generic.GenericDAOImpl#update(java.lang.String,
     * java.lang.Object[])
     * @param queryName
     * @param b
     */
    @Override
    public void update(String queryName, Object... obj) {
        currentSession().update(queryName, obj);

    }

    /**
     * Flush
     */
    @Override
    public void flush() {
        currentSession().flush();
    }

    /**
     * Get list of T using object b
     *
     * @param queryName
     * @param values
     * @return list of T
     */
    @Override
    @Transactional(readOnly = false)
    public List<T> findbyQuery(String queryName, Object... values) {
        Session session = currentSession();
        Query queryObject = session.getNamedQuery(queryName);
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                queryObject.setParameter(i, values[i]);
            }
        }
        return queryObject.list();
    }
}
