/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.rewards.RewardsPointDetails;
import in.gov.nvli.domain.rewards.UserRewardsSummary;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gulafsha
 */
@Repository
public class RewardsPointDetailsDAOImpl extends GenericDAOImpl<RewardsPointDetails, Long> implements RewardsPointDetailsDAO {

    private static final Logger LOG = Logger.getLogger(RewardsPointDetailsDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public RewardsPointDetails fetchPointsDetailsByActivity(String activityCode) {
        Criteria criteria = super.currentSession().createCriteria(RewardsPointDetails.class)
                .add(Restrictions.eq("activityCode", activityCode))
                .add(Restrictions.eq("isCurrent", true));
        return (RewardsPointDetails) criteria.uniqueResult();
    }

    @Override
    public List<RewardsPointDetails> getRewardPointDetails() {
        Criteria criteria = super.currentSession().createCriteria(RewardsPointDetails.class)
                .add(Restrictions.eq("isCurrent", true));
        return criteria.list();
    }
}
