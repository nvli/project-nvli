/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.rewards.LevelDetails;
import in.gov.nvli.domain.rewards.UserRewardsSummary;
import in.gov.nvli.domain.user.User;

/**
 *
 * @author Gulafsha
 */
public interface UserRewardsSummaryDAO extends IGenericDAO<UserRewardsSummary, Long> {

    public UserRewardsSummary getUserRewardSummeryByUser(User userObj);

    public int getUserEditPoints(User user);

    public LevelDetails findCurrentLevelByUser(User user);
}
