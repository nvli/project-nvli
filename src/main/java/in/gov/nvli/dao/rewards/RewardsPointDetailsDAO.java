/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.rewards.RewardsPointDetails;
import java.util.List;

/**
 *
 * @author Gulafsha
 */
public interface RewardsPointDetailsDAO extends IGenericDAO<RewardsPointDetails, Long> {

    public RewardsPointDetails fetchPointsDetailsByActivity(String activityCode);

    public List<RewardsPointDetails> getRewardPointDetails();
}
