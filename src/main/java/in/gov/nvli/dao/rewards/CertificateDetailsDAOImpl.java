package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.rewards.BadgeDetails;
import in.gov.nvli.domain.rewards.CertificateDetails;
import in.gov.nvli.domain.user.User;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Vivek Bugale
 * @author Gulafsha
 */
@Repository
public class CertificateDetailsDAOImpl extends GenericDAOImpl<CertificateDetails, Long> implements CertificateDetailsDAO {

    private static final Logger LOG = Logger.getLogger(CertificateDetailsDAOImpl.class);

    @Override
    public CertificateDetails getLastCertificate(User userObj) {
        Criteria criteria = super.currentSession().createCriteria(CertificateDetails.class)
                .createAlias("badge", "badgeDetails")
                .add(Restrictions.eq("user.id", userObj.getId()))
                .addOrder(Order.desc("badgeDetails.badgeWeight"))
                .setMaxResults(1);
        CertificateDetails certificateDetails = (CertificateDetails) criteria.uniqueResult();
        return certificateDetails;
    }

    @Override
    public Long findCertificateIdByUserIdAndBadgeId(Long userId, BadgeDetails badge) {
        Criteria criteria = super.currentSession().createCriteria(CertificateDetails.class)
                .setProjection(Projections.property("certificateId"))
                .add(Restrictions.eq("user.id", userId))
                .add(Restrictions.eq("badge", badge));
        return (Long) criteria.uniqueResult();
    }
}
