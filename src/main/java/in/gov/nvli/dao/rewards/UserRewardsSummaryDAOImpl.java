/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.rewards.LevelDetails;
import in.gov.nvli.domain.rewards.UserRewardsSummary;
import in.gov.nvli.domain.user.User;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gulafsha
 */
@Repository
public class UserRewardsSummaryDAOImpl extends GenericDAOImpl<UserRewardsSummary, Long> implements UserRewardsSummaryDAO {

    private static final Logger LOG = Logger.getLogger(UserRewardsSummaryDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public UserRewardsSummary getUserRewardSummeryByUser(User userObj) {
        Criteria criteria = super.currentSession().createCriteria(UserRewardsSummary.class);
        criteria.add(Restrictions.eq("userId.id", userObj.getId()));
        UserRewardsSummary u = (UserRewardsSummary) criteria.uniqueResult();
        if (null != u) {
            return u;

        } else {
            return null;
        }
    }

    @Override
    public int getUserEditPoints(User user) {
        Criteria criteria = super.currentSession().createCriteria(UserRewardsSummary.class);
        criteria.add(Restrictions.eq("userId.id", user.getId()));
        UserRewardsSummary userRewardsSummary = (UserRewardsSummary) criteria.uniqueResult();
        System.out.println("dao edit pointsss" + userRewardsSummary.getEditPoints());
        return userRewardsSummary.getEditPoints();
    }

    @Override
    public LevelDetails findCurrentLevelByUser(User user) {
        Criteria criteria = super.currentSession().createCriteria(UserRewardsSummary.class);
        criteria.add(Restrictions.eq("userId.id", user.getId()));
        UserRewardsSummary userRewardsSummary = (UserRewardsSummary) criteria.uniqueResult();
        return userRewardsSummary.getCurrentLevel();
    }
}
