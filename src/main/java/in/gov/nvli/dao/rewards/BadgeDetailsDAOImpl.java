package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.rewards.BadgeDetails;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gulafsha
 */
@Repository
public class BadgeDetailsDAOImpl extends GenericDAOImpl<BadgeDetails, Long> implements BadgeDetailsDAO {

    private static final Logger LOG = Logger.getLogger(BadgeDetailsDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public BadgeDetails getBadgeByBadgeCode(String badgeAwarded) {
        Criteria criteria = super.currentSession().createCriteria(BadgeDetails.class);
        criteria.add(Restrictions.eq("badgeCode", badgeAwarded));
        return (BadgeDetails) criteria.uniqueResult();
    }

    @Override
    public BadgeDetails findBadgeByWeight(int weight) {
        Criteria criteria = super.currentSession().createCriteria(BadgeDetails.class);
        criteria.add(Restrictions.eq("badgeWeight", weight));
        return (BadgeDetails) criteria.uniqueResult();
    }
}
