/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.rewards.BadgeDetails;
import in.gov.nvli.domain.rewards.CertificateDetails;
import in.gov.nvli.domain.user.User;

/**
 *
 * @author Gulafsha
 */
public interface CertificateDetailsDAO extends IGenericDAO<CertificateDetails, Long> {

    public CertificateDetails getLastCertificate(User userobj);

    public Long findCertificateIdByUserIdAndBadgeId(Long userId, BadgeDetails badge);
}
