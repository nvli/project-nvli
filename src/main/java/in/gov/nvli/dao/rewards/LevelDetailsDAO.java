/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.rewards.LevelDetails;
import java.util.List;

/**
 *
 * @author Gulafsha
 */
public interface LevelDetailsDAO extends IGenericDAO<LevelDetails, Long> {

    public List<LevelDetails> fetchLevelDetails();

    public LevelDetails findInitialLevelBylevelId(Long currentlevelId);

    public LevelDetails findCurrentLevel(int editPoints, int approvedPoints);

    public List<LevelDetails> fetchAllLevels();

    public LevelDetails getLastLevel();
}
