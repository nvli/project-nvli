/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.rewards.StarsDetails;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gulafsha
 */
@Repository
public class StarsDetailsDAOImpl extends GenericDAOImpl<StarsDetails, Long> implements StarsDetailsDAO {

    private static final Logger LOG = Logger.getLogger(StarsDetailsDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public StarsDetails getStarsByCode(String starsAwarded) {
        Criteria criteria = super.currentSession().createCriteria(StarsDetails.class);
        criteria.add(Restrictions.eq("starsCode", starsAwarded));
        return (StarsDetails) criteria.uniqueResult();
    }
}
