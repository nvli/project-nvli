/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.rewards.LevelDetails;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gulafsha
 */
@Repository
public class LevelDetailsDAOImpl extends GenericDAOImpl<LevelDetails, Long> implements LevelDetailsDAO {

    private static final Logger LOG = Logger.getLogger(LevelDetailsDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public List<LevelDetails> fetchLevelDetails() {
        Criteria criteria = super.currentSession().createCriteria(LevelDetails.class);
        criteria.addOrder(Order.asc("levelId"));
        return criteria.list();

    }

    @Override
    public LevelDetails findInitialLevelBylevelId(Long currentlevelId) {
        Criteria criteria = super.currentSession().createCriteria(LevelDetails.class)
                .add(Restrictions.eq("levelId", currentlevelId))
                .add(Restrictions.eq("isCurrent", true));
        return (LevelDetails) criteria.uniqueResult();
    }

    @Override
    public LevelDetails findCurrentLevel(int editPoints, int approvedPoints) {
        Criteria criteria = super.currentSession().createCriteria(LevelDetails.class)
                .add(Restrictions.eq("isCurrent", true))
                .add(Restrictions.le("editPoints", editPoints))
                .add(Restrictions.le("approvedPoints", approvedPoints))
                .addOrder(Order.desc("editPoints"))
                .addOrder(Order.desc("approvedPoints"))
                .setMaxResults(1);
        LevelDetails levelDetails = (LevelDetails) criteria.uniqueResult();
        return levelDetails;

    }

    @Override
    public List<LevelDetails> fetchAllLevels() {
        Criteria criteria = super.currentSession().createCriteria(LevelDetails.class)
                .add(Restrictions.eq("isCurrent", true));
        return criteria.list();

    }

    @Override
    public LevelDetails getLastLevel() {
        Criteria criteria = super.currentSession().createCriteria(LevelDetails.class)
                .addOrder(Order.desc("approvedPoints"))
                .addOrder(Order.desc("editPoints"))
                .setMaxResults(1);
        LevelDetails levelDetails = (LevelDetails) criteria.uniqueResult();
        System.out.println("level" + levelDetails.getLevelName());
        return levelDetails;

    }
}
