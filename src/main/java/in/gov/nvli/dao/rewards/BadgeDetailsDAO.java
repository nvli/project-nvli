package in.gov.nvli.dao.rewards;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.rewards.BadgeDetails;

/**
 *
 * @author Gulafsha
 */
public interface BadgeDetailsDAO extends IGenericDAO<BadgeDetails, Long> {

    public BadgeDetails getBadgeByBadgeCode(String badgeAwarded);

    public BadgeDetails findBadgeByWeight(int weight);

}
