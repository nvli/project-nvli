package in.gov.nvli.dao.dataimport;

import org.springframework.transaction.annotation.Transactional;
import java.sql.Connection;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Repository;

@Repository
public class DataImportDAO implements IDataImportDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public boolean udcDataImportInDB() throws Exception {
        DataSource dataSource = SessionFactoryUtils.getDataSource(sessionFactory);
        ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
        rdp.addScript(new ClassPathResource("schema-create-udc-tables.sql"));
        rdp.setContinueOnError(true);
        rdp.setSqlScriptEncoding("utf8");
        Connection connection = dataSource.getConnection();
        rdp.populate(connection); // this starts the script execution, in the order as added
        return true;
    }

    @Override
    @Transactional
    public boolean marc21DataImportInDB() throws Exception {
        DataSource dataSource = SessionFactoryUtils.getDataSource(sessionFactory);
        ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
        rdp.addScript(new ClassPathResource("schema-create-marc21-tables.sql"));
        rdp.setContinueOnError(true);
        rdp.setSqlScriptEncoding("utf8");
        Connection connection = dataSource.getConnection();
        rdp.populate(connection); // this starts the script execution, in the order as added
        return true;
    }

    @Override
    public boolean userProfessionDataImportInDB() throws Exception {
        DataSource dataSource = SessionFactoryUtils.getDataSource(sessionFactory);
        ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
        rdp.addScript(new ClassPathResource("schema-create-user-profession.sql"));
        rdp.setContinueOnError(true);
        rdp.setSqlScriptEncoding("utf8");
        Connection connection = dataSource.getConnection();
        rdp.populate(connection); // this starts the script execution, in the order as added
        return true;
    }

    @Override
    public boolean themeDataImportInDB() throws Exception {
        DataSource dataSource = SessionFactoryUtils.getDataSource(sessionFactory);
        ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();
        rdp.addScript(new ClassPathResource("schema-create-theme.sql"));
        rdp.setContinueOnError(true);
        rdp.setSqlScriptEncoding("utf8");
        Connection connection = dataSource.getConnection();
        rdp.populate(connection); // this starts the script execution, in the order as added
        return true;
    }
}
