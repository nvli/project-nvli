package in.gov.nvli.dao.dataimport;

/**
 * Interface for LanguageDAO
 *
 * @author Ritesh MAlviya
 * @version 1
 * @since 1
 */
public interface IDataImportDAO {

    public boolean udcDataImportInDB() throws Exception;

    public boolean marc21DataImportInDB() throws Exception;

    public boolean userProfessionDataImportInDB() throws Exception;

    public boolean themeDataImportInDB() throws Exception;
}
