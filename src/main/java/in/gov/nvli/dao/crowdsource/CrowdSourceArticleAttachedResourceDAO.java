package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticleAttachedResource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class CrowdSourceArticleAttachedResourceDAO extends GenericDAOImpl<CrowdSourceArticleAttachedResource, Long> implements ICrowdSourceArticleAttachedResourceDAO {

}
