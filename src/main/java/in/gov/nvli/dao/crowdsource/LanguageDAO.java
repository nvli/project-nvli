package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.util.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Suman
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class LanguageDAO extends GenericDAOImpl<UDCLanguage, Long> implements ILanguageDAO {

    @Override
    @Transactional
    public List<UDCLanguage> getIndianLanguageList() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCLanguage.class);
        criteria.add(Restrictions.eq("isIndianLanguage", Byte.parseByte("1")));
        criteria.addOrder(Order.asc("languageName"));
        return criteria.list();
    }

    @Override
    @Transactional
    public List<UDCLanguage> getAllUDCLanguageList() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCLanguage.class);
        criteria.addOrder(Order.asc("languageName"));
        return criteria.list();
    }

    /**
     * doPaginateLanguage method used to do language list pagination.
     *
     * @param searchString
     * @param pageNum
     * @param pageWin
     * @return resultHolder {@link ResultHolder}
     */
    @Override
    @Transactional
    public ResultHolder doPaginateLanguageList(String searchString, int pageNum, int pageWin) {
        List list = null;
        long resultCount = 0L;
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        if (searchString != null && !searchString.equals("")) {
            list = currentSession().createCriteria(UDCLanguage.class).add(Restrictions.like("languageName", searchString, MatchMode.ANYWHERE)).addOrder(Order.asc("languageName")).setFirstResult(from).setMaxResults(to).list();
             resultCount = (Long) currentSession().createCriteria(UDCLanguage.class).add(Restrictions.like("languageName", searchString, MatchMode.ANYWHERE)).setProjection(Projections.count("id")).uniqueResult();
        } else {
            list = currentSession().createCriteria(UDCLanguage.class).addOrder(Order.asc("languageName")).setFirstResult(from).setMaxResults(to).list();
             resultCount = (Long) currentSession().createCriteria(UDCLanguage.class).setProjection(Projections.count("id")).uniqueResult();
        }

        ResultHolder resultHolder = new ResultHolder(list, resultCount);
        return resultHolder;
    }

    @Override
    public UDCLanguage getObjectByCode(String langCode) {
        return (UDCLanguage) super.currentSession().createCriteria(UDCLanguage.class).add(Restrictions.eq("languageCode", langCode)).uniqueResult();
    }

    @Override
    public List<UDCLanguage> getLanguageListByLangCodeList(String[] langCodeList) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCLanguage.class);
        criteria.add(Restrictions.in("languageCode", langCodeList));
        criteria.addOrder(Order.asc("languageName"));
        return criteria.list();
    }

    @Override
    @Transactional
    public UDCLanguage getLanguageByLanguageName(String langName) {
        Criteria criteria = currentSession().createCriteria(UDCLanguage.class);
        criteria.add(Restrictions.eq("languageName", langName));
        return (UDCLanguage) criteria.uniqueResult();

    }
}
