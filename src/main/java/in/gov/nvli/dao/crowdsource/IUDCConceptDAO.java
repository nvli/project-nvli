package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import java.util.List;
import java.util.Map;

/**
 * Dao Interface for {@link UDCConcept} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface IUDCConceptDAO extends IGenericDAO<UDCConcept, Long> {

    /**
     * This method used to fetch UDC description classification grouped by
     * language.
     *
     * @return Map,key as language id and value as list of
     * {@link UDCConceptDescription}
     */
    public Map<Long, List<UDCConceptDescription>> getUDCClassification();
}
