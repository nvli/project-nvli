package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.util.ResultHolder;
import java.util.List;

/**
 * Dao Interface for {@link UDCLanguage} domain.
 *
 * @author Suman Behara
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface ILanguageDAO extends IGenericDAO<UDCLanguage, Long> {

    /**
     * This method used to fetch all indian languages.
     *
     * @return list of {@link UDCLanguage}
     * @throws Exception
     */
    public List<UDCLanguage> getIndianLanguageList() throws Exception;

    /**
     * This method used to fetch all languages.
     *
     * @return list of {@link UDCLanguage}
     * @throws Exception
     */
    public List<UDCLanguage> getAllUDCLanguageList() throws Exception;

    /**
     *
     * @param searchString
     * @param pageNumber
     * @param pageWindow
     * @return
     */
    public ResultHolder doPaginateLanguageList(String searchString, int pageNumber, int pageWindow);

    /**
     * This method used to fetch language by given language code.
     *
     * @param langCode Set language code
     * @return {@link UDCLanguage}
     */
    public UDCLanguage getObjectByCode(String langCode);

    /**
     * This method used to fetch languages by given language code list.
     *
     * @param langCodeList Set list of language code
     * @return list of {@link UDCLanguage}
     * @throws Exception
     */
    public List<UDCLanguage> getLanguageListByLangCodeList(String[] langCodeList) throws Exception;

    /**
     * This method used to fetch language by given language name.
     *
     * @param langName Set language name
     * @return {@link UDCLanguage}
     */
    public UDCLanguage getLanguageByLanguageName(String langName);
}
