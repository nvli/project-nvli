package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import java.util.List;

/**
 * Dao Interface for {@link CrowdSourceArticle} domain.
 *
 * @author Ritesh Malviya
 * @author Vivek
 * @version 1
 * @since 1
 */
public interface ICrowdSourceArticleDAO extends IGenericDAO<CrowdSourceArticle, Long> {

    /**
     * This method used to fetch user article with filter and pagination.
     *
     * @param userId Set id of user
     * @param searchString Set search string
     * @param statusTypeFilter Set status filter
     * @param dataLimit Set data limit
     * @param pageNo Set page no
     * @param isRequiredUserRestriction Set user restriction required or not
     * @return List of {@link CrowdSourceArticle}
     * @throws Exception
     */
    public List<CrowdSourceArticle> getArticlesByFiltersWithLimit(long userId, String searchString, String statusTypeFilter, int dataLimit, int pageNo, boolean isRequiredUserRestriction) throws Exception;

    /**
     * This method used to fetch total count of user article with filter.
     *
     * @param userId Set id of user
     * @param searchString Set search string
     * @param statusTypeFilter Set status filter
     * @param isRequiredUserRestriction Set user restriction required or not
     * @return Total count
     * @throws Exception
     */
    public long getArticlesTotalCountByFilters(long userId, String searchString, String statusTypeFilter, boolean isRequiredUserRestriction) throws Exception;

    /**
     * This method used to fetch article by given article ids.
     *
     * @param articleIds Set list of article ids
     * @return List of {@link CrowdSourceArticle}
     * @throws Exception
     */
    public List<CrowdSourceArticle> getArticleList(List<Long> articleIds) throws Exception;
}
