package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;

/**
 * Dao Interface for {@link CrowdSourceEdit} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface ICrowdSourceEditDAO extends IGenericDAO<CrowdSourceEdit, Long> {

}
