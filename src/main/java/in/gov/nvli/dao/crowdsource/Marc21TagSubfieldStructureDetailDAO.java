package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.Marc21TagSubfieldStructureDetail;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class Marc21TagSubfieldStructureDetailDAO extends GenericDAOImpl<Marc21TagSubfieldStructureDetail, Long> implements IMarc21TagSubfieldStructureDetailDAO {

    @Override
    @Transactional
    public List<Marc21TagSubfieldStructureDetail> getMarc21TagsSuggestions(String tagField) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Marc21TagSubfieldStructureDetail.class);
        criteria.add(Restrictions.like("tagField", tagField + "%"));
        criteria.addOrder(Order.asc("tagField"));
        return criteria.list();
    }

    @Override
    public List<Marc21TagSubfieldStructureDetail> getMarc21DescriptionSuggestions(String librarianTitle) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Marc21TagSubfieldStructureDetail.class).add(Restrictions.like("librarianTitle", librarianTitle + "%")).addOrder(Order.asc("librarianTitle"));
        return criteria.list();
    }
}
