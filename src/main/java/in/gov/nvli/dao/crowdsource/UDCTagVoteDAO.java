package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCTagVote;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class UDCTagVoteDAO extends GenericDAOImpl<UDCTagVote, Long> implements IUDCTagVoteDAO {

    @Override
    @Transactional
    public boolean isVoted(long udcConceptId, long languageId, long userId) throws Exception {
        List<Long> udcTagsCrowdsourceIds = new ArrayList<>();
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class)
                .add(Restrictions.eq("udcConcept.id", udcConceptId))
                .add(Restrictions.eq("udcLanguage.id", languageId))
                .setProjection(Projections.property("id"));

        for (Object obj : criteria.list()) {
            udcTagsCrowdsourceIds.add((Long) obj);
        }

        if (udcTagsCrowdsourceIds.isEmpty()) {
            return false;
        }

        criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagVote.class)
                .add(Restrictions.eq("user.id", userId))
                .add(Restrictions.in("udcTagsCrowdsource.id", udcTagsCrowdsourceIds));

        return criteria.uniqueResult() != null;
    }
}
