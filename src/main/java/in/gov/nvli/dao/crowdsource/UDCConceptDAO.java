package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.application.listeners.ContextLoaderListener;
import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class UDCConceptDAO extends GenericDAOImpl<UDCConcept, Long> implements IUDCConceptDAO {

    @Override
    @Transactional
    public Map<Long, List<UDCConceptDescription>> getUDCClassification() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCConcept.class);
        criteria.add(Restrictions.eq("id", 2450l));
        UDCConcept udcConcept = (UDCConcept) criteria.uniqueResult();
        Hibernate.initialize(udcConcept.getUdcConceptSet());

        Map<Long, List<UDCConceptDescription>> udcClassificationMap = new HashMap<>();
        try {
            List<UDCConceptDescription> resultList;
            TreeSet<UDCConcept> udcConceptSet = new TreeSet<>(udcConcept.getUdcConceptSet());
            UDCConceptDescription tempUDCConceptDescription;
            for (UDCConcept tempUDCConcept : udcConceptSet) {
                Hibernate.initialize(tempUDCConcept.getUdcConceptDescriptionSet());
                for (UDCConceptDescription udcConceptDescription : tempUDCConcept.getUdcConceptDescriptionSet()) {
                    if (udcConceptDescription.getUdcTermId().getId().equals(1l)) {
                        tempUDCConceptDescription = new UDCConceptDescription();
                        tempUDCConceptDescription.setDescription(udcConceptDescription.getDescription());
                        tempUDCConceptDescription.setId(tempUDCConcept.getId());
                        if (udcClassificationMap.containsKey(udcConceptDescription.getLanguageId().getId())) {
                            resultList = udcClassificationMap.get(udcConceptDescription.getLanguageId().getId());
                        } else {
                            resultList = new ArrayList<>();
                            udcClassificationMap.put(udcConceptDescription.getLanguageId().getId(), resultList);
                        }
                        resultList.add(tempUDCConceptDescription);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ContextLoaderListener.class).error(ex.getMessage(), ex);
        }
        return udcClassificationMap;
    }
}
