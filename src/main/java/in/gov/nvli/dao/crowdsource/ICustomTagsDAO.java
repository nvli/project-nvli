package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CustomTags;
import java.util.List;

/**
 * Dao Interface for {@link CustomTags} domain.
 *
 * @author Doppa Srinivas
 * @version 1
 * @since 1
 */
public interface ICustomTagsDAO extends IGenericDAO<CustomTags, Long> {

    /**
     * This method used to fetch custom tag object using given tag and language
     *
     * @param tag Set tag
     * @param languageId Set language id
     * @return {@link CustomTags}
     * @throws Exception
     */
    public CustomTags getCustomTag(String tag, long languageId) throws Exception;

    /**
     * This method used to fetch custom tag suggestions by given language and
     * prefix search string.
     *
     * @param languageId Set language id
     * @param tagPrefix Set prefix search string
     * @return list of {@link CustomTags}
     * @throws Exception
     */
    public List<CustomTags> getCustomTagsSuggestions(String languageId, String tagPrefix) throws Exception;
}
