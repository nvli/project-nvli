package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCTagVote;

/**
 * Dao Interface for {@link UDCTagVote} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface IUDCTagVoteDAO extends IGenericDAO<UDCTagVote, Long> {

    /**
     * This method used to check whether user vote or not for given UDC tag and
     * language id
     *
     * @param udcConceptId Set UDC id
     * @param languageId Set language id
     * @param userId Set user id
     * @return true if user voted otherwise false
     * @throws Exception
     */
    public boolean isVoted(long udcConceptId, long languageId, long userId) throws Exception;
}
