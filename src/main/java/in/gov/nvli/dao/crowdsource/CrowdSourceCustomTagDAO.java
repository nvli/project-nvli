package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.user.User;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Doppa Srinivas
 */
@Repository
public class CrowdSourceCustomTagDAO extends GenericDAOImpl<CrowdSourceCustomTag, Long> implements ICrowdSourceCustomTagDAO {

    @Override
    @Transactional
    public CrowdSourceCustomTag getCrowdSourceCustomTag(String recordIdentifier, User user) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceCustomTag.class);
        criteria.add(Restrictions.eq("recordIdentifier", recordIdentifier));
        criteria.add(Restrictions.eq("user", user));
        return (CrowdSourceCustomTag) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public List<CrowdSourceCustomTag> getuserTagsPaginated(long userId, int pageNumber, int pageWindow) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceCustomTag.class).createAlias("user", "userObj");
        criteria.add(Restrictions.eq("userObj.id", userId));
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        return criteria.list();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public CrowdSourceCustomTag saveOrUpdateCrowdSourceCustomTag(CrowdSourceCustomTag crowdSourceCustomTag) throws Exception {
        sessionFactory.getCurrentSession().saveOrUpdate(crowdSourceCustomTag);
        return crowdSourceCustomTag;
    }

}
