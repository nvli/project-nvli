package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.crowdsource.UDCTranslationExperts;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ritesh Malviya
 * @author Vivek Bugale
 *
 */
@Repository
public class UDCTranslationExpertDAO extends GenericDAOImpl<UDCTranslationExperts, Long> implements IUDCTranslationExpertDAO {

    @Override
    public UDCTranslationExperts checkEntryPresent(UDCConceptDescription udcConceptDiscription, UDCLanguage udcLanguage) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTranslationExperts.class)
                .add(Restrictions.eq("udcConceptDescription.id", udcConceptDiscription.getId()))
                .add(Restrictions.eq("udcLanguage.id", udcLanguage.getId()));
            return (UDCTranslationExperts) criteria.uniqueResult();
    }
}
