package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.Marc21TagStructureDetail;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class Marc21TagStructureDetailDAO extends GenericDAOImpl<Marc21TagStructureDetail, Long> implements IMarc21TagStructureDetailDAO {

}
