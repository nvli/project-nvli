package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import in.gov.nvli.domain.crowdsource.UDCTranslationExperts;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class UDCConceptDescriptionDAO extends GenericDAOImpl<UDCConceptDescription, Long> implements IUDCConceptDescriptionDAO {

    @Autowired
    private IUDCTermDAO uDCTermDAO;

    @Autowired
    private IUDCConceptDAO uDCConceptDAO;

    @Autowired
    private ILanguageDAO languageDAO;

    @Override
    @Transactional
    public List<UDCConceptDescription> getUDCTagsSuggestions(String languageId, String tagPrefix) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class);
        criteria.add(Restrictions.eq("languageId.id", Long.parseLong(languageId)));
        criteria.add(Restrictions.eq("udcTermId.id", Long.parseLong("1")));
        criteria.add(Restrictions.like("description", tagPrefix + "%"));
        criteria.addOrder(Order.asc("description"));
        return criteria.list();
    }

    @Override
    @Transactional
    public UDCConceptDescription getUUDCConceptDescription(UDCConcept udcConcept, long udcLanguageId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class);
        criteria.add(Restrictions.eq("languageId.id", udcLanguageId));
        criteria.add(Restrictions.eq("udcTermId.id", Long.parseLong("1")));
        criteria.add(Restrictions.eq("udcConceptId", udcConcept));
        return (UDCConceptDescription) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public Map<String, String> getUDCTagDescription(List<String> notations, long languageId, boolean isReplaceByEnglish) throws Exception {
        Map<String, String> map = new HashMap<>();
        List<Long> languages = new ArrayList<>();
        languages.add(languageId);

        if (isReplaceByEnglish) {
            languages.add(40l);
        }

        Order order;
        if (languageId < 40l) {
            order = Order.desc("li.id");
        } else {
            order = Order.asc("li.id");
        }

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class)
                .createAlias("udcConceptId", "uci")
                .createAlias("languageId", "li")
                .createAlias("udcTermId", "ti")
                .add(Restrictions.in("uci.udcNotation", notations))
                .add(Restrictions.in("li.id", languages))
                .add(Restrictions.eq("ti.id", 1l))
                .addOrder(order)
                .setProjection(Projections.projectionList().add(Projections.property("uci.udcNotation")).add(Projections.property("description")));

        for (Object obj : criteria.list()) {
            Object[] objects = (Object[]) obj;
            map.put((String) objects[0], (String) objects[1]);
        }
        return map;
    }

    @Override
    @Transactional
    public List<UDCConceptDescription> getUntranslatedUDCTagDescription(String parentId, long languageId, long translationId) throws Exception {

        List<Long> udcConceptIds = new ArrayList<>();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class)
                .createAlias("udcConceptId", "uci")
                .createAlias("languageId", "li")
                .createAlias("udcTermId", "ti")
                .add(Restrictions.ilike("uci.udcNotation", parentId, MatchMode.START))
                .add(Restrictions.eq("li.id", translationId))
                .add(Restrictions.eq("ti.id", 1l))
                .setProjection(Projections.property("uci.id"));

        for (Object obj : criteria.list()) {
            udcConceptIds.add((Long) obj);
        }
        Criteria resultCriteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class)
                .createAlias("udcConceptId", "uci")
                .createAlias("languageId", "li")
                .createAlias("udcTermId", "ti")
                .add(Restrictions.ilike("uci.udcNotation", parentId, MatchMode.START))
                .add(Restrictions.eq("li.id", languageId))
                .add(Restrictions.eq("ti.id", 1l));

        if (!udcConceptIds.isEmpty()) {
            resultCriteria.add(Restrictions.not(Restrictions.in("uci.id", udcConceptIds)));
        }

        return (List<UDCConceptDescription>) resultCriteria.list();
    }

    @Override
    @Transactional
    public List<UDCConceptDescription> getNewlyTranslatedUDCTagDescription(String parentId, long languageId, long translationId) throws Exception {

        List<UDCConceptDescription> resultList = new ArrayList<>();
        List<Long> udcConceptIds = new ArrayList<>();

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class)
                .createAlias("udcConcept", "uc")
                .createAlias("udcLanguage", "ul")
                .add(Restrictions.ilike("uc.udcNotation", parentId, MatchMode.START))
                .add(Restrictions.eq("ul.id", translationId))
                .setProjection(Projections.property("uc.id"))
                .setProjection(Projections.groupProperty("uc.id"));

        for (Object obj : criteria.list()) {
            udcConceptIds.add((Long) obj);
        }

        List<Long> languages = new ArrayList<>();
        languages.add(translationId);
        languages.add(languageId);

        Order order;
        if (languageId < 40l) {
            order = Order.desc("li.id");
        } else {
            order = Order.asc("li.id");
        }

        if (!udcConceptIds.isEmpty()) {
            criteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class)
                    .createAlias("udcConceptId", "uci")
                    .createAlias("languageId", "li")
                    .createAlias("udcTermId", "ti")
                    .add(Restrictions.in("uci.id", udcConceptIds))
                    .add(Restrictions.in("li.id", languages))
                    .add(Restrictions.eq("ti.id", 1l))
                    .addOrder(order)
                    .setProjection(Projections.projectionList().add(Projections.property("uci.id")).add(Projections.property("uci.udcNotation")).add(Projections.property("description")));

            UDCConceptDescription tempUDCConceptDescription;
            Map<String, UDCConceptDescription> map = new HashMap<>();
            for (Object obj : criteria.list()) {
                Object[] objects = (Object[]) obj;
                tempUDCConceptDescription = new UDCConceptDescription();
                tempUDCConceptDescription.setDescription("[" + (String) objects[1] + "] " + (String) objects[2]);
                tempUDCConceptDescription.setId((Long) objects[0]);
                map.put((String) objects[1], tempUDCConceptDescription);
            }

            resultList.addAll(map.values());
        }
        return resultList;
    }

    @Override
    public List<UDCTranslationExperts> fetchTranslatedTagsWithLimit(long userId, String searchString, int dataLimit, int pageNo) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class)
                .createAlias("uDCTranslationExpertId", "udcTranslation")
                .createAlias("udcTranslation.expert", "exp")
                .createAlias("udcTranslation.udcConceptDescription", "udcConceptDes")
                .createAlias("udcConceptDes.udcConceptId", "udcConcept")
                .createAlias("udcConceptDes.languageId", "language")
                .createAlias("udcConceptDes.udcTermId", "udcTerm")
                .add(Restrictions.eq("language.id", 40l))
                .add(Restrictions.eq("udcTerm.id", 1l))
                .add(Restrictions.eq("isReviewed", false))
                .add(Restrictions.isNotNull("udcTranslation.expert"))
                .add(Restrictions.eq("exp.id", userId))
                .add(Restrictions.like("udcConceptDes.description", searchString, MatchMode.ANYWHERE))
                .addOrder(Order.asc("udcConcept.udcNotation"))
                .setProjection(Projections.distinct(Projections.property("uDCTranslationExpertId")))
                .setFirstResult((pageNo - 1) * dataLimit)
                .setMaxResults(dataLimit);
        return criteria.list();
    }

    @Override
    public long fetchTranslatedTagsTotalCount(long userId, String searchString) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class)
                .createAlias("uDCTranslationExpertId", "udcTranslation")
                .createAlias("udcTranslation.expert", "exp")
                .createAlias("udcTranslation.udcConceptDescription", "udcConceptDes")
                .createAlias("udcConceptDes.udcConceptId", "udcConcept")
                .createAlias("udcConceptDes.languageId", "language")
                .createAlias("udcConceptDes.udcTermId", "udcTerm")
                .add(Restrictions.eq("language.id", 40l))
                .add(Restrictions.eq("udcTerm.id", 1l))
                .add(Restrictions.eq("isReviewed", false))
                .add(Restrictions.isNotNull("udcTranslation.expert"))
                .add(Restrictions.eq("exp.id", userId))
                .add(Restrictions.like("udcConceptDes.description", searchString, MatchMode.ANYWHERE))
                .setProjection(Projections.distinct(Projections.property("uDCTranslationExpertId")))
                .setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }

    @Override
    public void saveDescriptionHistory(JSONArray selectedTranslationArr) {
        try {
            //criteria for getting UDCConceptDescription object by conceptId,LanguageId,UdcTerm
            Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCConceptDescription.class)
                    .createAlias("udcConceptId", "udcConcept")
                    .createAlias("languageId", "language")
                    .createAlias("udcTermId", "udcTerm");

            //to save oldDescription,descriptionHistory,newTranslationDescription
            String descriptionHistory = "", newTranslationDescription = "";

            //to get udc concept description object by iterrating selectedTranslationArr
            List<UDCConceptDescription> uDCConceptDescriptionList = new ArrayList<>();
            for (int i = 0; i < selectedTranslationArr.size(); i++) {
                JSONObject obj = (JSONObject) selectedTranslationArr.get(i);
                //save accepted translation to the newTranslationDescription
                newTranslationDescription = (String) obj.get("translatedDescription");
                //to get UDCConceptDescription object
                criteria.add(Restrictions.eq("udcConcept.id", (long) obj.get("udcConceptId")))
                        .add(Restrictions.eq("language.id", (long) obj.get("languageId")))
                        .add(Restrictions.eq("udcTerm.id", new Long(1)));
                uDCConceptDescriptionList = criteria.list();
            }

            //to save old_description           
            if (!uDCConceptDescriptionList.isEmpty()) {
                //update uDCConceptDescription
                //JSON object to save history according to date
                JSONObject newHistoryJSONObject = new JSONObject();
                for (UDCConceptDescription uDCConceptDescription : uDCConceptDescriptionList) {
                    descriptionHistory = uDCConceptDescription.getDescriptionHistory();
                    newHistoryJSONObject.put("date", new Date().getTime() + "");
                    newHistoryJSONObject.put("history", uDCConceptDescription.getDescription());

                    JSONParser parser = new JSONParser();
                    JSONArray descriptionHistoryJSONArray = new JSONArray();
                    //covert description history to JSONArray
                    if (descriptionHistory != null) {
                        descriptionHistoryJSONArray = (JSONArray) parser.parse(descriptionHistory);
                    }
                    descriptionHistoryJSONArray.add(newHistoryJSONObject);
                    //update uDCConceptDescription
                    uDCConceptDescription.setDescription(newTranslationDescription);
                    uDCConceptDescription.setDescriptionHistory(descriptionHistoryJSONArray.toString());
                    saveOrUpdate(uDCConceptDescription);
                }
            } else {
                //create new entry
                for (int i = 0; i < selectedTranslationArr.size(); i++) {
                    JSONObject obj = (JSONObject) selectedTranslationArr.get(i);
                    UDCConceptDescription uDCConceptDescription = new UDCConceptDescription();
                    uDCConceptDescription.setUdcConceptId(uDCConceptDAO.get((long) obj.get("udcConceptId")));
                    uDCConceptDescription.setLanguageId(languageDAO.get((long) obj.get("languageId")));
                    uDCConceptDescription.setUdcTermId(uDCTermDAO.get(new Long(1)));
                    uDCConceptDescription.setDescription((String) obj.get("translatedDescription"));
                    uDCConceptDescription.setDescriptionHistory("");
                    saveOrUpdate(uDCConceptDescription);
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
