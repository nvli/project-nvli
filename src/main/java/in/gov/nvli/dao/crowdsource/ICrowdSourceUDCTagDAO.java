package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceUDCTag;
import java.util.List;

/**
 * Dao Interface for {@link CrowdSourceUDCTag} domain.
 *
 * @author Doppa Srinivas
 * @version 1
 * @since 1
 */
public interface ICrowdSourceUDCTagDAO extends IGenericDAO<CrowdSourceUDCTag, Long> {

    /**
     * This method used to fetch crowdsource UDC tag using record identifier
     * with approve filter.
     *
     * @param recordIdentifier Set record identifier
     * @param isApproved Set approve flag
     * @return {@link CrowdSourceUDCTag}
     * @throws Exception
     */
    public CrowdSourceUDCTag getCrowdSourceUDCTag(String recordIdentifier, byte isApproved) throws Exception;

    /**
     * This method used to fetch list of crowdsource UDC tag using pagination.
     *
     * @param pageNumber Set page no
     * @param pageWindow Set data limit
     * @return list of {@link CrowdSourceUDCTag}
     * @throws Exception
     */
    public List<CrowdSourceUDCTag> getCrowdSourceUDCTagListByUserId(int pageNumber, int pageWindow) throws Exception;

    /**
     * This method used to get total count crowdsource UDC tag.
     *
     * @return Total count
     * @throws Exception
     */
    public long getCrowdSourceTotalUDCTagCountByUserId() throws Exception;
}
