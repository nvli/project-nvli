package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class CrowdSourceArticleDAO extends GenericDAOImpl<CrowdSourceArticle, Long> implements ICrowdSourceArticleDAO {

    @Override
    @Transactional
    public List<CrowdSourceArticle> getArticleList(List<Long> articleIds) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceArticle.class);
        criteria.add(Restrictions.in("id", articleIds.toArray()));
        return criteria.list();
    }

    @Override
    public List<CrowdSourceArticle> getArticlesByFiltersWithLimit(long userId, String searchString, String statusTypeFilter, int dataLimit, int pageNo, boolean isRequiredUserRestriction) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceArticle.class);
        if (isRequiredUserRestriction) {
            criteria.add(Restrictions.eq("user.id", userId));
        }
        criteria.add(Restrictions.like("articleTitle", searchString, MatchMode.ANYWHERE));
        switch (statusTypeFilter) {
            case "draft":
                criteria.add(Restrictions.eq("isPublished", false));
                break;
            case "pending":
                criteria.add(Restrictions.eq("isPublished", true)).add(Restrictions.eq("isApproved", false));
                break;
            case "published":
                criteria.add(Restrictions.eq("isApproved", true));
                break;
        }
        criteria.addOrder(Order.desc("lastModifiedDate"));
        criteria.setFirstResult((pageNo - 1) * dataLimit);
        criteria.setMaxResults(dataLimit);
        return criteria.list();
    }

    @Override
    public long getArticlesTotalCountByFilters(long userId, String searchString, String statusTypeFilter, boolean isRequiredUserRestriction) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceArticle.class);
        if (isRequiredUserRestriction) {
            criteria.add(Restrictions.eq("user.id", userId));
        }
        criteria.add(Restrictions.like("articleTitle", searchString, MatchMode.ANYWHERE));
        switch (statusTypeFilter) {
            case "draft":
                criteria.add(Restrictions.eq("isPublished", false));
                break;
            case "pending":
                criteria.add(Restrictions.eq("isPublished", true)).add(Restrictions.eq("isApproved", false));
                break;
            case "published":
                criteria.add(Restrictions.eq("isPublished", true));
                break;
        }
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }
}
