package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.Marc21TagSubfieldStructureDetail;
import java.util.List;

/**
 * Dao Interface for {@link Marc21TagSubfieldStructureDetail} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface IMarc21TagSubfieldStructureDetailDAO extends IGenericDAO<Marc21TagSubfieldStructureDetail, Long> {

    /**
     * This method used to fetch list Marc21TagSubfieldStructureDetail by given
     * tag field.
     *
     * @param tagField Set tag field
     * @return list of {@link Marc21TagSubfieldStructureDetail}
     * @throws Exception
     */
    public List<Marc21TagSubfieldStructureDetail> getMarc21TagsSuggestions(String tagField) throws Exception;

    /**
     * This method used to fetch list Marc21TagSubfieldStructureDetail by given
     * librarian title.
     *
     * @param librarianTitle
     * @return list of {@link Marc21TagSubfieldStructureDetail}
     * @throws Exception
     */
    public List<Marc21TagSubfieldStructureDetail> getMarc21DescriptionSuggestions(String librarianTitle) throws Exception;
}
