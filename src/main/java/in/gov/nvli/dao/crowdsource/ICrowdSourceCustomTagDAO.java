package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.user.User;
import java.util.List;

/**
 * Dao Interface for {@link CrowdSourceCustomTag} domain.
 *
 * @author Doppa Srinivas
 * @version 1
 * @since 1
 */
public interface ICrowdSourceCustomTagDAO extends IGenericDAO<CrowdSourceCustomTag, Long> {

    /**
     * This method used to fetch custom tag using record identifier and user
     * object.
     *
     * @param recordIdentifier Set record identifier
     * @param user Set {@link User} domain
     * @return {@link CrowdSourceCustomTag}
     * @throws Exception
     */
    public CrowdSourceCustomTag getCrowdSourceCustomTag(String recordIdentifier, User user) throws Exception;

    /**
     * This method used to fetch list of custom tag using user id with
     * pagination.
     *
     * @param userId Set user id
     * @param pageNumber Set page no
     * @param pageWindow Set data limit
     * @return List of {@link CrowdSourceCustomTag}
     * @throws Exception
     */
    public List<CrowdSourceCustomTag> getuserTagsPaginated(long userId, int pageNumber, int pageWindow) throws Exception;

    /**
     * This method used to save or update custom tag.
     *
     * @param crowdSourceCustomTag Set {@link CrowdSourceCustomTag}
     * @return {@link CrowdSourceCustomTag}
     * @throws Exception
     */
    public CrowdSourceCustomTag saveOrUpdateCrowdSourceCustomTag(CrowdSourceCustomTag crowdSourceCustomTag) throws Exception;
}
