package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCTerm;

/**
 * Dao Interface for {@link UDCTerm} domain.
 *
 * @author Ritesh MAlviya
 * @version 1
 * @since 1
 */
public interface IUDCTermDAO extends IGenericDAO<UDCTerm, Long> {

}
