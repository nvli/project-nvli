package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.UDCTranslationExperts;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.UserService;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class CrowdSourceDAO extends GenericDAOImpl<CrowdSource, Long> implements ICrowdSourceDAO {

    @Autowired
    private UserService userService;
            
    @Override
    @Transactional
    public CrowdSource getCrowdSourceByRecordIdentifier(String recordIdentifier) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class);
        criteria.add(Restrictions.eq("recordIdentifier", recordIdentifier));
        criteria.add(Restrictions.eq("isApproved", Byte.valueOf("0")));
        return (CrowdSource) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public Integer getCrowdSourceLastEditNumber(String recordIdentifier) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class);
        criteria.add(Restrictions.eq("recordIdentifier", recordIdentifier));
        criteria.setProjection(Projections.max("editNumber"));
        return (Integer) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public List<CrowdSource> getMetadataCuratedRecordListByUserId(int pageNumber, int pageWindow, long userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class);
        criteria.add(Restrictions.eq("isApproved", Byte.parseByte("0")));
        criteria.add(Restrictions.eq("expertUserId.id", userId));
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        return criteria.list();
    }

    @Override
    @Transactional
    public long getMetadataCuratedRecordTotalCountByUserId(long userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class);
        criteria.add(Restrictions.eq("isApproved", Byte.parseByte("0")));
        criteria.add(Restrictions.eq("expertUserId.id", userId));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public User getMinimumRecordAssignedUserFromUserList(List<Long> userIds) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class)
                .add(Restrictions.eq("isApproved", Byte.parseByte("0")))
                .add(Restrictions.in("expertUserId.id", userIds))
                .setProjection(Projections.projectionList()
                        .add(Projections.property("expertUserId"))
                        .add(Projections.count("expertUserId"), "recordCount")
                        .add(Projections.groupProperty("expertUserId")))
                .addOrder(Order.asc("recordCount")).setMaxResults(1);

        Object obj = criteria.uniqueResult();
        if (obj != null) {
            return (User) ((Object[]) obj)[0];
        } else {
            return null;
        }
    }
    
    @Override
    @Transactional
    public User getMinimumTagsAssignedUserFromUserList(List<Long> userIds) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTranslationExperts.class)
                .createAlias("expert", "user",JoinType.RIGHT_OUTER_JOIN)
                .add(Restrictions.in("user.id", userIds))
                .setProjection(Projections.projectionList()
                        .add(Projections.property("user.id"))
                        .add(Projections.rowCount(), "tagCount")
                        .add(Projections.groupProperty("user.id")))    
                .addOrder(Order.asc("tagCount")).setMaxResults(1);
        Object obj = criteria.uniqueResult();
        User user = userService.getUserById((Long)((Object[]) obj)[0]);
        return user != null ? user : null;       
    }

    @Override
    @Transactional
    public boolean assignedLibExpertToCrowdsourceList(List<String> resourceCodes, User user) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class)
                .add(Restrictions.in("subResourceCode", resourceCodes))
                .add(Restrictions.eq("expertUserId.id", null));

        for (CrowdSource crowdSource : (List<CrowdSource>) criteria.list()) {
            crowdSource.setExpertUserId(user);
        }
        return true;
    }
}
