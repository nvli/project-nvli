package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCTerm;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Repository
public class UDCTermDAO extends GenericDAOImpl<UDCTerm, Long> implements IUDCTermDAO {

}