package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.user.User;
import java.util.List;

/**
 * Dao Interface for {@link CrowdSource} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface ICrowdSourceDAO extends IGenericDAO<CrowdSource, Long> {

    /**
     * This method used to fetch crowdsource using record identifier.
     *
     * @param recordIdentifier Set record identifier
     * @return {@link CrowdSource}
     * @throws Exception
     */
    public CrowdSource getCrowdSourceByRecordIdentifier(String recordIdentifier) throws Exception;

    /**
     * This method used to get last edit no for crowdsource.
     *
     * @param recordIdentifier Set record identifier
     * @return last edit no
     * @throws Exception
     */
    public Integer getCrowdSourceLastEditNumber(String recordIdentifier) throws Exception;

    /**
     * This method used to fetch list of crowdsource with pagination using user
     * id.
     *
     * @param pageNumber Set page no
     * @param pageWindow Set data limit
     * @param userId Set user id
     * @return List of {@link CrowdSource}
     * @throws Exception
     */
    public List<CrowdSource> getMetadataCuratedRecordListByUserId(int pageNumber, int pageWindow, long userId) throws Exception;

    /**
     * This method used to fetch Total count of crowdsource using user id.
     *
     * @param userId Set user id
     * @return Total count
     * @throws Exception
     */
    public long getMetadataCuratedRecordTotalCountByUserId(long userId) throws Exception;

    /**
     * This method used to find user who have minimum record among given users.
     *
     * @param userIds Set list of user ids
     * @return {@link User}
     * @throws Exception
     */
    public User getMinimumRecordAssignedUserFromUserList(List<Long> userIds) throws Exception;
    
    /**
     * This method used to find user who have minimum tags among given users.
     * @param userIds
     * @return
     * @throws Exception 
     */
    public User getMinimumTagsAssignedUserFromUserList(List<Long> userIds) throws Exception;

    /**
     * This method used to assigned given user to crowdsource of all given
     * resource code.
     *
     * @param resourceCodes Set list of resource code
     * @param user Set {@link User} for assigned
     * @return true/false.True for success and False for failed
     * @throws Exception
     */
    public boolean assignedLibExpertToCrowdsourceList(List<String> resourceCodes, User user) throws Exception;
}
