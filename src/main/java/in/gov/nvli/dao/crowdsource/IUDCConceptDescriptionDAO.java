package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import in.gov.nvli.domain.crowdsource.UDCTranslationExperts;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;

/**
 * Dao Interface for {@link UDCConceptDescription} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface IUDCConceptDescriptionDAO extends IGenericDAO<UDCConceptDescription, Long> {

    /**
     * This method used to fetch UDC tag suggestions by given language and
     * prefix search string.
     *
     * @param languageId Set language id
     * @param tagPrefix Set prefix search string
     * @return list of {@link UDCConceptDescription}
     * @throws Exception
     */
    public List<UDCConceptDescription> getUDCTagsSuggestions(String languageId, String tagPrefix) throws Exception;
    
    /**
     * 
     * @param udcConcept
     * @param udcLanguage
     * @return
     * @throws Exception 
     */
    public UDCConceptDescription getUUDCConceptDescription(UDCConcept udcConcept, long udcLanguageId) throws Exception;

    /**
     * This method used to fetch descriptions of given notations by language id.
     *
     * @param notations Set list of notation
     * @param languageId Set language id
     * @param isReplaceByEnglish Set true if u want description not present in
     * given language then return english description otherwise false
     * @return Map,key as notation and value as description
     * @throws Exception
     */
    public Map<String, String> getUDCTagDescription(List<String> notations, long languageId, boolean isReplaceByEnglish) throws Exception;

    /**
     * This method used to fetch list of UDC notation which description is not
     * translated in given translation language.
     *
     * @param parentId Set parent id
     * @param languageId Set response language id
     * @param translationId Set translation language id
     * @return list of {@link UDCConceptDescription}
     * @throws Exception
     */
    public List<UDCConceptDescription> getUntranslatedUDCTagDescription(String parentId, long languageId, long translationId) throws Exception;

    /**
     * This method used to fetch list of UDC notation which description is
     * recently translated in given translation language.
     *
     * @param parentId Set parent id
     * @param languageId Set response language id
     * @param translationId Set translation language id
     * @return list of {@link UDCConceptDescription}
     * @throws Exception
     */
    public List<UDCConceptDescription> getNewlyTranslatedUDCTagDescription(String parentId, long languageId, long translationId) throws Exception;
    
    public List<UDCTranslationExperts> fetchTranslatedTagsWithLimit(long userId, String searchString, int dataLimit, int pageNo);

    public long fetchTranslatedTagsTotalCount(long userId, String searchString);

    public void saveDescriptionHistory(JSONArray selectedTranslationArr);
}