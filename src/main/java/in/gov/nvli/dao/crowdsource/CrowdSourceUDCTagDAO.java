package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.CrowdSourceUDCTag;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Doppa Srinivas
 */
@Repository
public class CrowdSourceUDCTagDAO extends GenericDAOImpl<CrowdSourceUDCTag, Long> implements ICrowdSourceUDCTagDAO {

    @Override
    @Transactional
    public CrowdSourceUDCTag getCrowdSourceUDCTag(String recordIdentifier, byte isApproved) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceUDCTag.class);
        criteria.add(Restrictions.eq("recordIdentifier", recordIdentifier));
        criteria.add(Restrictions.eq("isApproved", isApproved));
        return (CrowdSourceUDCTag) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public List<CrowdSourceUDCTag> getCrowdSourceUDCTagListByUserId(int pageNumber, int pageWindow) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceUDCTag.class);
        criteria.add(Restrictions.eq("isApproved", Byte.parseByte("0")));
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        return criteria.list();
    }

    @Override
    @Transactional
    public long getCrowdSourceTotalUDCTagCountByUserId() throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSourceUDCTag.class);
        criteria.add(Restrictions.eq("isApproved", Byte.parseByte("0")));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }
}
