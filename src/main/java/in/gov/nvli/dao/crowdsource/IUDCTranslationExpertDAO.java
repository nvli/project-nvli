package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.crowdsource.UDCTranslationExperts;
import java.util.List;
import org.json.simple.JSONArray;

/**
 * Dao Interface for {@link UDCTranslationExperts} domain.
 *
 * @author Ritesh Malviya
 * @author Vivek Bugale
 */
public interface IUDCTranslationExpertDAO extends IGenericDAO<UDCTranslationExperts, Long> {

    public UDCTranslationExperts checkEntryPresent(UDCConceptDescription udcConceptDiscription, UDCLanguage udcLanguage);    
}
