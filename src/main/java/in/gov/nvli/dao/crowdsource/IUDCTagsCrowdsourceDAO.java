package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import java.util.List;

/**
 * Dao Interface for {@link UDCTagsCrowdsource} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface IUDCTagsCrowdsourceDAO extends IGenericDAO<UDCTagsCrowdsource, Long> {

    /**
     * This method used to fetch list of UDCTagsCrowdsource
     * {@link UDCTagsCrowdsource} by given UDC tag id and language id.
     *
     * @param udcConceptId Set UDC tag id
     * @param languageId Set language id
     * @return List of {@link UDCTagsCrowdsource}
     * @throws Exception
     */
    public List<UDCTagsCrowdsource> getUDCTagsCrowdsourceList(long udcConceptId, long languageId) throws Exception;

    /**
     * This method used to fetch UDCTagsCrowdsource {@link UDCTagsCrowdsource}
     * by given UDC tag id, language id and user id.
     *
     * @param udcConceptId Set UDC tag id
     * @param languageId Set language id
     * @param userId Set user id
     * @return {@link UDCTagsCrowdsource}
     * @throws Exception
     */
    public UDCTagsCrowdsource checkUDCTagsCrowdsourceAlreadyExist(long udcConceptId, long languageId, long userId) throws Exception;

    /**
     * This method used to fetch list of UDCTagsCrowdsource
     * {@link UDCTagsCrowdsource} by given user id.
     *
     * @param userId Set user id
     * @return List of {@link UDCTagsCrowdsource}
     * @throws Exception
     */
    public List<UDCTagsCrowdsource> getUDCTagsCrowdsourceList(long userId) throws Exception;

    public List<UDCTagsCrowdsource> seeUDCTranslatedTags(long conceptId,long languageId);    
}