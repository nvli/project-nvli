package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.ResourcesWithoutLibUser;

/**
 * Dao Interface for {@link ResourcesWithoutLibUser} domain.
 *
 * @author Vivek Bugale
 * @version 1
 * @since 1
 */
public interface IResourcesWithoutLibUserDAO extends IGenericDAO<ResourcesWithoutLibUser, Long> {

}
