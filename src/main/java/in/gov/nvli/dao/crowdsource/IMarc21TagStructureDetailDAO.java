package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.Marc21TagStructureDetail;

/**
 * Dao Interface for {@link Marc21TagStructureDetail} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface IMarc21TagStructureDetailDAO extends IGenericDAO<Marc21TagStructureDetail, Long> {

}
