package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.CustomTags;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Doppa Srinivas
 */
@Repository
public class CustomTagsDAO extends GenericDAOImpl<CustomTags, Long> implements ICustomTagsDAO {

    @Override
    @Transactional
    public CustomTags getCustomTag(String tag, long languageId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CustomTags.class);
        criteria.add(Restrictions.eq("tagName", tag));
        criteria.add(Restrictions.eq("languageId", languageId));
        return (CustomTags) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public List<CustomTags> getCustomTagsSuggestions(String languageId, String tagPrefix) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CustomTags.class);
        criteria.add(Restrictions.eq("languageId", Long.parseLong(languageId)));
        criteria.add(Restrictions.like("tagName", tagPrefix + "%"));
        criteria.addOrder(Order.asc("tagName"));
        return criteria.list();
    }
}
