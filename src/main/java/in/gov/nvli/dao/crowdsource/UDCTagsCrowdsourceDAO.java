package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Suman
 * @version 1
 * @since 1
 */
@Repository
public class UDCTagsCrowdsourceDAO extends GenericDAOImpl<UDCTagsCrowdsource, Long> implements IUDCTagsCrowdsourceDAO {

    @Override
    @Transactional
    public List<UDCTagsCrowdsource> getUDCTagsCrowdsourceList(long udcConceptId, long languageId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class);
        criteria.add(Restrictions.eq("udcConcept.id", udcConceptId));
        criteria.add(Restrictions.eq("udcLanguage.id", languageId));
        criteria.add(Restrictions.eq("isReviewed",false));
        return criteria.list();
    }

    @Override
    @Transactional
    public UDCTagsCrowdsource checkUDCTagsCrowdsourceAlreadyExist(long udcConceptId, long languageId, long userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class)
                .add(Restrictions.eq("udcConcept.id", udcConceptId))
                .add(Restrictions.eq("udcLanguage.id", languageId))
                .add(Restrictions.eq("user.id", userId))
                .add(Restrictions.eq("isReviewed", false));        
        return (UDCTagsCrowdsource) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public List<UDCTagsCrowdsource> getUDCTagsCrowdsourceList(long userId) throws Exception {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class);
        criteria.add(Restrictions.eq("user.id", userId));
        return criteria.list();
    }

    @Override
    public List<UDCTagsCrowdsource> seeUDCTranslatedTags(long conceptId,long languageId) {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(UDCTagsCrowdsource.class)
                .add(Restrictions.eq("udcConcept.id", conceptId))
                .add(Restrictions.eq("udcLanguage.id", languageId))
                .add(Restrictions.eq("isReviewed", false));
        return criteria.list();
    }
}