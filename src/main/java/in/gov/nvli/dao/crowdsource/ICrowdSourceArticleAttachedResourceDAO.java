package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticleAttachedResource;

/**
 * Dao Interface for {@link CrowdSourceArticleAttachedResource} domain.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public interface ICrowdSourceArticleAttachedResourceDAO extends IGenericDAO<CrowdSourceArticleAttachedResource, Long> {

}
