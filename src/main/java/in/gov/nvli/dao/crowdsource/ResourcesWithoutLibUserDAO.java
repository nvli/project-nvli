package in.gov.nvli.dao.crowdsource;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.crowdsource.ResourcesWithoutLibUser;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Vivek Bugale
 * @version 1
 * @since 1
 */
@Repository
public class ResourcesWithoutLibUserDAO extends GenericDAOImpl<ResourcesWithoutLibUser, Long> implements IResourcesWithoutLibUserDAO {

}
