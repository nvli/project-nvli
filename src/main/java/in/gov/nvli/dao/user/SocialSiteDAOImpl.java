package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.SocialSite;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
@Repository
public class SocialSiteDAOImpl extends GenericDAOImpl<SocialSite, Integer> implements SocialSiteDAO {
    private static final Logger LOG = Logger.getLogger(SocialSiteDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); 
    }

    @Override
    public SocialSite getSocialSiteById(int siteId) {
        return get(siteId);
    }
    
    @Override
    public List<SocialSite> getAllSocialSites(){
        return list();
    }
    
}
