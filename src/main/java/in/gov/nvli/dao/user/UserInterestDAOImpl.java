package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserInterest;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Repository
public class UserInterestDAOImpl extends GenericDAOImpl<UserInterest, Long> implements UserInterestDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
