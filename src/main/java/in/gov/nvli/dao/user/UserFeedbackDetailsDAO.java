package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.FeedbackForm;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@Repository
public class UserFeedbackDetailsDAO  extends GenericDAOImpl<FeedbackForm, Long> implements IUserFeedbackDetailsDAO {
    
}
