package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import java.util.List;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public interface UserRoleDAO extends IGenericDAO<Role, Long> {

    @Override
    public Role get(Long roleId);

    public List<Role> listRoles();
    
    public Role getRoleByCode(String roleCode);
    
}
