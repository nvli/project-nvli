package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserEmailRecord;
import java.util.List;

/**
 * @author Bhumika
 * @since 1
 * @version 1
 */
public interface UserEmailRecordDAO extends IGenericDAO<UserEmailRecord, Long> {

    public boolean saveRecordEmail(UserEmailRecord obj);

    public List<UserEmailRecord> getAllEmailsOfRecordsOfUser(long userId) ;

    public List<UserEmailRecord> getEmailsOfRecord(String uniqueId);

    public UserEmailRecord getObjectByEmailId(long id);

    public UserEmailRecord getEmailObjectByRecordIdAndUserId(String recId, long userId);
    
}
