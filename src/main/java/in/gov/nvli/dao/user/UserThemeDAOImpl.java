package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserTheme;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Repository
public class UserThemeDAOImpl extends GenericDAOImpl<UserTheme, Long> implements UserThemeDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional
    public List<HashMap<String, Object>> getThemes() {
        List<HashMap<String, Object>> list = this.currentSession()
                .createCriteria(UserTheme.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("id"))
                        .add(Projections.property("themeCode"))
                        .add(Projections.property("themeName"))
                ).list();
        return list;
    }

    @Override
    public UserTheme getThemeByName(String themeName) {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeName", themeName));
        return (UserTheme) c.uniqueResult();
    }

    @Override
    public UserTheme getThemeByDate(Date themedate) {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeActivationDate", themedate));
        c.add(Restrictions.eq("themeType", "global"));
        return (UserTheme) c.uniqueResult();
    }

    @Override
    public List<UserTheme> getThemesByDate(Date themedate) {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeActivationDate", themedate));
        c.add(Restrictions.eq("themeType", "global"));
        return c.list();
    }

    @Override
    public List<UserTheme> getAllPersonalThemes() {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeType", "personal"));
        return c.list();
    }

    @Override
    public UserTheme getThemeByThemeCode(String themeCode) {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeCode", themeCode));
        return (UserTheme) c.uniqueResult();
    }

    @Override
    public List<UserTheme> getAllGlobalThemes() {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeType", "global"));
        return c.list();
    }

    @Override
    public List<UserTheme> getThemeList() {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.addOrder(Order.asc("themeActivationDate"));
        return c.list();
    }

    @Override
    public List<UserTheme> getAllRepeatableTheme() {
        Criteria c = super.currentSession().createCriteria(UserTheme.class);
        c.add(Restrictions.eq("themeRepeatable", Boolean.TRUE));
        return c.list();
    }
}
