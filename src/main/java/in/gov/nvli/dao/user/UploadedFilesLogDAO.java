/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserUploadedFile;
import java.util.List;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public interface UploadedFilesLogDAO extends IGenericDAO<UserUploadedFile, Long> {
    public List<UserUploadedFile> fetchByUID(long uid);
}
