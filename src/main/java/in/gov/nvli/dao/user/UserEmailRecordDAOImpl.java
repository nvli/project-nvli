package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserEmailRecord;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
@Repository
public class UserEmailRecordDAOImpl extends GenericDAOImpl<UserEmailRecord, Long> implements UserEmailRecordDAO {

    private static final Logger LOG = Logger.getLogger(UserEmailRecordDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveRecordEmail(UserEmailRecord obj) {
        try {
            return createNew(obj);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public List<UserEmailRecord> getAllEmailsOfRecordsOfUser(long userId) {
        List<UserEmailRecord> list = super.currentSession().createCriteria(UserEmailRecord.class).createAlias("user", "usrObj").add(Restrictions.eq("usrObj.id", userId)).addOrder(Order.desc("addedTime")).list();
        return list;
    }

    @Override
    public List<UserEmailRecord> getEmailsOfRecord(String uniqueId) {
        List<UserEmailRecord> list = null;
        try {
            list = super.currentSession().createCriteria(UserEmailRecord.class).add(Restrictions.eq("uniqueRecordIdentifier", uniqueId)).list();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public UserEmailRecord getObjectByEmailId(long id) {
        UserEmailRecord obj = null;
        try {
            obj = (UserEmailRecord) super.currentSession().createCriteria(UserEmailRecord.class).add(Restrictions.eq("id", id)).uniqueResult();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return obj;
    }

    @Override
    public UserEmailRecord getEmailObjectByRecordIdAndUserId(String recId, long userId) {
        UserEmailRecord obj = null;
        try {
            obj = (UserEmailRecord) super.currentSession().createCriteria(UserEmailRecord.class).createAlias("user", "usrObj").add(Restrictions.and(Restrictions.eq("usrObj.id", userId),Restrictions.eq("uniqueRecordIdentifier", recId))).uniqueResult();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return obj;
    }

}
