package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserActivity;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
@Repository
public class UserActivityDAOImpl extends GenericDAOImpl<UserActivity, Integer> implements UserActivityDAO {

    private static final Logger LOG = Logger.getLogger(UserActivityDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserActivity getActivityObjectById(Integer activityId) {
        return get(activityId);
    } 
}
