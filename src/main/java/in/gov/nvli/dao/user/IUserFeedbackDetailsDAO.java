package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.FeedbackForm;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
public interface IUserFeedbackDetailsDAO extends IGenericDAO<FeedbackForm, Long>{
    
}
