package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserInterest;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public interface UserInterestDAO extends IGenericDAO<UserInterest, Long> {

}
