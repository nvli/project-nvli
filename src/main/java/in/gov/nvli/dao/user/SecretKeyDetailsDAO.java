/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.SecretKeyDetails;
import java.util.List;

/**
 *
 * @author Gulafsha
 */
public interface SecretKeyDetailsDAO extends IGenericDAO<SecretKeyDetails, Long> {

    public List<SecretKeyDetails> getUsersByFiltersWithLimit(String searchString, int dataLimit, int pageNo) throws Exception;

    public long getUsersTotalCountByFilters(String searchString) throws Exception;

}
