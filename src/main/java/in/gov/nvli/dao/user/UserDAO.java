package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.Cities;
import in.gov.nvli.domain.user.Country;
import in.gov.nvli.domain.user.DeactivationFeedbackBase;
import in.gov.nvli.domain.user.District;
import in.gov.nvli.domain.user.SecretKeyDetails;
import in.gov.nvli.domain.user.States;
import in.gov.nvli.domain.user.User;
import java.util.List;
import java.util.Set;
import org.json.simple.JSONArray;

/**
 * All user related DAO methods can be implemented here
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Vivek Buagle <bvivek@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public interface UserDAO extends IGenericDAO<User, Long> {

    public List<User> getUsers();

    public User findByEmail(String email);

    public User findBySalt(String salt);

    public User getUserByMailAndPasscode(String emailId, String passcode);

    public boolean existsUsername(String username);

    public boolean existsUseremail(String useremail);

    public User findUserByUsername(String username);

    public User getUserById(Long userId);

    /**
     *
     * @param username
     * @param showOnlyActiveUsers
     * @return List<User>
     */
    public List<User> usersSuggestions(String username, boolean showOnlyActiveUsers);

    public List<String> getEmailList(List<Long> userIdList);

    public List<User> getUsersByRoleId(long roleId, int pageNumber, int pageWindow, String sortBy, String sortOrder);

    public long getTotalUserCountByRoleId(long roleId);

    public List<Long> getUserIdsByAssignedResource(String resourceCode, String userRole) throws Exception;

    public List<Long> getUserIdsByAssignedUDCLanguageId(long udcLanguageId, String userRole) throws Exception;

    /**
     *
     * @param resourceCode
     * @param userIds
     * @param userRole
     * @return
     * @throws Exception
     */
    public JSONArray getUserByAssignedResourceAndUserIds(String resourceCode, Set<Long> userIds, String userRole) throws Exception;

    /**
     *
     * @param Principal
     * @return {@link Boolean} whether email is valid or not
     */
    public boolean isValidEmail(String Principal);

    /**
     * get all countries
     *
     * @return {@link Country} list of all countries
     */
    public List<Country> getAllCountries();

    /**
     * get all state by country id
     *
     * @param countryId that uses to find all states
     * @return {@link List} list of all states
     */
    public List<States> getStateByCountry(long countryId);

    /**
     * get all district by state id
     *
     * @param stateId that uses to find all district
     * @return {@link List} list of all district
     */
    public List<District> getDistrictByState(Long stateId);

    /**
     * get all cities by district id
     *
     * @param distictId that uses to find all cities
     * @return {@link List} list of all cities
     */
    public List<Cities> getCityByDistrict(Long distictId);

    /**
     * method use to check whether user is activated or deactivated
     *
     * @return {@link Boolean}
     */
    public boolean isUserEnabled(Long userId);

    /**
     * get list of all feedback available in database
     *
     * @return {@link List}
     */
    public List<DeactivationFeedbackBase> getFeedbackList();

    public Long findUserCount(String secretKey);
}
