package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserActivityLog;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Bhumika,Ruturaj
 */
@Repository
public class UserActivityLogDAOImpl extends GenericDAOImpl<UserActivityLog, Long> implements UserActivityLogDAO {

    private static final Logger LOG = Logger.getLogger(UserActivityLogDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveActivityLog(UserActivityLog logObj) {
        try {
            return createNew(logObj);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserActivityLog saveUserActivityLog(UserActivityLog logObj) {
        try {
            if (createNew(logObj)) {
                return logObj;
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public List<UserActivityLog> getActivityLogListOfUser(int pageNumber, int pageWindow, long userId) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(UserActivityLog.class);
        criteria.createAlias("user", "usrObj").add(Restrictions.eq("usrObj.id", userId)).addOrder(Order.desc("activityTime"));
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        return criteria.list();
    }

    public List<UserActivityLog> getFilteredActivityLogListOfUser(int pageNumber, int pageWindow, long userId, Set<Integer> activityIds) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(UserActivityLog.class);
        criteria.createAlias("user", "usrObj");
        criteria.createAlias("userActivity", "activityObj");
        criteria.add(Restrictions.and(
                Restrictions.eq("usrObj.id", userId),
                Restrictions.in("activityObj.activityId", activityIds)
        ));
        criteria.addOrder(Order.desc("activityTime"));
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        criteria.list();
        return criteria.list();
    }

    @Override
    @Transactional
    public long getActivityLogListOfUserTotalCountByUserId(long userId) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(UserActivityLog.class);
        criteria.createAlias("user", "usrObj").add(Restrictions.eq("usrObj.id", userId));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }

    @Override
    @Transactional
    public long getFilteredActivityLogListOfUserTotalCountByUserId(long userId, Set<Integer> activityIds) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(UserActivityLog.class);
        criteria.createAlias("user", "usrObj");
        criteria.createAlias("userActivity", "activityObj");
        criteria.add(Restrictions.and(
                Restrictions.eq("usrObj.id", userId),
                Restrictions.in("activityObj.activityId", activityIds)
        ));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }
}
