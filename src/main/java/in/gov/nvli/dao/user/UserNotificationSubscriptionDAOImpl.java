/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.NotificationSubscription;
import org.hibernate.Criteria;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Repository
public class UserNotificationSubscriptionDAOImpl extends GenericDAOImpl<NotificationSubscription, Long> implements UserNotificationSubscriptionDAO {

    private static final Logger LOG = Logger.getLogger(NotificationSubscription.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional
    public NotificationSubscription GetNotificationSettingsByUserId(long userId) {
        Criteria criteria = super.currentSession().createCriteria(NotificationSubscription.class).add(Restrictions.eq("userId", userId));
        NotificationSubscription notifObj = (NotificationSubscription) criteria.uniqueResult();
        return notifObj;
    }

    @Override
    public boolean updateNvliLinkShareWithEmail(boolean NvlilLinkShareWithEmailStatus, long userId) {
        Criteria criteria = super.currentSession().createCriteria(NotificationSubscription.class).add(Restrictions.eq("userId", userId));
        NotificationSubscription notifObj = (NotificationSubscription) criteria.uniqueResult();
        notifObj.setNvliShareLinkWithEmail(NvlilLinkShareWithEmailStatus);
        System.out.println(notifObj.getNvliShareLinkWithEmail());
        saveOrUpdate(notifObj);
        return true;
    }

}
