package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserInvitation;
import in.gov.nvli.util.Constants;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
@Repository
public class UserInvitationDAOImpl extends GenericDAOImpl<UserInvitation, Long> implements UserInvitationDAO {

    private static final Logger LOG = Logger.getLogger(in.gov.nvli.dao.user.UserInvitationDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    public UserInvitation getInvitationObjectByHash(String invitationHash) {
        return (UserInvitation) super.currentSession().createCriteria(UserInvitation.class).add(Restrictions.eq("invitationCode", invitationHash)).uniqueResult();
    }

    @Override
    public UserInvitation getInvitaionObjectByInvitationId(long invitationId) {
        return (UserInvitation) super.currentSession().createCriteria(UserInvitation.class).add(Restrictions.eq("invitationId", invitationId)).uniqueResult();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveInvitationObject(UserInvitation invitationObj) {
        try {
            return createNew(invitationObj);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public void updateInvitationObject(UserInvitation invitationObj) {
        try {
            update(invitationObj);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public List<UserInvitation> getInvitationsSentByUser(long userId) {
        return super.currentSession().createCriteria(UserInvitation.class).createAlias("invitedBy", "usrObj").add(Restrictions.eq("usrObj.id", userId)).addOrder(Order.desc("invitationDate")).list();
    }

    @Override
    public List<UserInvitation> getInvitaionObjectByEmailId(String inviteeEmail) {
        return super.currentSession().createCriteria(UserInvitation.class)
                .add(Restrictions.eq("inviteeEmail", inviteeEmail))
                .add(Restrictions.ne("invitationAccepted", Constants.INVITE_CANCELLED))
                .list();
    }

    @Override
    public List<UserInvitation> getInvitedUser(int pageNumber, int pageWindow, String sortBy, String sortOrder) {

        Criteria criteria = (Criteria) super.currentSession().createCriteria(UserInvitation.class);

        if (sortOrder.equals("asc")) {
            criteria.addOrder(Order.asc(sortBy));
        } else {
            criteria.addOrder(Order.desc(sortBy));
        }
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        return criteria.list();
    }

    @Override
    public List<UserInvitation> getInvitedUserByFiltersWithLimit(String searchString, int dataLimit, int pageNo) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserInvitation.class);
        if (!"null".equals(searchString)) {
            criteria.add(Restrictions.like("inviteeEmail", searchString, MatchMode.ANYWHERE));
        }
        criteria.addOrder(Order.desc("invitationDate"));
        criteria.setFirstResult((pageNo - 1) * dataLimit);
        criteria.setMaxResults(dataLimit);
        return criteria.list();
    }

    @Override
    public long getInvitedUserCountByFilters(String searchString) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserInvitation.class);
        if (!"null".equals(searchString)) {
            criteria.add(Restrictions.like("inviteeEmail", searchString, MatchMode.ANYWHERE));
        }
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }

    @Override
    public Long getInvitedUserCount() {

        Criteria criteria = (Criteria) super.currentSession().createCriteria(UserInvitation.class);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();

    }

    @Override
    public boolean checkInvitationAlreadySent(String emailId) {
        return Optional.ofNullable(currentSession().createCriteria(UserInvitation.class).add(Restrictions.eq("inviteeEmail", emailId)).uniqueResult()).isPresent();
    }
}
