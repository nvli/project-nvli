/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserFeedback;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * All user feedback related DAO methods can be implemented here
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Repository
public class UserFeedbackDAOImpl extends GenericDAOImpl<UserFeedback, Long> implements UserFeedbackDAO {

    private static final Logger LOG = Logger.getLogger(UserFeedbackDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
