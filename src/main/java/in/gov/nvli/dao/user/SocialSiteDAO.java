package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.SocialSite;
import java.util.List;


/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
public interface SocialSiteDAO  extends IGenericDAO<SocialSite, Integer> {
    SocialSite getSocialSiteById(int siteId);
    List<SocialSite> getAllSocialSites();
    
}
