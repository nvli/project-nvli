/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserUploadedFile;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Repository
public class UploadedFilesLogDAOImpl extends GenericDAOImpl<UserUploadedFile, Long> implements UploadedFilesLogDAO{
    @Override
    public List<UserUploadedFile> fetchByUID(long uid){
        Criteria criteria = super.currentSession().createCriteria(UserUploadedFile.class);
        criteria.add(Restrictions.eq("createdBy", uid));
        criteria.addOrder(Order.desc("createdOn"));
        return criteria.list();
    }
}
