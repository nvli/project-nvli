package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Repository
public class UserRoleDAOImpl extends GenericDAOImpl<Role, Long> implements UserRoleDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Method to get Role object by role id.
     *
     * @param roleId id of role to be fetched.
     * @return {@link Role}
     */
    @Transactional
    @Override
    public Role get(Long roleId) {
        Criteria criteria = currentSession().createCriteria(Role.class);
        criteria
                .setFetchMode("users", FetchMode.JOIN)
                .setFetchMode("permissions", FetchMode.JOIN);
        criteria.add(Restrictions.eq("roleId", roleId));
        return (Role) criteria.uniqueResult();
    }

    @Transactional
    @Override
    public List<Role> listRoles() {
        Criteria roleCriteria = currentSession().createCriteria(Role.class);
//        roleCriteria
//                .setFetchMode("users", FetchMode.JOIN)
//                .setFetchMode("permissions", FetchMode.JOIN);
        return roleCriteria.list();

    }

    @Transactional
    @Override
    public Role getRoleByCode(String roleCode) {
        Criteria roleCriteria = currentSession().createCriteria(Role.class);
        roleCriteria.add(Restrictions.eq("code", roleCode));
        Role role = (Role) roleCriteria.uniqueResult();
        return role;
    }
}
