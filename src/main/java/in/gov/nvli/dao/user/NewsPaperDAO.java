package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.NewsPaper;
import java.util.List;

/**
 *
 * @author Ritesh
 */
public interface NewsPaperDAO extends IGenericDAO<NewsPaper, String> {
    
    /**
     * 
     * @param paperLang
     * @param newsType
     * @return 
     */
    public List<NewsPaper> getNewsPapers(String paperLang, boolean newsType);
}
