package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserActivity;

/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
public interface UserActivityDAO extends IGenericDAO<UserActivity, Integer> {

    public UserActivity getActivityObjectById(Integer activityId);    
}
