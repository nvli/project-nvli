/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;
import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.NotificationSubscription;
import in.gov.nvli.domain.user.UserActivity;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public interface UserNotificationSubscriptionDAO extends IGenericDAO<NotificationSubscription, Long> {

    public NotificationSubscription GetNotificationSettingsByUserId(long userId);

    public boolean  updateNvliLinkShareWithEmail(boolean NvlilLinkShareWithEmailStatus, long userId);
    
}
