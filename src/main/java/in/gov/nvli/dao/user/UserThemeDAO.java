package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserTheme;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public interface UserThemeDAO extends IGenericDAO<UserTheme, Long> {

    public List<HashMap<String, Object>> getThemes();

    public UserTheme getThemeByName(String ThemeName);

    public UserTheme getThemeByThemeCode(String themeCode);

    public UserTheme getThemeByDate(Date ThemeDate);

    public List<UserTheme> getThemesByDate(Date themedate);

    public List<UserTheme> getAllPersonalThemes();

    public List<UserTheme> getAllGlobalThemes();

    public List<UserTheme> getThemeList();

    public List<UserTheme> getAllRepeatableTheme();

}
