package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.NewsPaper;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ritesh
 */
@Repository
public class NewsPaperDAOImpl extends GenericDAOImpl<NewsPaper, String> implements NewsPaperDAO {

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
    
    @Transactional
    @Override
    public List<NewsPaper> getNewsPapers(String paperLang, boolean newsType) {
        Criteria criteria = super.currentSession().createCriteria(NewsPaper.class);
        if(!paperLang.equalsIgnoreCase("all")){
            criteria.add(Restrictions.eq("paperLang", paperLang));
        }
        criteria.add(Restrictions.eq("newsType", newsType));
        criteria.addOrder(Order.asc("paperName"));
        return criteria.list();
    }
}
