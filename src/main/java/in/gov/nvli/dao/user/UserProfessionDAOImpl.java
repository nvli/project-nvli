package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserProfession;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @Since 1
 * @version 1
 */
@Repository
public class UserProfessionDAOImpl extends GenericDAOImpl<UserProfession, Long> implements UserProfessionDAO {

    private static final Logger LOG = Logger.getLogger(UserProfessionDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional
    public List<UserProfession> listBaseProfessions() {
        return currentSession().createCriteria(UserProfession.class).
                add(Restrictions.isNull("parentProfession")).
                add(Restrictions.eq("isOther", false))
                .list();
    }

    @Override
    @Transactional
    public Serializable create(UserProfession userProfession) {
        return currentSession().save(userProfession);
    }
}
