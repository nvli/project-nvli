/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.UserOtherFeedback;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * All user other feedback related DAO methods can be implemented here
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Repository
public class UserOtherFeedbackImpl extends GenericDAOImpl<UserOtherFeedback, Long> implements UserOtherFeedbackDAO {

    private static final Logger LOG = Logger.getLogger(UserOtherFeedbackImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
