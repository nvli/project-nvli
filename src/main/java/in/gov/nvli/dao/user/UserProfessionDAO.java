package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserProfession;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @Since 1
 * @version 1
 */
public interface UserProfessionDAO extends IGenericDAO<UserProfession, Long> {

    public Serializable create(UserProfession userProfession);
//
//    public boolean getByID(long professionId);

    public List<UserProfession> listBaseProfessions();

}
