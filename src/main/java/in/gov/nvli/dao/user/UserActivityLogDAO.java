package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserActivityLog;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Bhumika,Ruturaj
 * @since 1
 * @version 1
 */
public interface UserActivityLogDAO extends IGenericDAO<UserActivityLog, Long> {

    public boolean saveActivityLog(UserActivityLog logObj);

    public UserActivityLog saveUserActivityLog(UserActivityLog logObj);

    public List<UserActivityLog> getActivityLogListOfUser(int pageNumber, int pageWindow, long userId) throws Exception;

    public List<UserActivityLog> getFilteredActivityLogListOfUser(int pageNumber, int pageWindow, long userId, Set<Integer> activityIds) throws Exception;

    public long getActivityLogListOfUserTotalCountByUserId(long userId) throws Exception;

    public long getFilteredActivityLogListOfUserTotalCountByUserId(long userId, Set<Integer> activityIds) throws Exception;

}
