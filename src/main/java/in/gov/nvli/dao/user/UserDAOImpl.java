package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.Cities;
import in.gov.nvli.domain.user.Country;
import in.gov.nvli.domain.user.DeactivationFeedbackBase;
import in.gov.nvli.domain.user.District;
import in.gov.nvli.domain.user.SecretKeyDetails;
import in.gov.nvli.domain.user.States;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Vivek Bugale <bvivek@cdac.in>
 * @author Gulafsha <gulafsha@cdac.in>
 * @author Ruturaj Powar<rutupowar@gmail.com>
 */
@Repository("userDAO")
public class UserDAOImpl extends GenericDAOImpl<User, Long> implements UserDAO {

    @Autowired
    private UserService userService;

    /**
     * Method to get list of all users.
     *
     * @return List<User>
     */
    @Override
    @Transactional
    public List<User> getUsers() {
        return list();
    }

    /**
     * Method to find user by email.
     *
     * @param email
     * @return {@link User}
     */
    @Override
    @Transactional
    public User findByEmail(String email) {
        Criteria criteria = super.currentSession().createCriteria(User.class);
//        criteria.setFetchMode("roles", FetchMode.JOIN)
//                .setFetchMode("activityLogs", FetchMode.JOIN)
//                .setFetchMode("interests", FetchMode.JOIN);
        criteria.add(Restrictions.eq("email", email));
        User user = (User) criteria.uniqueResult();
        if (user != null) {
            Hibernate.initialize(user.getRoles());
        }
        return user;
    }

    /**
     * Method to find user by salt
     *
     * @param salt
     * @return {@link User}
     */
    @Override
    public User findBySalt(String salt) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("salt", salt));
        return (User) criteria.uniqueResult();
    }

    /**
     * When login with mail and password this method is used to fetch user.
     *
     * @param emailId
     * @param passcode
     * @return {@link User}
     */
    @Override
    public User getUserByMailAndPasscode(String emailId, String passcode) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("email", emailId)).add(Restrictions.eq("forgottenPasswordCode", passcode));
        return (User) criteria.uniqueResult();
    }

    @Override
    public void flush() {
    }

    /**
     * Method to check if username is already exits.
     *
     * @param username
     * @return {@link Boolean}
     */
    @Override
    public boolean existsUsername(String username) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("username", username));
        User u = (User) criteria.uniqueResult();
        if (null != u) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to check if email is already present or not
     *
     * @param useremail
     * @return {@link Boolean}
     */
    @Override
    public boolean existsUseremail(String useremail) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("email", useremail));
        User u = (User) criteria.uniqueResult();
        if (null != u) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to fetch user by id.
     *
     * @param userId
     * @return {@link User}
     */
    @Override
    public User getUserById(Long userId) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("id", userId));
        return (User) criteria.uniqueResult();
    }

    /**
     * Method to fetch suggested users as logged in user start typing name to
     * find another user
     *
     * @param username as typed by logged in user.
     * @param showOnlyActiveUsers ether to show all users or only active users
     * @return List<User>
     */
    @Override
    public List<User> usersSuggestions(String username, boolean showOnlyActiveUsers) {
        if (username != null) {
            byte b = 1;
            byte enabled = 0;
            String currentUserEmail = userService.getCurrentUser().getEmail();
            String[] userNameList = username.trim().split(" ");
            Query query = null;
            String queryString = "";
            switch (userNameList.length) {
                case 1:
                    if (showOnlyActiveUsers) {
                        queryString = " FROM User u WHERE (u.firstName like :firstName or u.middleName like :middleName or u.lastName like :lastName) and (u.deleted !=:deleted) and (u.email !=:email) and (u.enabled !=:enabled) order by u.firstName,u.middleName,u.lastName";
                        query = super.currentSession().createQuery(queryString);
                        query.setParameter("deleted", b);
                        query.setParameter("email", currentUserEmail);
                        query.setParameter("enabled", enabled);

                    } else {
                        queryString = " FROM User u WHERE (u.firstName like :firstName or u.middleName like :middleName or u.lastName like :lastName) order by u.firstName,u.middleName,u.lastName";
                        query = super.currentSession().createQuery(queryString);
                    }
                    query.setParameter("firstName", userNameList[0] + "%");
                    query.setParameter("middleName", userNameList[0] + "%");
                    query.setParameter("lastName", userNameList[0] + "%");
                    break;
                case 2:
                    queryString = " FROM User u WHERE ((u.firstName like :firstName and (u.middleName like :middleName or u.lastName like :lastName))";
                    if (showOnlyActiveUsers) {
                        queryString = queryString + "or (u.firstName like :firstName1 and (u.middleName like :middleName1 or u.lastName like :lastName1))) and (u.deleted !=:deleted) and (u.email !=:email) and (u.enabled !=:enabled) order by u.firstName,u.middleName,u.lastName";
                        query = super.currentSession().createQuery(queryString);
                        query.setParameter("deleted", b);
                        query.setParameter("email", currentUserEmail);
                        query.setParameter("enabled", enabled);
                    } else {
                        queryString = " FROM User u WHERE ((u.firstName like :firstName and (u.middleName like :middleName or u.lastName like :lastName))";
                        queryString = queryString + "or (u.firstName like :firstName1 and (u.middleName like :middleName1 or u.lastName like :lastName1))) order by u.firstName,u.middleName,u.lastName";
                        query = super.currentSession().createQuery(queryString);
                    }
                    query.setParameter("firstName", userNameList[0] + "%");
                    query.setParameter("middleName", userNameList[1] + "%");
                    query.setParameter("lastName", userNameList[1] + "%");
                    query.setParameter("firstName1", userNameList[1] + "%");
                    query.setParameter("middleName1", userNameList[0] + "%");
                    query.setParameter("lastName1", userNameList[0] + "%");
                    break;
                case 3:
                    queryString = " FROM User u WHERE ((u.firstName like :firstName and u.middleName like :middleName and u.lastName like :lastName)";
                    queryString = queryString + " or (u.firstName like :firstName1 and u.middleName like :middleName1 and u.lastName like :lastName1)";
                    queryString = queryString + " or (u.firstName like :firstName2 and u.middleName like :middleName2 and u.lastName like :lastName2)";
                    queryString = queryString + " or (u.firstName like :firstName3 and u.middleName like :middleName3 and u.lastName like :lastName3)";
                    queryString = queryString + " or (u.firstName like :firstName4 and u.middleName like :middleName4 and u.lastName like :lastName4)";
                    if (showOnlyActiveUsers) {
                        queryString = queryString + " or (u.firstName like :firstName5 and u.middleName like :middleName5 and u.lastName like :lastName5)) and (u.deleted !=:deleted) and (u.email !=:email) and (u.enabled !=:enabled) ";
                        query = super.currentSession().createQuery(queryString + " order by u.firstName,u.middleName,u.lastName");
                        query.setParameter("deleted", b);
                        query.setParameter("email", currentUserEmail);
                        query.setParameter("enabled", enabled);
                    } else {
                        queryString = queryString + " or (u.firstName like :firstName5 and u.middleName like :middleName5 and u.lastName like :lastName5))";
                        query = super.currentSession().createQuery(queryString + " order by u.firstName,u.middleName,u.lastName");

                    }
                    query.setParameter("firstName", "%" + userNameList[0] + "%");
                    query.setParameter("middleName", userNameList[1] + "%");
                    query.setParameter("lastName", userNameList[2] + "%");

                    query.setParameter("firstName1", "%" + userNameList[0] + "%");
                    query.setParameter("middleName1", userNameList[2] + "%");
                    query.setParameter("lastName1", userNameList[1] + "%");

                    query.setParameter("firstName2", "%" + userNameList[1] + "%");
                    query.setParameter("middleName2", userNameList[0] + "%");
                    query.setParameter("lastName2", userNameList[2] + "%");

                    query.setParameter("firstName3", "%" + userNameList[1] + "%");
                    query.setParameter("middleName3", userNameList[2] + "%");
                    query.setParameter("lastName3", userNameList[0] + "%");

                    query.setParameter("firstName4", "%" + userNameList[2] + "%");
                    query.setParameter("middleName4", userNameList[1] + "%");
                    query.setParameter("lastName4", userNameList[0] + "%");

                    query.setParameter("firstName5", "%" + userNameList[2] + "%");
                    query.setParameter("middleName5", userNameList[0] + "%");
                    query.setParameter("lastName5", userNameList[1] + "%");
                    break;
            }
            List<User> userList = query.setMaxResults(50).list();
            if (userList.size() > 10) {
                if ((userList == null || userList.isEmpty()) && userNameList.length == 1) {
                    if (showOnlyActiveUsers) {
                        queryString = " FROM User u WHERE (u.firstName like :firstName or u.middleName like :middleName or u.lastName like :lastName) and (u.deleted !=:deleted) and (u.email !=:email) and (u.enabled !=:enabled) ";
                        query = super.currentSession().createQuery(queryString + " order by u.firstName , u.middleName , u.lastName");
                        query.setParameter("deleted", b);
                        query.setParameter("email", currentUserEmail);
                        query.setParameter("enabled", enabled);
                    } else {
                        queryString = " FROM User u WHERE (u.firstName like :firstName or u.middleName like :middleName or u.lastName like :lastName)";
                        query = super.currentSession().createQuery(queryString + " order by u.firstName , u.middleName , u.lastName");
                    }
                    query.setParameter("firstName", "%" + userNameList[0] + "%");
                    query.setParameter("middleName", "%" + userNameList[0] + "%");
                    query.setParameter("lastName", "%" + userNameList[0] + "%");
                    userList = query.setMaxResults(50 - userList.size()).list();
                }
            }
            return userList;
        } else {
            return null;
        }

    }

    /**
     * Method to gets email of users
     *
     * @param userIdList list of users whoes emails to be fetched.
     * @return List<String>
     */
    @Override
    public List<String> getEmailList(List<Long> userIdList) {
        Criteria criteria = super.currentSession().createCriteria(User.class);
        if (!userIdList.isEmpty()) {
            criteria.add(Restrictions.in("id", userIdList));
            List<User> userList = criteria.list();
            List<String> emailList = new ArrayList<>();
            for (User user : userList) {
                emailList.add(user.getEmail());
            }
            return emailList;
        }
        return null;
    }

    /**
     * Method to fetch users having same role.
     *
     * @param roleId role of which users to be fetched
     * @param pageNumber current page number
     * @param pageWindow maximum entries per page
     * @param sortBy Column on which sorting to be done
     * @param sortOrder either ascending or descending
     * @return List<User>
     */
    @Override
    public List<User> getUsersByRoleId(long roleId, int pageNumber, int pageWindow, String sortBy, String sortOrder) {
        Criteria criteria = super.currentSession().createCriteria(User.class).createAlias("roles", "userRole").
                add(Restrictions.eq("userRole.roleId", roleId));
        if (sortOrder.equals("asc")) {
            criteria.addOrder(Order.asc(sortBy));
        } else {
            criteria.addOrder(Order.desc(sortBy));
        }
        criteria.setFirstResult((pageNumber - 1) * pageWindow);
        criteria.setMaxResults(pageWindow);
        return criteria.list();
    }

    /**
     * Method to get how many users are there with same role.
     *
     * @param roleId role of which count of users to be fetched
     * @return long count of users.
     */
    @Override
    public long getTotalUserCountByRoleId(long roleId) {
        Criteria criteria = super.currentSession().createCriteria(User.class).createAlias("roles", "userRole").
                add(Restrictions.eq("userRole.roleId", roleId));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }

    /**
     * Method to find user by username.
     *
     * @param username
     * @return {@link User}
     */
    @Override
    public User findUserByUsername(String username) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("username", username));
        User user = (User) criteria.uniqueResult();
        if (user != null) {
            Hibernate.initialize(user.getRoles());
        }
        return user;
    }

    /**
     *
     * @param Principal
     * @return {@link Boolean} whether email is valid or not
     */
    @Override
    public boolean isValidEmail(String Principal) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        if (Principal.matches(EMAIL_REGEX)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Method to get users by assigned resources to them.
     *
     * @param resourceCode
     * @param userRole
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public List<Long> getUserIdsByAssignedUDCLanguageId(long udcLanguageId, String userRole) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(User.class)
                .createAlias("roles", "userRole")
                .createAlias("userLanguages", "language")
                .add(Restrictions.eq("userRole.code", userRole))
                .add(Restrictions.eq("language.id", udcLanguageId))
                .setProjection(Projections.property("id"));
        return criteria.list();
    }

    /**
     * Method to get users by assigned resources to them.
     *
     * @param resourceCode
     * @param userRole
     * @return
     * @throws Exception
     */
    @Override
    @Transactional
    public List<Long> getUserIdsByAssignedResource(String resourceCode, String userRole) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(User.class).createAlias("roles", "userRole").createAlias("assignedResources", "resource").add(Restrictions.eq("userRole.code", userRole)).add(Restrictions.eq("resource.resourceCode", resourceCode)).setProjection(Projections.property("id"));
        return criteria.list();
    }

    @Override
    @Transactional
    public JSONArray getUserByAssignedResourceAndUserIds(String resourceCode, Set<Long> userIds, String userRole) throws Exception {
        JSONObject jsonObject;
        JSONArray jsonArray = new JSONArray();
        Criteria criteria = super.currentSession().createCriteria(User.class)
                .createAlias("roles", "userRole")
                .createAlias("assignedResources", "resource")
                .add(Restrictions.eq("userRole.code", userRole))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .addOrder(Order.asc("firstName"))
                .addOrder(Order.asc("lastName"));
        if (userIds != null) {
            criteria.add(Restrictions.or(Restrictions.eq("resource.resourceCode", resourceCode), Restrictions.in("id", userIds)));
        } else {
            criteria.add(Restrictions.eq("resource.resourceCode", resourceCode));

        }

        for (User user : (List<User>) criteria.list()) {
            jsonObject = new JSONObject();
            jsonObject.put("id", user.getId());
            jsonObject.put("name", user.getFirstName() + " " + user.getLastName());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    /**
     * Method to get list of all countries
     *
     * @return List<Country>
     */
    @Override
    @Transactional
    public List<Country> getAllCountries() {
        Criteria criteria = super.currentSession().createCriteria(Country.class);
        return criteria.list();
    }

    /**
     * Method to get states belonging to particular country.
     *
     * @param countryId id of country
     * @return List<States>
     */
    @Override
    @Transactional
    public List<States> getStateByCountry(long countryId) {
        Criteria criteria = super.currentSession().createCriteria(Country.class).add(Restrictions.eq("countryId", countryId));
        Country country = (Country) criteria.uniqueResult();
        return country.getStateList();
    }

    /**
     * Method to get district belonging to particular state.
     *
     * @param stateId id of state of which districts to be fetched.
     * @return List<District>
     */
    @Override
    @Transactional
    public List<District> getDistrictByState(Long stateId) {
        Criteria criteria = super.currentSession().createCriteria(States.class).add(Restrictions.eq("stateId", stateId));
        States state = (States) criteria.uniqueResult();
        return state.getDisttList();
    }

    /**
     * Method to get cities belonging to particular cities.
     *
     * @param distictId id of district which cities to be fetched.
     * @return List<Cities>
     */
    @Override
    @Transactional
    public List<Cities> getCityByDistrict(Long distictId) {
        Criteria criteria = super.currentSession().createCriteria(District.class).add(Restrictions.eq("districtId", distictId));
        District district = (District) criteria.uniqueResult();
        return district.getCityList();
    }

    /**
     * method use to check whether user is activated or deactivated
     *
     * @return {@link Boolean}
     */
    @Override
    public boolean isUserEnabled(Long userId) {
        Criteria criteria = super.currentSession().createCriteria(User.class).add(Restrictions.eq("id", userId));
        User u = (User) criteria.uniqueResult();
        if (null != u) {
            if (u.getId() == 1) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * get list of all feedback available in database
     *
     * @return {@link List}
     */
    public List<DeactivationFeedbackBase> getFeedbackList() {
        Criteria criteria = super.currentSession().createCriteria(DeactivationFeedbackBase.class);
        return criteria.list();

    }

    @Override
    public Long findUserCount(String secretKey) {
        Criteria criteria = super.currentSession().createCriteria(SecretKeyDetails.class)
                .add(Restrictions.eq("secretKey", secretKey));
        criteria.setProjection(Projections.rowCount());
        System.out.println("criteria.uniqueResult();" + criteria.uniqueResult());
        return (Long) criteria.uniqueResult();
    }
}
