/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.DeactivationFeedbackBase;
import org.springframework.stereotype.Repository;

/**
 * All feedback base related DAO methods can be implemented here
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Repository
public class DeactivationFeedbackBaseDAOImpl extends GenericDAOImpl<DeactivationFeedbackBase, Long> implements DeactivationFeedbackBaseDAO {

}
