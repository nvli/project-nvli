/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.SecretKeyDetails;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Gulafsha
 */
@Repository
public class SecretKeyDetailsDAOImpl extends GenericDAOImpl<SecretKeyDetails, Long> implements SecretKeyDetailsDAO {

    @Override
    public List<SecretKeyDetails> getUsersByFiltersWithLimit(String searchString, int dataLimit, int pageNo) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(SecretKeyDetails.class);
        criteria.add(Restrictions.like("secretKey", searchString, MatchMode.ANYWHERE));
        criteria.setFirstResult((pageNo - 1) * dataLimit);
        criteria.setMaxResults(dataLimit);
        return criteria.list();
    }

    @Override
    public long getUsersTotalCountByFilters(String searchString) throws Exception {
        Criteria criteria = super.currentSession().createCriteria(SecretKeyDetails.class);
        criteria.add(Restrictions.like("secretKey", searchString, MatchMode.ANYWHERE));
        criteria.setProjection(Projections.rowCount());
        return (long) criteria.uniqueResult();
    }
}