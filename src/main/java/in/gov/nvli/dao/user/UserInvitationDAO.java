package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserInvitation;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Bhumika
 * @since 1
 * @version 1
 */
public interface UserInvitationDAO extends IGenericDAO<UserInvitation, Long> {

    public UserInvitation getInvitationObjectByHash(String invitationHash);

    public boolean saveInvitationObject(UserInvitation invitationObj);

    public void updateInvitationObject(UserInvitation invitationObj);

    public List<UserInvitation> getInvitationsSentByUser(long userId);

    public UserInvitation getInvitaionObjectByInvitationId(long invitationId);

    public List getInvitaionObjectByEmailId(String inviteeEmail);

    public List<UserInvitation> getInvitedUser(int pageNumber, int pageWindow, String sortBy, String sortOrder);

    public Long getInvitedUserCount();

    public boolean checkInvitationAlreadySent(String emailId);

    public List<UserInvitation> getInvitedUserByFiltersWithLimit(String searchString, int dataLimit, int pageNo);

    public long getInvitedUserCountByFilters(String searchString);

}
