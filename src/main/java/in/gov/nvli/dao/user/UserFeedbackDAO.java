/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.user;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.UserFeedback;

/**
 * All user feedback related DAO methods can be implemented here
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public interface UserFeedbackDAO extends IGenericDAO<UserFeedback, Long> {

}
