package in.gov.nvli.dao.curationactivity;

import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.domain.crowdsource.ResourcesWithoutLibUser;
import in.gov.nvli.domain.resource.Organization;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Interface for CurationActivityDAO
 *
 * @author Ritesh MAlviya
 * @author Vivek Bugale
 * @version 1
 * @since 1
 */
public interface ICurationActivityDAO {

    /**
     * To get List of{@link CrowdSourceEdit} by using user id for particular
     * period.
     *
     * @param userId
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public List<CrowdSourceEdit> getCrowdSourceEditRecordListByUserId(Long userId, Date startDate, Date endDate) throws Exception;

    /**
     * To get List of {@link CrowdSource} having expert assigned to it for
     * particular periods .
     *
     * @param expertId
     * @param startDate
     * @param endDate
     * @return List of {@link CrowdSource}
     * @throws Exception
     */
    public List<CrowdSource> getCrowdSourceRecordListByExpertId(Long expertId, Date startDate, Date endDate) throws Exception;

    /**
     * To get Map of resource code and resource name.
     *
     * @param resourceCodeList
     * @return Map of resource code and resource name.
     * @throws Exception
     */
    public Map<String, String> getResourceCodeNameMap(List<String> resourceCodeList) throws Exception;

    /**
     * To check resources without Library User Register
     *
     * @return List of {@link ResourcesWithoutLibUser}.
     * @throws java.lang.Exception
     */
    public List<ResourcesWithoutLibUser> checkResourcesWithoutLibUserRegister() throws Exception;

    /**
     * To get Library Expert Statistics.
     *
     * @param resourceCode
     * @return List<CrowdSource>.
     */
    public List<CrowdSource> getLibraryExpertStatistics(String resourceCode);

    /**
     * To fetch all available organizations.
     */
    public List<Organization> fetchAllOrganizations();
}
