package in.gov.nvli.dao.curationactivity;

import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.domain.crowdsource.ResourcesWithoutLibUser;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.domain.resource.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * This is the implementation of curation activity DAO.
 *
 * @author Ritesh Malviya
 * @author Vivek Bugale
 * @version 1
 * @since 1
 */
@Repository
public class CurationActivityDAO implements ICurationActivityDAO {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Transactional
    @Override
    public List<CrowdSourceEdit> getCrowdSourceEditRecordListByUserId(Long userId,Date startDate, Date endDate) throws Exception {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(CrowdSourceEdit.class);
        criteria.add(Restrictions.eq("user.id",userId))
                .add(Restrictions.ge("crowdSourceEditTimestamp",startDate))
                .add(Restrictions.le("crowdSourceEditTimestamp",endDate));
        return criteria.list();
    }
    
    @Transactional
    @Override
    public List<CrowdSource> getCrowdSourceRecordListByExpertId(Long expertId,Date startDate, Date endDate) throws Exception {
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(CrowdSource.class);
        criteria.add(Restrictions.eq("expertUserId.id",expertId))
                .add(Restrictions.ge("endTimestamp",startDate))
                .add(Restrictions.le("endTimestamp",endDate));
        return criteria.list();
    }

    @Override
    public Map<String, String> getResourceCodeNameMap(List<String> resourceCodeList) throws Exception {
        if(!resourceCodeList.isEmpty()) {
            Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Resource.class).add(Restrictions.in("resourceCode", resourceCodeList)).setProjection(Projections.projectionList().add(Projections.property("resourceCode")).add(Projections.property("resourceName")));
            Map<String,String> resourceCodeNameMap=new HashMap<>();
            for (Object obj : criteria.list()) {
                Object[] objects=(Object[])obj;
                resourceCodeNameMap.put((String)objects[0], (String)objects[1]);
            }
            return resourceCodeNameMap;
        }
        return null;
    }

    @Override
    public List<ResourcesWithoutLibUser> checkResourcesWithoutLibUserRegister() throws Exception{
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(ResourcesWithoutLibUser.class);       
        return criteria.list();
    }

    @Override
    public List<CrowdSource> getLibraryExpertStatistics(String resourceCode) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CrowdSource.class);
        List<CrowdSource> crowdSourceList = criteria.add(Restrictions.eq("subResourceCode", resourceCode)).add(Restrictions.isNotNull("expertUserId")).add(Restrictions.isNotNull("expertUserId")).list();
        return crowdSourceList;
    }

    @Override
    public List<Organization> fetchAllOrganizations() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Organization.class);
        List<Organization> organizationsList = criteria.list();
        return organizationsList;
    }

}
