/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.app;

import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.app.FeatureConfig;
import java.util.List;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public interface FeatureConfigurationDAO extends IGenericDAO<FeatureConfig, Long> {

    public List<FeatureConfig> featuresList();

    public void updateFeatureStatus(Long featureId, boolean featureStatus);

    public boolean existsFeatureName(String FeatureName);

    public boolean addFeature(FeatureConfig feature);

    public boolean checkFeatureStatus(String featureName);
}
