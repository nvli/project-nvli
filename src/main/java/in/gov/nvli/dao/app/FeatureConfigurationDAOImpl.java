/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dao.app;

import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.dao.user.UserRoleDAO;
import in.gov.nvli.domain.app.FeatureConfig;
import in.gov.nvli.domain.user.NotificationSubscription;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Repository
public class FeatureConfigurationDAOImpl extends GenericDAOImpl<FeatureConfig, Long> implements FeatureConfigurationDAO {

    private static final Logger LOG = Logger.getLogger(in.gov.nvli.dao.app.FeatureConfigurationDAOImpl.class);

    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional
    @Override
    public List<FeatureConfig> featuresList() {
        Criteria featureCriteria = currentSession().createCriteria(FeatureConfig.class);
        return featureCriteria.list();
    }

    @Override
    public void updateFeatureStatus(Long featureId, boolean featureStatus) {
        try {
            Criteria criteria = super.currentSession().createCriteria(FeatureConfig.class).add(Restrictions.eq("id", featureId));
            FeatureConfig featureObj = (FeatureConfig) criteria.uniqueResult();
            featureObj.setFeatureEnabledStatus(featureStatus);
            saveOrUpdate(featureObj);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean existsFeatureName(String FeatureName) {
        Criteria criteria = super.currentSession().createCriteria(FeatureConfig.class).add(Restrictions.eq("featureName", FeatureName));
        FeatureConfig feature = (FeatureConfig) criteria.uniqueResult();
        if (null != feature) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addFeature(FeatureConfig feature) {
        try {
            return createNew(feature);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public boolean checkFeatureStatus(String featureName) {
        try {
            Criteria criteria = super.currentSession().createCriteria(FeatureConfig.class).add(Restrictions.eq("featureName", featureName));
            FeatureConfig featureObj = (FeatureConfig) criteria.uniqueResult();
            return featureObj.isFeatureEnabledStatus();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

}
