package in.gov.nvli.analytics.mongodb.domain;

import java.io.Serializable;

/**
 *
 * @author Ruturaj
 */
public class Record implements Serializable{
    String recordIdentifier;
    Integer count;

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public Integer getCount() {
        return count;
    }

    /**
     *
     * @param count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

}
