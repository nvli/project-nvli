package in.gov.nvli.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.springframework.web.multipart.MultipartFile;

/**
 * this bean use to hold user information at the time of any update in user
 * profile
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class UserDetailsBean implements Serializable {

    /**
     * variable use to hold user id
     */
    private Long id;
    /**
     * variable use to check whether user is enable or not
     */
    private byte enabled;
    /**
     * variable use to hold username
     */
    private String username;
    /**
     * variable use to hold user id
     */
    private String email;
    /**
     * variable use to hold first name
     */
    private String firstName;
    /**
     * variable use to hold middle name
     */
    private String middleName;
    /**
     * variable use to hold last name
     */
    private String lastName;
    /**
     * variable use to hold contact
     */
    private String contact;
    /**
     * variable use to hold address
     */
    private String address;
    /**
     * variable use to hold city
     */
    private String city;
    /**
     * variable use to hold district
     */
    private String district;
    /**
     * variable use to hold state
     */
    private String state;
    /**
     * variable use to hold country
     */
    private String country;
    /**
     * variable use to hold gender
     */
    private String gender;
    /**
     * variable use to hold all roleIds that is assigned to a user
     */
    private List<Long> roleIds;
    /**
     * variable use to hold map for showing roleIds and role names
     */
    private Map<Long, String> rolemap;
    /**
     * used while updating image
     */
    private MultipartFile profileImage;
    /**
     * used while showing image
     */
    private byte[] profileImageByte;
    /**
     * variable use to hold profile pic path
     */
    private String profilePicPath;

    /**
     * Constructor
     */
    public UserDetailsBean() {
    }

    /**
     * Constructor
     *
     * @param id
     * @param enabled
     * @param username
     * @param email
     * @param firstName
     * @param middleName
     * @param lastName
     * @param contact
     * @param address
     * @param city
     * @param district
     * @param state
     * @param country
     * @param gender
     * @param roleIds
     * @param profilePicPath
     */
    public UserDetailsBean(
            Long id, byte enabled, String username, String email, String firstName,
            String middleName, String lastName, String contact,
            String city, String address, String district, String state, String country,
            String gender, List<Long> roleIds, String profilePicPath) {
        this.id = id;
        this.enabled = enabled;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.contact = contact;
        this.address = address;
        this.city = city;
        this.district = district;
        this.state = state;
        this.country = country;
        this.gender = gender;
        this.roleIds = roleIds;
        this.profilePicPath = profilePicPath;
        this.profileImageByte = profileImageByte;
//        this.userPhotoExtension=userPhotoExtension;

    }

    /**
     * Constructor
     *
     * @param id
     * @param enabled
     * @param username
     * @param email
     * @param firstName
     * @param middleName
     * @param lastName
     * @param contact
     * @param address
     * @param city
     * @param district
     * @param state
     * @param country
     * @param gender
     * @param rolemap
     * @param profilePicPath
     */
    public UserDetailsBean(
            Long id, byte enabled, String username, String email, String firstName,
            String middleName, String lastName, String contact, String address,
            String city, String district, String state, String country,
            String gender, Map<Long, String> rolemap, String profilePicPath) {
        this.id = id;
        this.enabled = enabled;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.contact = contact;
        this.address = address;
        this.city = city;
        this.district = district;
        this.state = state;
        this.country = country;
        this.gender = gender;
        this.rolemap = rolemap;
        this.profilePicPath = profilePicPath;
//        this.profileImageByte=profileImageByte;
//        this.userPhotoExtension=userPhotoExtension;
    }

    /**
     *
     * @return {@link UserDetailsBean#id}
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return {@link UserDetailsBean#enabled}
     */
    public byte getEnabled() {
        return enabled;
    }

    /**
     *
     * @param enabled
     */
    public void setEnabled(byte enabled) {
        this.enabled = enabled;
    }

    /**
     *
     * @return {@link UserDetailsBean#username}
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return {@link UserDetailsBean#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return {@link UserDetailsBean#firstName}
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return {@link UserDetailsBean#middleName}
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     *
     * @return {@link UserDetailsBean#lastName}
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return {@link UserDetailsBean#contact}
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     *
     * @return {@link UserDetailsBean#address}
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return {@link UserDetailsBean#city}
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return {@link UserDetailsBean#district}
     */
    public String getDistrict() {
        return district;
    }

    /**
     *
     * @param district
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     *
     * @return {@link UserDetailsBean#state}
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return {@link UserDetailsBean#country}
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return {@link UserDetailsBean#gender}
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return {@link UserDetailsBean#roleIds}
     */
    public List<Long> getRoleIds() {
        return roleIds;
    }

    /**
     *
     * @param roleIds
     */
    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }

    /**
     *
     * @return {@link UserDetailsBean#rolemap}
     */
    public Map<Long, String> getRolemap() {
        return rolemap;
    }

    /**
     *
     * @param rolemap
     */
    public void setRolemap(Map<Long, String> rolemap) {
        this.rolemap = rolemap;
    }

    /**
     *
     * @return {@link UserDetailsBean#profileImage}
     */
    public MultipartFile getProfileImage() {
        return profileImage;
    }

    /**
     *
     * @param profileImage
     */
    public void setProfileImage(MultipartFile profileImage) {
        this.profileImage = profileImage;
    }

    /**
     *
     * @return {@link UserDetailsBean#profilePicPath}
     */
    public String getProfilePicPath() {
        return profilePicPath;
    }

    /**
     *
     * @param profilePicPath
     */
    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    /**
     *
     * @return {@link UserDetailsBean#profileImageByte}
     */
    public byte[] getProfileImageByte() {
        return profileImageByte;
    }

    /**
     *
     * @param profileImageByte
     */
    public void setProfileImageByte(byte[] profileImageByte) {
        this.profileImageByte = profileImageByte;
    }

    @Override
    public String toString() {
        return "UserDetailsBean{" + "roles=" + roleIds.get(0) + '}';
    }

}
