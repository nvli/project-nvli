package in.gov.nvli.beans;

import in.gov.nvli.custom.annotation.IsEmailAvailable;
import in.gov.nvli.custom.annotation.PasswordMatches;
import in.gov.nvli.custom.annotation.ValidEmail;
import javax.persistence.Basic;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * This bean is use to hold user information at the time of registration
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@PasswordMatches
public class NewUserBean {

    /**
     * The username of New User, which should not be empty with size 3-20
     * characters of type String
     */
    @NotEmpty(message = "Please enter a username")
    @Size(min = 3, max = 20, message = "Username should be between 3 and 20 characters")
    private String username;

    /**
     * Password of New User
     */
    @Basic(optional = false)
    @NotNull
    @NotEmpty(message = "Please enter your password.")
    @Size(min = 6, max = 15, message = "Password must be between 6 and 15 characters")
    private String password;

    /**
     * Repeat Password of New User for password matching validation of type
     * String
     */
    @Basic(optional = false)
    @NotNull
    @NotEmpty(message = "Please enter your confirm password.")
    @Size(min = 6, max = 15, message = "Password must be between 6 and 15 characters")
    private String matchingPassword;

    /**
     * Email of User as string of maximum size 50
     */
    @IsEmailAvailable(message = "Account already regsitered with this email id")
    @ValidEmail
    @Basic(optional = false)
    @NotNull
    @Size(max = 50)
    @NotEmpty(message = "Please enter your Email ID")
    private String email;

    /**
     * 1-40 character long First Name of User
     */
    @Size(min = 1, max = 40)
    @NotEmpty(message = "First Name can't be empty")
    private String firstName;
    /**
     * 1-40 character long middle name of User
     */
    @Size(max = 40)
    private String middleName;

    /**
     * 1-40 character long last name of User
     */
    @Size(max = 40)
    @NotEmpty(message = "Last Name can't be empty")
    private String lastName;

    /**
     * Max 40 Character long User's Profession
     */
    @Size(max = 40)
    private String profession;

    /**
     * User's Contact number
     */
    @Size(max = 15)
    private String contact;
    /**
     * User's address if user does not belongs India
//     */
//    private String address;
//    /**
//     * User's city location
//     */
//    private String city;
//
//    /**
//     * User's district location
//     */
//    private String district;
//
//    /**
//     * User's state location
//     */
//    private String state;
//
//    @Basic(optional = false)
////    @NotNull
////    @NotEmpty(message = "Please enter your country")
//    private String country;

    /**
     * Gender of user
     */
    @Size(max = 15)
    @NotNull
    @NotEmpty(message = "Please enter your gender")
    private String gender;

    private String captchatext;

    /**
     * Default Constructor
     */
    public NewUserBean() {
    }

    /**
     *
     * @return {@link NewUserBean#username}
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return {@link NewUserBean#password}
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return {@link NewUserBean#matchingPassword}
     */
    public String getMatchingPassword() {
        return matchingPassword;
    }

    /**
     *
     * @param matchingPassword
     */
    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    /**
     *
     * @return {@link NewUserBean#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return {@link NewUserBean#firstName}
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return {@link NewUserBean#middleName}
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     *
     * @return {@link NewUserBean#lastName}
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return {@link NewUserBean#profession}
     */
    public String getProfession() {
        return profession;
    }

    /**
     *
     * @param profession
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }

    /**
     *
     * @return {@link NewUserBean#contact}
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     *
     * @return {@link NewUserBean#city}
     */
//    public String getCity() {
//        return city;
//    }
//
//    /**
//     *
//     * @param city
//     */
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    /**
//     *
//     * @return {@link NewUserBean#district}
//     */
//    public String getDistrict() {
//        return district;
//    }

    /**
     *
     * @param district
     */
//    public void setDistrict(String district) {
//        this.district = district;
//    }
//
//    /**
//     *
//     * @return {@link NewUserBean#state}
//     */
//    public String getState() {
//        return state;
//    }
//
//    /**
//     *
//     * @param state
//     */
//    public void setState(String state) {
//        this.state = state;
//    }
    /**
     *
     * @return {@link NewUserBean#country}
     */
//    public String getCountry() {
//        return country;
//    }
//
//    /**
//     *
//     * @return {@link NewUserBean#address}
//     */
//    public String getAddress() {
//        return address;
//    }
//
//    /**
//     *
//     * @param address
//     */
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    /**
//     *
//     * @param country
//     */
//    public void setCountry(String country) {
//        this.country = country;
//    }
    /**
     *
     * @return {@link NewUserBean#gender}
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return {@link NewUserBean#captchatext}
     */
    public String getCaptchatext() {
        return captchatext;
    }

    /**
     *
     * @param captchatext
     */
    public void setCaptchatext(String captchatext) {
        this.captchatext = captchatext;
    }

}
