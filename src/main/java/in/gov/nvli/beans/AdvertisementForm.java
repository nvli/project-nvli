/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.beans;

import java.util.Date;

/**
 * This bean is used to submit data to ad-analytics server
 *
 * @author Doppa Srinivas
 */
public class AdvertisementForm {

    /**
     * Id of the user who is created this ad.
     */
    private int userId;
    /**
     * Description of the ad.
     */
    private String description;
    /**
     * Header of the ad.
     */
    private String header;
    /**
     * Keywords associated with the ad.
     */
    private String tags;
    /**
     * Destination URL of the ad.
     */
    private String destinationUrl;
    /**
     * Date of creation of ad.
     */
    private Date creationDate;
    /**
     * Date of ad will expire.
     */
    private Date expireDate;
    /**
     * Path of the media.
     */
    private String relativePath;
    /**
     * Size of the file.
     */
    private double fileSize;
    /**
     * Name of the file or media.
     */
    private String mediaName;
    /**
     * Duration of video
     */
    private int videoDuration;
    /**
     * Mime type of the media.
     */
    private long mimeType;
    /**
     * Dimension of the ad.
     */
    private long dimension;
    /**
     * Interest areas of the targeted user
     */
    private String interestAreas;
    /**
     * State of the targeted user
     */
    private String state;
    /**
     * City of the targeted user
     */
    private String city;
    /**
     * Country of the targeted user
     */
    private String country;
    /**
     * professions of the targeted user
     */
    private String profession;
    /**
     * gender of the user of the targeted user
     */
    private String gender;
    /**
     * age group of the targeted user
     */
    private String ageGroup;
    /**
     * languages known of the targeted user
     */
    private String languages;
    /**
     * District of the user of the targeted user
     */
    private String district;

    public AdvertisementForm() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDestinationUrl() {
        return destinationUrl;
    }

    public void setDestinationUrl(String destinationUrl) {
        this.destinationUrl = destinationUrl;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getMimeType() {
        return mimeType;
    }

    public void setMimeType(long mimeType) {
        this.mimeType = mimeType;
    }

    public long getDimension() {
        return dimension;
    }

    public void setDimension(long dimension) {
        this.dimension = dimension;
    }

    public String getInterestAreas() {
        return interestAreas;
    }

    public void setInterestAreas(String interestAreas) {
        this.interestAreas = interestAreas;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
