/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.beans.ontology;

/**
 *
 * @author Gajraj
 */
public class UserCustomBookmarkedRecord {

    private String recordIdentifier;
    private long customClassId;

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public long getCustomClassId() {
        return customClassId;
    }

    public void setCustomClassId(long customClassId) {
        this.customClassId = customClassId;
    }

}
