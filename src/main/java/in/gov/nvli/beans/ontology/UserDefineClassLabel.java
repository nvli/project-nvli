/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.beans.ontology;

import java.util.Set;

/**
 *
 * @author Gajraj
 */
public class UserDefineClassLabel {

    private OntDomainClasses ontDomainClass;

    private String classLabel;

    private String languageCode;

    private Set<UserCustomBookmarkedRecord> bookmarkedRecords;

    public UserDefineClassLabel() {
    }

    public UserDefineClassLabel(OntDomainClasses ontDomainClass, String classlabel, String languageCode) {
        this.ontDomainClass = ontDomainClass;
        this.classLabel = classlabel;
        this.languageCode = languageCode;
    }

    public OntDomainClasses getOntDomainClass() {
        return ontDomainClass;
    }

    public void setOntDomainClass(OntDomainClasses ontDomainClass) {
        this.ontDomainClass = ontDomainClass;
    }

    public String getClassLabel() {
        return classLabel;
    }

    public void setClassLabel(String classLabel) {
        this.classLabel = classLabel;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Set<UserCustomBookmarkedRecord> getBookmarkedRecords() {
        return bookmarkedRecords;
    }

    public void setBookmarkedRecords(Set<UserCustomBookmarkedRecord> bookmarkedRecords) {
        this.bookmarkedRecords = bookmarkedRecords;
    }
}
