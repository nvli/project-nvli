package in.gov.nvli.beans.ontology;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gajraj
 */
public class UserDefinedCustomLabelDto {

    long customLabelId;
    long userId;
    long ontologyFileId;
    String className;
    long refOntClassId;
    String customLabel;
    String lanuageCode;

    public UserDefinedCustomLabelDto() {
    }

    public UserDefinedCustomLabelDto(long userId, String className, long refOntClassId, String customLabel, String lanuageCode) {
        this.userId = userId;
        this.className = className;
        this.refOntClassId = refOntClassId;
        this.customLabel = customLabel;
        this.lanuageCode = lanuageCode;
    }

    public UserDefinedCustomLabelDto(long userId, long refOntClassId, String customLabel) {
        this.userId = userId;
        this.refOntClassId = refOntClassId;
        this.customLabel = customLabel;
    }

    public void setCustomLabelId(long customLabelId) {
        this.customLabelId = customLabelId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getRefOntClassId() {
        return refOntClassId;
    }

    public void setRefOntClassId(long refOntClassId) {
        this.refOntClassId = refOntClassId;
    }

    public String getCustomLabel() {
        return customLabel;
    }

    public void setCustomLabel(String customLabel) {
        this.customLabel = customLabel;
    }

    public String getLanuageCode() {
        return lanuageCode;
    }

    public void setLanuageCode(String lanuageCode) {
        this.lanuageCode = lanuageCode;
    }

    @Override
    public String toString() {
        return "UserDefinedCustomLabelDto{" + "customClassId=" + customLabelId + ", userId=" + userId + ", refOntClassId=" + refOntClassId + ", customLabel=" + customLabel + ", lanuageCode=" + lanuageCode + ",ontologyFileId :" + ontologyFileId + ",className:" + className + " '}'";
    }

    public long getOntologyFileId() {
        return ontologyFileId;
    }

    public void setOntologyFileId(long ontologyFileId) {
        this.ontologyFileId = ontologyFileId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
