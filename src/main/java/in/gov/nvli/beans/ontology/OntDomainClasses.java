package in.gov.nvli.beans.ontology;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author Gajraj
 */
public class OntDomainClasses implements Serializable {

    private String classname;
    private String qualifiedClassName;
    private int strength;
    private String description;
    private DomainOntology domainOntology;
    private Set<OntDomainClasses> childClasses;
    private OntDomainClasses parentClass;

    public OntDomainClasses() {
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DomainOntology getDomainOntology() {
        return domainOntology;
    }

    public void setDomainOntology(DomainOntology domainOntology) {
        this.domainOntology = domainOntology;
    }

    public Set<OntDomainClasses> getChildClasses() {
        return childClasses;
    }

    public void setChildClasses(Set<OntDomainClasses> childClasses) {
        this.childClasses = childClasses;
    }

    public OntDomainClasses getParentClass() {
        return parentClass;
    }

    public void setParentClass(OntDomainClasses parentClass) {
        this.parentClass = parentClass;
    }

    public String getQualifiedClassName() {
        return qualifiedClassName;
    }

    public void setQualifiedClassName(String qualifiedClassName) {
        this.qualifiedClassName = qualifiedClassName;
    }

    @Override
    public String toString() {
        return "OntDomainClasses{" + "classname=" + classname + ", qualifiedClassName=" + qualifiedClassName + ", strength=" + strength + ", description=" + description + ", domainOntology=" + domainOntology + ", childClasses=" + childClasses + ", parentClass=" + parentClass + '}';
    }

}
