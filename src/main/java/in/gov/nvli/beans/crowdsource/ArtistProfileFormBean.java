/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.beans.crowdsource;

/**
 *
 * @author richa
 */
public class ArtistProfileFormBean {

  private String recordIdentifier;
  private ArtistProfile artistProfile;

  public ArtistProfileFormBean() {
  }
  
  

  public ArtistProfileFormBean(String recordIdentifier, ArtistProfile artistProfile) {
    this.recordIdentifier = recordIdentifier;
    this.artistProfile = artistProfile;
  }

  public String getRecordIdentifier() {
    return recordIdentifier;
  }

  public void setRecordIdentifier(String recordIdentifier) {
    this.recordIdentifier = recordIdentifier;
  }

  public ArtistProfile getArtistProfile() {
    return artistProfile;
  }

  public void setArtistProfile(ArtistProfile artistProfile) {
    this.artistProfile = artistProfile;
  }

}
