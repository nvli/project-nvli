package in.gov.nvli.beans.crowdsource;

import java.util.List;

/**
 * This bean is used to hold crowdsource form data.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public class CrowdSourceBean {
    /**
     * This is the unique id of crowdsource.
     */
    private String crowdSourceId;
    /**
     * This is the original meta data of the record.
     */
    private MetadataBean originalMetadata;
    /**
     * This is list of the edited versions of the record.
     */
    private List<CrowdSourceEditBean> crowdSourceEditBeanList;
    /**
     * These are the original UDC tags attached at time of record integrated
     * into system and separated by &|&.
     */
    private String crowdSourceUDCOriginalTags;
    /**
     * These are the UDC tags added by various users and separated by &|&.
     */
    private String crowdSourceUDCTagsAdded;
    /**
     * These are the custom tags and tagged by user
     */
    private String crowdSourceCustomTags;
    /**
     * This is the unique identifier of the record.
     */
    private String recordIdentifier;
    /**
     * This tag is to ensure whether record is editable or not.By default it's
     * set to true.
     */
    private boolean isRecordEditable = true;

    /**
     * This is to get the originalMetadata
     *
     * @return originalMetadata
     */
    public MetadataBean getOriginalMetadata() {
        return originalMetadata;
    }

    /**
     * This is to set originalMetadata.
     *
     * @param originalMetadata
     */
    public void setOriginalMetadata(MetadataBean originalMetadata) {
        this.originalMetadata = originalMetadata;
    }

    /**
     * This is to get the crowdSourceEditBeanList.
     *
     * @return crowdSourceEditBeanList
     */
    public List<CrowdSourceEditBean> getCrowdSourceEditBeanList() {
        return crowdSourceEditBeanList;
    }

    /**
     * This is to set the crowdSourceEditBeanList.
     *
     * @param crowdSourceEditBeanList
     */
    public void setCrowdSourceEditBeanList(List<CrowdSourceEditBean> crowdSourceEditBeanList) {
        this.crowdSourceEditBeanList = crowdSourceEditBeanList;
    }

    /**
     * This is to get the crowdSourceId.
     *
     * @return crowdSourceId
     */
    public String getCrowdSourceId() {
        return crowdSourceId;
    }

    /**
     * This is to set the crowdSourceId.
     *
     * @param crowdSourceId
     */
    public void setCrowdSourceId(String crowdSourceId) {
        this.crowdSourceId = crowdSourceId;
    }

    /**
     * This is to get the crowdSourceCustomTags.
     *
     * @return crowdSourceCustomTags
     */
    public String getCrowdSourceCustomTags() {
        return crowdSourceCustomTags;
    }

    /**
     * This is to set the crowdSourceCustomTags.
     *
     * @param crowdSourceCustomTags
     */
    public void setCrowdSourceCustomTags(String crowdSourceCustomTags) {
        this.crowdSourceCustomTags = crowdSourceCustomTags;
    }

    /**
     * This is to get the crowdSourceUDCOriginalTags.
     *
     * @return crowdSourceUDCOriginalTags
     */
    public String getCrowdSourceUDCOriginalTags() {
        return crowdSourceUDCOriginalTags;
    }

    /**
     * This is to set the crowdSourceUDCOriginalTags.
     *
     * @param crowdSourceUDCOriginalTags
     */
    public void setCrowdSourceUDCOriginalTags(String crowdSourceUDCOriginalTags) {
        this.crowdSourceUDCOriginalTags = crowdSourceUDCOriginalTags;
    }

    /**
     * This is to get the crowdSourceUDCTagsAdded.
     *
     * @return crowdSourceUDCTagsAdded
     */
    public String getCrowdSourceUDCTagsAdded() {
        return crowdSourceUDCTagsAdded;
    }

    /**
     * This is to set the crowdSourceUDCTagsAdded.
     *
     * @param crowdSourceUDCTagsAdded
     */
    public void setCrowdSourceUDCTagsAdded(String crowdSourceUDCTagsAdded) {
        this.crowdSourceUDCTagsAdded = crowdSourceUDCTagsAdded;
    }

    /**
     * This is to get the recordIdentifier.
     *
     * @return recordIdentifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is to set the recordIdentifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This is to get the isRecordEditable.
     *
     * @return
     */
    public boolean isIsRecordEditable() {
        return isRecordEditable;
    }

    /**
     * This is to set the isRecordEditable.
     *
     * @param isRecordEditable
     */
    public void setIsRecordEditable(boolean isRecordEditable) {
        this.isRecordEditable = isRecordEditable;
    }
}
