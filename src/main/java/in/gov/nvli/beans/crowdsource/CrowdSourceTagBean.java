package in.gov.nvli.beans.crowdsource;

/**
 * This bean is used to hold crowdsource custom tag form data.
 *
 * @author Milind
 * @version 1
 * @since 1
 */
public class CrowdSourceTagBean {
    /**
     * This is the unique identifier of the record.
     */
    private String recordIdentifier;
    /**
     * This will hold all the tags added by user , separated by &|&.
     */
    private String crowdSourceTags;
    /**
     * This is to differentiate the different type of tagging (UDC/Custom).
     */
    private String crowdSourceTagType;        

    /**
     * This is to get recordIdentifier.
     *
     * @return recordIdentifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is to set recordIdentifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This is to get crowdSourceTags.
     *
     * @return crowdSourceTags
     */
    public String getCrowdSourceTags() {
        return crowdSourceTags;
    }

    /**
     * This is to set crowdSourceTags.
     *
     * @param crowdSourceTags
     */
    public void setCrowdSourceTags(String crowdSourceTags) {
        this.crowdSourceTags = crowdSourceTags;
    }

    /**
     * This is to get crowdSourceTagType.
     *
     * @return crowdSourceTagType
     */
    public String getCrowdSourceTagType() {
        return crowdSourceTagType;
    }

    /**
     * This is to set crowdSourceTagType.
     *
     * @param crowdSourceTagType
     */
    public void setCrowdSourceTagType(String crowdSourceTagType) {
        this.crowdSourceTagType = crowdSourceTagType;
    }
}