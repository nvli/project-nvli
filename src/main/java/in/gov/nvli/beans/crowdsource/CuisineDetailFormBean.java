/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.beans.crowdsource;

/**
 *
 * @author root
 */
public class CuisineDetailFormBean {

    private String recordIdentifier;
    private CuisineDetail cuisineDetail;
    private String path;

    public CuisineDetailFormBean() {
    }

    public CuisineDetailFormBean(String recordIdentifier, CuisineDetail cuisineDetail, String path) {
        this.recordIdentifier = recordIdentifier;
        this.cuisineDetail = cuisineDetail;
        this.path = path;
    }

    

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public CuisineDetail getCuisineDetail() {
        return cuisineDetail;
    }

    public void setCuisineDetail(CuisineDetail cuisineDetail) {
        this.cuisineDetail = cuisineDetail;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    
}
