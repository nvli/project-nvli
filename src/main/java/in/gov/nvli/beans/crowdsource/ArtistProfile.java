package in.gov.nvli.beans.crowdsource;

import java.util.List;

/**
 * Used for ArtistProfile Information.
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
public class ArtistProfile {
  private String artistName;
  private String artistProfileLink;
  private String artistImageURL;
  private String artistDescription;
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String state;
  private String pincode;
  private String contact;
  private String socialContact;
  private String other;
  private String imageUploadPath;
  private List<ArtistImage> artistImages;

  public ArtistProfile() {
  }
  
  

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getArtistDescription() {
        return artistDescription;
    }

    public void setArtistDescription(String artistDescription) {
        this.artistDescription = artistDescription;
    }

    public String getArtistImageURL() {
        return artistImageURL;
    }

    public void setArtistImageURL(String artistImageURL) {
        this.artistImageURL = artistImageURL;
    }

    public List<ArtistImage> getArtistImages() {
        return artistImages;
    }

    public void setArtistImages(List<ArtistImage> artistImages) {
        this.artistImages = artistImages;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistProfileLink() {
        return artistProfileLink;
    }

    public void setArtistProfileLink(String artistProfileLink) {
        this.artistProfileLink = artistProfileLink;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getImageUploadPath() {
        return imageUploadPath;
    }

    public void setImageUploadPath(String imageUploadPath) {
        this.imageUploadPath = imageUploadPath;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSocialContact() {
        return socialContact;
    }

    public void setSocialContact(String socialContact) {
        this.socialContact = socialContact;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    } 

}
