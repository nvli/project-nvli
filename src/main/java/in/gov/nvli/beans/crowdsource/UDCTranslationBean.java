package in.gov.nvli.beans.crowdsource;

import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import java.util.List;
import java.util.Map;

/**
 * This bean is used to hold UDC translation form data.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public class UDCTranslationBean {
    /**
     * This is hold the UDC notation.
     */
    private String selectedNotation;
    /**
     * This is the original translation of a UDC .
     */
    private String originalTranslation;
    /**
     * This is the list of UDC translation done by users.
     */
    private List<UDCTagsCrowdsource> otherUsersTranslation;
    /**
     * ownTranslation
     */
    private String ownTranslation;
    /**
     * This is to check whether voting has done or not.
     */
    private boolean isVoted;
    /**
     * Map of voting done by different users.
     */
    private Map<Long, String> votingMap;

    /**
     * This empty constructor.
     */
    public UDCTranslationBean() {
    }

    /**
     * This is to get the value of selectedNotation.
     *
     * @return selectedNotation
     */
    public String getSelectedNotation() {
        return selectedNotation;
    }

    /**
     * This is to set the value of selectedNotation.
     *
     * @param selectedNotation
     */
    public void setSelectedNotation(String selectedNotation) {
        this.selectedNotation = selectedNotation;
    }

    /**
     * This is to get the value of originalTranslation.
     *
     * @return originalTranslation
     */
    public String getOriginalTranslation() {
        return originalTranslation;
    }

    /**
     * This is to set the value of originalTranslation.
     *
     * @param originalTranslation
     */
    public void setOriginalTranslation(String originalTranslation) {
        this.originalTranslation = originalTranslation;
    }

    /**
     * This is to get the value of otherUsersTranslation.
     *
     * @return otherUsersTranslation
     */
    public List<UDCTagsCrowdsource> getOtherUsersTranslation() {
        return otherUsersTranslation;
    }

    /**
     * This is to set the value of otherUsersTranslation.
     *
     * @param otherUsersTranslation
     */
    public void setOtherUsersTranslation(List<UDCTagsCrowdsource> otherUsersTranslation) {
        this.otherUsersTranslation = otherUsersTranslation;
    }

    /**
     * This is to get the value of ownTranslation.
     *
     * @return ownTranslation.
     */
    public String getOwnTranslation() {
        return ownTranslation;
    }

    /**
     * This is to set the value of ownTranslation.
     *
     * @param ownTranslation
     */
    public void setOwnTranslation(String ownTranslation) {
        this.ownTranslation = ownTranslation;
    }

    /**
     * This is to get the value of isVoted.
     *
     * @return isVoted
     */
    public boolean isIsVoted() {
        return isVoted;
    }

    /**
     * This is to set the value of isVoted.
     *
     * @param isVoted
     */
    public void setIsVoted(boolean isVoted) {
        this.isVoted = isVoted;
    }

    /**
     * This is to get the value of votingMap.
     *
     * @return votingMap
     */
    public Map<Long, String> getVotingMap() {
        return votingMap;
    }

    /**
     * This is to set the value of votingMap.
     *
     * @param votingMap
     */
    public void setVotingMap(Map<Long, String> votingMap) {
        this.votingMap = votingMap;
    }
}
