package in.gov.nvli.beans.crowdsource;

/**
 * Used For Indian Recipes.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
public class CuisineDetail {

    /**
     * name of Indian Recipes.
     */
    private String cuisineName;
    /**
     * state name.
     */
    private String state;
    /*
     * contributor Information
     */
    private String courtesy;
    /**
     * Veg or Non Veg.
     */
    private String cuisinetype;
    /**
     * description of recipes 
     */
    private String description;
    /**
     * Items that is used.
     */
    private String ingredients;
    /**
     * method of making
     */
    private String method;
    /**
     * site URL
     */
    private String siteURL;
    /**
     * you tube URL 
     */
    private String cuisineYoutubeURL;
    /**
     * Image upload location
     */
    private String imageUploadPath;
    /**
     * Name of Image.
     */
    private String imageName;

    public String getImageUploadPath() {
        return imageUploadPath;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
     
    public void setImageUploadPath(String imageUploadPath) {
        this.imageUploadPath = imageUploadPath;
    }
    
    public String getCourtesy() {
        return courtesy;
    }

    public void setCourtesy(String courtesy) {
        this.courtesy = courtesy;
    }

    public String getCuisineName() {
        return cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public String getCuisineYoutubeURL() {
        return cuisineYoutubeURL;
    }

    public void setCuisineYoutubeURL(String cuisineYoutubeURL) {
        this.cuisineYoutubeURL = cuisineYoutubeURL;
    }

    public String getCuisinetype() {
        return cuisinetype;
    }

    public void setCuisinetype(String cuisinetype) {
        this.cuisinetype = cuisinetype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getSiteURL() {
        return siteURL;
    }

    public void setSiteURL(String siteURL) {
        this.siteURL = siteURL;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
