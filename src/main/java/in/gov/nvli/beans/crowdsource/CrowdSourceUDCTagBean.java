package in.gov.nvli.beans.crowdsource;

/**
 * This bean is used to hold crowdsource UDC tag form data.
 *
 * @author Milind
 * @version 1
 * @since 1
 */
public class CrowdSourceUDCTagBean {

    /**
     * It hold crowdsource UDC tag id
     */
    private String crowdSourceUDCTagId;

    /**
     * It hold crowdsource UDC tags
     */
    private String crowdSourceTags;

    /**
     * It hold is edit flag values
     */
    private String isEdit;

    /**
     * It hold record identifier
     */
    private String recordIdentifier;

    /**
     * Getter
     *
     * @return crowdSourceTags
     */
    public String getCrowdSourceTags() {
        return crowdSourceTags;
    }

    /**
     * Setter
     *
     * @param crowdSourceTags
     */
    public void setCrowdSourceTags(String crowdSourceTags) {
        this.crowdSourceTags = crowdSourceTags;
    }

    /**
     * Getter
     *
     * @return crowdSourceUDCTagId
     */
    public String getCrowdSourceUDCTagId() {
        return crowdSourceUDCTagId;
    }

    /**
     * Setter
     *
     * @param crowdSourceUDCTagId
     */
    public void setCrowdSourceUDCTagId(String crowdSourceUDCTagId) {
        this.crowdSourceUDCTagId = crowdSourceUDCTagId;
    }

    /**
     * Getter
     *
     * @return isEdit
     */
    public String getIsEdit() {
        return isEdit;
    }

    /**
     * Setter
     *
     * @param isEdit
     */
    public void setIsEdit(String isEdit) {
        this.isEdit = isEdit;
    }

    /**
     * Getter
     *
     * @return recordIdentifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * Setter
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }
}
