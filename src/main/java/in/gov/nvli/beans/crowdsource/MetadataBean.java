package in.gov.nvli.beans.crowdsource;

import in.gov.nvli.domain.dublincore.DublinCoreMetadata;
import in.gov.nvli.domain.marc21.CollectionType;

/**
 * This bean is used to hold crowdsource metadata form data.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public class MetadataBean {
    /**
     * This is differentiate type of meta data.
     */
    private String metadataType;
    /**
     * This is for MARC21 meta data.
     */
    private CollectionType collectionType;
    /**
     * This is for dublincore meta data.
     */
    private DublinCoreMetadata dcMetadata;
    /**
     * This is the unique id of crowdsource.
     */
    private String crowdSourceId;
    /**
     * This is to differentiate b/w normal user and the library expert.
     */
    private String userType;
    /**
     * This is to differentiate whether record is going to edit for the first
     * time or not.
     */
    private boolean isEditFirstTime;
    /**
     * This will hold the status of edit.
     */
    private String editStatus;
    /**
     * This is the unique identifier of the record.
     */
    private String recordIdentifier;
    /**
     * This is to whether the record meta data is approved or not.
     */
    private boolean approved;
    /**
     * This is determine whether the record is editable or not.
     */
    private boolean isEdit;

    /**
     * This is to get the value of metadataType.
     *
     * @return metadataType
     */
    public String getMetadataType() {
        return metadataType;
    }

    /**
     * This is to set the value of metadataType
     *
     * @param metadataType
     */
    public void setMetadataType(String metadataType) {
        this.metadataType = metadataType;
    }

    /**
     * This is to get the value of collectionType.
     *
     * @return collectionType
     */
    public CollectionType getCollectionType() {
        return collectionType;
    }

    /**
     * This is to set the value of collectionType.
     *
     * @param collectionType
     */
    public void setCollectionType(CollectionType collectionType) {
        this.collectionType = collectionType;
    }

    /**
     * This is to get the value of dcMetadata.
     *
     * @return dcMetadata
     */
    public DublinCoreMetadata getDcMetadata() {
        return dcMetadata;
    }

    /**
     * This is to set the value of dcMetadata.
     *
     * @param dcMetadata
     */
    public void setDcMetadata(DublinCoreMetadata dcMetadata) {
        this.dcMetadata = dcMetadata;
    }

    /**
     * This is to get the value of crowdSourceId
     *
     * @return
     */
    public String getCrowdSourceId() {
        return crowdSourceId;
    }

    /**
     * This is to set the value of crowdSourceId.
     *
     * @param crowdSourceId
     */
    public void setCrowdSourceId(String crowdSourceId) {
        this.crowdSourceId = crowdSourceId;
    }

    /**
     * This is to get the value of userType.
     *
     * @return userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * This is to set the value of userType.
     *
     * @param userType
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * This is to get the value of isEditFirstTime.
     *
     * @return isEditFirstTime
     */
    public boolean getIsEditFirstTime() {
        return isEditFirstTime;
    }

    /**
     * This is to set the value of isEditFirstTime.
     *
     * @param isEditFirstTime
     */
    public void setIsEditFirstTime(boolean isEditFirstTime) {
        this.isEditFirstTime = isEditFirstTime;
    }

    /**
     * This is to get the value of editStatus.
     *
     * @return editStatus
     */
    public String getEditStatus() {
        return editStatus;
    }

    /**
     * This is to set the value of editStatus.
     *
     * @param editStatus
     */
    public void setEditStatus(String editStatus) {
        this.editStatus = editStatus;
    }

    /**
     * This is to get the value of recordIdentifier.
     *
     * @return recordIdentifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is to set the value of recordIdentifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This is to get the value of approved.
     *
     * @return approved
     */
    public boolean isApproved() {
        return approved;
    }

    /**
     * This is to set the value of approved.
     *
     * @param approved
     */
    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    /**
     * This is to get the value of isEdit.
     *
     * @return isEdit
     */
    public boolean isIsEdit() {
        return isEdit;
    }

    /**
     * This is to set the value of isEdit.
     *
     * @param isEdit
     */
    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }
}
