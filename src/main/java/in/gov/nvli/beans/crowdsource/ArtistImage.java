package in.gov.nvli.beans.crowdsource;

/**
 * Used For Artist Images.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
public class ArtistImage {

  private String imageName;
  private String imageTitle;
  private String imagePath;
  private String imageDescription;
  private String imageType;

  public ArtistImage() {
  }
  
  

  public ArtistImage(String imageName, String imageTitle, String imagePath, String imageDescription, String imageType) {
    this.imageName = imageName;
    this.imageTitle = imageTitle;
    this.imagePath = imagePath;
    this.imageDescription = imageDescription;
    this.imageType = imageType;
  }

 

  public String getImageDescription() {
    return imageDescription;
  }

  public void setImageDescription(String imageDescription) {
    this.imageDescription = imageDescription;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImageTitle() {
    return imageTitle;
  }

  public void setImageTitle(String imageTitle) {
    this.imageTitle = imageTitle;
  }

  public String getImageType() {
    return imageType;
  }

  public void setImageType(String imageType) {
    this.imageType = imageType;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }
  
}
