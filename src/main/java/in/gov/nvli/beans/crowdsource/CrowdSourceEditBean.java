package in.gov.nvli.beans.crowdsource;

/**
 * This bean is used to hold crowdsource edit form data.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public class CrowdSourceEditBean {
    /**
     * This is the unique identifier of the edited version.
     */
    private String crowdSourceEditId;
    /**
     * This is the edit number.
     */
    private Integer subEditNumber;
    /**
     * This object will hold the edited meta data.
     */
    private MetadataBean editedMetadata;
    /**
     * This flag will differentiate whether it is a curated version or
     * crowdsourced version.
     */
    private boolean isCuratedVersion;

    /**
     * This is to the get crowdSourceEditId.
     *
     * @return crowdSourceEditId
     */
    public String getCrowdSourceEditId() {
        return crowdSourceEditId;
    }

    /**
     * This is to the set crowdSourceEditId.
     *
     * @param crowdSourceEditId
     */
    public void setCrowdSourceEditId(String crowdSourceEditId) {
        this.crowdSourceEditId = crowdSourceEditId;
    }

    /**
     * This is to the get subEditNumber.
     *
     * @return subEditNumber.
     */
    public Integer getSubEditNumber() {
        return subEditNumber;
    }

    /**
     * This is to the set subEditNumber.
     *
     * @param subEditNumber
     */
    public void setSubEditNumber(Integer subEditNumber) {
        this.subEditNumber = subEditNumber;
    }

    /**
     * This is to the get editedMetadata.
     *
     * @return editedMetadata
     */
    public MetadataBean getEditedMetadata() {
        return editedMetadata;
    }

    /**
     * This is to the set editedMetadata.
     *
     * @param editedMetadata
     */
    public void setEditedMetadata(MetadataBean editedMetadata) {
        this.editedMetadata = editedMetadata;
    }

    /**
     * This is to the get isCuratedVersion.
     *
     * @return isCuratedVersion
     */
    public boolean getIsCuratedVersion() {
        return isCuratedVersion;
    }

    /**
     * This is to the set isCuratedVersion.
     *
     * @param isCuratedVersion
     */
    public void setIsCuratedVersion(boolean isCuratedVersion) {
        this.isCuratedVersion = isCuratedVersion;
    }
}
