package in.gov.nvli.beans.crowdsource;

import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticleAttachedResource;
import java.util.List;

/**
 * This bean is used to hold create article form data.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public class CreateArticleFormBean {

    /**
     * @see CrowdSourceArticle
     */
    private CrowdSourceArticle crowdSourceArticle;

    /**
     * list of {@link CrowdSourceArticleAttachedResource}
     */
    private List<CrowdSourceArticleAttachedResource> crowdSourceArticleAttachedResources;

    /**
     * Getter
     *
     * @return {@link CrowdSourceArticle}
     */
    public CrowdSourceArticle getCrowdSourceArticle() {
        return crowdSourceArticle;
    }

    /**
     * Setter
     *
     * @param crowdSourceArticle Set {@link CrowdSourceArticle}
     */
    public void setCrowdSourceArticle(CrowdSourceArticle crowdSourceArticle) {
        this.crowdSourceArticle = crowdSourceArticle;
    }

    /**
     * Getter
     *
     * @return list of {@link CrowdSourceArticleAttachedResource}
     */
    public List<CrowdSourceArticleAttachedResource> getCrowdSourceArticleAttachedResources() {
        return crowdSourceArticleAttachedResources;
    }

    /**
     * Setter
     *
     * @param crowdSourceArticleAttachedResources Set list of
     * {@link CrowdSourceArticleAttachedResource}
     */
    public void setCrowdSourceArticleAttachedResources(List<CrowdSourceArticleAttachedResource> crowdSourceArticleAttachedResources) {
        this.crowdSourceArticleAttachedResources = crowdSourceArticleAttachedResources;
    }
}
