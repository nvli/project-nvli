package in.gov.nvli.beans;

import in.gov.nvli.util.CurationStatus;
import in.gov.nvli.webserviceclient.mit.SearchResult;
import java.io.Serializable;
import java.util.List;

/**
 * Form to communicate b/w nvli and mit webservice.
 *
 * @author Hemant Anjana
 */
@SuppressWarnings("serial")
public class CurationForm implements Serializable {

    /**
     * Resource code
     */
    private String resourceCode;
    /**
     * user id
     */
    private Long userId;
    /**
     * page number
     */
    private String pageNo;
    /**
     * page window i.e per page
     */
    private String pageWin;
    /**
     * curated or uncurated status record
     */
    private CurationStatus status;
    /**
     * comma separated values of languages.
     */
    private List<String> languages;

    /**
     * list of SearchResult
     */
    private List<SearchResult> listOfResult;

    /**
     * size of result
     */
    private Long resultSize;
    /**
     * search term to filter results based on title
     */
    private String searchTerm;
    /**
     * List of subResource identifier or code
     */
    private List<String> subResourceIdentifiers;
    /**
     * List of all subResource identifier assigned to user.
     */
    private List<String> allSubResourceIdentifiers;
    
    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getPageWin() {
        return pageWin;
    }

    public void setPageWin(String pageWin) {
        this.pageWin = pageWin;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public CurationStatus getStatus() {
        return status;
    }

    public void setStatus(CurationStatus status) {
        this.status = status;
    }

    public List<SearchResult> getListOfResult() {
        return listOfResult;
    }

    public void setListOfResult(List<SearchResult> listOfResult) {
        this.listOfResult = listOfResult;
    }

    public Long getResultSize() {
        return resultSize;
    }

    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public List<String> getSubResourceIdentifiers() {
        return subResourceIdentifiers;
    }

    public void setSubResourceIdentifiers(List<String> subResourceIdentifiers) {
        this.subResourceIdentifiers = subResourceIdentifiers;
    }

    public List<String> getAllSubResourceIdentifiers() {
        return allSubResourceIdentifiers;
    }

    public void setAllSubResourceIdentifiers(List<String> allSubResourceIdentifiers) {
        this.allSubResourceIdentifiers = allSubResourceIdentifiers;
    }

    @Override
    public String toString() {
        return "CurationForm{" + "resourceCode=" + resourceCode + ", userId=" + userId + ", pageNo=" + pageNo + ", pageWin=" + pageWin + ", status=" + status + ", languages=" + languages + ", listOfResult=" + listOfResult + ", resultSize=" + resultSize + ", searchTerm=" + searchTerm + ", subResourceIdentifiers=" + subResourceIdentifiers + ", allSubResourceIdentifiers=" + allSubResourceIdentifiers + '}';
    }
}
