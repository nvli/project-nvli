package in.gov.nvli.rabbitmq;

import in.gov.nvli.dto.ActivityLog;
import in.gov.nvli.util.Constants;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class RabbitmqProducer {

    private static final Logger LOG = Logger.getLogger(RabbitmqProducer.class);

    /**
     * This method send email details to the queue.
     *
     * @param amqpTemplate
     * @param detailsJson
     * @param queueKey
     */
    public void sendEmailDetailsToQueue(AmqpTemplate amqpTemplate, JSONObject detailsJson, String queueKey) {
        try {
            Optional.ofNullable(queueKey).ifPresent((String key) -> {
                if (!key.isEmpty()) {
                    System.out.println("[x] email sending to the queue...");
                    amqpTemplate.convertAndSend(Constants.RabbitMq.EMAIL_EXCHANGE, key, detailsJson.toJSONString().getBytes());
                    System.out.println("[x] email sent to the queue.");
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * @author:Ruturaj This method add activities to queue. Further they will be
     * send to recommendation system for processing.
     *
     * @param amqpTemplate
     * @param activityLog
     */
    public void SendActivityLogToQueue(AmqpTemplate amqpTemplate, ActivityLog activityLog) {
        amqpTemplate.convertAndSend(Constants.RabbitMq.ACTIVITYLOG_EXCHANGE,
                Constants.RabbitMq.ACTIVITYLOG_RECOMMENDATION_KEY, activityLog);
    }
}
