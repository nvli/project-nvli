package in.gov.nvli.social;

import in.gov.nvli.custom.user.RequestAttributes;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserConnection;
import in.gov.nvli.service.UserActivityService;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * helps in provider sign
 *
 * @author Ruturaj Powar<ruturajp@cdac.in>
 */
@Controller

@Service
public final class SpringSecuritySignInAdapter implements SignInAdapter {

    @Autowired
    UserActivityService activityServiceObj;

    @Autowired
    private UserService userService;

    @Autowired
    UserConnectionDAO userConnectionDAO;

    @Autowired
    private ConnectionRepository connectionRepository;
    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;
    @Autowired
    private UsersConnectionRepository usersConnectionRepository;

    private HttpSessionRequestCache httpSessionRequestCache;

    /**
     * Constructor for SpringSecuritySignInAdapter
     */
    public SpringSecuritySignInAdapter() {
    }

    SpringSecuritySignInAdapter(HttpSessionRequestCache httpSessionRequestCache, ConnectionRepository connectionRepository) {
        this.httpSessionRequestCache = httpSessionRequestCache;
        this.connectionRepository = connectionRepository;
    }

    /**
     * User will get redirected to this method after social sign in if user
     * connection is already made before. If user connection is present but user
     * is not registered then user will be redirected to registration page.
     *
     * @param email unique id for user.
     * @param connection connection for provider by which user has logged in
     * @param request
     * @return String
     */
    @Override
    @RequestMapping(value = "/signinProvider")
    public String signIn(String email, Connection<?> connection, NativeWebRequest request) {
        User user = userService.getUserByEmailID(email);
        if (null != user) {
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                    accountNonLocked, getAuthorities(user.getRoles()));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    getAuthorities(user.getRoles()));
            providerSignInLogging(email, connection);
//            auth.setDetails(userDetails)
            SecurityContextHolder.getContext().setAuthentication(auth);

        } else {
            return "/auth/registration";

        }
        return "/security/handler?isSocialSignIn=yes";

    }

    /**
     * Method to save log of social logging.
     *
     * @param email
     * @param connection
     */
    public void providerSignInLogging(String email, Connection<?> connection) {
        User user = userService.getUserByEmailID(email);
        List<UserConnection> userConnections = userConnectionDAO.getUserConnection(email);
        String providerId = null;
        for (UserConnection userConnection : userConnections) {
            if (userConnection.getImageUrl().equalsIgnoreCase(connection.getImageUrl())) {
                providerId = userConnection.getProviderId();
            }
        }
        try {
            switch (providerId) {
                case "facebook":
                    activityServiceObj.saveUserActivityLog(Constants.UserActivities.FACEBOOK_LOGIN, null, user, null, new RequestAttributes());
                    break;
                case "google":
                    activityServiceObj.saveUserActivityLog(Constants.UserActivities.GOOGLE_LOGIN, null, user, null, new RequestAttributes());
                    break;
                case "linkedin":
                    activityServiceObj.saveUserActivityLog(Constants.UserActivities.LINKEDIN_LOGIN, null, user, null, new RequestAttributes());
                    break;
                case "github":
                    activityServiceObj.saveUserActivityLog(Constants.UserActivities.GITHUB_LOGIN, null, user, null, new RequestAttributes());
                    break;
            }
            userConnectionDAO.fetchProfileImage(providerId, user, connectionRepository, connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(roles));
        return authList;
    }

    private List<String> getRoles(Set<Role> roles) {
        List<String> roleCodes = new ArrayList<>();
        for (Role role : roles) {
            roleCodes.add(role.getCode());
        }
        return roleCodes;
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    public void setHttpSessionRequestCache(HttpSessionRequestCache httpSessionRequestCache) {
        this.httpSessionRequestCache = httpSessionRequestCache;
    }

    public void setConnectionRepository(ConnectionRepository connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

}
