/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.social;

import in.gov.nvli.custom.user.RequestAttributes;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.CustomUserDetailService;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.github.api.GitHub;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.social.google.api.Google;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.twitter.api.Twitter;

/**
 * Redirect User as per the signed up status from social platform
 *
 * @author prabhat
 * @author Ruturaj Powar<ruturajp@cdac.in>
 */
@Controller
@RequestMapping(value = "/connect/redirect")
public class SocialRedirectController {

    @Autowired
    @Qualifier("authManager")
    AuthenticationManager authManager;

    @Autowired
    CustomUserDetailService userDetailService;

    @Autowired
    UserService userService;

    @Autowired
    ConnectionRepository connectionRepository;

    @Autowired
    UserActivityService activityServiceObj;

    @Autowired
    UserConnectionDAO userConnectionDAO;

    /**
     * redirect the user to the profile page if already signed in and present in
     * the database else redirect it to the registration page to complete the
     * profile.
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/socialSettings")
    public ModelAndView loadSocialSettings(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        return new ModelAndView(new RedirectView("/user/social/settings", true, true, true));
    }

    /**
     * Method to handle user connection with facebook
     *
     * @param request
     * @param response
     * @return ModelAndView
     */
    @RequestMapping(value = "/facebookProfile")
    public ModelAndView profile(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        Connection<Facebook> connection = connectionRepository.findPrimaryConnection(Facebook.class);
        Facebook facebook = (Facebook) connection.getApi();
        String[] fields = {"email"};
        User userProfile = facebook.fetchObject("me", User.class, fields);
        String email = userProfile.getEmail();
        String facebookId = userProfile.getId().toString();
        // Check if User Exists by the email returned by facebook
        User user = userService.getUserByEmailID(email);
        long userId = user.getId();
        if (null != user) { // If User exists
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                    accountNonLocked, getAuthorities(user.getRoles()));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    getAuthorities(user.getRoles()));
            activityServiceObj.saveUserActivityLog(Constants.UserActivities.FACEBOOK_LOGIN, null, user, null, new RequestAttributes(request));
            SecurityContextHolder.getContext().setAuthentication(auth);

        } else {
            // User Does not exits, Proceed to Registration
            ModelAndView modelAndView = new ModelAndView(new RedirectView("/auth/registration", true, true, true));
            modelAndView.addObject("message", "Account does not exists. Please create one.");
            return modelAndView;
        }
        userConnectionDAO.updateConnection(userId, facebookId);
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/user/profile", true, true, true));
        return modelAndView;
    }

    /**
     * Method to handle connection with twitter
     *
     * @param request
     * @param response
     * @return ModelAndView
     */
    @RequestMapping(value = "/twitterProfile")
    public ModelAndView twitterProfile(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        Connection<Twitter> connection = connectionRepository.findPrimaryConnection(Twitter.class);
        String email = connection.getApi().userOperations().getUserProfile().getName();
        String facebookId = connection.getApi().userOperations().getUserProfile().getProfileUrl();
        // Check if User Exists by the email returned by facebook
        User user = userService.getUserByEmailID(email);
        if (null != user) { // If User exists
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                    accountNonLocked, getAuthorities(user.getRoles()));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    getAuthorities(user.getRoles()));

//            auth.setDetails(userDetails);
            activityServiceObj.saveUserActivityLog(Constants.UserActivities.FACEBOOK_LOGIN, null, user, null, new RequestAttributes(request));
            SecurityContextHolder.getContext().setAuthentication(auth);

        } else {
            // User Does not exits, Proceed to Registration
            ModelAndView modelAndView = new ModelAndView(new RedirectView("/auth/registration", true, true, true));
            modelAndView.addObject("message", "Account does not exists. Please create one.");
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(new RedirectView("/user/profile", true, true, true));
        return modelAndView;
    }

    /**
     * Method to handle connection with google
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/googleProfile")
    public ModelAndView googleProfile(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        Connection<Google> connection = connectionRepository.findPrimaryConnection(Google.class);
        String email = connection.getApi().userOperations().getUserInfo().getEmail();
        connection.getApi().userOperations().getUserInfo().getProfilePictureUrl();
        String googleId = connection.getApi().userOperations().getUserInfo().getId();
        // Check if User Exists by the email returned by facebook
        User user = userService.getUserByEmailID(email);
        if (null != user) { // If User exists
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                    accountNonLocked, getAuthorities(user.getRoles()));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    getAuthorities(user.getRoles()));

//            auth.setDetails(userDetails);
            activityServiceObj.saveUserActivityLog(Constants.UserActivities.FACEBOOK_LOGIN, null, user, null, new RequestAttributes(request));
            SecurityContextHolder.getContext().setAuthentication(auth);

        } else {
            // User Does not exits, Proceed to Registration
            ModelAndView modelAndView = new ModelAndView(new RedirectView("/auth/registration", true, true, true));
            modelAndView.addObject("message", "Account does not exists. Please create one.");
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(new RedirectView("/user/profile", true, true, true));
        return modelAndView;
    }

    /**
     * Method to handle connection with linkedin
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/linkedinProfile")
    public ModelAndView linkedinProfile(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        Connection<LinkedIn> connection = connectionRepository.findPrimaryConnection(LinkedIn.class);
        String email = connection.getApi().profileOperations().getUserProfile().getEmailAddress();
        String linkedinId = connection.getApi().profileOperations().getUserProfile().getId();
        // Check if User Exists by the email returned by facebook
        User user = userService.getUserByEmailID(email);
        if (null != user) { // If User exist
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                    accountNonLocked, getAuthorities(user.getRoles()));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    getAuthorities(user.getRoles()));
            activityServiceObj.saveUserActivityLog(Constants.UserActivities.FACEBOOK_LOGIN, null, user, null, new RequestAttributes(request));
            SecurityContextHolder.getContext().setAuthentication(auth);

        } else {
            // User Does not exits, Proceed to Registration
            ModelAndView modelAndView = new ModelAndView(new RedirectView("/auth/registration", true, true, true));
            modelAndView.addObject("message", "Account does not exists. Please create one.");
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(new RedirectView("/user/profile", true, true, true));
        return modelAndView;
    }

    /**
     * Method to handle connection with github.
     *
     * @param request
     * @param response
     * @return ModelAndView
     */
    @RequestMapping(value = "/githubProfile")
    public ModelAndView githubProfile(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        Connection<GitHub> connection = connectionRepository.findPrimaryConnection(GitHub.class);
        System.out.println(connection.fetchUserProfile().getEmail());
        System.out.println(connection.hasExpired());
        if (connection.hasExpired()) {
            connection.refresh();
            connection.sync();
        }
        String email = connection.getApi().userOperations().getUserProfile().getEmail();
        long githubId = connection.getApi().userOperations().getUserProfile().getId();
        // Check if User Exists by the email returned by facebook
        User user = userService.getUserByEmailID(email);
        if (null != user) { // If User exists
//            try {
            boolean enabled = true;
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
                    user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                    accountNonLocked, getAuthorities(user.getRoles()));
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    getAuthorities(user.getRoles()));

//            auth.setDetails(userDetails);
            activityServiceObj.saveUserActivityLog(Constants.UserActivities.FACEBOOK_LOGIN, null, user, null, new RequestAttributes(request));
            SecurityContextHolder.getContext().setAuthentication(auth);
        } else {
            // User Does not exits, Proceed to Registration
            ModelAndView modelAndView = new ModelAndView(new RedirectView("/auth/registration", true, true, true));
            modelAndView.addObject("message", "Account does not exists. Please create one.");
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(new RedirectView("/user/profile", true, true, true));
        return modelAndView;
    }

    /**
     * Redirects the user to the error page after failed login from social
     *
     * @return
     */
    @RequestMapping(value = "/error")
    public ModelAndView socialError() {
        ModelAndView modelAndView = new ModelAndView(new RedirectView("/user/profile", true, true, true));
        return modelAndView;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(roles));
        return authList;
    }

    private List<String> getRoles(Set<Role> roles) {
        List<String> roleCodes = new ArrayList<>();
        for (Role role : roles) {
            roleCodes.add(role.getCode());
        }
        return roleCodes;
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}
