package in.gov.nvli.social;

/**
 * configure the connection factories to add the connection factories for
 * different sign in providers and create connectController to initiate the
 * oAuth process
 *
 * @author Ruturaj Powar<ruturajp@cdac.in>
 */
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.github.connect.GitHubConnectionFactory;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.linkedin.connect.LinkedInConnectionFactory;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

@Configuration
@EnableSocial
public class SocialConfig implements SocialConfigurer {

    //App Id for facebook
    @Value("${social.facebook.appid}")
    String facebookAppId;

    //App Secret for facebook
    @Value("${social.facebook.appsecret}")
    String facebookAppSecret;

    //App Id for twitter
    @Value("${social.twitter.appid}")
    String twitterAppId;

    //App Secret for twitter
    @Value("${social.twitter.appsecret}")
    String twitterAppSecret;

    //App Id for linkedin
    @Value("${social.linkedin.appid}")
    String linkedinAppId;

    //App Secret for linkedin
    @Value("${social.linkedin.appsecret}")
    String linkedinAppSecret;

    @Value("${application.url}")
    String applicationURL;

    //App Id for google+
    @Value("${social.google.appid}")
    String googleAppId;

    //App Secret for google+
    @Value("${social.google.appsecret}")
    String googleAppSecret;

    //App Id for github
    @Value("${social.github.appid}")
    String githubAppId;

    //App Secret for github
    @Value("${social.github.appsecret}")
    String githubAppSecret;

    @Autowired
    DataSource portalJNDI;

    /**
     *
     * @param connectionFactoryConfigurer used to register connection factories.
     * @param environment represents the environment in which our example
     * application is running.
     */
    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer, Environment environment) {
        connectionFactoryConfigurer.addConnectionFactory(new FacebookConnectionFactory(facebookAppId, facebookAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new TwitterConnectionFactory(twitterAppId, twitterAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new LinkedInConnectionFactory(linkedinAppId, linkedinAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new GoogleConnectionFactory(googleAppId, googleAppSecret));
        connectionFactoryConfigurer.addConnectionFactory(new GitHubConnectionFactory(githubAppId, githubAppSecret));
    }

    /**
     * Bean for registering connection factories
     *
     * @return ConnectionFactoryLocator
     */
    @Bean
    @Scope(value = "singleton", proxyMode = ScopedProxyMode.INTERFACES)
    public ConnectionFactoryLocator connectionFactoryLocator() {
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();

        registry.addConnectionFactory(new FacebookConnectionFactory(facebookAppId, facebookAppSecret));
        registry.addConnectionFactory(new LinkedInConnectionFactory(linkedinAppId, linkedinAppSecret));
        registry.addConnectionFactory(new GoogleConnectionFactory(googleAppId, googleAppSecret));
        registry.addConnectionFactory(new TwitterConnectionFactory(twitterAppId, twitterAppSecret));
        registry.addConnectionFactory(new GitHubConnectionFactory(githubAppId, githubAppSecret));
        return registry;
    }

    /**
     * Method to establish connection with database.
     *
     * @param connectionFactoryLocator
     * @return
     */
    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        return new JdbcUsersConnectionRepository(portalJNDI, connectionFactoryLocator, Encryptors.noOpText());
    }

    /**
     * Method to return email of user as unique id.
     *
     * @return UserIdSource responsible of determining the correct account id of
     * the user.
     */
    @Override
    public UserIdSource getUserIdSource() {
        return new UserIdSource() {
            @Override
            public String getUserId() {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication == null) {
                    throw new IllegalStateException("Unable to get a ConnectionRepository: no user signed in");
                }
                return authentication.getName();
            }
        };
    }

    /**
     * Instantiate the connect controller with connectionfactoryLocator and
     * connectionRepository to initiate the connection so that oAuth dance can
     * be performed
     *
     * @param connectionFactoryLocator
     * @param connectionRepository
     * @return
     */
    @Bean
    public ConnectController connectController(
            ConnectionFactoryLocator connectionFactoryLocator,
            ConnectionRepository connectionRepository) {
        ConnectController connectController = new ConnectController(connectionFactoryLocator, connectionRepository);
        return connectController;
    }

    /**
     * If user connection is not present then redirect user to registration
     * method and if any error occurs while sign in through social redirect user
     * again to login page. If user is already present then then redirect user
     * to SignIN method of SpringSecuritySignINAdapter.
     *
     * @param connectionFactoryLocator
     * @param usersConnectionRepository
     * @param springSecuritySignInAdapter
     * @param connectionRepository
     * @return ProviderSignInController controller to handle redirecting user to
     * proper method according to conditions mentioned above.
     */
    @Bean
    public ProviderSignInController providerSignInController(
            ConnectionFactoryLocator connectionFactoryLocator,
            UsersConnectionRepository usersConnectionRepository,
            SpringSecuritySignInAdapter springSecuritySignInAdapter,
            ConnectionRepository connectionRepository) {
        springSecuritySignInAdapter.setHttpSessionRequestCache(new HttpSessionRequestCache());
        springSecuritySignInAdapter.setConnectionRepository(connectionRepository);
        ProviderSignInController controller = new ProviderSignInController(
                connectionFactoryLocator,
                usersConnectionRepository,
                springSecuritySignInAdapter);
        controller.setApplicationUrl(applicationURL);
        controller.setSignUpUrl("/auth/signup");
        controller.setSignInUrl("/auth/login");
        return controller;
    }

}
