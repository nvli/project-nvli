/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.social;

import in.gov.nvli.beans.UserDetailsBean;
import in.gov.nvli.dao.generic.GenericDAOImpl;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserConnection;
import in.gov.nvli.service.UserService;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.github.api.GitHub;
import org.springframework.social.google.api.Google;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Author : Gulafsha <gulafsha@cdac.in>
 * Author : Ruturaj Powar <ruturajp@cdac.in>
 */
@Repository
public class UserConnectionDAOImpl extends GenericDAOImpl<UserConnection, String> implements UserConnectionDAO {

    private static final Logger LOG = Logger.getLogger(UserConnectionDAOImpl.class);

    @Autowired
    UserService userService;

    @Value("${user.parent.directory}")
    private String userRelatedDirecory;
    @Value("${profile.pic.directory.name}")
    private String profilePicDirectory;

    /**
     * Method to set session factory
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {

        super.setSessionFactory(sessionFactory); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Method to update the connection
     *
     * @param userid
     * @param providerUserid
     */
    @Override
    @Transactional
    public void updateConnection(long userid, String providerUserid) {

        Criteria criteria = currentSession().createCriteria(UserConnection.class);
        criteria.add(Restrictions.eq("providerUserId", providerUserid));
        UserConnection userConnection = (UserConnection) criteria.uniqueResult();
        if (null != userConnection) {
            String id = String.valueOf(userid);
            userConnection.setUserId(id);
            saveOrUpdate(userConnection);
        }
    }

    /**
     * Method to get all social connection of user.
     *
     * @param email
     * @return
     */
    @Override
    @Transactional
    public List<UserConnection> getUserConnection(String email) {
        Criteria criteria = currentSession().createCriteria(UserConnection.class);
        criteria.add(Restrictions.eq("userId", email));
        List<UserConnection> userConnectionsList = (List<UserConnection>) criteria.list();
        return userConnectionsList;
    }

    /**
     * Method to fetch profile pic according to social provider.
     *
     * @param providerId
     * @param user
     * @param connectionRepository
     * @param connection
     * @throws MalformedURLException
     */
    @Override
    @Transactional
    public void fetchProfileImage(String providerId, User user, ConnectionRepository connectionRepository, Connection<?> connection) throws MalformedURLException {
        UserDetailsBean userDetObj = new UserDetailsBean();
        userDetObj.setId(user.getId());
        userDetObj.setEnabled(user.getEnabled());
        userDetObj.setUsername(user.getUsername());
        userDetObj.setEmail(user.getEmail());
        userDetObj.setFirstName(user.getFirstName());
        userDetObj.setMiddleName(user.getMiddleName());
        userDetObj.setLastName(user.getLastName());
        userDetObj.setContact(user.getContact());
        userDetObj.setCity(user.getCity());
        userDetObj.setDistrict(user.getDistrict());
        userDetObj.setState(user.getState());
        userDetObj.setCountry(user.getCountry());
        userDetObj.setGender(user.getGender());
        userDetObj.setProfilePicPath(user.getProfilePicPath());
        
        if (user.getProfilePicPath() == null || user.getProfilePicPath().isEmpty()) {
            String relativeUserProfilePicPath = "";
            String imageExtension = "";
            DataBufferByte data = null;
            BufferedImage image = null;
            URL url = new URL(connection.getProfileUrl());

            switch (providerId) {
                case "facebook":
                    Facebook facebook = (Facebook) connection.getApi();
                    String[] fields = {"id", "email"};
                    User userProfile = facebook.fetchObject("me", User.class, fields);
                    url = new URL(getFinalLocation("http://graph.facebook.com/v2.5/" + userProfile.getId() + "/picture?type=large&width=300&height=300"));
                    break;
                case "linkedin":
                    LinkedIn linkedin = (LinkedIn) connection.getApi();
                    url = new URL(linkedin.profileOperations().getUserProfileFull().getProfilePictureUrl() + "?type=large&width=300&height=300");
                    break;
                case "google":
                    Google google = (Google) connection.getApi();
                    url = new URL(google.userOperations().getUserInfo().getProfilePictureUrl());
                    break;

                case "github":
                    GitHub github = (GitHub) connection.getApi();
                    url = new URL("https://avatars2.githubusercontent.com/u/" + github.userOperations().getUserProfile().getId() + "?v=3&s=460");
                    break;
            }
            try {
                image = ImageIO.read(url);
                WritableRaster raster = image.getRaster();
                data = (DataBufferByte) raster.getDataBuffer();
                userDetObj.setProfileImageByte(data.getData());
                if (userDetObj.getProfileImageByte().length > 0) {
                    imageExtension = "jpg";
                    relativeUserProfilePicPath = user.getId() + File.separator + profilePicDirectory + File.separator + user.getId() + "." + imageExtension;
                    LOG.info("image uploaded" + relativeUserProfilePicPath);
                    userService.saveProfileImageGotFromSocialOnDrive(userRelatedDirecory + File.separator + relativeUserProfilePicPath, image);
                    userDetObj.setProfilePicPath(relativeUserProfilePicPath);
                } else {
                    LOG.info("image not uploaded");
                }
                userDetObj.setId(user.getId());
                userService.updateUserProfile(userDetObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Method to get url for facebook profile picture.
     *
     * @param address request url
     * @return String final url of profile pic.
     */
    public static String getFinalLocation(String address) {
        try {
            URL url = new URL(address);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            int status = conn.getResponseCode();
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                        || status == HttpURLConnection.HTTP_MOVED_PERM
                        || status == HttpURLConnection.HTTP_SEE_OTHER) {
                    String newLocation = conn.getHeaderField("Location");
                    return getFinalLocation(newLocation);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }
}
