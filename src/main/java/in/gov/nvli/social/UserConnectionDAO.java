/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.social;

import in.gov.nvli.domain.user.UserConnection;
import in.gov.nvli.dao.generic.IGenericDAO;
import in.gov.nvli.domain.user.User;
import java.net.MalformedURLException;
import java.util.List;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.web.context.request.WebRequest;

/**
 * Author : Gulafsha <gulafsha@cdac.in>
 * Author : Ruturaj Powar <ruturajp@cdac.in>
 */
public interface UserConnectionDAO extends IGenericDAO<UserConnection, String> {

    public void updateConnection(long userid, String providerUserid);

    public List<UserConnection> getUserConnection(String email);

    public void fetchProfileImage(String providerId, User user, ConnectionRepository connectionRepository, Connection<?> connection) throws MalformedURLException;
}
