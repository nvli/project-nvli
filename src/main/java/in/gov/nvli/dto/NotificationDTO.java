package in.gov.nvli.dto;

/**
 * class use to represents notification related information
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class NotificationDTO {

    /**
     * variable use to hold notification id
     */
    private long notificationId;
    /**
     * variable use to hold notification messege
     */

    private String notificationMessage;
    /**
     * variable use to hold notification date and time
     */

    private String notifactionDateTime;
    /**
     * variable use to hold sender name of notification
     */

    private String senderName;
    /**
     * variable use to hold sender profile pic path of notification
     */

    private String senderProfilePicPath;
    /**
     * variable use to hold notification target URL
     */

    private String notificationTargetURL;
    /**
     * variable use to hold check whether notification is read by receiver of
     * not
     */

    private boolean isReadByReceiver;
    /**
     * variable use to hold notification read time by receiver
     */

    private String notificationReadTime;

    /**
     * Gets the notification message
     *
     * @return {@link String} notification message
     */
    public String getNotificationMessage() {
        return notificationMessage;
    }

    /**
     * This is a setter which sets the notification message
     *
     * @param notificationMessage contains
     */
    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    /**
     * Gets the notification date and time
     *
     * @return {@link String} notification date and time
     */
    public String getNotifactionDateTime() {
        return notifactionDateTime;
    }

    /**
     * This is a setter which sets the notification date and time
     *
     * @param notifactionDateTime contains notification date and time
     */
    public void setNotifactionDateTime(String notifactionDateTime) {
        this.notifactionDateTime = notifactionDateTime;
    }

    /**
     * Gets the notification sender name
     *
     * @return {@link String} sender name
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * This is a setter which sets the notification sender name
     *
     * @param senderName contains sender name of notification
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    /**
     * Gets the notification target URL
     *
     * @return {@link String} notification Target URL
     */
    public String getNotificationTargetURL() {
        return notificationTargetURL;
    }

    /**
     * This is a setter which sets the notification target URL
     *
     * @param notificationTargetURL contains notification target URL
     */
    public void setNotificationTargetURL(String notificationTargetURL) {
        this.notificationTargetURL = notificationTargetURL;
    }

    /**
     * check whether notification is read by receiver of not
     *
     * @return {@link Boolean} whether read by receiver of not
     */
    public boolean isIsReadByReceiver() {
        return isReadByReceiver;
    }

    /**
     * This is a setter which sets whether notification is read by receiver or
     * not
     *
     * @param isReadByReceiver
     */
    public void setIsReadByReceiver(boolean isReadByReceiver) {
        this.isReadByReceiver = isReadByReceiver;
    }

    /**
     * Gets the notification read time by receiver
     *
     * @return {@link String} notification read time
     */
    public String getNotificationReadTime() {
        return notificationReadTime;
    }

    /**
     * This is a setter which sets the notification read time by receiver
     *
     * @param notificationReadTime contains notification read time by receiver
     */
    public void setNotificationReadTime(String notificationReadTime) {
        this.notificationReadTime = notificationReadTime;
    }

    /**
     * Gets the notification id
     *
     * @return {@link Long} notification id
     */
    public long getNotificationId() {
        return notificationId;
    }

    /**
     * This is a setter which sets the notification id
     *
     * @param notificationId contains notification id of notification
     */
    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }

    /**
     * Gets the notification sender profile pic path
     *
     * @return {@link String} sender profile pic path
     */
    public String getSenderProfilePicPath() {
        return senderProfilePicPath;
    }

    /**
     * This is a setter which sets the sender profile pic path of notification
     *
     * @param senderProfilePicPath contains notification sender profile pic path
     */
    public void setSenderProfilePicPath(String senderProfilePicPath) {
        this.senderProfilePicPath = senderProfilePicPath;
    }
}
