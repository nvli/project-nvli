package in.gov.nvli.dto;

/**
 * class uses when user try to connect any social network like facebook,linkedin,github,google+ etc
 * @author Gulafsha Khan<gulafsha@cdac.in>
 */
public class socialSettingDTO {

    /**
     * variable use to hold provider is means whether provider is facebook,
     * google+, github, linkedin etc
     */
    private String providerId;
    /**
     * variable use to hold provider user id
     */
    private String providerUserId;
    /**
     * variable use to hold connected status whether user is connected to any
     * social site or not
     */
    private boolean connectedStatus;

    /**
     * constructor use to initialized
     *
     * @see socialSettingDTO
     * @param providerId provider id
     */
    public socialSettingDTO(String providerId) {
        this.providerId = providerId;
    }

    /**
     * constructor use to initialized
     *
     * @see socialSettingDTO without any parameter
     */
    public socialSettingDTO() {
        this.providerId = null;
        this.providerUserId = null;
        this.connectedStatus = false;
    }

    /**
     * constructor use to initialized
     *
     * @see socialSettingDTO
     * @param providerId
     * @param providerUserId
     * @param connectedStatus
     */
    public socialSettingDTO(String providerId, String providerUserId, boolean connectedStatus) {
        this.providerId = providerId;
        this.providerUserId = providerUserId;
        this.connectedStatus = connectedStatus;
    }

    /**
     * Gets the provider id
     * @return {@link String} provider id
     */
    public String getProviderId() {
        return providerId;
    }

    /**
     * Gets the provider user id
     * @return {@link String} provider user id
     */
    public String getProviderUserId() {
        return providerUserId;
    }

    /**
     * Gets the connected status
     * @return {@link Boolean} connected status whether connected to any social network or not
     */
    public boolean getConnectedStatus() {
        return connectedStatus;
    }

    /**
     * This is a setter which sets provider id 
     * @param providerId
     */
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
     * This is a setter which sets provider user id 
     * @param providerUserId
     */
    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }

    /**
     * This is a setter which sets connected status
     * @param connectedStatus
     */
    public void setConnectedStatus(boolean connectedStatus) {
        this.connectedStatus = connectedStatus;
    }

}
