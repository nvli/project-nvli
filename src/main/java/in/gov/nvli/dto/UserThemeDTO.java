package in.gov.nvli.dto;

import in.gov.nvli.domain.user.UserTheme;
import java.io.Serializable;
import java.util.Date;

/**
 * class use to represents user theme related information
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public class UserThemeDTO implements Serializable {

    /**
     * variable use to hold theme id
     */
    private long id;
    /**
     * variable use to hold theme code
     */
    private String themeCode;
    /**
     * variable use to hold theme name
     */
    private String themeName;
    /**
     * variable use to hold theme description
     */
    private String themeDescription;
    /**
     * variable use to identify whether the theme is customized is not
     */
    private boolean custom;
    /**
     * variable use to hold header image location
     */
    private String headerImageLocation;
    /**
     * variable use to hold background image location
     */
    private String bgImageLocation;
    /**
     * variable use to hold theme type
     */
    private String themeType;
    /**
     * variable use to hold theme activation date
     */
    private Date themeActivationDate;
    /**
     * variable use to hold theme directory
     */
    private String themeDirectory;

    private boolean themeRepeatable;

    /**
     *
     */
    public UserThemeDTO() {
    }

    /**
     * constructor use to initialized
     *
     * @see UserTheme
     * @param userTheme object to hold information of user theme that contains
     * theme id,theme code,theme name,theme description,theme header image
     * location,theme background image location,theme directory,custom
     */
    public UserThemeDTO(UserTheme userTheme) {
        this.id = userTheme.getId();
        this.themeCode = userTheme.getThemeCode();
        this.themeName = userTheme.getThemeName();
        this.themeDescription = userTheme.getThemeDescription();
        this.custom = userTheme.isCustom();
        this.headerImageLocation = userTheme.getHeaderImageLocation();
        this.bgImageLocation = userTheme.getBgImageLocation();
        this.themeDirectory = userTheme.getThemeDirectory();
        this.themeType = userTheme.getThemeType();
        this.themeActivationDate = userTheme.getThemeActivationDate();
    }

    /**
     * Gets the theme directory
     *
     * @return {@link String} theme directory
     */
    public String getThemeDirectory() {
        return themeDirectory;
    }

    /**
     * This is a setter which sets the theme directory
     *
     * @param themeDirectory
     */
    public void setThemeDirectory(String themeDirectory) {
        this.themeDirectory = themeDirectory;
    }

    /**
     * Gets the theme id
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     * This is a setter which sets the theme id
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets the theme code
     *
     * @return {@link String} theme code
     */
    public String getThemeCode() {
        return themeCode;
    }

    /**
     * This is a setter which sets the theme code
     *
     * @param themeCode
     */
    public void setThemeCode(String themeCode) {
        this.themeCode = themeCode;
    }

    /**
     * Gets the theme name
     *
     * @return {@link String} theme name
     */
    public String getThemeName() {
        return themeName;
    }

    /**
     * This is a setter which sets theme name
     *
     * @param themeName
     */
    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    /**
     * Gets the theme description
     *
     * @return {@link String} theme description
     */
    public String getThemeDescription() {
        return themeDescription;
    }

    /**
     * This is a setter which sets the theme description
     *
     * @param themeDescription
     */
    public void setThemeDescription(String themeDescription) {
        this.themeDescription = themeDescription;
    }

    /**
     * Gets the custom status
     *
     * @return {@link Boolean} whether theme is custom or not
     */
    public boolean isCustom() {
        return custom;
    }

    /**
     * This is a setter which sets the whether theme is custom or not
     *
     * @param custom
     */
    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    /**
     * Gets the header image location
     *
     * @return {@link String} header image location
     */
    public String getHeaderImageLocation() {
        return headerImageLocation;
    }

    /**
     * This is a setter which sets the header image location
     *
     * @param headerImageLocation
     */
    public void setHeaderImageLocation(String headerImageLocation) {
        this.headerImageLocation = headerImageLocation;
    }

    /**
     * Gets the theme background image location
     *
     * @return {@link String} theme background image location
     */
    public String getBgImageLocation() {
        return bgImageLocation;
    }

    /**
     * This is a setter which sets the theme background image location
     *
     * @param bgImageLocation
     */
    public void setBgImageLocation(String bgImageLocation) {
        this.bgImageLocation = bgImageLocation;
    }

    /**
     * gets the theme type
     *
     * @return {@link String} theme type
     */
    public String getThemeType() {
        return themeType;
    }

    /**
     * This is a setter which sets theme type
     *
     * @param themeType
     */
    public void setThemeType(String themeType) {
        this.themeType = themeType;
    }

    /**
     * gets the theme activation date
     *
     * @return {@link Date} theme activation date
     */
    public Date getThemeActivationDate() {
        return themeActivationDate;
    }

    /**
     * This is a setter which sets theme activation date
     *
     * @param themeActivationDate
     */
    public void setThemeActivationDate(Date themeActivationDate) {
        this.themeActivationDate = themeActivationDate;
    }

    public boolean isThemeRepeatable() {
        return themeRepeatable;
    }

    public void setThemeRepeatable(boolean themeRepeatable) {
        this.themeRepeatable = themeRepeatable;
    }

}
