/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dto;

/**
 * class uses when user try to connect any social network
 *
 * @author Gulafsha Khan<gulafsha@cdac.in>
 */
public class socialDTO {

    /**
     * variable use to hold provider user id that is user of anu social site
     */
    private String providerUserId;
    /**
     * variable use to hold connected status of user whether user is connected
     * to any social site or not
     */
    private boolean connectedStatus;

    /**
     * constructor use to initialized
     *
     * @see socialDTO without any parameter
     */
    public socialDTO() {
        this.providerUserId = null;
        this.connectedStatus = false;
    }

    /**
     * constructor use to initialized 
     * @see socialDTO
     *
     * @param providerUserId provider user id
     * @param connectedStatus connected status
     */
    public socialDTO(String providerUserId, boolean connectedStatus) {
        this.providerUserId = providerUserId;
        this.connectedStatus = connectedStatus;
    }

    /**
     * Gets the provider user id
     * @return {@link String}
     */
    public String getProviderUserId() {
        return providerUserId;
    }

    /**
     * Gets the connected status
     * @return {@link Boolean} status whether connected to any social network or not
     */
    public boolean getConnectedStatus() {
        return connectedStatus;
    }

    /**
     * This is a setter which sets provider user id 
     * @param providerUserId
     */
    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }

    /**
     * This is a setter which sets the user connected status
     * @param connectedStatus
     */
    public void setConnectedStatus(boolean connectedStatus) {
        this.connectedStatus = connectedStatus;
    }

}
