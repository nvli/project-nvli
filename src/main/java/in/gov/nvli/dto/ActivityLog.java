/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dto;

import java.io.Serializable;

/**
 *
 * @author ruturaj<ruturajp@cdac.in>
 */
public class ActivityLog implements Serializable {

    private static final long serialVersionUID = 1L;

    private String recordIdentifier;
    private Long userId;
    private String ActivityName;
    private Double rating;
    private String recordType;

    public ActivityLog() {
    }

   

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public ActivityLog(String recordIdentifier, Long userId, String ActivityName, Double rating, String recordType) {
        this.recordIdentifier = recordIdentifier;
        this.userId = userId;
        this.ActivityName = ActivityName;
        this.rating = rating;
        this.recordType = recordType;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getActivityName() {
        return ActivityName;
    }

    public void setActivityName(String ActivityName) {
        this.ActivityName = ActivityName;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
       
    @Override
    public String toString() {
        return "ActivityLog{" + "recordIdentifier=" + recordIdentifier + ", userId=" + userId + ", ActivityName=" + ActivityName + ", rating=" + rating + '}';
    }

}
