/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dto;

/**
 *  DTO used for #PolicyType
 * @author Gulafsha
 */
public class PolicyTypeDTO {

    private Long typeId;

    private String PolicyName;

    /**
     *
     * @return
     */
    public Long getTypeId() {
        return typeId;
    }

    /**
     *
     * @return
     */
    public String getPolicyName() {
        return PolicyName;
    }

    /**
     *
     * @param typeId
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    /**
     *
     * @param PolicyName
     */
    public void setPolicyName(String PolicyName) {
        this.PolicyName = PolicyName;
    }

    public PolicyTypeDTO(Long typeId, String PolicyName) {
        this.typeId = typeId;
        this.PolicyName = PolicyName;
    }

    public PolicyTypeDTO() {
    }
    

}
