package in.gov.nvli.dto;

import java.io.Serializable;

/**
 * class use to represents user profile related information
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class UserProfileDTO implements Serializable {

    /**
     * variable use to hold country
     */
    private String country;
    /**
     * variable use to hold state
     */
    private String state;
    /**
     * variable use to hold city
     */
    private String city;
    /**
     * variable use to hold pin code
     */
    private String pin;
    /**
     * variable use to hold mobile No
     */
    private String mobileNo;

    /**
     * Gets the country
     *
     * @return {@link String} country
     */
    public String getCountry() {
        return country;
    }

    /**
     * This is a setter which sets the country
     *
     * @param country contains country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets the state
     *
     * @return {@link String} state
     */
    public String getState() {
        return state;
    }

    /**
     * This is a setter which sets the state
     *
     * @param state contains state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets the city name
     *
     * @return {@link String} city
     */
    public String getCity() {
        return city;
    }

    /**
     * This is a setter which sets the city
     *
     * @param city contains city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets the pin code of user
     *
     * @return {@link String} pin code
     */
    public String getPin() {
        return pin;
    }

    /**
     * This is a setter which sets the pin code
     *
     * @param pin contains pin code
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * Gets the mobile no
     *
     * @return {@link String} mobile no
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * This is a setter which sets the mobile no
     *
     * @param mobileNo contains mobile no
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

}
