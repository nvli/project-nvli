/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dto;

/**
 *
 * @author Gulafsha
 */
public class SecretKeyDTO {

    private Long secretKeyId;

    private String secretKey;

    private String name;

    public Long getSecretKeyId() {
        return secretKeyId;
    }

    public void setSecretKeyId(Long secretKeyId) {
        this.secretKeyId = secretKeyId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
