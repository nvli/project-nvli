package in.gov.nvli.dto;

/**
 * class use to represents user assigned role related information
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class RoleDTO {

    /**
     * variable use to hold role id
     */
    private Long roleId;
    /**
     * variable use to hold role name
     */
    private String roleName;
    /**
     * variable use to hold role description
     */
    private String roleDescription;
    /**
     * variable use to hold role code
     */
    private String roleCode;
    /**
     * variable use to hold no no of user of a role
     */
    private long noOfUsers;

    /**
     * constrictor use to initialize all properties
     *
     * @see RoleDTO
     * @param roleId contains role id
     * @param roleName contains role name
     * @param roledDescription contains role description
     * @param roleCode contains role code
     * @param noOfUsers contains no of users
     */
    public RoleDTO(Long roleId, String roleName, String roledDescription, String roleCode, long noOfUsers) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleDescription = roledDescription;
        this.roleCode = roleCode;
        this.noOfUsers = noOfUsers;
    }

    /**
     * Gets the role Id
     *
     * @return {@link Long} role Id
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * Gets the role name
     *
     * @return {@link String} role name
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Gets the role description
     *
     * @return {@link String} role description
     */
    public String getRoleDescription() {
        return roleDescription;
    }

    /**
     * Gets the role code
     *
     * @return {@link String} role code
     */
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Gets the no of users
     *
     * @return {@link Long} no of users
     */
    public long getNoOfUsers() {
        return noOfUsers;
    }

    /**
     * This is a setter which sets the role id
     *
     * @param roleId contains role id
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * This is a setter which sets the role name
     *
     * @param roleName contains role name
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * This is a setter which sets the role description
     *
     * @param roleDescription contains role description
     */
    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    /**
     * This is a setter which sets the role code
     *
     * @param roleCode contains role code
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    /**
     * This is a setter which sets the no of users for a role
     *
     * @param noOfUsers contains no of users
     */
    public void setNoOfUsers(long noOfUsers) {
        this.noOfUsers = noOfUsers;
    }
}
