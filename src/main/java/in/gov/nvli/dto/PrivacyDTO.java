/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.dto;

import java.util.Date;

/**
 * DTO used for #PolicyDocument
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class PrivacyDTO {

    private Long policyId;

    private String tittle;

    private String description;

    private String createdBy;

    private String updatedBy;

    private Date lastModifiedDate;

    private Date publishDate;

    private Long version;

    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @return
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     *
     * @return
     */
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     *
     * @return
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     *
     * @param updatedBy
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     *
     * @param lastModifiedDate
     */
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     *
     * @param publishDate
     */
    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public String getTittle() {
        return tittle;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param tittle
     */
    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @param policyId
     */
    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    /**
     *
     * @return
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     *
     * @param policyId
     * @param tittle
     * @param description
     * @param createdBy
     * @param updatedBy
     * @param lastModifiedDate
     * @param publishDate
     * @param version
     */
    public PrivacyDTO(Long policyId, String tittle, String description, String createdBy, String updatedBy, Date lastModifiedDate, Date publishDate, Long version) {
        this.policyId = policyId;
        this.tittle = tittle;
        this.description = description;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.publishDate = publishDate;
        this.version = version;
    }

    /**
     *
     */
    public PrivacyDTO() {
    }

}
