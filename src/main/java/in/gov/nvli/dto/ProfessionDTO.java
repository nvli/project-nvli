package in.gov.nvli.dto;

/**
 * class use to represents user profession related information
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class ProfessionDTO {

    /**
     * variable use to hold profession name
     */
    private String professionName;

    /**
     * variable use to hold profession description
     */
    private String professionDescription;
    /**
     * variable use to hold udc tags
     */
    private String udcTags;
    /**
     * variable use to hold parent profession
     */
    private Integer parentProfession;

    /**
     * constructor use to initialized
     *
     */
    public ProfessionDTO() {
    }

    /**
     * constructor use to initialized
     *
     * @see ProfessionDTO
     * @param professionName contains profession name
     * @param professionDescription contains profession description
     * @param udcTags contains udc tags
     * @param parentProfession contains parent profession name
     */
    public ProfessionDTO(String professionName, String professionDescription, String udcTags, Integer parentProfession) {
        this.professionName = professionName;
        this.professionDescription = professionDescription;
        this.udcTags = udcTags;
        this.parentProfession = parentProfession;
    }

    /**
     * Gets the profession name
     *
     * @return {@link String} profession name
     */
    public String getProfessionName() {
        return professionName;
    }

    /**
     * This is a setter which sets the profession name
     *
     * @param professionName contains profession name
     */
    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    /**
     * Gets the profession name
     *
     * @return {@link String} profession description
     */
    public String getProfessionDescription() {
        return professionDescription;
    }

    /**
     * This is a setter which sets the profession description
     *
     * @param professionDescription contains profession description
     */
    public void setProfessionDescription(String professionDescription) {
        this.professionDescription = professionDescription;
    }

    /**
     * Gets the udc tags
     *
     * @return {@link String} udc tags
     */
    public String getUdcTags() {
        return udcTags;
    }

    /**
     * This is a setter which sets the udc tags
     *
     * @param udcTags contains udc tags
     */
    public void setUdcTags(String udcTags) {
        this.udcTags = udcTags;
    }

    /**
     * Gets the parent profession
     *
     * @return {@link Integer} parent profession id
     */
    public Integer getParentProfession() {
        return parentProfession;
    }

    /**
     * This is a setter which sets the parent parent id
     *
     * @param parentProfession contains parent profession id
     */
    public void setParentProfession(Integer parentProfession) {
        this.parentProfession = parentProfession;
    }

}
