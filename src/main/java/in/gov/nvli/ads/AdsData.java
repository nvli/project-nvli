/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.ads;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 * This Class used for parse JSON data of Ads
 *
 * @author Nitin
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AdsData implements Serializable {

    @JsonProperty("header")
    private String header;

    @JsonProperty("displayUrl")
    private String displayUrl;

    @JsonProperty("destinationUrl")
    private String destinationUrl;

    @JsonProperty("adType")
    private String adType;

    @JsonProperty
    private String adName;

    @JsonProperty
    private String relativePath;

    /**
     *
     */
    public AdsData() {

    }

    /**
     *
     * @param header
     * @param displayUrl
     * @param destinationUrl
     * @param adType
     * @param adName
     * @param relativePath
     */
    public AdsData(@JsonProperty("header") String header, @JsonProperty("displayUrl") String displayUrl,
                    @JsonProperty("destinationUrl") String destinationUrl, @JsonProperty("adType") String adType,
            @JsonProperty("adName") String adName, @JsonProperty("relativePath") String relativePath) {

        this.header = header;
        this.displayUrl = displayUrl;
        this.destinationUrl = destinationUrl;
        this.adType = adType;
        this.adName = adName;
        this.relativePath = relativePath;

    }

    /**
     *
     * @return
     */
    public String getHeader() {
        return header;
    }

    /**
     *
     * @param header
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     *
     * @return
     */
    public String getDisplayUrl() {
        return displayUrl;
    }

    /**
     *
     * @param displayUrl
     */
    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    /**
     *
     * @return
     */
    public String getDestinationUrl() {
        return destinationUrl;
    }

    /**
     *
     * @param destinationUrl
     */
    public void setDestinationUrl(String destinationUrl) {
        this.destinationUrl = destinationUrl;
    }

    /**
     *
     * @return
     */
    public String getAdType() {
        return adType;
    }

    /**
     *
     * @param adType
     */
    public void setAdType(String adType) {
        this.adType = adType;
    }

    /**
     *
     * @return
     */
    public String getAdName() {
        return adName;
    }

    /**
     *
     * @param adName
     */
    public void setAdName(String adName) {
        this.adName = adName;
    }

    /*
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AdsData{" + "header=" + header + ", displayUrl=" + displayUrl + ", destinationUrl=" + destinationUrl + ", adType=" + adType + ", adName=" + adName + ", relativePath=" + relativePath + '}';
    }

    /**
     *
     * @return
     */
    public String getRelativePath() {
        return relativePath;
    }

    /**
     *
     * @param relativePath
     */
    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

}
