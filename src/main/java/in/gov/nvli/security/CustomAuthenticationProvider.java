/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.security;

import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Prabhat
 */
@Transactional
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOG = Logger.getLogger(CustomAuthenticationProvider.class);
    private UserDAO userDAO;
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        User user = null;
        try {
            System.out.println("authentication.getName() => " + authentication.getName());
            System.out.println("authentication.getCredentials() => " + authentication.getCredentials().toString());
            System.out.println("authentication.getAuthorities() => " + authentication.getAuthorities().toString());

            user = userDAO.findByEmail(authentication.getName());
            System.out.println("user.email >> " + user.getEmail());
            System.out.println("user.active >> " + user.getActive());
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage(), ex);
            throw new BadCredentialsException("Invalid User Name or Password.");
        }
        if (user == null) {
            System.out.println("user == null");
            throw new BadCredentialsException("Invalid User Name or Password.");
        } else {
            System.out.println("user != null");
            /*if (user.getActive() == Constants.USER_INACTIVE) {
             throw new BadCredentialsException("User Inactive.Please Verify Email TO Activate Your Account.");
             }*/
            if (!passwordEncoder.matches(authentication.getCredentials().toString(), user.getPassword())) {
                System.out.println("Invalid User Name or Password");
                throw new BadCredentialsException("Invalid User Name or Password.");
            }
            return new UsernamePasswordAuthenticationToken(
                    user.getEmail(),
                    user.getPassword(),
                    getAuthorities(user.getRoles())
            );
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(roles));
        return authList;
    }

    private List<String> getRoles(Set<Role> roles) {
        List<String> roleCodes = new ArrayList<String>();
        for (Role role : roles) {
            roleCodes.add(role.getCode());
        }
        return roleCodes;
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public PasswordEncoder getPasswordEncoder() {
        return passwordEncoder;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean supports(Class<?> type) {
        return true;
    }

}
