package in.gov.nvli.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Filter to check addition parameters other than user name and password, or to
 * do some additional filtering work before the actual authentication get
 * started.e
 *
 * @author Prabhat
 */
public class UserNamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String referrer = (String) request.getParameter("url_prior_login");
        request.getSession(false);
        //Creating new JSESSION ID so that the JSESSION ID before and after login are different
        request.getSession().invalidate();
        request.getSession(true);
        if(referrer!=null && !referrer.isEmpty()){
            request.setAttribute("url_prior_login", referrer);
        }
        return super.attemptAuthentication(request, response);
    }
}
