package in.gov.nvli.security;

import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "security")
public class SuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private UserActivityService userActivityService;

    @Autowired
    private UserService userService;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.web.authentication.
     * SavedRequestAwareAuthenticationSuccessHandler
     * #onAuthenticationSuccess(javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse,
     * org.springframework.security.core.Authentication)
     */
    @RequestMapping(value = "/handler")
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws ServletException, IOException {
        User loggedInUser = userService.getCurrentUser();
        if (loggedInUser.getEnabled() == 0) {
            loggedInUser.setEnabled(Constants.USER_ACTIVATED);
            userService.updateUser(loggedInUser);
        }
        String url = request.getParameter("url_prior_login");
        if (url == null || url.isEmpty() || this.escapeURL(url)) {
            url = "/auth/login/success";
        }
        if (request.getParameter("isSocialSignIn") == null) {
            userActivityService.saveOtherActivityOfUser(loggedInUser.getId(), null, Constants.UserActivityConstant.LOGIN, request, loggedInUser.getUsername(), null);
        }
        request.getSession().setAttribute("user", loggedInUser);
        getRedirectStrategy().sendRedirect(request, response, url);
    }

    private boolean escapeURL(String url) {
        String[] uris = Constants.ESCAPE_URLS_FROM_REDIRECTION;
        for (String uri : uris) {
            if (url.contains(uri)) {
                return true;
            };
        }
        return false;
    }
}
