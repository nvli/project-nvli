package in.gov.nvli.custom.annotation;

import in.gov.nvli.custom.validator.EmailAvailableValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * This is a custom annotation for checking email availability 
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailAvailableValidator.class)
@Documented
public @interface IsEmailAvailable {

    String message() default "Invalid email";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
