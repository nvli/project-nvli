package in.gov.nvli.custom.user;

/**
 *
 * @author Bhumika
 */
public class CustomUserPasswordReset {   
    private Long userId;

    private String emailId;

    private String newPassword;

    private String confirmPassword;
    
     public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CustomUserPasswordReset() {
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newpassword) {
        this.newPassword = newpassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
