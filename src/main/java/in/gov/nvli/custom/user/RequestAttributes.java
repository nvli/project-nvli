package in.gov.nvli.custom.user;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Bhumika
 */
public class RequestAttributes {

    private String ipAddress;
    private String userAgent;

    public RequestAttributes(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        String ua = request.getHeader("User-Agent");
        if (ua == null) {
            ua = "";
        }

        this.setIpAddress(ip);
        this.setUserAgent(ua);
    }

    public RequestAttributes() {
    }

    
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

}
