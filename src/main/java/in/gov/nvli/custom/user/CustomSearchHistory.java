package in.gov.nvli.custom.user;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Bhumika
 */
public class CustomSearchHistory {

    String searchedRecord;
    Date searchDate;

    public CustomSearchHistory() {
    }

    public CustomSearchHistory(String searchedRecord, Date searchDate) {
        this.searchedRecord = searchedRecord;
        this.searchDate = searchDate;
    }

    public String getSearchedRecord() {
        return searchedRecord;
    }

    public void setSearchedRecord(String searchedRecord) {
        this.searchedRecord = searchedRecord;
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.searchedRecord);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CustomSearchHistory other = (CustomSearchHistory) obj;
        if (!Objects.equals(this.searchedRecord, other.searchedRecord)) {
            return false;
        }
        return true;
    }
}
