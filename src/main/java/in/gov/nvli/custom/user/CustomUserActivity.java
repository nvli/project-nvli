package in.gov.nvli.custom.user;

/**
 * Customized UserActivityLog bean having only those properties that are needed
 * in the view
 *
 * @author Bhumika,Ruturaj
 * @since 1
 * @version 1
 */
public class CustomUserActivity {

    Long uid;
    String activityId;
    String profilePicPath;
    String activityText;
    String activityTime;
    String recordId;
    String recordImgPath;

    public CustomUserActivity() {
    }

    public Long getUid() {
        return uid;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getActivityText() {
        return activityText;
    }

    public void setActivityText(String activityText) {
        this.activityText = activityText;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getRecordImgPath() {
        return recordImgPath;
    }

    public void setRecordImgPath(String recordImgPath) {
        this.recordImgPath = recordImgPath;
    }

}
