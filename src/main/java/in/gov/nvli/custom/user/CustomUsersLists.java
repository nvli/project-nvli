package in.gov.nvli.custom.user;

import java.util.List;
import org.json.simple.JSONObject;

/**
 *
 * @author Bhumika
 */
public class CustomUsersLists {

    private long userid;
    private long invitationId;
    private String userName;
    private String userMail;
    private List<JSONObject> userRoles;
    private String invitationDate;
    private String userStatus;

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public List<JSONObject> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<JSONObject> userRoles) {
        this.userRoles = userRoles;
    }

    public String getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(String invitationDate) {
        this.invitationDate = invitationDate;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public long getInvitationId() {
        return invitationId;
    }

    public void setInvitationId(long invitationId) {
        this.invitationId = invitationId;
    }

}
