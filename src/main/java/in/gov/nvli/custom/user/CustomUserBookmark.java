package in.gov.nvli.custom.user;

/**
 *
 * @author Bhumika
 */
public class CustomUserBookmark{
    
    private Long id;
    private String uniqueResIdentifier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueResIdentifier() {
        return uniqueResIdentifier;
    }

    public void setUniqueResIdentifier(String uniqueResIdentifier) {
        this.uniqueResIdentifier = uniqueResIdentifier;
    }
}
