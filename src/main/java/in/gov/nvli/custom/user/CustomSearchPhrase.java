/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.custom.user;

import java.util.List;

/**
 *
 * @author vootla
 */
public class CustomSearchPhrase 
{
private String phrase;
private String link;
private String addedTime;
private List<CustomDashBoard> visitedResources;

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime = addedTime;
    }

    public List<CustomDashBoard> getVisitedResources() {
        return visitedResources;
    }

    public void setVisitedResources(List<CustomDashBoard> visitedResources) {
        this.visitedResources = visitedResources;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
    
}
