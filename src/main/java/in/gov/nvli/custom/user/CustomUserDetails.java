/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.custom.user;

import in.gov.nvli.domain.user.Role;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Bhumika
 */
public class CustomUserDetails {
    private Long id;
    private String email;
    private byte enabled;
    private byte deleted;
    private String firstName;
    private String middleName;
    private String lastName;
    private Set<Role> roles;

    public CustomUserDetails() {

    }

    public CustomUserDetails(Long id,  String email, byte enabled, byte deleted,String firstName, String middleName, String lastName, Set<Role> roles) {
        this.id = id;
        this.email = email;
        this.enabled = enabled;
        this.deleted=deleted;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte getEnabled() {
        return enabled;
    }

    public void setEnabled(byte enabled) {
        this.enabled = enabled;
    }

    public byte getDeleted() {
        return deleted;
    }

    public void setDeleted(byte deleted) {
        this.deleted = deleted;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

}
