/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.custom.user;

/**
 *
 * @author Bhumika
 */
public class CustomUserSavedList {
    private String listId;
    private String listName;
    private String listVisibility;

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

   

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListVisibility() {
        return listVisibility;
    }

    public void setListVisibility(String listVisibility) {
        this.listVisibility = listVisibility;
    }
    
}
