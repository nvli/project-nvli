package in.gov.nvli.custom.user;

/**
 *
 * @author Bhumika
 */
public class CustomRecordReview {

    private String addedTime;
    private String userName;
    private String reviews;
    private String userProfilePicPath;
    /**
     * The property recordRating is used to store rating on record.This is used
     * in Rating activity.
     */
    private int recordRating;

    public String getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime = addedTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getUserProfilePicPath() {
        return userProfilePicPath;
    }

    public void setUserProfilePicPath(String userProfilePicPath) {
        this.userProfilePicPath = userProfilePicPath;
    }

    public int getRecordRating() {
        return recordRating;
    }

    public void setRecordRating(int recordRating) {
        this.recordRating = recordRating;
    }
}
