package in.gov.nvli.custom.validator;

import in.gov.nvli.custom.annotation.IsEmailAvailable;
import in.gov.nvli.service.UserService;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Email available constraint validator
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public class EmailAvailableValidator implements ConstraintValidator<IsEmailAvailable, String> {

    @Autowired
    UserService userService;

    @Override
    public void initialize(IsEmailAvailable constraintAnnotation) {
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return !userService.emailExist(email);
    }
}
