package in.gov.nvli.custom.validator;

import in.gov.nvli.beans.NewUserBean;
import in.gov.nvli.custom.annotation.PasswordMatches;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Password match constraint validator
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        NewUserBean user = (NewUserBean) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}
