package in.gov.nvli.custom.exceptions;

/**
 * Custom Exception to handle if Email Already exists
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public class EmailExistsException extends Exception {

    public EmailExistsException(String s) {
        super(s);
    }

}
