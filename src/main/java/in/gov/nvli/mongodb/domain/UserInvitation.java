package in.gov.nvli.mongodb.domain;

import java.io.Serializable;

/**
 * UserInvitation class is used to hold UserInvitation activity
 *
 * @author Madhuri
 * @version 1
 * @since 1
 */
public class UserInvitation extends Activity implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property inviteeEmail is used to store email id of invitee
     */
    private String inviteeEmail;
    /**
     * The property grantedRoles is used to store roles allowed for invitee
     */
    private String grantedRoles;
    private Long senderUserId;
    private String senderUserName;

    public Long getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(Long senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }

    /**
     * The getInviteeEmail method used to get email id of invitee
     *
     * @return inviteeEmail
     */
    public String getInviteeEmail() {
        return inviteeEmail;
    }

    /**
     * The setInviteeEmail method used to set email id of invitee
     *
     * @param inviteeEmail
     */
    public void setInviteeEmail(String inviteeEmail) {
        this.inviteeEmail = inviteeEmail;
    }

    /**
     * The method getGrantedRoles is used to get roles allowed for invitee
     *
     * @return grantedRoles
     */
    public String getGrantedRoles() {
        return grantedRoles;
    }

    /**
     * The method setGrantedRoles is used to set roles allowed for invitee
     *
     * @param grantedRoles
     */
    public void setGrantedRoles(String grantedRoles) {
        this.grantedRoles = grantedRoles;
    }
}
