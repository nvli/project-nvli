/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ruturaj
 */
@Document(collection = "userAddedList")
public class UserAddedList extends Activity implements Serializable {
      /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;
    /**
     * The property _id used to store BSON id in mongoDB
     */
    @Id
    ObjectId _id;
    /**
     * The property userId used to store user id
     */
    private Long userId;
    /**
     * The property username used to store name of user
     */
    private String username;
    /**
     * The property recordIdentifier used to store record identifier
     */
    private String recordIdentifier;
    /**
     * The property recordTitle used to store title of records
     */
    private String recordTitle;
     /**
     * The property listName used to store user searched record list
     */
    private String listName;
    /**
     * This property updatedListName used to store updated name of list.
     */
     private String updatedListName;
    /**
     * The property publicPrivate used to hold Integer value for user searched
     * record list is private or public
     * 0-public
     * 1-private
     */
    private Integer publicPrivate;
     /**
     * This property used to add type of record added to list.
     */
    private String typeOfRecord;
     /**
     * The property isUpdate used to hold Integer value for if user created or updated list
     * 0-created list
     * 1-updated
     */
    private Integer isUpdate;
    /**
     * This property used to store number of records in a list.
     */
    private Integer recordsCount;
    /**
     * This property used to hold number of subscribed user for this list.
     */
    private Integer followerCount;
   /**
    * This property used to hold path of profile photo of creator of list.
    */
    private String userProfilePhotoPath;

    public UserAddedList() {
    }

    public UserAddedList(Long userId, String username, String recordIdentifier, String recordTitle, String listName, Integer publicPrivate,Integer isUpdate, Date activityTs) {
        this.userId = userId;
        this.username = username;
        this.recordIdentifier = recordIdentifier;
        this.recordTitle = recordTitle;
        this.listName = listName;
        this.publicPrivate = publicPrivate;
        this.isUpdate=isUpdate;
        this.activityTs = activityTs;
    }

    
    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId _id) {
        this._id = _id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public String getRecordTitle() {
        return recordTitle;
    }

    public void setRecordTitle(String recordTitle) {
        this.recordTitle = recordTitle;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public Integer getPublicPrivate() {
        return publicPrivate;
    }

    public void setPublicPrivate(Integer publicPrivate) {
        this.publicPrivate = publicPrivate;
    }  

    public Date getActivityTs() {
        return activityTs;
    }

    public void setActivityTs(Date activityTs) {
        this.activityTs = activityTs;
    }

    public String getUpdatedListName() {
        return updatedListName;
    }

    public void setUpdatedListName(String updatedListName) {
        this.updatedListName = updatedListName;
    } 

    public Integer getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(Integer isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getTypeOfRecord() {
        return typeOfRecord;
    }

    public void setTypeOfRecord(String typeOfRecord) {
        this.typeOfRecord = typeOfRecord;
    }

    public Integer getRecordsCount() {
        return recordsCount;
    }

    public void setRecordsCount(Integer recordsCount) {
        this.recordsCount = recordsCount;
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public String getUserProfilePhotoPath() {
        return userProfilePhotoPath;
    }

    public void setUserProfilePhotoPath(String userProfilePhotoPath) {
        this.userProfilePhotoPath = userProfilePhotoPath;
    }   
}
