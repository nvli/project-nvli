package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.List;

/**
 * UserActivity class used to holds user activity logs for each activity type
 * <ul>
 * <li>Rate Record</li>
 * <li>Like Record</li>
 * <li>Review on Record</li>
 * <li>Share records to nvli users</li>
 * <li>Share records on social networking</li>
 * </ul>
 *
 * @author Sujata Aher
 * @author Darshana Agrawal
 * @version 1
 * @version 1
 */
public class UserActivity extends Activity implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property recordRating is used to store rating on record.This is used
     * in Rating activity.
     */
    private Integer recordRating;
    /**
     * The property message used to store what is the user message on records
     * This field is used in giving the Review and nvlishare activity
     */
    private String message;

    /**
     * The property siteName used to store to which site it belongs like
     * Facebook share,Google share etc.This is used in Socialshare activity.
     */
    private String siteName;

    /**
     * The property searchPhrase used to store user searched keyword.This is
     * used in Search and SaveSearch activity.
     */
    private String searchPhrase;
    /**
     * The property permaLink used to store permanent link of searched
     * record.This is used in Search , SaveSearch and VisitedLink activity.
     */
    private String permaLink;
    /**
     * Name of ebook
     */
    private String eBookName;

    public String geteBookName() {
        return eBookName;
    }

    public void seteBookName(String eBookName) {
        this.eBookName = eBookName;
    }
    /**
     *
     */
    private List<String> tokanizedPhrase;

    public List<String> getTokanizedPhrase() {
        return tokanizedPhrase;
    }

    public void setTokanizedPhrase(List<String> tokanizedPhrase) {
        this.tokanizedPhrase = tokanizedPhrase;
    }

    /**
     * The method getRecordRating is used to get rating on record
     *
     * @return recordRating
     */
    public Integer getRecordRating() {
        return recordRating;
    }

    /**
     * The method setRecordRating is used to set rating on record
     *
     * @param recordRating
     */
    public void setRecordRating(Integer recordRating) {
        this.recordRating = recordRating;
    }

    /**
     * The getSiteName method used to get siteName
     *
     * @return siteName
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * The setSiteName method used to set siteName
     *
     * @param siteName
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * The method getMessage is used to get message what the user share
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * The method setMessage is used to set message what the user share
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * The method getPermaLink is used to get permanent link of searched record
     *
     * @return permaLink
     */
    public String getPermaLink() {
        return permaLink;
    }

    /**
     * The method setPermaLink is used to set permanent link of searched record
     *
     * @param permaLink
     */
    public void setPermaLink(String permaLink) {
        this.permaLink = permaLink;
    }

    /**
     * The method getSearchPhrase is used to get user searched keyword
     *
     * @return searchPhrase
     */
    public String getSearchPhrase() {
        return searchPhrase;
    }

    /**
     * The method setSearchPhrase is used to set user searched keyword
     *
     * @param searchPhrase
     */
    public void setSearchPhrase(String searchPhrase) {
        this.searchPhrase = searchPhrase;
    }
}
