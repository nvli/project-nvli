/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.domain;

/**
 *
 * @author Gulafsha
 */
public class Tags {

    /**
     * The property languageId used to store tagged languageId
     */
    Long languageId;

    String udcNotation;

    String tag;

    Integer points;

    public Tags() {
    }

    public Tags(Long languageId, String tag, Integer points) {
        this.languageId = languageId;
        this.tag = tag;
        this.points = points;
    }

    public Tags(String udcNotation, Integer points) {
        this.udcNotation = udcNotation;
        this.points = points;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getUdcNotation() {
        return udcNotation;
    }

    public String getTag() {
        return tag;
    }

    public void setUdcNotation(String udcNotation) {
        this.udcNotation = udcNotation;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

}
