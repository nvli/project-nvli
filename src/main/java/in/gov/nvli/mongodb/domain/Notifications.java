package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Notifications class is used to save notifications of various activity type
 *
 * @author Darshana
 * @version 1
 * @version 1
 */
@Document(collection = "notification")
public class Notifications implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property _id used to store BSON id in mongoDB
     */
    @Id
    ObjectId _id;
    /**
     * The property objectId used to holds _id for manipulation in application.
     */
    @Transient
    private String objectId;
    /**
     * The property notifactionDateTime property used to store notification date
     * and time
     */
    private Date notifactionDateTime;
    /**
     * The property notificationMessage used to holds proper message , which
     * record is liked ,rated ,shares etc by user will displayed along with the
     * user name
     */
    private String notificationMessage;
    /**
     * The property notificationTargetURL used to store notified records URL
     */
    private String notificationTargetURL;
    /**
     * The property notificationActivityType used to store notification activity
     * type
     */
    private String notificationActivityType;
    /**
     * The property isReadByReceiver used to store boolean value true/false
     * weather record read by user or not
     */

    @Field(value = "isReadByReceiver")
    private Boolean isReadByReceiver;
    /**
     * The property isDeletedByUser used to store boolean value true/false
     * weather record delete by user or not
     */
    private Boolean isDeletedByUser;
    /**
     * The property senderId used to store sender'sID who is sending the record
     */
    private Long senderId;
    /**
     * The property recieverId used to store reciever'sID who is receiving the
     * record
     */
    @Field(value = "reciever_Id")
    private Long recieverId;

    /**
     * The method getObjectId is used to get _id for manipulation in application
     *
     * @return objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * The method setObjectId is used to set _id for manipulation in application
     *
     * @param objectId
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * The method getSenderId is used to get sender'sID who is sending the
     * record
     *
     * @return senderId
     */
    public Long getSenderId() {
        return senderId;
    }

    /**
     * The method setSenderId is used to set sender'sID who is sending the
     * record
     *
     * @param senderId
     */
    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    /**
     * The method getId is used to get BSON id in mongoDB
     *
     * @return _id
     */
    public ObjectId getId() {
        return _id;
    }

    /**
     * The method setId is used to set BSON id in mongoDB
     *
     * @param _id
     */
    public void setId(ObjectId _id) {
        this._id = _id;
    }

    /**
     * The method isIsReadByReceiver is used to check the boolean value
     * true/false weather record read by user or not
     *
     * @return isReadByReceiver
     */
    public Boolean isIsReadByReceiver() {
        return isReadByReceiver;
    }

    /**
     * The method setIsReadByReceiver is used to set the boolean value
     * true/false to check weather record read by user or not
     *
     * @param isReadByReceiver
     */
    public void setIsReadByReceiver(Boolean isReadByReceiver) {
        this.isReadByReceiver = isReadByReceiver;
    }

    /**
     * The method isIsDeletedByUser used to check boolean value true/false
     * weather record delete by user or not
     *
     * @return isDeletedByUser
     */
    public Boolean isIsDeletedByUser() {
        return isDeletedByUser;
    }

    /**
     * The method setIsDeletedByUser used to set boolean value true/false to
     * check weather record delete by user or not
     *
     * @param isDeletedByUser
     */
    public void setIsDeletedByUser(Boolean isDeletedByUser) {
        this.isDeletedByUser = isDeletedByUser;
    }

    /**
     * The method getNotifactionDateTime is used to get notification date and
     * time
     *
     * @return notifactionDateTime
     */
    public Date getNotifactionDateTime() {
        return notifactionDateTime;
    }

    /**
     * The method setNotifactionDateTime is used to set notification date and
     * time
     *
     * @param notifactionDateTime
     */
    public void setNotifactionDateTime(Date notifactionDateTime) {
        this.notifactionDateTime = notifactionDateTime;
    }

    /**
     * The method getNotificationMessage is used to get proper message , which
     * record is liked ,rated ,shares etc by user will displayed along with the
     * user name
     *
     * @return notificationMessage
     */
    public String getNotificationMessage() {
        return notificationMessage;
    }

    /**
     * The method setNotificationMessage is used to set proper message , which
     * record is liked ,rated ,shares etc by user will displayed along with the
     * user name
     *
     * @param notificationMessage
     */
    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    /**
     * The method getNotificationTargetURL is used to get notified records URL
     *
     * @return notificationTargetURL
     */
    public String getNotificationTargetURL() {
        return notificationTargetURL;
    }

    /**
     * The method setNotificationTargetURL is used to set notified records URL
     *
     * @param notificationTargetURL
     */
    public void setNotificationTargetURL(String notificationTargetURL) {
        this.notificationTargetURL = notificationTargetURL;
    }

    /**
     * The method getRecieverId is used to get reciever'sID who is receiving the
     * record
     *
     * @return recieverId
     */
    public Long getRecieverId() {
        return recieverId;
    }

    /**
     * The method setRecieverId is used to set reciever'sID who is receiving the
     * record
     *
     * @param recieverId
     */
    public void setRecieverId(Long recieverId) {
        this.recieverId = recieverId;
    }

    /**
     * The method getNotificationActivityType is used to get notification
     * activity type
     *
     * @return notificationActivityType
     */
    public String getNotificationActivityType() {
        return notificationActivityType;
    }

    /**
     * The method setNotificationActivityType is used to set notification
     * activity type
     *
     * @param notificationActivityType
     */
    public void setNotificationActivityType(String notificationActivityType) {
        this.notificationActivityType = notificationActivityType;
    }
}
