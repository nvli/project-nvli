/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.domain;

import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 *
 * @author ruturaj
 */
@Document(collection = "userSubscribedList")
public class UserSubscribedList {
     /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;
    /**
     * The property _id used to store BSON id in mongoDB
     */
    @Id
    ObjectId _id;
    /**
     * The property listName used to store name of subscribed list
     */
    private String listName;
    /**
     * The property subscribedUserId used to store user id who has subscribed to list
     */
    private Long subscribedUserId;
    /**
     * The property subscribedUsername used to store name of user who has subscribed to list
     */
    private String subscribedUsername;
     /**
     * The property createdByUserId used to store name of user who has created the list
     */
    private Long createdByUserId;
     /**
     * The property createdByUserName used to store name of user who has created the list
     */
    private String createdByUserName;
    /**
     * The property createdByUserName used to store url of profile pic of user who has created the list
     */
    private String createdByProfilePicPath;
    
    /**
     * The property activityTs is used to hold timestamp
     */
    @Field(value = "subscribed_date")
    Date subscribedDate;

    public UserSubscribedList(String listName, Long subscribedUserId, String subscribedUsername, Long createdByUserId, String createdByUserName, String createdByProfilePicPath, Date subscribedDate) {
        this.listName = listName;
        this.subscribedUserId = subscribedUserId;
        this.subscribedUsername = subscribedUsername;
        this.createdByUserId = createdByUserId;
        this.createdByUserName = createdByUserName;
        this.createdByProfilePicPath = createdByProfilePicPath;
        this.subscribedDate = subscribedDate;
    }

   

    public UserSubscribedList() {
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public Long getSubscribedUserId() {
        return subscribedUserId;
    }

    public void setSubscribedUserId(Long subscribedUserId) {
        this.subscribedUserId = subscribedUserId;
    }

    public String getSubscribedUsername() {
        return subscribedUsername;
    }

    public void setSubscribedUsername(String subscribedUsername) {
        this.subscribedUsername = subscribedUsername;
    }

    public Long getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(Long createdByUserId) {
        this.createdByUserId = createdByUserId;
    }


    public String getCreatedByUserName() {
        return createdByUserName;
    }

    public void setCreatedByUserName(String createdByUserName) {
        this.createdByUserName = createdByUserName;
    }

    public Date getSubscribedDate() {
        return subscribedDate;
    }

    public void setSubscribedDate(Date subscribedDate) {
        this.subscribedDate = subscribedDate;
    }

    public String getCreatedByProfilePicPath() {
        return createdByProfilePicPath;
    }

    public void setCreatedByProfilePicPath(String createdByProfilePicPath) {
        this.createdByProfilePicPath = createdByProfilePicPath;
    }
}
