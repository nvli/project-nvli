package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 *
 * @author Darshana
 */
public class UserEBook implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * The property _id used to store BSON id in mongoDB
     */
    @Id
    ObjectId _id;
    /**
     * The property userId used to store user id
     */
    @Indexed
    Long userId;
    /**
     * The Property eBookName is used to hold name of ebook
     */
    private String eBookName;
    /**
     * The Property recordIdentifier is used to hold record id
     */
    private String recordIdentifier;
    /**
     * The Property addedTime is used to hold time of activity
     */
    private Date addedTime;

    /**
     * The method getId is used to get BSON id in mongoDB
     *
     * @return _id
     */
    public ObjectId getId() {
        return _id;
    }

    /**
     * The method setId is used to set BSON id in mongoDB
     *
     * @param _id
     */
    public void setId(ObjectId _id) {
        this._id = _id;
    }

    /**
     * The method getUserId is used to get userID of user
     *
     * @return userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * The method setUserId is used to set userID of user
     *
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * The method geteBookName is used to get the name of ebook
     *
     * @return eBookName
     */
    public String geteBookName() {
        return eBookName;
    }

    /**
     * The method seteBookName is used to set the name of ebook
     *
     * @param eBookName
     */
    public void seteBookName(String eBookName) {
        this.eBookName = eBookName;
    }

    /**
     * The method getRecordIdentifier is used get record identifier of record
     *
     * @return recordIdentifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * The method setRecordIdentifier is used set record identifier of record
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * The method getAddedTime is used to get the time of userebook activity
     *
     * @return addedTime
     */
    public Date getAddedTime() {
        return addedTime;
    }

    /**
     * The method setAddedTime is used to set the time of userebook activity
     *
     * @param addedTime
     */
    public void setAddedTime(Date addedTime) {
        this.addedTime = addedTime;
    }

}
