package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Activity class used to holds user Activity. It's a Base class of all
 * Activities
 *
 * @author Madhuri
 * @version 1
 * @since 1
 *
 */
public class Activity implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property activityTs is used to hold timestamp
     */
    @Field(value = "activity_ts")
    Date activityTs;
    /**
     * The property updatedOrSharedWithUserId is used to hold userID who has
     * done shared or updated activity.This property is used in NVLI share
     * activity.
     */
    Long updatedOrSharedWithUserId;
    /**
     * The property updatedOrSharedWithUserFullName is used to hold the full
     * name of affected user
     */
    String updatedOrSharedWithUserFullName;

    /**
     * The method getUpdatedOrSharedWithUserFullName is used to get the full
     * name of affected user
     *
     * @return updatedOrSharedWithUserFullName
     */
    public String getUpdatedOrSharedWithUserFullName() {
        return updatedOrSharedWithUserFullName;
    }

    /**
     * The method setUpdatedOrSharedWithUserFullName is used to set the full
     * name of affected user
     *
     * @param updatedOrSharedWithUserFullName
     */
    public void setUpdatedOrSharedWithUserFullName(String updatedOrSharedWithUserFullName) {
        this.updatedOrSharedWithUserFullName = updatedOrSharedWithUserFullName;
    }

    /**
     * The method getUpdatedOrSharedWithUserId is used to get the userID who has
     * done shared or updated activity
     *
     * @return updatedOrSharedWithUserId
     */
    public Long getUpdatedOrSharedWithUserId() {
        return updatedOrSharedWithUserId;
    }

    /**
     * The method setUpdatedOrSharedWithUserId is used to set the userID who has
     * done shared or updated activity
     *
     * @param updatedOrSharedWithUserId
     */
    public void setUpdatedOrSharedWithUserId(Long updatedOrSharedWithUserId) {
        this.updatedOrSharedWithUserId = updatedOrSharedWithUserId;
    }

    /**
     * The method getActivityTs is used to get the activity timestamp
     *
     * @return activityTs
     */
    public Date getActivityTs() {
        return activityTs;
    }

    /**
     * The method setActivityTs is used to set the activity timestamp
     *
     * @param activityTs
     */
    public void setActivityTs(Date activityTs) {
        this.activityTs = activityTs;
    }
}
