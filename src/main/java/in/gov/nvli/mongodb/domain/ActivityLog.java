package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * ActivityLog Entity representing individual activity log. This entity is used
 * to save logs of activities like login,sign-up,change password,activate user,
 * deactivate user,delete user,update profile and edit user details performed by
 * logged in user.
 *
 * @author Madhuri
 * @author Suman Behara <sumanb@cdac.in>
 * @param <T>
 * @since 1
 * @version 1
 *
 */
@Document(collection = "activityLog")
public class ActivityLog<T> implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property _id used to store BSON id in mongoDB
     */
    @Id
    ObjectId _id;
    /**
     * The property userId used to store user id
     */
    Long userId;
    /**
     * The property is used to store user name of the user. It is the shard key
     * for sharding cluster
     *
     */
    private String username;
    /**
     * The property activityType used to store activity Type
     */
    @Field(value = "activity_type")
    String activityType;
    /**
     * The property ipAddress used to holds client machine IP Address
     */
    private String ipAddress;
    /**
     * The property userAgentInfo used to holds client machine browser
     * information
     */
    private String userAgentInfo;
    /**
     * The property recordIdentifier used to store record identifier
     */
    private String recordIdentifier;
    /**
     * The property recordTitle is used to hold the title of records. this
     * property is used in visited links ,rating,like ,review,NVLI share,social
     * share,save search,
     */
    private String recordTitle;
    /**
     * The property recordType is used to hold the type of
     * records(audio/text/video/image). this property is used in record related
     * activities.
     */
    private String recordType;
    /**
     * activity used to holds T type of activity where T can be
     * UserLike,UserRating etc
     */
    T activity;

    /**
     * The method getRecordTitle is used to get the title of records
     *
     * @return recordTitle
     */
    public String getRecordTitle() {
        return recordTitle;
    }

    /**
     * The method setRecordTitle is used to set the title of records
     *
     * @param recordTitle
     */
    public void setRecordTitle(String recordTitle) {
        this.recordTitle = recordTitle;
    }

    /**
     * The method getActivityType is used to get activity Type
     *
     * @return activityType
     */
    public String getActivityType() {
        return activityType;
    }

    /**
     * The method setActivityType is used to set activity Type
     *
     * @param activityType
     */
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    /**
     * The method getUserId is used to get userID of user
     *
     * @return userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * The method setUserId is used to set userID of user
     *
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * The method getId is used to get BSON id in mongoDB
     *
     * @return _id
     */
    public ObjectId getId() {
        return _id;
    }

    /**
     * The method setId is used to set BSON id in mongoDB
     *
     * @param _id
     */
    public void setId(ObjectId _id) {
        this._id = _id;
    }

    /**
     * The method getActivity is used to get activity used to holds T type of
     * activity
     *
     * @return activity
     */
    public T getActivity() {
        return activity;
    }

    /**
     * The method setActivity is used to set activity used to holds T type of
     * activity
     *
     * @param activity
     */
    public void setActivity(T activity) {
        this.activity = activity;
    }

    /**
     * The method getIpAddress is used to get client machine IP Address
     *
     * @return ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * The method setIpAddress is used to set client machine IP Address
     *
     * @param ipAddress
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * The method getUserAgentInfois used to get client machine browser
     * information
     *
     * @return userAgentInfo
     */
    public String getUserAgentInfo() {
        return userAgentInfo;
    }

    /**
     *
     * The method setUserAgentInfois used to set client machine browser
     * information
     *
     * @param userAgentInfo
     */
    public void setUserAgentInfo(String userAgentInfo) {
        this.userAgentInfo = userAgentInfo;
    }

    /**
     * The method getRecordIdentifier is used get record identifier of record
     *
     * @return recordIdentifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * The method setRecordIdentifier is used set record identifier of record
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This method is used to get username
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * This method is used to set username
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * This method is used to get type of record
     *
     * @return
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * This method is used to set type of record.
     *
     * @param recordType
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
}
