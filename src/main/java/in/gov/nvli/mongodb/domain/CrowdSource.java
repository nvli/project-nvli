/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.domain;

/**
 * This class will be used as embedded object for CrowdSourceAndTagging class
 * which tag details or edited metadata details.
 *
 * @author ruturaj powar
 */
public class CrowdSource {

    /**
     * This filed will hold edited field name as key for edit metadata activity.
     */
    String fieldName;
    /**
     * In edit metadata activity,this field will hold old value of edited field.
     */
    String oldValue;
    /**
     * In edit metadata activity,this field will hold new value of edited field.
     */
    String newValue;
    /**
     * This filed will hold rewards points achieved by user for particular
     * activity
     */
    Integer points;

    public CrowdSource() {
    }

    /**
     * This constructor will be used while edit metadata activity.
     * @param fieldName
     * @param oldValue
     * @param newValue
     * @param points
     */
    public CrowdSource(String fieldName, String oldValue, String newValue, Integer points) {
        this.fieldName = fieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.points = points;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

}
