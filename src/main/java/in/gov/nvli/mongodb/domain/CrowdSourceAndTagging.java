package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * This class used for logging of any crowdsouce activity(edit metadata, UDC or
 * custom tagging) by user.
 *
 * @author ruturaj powar
 */
public class CrowdSourceAndTagging extends Activity implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;
    /**
     * This field will be used for holding tag type added by user.
     */
    String tagType;
    /**
     * This field is used to maintain which version of metadata user has edited
     */
    Long crowdSourceId;
    /**
     * This field is used to hold edit number of specific version metadata.
     */
    Integer editNumber;
    /**
     * The property languageId used to store tagged languageId
     */
    Long languageId;

    /**
     * This field is used to hold list of edited metadata details.
     */
    List<CrowdSource> crowdSource;
    /**
     * This field is used to hold list of added tag details.
     */
    List<Tags> tags;

    public CrowdSourceAndTagging() {
    }

    public CrowdSourceAndTagging(Long crowdSourceId, Integer editNumber, List<CrowdSource> crowdSource, Date activityTs) {
        this.activityTs = activityTs;
        this.crowdSourceId = crowdSourceId;
        this.editNumber = editNumber;
        this.crowdSource = crowdSource;
    }

    public CrowdSourceAndTagging(String tagType, Long crowdSourceId, List<Tags> tags, Date activityTs) {
        this.tagType = tagType;
        this.crowdSourceId = crowdSourceId;
        this.tags = tags;
        this.activityTs = activityTs;
    }

    public String getTagType() {
        return tagType;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public Long getCrowdSourceId() {
        return crowdSourceId;
    }

    public void setCrowdSourceId(Long crowdSourceId) {
        this.crowdSourceId = crowdSourceId;
    }

    public Integer getEditNumber() {
        return editNumber;
    }

    public void setEditNumber(Integer editNumber) {
        this.editNumber = editNumber;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public List<CrowdSource> getCrowdSource() {
        return crowdSource;
    }

    public void setCrowdSource(List<CrowdSource> crowdSource) {
        this.crowdSource = crowdSource;
    }
}
