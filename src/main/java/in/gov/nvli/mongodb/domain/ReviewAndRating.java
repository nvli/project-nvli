package in.gov.nvli.mongodb.domain;

import java.io.Serializable;
import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author savitak
 */
@Document(collection = "userReviewAndRating")
public class ReviewAndRating implements Serializable {

    /**
     * The property serialVersionUID used to hold serialVersionUID. The
     * serialization runtime associates with each serializable class a version
     * number, called a serialVersionUID, which is used during de-serialization
     * to verify that the sender and receiver of a serialized object have loaded
     * classes for that object that are compatible with respect to serialization
     */
    private static final long serialVersionUID = 1L;
    /**
     * The property _id used to store BSON id in mongoDB
     */
    @Id
    ObjectId _id;
    /**
     * The property userId used to store user id
     */
    @Indexed
    Long userId;
    /**
     * The property recordIdentifier used to store record identifier
     */
    private String recordIdentifier;
    /**
     * The property reviewText used to store what is the user message on records
     */
    private String reviewText;
    /**
     * The property activityTs is used to hold timestamp
     */
    Date timeStamp;
    /**
     * The property recordRating is used to store rating on record.This is used
     * in Rating activity.
     */
    private Integer recordRating;
    /**
     *
     */
    Long recordRatingCount;
    private Boolean isLiked;

    public Boolean getIsLiked() {
        return isLiked == null ? false : isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    public Integer getRecordRating() {
        return recordRating == null ? 0 : recordRating;
    }

    public void setRecordRating(Integer recordRating) {
        this.recordRating = recordRating;
    }

    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId _id) {
        this._id = _id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public String getReviewText() {
        return reviewText;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public Long getRecordRatingCount() {
        return recordRatingCount;
    }

    public void setRecordRatingCount(Long recordRatingCount) {
        this.recordRatingCount = recordRatingCount;
    }

    @Override
    public String toString() {
        return "ReviewAndRating{" + "_id=" + _id + ", userId=" + userId + ", recordIdentifier=" + recordIdentifier + ", reviewText=" + reviewText + ", timeStamp=" + timeStamp + ", recordRating=" + recordRating + ", recordRatingCount=" + recordRatingCount + '}';
    }

}
