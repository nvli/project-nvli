package in.gov.nvli.mongodb.repositories;

import in.gov.nvli.mongodb.domain.ReviewAndRating;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 *
 * @author savitak
 */
public interface ReviewAndRatingRepository extends MongoRepository<ReviewAndRating, ObjectId> {

    /**
     * This method is used to get the ReviewAndRating
     *
     * @param userId
     * @param recordId
     * @return
     */
    public ReviewAndRating findByUserIdAndRecordIdentifier(Long userId, String recordId);

    /**
     * This method is used to get the list of UserReviews where matches the
     * recordIdentifier and contains recordRating and reviewText fields with
     * pagination
     *
     * @param recordIdentifier
     * @param rating
     * @param review
     * @param pageable
     * @return
     */
    @Query("{'recordIdentifier' : ?0 , '$or' : [{'recordRating' : {$exists :true}}, {'reviewText' : {$exists : true}}]}")
    public List<ReviewAndRating> findReviewsByRecordIdentifier(String recordIdentifier, Pageable pageable);

    /**
     * This method is used to get the list of UserReviews
     *
     * @param recordIdentifier
     * @return
     */
    @Query(value = "{ 'recordIdentifier' : ?0 }", fields = "{ 'userId' : 1}")
    public List<ReviewAndRating> findReviewByRecordIdentifier(String recordIdentifier);

}
