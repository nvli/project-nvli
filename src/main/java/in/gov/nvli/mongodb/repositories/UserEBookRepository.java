/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.repositories;

import in.gov.nvli.mongodb.domain.UserEBook;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Mongo Repository Interface for UserEbook domain which handles query method
 * like findBy..,countBy..,deleteBy.. etc
 *
 * @author Darshana
 */
public interface UserEBookRepository extends MongoRepository<UserEBook, ObjectId> {

    /**
     * This is used to get all ebooks by userId with pagination
     *
     * @param userId
     * @return List<UserEBook>
     */
    public List<UserEBook> findUserEBookByUserId(Long userId, Pageable pageable);

    /**
     * This is used to get all ebooks by userId.
     *
     * @param userId
     * @return List<UserEBook>
     */
    public List<UserEBook> findByUserId(Long userId);

    public List<UserEBook> deleteByUserIdAndRecordIdentifier(Long userId, String recordIdentifier);
}
