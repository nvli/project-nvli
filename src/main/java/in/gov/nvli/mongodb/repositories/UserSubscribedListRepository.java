/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.repositories;

import in.gov.nvli.mongodb.domain.UserSubscribedList;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ruturaj
 */
@Repository
public interface UserSubscribedListRepository extends MongoRepository<UserSubscribedList, ObjectId>{
    
    /**
     * 
     * @param subscribedUserId
     * @param userId
     * @param pageable
     * @return 
     */
    public List<UserSubscribedList> findUserSubscribedListBySubscribedUserId(Long subscribedUserId,Pageable pageable);
    
}
