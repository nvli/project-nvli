package in.gov.nvli.mongodb.repositories;

import in.gov.nvli.mongodb.domain.ActivityLog;
import java.util.List;
import java.util.Set;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Service;

/**
 * Mongo Repository Interface for ActivityLog domain which handles query method
 * like findBy..,countBy..,deleteBy.. etc
 *
 * @author Madhuri
 * @author Priya Bhalerao
 * @version 1
 * @since 1
 */
@Service
public interface ActivityLogRepository extends MongoRepository<ActivityLog, ObjectId> {

    /**
     * The method findByUserId is used to retrieve ActivityLog by Id with
     * pagination
     *
     * @param userId {@link  User}
     * @param pageable
     * @return
     */
    public List<ActivityLog> findByUserIdAndActivityTypeNot(Long userId, String activityType, Pageable pageable);

    /**
     * The method findByUserIdAndActivityTypeIn is used to retrieve ActivityLog
     * by userId and set of activity with pagination.
     *
     * @param userId {@link  User}
     * @param activityIds
     * @param pageable
     * @return
     */
    public List<ActivityLog> findByUserIdAndActivityTypeIn(Long userId, Set<String> activityIds, Pageable pageable);

    /**
     * The method findLikeByRecordIdentifier is used to get all likes by record
     * Id
     *
     * @param activityType {@link ActivityLog}
     * @param recordid
     * @return
     */
    @Query(value = "{'activity_type':?0,'recordIdentifier':?1}")
    public List<ActivityLog> findVisitedLInkByRecordIdentifier(String activityType, String recordid);

    /**
     * The method findByUsernameAndActivityTypeAndPermaLink is used to find
     * saved search by user username,type and link to which user is currently
     * visiting.
     *
     * @param userId
     * @param activity_type {@link ActivityLog}
     * @param permaLink
     * @return
     */
    @Query(value = "{'userId' : ?0,'activity_type':?1,'activity.permaLink':?2}")
    public ActivityLog findByUserIdAndActivityTypeAndPermaLink(Long userId, String activity_type, String permaLink);

    @Query(value = "{'username':?0,'activity_type':?1}")
    public List<ActivityLog> findFirst5ByUsernameAndActivityType(String username, String SAVED_PHRASE, Sort sort);

    @Query(value = "{'userId':?0,'activity_type':?1}")
    public List<ActivityLog> findActivityLogsByUserIdAndActivityType(Long userId, String SAVED_PHRASE, Pageable Pageable);
}
