package in.gov.nvli.mongodb.repositories;

import in.gov.nvli.mongodb.domain.Notifications;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Mongo Repository Interface for Notifications domain which handles query method
 * like findBy..,countBy..,deleteBy.. etc
 *
 * @author Darshana
 * @version 1
 * @since 1
 */
public interface NotificationsRepository extends MongoRepository<Notifications, ObjectId> {
    /**
     *  The method  findByRecieverId is used to find the records on the basis of reciverId
     * @param reciever_Id {@link ActivityLog}
     * @param pageable
     * @return 
     */

    public List<Notifications> findByRecieverId(Long reciever_Id, Pageable pageable);
    /**
     * The method countByRecieverIdAndIsReadByReceiver is used to show count of notifications and check weather is read by user or not
     * @param reciever_Id {@link ActivityLog}
     * @param isReadByReceiver {@link ActivityLog}
     * @return 
     */

    public Long countByRecieverIdAndIsReadByReceiver(Long reciever_Id, Boolean isReadByReceiver);

}
