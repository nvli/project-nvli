/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.repositories;

import in.gov.nvli.mongodb.domain.UserAddedList;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author ruturaj<ruturajp@cdac.in>
 */
@Service
public interface UserAddedListRepository extends MongoRepository<UserAddedList, ObjectId> {
    /**
     * 
     * @param userId
     * @param pageable
     * @return 
     */   
     public List<UserAddedList> findDistinctUserAddedListByUserId(Long userId,Pageable pageable);

    public List<UserAddedList> findUserAddedListsByUserIdAndIsUpdate(Long userId, Integer isUpdate,Pageable pageable);
      
}
