/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.service;

import in.gov.nvli.mongodb.domain.UserEBook;
import in.gov.nvli.mongodb.repositories.UserEBookRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Service class that calls UserEbook repository method
 *
 * @author Darshana
 */
@Service
public class UserEBookService {

    @Autowired
    UserEBookRepository userEBookRepository;

    public UserEBook saveObject(UserEBook object) {
        return userEBookRepository.save(object);
    }

    /**
     * The method getEBooksOfUserByUserId is used to get the userEbooks by
     * userId with pagination
     *
     * @param userId
     * @param pageNumber
     * @param pageWindow
     * @return
     */
    public List<UserEBook> getEBooksOfUserByUserId(Long userId, int pageNumber, int pageWindow) {
        List<UserEBook> eBooks = userEBookRepository.findUserEBookByUserId(userId, new PageRequest(pageNumber - 1, pageWindow, new Sort(Sort.Direction.DESC, "addedTime")));
        return eBooks != null ? eBooks : new ArrayList<>();

    }

    public List<UserEBook> findUserEBooksByUserId(Long userId) {
        return userEBookRepository.findByUserId(userId);
    }

    /**
     * The method deleteUserEBooksByUserIdAndRecordIdentifier is used to delete
     * the Ebook
     *
     * @param userId
     * @param recordIdentifier
     * @return
     */
    public List<UserEBook> deleteUserEBooksByUserIdAndRecordIdentifier(Long userId, String recordIdentifier) {
        return userEBookRepository.deleteByUserIdAndRecordIdentifier(userId, recordIdentifier);
    }

}
