package in.gov.nvli.mongodb.service;

import in.gov.nvli.mongodb.domain.ReviewAndRating;
import in.gov.nvli.mongodb.repositories.ReviewAndRatingRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Service class that calls ReviewAndRating repository method
 *
 * @author savitak
 */
@Service
public class ReviewAndRatingService {

    @Autowired
    ReviewAndRatingRepository userReviewAndRatingRepository;

    public ReviewAndRating saveObject(ReviewAndRating object) {
        return userReviewAndRatingRepository.save(object);
    }

    public ReviewAndRating getUserReviewByUserIdAndRecordIdentifier(Long userId, String recordId) {
        return userReviewAndRatingRepository.findByUserIdAndRecordIdentifier(userId, recordId);
    }

    public List<ReviewAndRating> findReviewsByRecordIdentifier(String recordIdentifier, int pageNumber, int pageWindow) {
        List<ReviewAndRating> userReviews = userReviewAndRatingRepository.findReviewsByRecordIdentifier(recordIdentifier, new PageRequest(pageNumber - 1, pageWindow, new Sort(Sort.Direction.DESC, "timeStamp")));
        return userReviews != null ? userReviews : new ArrayList<>();
    }

    public List<ReviewAndRating> getReviewByRecordIdentifier(String recordIdentifier) {
        List<ReviewAndRating> userReviews = userReviewAndRatingRepository.findReviewByRecordIdentifier(recordIdentifier);
        return userReviews != null ? userReviews : new ArrayList<>();
    }

    public ReviewAndRating getUserRatingsByUserIdAndRecordIdentifier(Long userId, String recordId) {
        return userReviewAndRatingRepository.findByUserIdAndRecordIdentifier(userId, recordId);
    }

}
