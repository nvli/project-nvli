/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.service;

import in.gov.nvli.mongodb.domain.UserAddedList;
import in.gov.nvli.mongodb.domain.UserSubscribedList;
import in.gov.nvli.mongodb.repositories.UserAddedListRepository;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 *
 * @author ruturaj<ruturajp@cdac.in>
 */
@Service
public class UserAddedListService {

    private static final Logger LOG = Logger.getLogger(UserAddedListService.class);

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    UserAddedListRepository userAddedListRepository;
    
    @Autowired
    UserSubscribedListService userSubscribedListService;

    public void save(UserAddedList addedList) {
        mongoTemplate.save(addedList);
    }

    public UserAddedList getListByUserIDListName(Long userId, String listName) {      
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("listName").is(listName));
         query.addCriteria(Criteria.where("isUpdate").is(0));      
        return mongoTemplate.findOne(query, UserAddedList.class);
    }

    public boolean checkIfResourceAlreadyInList(Long userId, String uniqueIdentifier, String listName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("recordIdentifier").is(uniqueIdentifier));
        query.addCriteria(Criteria.where("listName").is(listName));
        UserAddedList userAddedList = mongoTemplate.findOne(query, UserAddedList.class);
        return userAddedList == null;
    }

    /**
     * @author Ruturaj
     * @param listName name of list
     */
    public List<UserAddedList> getListContentByUserIdAndListName(long userId, String listName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("listName").is(listName));
        return mongoTemplate.find(query, UserAddedList.class);

    }

    /**
     *
     * @param listId
     * @return
     */
    public UserAddedList getListByUserIdAndObjectId(long userId, ObjectId listId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(listId));
        query.addCriteria(Criteria.where("userId").is(userId));
        return mongoTemplate.findOne(query, UserAddedList.class);
    }

    public List<UserAddedList> getSavedListsByUserPaginated(Long userId, int pageNumber, int pageWindow) {
        return userAddedListRepository.findUserAddedListsByUserIdAndIsUpdate(userId, 0, new PageRequest(pageNumber - 1, pageWindow, new Sort(Direction.DESC, "activity_ts")));
    }

    public JSONArray getListsByUserInJSonArrayFormat(List<UserAddedList> lists) {
        JSONArray listArray = new JSONArray();
      lists.stream().forEach(publicList->{
        JSONObject list=new JSONObject();
        list.put("listTitle",publicList.getListName());
        PrettyTime p = new PrettyTime();
        list.put("addedTime",p.format(publicList.getActivityTs()));
        list.put("authorId",publicList.getUserId());
        list.put("authorName",publicList.getUsername());
        list.put("profilePicUrl",publicList.getUserProfilePhotoPath());
        list.put("followersCount",publicList.getFollowerCount());
         JSONArray itemArray =new JSONArray();
     List<UserAddedList> userAddedLists= getListContentByUserIdAndListName(publicList.getUserId(),publicList.getListName());
     userAddedLists.stream().forEach(content->{
     JSONObject item=new JSONObject();
     item.put("itemUrl",content.getRecordIdentifier());
     item.put("itemName",content.getRecordTitle());
     itemArray.add(item);
     });
     list.put("itemArray",itemArray);
     listArray.add(list);            
    });
        return listArray;
    }
    
     public JSONArray getSubscribedListsByUserInJSonArrayFormat(List<UserSubscribedList> lists) {
        JSONArray listArray = new JSONArray();
      lists.stream().forEach(subList->{
        JSONObject list=new JSONObject();
        list.put("listTitle",subList.getListName());
        PrettyTime p = new PrettyTime();
        list.put("addedTime",p.format(subList.getSubscribedDate()));
        list.put("authorId",subList.getCreatedByUserId());
        list.put("authorName",subList.getCreatedByUserName());
        list.put("profilePicUrl",subList.getCreatedByProfilePicPath());
         JSONArray itemArray =new JSONArray();
     List<UserAddedList> userAddedLists= getListContentByUserIdAndListName(subList.getCreatedByUserId(),subList.getListName());
     userAddedLists.stream().forEach(content->{
     JSONObject item=new JSONObject();
     item.put("itemUrl",content.getRecordIdentifier());
     item.put("itemName",content.getRecordTitle());
     itemArray.add(item);
     });
     list.put("itemArray",itemArray);
     listArray.add(list);            
    });
        return listArray;
    }


    public UserAddedList getListByUserIdAndListName(Long userId, String originalListName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("listName").is(originalListName));
        query.addCriteria(Criteria.where("userId").is(userId));
        return mongoTemplate.findOne(query, UserAddedList.class);

    }

    public void updateUserAddedList(String originalListName, UserAddedList userAddedList) {
        Query query = new Query();
        query.addCriteria(Criteria.where("listName").is(originalListName));
        query.addCriteria(Criteria.where("userId").is(userAddedList.getUserId()));
        Update update = new Update();
        update.set("listName", userAddedList.getListName());
        update.set("publicPrivate", userAddedList.getPublicPrivate());
        mongoTemplate.updateMulti(query, update, UserAddedList.class);
    }

    public boolean deleteUserAddedListByUserIdAndListName(Long userId, String listName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("listName").is(listName));
        query.addCriteria(Criteria.where("userId").is(userId));
        List<UserAddedList> userAddedLists = mongoTemplate.findAllAndRemove(query, UserAddedList.class);
        return !userAddedLists.isEmpty();
    }

    public List<UserAddedList> gelAllSavedListOfUser(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("isUpdate").is(0));
        return mongoTemplate.find(query, UserAddedList.class);
    }

    public boolean deleteUserAddedListContentByUserIdAndListNameAndRecordId(Long userId, String listName, String recordIdentifier) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("listName").is(listName));
            query.addCriteria(Criteria.where("userId").is(userId));
            query.addCriteria(Criteria.where("recordIdentifier").is(recordIdentifier));
            List<UserAddedList> userAddedLists = mongoTemplate.findAllAndRemove(query, UserAddedList.class);
            return !userAddedLists.isEmpty();
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean IsListPresentByUserIDListName(Long userId, String newListName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("listName").is(newListName));
        UserAddedList userAddedList = mongoTemplate.findOne(query, UserAddedList.class);
        return userAddedList != null;
    }

    public List<UserAddedList> getPublicLists(int pageNumber, int pageWindow,Long loggedUserId) {
        PageRequest pageable=new PageRequest(pageNumber-1, pageWindow,new Sort(Direction.DESC,"followerCount"));
       List<UserSubscribedList> userSubscribedLists = userSubscribedListService.getSubscribedListOfUser(loggedUserId);
        List<Criteria> criterias = new ArrayList<Criteria>();
        userSubscribedLists.stream().forEach(userSubscribedList->{
          criterias.add(new Criteria().orOperator(
         Criteria.where("userId").ne(userSubscribedList.getCreatedByUserId()),
         Criteria.where("listName").ne(userSubscribedList.getListName())
         ));
        });
         Query query = new Query();
         query.addCriteria(Criteria.where("isUpdate").is(0));
         query.addCriteria(Criteria.where("publicPrivate").is(0));
         query.addCriteria(Criteria.where("userId").ne(loggedUserId));
         if(criterias.size()!=0){
        query.addCriteria(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()])));
         }
        query.with(pageable);
        return mongoTemplate.find(query, UserAddedList.class);
    }

    public void updateUserAddedListFollowersCount(String listName, Long createdById, int followersCount) {
         Update update=new Update();
         update.set("followerCount",followersCount);
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(createdById));
        query.addCriteria(Criteria.where("listName").is(listName));
        query.addCriteria(Criteria.where("isUpdate").is(0));
        mongoTemplate.updateFirst(query,update, UserAddedList.class);
     }

    public void increamentSubscriptionCountOfList(String listName, Long createdById) {
        UserAddedList userAddedList=getListByUserIDListName(createdById, listName);
        int followersCount;
        if(userAddedList.getFollowerCount()==null)
           followersCount=1;
        else
           followersCount=userAddedList.getFollowerCount()+1;
        updateUserAddedListFollowersCount(listName,createdById,followersCount);

    }

    public void decrementSubscriptionCountOfList(String listName, Long createdById) {
    UserAddedList userAddedList=getListByUserIDListName(createdById, listName);
        int followersCount;
        if(userAddedList.getFollowerCount()==null)
           followersCount=0;
        else
           followersCount=userAddedList.getFollowerCount()-1;
        updateUserAddedListFollowersCount(listName,createdById,followersCount);
    }

}
