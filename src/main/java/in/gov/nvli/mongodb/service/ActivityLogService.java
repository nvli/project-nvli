package in.gov.nvli.mongodb.service;

import com.google.common.collect.Lists;
import com.mongodb.WriteResult;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.ActivityLog;
import in.gov.nvli.mongodb.domain.UserActivity;
import in.gov.nvli.mongodb.domain.UserAddedList;
import in.gov.nvli.mongodb.repositories.ActivityLogRepository;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.util.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * Service class that calls activity log repository method
 *
 * @author Madhuri
 * @since 1
 * @version 1
 */
@Service
public class ActivityLogService {

    @Autowired
    MongoOperations mongoOperations;
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    private ActivityLogRepository activityLogRepository;
    @Autowired
    private UserAddedListService userAddedListService;
    /**
     * The name of the property activityServiceObj used to holds
     * UserActivityService object
     */
    @Autowired
    private UserActivityService activityServiceObj;

    /**
     * the method saveObject save all the entities
     *
     * @param object
     */
    public ActivityLog saveObject(ActivityLog object) {
        return activityLogRepository.save(object);
    }

    /**
     * The method getAllLogsOfUser is used to get all logs of user with
     * pagination and sort by timestamp in descending order
     *
     * @param userId {@link User}
     * @param pageNumber
     * @param pageWindow
     * @return list of all logs of user
     */
    public List<ActivityLog> getAllLogsOfUser(Long userId, int pageNumber, int pageWindow) {
        PageRequest pageable = new PageRequest(pageNumber - 1, pageWindow, new Sort(Sort.Direction.DESC, "activity.activity_ts"));
        List<ActivityLog> activityLogs = activityLogRepository.findByUserIdAndActivityTypeNot(userId, Constants.UserActivityConstant.LOGIN, pageable);
        return (activityLogs != null ? activityLogs : new ArrayList<>());
    }

    /**
     * The method getFilteredActivityLogListOfUser is used to get logs of user
     * with pagination sort by activity_ts in descending order
     *
     * @param pageNumber
     * @param pageWindow
     * @param userId {@link User}
     * @param activityIds
     * @return
     */
    public List<ActivityLog> getFilteredActivityLogListOfUser(int pageNumber, int pageWindow, long userId, Set<String> activityIds) {
        PageRequest pageable
                = new PageRequest(pageNumber - 1, pageWindow, new Sort(Sort.Direction.DESC, "activity.activity_ts"));
        List<ActivityLog> activityLogs
                = activityLogRepository.findByUserIdAndActivityTypeIn(userId, activityIds, pageable);
        return (activityLogs != null ? activityLogs : new ArrayList<>());
    }

    public List<ActivityLog> getRewardActivityLogListOfUser(int pageNumber, int pageWindow, long userId) {
        Set<String> activityIds
                = new HashSet<>(Arrays.asList(
                        Constants.CrowdSourceActivity.EDIT_METADATA,
                        Constants.CrowdSourceActivity.ADD_CUSTOM_TAG,
                        Constants.CrowdSourceActivity.ADD_UDC_TAG));
        return this.getFilteredActivityLogListOfUser(pageNumber, pageWindow, userId, activityIds);
    }

    /**
     * @author:Ruturaj This methods returns list of visited link.
     * @param recordId
     * @return
     */
    public List<ActivityLog> getVisitedLinkLogByRecordId(String recordId) {
        return activityLogRepository.findVisitedLInkByRecordIdentifier(Constants.UserActivityConstant.LINK_VISITED, recordId);
    }

    /**
     * @param userId
     * @author:Ruturaj--This method is used to chech if link which is currently
     * visited by user is already saved or not
     * @param link
     * @return String "notSaved" when link is not saved already and if saved
     * already then returns the savedPhrase for that link.
     */
    public String checkIfLinkAlreadySaved(Long userId, String link) {
        ActivityLog activityLog = activityLogRepository.findByUserIdAndActivityTypeAndPermaLink(
                userId, Constants.UserActivityConstant.SAVED_PHRASE, link);
        return activityLog == null ? "notSaved" : ((UserActivity) activityLog.getActivity()).getSearchPhrase();
    }

    /**
     * @author:Ruturaj--This method is used to get all savedPhrases by
     * user(currently unused)
     * @param username
     * @return List<ActivityLog> see {@link ActivityLog}
     */
    public List<ActivityLog> getSavedSearchPhrasesOfUser(String username) {
        Sort sort = new Sort(Direction.DESC, "activity.activity_ts");
        List<ActivityLog> activityLogs = activityLogRepository.findFirst5ByUsernameAndActivityType(
                username, Constants.UserActivityConstant.SAVED_PHRASE, sort);
        return activityLogs;
    }

    /**
     * @author:Ruturaj--This method is used to retrive saved searches by user
     * with pagination.
     * @param userId
     * @param pageNumber
     * @param pageWindow
     * @return List<ActivityLog> see {@link ActivityLog}
     */
    public List<ActivityLog> getSavedSearchPhrasesOfUserPaginated(Long userId, int pageNumber, int pageWindow) {
        Sort sort = new Sort(Direction.DESC, "activity.activity_ts");
        PageRequest pageable = new PageRequest(pageNumber - 1, pageWindow, sort);
        List<ActivityLog> activityLogs = activityLogRepository.findActivityLogsByUserIdAndActivityType(
                userId, Constants.UserActivityConstant.SAVED_PHRASE, pageable);
        return activityLogs != null ? Lists.newArrayList(activityLogs.iterator()) : new ArrayList<>();
    }

    public ActivityLog getActivityLogBySearchedPhraseIdInSession(ObjectId userSearchedPhraseIdInSession) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(userSearchedPhraseIdInSession));
        return mongoTemplate.findOne(query, ActivityLog.class);
    }

    /**
     *
     * @param userId
     * @return
     */
    public List<ActivityLog> gelAllSavedListOfUser(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("activity_type").is(Constants.UserActivityConstant.CREATE_LIST));
        return mongoTemplate.find(query, ActivityLog.class);      //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Method to save log when user deleted his created list.
     *
     * @param user
     * @param listName
     * @param request
     */
    public void saveLogOfDeleteList(User user, String listName, HttpServletRequest request) {
        UserAddedList userAddedList = userAddedListService.getListByUserIDListName(user.getId(), listName);
        if (userAddedList != null) {
            activityServiceObj.saveActivityLog(userAddedList.getRecordIdentifier(),
                    user.getId(), Constants.UserActivityConstant.DELETE_LIST, userAddedList,
                    request, user.getUsername(), null, Constants.FilterActivityLogs.ListActivities, null);
        }

    }

    /**
     * Method to save log when user update particular list;
     *
     * @param user
     * @param userAddedList
     * @param request
     */
    public void saveLogOfUpdateList(User user, UserAddedList userAddedList, HttpServletRequest request) {
        activityServiceObj.saveActivityLog(userAddedList.getRecordIdentifier(),
                user.getId(), Constants.UserActivityConstant.UPDATE_LIST, userAddedList,
                request, user.getUsername(), null, Constants.FilterActivityLogs.ListActivities, null);
    }

    public void saveLogOfDeleteListContent(User user, String listName, String recordIdentifier, String recordTitle, HttpServletRequest request) {
        UserAddedList userAddedList = userAddedListService.getListByUserIDListName(user.getId(), listName);
        activityServiceObj.saveActivityLog(recordIdentifier,
                user.getId(), Constants.UserActivityConstant.REMOVE_FROM_LIST, userAddedList,
                request, user.getUsername(), null, Constants.FilterActivityLogs.ListActivities, null);
    }

    public List<ActivityLog> getEditMetadataActivityLogByCrowdSourceId(Long crowdSourceId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("activity.crowdSourceId").is(crowdSourceId));
        query.addCriteria(Criteria.where("activity_type").is(Constants.CrowdSourceActivity.EDIT_METADATA));
        return mongoTemplate.find(query, ActivityLog.class);
    }

    public List<ActivityLog> getUDCActivityLogByCrowdSourceId(Long crowdSourceId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("activity.crowdSourceId").is(crowdSourceId));
        query.addCriteria(Criteria.where("activity_type").is(Constants.CrowdSourceActivity.ADD_UDC_TAG));
        return mongoTemplate.find(query, ActivityLog.class);
    }
}
