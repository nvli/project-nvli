/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.service;

import in.gov.nvli.listener.CustomRetryListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

/**
 *
 * @author Doppa Srinivas
 */
@Configuration
@EnableRetry
public class RetryTemplateService {

    @Value("${mongoDB.retry-max-attempts}")
    private int maxAttempts;
    @Bean
    public RetryTemplate retryTemplate() {
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(maxAttempts);
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        //backOffPolicy.setBackOffPeriod(5); //Default value is 1000ms.
        RetryTemplate template = new RetryTemplate();
        template.setRetryPolicy(retryPolicy);
        template.setBackOffPolicy(backOffPolicy);
        template.registerListener(new CustomRetryListener());
        return template;
    }
}
