package in.gov.nvli.mongodb.service;

import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.Notifications;
import in.gov.nvli.mongodb.repositories.NotificationsRepository;
import in.gov.nvli.service.NotificationSubscriptionService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * service class used to call notification Repository methods
 *
 * @author Suman Behara<sumanb@cdac.in>
 * @author Darshana
 * @version 1
 * @version 1
 *
 */
@Service
public class NotificationService {

    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    @Qualifier(value = "mongoTemplate")
    MongoOperations mongoOperations;
    @Autowired
    private NotificationsRepository notificationsRepository;
    /**
     * the name of this property userService is used to hold UserService object
     */
    @Autowired
    UserService userService;

    /**
     * The method saveNotification is used to save the notifications for all the
     * activities
     *
     * @param notifications {@link Notifications}
     * @param receivers
     */
    public void saveNotification(Notifications notifications, List<Long> receivers) {
        User currentUser = userService.getCurrentUser();
        if (!receivers.isEmpty()) {
            receivers.parallelStream().forEach(receiver -> {
                Notifications notification = new Notifications();
                notification.setNotifactionDateTime(new Date());

                notification.setNotificationMessage(notifications.getNotificationMessage());
                notification.setNotificationTargetURL(notifications.getNotificationTargetURL());
                notification.setIsDeletedByUser(Boolean.FALSE);
                notification.setIsReadByReceiver(Boolean.FALSE);
                notification.setSenderId(currentUser.getId());
                notification.setNotificationActivityType(notifications.getNotificationActivityType());
                notification.setRecieverId(receiver);

                this.sendNotification(Constants.Notification.DEFAULT_NOTIFICATION_URL + receiver, notification.getNotificationMessage());
                notificationsRepository.save(notification);
            });
        }
    }

    /**
     *
     * @param receiver_Id {@link Notifications}
     * @param maxPerPage
     * @param pageNo
     * @return
     */
    public List<Notifications> getUserNotifsByUser(long receiver_Id, int maxPerPage, int pageNo) {
        try {
            return notificationsRepository.findByRecieverId(receiver_Id, new PageRequest(pageNo - 1, maxPerPage, new Sort(Sort.Direction.DESC, "notifactionDateTime")));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }
    /**
     * The method getUnreadNotifCount is used to show the unread notification
     * count on dashboard
     *
     * @param reciever_Id {@link Notifications}
     * @param isReadByReceiver {@link Notifications}
     * @return hash map
     */
    public HashMap<String, Object> getUnreadNotifCount(Long reciever_Id, Boolean isReadByReceiver) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("unread_count", notificationsRepository.countByRecieverIdAndIsReadByReceiver(reciever_Id, isReadByReceiver));
        return hashMap;
    }

    /**
     * The method markNotificationAsRead is used to mark the notification as
     * read which is seen by user
     *
     * @param notificationId
     * @return
     */
    public boolean markNotificationAsRead(String notificationId) {
        Notifications notification = notificationsRepository.findOne(new ObjectId(notificationId));
        notification.setIsReadByReceiver(true);
        Notifications dbNotificationInstance = notificationsRepository.save(notification);
        return dbNotificationInstance != null;
    }

    /**
     * The method sendNotification sends the notifications to the users who also
     * visited the same record
     *
     * @param url
     * @param message
     */
    public void sendNotification(String url, String message) {
        this.template.convertAndSend(url, message);
    }
}
