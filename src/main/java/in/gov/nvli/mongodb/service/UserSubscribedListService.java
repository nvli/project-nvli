/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.mongodb.service;

import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.UserSubscribedList;
import in.gov.nvli.mongodb.repositories.UserSubscribedListRepository;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ruturaj
 */
@Service
public class UserSubscribedListService {

    private static final Logger LOG = Logger.getLogger(UserAddedListService.class);

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    UserSubscribedListRepository userSubscribedListRepository;

    public void saveSubscribedList(String listName, User creator, User subscribedUser) {
        UserSubscribedList userSubscribedList = new UserSubscribedList(listName, subscribedUser.getId(), subscribedUser.getFirstName() + " "
                + subscribedUser.getLastName(), creator.getId(), creator.getUsername(),creator.getProfilePicPath(), new Date());
        mongoTemplate.save(userSubscribedList);
    }

    public List<UserSubscribedList> getSubscribedListOfUser(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("subscribedUserId").is(userId));
        return mongoTemplate.find(query, UserSubscribedList.class);
    }

    public List<UserSubscribedList> getSubscribedListOfUser(Long userId, int pageNumber, int pageWindow) {
        return userSubscribedListRepository.findUserSubscribedListBySubscribedUserId(userId, new PageRequest(pageNumber - 1, pageWindow, new Sort(Sort.Direction.DESC, "subscribed_date")));
    }

    public void unSubscribeSubscribedList(String listName, Long createdByUserId, String createdByUsername, User user) {
        Query query = new Query();
        query.addCriteria(Criteria.where("listName").is(listName));
        query.addCriteria(Criteria.where("createdByUserId").is(createdByUserId));
        query.addCriteria(Criteria.where("subscribedUserId").is(user.getId()));
        mongoTemplate.remove(query, UserSubscribedList.class);
    }

    boolean isAlreadySubscribedToList(Long createdByUserId, String listName, Long subscribedUserId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("createdByUserId").is(createdByUserId));
        query.addCriteria(Criteria.where("listName").is(listName));
        query.addCriteria(Criteria.where("subscribedUserId").is(subscribedUserId));
        return mongoTemplate.findOne(query, UserSubscribedList.class) != null;
    }
}
