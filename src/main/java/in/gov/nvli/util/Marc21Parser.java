package in.gov.nvli.util;

import in.gov.nvli.domain.marc21.CollectionType;
import in.gov.nvli.domain.marc21.ControlFieldType;
import in.gov.nvli.domain.marc21.DataFieldType;
import in.gov.nvli.domain.marc21.RecordType;
import in.gov.nvli.domain.marc21.SubfieldatafieldType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author Ritesh Malviya
 */
public class Marc21Parser {

    /**
     * This method is used to get marc21 metadata as Map (Key as Marc21 tag name
     * and Value as List of corresponding values).
     *
     * @param collectionType
     * @return return Map,key as Marc21 tag name and value as List of
     * corresponding values
     * @throws java.lang.Exception
     */
    public static Map<String, List<String>> getValuesAsMap(CollectionType collectionType) throws Exception {
        Map<String, List<String>> marc21XmlTagValueMap = new HashMap<>();

        RecordType recordType = collectionType.getRecord().get(0);
        String tagName;
        List<String> valueList;
        for (ControlFieldType controlField : (List<ControlFieldType>) recordType.getControlfield()) {
            tagName = controlField.getTag() + "_";

            if (marc21XmlTagValueMap.containsKey(tagName)) {
                valueList = marc21XmlTagValueMap.get(tagName);
            } else {
                valueList = new ArrayList<>();
            }

            valueList.add(StringEscapeUtils.unescapeXml(controlField.getValue()));
            marc21XmlTagValueMap.put(tagName, valueList);
        }

        String tagSubfieldName;
        for (DataFieldType dataField : (List<DataFieldType>) recordType.getDatafield()) {
            tagName = dataField.getTag();
            for (SubfieldatafieldType subfield : (List<SubfieldatafieldType>) dataField.getSubfield()) {
                tagSubfieldName = tagName + "_" + subfield.getCode();

                if (marc21XmlTagValueMap.containsKey(tagSubfieldName)) {
                    valueList = marc21XmlTagValueMap.get(tagSubfieldName);
                } else {
                    valueList = new ArrayList<>();
                }

                valueList.add(StringEscapeUtils.unescapeXml(subfield.getValue()));
                marc21XmlTagValueMap.put(tagSubfieldName, valueList);
            }
        }
        return marc21XmlTagValueMap;
    }
}
