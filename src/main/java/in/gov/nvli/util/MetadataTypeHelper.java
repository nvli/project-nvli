package in.gov.nvli.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.beans.crowdsource.ArtistProfile;
import in.gov.nvli.beans.crowdsource.CuisineDetail;
import in.gov.nvli.beans.crowdsource.MetadataBean;
import in.gov.nvli.domain.dublincore.DublinCoreMetadata;
import in.gov.nvli.domain.marc21.CollectionType;
import in.gov.nvli.domain.timecodemarker.Content;
import java.io.IOException;

/**
 * This class is used to Metadata type mapper.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
public class MetadataTypeHelper {

    /**
     * This method used to convert any object to string.
     *
     * @param object Set any object
     * @return String
     * @throws JsonProcessingException
     */
    public static String objectToJSONString(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        // to exclude null and non empty values.
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        return mapper.writeValueAsString(object);
    }

    /**
     * This method used to map json string to {@link MetadataBean} using
     * metadata standard type.
     *
     * @param metadataStandardType Set metadata standard type
     * @param metadataJSONString Set json string
     * @return object of {@link MetadataBean}
     * @throws IOException
     */
    public static MetadataBean getMetadataBeanFromMetadataJSONString(String metadataStandardType, String metadataJSONString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        MetadataBean metadataBean = new MetadataBean();
        metadataBean.setMetadataType(metadataStandardType);
        switch (metadataStandardType) {
            case Constants.MetadataStandardType.DUBLIN_CORE:
                DublinCoreMetadata dcMetadata = mapper.readValue(metadataJSONString, DublinCoreMetadata.class);
                metadataBean.setDcMetadata(dcMetadata);
                break;
            case Constants.MetadataStandardType.MARC21:
                CollectionType collectionType = mapper.readValue(metadataJSONString, CollectionType.class);
                metadataBean.setCollectionType(collectionType);
                break;
        }
        return metadataBean;
    }

    /**
     * This method used to map json string to {@link MetadataBean}.
     *
     * @param metadataJSONString Set json string
     * @return object of {@link MetadataBean}
     * @throws Exception
     */
    public static MetadataBean getMetadataBeanFromMetadataJSONString(String metadataJSONString) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(metadataJSONString, MetadataBean.class);
    }

    /**
     * This method used to map Object to {@link Content}.
     *
     * @param object Set object
     * @return {@link Content}
     * @throws Exception
     */
    public static Content getContentFromObject(Object object) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(mapper.writeValueAsString(object), Content.class);
    }

    /**
     * This method used to map Object to {@link String}.
     *
     * @param object Set object
     * @return {@link String}
     * @throws Exception
     */
    public static String getStringFromObject(Object object) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(mapper.writeValueAsString(object), String.class);
    }

    /**
     * This method used to map json string to {@link ArtistProfile}.
     *
     * @param metadataJSONString Set json string
     * @return object of {@link ArtistProfile}
     * @throws IOException
     */
    public static ArtistProfile getArtistProfileFromMetadataJSONString(String metadataJSONString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(metadataJSONString, ArtistProfile.class);
    }

    /**
     * This method used to map json string to {@link CuisineDetail}.
     *
     * @param metadataJSONString Set json string
     * @return object of {@link CuisineDetail}
     * @throws IOException
     */
    public static CuisineDetail getIndianRecipeFromMetadataJSONString(String metadataJSONString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(metadataJSONString, CuisineDetail.class);
    }
}
