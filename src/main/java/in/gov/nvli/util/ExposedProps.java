package in.gov.nvli.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Component
public class ExposedProps {

    @Value("${social.facebook.appid}")
    private String fbAppID;

    @Value("${application.url}")
    private String applicationUrl;

    @Value("${cdn1.base.url}")
    private String nvliCDN1;

    @Value("${cdn2.base.url}")
    private String nvliCDN2;

    @Value("${user.image.base.url}")
    private String userImageBase;

    @Value("${theme.base.url}")
    private String themeBase;

    @Value("${articles.image.base.url}")
    private String articleImageBase;
    
    @Value("${wiki.base.url}")
    private String wikiBaseURL;

    public String getWikiBaseURL() {
        return wikiBaseURL;
    }

    @Value("${user.parent.directory}")
    private String userFilesParentDir;
    
    public String getApplicationUrl() {
        return applicationUrl;
    }

    public String getFbAppID() {
        return fbAppID;
    }

    public String getNvliCDN1() {
        return nvliCDN1;
    }

    public String getNvliCDN2() {
        return nvliCDN2;
    }

    public String getUserImageBase() {
        return userImageBase;
    }

    public String getThemeBase() {
        return themeBase;
    }

    public void setThemeBase(String themeBase) {
        this.themeBase = themeBase;
    }

    public String getArticleImageBase() {
        return articleImageBase;
    }

    public String getUserFilesParentDir() {
        return userFilesParentDir;
    }

}
