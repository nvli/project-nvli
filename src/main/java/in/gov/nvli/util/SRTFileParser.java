package in.gov.nvli.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

public class SRTFileParser {

    public static Map<Integer, String> parseFile(InputStream is, String filterString) throws IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        int captionNumber = 1;
        boolean allGood;
        //first lets load the file
        InputStreamReader in = new InputStreamReader(is, "UTF8");
        BufferedReader br = new BufferedReader(in);
        String line = br.readLine();
        line = line.replace("\uFEFF", ""); //remove BOM character
        line = line.replace("ï»¿", "");
        Time startTime = null;
        Map<Integer, String> timecodeMap = new TreeMap<Integer, String>();
        try {
            while (line != null) {
                line = line.trim();
                //if its a blank line, ignore it, otherwise...
                if (!line.isEmpty()) {
                    allGood = false;
                    //the first thing should be an increasing number
                    try {
                        int num = Integer.parseInt(line);
                        if (num != captionNumber) {
                            throw new Exception();
                        } else {
                            captionNumber++;
                            allGood = true;
                        }
                    } catch (Exception e) {
                        //e.printStackTrace();
                        allGood = true;
                    }
                    if (allGood) {
                        //we go to next line, here the begin and end time should be found
                        try {
                            line = br.readLine().trim();
                            startTime = new Time("hh:mm:ss,ms", line.substring(0, 12));
                        } catch (Exception e) {
                            e.printStackTrace();
                            allGood = false;
                        }

                        //we go to next line where the caption text starts
                        line = br.readLine().trim();
                        String text = "";
                        while (!line.isEmpty()) {
                            text += line + " ";
                            line = br.readLine().trim();
                        }
                        //for first time
                        if(filterString.equalsIgnoreCase("all"))
                            timecodeMap.put(startTime.getMseconds(), text + Constants.STRING_SEPARATOR + startTime.getTime("hh:mm:ss.ms"));
                        else if (text.toLowerCase().contains(filterString.toLowerCase())) 
                            timecodeMap.put(startTime.getMseconds(), text.replaceAll(filterString, "<b>"+filterString+"</b>").replaceAll(filterString.toLowerCase(), "<b>"+filterString.toLowerCase()+"</b>").replaceAll(filterString.toUpperCase(), "<b>"+filterString.toUpperCase()+"</b>").replaceAll(filterString.substring(0,1).toUpperCase()+(filterString.length()>1 ? filterString.substring(1).toLowerCase() : ""), "<b>"+filterString.substring(0,1).toUpperCase()+(filterString.length()>1 ? filterString.substring(1).toLowerCase() : "")+"</b>") + Constants.STRING_SEPARATOR + startTime.getTime("hh:mm:ss.ms"));
                    }
                    //we go to next blank
                    while (!line.isEmpty()) {
                        line = br.readLine().trim();
                    }
                }
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //we close the reader
            is.close();
        }
        return timecodeMap;
    }
}
