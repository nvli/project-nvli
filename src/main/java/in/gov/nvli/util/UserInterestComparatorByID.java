/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.util;

import in.gov.nvli.domain.user.UserInterest;
import java.util.Comparator;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
public class UserInterestComparatorByID implements Comparator<UserInterest> {

    @Override
    public int compare(UserInterest o1, UserInterest o2) {
        return Integer.parseInt(o1.getId() + "") - Integer.parseInt(o2.getId() + "");
    }

}
