package in.gov.nvli.util;

import java.util.List;

/**
 * calculate the similarity between two sentences. it is based on purely on
 * words positions. it doesn't take consideration context into the account.
 *
 * To score the overlap we use a new scoring mechanism that differentiates
 * between N-single words and N-consecutive word overlaps and effectively treats
 * each gloss as a bag of words. It is based on ZipF's Law, which says that the
 * length of words is inversely proportional to their usage. The shortest words
 * are those which are used more often, the longest ones are used less often.
 *
 * Measuring overlaps between two strings is reduced to solve the problem of
 * finding the longest common sub-string with maximal consecutives. Each overlap
 * which contains N consecutive words, contributes N^2 to the score of the gloss
 * sense combination. For example: an overlap "ABC" has a score of 3^2=9 and two
 * single overlaps "AB" and "C" has a score of 2^2 + 1^1=5.
 *
 * TODO 1.use stopwords and stemming for better performance 2. use iterative
 * approach instead of recursion
 *
 * @author Gajraj
 */
public class StatementsSimilarityUtils {

    /**
     *
     * @param s1
     * @param s2
     * @return
     */
    public static int similarityScore(String s1, String s2) {
        return similarityScore(s1.split("\\s"), s2.split("\\s"));
    }

    /**
     *
     * @param s1
     * @param s2
     * @return
     */
    public static int similarityScore(String[] s1, String[] s2) {
        return similarityScore(s1, 0, s2, 0);
    }

    /**
     *
     * @param s1
     * @param s2
     * @return
     */
    public static int similarityScore(List<String> s1, List<String> s2) {
        return similarityScore(s1, 0, s2, 0);
    }

    /**
     * TODO use
     *
     * @param s1
     * @param index1
     * @param s2
     * @param index2
     * @return
     */
    public static int similarityScore(String[] s1, int index1, String[] s2, int index2) {
        int match = 0;
        while (index1 < s1.length && index2 < s2.length && s1[index1].equalsIgnoreCase(s2[index2])) {
            match++;
            index1++;
            index2++;
        }
        int value = 0;
        if (index1 < s1.length && index2 < s2.length) {
            value = Math.max(similarityScore(s1, index1, s2, index2 + 1),
                    similarityScore(s1, index1 + 1, s2, index2));
        }
        return match * match + value;
    }

    /**
     *
     * @param s1
     * @param index1
     * @param s2
     * @param index2
     * @return
     */
    public static int similarityScore(List<String> s1, int index1, List<String> s2, int index2) {
        int match = 0;
        while (index1 < s1.size() && index2 < s2.size() && s1.get(index1).equalsIgnoreCase(s2.get(index2))) {
            match++;
            index1++;
            index2++;
        }
        int value = 0;
        if (index1 < s1.size() && index2 < s2.size()) {
            value = Math.max(similarityScore(s1, index1, s2, index2 + 1),
                    similarityScore(s1, index1 + 1, s2, index2));
        }
        return match * match + value;
    }

    /**
     *
     * @param source
     * @param target
     * @return
     */
    public static double calculatePercSim(List<String> source, List<String> target) {
        return Math.sqrt(similarityScore(source, target)) / (double) source.size();
    }

    /**
     *
     * @param source
     * @param target
     * @return
     */
    public static double calculatePercSim(String source, String target) {
        return Math.sqrt(similarityScore(source, target)) / (double) source.split("\\s").length;
    }
}
