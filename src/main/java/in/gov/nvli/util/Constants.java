package in.gov.nvli.util;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 *
 * @author Sujata Aher
 * @author Priya More
 * @since 1
 * @version 1
 */
public class Constants {

    /**
     * NVLI portal table prefix.
     *
     * @since 1
     */
    public final static String PORTAL_TABLE_PREFIX = "pr_";
    /**
     * NVLI Portal Table Names
     */
    public final static String PORTAL_USER_TABLE_NAME = "user";
    public final static String PORTAL_CATALOG_NAME = "test_nvli";
    public final static String PORTAL_WIDGET_TABLE_NAME = "widget";
    public final static String PORTAL_CALEVENT_TABLE_NAME = "calendar_event";

    /**
     * Properties for user activity logging
     */
    public static final class UserActivities {

        public static final int LOGIN = 1;
        public static final int PASSWORD_CHANGE = 2;
        public static final int SIGNUP = 3;
        public static final int ACTIVATE_USER = 4;
        public static final int DEACTIVATE_USER = 5;
        public static final int DELETE_USER = 6;
        public static final int EDIT_USER_DETAILS = 7;
        public static final int UPDATE_PROFILE = 8;
        public static final int INVITE_USER = 9;
        public static final int LIKE_RECORD = 10;
        public static final int SOCIAL_SHARE_RECORD = 11;
        public static final int INTRA_SHARE_RECORD = 12;
        public static final int EMAIL_RECORD = 13;
        public static final int DOWNLOAD_RECORD = 14;
        public static final int EXPORT_RECORD = 15;
        public static final int ADD_TO_LIST = 16;
        public static final int REMOVE_FROM_LIST = 17;
        public static final int CREATE_LIST = 18;
        public static final int DELETE_LIST = 19;
        public static final int ADD_REVIEW = 20;
        public static final int UPDATE_REVIEW = 21;
        public static final int DELETE_REVIEW = 22;
        public static final int RATE_RECORD = 23;
        public static final int KEYWORD_SEARCHED = 24;
        public static final int LINK_VISITED = 25;
        public static final int ADD_TAG = 26;
        public static final int REMOVE_TAG = 27;
        public static final int FACEBOOK_LOGIN = 28;
        public static final int TWITTER_LOGIN = 31;
        public static final int GOOGLE_LOGIN = 32;
        public static final int LINKEDIN_LOGIN = 33;
        public static final int GITHUB_LOGIN = 34;
        public static final int ADD_EBOOK = 29;
        public static final int SAVED_LINK = 35;
    }
    public static final String USER_SEARCHED_RECORDS = "userViditedLinks";
    public static final String USER_SEARCHED_PHRASE_ID = "searchedPhraseId";
    public static final String USER_SAVED_PHRASE_ID = "savedPhraseId";
    public static final String USER_PREV_SEARCHED_PHRASE = "userPreviousSearchPhrase";
    /**
     * property for deleted/non deleted
     */
    public static final int DELETED = 1;
    public static final int NON_DELETED = 0;
    /**
     * end of property for deleted/non deleted
     */
    /**
     * property for user saved/unsaved search
     */
    public static final int SAVED_SEARCH = 1;
    public static final int UNSAVED_SEARCH = 0;
    /**
     * end of property for user saved/unsaved search
     */
    /**
     * property for user active/inactive status i.e email verified or not
     */
    public static final byte USER_ACTIVE = 1;
    public static final byte USER_INACTIVE = 0;
    /**
     * end of property for user active/inactive status i.e email verified or not
     */
    /**
     * property for user deleted/non-deleted status
     */
    public static final byte USER_DELETED = 1;
    public static final byte USER_NOT_DELETED = 0;
    /**
     * end of property for user deleted/non-deleted status
     */
    /**
     * property for user activated/deactivated user
     */
    public static final byte USER_DEACTIVATED = 0;
    public static final byte USER_ACTIVATED = 1;
    /**
     * end of property for user activated/deactivated user
     */
    /**
     * property for list visibility
     */
    public static final int PUBLIC_LIST_ID = 0;
    public static final int PRIVATE_LIST_ID = 1;
    public static final String PUBLIC_LIST_LABEL = "Public";
    public static final String PRIVATE_LIST_LABEL = "Private";
    /**
     * end of property for list visibility
     */
    /**
     * property for user invitation_accepted or not
     */
    public static final int INVITE_NOT_ACCEPTED = 0;
    public static final int INVITE_ACCEPTED = 1;
    public static final String INVITE_NOT_ACCEPTED_STR = "Pending";
    public static final String INVITE_ACCEPTED_STR = "Registered";
    public static final String INVITE_CANCELLED_STR = "Cancelled";
    public static final int INVITE_CANCELLED = 2;
    /**
     * end of property for user invitation_accepted or not
     */
    public static final String REG_EXP_FOR_EXTENSION = "\\.(?=[^\\.]+$)";

    /**
     * User Roles
     */
    public static final class UserRole {

        public static final String ANONYMOUS_USER = "ANONYMOUS";
        public static final String ADMIN_USER = "ADMIN_USER";
        public static final String GENERAL_USER = "GENERAL_USER";
        public static final long GENERAL_USER_ID = 1;
        public static final String LIB_USER = "LIB_USER";
        public static final String MOC_USER = "MOC_USER";
        public static final String TRANSLATOR_USER = "TRANSLATOR_USER";
        public static final String CURATOR_USER = "CURATOR_USER";
        public static final String METADATA_ADMIN = "METADATA_ADMIN";
    }
    public static final String MARKER_NAME = "[a-zA-Z0-9.? _-]+";
    public static final String TIME_CODE = "([0-9]+):([0-5][0-9]):([0-5][0-9]).([0-9]{1,3})";

    /**
     * Meta-data type constants
     */
    public static final class MetadataStandardType {

        public final static String MARC21 = "marc21";
        public final static String DUBLIN_CORE = "dublin-core";
        public final static String ARTIST_PROFILE = "artist-profile";
        public final static String INDIAN_RECIPES = "indian-recipes";
    }

    /**
     * Tag Type
     */
    public static final class TagType {

        public static final String UDC = "UDCTagging";
        public static final String CUSTOM = "CustomTagging";
        public static final String MARKER = "TimecodeMarking";
    }

    /**
     * AJAX Response Status CODES
     */
    public static final class AjaxResponseCode {

        public static final String SUCCESS = "success";
        public static final String FAILED = "failed";
        public static final String ACCESS_DENIED = "access_denied";
        public static final String ERROR = "error";
        public static final String INPUT_ERROR = "input_error";
    }

    /**
     * Content Type
     */
    public static final class ContentType {

        public static final String AUDIO = "audio";
        public static final String VIDEO = "video";
        public static final String JPEG = "jpeg";
        public static final String TIFF = "tif";
        public static final String DOCUMENT = "document";
    }
    public static final Map GROUP_WITH_CLASS = new HashMap<String, String>() {

        {
            put("Management", "mgt");
            put("Agriculture", "agri");
            put("Technology", "tech");
            put("Science", "sci");
            put("Economics", "economy");
            put("University", "university");
            put("Electronic Thesis and Dissertations", "");
            put("Library", "");
            put("Audio-Video", "aud_vis");
            put("Astronomy", "astro");
            put("Science", "sci");
            put("Health", "health");
            put("Environment", "env");
            put("Museum Archelogy", "mus_arc");
            put("Engineering", "engg");
            put("Medicine", "med");
            put("Medical", "med");
            put("Literature", "litr");
            put("Library and Information Science", "library");
            put("Biology", "bio");
            put("Aerospace", "aero");
            put("Scientific and Industrial Research", "sci_indus");
            put("National Museums", "nat_mus");
            put("Astrophysics", "astro");
            put("Archaeological Survey of India", "asi");
            put("National Gallery of Modern Arts", "ngma");

        }
    };
    public static final String[] ESCAPE_URLS_FROM_REDIRECTION = {
        "/auth/"
    };
    public final static String STRING_SEPARATOR = "&|&";
    /**
     * ranking rules
     */
    public final static String NVLI_RL_1 = "nvli_rl_1";
    public final static String NVLI_RL_2 = "nvli_rl_2";
    public final static String NVLI_RL_3 = "nvli_rl_3";
    public final static String NVLI_RL_4 = "nvli_rl_4";
    public final static String NVLI_RL_5 = "nvli_rl_5";
    public final static String NVLI_RL_6 = "nvli_rl_6";

    public static final class Notification {

        public static final String DEFAULT_NOTIFICATION_ENDPOINT = "";
        /**
         * Please add `user id` at the end i.e.
         * `Constants.Notification.DEFAULT_NOTIFICATION_URL + user.getId()`
         */
        public static final String DEFAULT_NOTIFICATION_URL = "/notifs/user-notifications/";

    }
    public static final String DEFAULT_THEME_BROKER_URL = "/themeBroker/themepush";
    public final static String METS = "mets";
    public final static String ORE = "ore";

    public static final class SocialProviders {

        public static final String FACEBOOK = "facebook";
        public static final String TWITTER = "twitter";
        public static final String LINKEDIN = "linkedin";
        public static final String GITHUB = "github";
        public static final String GOOGLE = "google";
    }

    public final static String ASSIGNED_ID = "assign-id";
    public final static String CURATION_STATUS = "curation-status";
    public final static String CURATED = "curated";
    public final static String UNCURATED = "uncurated";

    public static final String FEED_URL_EXCEPTIONS[] = new String[]{
        "my.aol.com",
        "bloglines.com",
        "my.msn.com",
        "pageflakes.com",
        "netvibes.com",
        "rojo.com",
        "protopage.com",
        "live.com",
        "newsgator.com",
        "add.my.yahoo.com",
        "fusion.google.com"
    };

    public static final int CONNECTION_TIMEOUT = 5000;

    public static final String RESOURCE_TYPE_MIT = "Resource with Metadata";
    public static final String RESOURCE_TYPE_HARVEST = "Harvest";
    public static final String RESOURCE_TYPE_WEB_CRAWLER = "Web crawler";
    public static final String RESOURCE_TYPE_ENEWS = "e-News";
    public static final String RESOURCE_TYPE_BLOG_CODE = "BLOG";

    public static final class InvitationStatus {

        public static final int PENDING = 0;
        public static final int CANCELLED = 2;
        public static final int ACCEPTED = 1;

    }

    public static final class UserActivityConstant {

        public static final String LOGIN = "login";
        public static final String PASSWORD_CHANGE = "changedPassword";
        public static final String SIGNUP = "signup";
        public static final String ACTIVATE_USER = "activateUser";
        public static final String DEACTIVATE_USER = "deactivateUser";
        public static final String DELETE_USER = "deleteUser";
        public static final String EDIT_USER_DETAILS = "editUserDetails";
        public static final String UPDATE_PROFILE = "updateProfile";
        public static final String SEND_INVITATION = "sendInvitation";
        public static final String CANCEL_INVITATION = "cancelInvitation";
        public static final String ACCEPT_INVITATION = "acceptInvitation";
        public static final String LIKE_RECORD = "likeRecord";
        public static final String REMOVE_LIKE = "removelike";
        public static final String SOCIAL_SHARE_RECORD = "socialShareRecord";
        public static final String RATE_RECORD = "rateRecord";
        public static final String UPDATE_RATE_RECORD = "updateRating";
        public static final String FACEBOOK_LOGIN = "facebookLogin";
        public static final String TWITTER_LOGIN = "twitterLogin";
        public static final String GOOGLE_LOGIN = "googleLogin";
        public static final String LINKEDIN_LOGIN = "linkedinLogin";
        public static final String GITHUB_LOGIN = "githubLogin";
        public static final String ADD_EBOOK = "addEbook";
        public static final String DELETE_EBOOK = "deleteEbook";
        public static final String INTRA_SHARE_RECORD = "intraShareRecord";
        public static final String KEYWORD_SEARCHED = "userSearchPhrases";
        public static final String EMAIL_RECORD = "emailrecord";
        public static final String DOWNLOAD_RECORD = "downloadRecord";
        public static final String EXPORT_RECORD = "exportRecord";
        public static final String ADD_REVIEW = "addReview";
        public static final String UPDATE_REVIEW = "updateReview";
        public static final String DELETE_REVIEW = "deleteReview";
        public static final String REVIEW = "Review";
        public static final String ADD_TO_LIST = "addToList";
        public static final String REMOVE_FROM_LIST = "removeFromList";
        public static final String CREATE_LIST = "createList";
        public static final String DELETE_LIST = "deleteList";
        public static final String UPDATE_LIST = "updateList";
        public static final String LINK_VISITED = "linkVisited";
        public static final String SAVED_PHRASE = "userSavedPhrases";
        public static final String ADMIN_ROLE = "adminRole";
        public static final String POLICY_UPDATED = "policyUpdated";

    }

    public static final class CrowdSourceActivity {

        public static final String EDIT_METADATA = "editMetadata";
        public static final String ADD_UDC_TAG = "addUDCTag";
        public static final String ADD_CUSTOM_TAG = "addCustomTag";
        public static final String APPROVED_METADATA = "approvedMetadata";
        public static final String APPROVED_UDC_TAG = "approvedUDCTag";
    }

    public static final class PrivacypPolicyStatus {

        public static final int CREATED = 1;
        public static final int UPDATED = 3;
        public static final int NOTCREATED = 0;
        public static final int ALREADYUPDATED = 2;

    }

    public static final class PolicyType {

        public static final String PRIVACYPOLICY = "Privacy Policy";
        public static final String TNCPOLICY = "Terms and Services";
    }
    public static final Set<String> ENEWS_CATEGORY_SET = new HashSet<String>() {
        {
            add("en");
            add("hi");
            add("ml");
            add("ta");
            add("te");
        }
    };

    static String elements[] = {"a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the"};

    public static Set STOP_WORDS = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(elements)));

    public static final class AnonymousUser {

        public static final long LOGIN_ID = 0;
        public static final String USER_NAME = "anonymoususer";
    }

    public static final class FilterActivityLogs {

        public static final String AllActivities = "allActivities";

        public static final String LoginActivities = "loginActivities";

        public static final String SearchActivities = "searchActivities";

        public static final String VisitActivities = "visitActivities";

        public static final String SaveActivities = "saveActivities";

        public static final String ShareActivities = "shareActivities";

        public static final String LikesActivities = "likesActivities";

        public static final String CrowdSourceActivities = "crowdSourceActivities";

        public static final String RatingActivities = "ratingActivities";

        public static final String ReviewActivities = "reviewActivities";

        public static final String EbookActivities = "ebookActivities";

        public static final String ListActivities = "listActivities";

    }

    public static final class CollectionNames {

        public static final String USER_REVIEW_RATING = "userReviewAndRating";
    }

    /**
     * RabbitMQ related all constants are here.
     */
    public static final class RabbitMq {

        public static final String EMAIL_EXCHANGE = "nvli.email.exchange";
        public static final String ACTIVITYLOG_EXCHANGE = "nvli.activityLog.exchange";
        public static final String EMAIL_RECOVERY_KEY = "emailRecoveryKey";
        public static final String EMAIL_VERIFICATION_KEY = "emailVerificationKey";
        public static final String EMAIL_NVLI_SHARE_KEY = "emailNvliShareKey";
        public static final String EMAIL_INVITATION_KEY = "emailInvitationKey";
        public static final String ACTIVITYLOG_RECOMMENDATION_KEY = "activityLogRecommendationKey";
    }

    public static final class ApplicationConfig {

        public static final String Login_Captacha = "Login Captcha";
        public static final String Registration_Captcha = "Registration Captcha";

    }

    public static final class PointsDetails {

        public static final String EDIT_METADATA = "EDIT_METADATA";
        public static final String APPROVED_METADATA = "APPROVED_METADATA";
        public static final String ADD_UDC_TAG = "ADD_UDC_TAG";
        public static final String ADD_TAG = "ADD_TAG";
        public static final String APPROVED_UDC_TAG = "APPROVED_UDC_TAG";

    }

    public static final class RewardsPath {

        public static final String REWARDS_DIR_PATH = System.getProperty("user.home") + "/.nvli/rewards";
        public static final String BARCODES_DIR_PATH = "barcode";
        public static final String CERTIFICATES_DIR_PATH = "certificate";
    }
}
