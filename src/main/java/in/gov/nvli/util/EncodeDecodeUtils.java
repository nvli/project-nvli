package in.gov.nvli.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Bhumika
 */
public class EncodeDecodeUtils implements MultipartFile{

    private static final Logger LOG = Logger.getLogger(EncodeDecodeUtils.class);

    /**
     * This method encodes a String using Base64 encoding.
     *
     * @param originalStr the original string
     * @return the encoded string
     * @throws UnsupportedEncodingException
     */
    public static String encodeString(String originalStr) throws UnsupportedEncodingException {
        byte[] originalStrByte = originalStr.getBytes("UTF-8");
        String encodedStr = DatatypeConverter.printBase64Binary(originalStrByte);
        return encodedStr;
    }

    /**
     * This method decodes back to original string using Base64 decoding.
     *
     * @param encodedStr the encoded string
     * @return the original decoded string
     * @throws UnsupportedEncodingException
     */
    public static String decodeString(String encodedStr) throws UnsupportedEncodingException {
        byte[] decodedStrByte = DatatypeConverter.parseBase64Binary(encodedStr);
        String decodedStr = new String(decodedStrByte, "UTF-8");
        return decodedStr;
    }
    
        /**
     * This method encodes bytes to Base64 encoding.
     *
     * @param originalBytes the bytes array to encode
     * @return the encoded string
     */
    public static String encodeByteArray(byte[] originalBytes) {
        String encodedStr = DatatypeConverter.printBase64Binary(originalBytes);
        return encodedStr;
    }

        /**
     * This method decodes back to original bytes using Base64 decoding.
     *
     * @param encodedStr the encoded string
     * @return the original decoded bytes array
     */
    public static byte[] decodeToBytesArray(String encodedStr) {
        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(encodedStr);
        return decodedBytes;
    }
    

    public static String getSHA256EncodedData(String dataToHash) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String encodedData = "";
        byte[] salt = GenerateSalt.getSalt().getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(salt);
        byte[] bytes = md.digest(dataToHash.getBytes());
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        encodedData = sb.toString();
        return encodedData;
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public String getOriginalFilename() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public String getContentType() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public long getSize() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public byte[] getBytes() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public InputStream getInputStream() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void transferTo(File file) throws IOException, IllegalStateException {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
    
    
    
    
}
