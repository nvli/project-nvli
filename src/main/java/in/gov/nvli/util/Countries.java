package in.gov.nvli.util;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Countries {

    public static List<String> getCountryList() {
        List<String> countryList = new LinkedList<>();
        String[] locales = Locale.getISOCountries();
        for (String countryCode : locales) {
            Locale obj = new Locale("", countryCode);
//      System.out.println("Country Code = " + obj.getCountry() + ", Country Name = " + obj.getDisplayName());
            countryList.add(obj.getDisplayName());
        }
        //    System.out.println("Done");
        Collections.sort(countryList);
        return countryList;
    }
}
