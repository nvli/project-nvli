package in.gov.nvli.util;

/**
 *
 * @author Sujata Aher
 */
public class SearchKeyword {
    private int score;
    private String value;

    public SearchKeyword() {
    }

    
    public SearchKeyword(int score, String value) {
        this.score = score;
        this.value = value;
    }

        public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "SearchKeyword{" + "score=" + score + ", value=" + value + '}';
    }
    
    
}
