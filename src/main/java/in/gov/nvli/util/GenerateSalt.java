package in.gov.nvli.util;

import org.springframework.security.crypto.keygen.KeyGenerators;

public class GenerateSalt {

    public static String getSalt() {

        return KeyGenerators.string().generateKey();

    }

}
