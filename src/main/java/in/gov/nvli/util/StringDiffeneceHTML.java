package in.gov.nvli.util;

import java.io.IOException;
import java.util.LinkedList;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import name.fraser.neil.plaintext.StandardBreakScorer;
import name.fraser.neil.plaintext.diff_match_patch;

/**
 * This class extend {@link SimpleTagSupport}.It's work to find difference
 * between two string.
 *
 * @author Doppa Srinivas
 * @version 1
 * @since 1
 */
public class StringDiffeneceHTML extends SimpleTagSupport {

    /**
     * Represent original value
     */
    private String originalValue;

    /**
     * Represent edited value
     */
    private String editedValue;

    @Override
    public void doTag() throws JspException, IOException {
        diff_match_patch d = new diff_match_patch(new StandardBreakScorer());
        LinkedList<diff_match_patch.Diff> diffs = d.diff_main(originalValue, editedValue);
        d.diff_cleanupSemantic(diffs);
        JspWriter out = getJspContext().getOut();
        out.println(diff_prettyHtml(diffs));
    }

    /**
     * This method used to mark different-2 color for differences found in
     * edited value.
     *
     * @param diffs
     * @return
     */
    private String diff_prettyHtml(LinkedList<diff_match_patch.Diff> diffs) {
        StringBuilder html = new StringBuilder();
        for (diff_match_patch.Diff aDiff : diffs) {
            String text = aDiff.text;
            switch (aDiff.operation) {
                case INSERT:
                    html.append("<mark>").append(text).append("</mark>");
                    break;
                case DELETE:
                    html.append("<del style=\"background:#f44141;\">").append(text).append("</del>");
                    break;
                case EQUAL:
                    html.append("<span>").append(text).append("</span>");
                    break;
            }
        }
        return html.toString();
    }

    /**
     * Setter
     *
     * @param originalValue
     */
    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }

    /**
     * Getter
     *
     * @param editedValue
     */
    public void setEditedValue(String editedValue) {
        this.editedValue = editedValue;
    }
}
