package in.gov.nvli.util;

import in.gov.nvli.domain.dublincore.DublinCoreMetadata;
import in.gov.nvli.domain.marc21.CollectionType;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Gulafsha Khan
 * @author Ritesh Malviya
 */
public class CompareMetadataVersionsUtils {

    /**
     *
     * @param source
     * @param target
     * @return
     */
    public static List<String> findModifications(List<String> source, List<String> target) {
        List<String> diffrence = new ArrayList<>();
        if (!source.containsAll(target)) {
            diffrence.addAll(target);
            diffrence.removeAll(source);
        }
        return diffrence;
    }

    /**
     *
     * @param source
     * @param target
     * @return
     * @throws Exception
     */
    public static Map<String, List<String>> findDCMetadataModifications(DublinCoreMetadata source, DublinCoreMetadata target) throws Exception {
        Map<String, List<String>> modifiedMetadataMap = new HashMap<>();
        Field[] allFields = DublinCoreMetadata.class.getDeclaredFields();
        List<String> field1, field2;
        for (Field field : allFields) {
            field.setAccessible(true);
            field1 = (List<String>) field.get(source);
            field2 = (List<String>) field.get(target);
            if (field1 == null && field2 != null) {
                modifiedMetadataMap.put(field.getName(), field2);
            } else if (field1 != null && field2 != null) {
                modifiedMetadataMap.put(field.getName(), findModifications(field1, field2));
            }
        }
        return modifiedMetadataMap;
    }

    /**
     *
     * @param source
     * @param target
     * @return
     * @throws Exception
     */
    public static Map<String, List<String>> findMarc21MetadataModifications(CollectionType source, CollectionType target) throws Exception {
        Map<String, List<String>> modifiedMetadataMap = new HashMap<>();
        Map<String, List<String>> sourceMap = Marc21Parser.getValuesAsMap(source);
        Map<String, List<String>> targetMap = Marc21Parser.getValuesAsMap(target);
        List<String> field1, field2;
        for (String field : targetMap.keySet()) {
            field1 = (List<String>) sourceMap.get(field);
            field2 = (List<String>) targetMap.get(field);
            if (field1 == null && field2 != null) {
                modifiedMetadataMap.put(field, field2);
            } else if (field1 != null && field2 != null) {
                modifiedMetadataMap.put(field, findModifications(field1, field2));
            }
        }
        return modifiedMetadataMap;
    }
}
