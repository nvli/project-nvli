package in.gov.nvli.util;

import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.resource.ResourceTypeInfo;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Base64;

/**
 *
 * @author Sujata Aher
 * @since 1
 * @version 1
 */
public class Helper {

    private final static Logger LOGGER = Logger.getLogger(Helper.class);
    
    public static Map z3950LibraryDetails=new HashMap<>();

    /**
     *
     * Gets the current timestamp in SQL format.
     *
     * @return java.sql.Timestamp
     * @since 1
     */
    public static Timestamp rightNow() {
        return new Timestamp(new java.util.Date().getTime());
    }

    public void getDatabaseNamesList() {
    }
    private static AtomicLong idCounter = new AtomicLong();

    public static AtomicLong getIdCounter() {
        return idCounter;
    }

    public static void setIdCounter(AtomicLong idCounter) {
        Helper.idCounter = idCounter;
    }

    public static String createID() {
        return String.valueOf(idCounter.getAndIncrement());
    }

    /**
     * method used to call MIT Web Service Authentication
     *
     * @return HttpHeaders Object
     */
    public static HttpHeaders mitWebserviceAthentication() {
        String plainCreds = "MetadataIntegratorUser:mit_user@123#";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        headers.add("User-Agent", "NVLI Internal Web Service");
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    /**
     * Used to check image is coming or not from Open Library Resource
     *
     * @param urlString url
     * @return status code(404|200)
     */
    public static int getResponseCode(String urlString) {
        try {
            URL u = new URL(urlString);
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("GET");
            huc.connect();
            return huc.getResponseCode();
        } catch (Exception e) {
            LOGGER.error("Error in getResponseCode", e);
        }
        return 200;
    }

    /**
     * To remove Punctuation from string
     *
     * @param originalStr String to remove punctuation.
     * @return
     */
    public static String removePunctuation(String originalStr) {
        return originalStr.replaceAll("[$&+,:;=?@#|'<>.^*()%!-]","");
    }

    /**
     * Remove Stop words  from string.
     * @param originalStr
     * @return 
     */
    public static boolean removeStopWords(String originalStr) {
        if (Constants.STOP_WORDS.contains(originalStr) || originalStr.endsWith(" ")) {
            return true;
        }
        return false;
    }
    
    /**
     * This method is used to remove null and empty string from map.
     * @param map
     * @return 
     */
    public static Map<String, List<String>> removeNullAndEmptyStringFromMap(Map<String, List<String>> map)
    {
        if(map!=null) {
            map.keySet().stream().map((key) -> map.get(key)).filter((list) -> (list!=null && !list.isEmpty())).forEach((list) -> {
                list.removeAll(Collections.singleton(null));
            });
        }
        return map;      
    }
    
    public static JSONObject resourceTypeInfoByLangAndCode(String resourceTypeCodeValue,String languageCode, HttpServletRequest request) {
        Map<String, ResourceType> resourceTypeMap = (Map<String, ResourceType>) request.getSession().getServletContext().getAttribute("resourceTypes");
        JSONObject responseJSON = new JSONObject();
        JSONObject resourceTypeJSON;
        if(!resourceTypeCodeValue.equalsIgnoreCase("all")) {
            Map<String, ResourceType> map=new HashMap<>();
            map.put(resourceTypeCodeValue,resourceTypeMap.get(resourceTypeCodeValue));
            resourceTypeMap=map;
        }
        for (String resourceTypeCode : resourceTypeMap.keySet()) {
            ResourceType resourceType = resourceTypeMap.get(resourceTypeCode);
            resourceTypeJSON = new JSONObject();
            ResourceTypeInfo resourceTypeInfoObj = null;
            ResourceTypeInfo resourceTypeInfoEnglish = null;
            for (ResourceTypeInfo resourceTypeInfo : resourceType.getResourceTypeInfo()) {
                if (resourceTypeInfo.getLanguage().getLanguageCode().equalsIgnoreCase(languageCode)) {
                    resourceTypeInfoObj = resourceTypeInfo;
                    break;
                }

                if (resourceTypeInfo.getLanguage().getLanguageCode().equalsIgnoreCase("en")) {
                    resourceTypeInfoEnglish = resourceTypeInfo;
                }
            }
            if (resourceTypeInfoObj == null) {
                resourceTypeInfoObj = resourceTypeInfoEnglish;
            }
            resourceTypeJSON.put("title", resourceTypeInfoObj.getTitle());
            resourceTypeJSON.put("description", resourceTypeInfoObj.getDescription());
            resourceTypeJSON.put("resourceTypeCode", resourceTypeCode);
            responseJSON.put(resourceTypeCode,resourceTypeJSON);
        }
        return responseJSON;
    }
    
     public static Map<String, String> sortByValue(Map<String, String> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, String>> list =
                new LinkedList<Map.Entry<String, String>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
            public int compare(Map.Entry<String, String> o1,
                               Map.Entry<String, String> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, String> sortedMap = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        /*
        //classic iterator example
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }*/
        return sortedMap;
    }
}
