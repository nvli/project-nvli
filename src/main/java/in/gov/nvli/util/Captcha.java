/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
public class Captcha {

    public Captcha() {
    }

    public static void generateCaptchImage(HttpServletRequest request, HttpServletResponse response) {
        try {
            int height = 40;
            int width = 150;

            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Max-Age", 0);

            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics2D = image.createGraphics();
            Random r = new Random();
            String token = Long.toString(Math.abs(r.nextLong()), 36);
            token = token.replace('l', 'm');
            token = token.replace('1', '2');
            token = token.replace('0', '3');
            token = token.replace('o', 'p');
            token = token.replace('g', 'h');
            token = token.replace('9', '4');

            String ch = token.substring(0, 6);
            GradientPaint gp = new GradientPaint(30, 30, new Color(255, 204, 153), 15, 25, new Color(255, 204, 153), true);
            graphics2D.setPaint(gp);
            Font font = new Font("Verdana", Font.BOLD, 22);

            graphics2D.setFont(font);
            graphics2D.fillRect(0, 0, width, height);
            graphics2D.setColor(new Color(255, 51, 51));
            for (int i = -2; i <= 160; i += 11) {
                graphics2D.drawLine(i, 0, i, 160);
                graphics2D.drawLine(0, i, 160, i);

            }
            graphics2D.setColor(new Color(8, 83, 115));
            int x = 0;
            int y = 0;
            char[] arr = ch.toCharArray();

            for (int i = 0; i < arr.length; i++) {
                x += 10 + (Math.abs(r.nextInt()) % 15);
                y = 15 + Math.abs(r.nextInt()) % 20;
                graphics2D.drawChars(arr, i, 1, x, y);
            }

            graphics2D.dispose();

            HttpSession session = request.getSession(true);
            session.setAttribute("captchaKey", ch);

            OutputStream outputStream = response.getOutputStream();
            ImageIO.write(image, "jpeg", outputStream);
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
