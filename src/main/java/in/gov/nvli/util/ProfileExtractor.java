/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.util;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import org.apache.log4j.Logger;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author admin
 */
public class ProfileExtractor {

    private final static Logger logger = Logger.getLogger(ProfileExtractor.class);
    private String siteUrl;

    private Map<String, Artist> possibleArtistMap;
    private Map<String, Integer> visitedLinkMap;
    private List<String> ignorableLinks;

    public ProfileExtractor(String siteUrl) {
        super();
        this.siteUrl = siteUrl;
        possibleArtistMap = new HashMap<>();
        visitedLinkMap = new HashMap<>();
        ignorableLinks = new ArrayList<>();

    }

    /**
     * @return the siteURL
     */
    public String getSiteUrl() {
        return siteUrl;
    }

    /**
     * @param siteURL the siteURL to set
     */
    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    /**
     * @return the possibleArtistMap
     */
    public Map<String, Artist> getLinkMap() {
        return possibleArtistMap;
    }

    /**
     * @param linkMap the possibleArtistMap to set
     */
    public void setLinkMap(Map<String, Artist> linkMap) {
        this.possibleArtistMap = linkMap;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ArtistProfileExtractor [siteURL=" + siteUrl + ", linkMap=" + possibleArtistMap + "]";
    }

    /**
     * inner class for artist information
     *
     * @author Gajraj Tanwar
     *
     */
    public class Artist {

        private String artistProfileLink;
        private String artistName;
        private int level;

        public Artist(String artistProfileLink, String artistName, int level) {
            super();
            this.artistProfileLink = artistProfileLink;
            this.artistName = artistName;
            this.level = level;
        }

        /**
         * @return the artistName
         */
        public String getArtistName() {
            return artistName;
        }

        /**
         * @param artistName the artistName to set
         */
        public void setArtistName(String artistName) {
            this.artistName = artistName;
        }

        /**
         * @return the level
         */
        public int getLevel() {
            return level;
        }

        /**
         * @param level the level to set
         */
        public void setLevel(int level) {
            this.level = level;
        }

        public String getArtistProfileLink() {
            return artistProfileLink;
        }

        public void setArtistProfileLink(String artistProfileLink) {
            this.artistProfileLink = artistProfileLink;
        }

        @Override
        public String toString() {
            return "Artist{" + "artistProfileLink=" + artistProfileLink + ", artistName=" + artistName + ", level=" + level + '}';
        }

    }

    /**
     * @param url
     * @return
     */
    private Document connect(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).timeout(2000).get();
        }
        catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
        catch (HttpStatusException e) {
            e.printStackTrace();
            return null;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;

        }
        return doc;
    }

    /**
     * @param maxLevel
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public List<Artist> getPossibleArtistFromSite(int maxLevel) throws IOException, InterruptedException, NullPointerException {
        Queue<Artist> queue = new LinkedList<>();
        int level = 1;
        queue.add(new Artist(siteUrl, "", level));

        while (!queue.isEmpty()) {
            Thread.sleep(2000);
            Artist artist = queue.poll();
            String profileLink = artist.getArtistProfileLink();
            String artistName = artist.getArtistName();
            logger.info(" Polled Object From Queue : \n" + artist.toString() + "\t Level : " + artist.getLevel());
            if (artist.getLevel() > maxLevel) {
                logger.info("Queue Level Crossed");
                queue.clear();
                break;
            }
            /*
             *process artist object
             */
            boolean isIgonrable = ArtistProfileUtils.isIgonrable(artistName);
            //  boolean isValidArtist=Utils.checkValidiyOfArtist(artist.getArtistName(), artist.getArtistProfileLink());

            //  List<String> nameList = Utils.findNames(artist.getArtistName());
            if (!isIgonrable) {
                String name = ArtistProfileUtils.findName(artist.getArtistName());
                if (name != null) {
                    logger.info("Putting profile into linkMap \n" + artist.getArtistProfileLink());
                    possibleArtistMap.put(artist.getArtistProfileLink(), artist);
                }
            }
            visitedLinkMap.put(profileLink, level);
            ignorableLinks.add(profileLink);
            /*
             get all links on this page
             */
            Document doc = connect(artist.artistProfileLink);
            if (doc != null) {
                Elements links = doc.select("a[href]");
                Elements links2 = doc.select("A[HREF]");
                if (links2 != null && !links2.isEmpty()) {
                    links.addAll(links2);
                }
                if (links == null || links.isEmpty()) {
                    ;
                } else {
                    level = artist.getLevel() + 1;
                    for (Element element : links) {
                        String linkText = element.text();
                        String link = "";
                        if (element.hasAttr("href")) {
                            link = element.attr("href");
                        }
                        if (element.hasAttr("HREF")) {
                            link = element.attr("HREF");
                        }

                        link = ArtistProfileUtils.getAbsoulteURL(siteUrl, link);
                        logger.info("Link Text : " + linkText + "\tLink URL " + link);
                        boolean isValidLink = ArtistProfileUtils.checkValidityOfURL(siteUrl, link);
                        if (isValidLink) {
                            /*
                             update name if new link text follow the  person regex
                             */
                            if (possibleArtistMap.containsKey(link)) {
                                Artist tempArtist = possibleArtistMap.get(link);
                                String names = ArtistProfileUtils.findName(linkText);
                                String prevName = tempArtist.getArtistName();
                                String[] words = null;
                                if (prevName != null && !prevName.isEmpty()) {
                                    words = prevName.split(" ");
                                }
                                if ((prevName == null || prevName.isEmpty() || words == null || words.length > 2) || (names != null && !names.isEmpty())) {
                                    logger.info("Updating Name  : \t prevName " + prevName + "new Name : " + names);
                                    possibleArtistMap.put(link, new Artist(link, names, tempArtist.getLevel()));
                                }
                            }
                            if (!visitedLinkMap.containsKey(link) && !ignorableLinks.contains(link)) {
                                logger.info("Adding New  link to the Queue : " + link + "\tlinkText : " + linkText + "\t level : " + level);
                                queue.add(new Artist(link, linkText, level));
                            }
                        }

                        ignorableLinks.add(link);
                    }
                }
            }
            logger.info("Queue Size : " + queue.size());
        }
        List<Artist> aList = new ArrayList<>(possibleArtistMap.values());
        return aList;

    }

    public void printLinkKeyMap() {
        Set<Map.Entry<String, Artist>> entrySet = possibleArtistMap.entrySet();
        for (Map.Entry<String, Artist> entry : entrySet) {
            logger.info(entry.getKey() + "  :  " + entry.getValue().toString());

        }
    }

    public List<Artist> getArtistList() {
        List<Artist> aList = new ArrayList<>();
        Set<Map.Entry<String, Artist>> entrySet = possibleArtistMap.entrySet();

        for (Map.Entry<String, Artist> entry : entrySet) {
            aList.add(new Artist(entry.getKey(), entry.getValue().getArtistName(), 0));
        }

        return aList;

    }

    /**
     * @param args
     * @throws IOException
     * @throws InterruptedException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args)
            throws IOException, InterruptedException, ClassNotFoundException, SQLException {
        // http://www.vadehraart.com error 429
        // http://www.cymroza.com/
        // http://www.mirageartgallery.in/index.html
        // http://www.galleryspace.in/
        // http://www.eyemage.gallery/
        // http://www.pearlartgallery.in/profile.php?name_artist=Bikash%20Poddar
        // /http://www.project88.in/
        // http://www.mahuagallery.com/
        // http://www.fizdi.com
        //http://www.archerindia.com/
        //http://www.samaraartgallery.com/
        //http://galeriesaraarakkal.com/
        //http://arthouz.com/
        //http://canvaslane.com/
        //http://www.eyemage.gallery/
        //http://www.artspread.com/index.html
        //http://guildindia.com/
        String siteURL = "http://www.fizdi.com";
        // System.out.println(siteURL.length());
        // first add site
        // get artist profiles
        ProfileExtractor profileExtractor = new ProfileExtractor(siteURL);
        List<Artist> aList = profileExtractor.getPossibleArtistFromSite(3);
        for (Artist aList1 : aList) {
            System.out.println(aList1.getArtistName());
        }

    }

}
