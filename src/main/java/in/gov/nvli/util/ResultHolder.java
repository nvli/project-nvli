package in.gov.nvli.util;

import java.util.Collections;
import java.util.List;


/**
 * Class needed for holding the returned results from db and their count.
 *
 * @author Sujata Aher
 * @since 1
 * @version 1
 */
public class ResultHolder {

    private final List results;
    private final Long resultSize;

    /**
     *
     * @param results list of results
     * @param resultSize size of results list
     */
    public ResultHolder(List results, Long resultSize) {
        this.results = Collections.unmodifiableList(results);
        this.resultSize = resultSize;
    }

    /**
     * Give size of results.
     *
     * @return size of results
     */
    public Long getResultSize() {
        return resultSize;
    }

    /**
     * Give list of results
     *
     * @return results
     */
    public List getResults() {
        return results;
    }
}
