package in.gov.nvli.util;

import java.util.List;
import java.util.Map;

/**
 * @author ksaurabh
 * @since 1
 * @version 1
 *
 */
public class Word implements Comparable<Word> {

    String word;
    String language;
    String characteristic;
    String domain;
    Map<String, List<String>> translations;

    /**
     * @return the translations
     */
    public Map<String, List<String>> getTranslations() {
        return translations;
    }

    /**
     * @param translations the translations to set
     */
    public void setTranslations(Map<String, List<String>> translations) {
        this.translations = translations;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the characteristic
     */
    public String getCharacteristic() {
        return characteristic;
    }

    /**
     * @param characteristic the characteristic to set
     */
    public void setCharacteristic(String characteristic) {
        this.characteristic = characteristic;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @param word
     * @param language
     * @param characteristic
     * @param domain
     */
    public Word(String word, String language, String characteristic, String domain) {
        super();
        this.word = word;
        this.language = language;
        this.characteristic = characteristic;
        this.domain = domain;
    }

    public Word() {
        super();
    }

    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "[word=" + word + ", language=" + language + ", characteristic=" + characteristic + ", domain="
                + domain + ", translations=" + translations + "]";
    }

    @Override
    public int compareTo(Word o) {
        // TODO Auto-generated method stub
        return o.getWord().compareTo(this.getWord());
    }

}
