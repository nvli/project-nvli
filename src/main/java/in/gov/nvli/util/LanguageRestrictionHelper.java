package in.gov.nvli.util;

/**
 * This class work as helper for language restriction.
 *
 * @author Ritesh
 * @version 1
 * @since 1
 */
public class LanguageRestrictionHelper {

    /**
     * This method used to modification in given allowed languages string.
     *
     * @param allowedLanguages Set allowed languages
     * @return modified allowed languages string
     */
    public static String prepareAllowedLanguages(String allowedLanguages) {
        if (allowedLanguages == null || allowedLanguages.isEmpty()) {
            allowedLanguages = "en";
        }

        if (allowedLanguages.contains("hi") && !allowedLanguages.contains("mr")) {
            allowedLanguages += "&|&mr&|&ne";
        } else if (allowedLanguages.contains("mr") && !allowedLanguages.contains("hi")) {
            allowedLanguages += "&|&hi&|&ne";
        } else if (allowedLanguages.contains("mr") && allowedLanguages.contains("hi")) {
            allowedLanguages += "&|&ne";
        }

        if (allowedLanguages.contains("en")) {
            allowedLanguages += "&|&ceb&|&ha&|&so&|&tlh&|&id&|&haw&|&la&|&sw&|&eu&|&nr&|&nso&|&zu"
                    + "&|&xh&|&ss&|&st&|&tn&|&ts&|&cs&|&af&|&pl&|&hr&|&ro&|&sk&|&sl&|&tr&|&hu&|&az&|&et"
                    + "&|&sq&|&ca&|&es&|&fr&|&de&|&nl&|&it&|&da&|&is&|&no&|&sv&|&fi&|&lv&|&pt&|&ve&|&lt&|&tl&|&cy&|&vi";
        }

        return allowedLanguages;
    }
}
