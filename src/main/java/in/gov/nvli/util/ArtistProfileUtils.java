/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.util;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Gajraj Singh Tanwar
 */
public class ArtistProfileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArtistProfileUtils.class);

    public static File ignoreNameFile = new File("E://IgonoreNameList.txt");
    private static final int PARAGRAPH_LENGTH_THRESHOLD = 100;

    /**
     * if URL in valid then it return true otherwise return false
     *
     * @param link
     * @return
     */
    public static boolean checkValidityOfURL(String site, String link) {
        /* match ingnre words in link */
        if (link == null || link.isEmpty() || link.contains("java") || link.contains("javascript")
                || link.contains("jpg") || link.contains("@") || link.contains("JPG") || link.contains("jpeg")
                || link.contains("JPEG") || link.contains(".gif") || link.contains(".GIF") || link.contains(".png")
                || link.contains(".PNG") || link.contains(".pdf") || link.contains(".doc") || link.contains("#1")
                || link.contains("mailto") || link.contains("rss") || link.contains("blog") || link.contains("article")) {
            return false;
        }

        if (link.startsWith("http://") || link.startsWith("www")) {
            if (!link.contains(site)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Fetches Absolute URL 
     * @param siteURL
     * @param url
     * @return
     */
    public static String getAbsoulteURL(String siteURL, String url) {
        if (url.startsWith("www") || url.startsWith("http")) {
            if (url.contains(siteURL)) {
                return url;
            } else {
                return null;
            }
        }
        if (url.startsWith("/")) {
            return new StringBuffer(siteURL).append(url).toString();
        } else {
            return new StringBuffer(siteURL).append("/").append(url).toString();
        }
    }

    /**
     * Find Names of Artist
     * @param linkText
     * @return
     */
    public static List<String> findNames(String linkText) {
        Pattern p = Pattern.compile("[A-Z]\\w*[' '][A-Z]\\w*");
        p = Pattern.compile("(([A-Z])[.]{0,1}[' ']{0,1}){0,2}(\\w+[' ']{0,1}){1,2}");
        Matcher m = p.matcher(linkText);
        List<String> names = new LinkedList<>();
        while (m.find()) {
            names.add(m.group());
        }
        return names;
    }

    /**
     * Fetcher Artist Profile Information
     * @param artistURL
     * @param threshold
     * @return
     * @throws IOException
     */
    public static String getArtistProfileInformation(String artistURL, int threshold) throws IOException {
        StringBuilder sb = new StringBuilder();
        Document doc = connect(artistURL);
        if (doc == null) {
            return "No Profile Information";
        }
        Elements paragraph = doc.getElementsByTag("p");
        if (paragraph != null) {
            LOGGER.info("Number of p tags : " + paragraph.size());
            for (Element element : paragraph) {
                String text = element.text();
                if (text.length() > threshold) {
                    sb.append(element.text());
                    sb.append("\n\n");
                }
            }
        }
        return sb.toString();
    }

    public static String getArtistFinalURL(String url, String siteURL) throws IOException {
        boolean foundBio = false;
        System.out.println("url : " + url);
        String profileURL = url;
        StringBuilder sb = new StringBuilder();
        try {
            Document doc = Jsoup.connect(url).timeout(2000).get();
            if (doc != null) {
                Elements elements = doc.select("a[href]");
                System.out.println("SIZE : " + elements.size());
                for (Element element : elements) {
                    String text = element.text();
                    String link = element.attr("href");
                    link = getAbsoulteURL(siteURL, link);
                    System.out.println("______________" + link);
                    System.out.println("______________" + text);
                    System.out.println();
                    if (link != null && !text.isEmpty() && (text.toLowerCase().contains("biography")
                            || ((text.toLowerCase().contains("view") || text.toLowerCase().contains("veiw"))
                            && text.toLowerCase().contains("profile") && text.toLowerCase().contains("profile"))
                            || (link.contains("artist") && text.toLowerCase().contains("profile")))) {
                        profileURL = link;
                        foundBio = true;
                        break;
                    }

                }
            }

        }
        catch (SocketTimeoutException | org.jsoup.HttpStatusException ste) {
        }
        return profileURL;
    }

    /**
     * check if the name is in ignore word file
     *
     * @param name
     * @return
     * @throws IOException
     */
    public static boolean isIgonrable(String name) throws IOException {
        List<String> ignoreWordList = FileUtils.readLines(ignoreNameFile, Charsets.UTF_8);
        for (String string : ignoreWordList) {
            if (!string.isEmpty() && name.toLowerCase().contains(string.toLowerCase().trim())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks for Validity of Name
     * @param name
     * @param link
     * @return
     * @throws IOException
     */
    public static boolean checkValidiyOfArtist(String name, String link) throws IOException {
        return isIgonrable(name) && (link.contains("artist") || link.contains("profile"));
    }

    public static String findName(String linkText) {
        Pattern p = Pattern.compile("[A-Z]\\w*[' '][A-Z]\\w*");
        p = Pattern.compile("(([A-Z])[.]{0,1}[' ']{0,1}){0,2}((\\w+[' ']{0,1}){2,3})");
        Matcher m = p.matcher(linkText);
        if (m.find()) {
            return m.group();
        }
        return null;

    }

    public static Document connect(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).timeout(2000).get();
        }
        catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
        catch (HttpStatusException e) {
            e.printStackTrace();
            return null;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;

        }
        return doc;
    }

    public static void main(String[] args) throws IOException {
//        File file = new File("D:\\BackUp Folder Gajraj\\NVLI\\names.txt");
//               String name = "view ";
//        String siteURL = "http://www.indiaart.com";
//        String url = "http://www.indiaart.com/Art-Marketplace/Artist-portfolio/20/Anuj-Malhotra";
//  List<String> lines = FileUtils.readLines(file, Charsets.UTF_8);
//        for (String line : lines) {
//            String name = findName(line.trim());
//            if(name!=null)
//                System.out.println(name);
//        }
        String name = "view ";
        String siteURL = "http://guildindia.com";
        String url = "http://guildindia.com/RakhiPeswani/Index.htm";
        String finalurl = getArtistFinalURL(url, siteURL);
        System.out.println("final URL : " + finalurl);
        System.out.println(getArtistProfileInformation(url, 40));
        // String input = "hdhsjd'hjdshdj''jkjdks";
        // System.out.println(StringEscapeUtils.escapeEcmaScript(input));
        // input=input.replace("'", "\'");
        // System.out.println(input);
        // http://www.pearlartgallery.in/profile.php?name_artist=Bikash%20Poddar
        // http://chawla-artgallery.com/artistworks.php?aid=16
        // String siteURL="http://chawla-artgallery.com";
        // String url="http://chawla-artgallery.com/artistworks.php?aid=16";
        // System.out.println(" final text "+getArtistInformation(url,
        // siteURL));
    }

}
