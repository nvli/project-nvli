package in.gov.nvli.util;

/**
 * Enum to hold curation status for record.
 *
 * @author Hemant Anjana
 */
public enum CurationStatus {

    CURATED,
    UNCURATED;
}
