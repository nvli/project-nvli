package in.gov.nvli.webserviceclient.user;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Bhumika
 */
@Component
public class UserSearchResultActivity {

    private static String mitWSBaseURL;

    private static final String FILE_SEPERTOR = "/";

//    @Value("${webservice.mit.base.url}")
//     private String value;
//
    @Value("${webservice.mit.base.url}")
    public void setValue(String value) {
        UserSearchResultActivity.mitWSBaseURL = value;
    }

    private static HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "MetadataIntegratorUser:mit_user@123#";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    public static long likeRecord(String recordIdentifier, boolean isLiked) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "like" + FILE_SEPERTOR + recordIdentifier + "?isLiked=" + isLiked;
        return restTemplate.postForObject(url, request, Long.class);
    }

    public static long shareRecord(String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "share" + FILE_SEPERTOR + recordIdentifier;
        return restTemplate.postForObject(url, request, Long.class);
    }

    public static long rateRecord(String recordIdentifier, float avgRecRating, boolean isUpdate) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "rating" + FILE_SEPERTOR + recordIdentifier + FILE_SEPERTOR + avgRecRating + FILE_SEPERTOR + isUpdate;
        return restTemplate.postForObject(url, request, Long.class);
    }

    public static long reviewRecord(String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "review" + FILE_SEPERTOR + recordIdentifier;
        return restTemplate.postForObject(url, request, Long.class);
    }

    public static long downloadRecord(String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "download" + FILE_SEPERTOR + recordIdentifier;
        return restTemplate.postForObject(url, request, Long.class);
    }

    public static Map<String, String> getRecordsTitle(List<String> recordIdentifiers) throws Exception {
        HttpEntity request = new HttpEntity(recordIdentifiers, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "information";
        return restTemplate.postForObject(url, request, Map.class);
    }

    public static List<Ebooks> getSuggestedBooks(List<String> keywords) throws Exception {
        HttpEntity request = new HttpEntity(keywords, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + FILE_SEPERTOR + "record" + FILE_SEPERTOR + "ebooks";
        return restTemplate.postForObject(url, request, List.class);
    }
}
