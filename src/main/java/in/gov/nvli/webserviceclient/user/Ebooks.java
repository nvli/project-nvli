package in.gov.nvli.webserviceclient.user;

import java.util.Objects;


/**
 * Used For E-books.
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
public class Ebooks {
    /**
     * recordIdentifier for free e-books
     */
    private String recordIdentifier;
    /**
     * title or subject 
     */
    private String titleOrSubject;
    /**
     * object link to show
     */
    private String objectLink;

    public String getObjectLink() {
        return objectLink;
    }

    public void setObjectLink(String objectLink) {
        this.objectLink = objectLink;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public String getTitleOrSubject() {
        return titleOrSubject;
    }

    public void setTitleOrSubject(String titleOrSubject) {
        this.titleOrSubject = titleOrSubject;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ebooks other = (Ebooks) obj;
        if (!Objects.equals(this.recordIdentifier, other.recordIdentifier)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.recordIdentifier);
        return hash;
    }
}
