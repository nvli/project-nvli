package in.gov.nvli.webserviceclient.mit;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Ritesh Malviya
 */
@Service
public class AdvanceSearchWebserviceHelper {

    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "MetadataIntegratorUser:mit_user@123#";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    public List<NamesIdentifiers> getResourceWiseFacets(String resourceCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/search/specificfacets/name/" + resourceCode;
        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
        ObjectMapper mapper = new ObjectMapper();
        List<String> facets = (List<String>) mapper.readValue(mapper.writeValueAsString(responseEntity.getBody()), List.class);

        List<NamesIdentifiers> facetList = new ArrayList<>();
        NamesIdentifiers namesIdentifiers;
        if (facets != null) {
            for (String facet : facets) {
                namesIdentifiers = new NamesIdentifiers();
                namesIdentifiers.setName(facet);
                namesIdentifiers.setIdentifier(facet);
                facetList.add(namesIdentifiers);
            }
        }
        return facetList;
    }

    public String getResourceWiseFieldSuggestions(String resourceCode, String field, String searchTerm, List<String> resourceCodeList) throws Exception {
        HttpEntity request;
        RestTemplate restTemplate = new RestTemplate();
        String url;
        ResponseEntity<List> responseEntity;
        if (resourceCode.equalsIgnoreCase("general")) {
            url = mitWSBaseURL + "/search/generalsuggestions/";
            GeneralSuggestionForm generalSuggestionForm = new GeneralSuggestionForm();
            generalSuggestionForm.setResourceCode(resourceCodeList);
            generalSuggestionForm.setSearchField(field);
            generalSuggestionForm.setSearchTerm(searchTerm);
            request = new HttpEntity(generalSuggestionForm, getConfiguredHttpHeaders());
            url = URLDecoder.decode(url, "utf-8");
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, request, List.class);
        } else {
            url = mitWSBaseURL + "/search/facetsuggestions/" + resourceCode + "/" + field + "/" + searchTerm;
            request = new HttpEntity(getConfiguredHttpHeaders());
            url = URLDecoder.decode(url, "utf-8");
            responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
        }

        ObjectMapper mapper = new ObjectMapper();
        List<String> suggestionList = (List<String>) mapper.readValue(mapper.writeValueAsString(responseEntity.getBody()), List.class);
        JSONObject finalSuggestionJSON = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (String suggestion : suggestionList) {
            jsonObject = new JSONObject();
            jsonObject.put("data", suggestion);
            jsonObject.put("value", suggestion);
            jsonArray.add(jsonObject);
        }        
        finalSuggestionJSON.put("query", searchTerm);
        finalSuggestionJSON.put("suggestions", jsonArray);
        return finalSuggestionJSON.toString();
    }

    public List<NamesIdentifiers> getMuseumDateList() throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/search/facets/date";
        ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Map.class);
        ObjectMapper mapper = new ObjectMapper();
        List<FacetNameCountHolder> facetNameCountHolders = ((Map<String, List<FacetNameCountHolder>>) mapper.readValue(mapper.writeValueAsString(responseEntity.getBody()), Map.class)).get("facet_date");
        List<NamesIdentifiers> museumDateList = new ArrayList<>();
        List<FacetNameCountHolder> facetNameCountHolderList = (List<FacetNameCountHolder>) mapper.readValue(mapper.writeValueAsString(facetNameCountHolders), new TypeReference<List<FacetNameCountHolder>>() {
        });
        NamesIdentifiers namesIdentifiers;
        if (facetNameCountHolderList != null) {
            for (FacetNameCountHolder facetNameCountHolder : facetNameCountHolderList) {
                namesIdentifiers = new NamesIdentifiers();
                namesIdentifiers.setName(facetNameCountHolder.getName());
                namesIdentifiers.setIdentifier(facetNameCountHolder.getName());
                museumDateList.add(namesIdentifiers);
            }
        }
        return museumDateList;
    }

    public List<NamesIdentifiers> getInstitutionList(String resourceCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/search/institutions/" + resourceCode;
        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
        ObjectMapper mapper = new ObjectMapper();
        return (List<NamesIdentifiers>) mapper.readValue(mapper.writeValueAsString(responseEntity.getBody()), new TypeReference<List<NamesIdentifiers>>() {
        });
    }
}
