package in.gov.nvli.webserviceclient.mit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Class that used for Question Answering System.
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @since 1
 * @version 1
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionAnswerForm implements Serializable {
    /**
     * Question string 
     */
    private String questionString;
    /**
     * Answers of question and result is hashMap that contains key and value
     * <b>key is two type</b>
     * <ul>
     * <li>Answer</li>
     * <li>Related</li>
     * </ul>
     * and value is List<String> which is answer.
     */
    private HashMap<String,List<String>> answers;
    /**
     * getter of Question string  
     * @return questionString 
     */
    public String getQuestionString() {
        return questionString;
    }
    /**
     * setter of Question string 
     * @param questionString 
     */
    public void setQuestionString(String questionString) {
        this.questionString = questionString;
    }
    /**
     * getter of Answers
     * @return answers
     */
    public HashMap<String, List<String>> getAnswers() {
        return answers;
    }
    /**
     * setter of Answers
     * @param answers 
     */
    public void setAnswers(HashMap<String, List<String>> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "QuestionAnswerForm{" + "questionString=" + questionString + ", answers=" + answers + '}';
    }
        
}
