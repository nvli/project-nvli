package in.gov.nvli.webserviceclient.mit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class act as Search holder to communication between web service and nvli
 * portal.
 *
 * @author Hemant Anjana
 * @see SearchResultResponse
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * search element
     */
    private String searchElement;
    /**
     * resource to search in.
     */
    private String searchingIn;
    /**
     * size of result
     */
    private Long resultSize;
    /**
     * result found or not
     */
    private boolean resultFound;
    /**
     * list of subResourceIdentifiers
     */
    private List<String> subResourceIdentifiers;
    /**
     * Map that hold SearchResultResponse <li>Key: resourceCode; value:
     * SearchResultResponse</li>
     */
    private Map<String, SearchResultResponse> results;
    /**
     * Map that hold faceting results of list<FacetNameCountHolder>
     */
    private Map<String, List<FacetNameCountHolder>> facetMap;
    /**
     * for holding filters
     */
    private Map<String, List<String>> filters;
    /**
     * for holding specific faceting result
     */
    private Map<String, List<FacetNameCountHolder>> specificFacetMap;
    /**
     * for holding specific faceting filter result
     */
    private Map<String, List<String>> specificFilters;
    /**
     * for holding resource Type faceting result
     */
    private Map<String, Long> allResources;
    /**
     * for holding resource faceting filter result
     */
    private Set<String> allResourcesFilters;
    /**
     * for which ranking rule to apply
     */
    private String rankingRule;
    /**
     * after ranking new results
     */
    private List<SearchResult> mixedSearchResults;
    /**
     * size of mixedSearchResults
     */
    private Long mixedSearchResultSize;
    /**
     * To hold search is basic or advance
     */
    private SearchComplexityEnum searchComplexity;
    /**
     * advance search string
     */
    private String advanceSearchQueryString;
    /**
     * From date in string format
     */
    private String fromDate;
    /**
     * To date in string format
     */
    private String toDate;
    /**
     * to hold advance query for particular resource
     */
    private Map<String, String> advanceSearchQueryMap;
    /**
     * will be set for museum case only.
     */
    private List<String> museumDates;
    /**
     * result has multimedia or not
     */
    private boolean multiMedia;
    
    private boolean fullText;
//<editor-fold defaultstate="collapsed">

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public SearchComplexityEnum getSearchComplexity() {
        return searchComplexity;
    }

    public void setSearchComplexity(SearchComplexityEnum searchComplexity) {
        this.searchComplexity = searchComplexity;
    }

    public String getAdvanceSearchQueryString() {
        return advanceSearchQueryString;
    }

    public void setAdvanceSearchQueryString(String advanceSearchQueryString) {
        this.advanceSearchQueryString = advanceSearchQueryString;
    }

    public Long getMixedSearchResultSize() {
        return mixedSearchResultSize;
    }

    public void setMixedSearchResultSize(Long mixedSearchResultSize) {
        this.mixedSearchResultSize = mixedSearchResultSize;
    }

    public List<SearchResult> getMixedSearchResults() {
        return mixedSearchResults;
    }

    public void setMixedSearchResults(List<SearchResult> mixedSearchResults) {
        this.mixedSearchResults = mixedSearchResults;
    }

    public String getRankingRule() {
        return rankingRule;
    }

    public void setRankingRule(String rankingRule) {
        this.rankingRule = rankingRule;
    }

    public Map<String, Long> getAllResources() {
        return allResources;
    }

    public void setAllResources(Map<String, Long> allResources) {
        this.allResources = allResources;
    }

    public Set<String> getAllResourcesFilters() {
        return allResourcesFilters;
    }

    public void setAllResourcesFilters(Set<String> allResourcesFilters) {
        this.allResourcesFilters = allResourcesFilters;
    }

    public Map<String, List<FacetNameCountHolder>> getSpecificFacetMap() {
        return specificFacetMap;
    }

    public void setSpecificFacetMap(Map<String, List<FacetNameCountHolder>> specificFacetMap) {
        this.specificFacetMap = specificFacetMap;
    }

    public Map<String, List<String>> getSpecificFilters() {
        return specificFilters;
    }

    public void setSpecificFilters(Map<String, List<String>> specificFilters) {
        this.specificFilters = specificFilters;
    }

    public List<String> getSubResourceIdentifiers() {
        return subResourceIdentifiers;
    }

    public void setSubResourceIdentifiers(List<String> subResourceIdentifiers) {
        this.subResourceIdentifiers = subResourceIdentifiers;
    }

    public String getSearchElement() {
        return searchElement;
    }

    public void setSearchElement(String searchElement) {
        this.searchElement = searchElement;
    }

    public String getSearchingIn() {
        return searchingIn;
    }

    public void setSearchingIn(String searchingIn) {
        this.searchingIn = searchingIn;
    }

    public Long getResultSize() {
        return resultSize;
    }

    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }

    public boolean isResultFound() {
        return resultFound;
    }

    public void setResultFound(boolean resultFound) {
        this.resultFound = resultFound;
    }

    public Map<String, SearchResultResponse> getResults() {
        return results;
    }

    public void setResults(Map<String, SearchResultResponse> results) {
        this.results = results;
    }

    public Map<String, List<FacetNameCountHolder>> getFacetMap() {
        return facetMap;
    }

    public void setFacetMap(Map<String, List<FacetNameCountHolder>> facetMap) {
        this.facetMap = facetMap;
    }

    public Map<String, List<String>> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, List<String>> filters) {
        this.filters = filters;
    }

    public Map<String, String> getAdvanceSearchQueryMap() {
        return advanceSearchQueryMap;
    }

    public void setAdvanceSearchQueryMap(Map<String, String> advanceSearchQueryMap) {
        this.advanceSearchQueryMap = advanceSearchQueryMap;
    }

    public List<String> getMuseumDates() {
        return museumDates;
    }

    public void setMuseumDates(List<String> museumDates) {
        this.museumDates = museumDates;
    }

    public boolean isMultiMedia() {
        return multiMedia;
    }

    public void setMultiMedia(boolean multiMedia) {
        this.multiMedia = multiMedia;
    }

    public boolean isFullText() {
        return fullText;
    }

    public void setFullText(boolean fullText) {
        this.fullText = fullText;
    }
    
//</editor-fold>
    @Override
    public String toString() {
        return "SearchResponse{" + "searchElement=" + searchElement + ", searchingIn=" + searchingIn + ", resultSize=" + resultSize + ", resultFound=" + resultFound + ", subResourceIdentifiers=" + subResourceIdentifiers + ", results=" + results + ", facetMap=" + facetMap + ", filters=" + filters + ", specificFacetMap=" + specificFacetMap + ", specificFilters=" + specificFilters + ", allResources=" + allResources + ", allResourcesFilters=" + allResourcesFilters + ", rankingRule=" + rankingRule + ", mixedSearchResults=" + mixedSearchResults + ", mixedSearchResultSize=" + mixedSearchResultSize + ", searchComplexity=" + searchComplexity + ", advanceSearchQueryString=" + advanceSearchQueryString + ", fromDate=" + fromDate + ", toDate=" + toDate + ", advanceSearchQueryMap=" + advanceSearchQueryMap + ", museumDates=" + museumDates + ", multiMedia=" + multiMedia + ", fullText=" + fullText + '}';
    }
}
