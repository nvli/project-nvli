package in.gov.nvli.webserviceclient.mit;

import java.util.List;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @version 1
 * @since 1
 */
public class BookViewer {

    private String resourceName;
    private String title;
    private String creator;
    private String subject;
    private String publisher;
    private String taggingJson;
    private List<ImageDetails> imagesList;

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public List<ImageDetails> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<ImageDetails> imagesList) {
        this.imagesList = imagesList;
    }

    public String getTaggingJson() {
        return taggingJson;
    }

    public void setTaggingJson(String taggingJson) {
        this.taggingJson = taggingJson;
    }
}
