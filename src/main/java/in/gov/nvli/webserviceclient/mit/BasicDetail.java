package in.gov.nvli.webserviceclient.mit;


import java.util.List;

/**
 * This Class will handle the Information for getDtabases and Resource
 * sub-category names.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
public class BasicDetail {

    /**
     * Error Information for the resources.
     */
    private String errorInformation;
    /**
     * List of Strings that contains the database names or resource sub-category
     * name.
     */
    private List<NamesIdentifiers> namesIdentifiers;

    /**
     * Getter for errorInformation
     *
     * @return errorInformation
     */
    public String getErrorInformation() {
        return errorInformation;
    }

    /**
     * Setter for errorInformation.
     *
     * @param errorInformation
     */
    public void setErrorInformation(String errorInformation) {
        this.errorInformation = errorInformation;
    }

    /**
     * Getter of Names and Identifers
     *
     * @return
     */
    public List<NamesIdentifiers> getNamesIdentifiers() {
        return namesIdentifiers;
    }

    /**
     * setter of Names and Identifers
     *
     * @param namesIdetifiers
     */
    public void setNamesIdentifiers(List<NamesIdentifiers> namesIdetifiers) {
        this.namesIdentifiers = namesIdetifiers;
    }

}
