package in.gov.nvli.webserviceclient.mit;

/**
 * This Class is used for getting Image Tags
 *
 * @author Priya More<mpriya@cdac.in>
 * @version 1
 * @since 1
 */
public class ImageTag {

    /**
     * Unique Id for each tag
     */
    private String tagId;
    /**
     * Width of tag
     */
    private String width;
    /**
     * Height of tag
     */
    private String height;
    /**
     * Top position of tag
     */
    private String top_pos;
    /**
     * Left position of tag
     */
    private String left;
    /**
     * Information of tag
     */
    private String label;

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTop_pos() {
        return top_pos;
    }

    public void setTop_pos(String top_pos) {
        this.top_pos = top_pos;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
