package in.gov.nvli.webserviceclient.mit;

import java.io.Serializable;

/**
 *
 * @author Ritesh Malviya
 */
public class QueryParameter implements Serializable {

    private String parameterName;

    private String firstQueryOperator;

    private String secondQueryOperator;

    private String parameterValue;

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getFirstQueryOperator() {
        return firstQueryOperator;
    }

    public void setFirstQueryOperator(String firstQueryOperator) {
        this.firstQueryOperator = firstQueryOperator;
    }

    public String getSecondQueryOperator() {
        return secondQueryOperator;
    }

    public void setSecondQueryOperator(String secondQueryOperator) {
        this.secondQueryOperator = secondQueryOperator;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    @Override
    public String toString() {
        return "QueryParameter{" + "parameterName=" + parameterName + ", firstQueryOperator=" + firstQueryOperator + ", secondQueryOperator=" + secondQueryOperator + ", parameterValue=" + parameterValue + '}';
    }
}
