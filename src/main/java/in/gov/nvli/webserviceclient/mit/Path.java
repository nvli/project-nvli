package in.gov.nvli.webserviceclient.mit;

import java.util.ArrayList;
import java.util.List;

/**
 * Class created to generate json file of paths for Book Reader.
 *
 * @author priya
 */
public class Path {

    List<ImageDetails> path = new ArrayList<ImageDetails>();

    public List<ImageDetails> getPath() {
        return path;
    }

    public void setPath(List<ImageDetails> path) {
        this.path = path;
    }
}
