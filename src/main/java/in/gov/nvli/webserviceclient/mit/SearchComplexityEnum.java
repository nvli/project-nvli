package in.gov.nvli.webserviceclient.mit;

/**
 *
 * @author Hemant Anjana
 */
public enum SearchComplexityEnum {
    BASIC,
    ADVANCED;
}
