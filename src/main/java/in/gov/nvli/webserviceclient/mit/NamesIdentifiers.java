package in.gov.nvli.webserviceclient.mit;


/**
 * This class will take name and identifier of resource.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 */
public class NamesIdentifiers {

    /**
     * Name of anyType.
     */
    private String name;
    /**
     * Unique Identifier.
     */
    private String identifier;

    /**
     * Getter of Name.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Setter of Name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter of Identifier.
     *
     * @return
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Setter of Identifier.
     *
     * @param identifier
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
