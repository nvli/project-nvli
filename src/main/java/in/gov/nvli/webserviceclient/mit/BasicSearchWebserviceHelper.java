package in.gov.nvli.webserviceclient.mit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@Service
public class BasicSearchWebserviceHelper {

    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "MetadataIntegratorUser:mit_user@123#";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    public SearchResponse fetchResults(SearchResponse searchForm, int pageNo, int dataLimit, boolean facetRequired,String languageCode) throws Exception {
        SearchResponse webserviceSearchForm;
        RestTemplate restTemplate = new RestTemplate();
        boolean isExactMatch = true;
        HttpEntity<String> httpEntity = new HttpEntity(searchForm, getConfiguredHttpHeaders());
        String url = mitWSBaseURL + "/search/result/" + pageNo + "/" + dataLimit + "?exactMatch=" + isExactMatch + "&facetRequired=" + facetRequired + "&language=" + languageCode;
        webserviceSearchForm = restTemplate.postForObject(url, httpEntity, SearchResponse.class);
        return webserviceSearchForm;
    }

    public SearchResponse fetchWebDiscoveryResults(SearchResponse searchForm, String resourceType, int pageNo, int dataLimit, boolean facetRequired, String languageCode) throws Exception {
        System.out.println("languageCode ::"+languageCode);
        SearchResponse webserviceSearchForm;
        RestTemplate restTemplate = new RestTemplate();
        String url = null;
        switch (resourceType) {
            case "WIKI":
                searchForm.setSearchingIn("WIKI");
                url = mitWSBaseURL + "/webdiscovery/wiki/result/" + pageNo + "/" + dataLimit + "?language=" + languageCode;
                break;
            case "Google-Books":
                url = mitWSBaseURL + "/webdiscovery/googlebooks/result";
                break;
            case "Open-Library":
                url = mitWSBaseURL + "/webdiscovery/openlibrary/result/" + 1;
                break;
            case "OCLC":
                url = mitWSBaseURL + "/webdiscovery/oclc/result/" + pageNo + "/" + dataLimit;
                break;
        }
        HttpEntity<String> httpEntity = new HttpEntity(searchForm, getConfiguredHttpHeaders());
        webserviceSearchForm = restTemplate.postForObject(url + "?facetRequired=" + facetRequired, httpEntity, SearchResponse.class);
        return webserviceSearchForm;
    }

    public SearchResponse fetchAdvSearchResults(AdvanceSearchForm advanceSearchForm, int pageNo, int dataLimit) throws Exception {
        SearchResponse webserviceSearchForm;
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> httpEntity = new HttpEntity(advanceSearchForm, getConfiguredHttpHeaders());
        String url = mitWSBaseURL + "/search/advanceResult/" + pageNo + "/" + dataLimit;
        webserviceSearchForm = restTemplate.postForObject(url, httpEntity, SearchResponse.class);
        return webserviceSearchForm;
    }
}
