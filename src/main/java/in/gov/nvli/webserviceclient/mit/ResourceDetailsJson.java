package in.gov.nvli.webserviceclient.mit;

import java.io.Serializable;

/**
 * This Class will handle the Information for Resource b/w Mit Webservice and
 * NVLI.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
@SuppressWarnings("serial")
public class ResourceDetailsJson implements Serializable {

    private String name;
   
    private String resourceCode;
    private String resourceTypeCode;
   
    private String classificationLevel;
    private String establishYear;
    private String totalCount;
    private String website;
    
    private String languages;
    private String itemsCollected;
    
    private String description;
    private String address;
    private String organization;
    private String contactPerson;
    private String designation;
    private String contactNo;
    private String emailId;
    private String thematicClassification;
    private boolean contentFlag;
    /**
     * used for enable/active the resource
     */
    private boolean activationFlag;
    /**
     * used for publishing the resource
     */
    private boolean publishFlag;
    /**
     * for crowdSourcing.
     */
    private boolean crowdSource;

    public String getResourceTypeCode() {
        return resourceTypeCode;
    }

    public void setResourceTypeCode(String resourceTypeCode) {
        this.resourceTypeCode = resourceTypeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassificationLevel() {
        return classificationLevel;
    }

    public void setClassificationLevel(String classificationLevel) {
        this.classificationLevel = classificationLevel;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getEstablishYear() {
        return establishYear;
    }

    public void setEstablishYear(String establishYear) {
        this.establishYear = establishYear;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getItemsCollected() {
        return itemsCollected;
    }

    public void setItemsCollected(String itemsCollected) {
        this.itemsCollected = itemsCollected;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public boolean isContentFlag() {
        return contentFlag;
    }

    public void setContentFlag(boolean contentFlag) {
        this.contentFlag = contentFlag;
    }

    public boolean isActivationFlag() {
        return activationFlag;
    }

    public void setActivationFlag(boolean activationFlag) {
        this.activationFlag = activationFlag;
    }

    public String getThematicClassification() {
        return thematicClassification;
    }

    public void setThematicClassification(String thematicClassification) {
        this.thematicClassification = thematicClassification;
    }

    public boolean isPublishFlag() {
        return publishFlag;
    }

    public void setPublishFlag(boolean publishFlag) {
        this.publishFlag = publishFlag;
    }

    public boolean isCrowdSource() {
        return crowdSource;
    }

    public void setCrowdSource(boolean crowdSource) {
        this.crowdSource = crowdSource;
    }

}
