/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.webserviceclient.mit;

/**
 *
 * @author user
 */
public enum ClassificationLevel {
     /**
     * Academic library
     */
    ACADEMIC("ACADEMIC"),
    /**
     * Public library
     */
    PUBLIC("PUBLIC"),
    /**
     * Special library
     */
    SPECIAL("SPECIAL"),
    /**
     * school library
     */
    SCHOOL("SCHOOL"),
    /**
     * national library
     */
    NATIONAL("NATIONAL"),
    /**
     * state central library
     */
    STATE_CENTRAL("STATE CENTRAL"),
    /**
     * state libraty
     */
    STATE("STATE");
    /**
     * value for constants.
     */
    private String value;

    /**
     *
     * @param value
     */
    private ClassificationLevel(String value) {
        this.value = value;
    }

    /**
     * get value of constants.
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static ClassificationLevel getEnum(String value) {
        for (ClassificationLevel v : values()) {
            if (v.getValue().equalsIgnoreCase(value)) {
                return v;
            }
        }
        throw new IllegalArgumentException();
    }
}
