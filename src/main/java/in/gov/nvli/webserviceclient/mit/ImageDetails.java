package in.gov.nvli.webserviceclient.mit;

import java.util.List;

/**
 * Image Details class for Book Reader.
 *
 * @author Priya More<mpriya@cdac.in>
 */
public class ImageDetails {

    /**
     * Path of image
     */
    private String imgPath;
    /**
     * Width of image
     */
    private String width;
    /**
     * Height of image
     */
    private String height;
    /**
     * Image tags information
     */
    private List<ImageTag> imageTags;

    public List<ImageTag> getImageTags() {
        return imageTags;
    }

    public void setImageTags(List<ImageTag> imageTags) {
        this.imageTags = imageTags;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
