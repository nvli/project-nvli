package in.gov.nvli.webserviceclient.mit;

import java.io.Serializable;

/**
 * This class handles Resource Information details b/w Mit Webservice and NVLI.
 *
 * @author Madhuri
 */
@SuppressWarnings("serial")
public class ResourceTypeInformationDetails implements Serializable {

    /**
     * Resource Type
     */
    private String resourceType;

    /**
     * Resource Code : will be used to uniquely identify resource type
     */
    private String resourceTypeCode;
    /**
     * used for publishing the resource type
     */

    private boolean publishFlag;
    /**
     * for crowdSourcing.
     */
    private boolean crowdSource;

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceTypeCode() {
        return resourceTypeCode;
    }

    public void setResourceTypeCode(String resourceTypeCode) {
        this.resourceTypeCode = resourceTypeCode;
    }

    public boolean isPublishFlag() {
        return publishFlag;
    }

    public void setPublishFlag(boolean publishFlag) {
        this.publishFlag = publishFlag;
    }

    public boolean isCrowdSource() {
        return crowdSource;
    }

    public void setCrowdSource(boolean crowdSource) {
        this.crowdSource = crowdSource;
    }

}
