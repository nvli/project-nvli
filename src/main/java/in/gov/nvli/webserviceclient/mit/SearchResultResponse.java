package in.gov.nvli.webserviceclient.mit;

import java.io.Serializable;
import java.util.List;

/**
 * class act as search result holder, used in communication between web service
 * and nvli portal.
 *
 * @author Hemant Anjana
 * @see SearchResponse
 */
public class SearchResultResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * list of SearchResult
     */
    private List<SearchResult> listOfResult;

    /**
     * size of result
     */
    private Long resultSize;
    /**
     * result found or not
     */
    private boolean resultFound;
//<editor-fold defaultstate="collapsed">

    public List<SearchResult> getListOfResult() {
        return listOfResult;
    }

    public void setListOfResult(List<SearchResult> listOfResult) {
        this.listOfResult = listOfResult;
    }

    public Long getResultSize() {
        return resultSize;
    }

    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }

    public boolean isResultFound() {
        return resultFound;
    }

    public void setResultFound(boolean resultFound) {
        this.resultFound = resultFound;
    }
//</editor-fold>
}
