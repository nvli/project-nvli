package in.gov.nvli.webserviceclient.mit;

import java.util.List;

/**
 * Class that act as Suggestion Form
 *
 * @author Saurabh Koriya
 * @since 1
 * @version 1
 */
public class GeneralSuggestionForm {

    /**
     * resource Code
     */
    private List<String> resourceCode;
    /**
     * search element
     */
    private String searchTerm;
    /**
     * field where to search.
     */
    private String searchField;

    public List<String> getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(List<String> resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
}
