package in.gov.nvli.webserviceclient.mit;

/**
 * Class needed for holding the returned Facet from db and their count.
 * @author Saurabh Koriya
 * @since 1
 * @version 1
 */
public class FacetNameCountHolder {
    /**
     * Name Value of Facet
     */
    public String name;
    /*
     * Count of the records   
     */
    public int count;
    
    /**
     * Getter of count
     * @return 
     */
    public int getCount() {
        return count;
    }
    /**
     * setter of count
     * @param count 
     */
    public void setCount(int count) {
        this.count = count;
    }
    /**
     * Getter of name
     * @return 
     */
    public String getName() {
        return name;
    }
    /**
     * Setter of name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
