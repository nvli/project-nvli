package in.gov.nvli.webserviceclient.mit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.Factory;
import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.list.LazyList;

/**
 *
 * @author Hemant Anjana
 */
public class AdvanceSearchForm implements Serializable {

    /**
     * From date in string format
     */
    private String fromDate;
    /**
     * To date in string format
     */
    private String toDate;
    /**
     * will be set for museum case only.
     */
    private List<String> museumDates;
    /**
     * for holding udc notation ...only notation.
     */
    private String udcNotations;

    /**
     * for holding resource filter
     */
    private Set<String> searchInResources;

    /**
     * for holding map,'key' as resource code and 'value' as query parameter
     * list
     */
    private Map<String, List<QueryParameter>> resourceWiseQueryParameterMap;

    /**
     * list of subResourceIdentifiers(Institution code)
     */
    private List<String> subResourceIdentifiers;

    public AdvanceSearchForm() {
        this.resourceWiseQueryParameterMap = MapUtils.lazyMap(new HashMap<String, List<Object>>(), new Factory() {
            @Override
            public Object create() {
                return LazyList.decorate(new ArrayList<QueryParameter>(),
                        FactoryUtils.instantiateFactory(QueryParameter.class));
            }
        });
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<String> getMuseumDates() {
        return museumDates;
    }

    public void setMuseumDates(List<String> museumDates) {
        this.museumDates = museumDates;
    }

    public String getUdcNotations() {
        return udcNotations;
    }

    public void setUdcNotations(String udcNotations) {
        this.udcNotations = udcNotations;
    }

    public Set<String> getSearchInResources() {
        return searchInResources;
    }

    public void setSearchInResources(Set<String> searchInResources) {
        this.searchInResources = searchInResources;
    }

    public Map<String, List<QueryParameter>> getResourceWiseQueryParameterMap() {
        return resourceWiseQueryParameterMap;
    }

    public void setResourceWiseQueryParameterMap(Map<String, List<QueryParameter>> resourceWiseQueryParameterMap) {
        this.resourceWiseQueryParameterMap = resourceWiseQueryParameterMap;
    }

    public List<String> getSubResourceIdentifiers() {
        return subResourceIdentifiers;
    }

    public void setSubResourceIdentifiers(List<String> subResourceIdentifiers) {
        this.subResourceIdentifiers = subResourceIdentifiers;
    }

    @Override
    public String toString() {
        return "AdvanceSearchForm{" + "fromDate=" + fromDate + ", toDate=" + toDate + ", museumDates=" + museumDates + ", udcNotations=" + udcNotations + ", searchInResources=" + searchInResources + ", resourceWiseQueryParameterMap=" + resourceWiseQueryParameterMap + ", subResourceIdentifiers=" + subResourceIdentifiers + '}';
    }
}
