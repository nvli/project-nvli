package in.gov.nvli.webserviceclient.news;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 * This class is represented by News Data.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsData {

    /**
     * It holds news feed category
     */
    public String category;

    /**
     * it holds list of news feed data
     */
    public List<FeedData> feedDataList;

    /**
     * Getter
     *
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Setter
     *
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Getter
     *
     * @return list of {@link FeedData}
     */
    public List<FeedData> getFeedDataList() {
        return feedDataList;
    }

    /**
     * Setter
     *
     * @param feedDataList Set list of {@link FeedData}
     */
    public void setFeedDataList(List<FeedData> feedDataList) {
        this.feedDataList = feedDataList;
    }
}
