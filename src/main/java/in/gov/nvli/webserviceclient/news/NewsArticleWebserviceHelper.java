package in.gov.nvli.webserviceclient.news;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import in.gov.nvli.webserviceclient.mit.QuestionAnswerForm;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * This class used as helper for communication between portal and middle ware
 * service.
 *
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Service
public class NewsArticleWebserviceHelper {

    /**
     * middle ware service base url.
     */
    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    /**
     * This method used to fetch configured HttpHeaders
     *
     * @return {@link HttpHeaders}
     * @throws Exception
     */
    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "MetadataIntegratorUser:mit_user@123#";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * This method used to fetch list news article feeds by news paper code.
     *
     * @param paperCode Set news paper code
     * @return list of {@link NewsData}
     * @throws Exception
     */
    public List<NewsData> fetchNewsArticleFeed(String paperCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/news/feed/" + paperCode;
        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
        Gson gson = new Gson();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(gson.toJson(responseEntity.getBody()), new TypeReference<List<NewsData>>() {
        });
    }

    /**
     * This method used to fetch news Channel url by news channel code.
     *
     * @param channelCode Set news channel code
     * @return news channel url
     * @throws Exception
     */
    public String fetchNewsChannels(String channelCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/news/channel/" + channelCode;
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        return responseEntity.getBody();
    }

    /**
     * This method used to fetch list google news feeds by language id.
     *
     * @param languageId Set language id
     * @return list of {@link NewsData}
     * @throws Exception
     */
    public List<NewsData> fetchGoogleNews(String languageCode,String categoryName) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/news/google/" + languageCode+"/"+categoryName;
        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
        Gson gson = new Gson();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(gson.toJson(responseEntity.getBody()), new TypeReference<List<NewsData>>() {
        });
    }
    
     /**
     * This method used to fetch list google news feeds by language id.
     *
     * @param languageId Set language id
     * @return list of {@link NewsData}
     * @throws Exception
     */
    public List<String> fetchNewsCategories(String languageCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/news/category/" + languageCode;
        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);        
        return responseEntity.getBody();        
    }
    
    public QuestionAnswerForm fetchAnswers(String question) throws Exception {
       QuestionAnswerForm questionAnswerForm = new QuestionAnswerForm();
       questionAnswerForm.setQuestionString(question);
       HttpEntity request = new HttpEntity(questionAnswerForm,getConfiguredHttpHeaders());
       RestTemplate restTemplate = new RestTemplate();
       String url = mitWSBaseURL + "/request/questionAnsewer";
       ResponseEntity<QuestionAnswerForm> responseEntity = restTemplate.postForEntity(url,request, QuestionAnswerForm.class);        
       return responseEntity.getBody();        
   }
}
