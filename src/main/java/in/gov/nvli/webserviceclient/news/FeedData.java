package in.gov.nvli.webserviceclient.news;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.net.URL;

/**
 * This class is represented by feed data.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedData {

    /**
     * image URL
     */
    public String imageUrl;

    /**
     * article author
     */
    public String author;

    /**
     * The item title.
     */
    private String title = null;

    /**
     * The item link.
     */
    private URL link = null;

    /**
     * The item description as HTML code.
     */
    private String descriptionAsHTML = null;

    /**
     * The item description as plain text.
     */
    private String descriptionAsText = null;

    /**
     * The item publication date.
     */
    private String publishDate = null;

    /**
     * The item publication date.
     */
    private boolean filterRequired = false;

    /**
     *
     * @return
     */
    public String getDescriptionAsHTML() {
        return descriptionAsHTML;
    }

    /**
     *
     * @param descriptionAsHTML
     */
    public void setDescriptionAsHTML(String descriptionAsHTML) {
        this.descriptionAsHTML = descriptionAsHTML;
    }

    /**
     *
     * @return
     */
    public String getDescriptionAsText() {
        return descriptionAsText;
    }

    /**
     *
     * @param descriptionAsText
     */
    public void setDescriptionAsText(String descriptionAsText) {
        this.descriptionAsText = descriptionAsText;
    }

    /**
     *
     * @return
     */
    public URL getLink() {
        return link;
    }

    /**
     *
     * @param link
     */
    public void setLink(URL link) {
        this.link = link;
    }

    /**
     *
     * @return
     */
    public String getPublishDate() {
        return publishDate;
    }

    /**
     *
     * @param publishDate
     */
    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     *
     * @return
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *
     * @return
     */
    public boolean isFilterRequired() {
        return filterRequired;
    }

    /**
     *
     * @param filterRequired
     */
    public void setFilterRequired(boolean filterRequired) {
        this.filterRequired = filterRequired;
    }
}
