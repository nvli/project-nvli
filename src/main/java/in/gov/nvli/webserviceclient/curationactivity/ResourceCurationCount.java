package in.gov.nvli.webserviceclient.curationactivity;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * Class is tend for Holding curation count of Resource.
 *
 * @author Hemant Anjana
 */
@SuppressWarnings("serial")
public class ResourceCurationCount implements Serializable {

    /**
     * Will hold Resource code
     */
    private String resourceTypeCode;
    /**
     * Will hold resource Name
     */
    private String resourceTypeName;
    /**
     * Map will hold Resource/institute wise curation count.
     * <p>
     * <li>Key: Resource code. </li>
     * <li>value: CurationCount @see CurationCount</li>
     * </p>
     *
     */
    private Map<String, CurationCount> userCurationCounts;

    /**
     * This is getter which get resource type code.
     *
     * @return String resource type code.
     */
    public String getResourceTypeCode() {
        return resourceTypeCode;
    }

    /**
     * This is setter which sets resource type code.
     *
     * @param resourceTypeCode
     */
    public void setResourceTypeCode(String resourceTypeCode) {
        this.resourceTypeCode = resourceTypeCode;
    }

    /**
     * This is getter which get resource type name.
     *
     * @return String resource type name.
     */
    public String getResourceTypeName() {
        return resourceTypeName;
    }

    /**
     * This is setter which sets resource type name.
     *
     * @param resourceTypeName
     */
    public void setResourceTypeName(String resourceTypeName) {
        this.resourceTypeName = resourceTypeName;
    }

    /**
     * This is getter which get user curation counts.
     *
     * @return Map of String and {@link CurationCount} user curation counts.
     */
    public Map<String, CurationCount> getUserCurationCounts() {
        return userCurationCounts;
    }

    /**
     * This is setter which sets user curation counts.
     *
     * @param userCurationCounts
     */
    public void setUserCurationCounts(Map<String, CurationCount> userCurationCounts) {
        this.userCurationCounts = userCurationCounts;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.resourceTypeCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceCurationCount other = (ResourceCurationCount) obj;
        if (!Objects.equals(this.resourceTypeCode, other.resourceTypeCode)) {
            return false;
        }
        return true;
    }

}
