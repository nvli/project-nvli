package in.gov.nvli.webserviceclient.curationactivity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.beans.CurationForm;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Represents curation activity web-service helper.
 *
 * @author Ritesh Malviya
 */
@Service
public class CurationActivityWebserviceHelper {

    /**
     * This is MIT web-service base URL.
     */
    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    /**
     * This method is used to get configured HttpHeaders.
     *
     * @return {@link HttpHeaders}
     * @throws Exception
     */
    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "MetadataIntegratorUser:mit_user@123#";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * This method is used to get record count by resource.
     *
     * @param resourceTypeCode
     * @param resourceCode
     * @return String
     * @throws Exception
     */
    public String getRecordCountByResource(String resourceTypeCode, String resourceCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "curation/count/" + resourceTypeCode+ "/" +resourceCode;
        return restTemplate.postForObject(url, request, String.class);
    }

    /**
     * This method is used to start distribution.
     *
     * @param userIds
     * @param resourceTypeCode
     * @param resourceCode
     * @return boolean
     * @throws Exception
     */
    public boolean startDistribution(List<Long> userIds, String resourceTypeCode, String resourceCode) throws Exception {
        HttpEntity request = new HttpEntity(userIds, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "curation/distribute/" + resourceTypeCode+ "/" +resourceCode;
        return restTemplate.postForObject(url, request, Boolean.class);
    }

    /**
     * This method is used to get work statistics.
     *
     * @param userId
     * @return {@link CurationStatistics{
     * @throws Exception
     */
    public CurationStatistics getWorkStatistics(Long userId) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/curation/stats/user/" + userId;
        ResponseEntity<CurationStatistics> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, CurationStatistics.class);
        return responseEntity.getBody();
    }

    /**
     * This method is used to get curation form.
     *
     * @param curationForm
     * @return {@link CurationForm}
     * @throws Exception
     */
    public CurationForm getCurationFrom(CurationForm curationForm) throws Exception {
        HttpEntity request = new HttpEntity(curationForm, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/curation/list";
        return restTemplate.postForObject(url, request, CurationForm.class);
    }

    /**
     * This method is used to get record status from server.
     *
     * @param recordIdentifiers
     * @return Map<String, String>
     * @throws Exception
     */
    public Map<String, String> getRecordStatusFromServer(String recordIdentifiers) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/curation/record/status/" + recordIdentifiers;
        ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Map.class);
        return responseEntity.getBody();
    }

    /**
     * This method is used to set record status to server.
     *
     * @param recordIdentifiers
     * @param curationStatus
     * @return boolean
     * @throws Exception
     */
    public boolean setRecordStatusToServer(String recordIdentifiers, String curationStatus) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/curation/update/" + curationStatus + "/" + recordIdentifiers;
        ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Boolean.class);
        return responseEntity.getBody();
    }

    /**
     * This method is used to get user assigned resource codes.
     *
     * @param userId
     * @return Set<String>
     * @throws Exception
     */
    public Set<String> getUserAssignedResourceCodes(Long userId) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/curation/resources/" + userId;
        return restTemplate.postForObject(url, request, Set.class);
    }
    
    /**
     * This method is used to get resource wise statistics.
     *
     * @param resourceTypeCode
     * @param resourceCode
     * @return Map of Long and {@link CurationCount}
     * @throws Exception
     */
    public Map<Long, CurationCount> getResourceWiseStatistics(String resourceTypeCode, String resourceCode) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/curation/stats/" + resourceTypeCode+ "/" +resourceCode;
        return new ObjectMapper().readValue(restTemplate.postForObject(url, request, String.class), new TypeReference<Map<Long,CurationCount>>() {});
    }
}
