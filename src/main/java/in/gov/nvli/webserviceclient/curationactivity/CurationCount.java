package in.gov.nvli.webserviceclient.curationactivity;

import java.io.Serializable;

/**
 *
 * @author Hemant Anjana
 */
@SuppressWarnings("serial")
public class CurationCount implements Serializable {

    /**
     * This is assigned count.
     */
    private Long assignedCount;

    /**
     * This is curated count.
     */
    private Long curatedCount;

    /**
     * This is getter which get assigned count.
     *
     * @return Long assigned count.
     */
    public Long getAssignedCount() {
        return assignedCount;
    }

    /**
     * This is setter which sets assigned count.
     *
     * @param assignedCount
     */
    public void setAssignedCount(Long assignedCount) {
        this.assignedCount = assignedCount;
    }

    /**
     * This is getter which get curated count.
     *
     * @return Long curated count.
     */
    public Long getCuratedCount() {
        return curatedCount;
    }

    /**
     * This is setter which sets curated count.
     *
     * @param curatedCount
     */
    public void setCuratedCount(Long curatedCount) {
        this.curatedCount = curatedCount;
    }

}
