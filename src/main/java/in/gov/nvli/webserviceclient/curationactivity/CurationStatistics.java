package in.gov.nvli.webserviceclient.curationactivity;

import java.io.Serializable;
import java.util.List;

/**
 * This is class is for communication b/w mit webservice and nvli
 *
 * @author Hemant Anjana
 */
@SuppressWarnings("serial")
public class CurationStatistics implements Serializable {

    /**
     * user id
     */
    private Long userId;
    
    /**
     * List of Resource curation count. @see ResourceCurationCount.
     */
    private List<ResourceCurationCount> resourceWiseCount;

    /**
     * This is getter which get user id.
     *
     * @return
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This is setter which sets user id.
     *
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This is getter which get resource wise count.
     *
     * @return List of {@link ResourceCurationCount}
     */
    public List<ResourceCurationCount> getResourceWiseCount() {
        return resourceWiseCount;
    }

    /**
     * This is setter which sets resource wise count.
     *
     * @param resourceWiseCount
     */
    public void setResourceWiseCount(List<ResourceCurationCount> resourceWiseCount) {
        this.resourceWiseCount = resourceWiseCount;
    }
}