package in.gov.nvli.webserviceclient.crowdsource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Z3950Search implements Serializable{
    /**
     * Title of record
     */
    private String title;
    /**
     * ISBN of record
     */
    private String isbn;
    /**
     * ISSN of record
     */
    private String issn;
    /**
     * LCCN of record
     */
    private String lccn;
    /**
     * Author of record
     */
    private String author;
    /**
     * Subject of record
     */
    private String subject;
    /**
     * Search in Any
     */
    private String any;
    /**
     * Personal Name in record
     */
    private String personalName;
    /**
     * Publisher of record
     */
    private String publisher;
    /**
     * Library Code
     */
    private String libCode;
    /**
     * size of result
     */
    private Long resultSize;
    /**
     * list of SearchResult
     */
    private List<MARCObject> listOfResult;

    public List<MARCObject> getListOfResult() { 
        return listOfResult;
    }

    public void setListOfResult(List<MARCObject> listOfResult) {
        this.listOfResult = listOfResult;
    }
    
    public String getLccn() {
        return lccn;
    }

    public void setLccn(String lccn) {
        this.lccn = lccn;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAny() {
        return any;
    }

    public void setAny(String any) {
        this.any = any;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getLibCode() {
        return libCode;
    }

    public void setLibCode(String libCode) {
        this.libCode = libCode;
    }

    public Long getResultSize() {
        return resultSize;
    }

    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }
}
