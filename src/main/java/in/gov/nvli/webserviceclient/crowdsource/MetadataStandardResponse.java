package in.gov.nvli.webserviceclient.crowdsource;

import java.util.List;

/**
 * Use for generating JSON format for DC Metadata
 *
 * @author Madhuri
 * @param <T>
 */
public class MetadataStandardResponse<T> {
    /**
     * This is the unique identifier of the record.
     */
    private String recordIdentifier;
    /**
     * This is to hold the code of sub resource.
     */
    private String subResourceCode;
    /**
     * This is to differentiate type of meta data (dublincore / marc21).
     */
    private String metadataStandard;
    /**
     * This is to hold the metadata.
     */
    private T metadata;
    /**
     * This is determine whether record is close for crowdsource or not.
     */
    private String isCloseForEdit;
    /**
     * This is to hold the list of path of physical content of record.
     */
    private List<String> path;
    /**
     * This will determine the languages that are allowed in crowdsourcing.
     */
    private String allowedLanguages;

    /**
     * This is getter which get metadata standard.
     *
     * @return String metadata standard.
     */
    public String getMetadataStandard() {
        return metadataStandard;
    }

    /**
     * This is getter which get sub resource code.
     *
     * @return String sub resource code.
     */
    public String getSubResourceCode() {
        return subResourceCode;
    }

    /**
     * This is setter which sets sub resource code.
     *
     * @param subResourceCode
     */
    public void setSubResourceCode(String subResourceCode) {
        this.subResourceCode = subResourceCode;
    }

    /**
     * This is setter which sets metadata standard.
     *
     * @param metadataStandard
     */
    public void setMetadataStandard(String metadataStandard) {
        this.metadataStandard = metadataStandard;
    }

    /**
     * This is getter which get status for record is close for edit.
     *
     * @return String status for record is close for edit.
     */
    public String getIsCloseForEdit() {
        return isCloseForEdit;
    }

    /**
     * This is setter which sets status for record is close for edit.
     *
     * @param isCloseForEdit
     */
    public void setIsCloseForEdit(String isCloseForEdit) {
        this.isCloseForEdit = isCloseForEdit;
    }

    /**
     * This is getter which get record identifier.
     *
     * @return String record identifier.
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is setter which sets record identifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This is getter which get metadata.
     *
     * @return T
     */
    public T getMetadata() {
        return metadata;
    }

    /**
     * This is setter which sets metadata.
     *
     * @param metadata
     */
    public void setMetadata(T metadata) {
        this.metadata = metadata;
    }

    /**
     * This is getter which get path.
     *
     * @return List of String path.
     */
    public List<String> getPath() {
        return path;
    }

    /**
     * This is setter which sets path.
     *
     * @param path
     */
    public void setPath(List<String> path) {
        this.path = path;
    }

    /**
     * This is getter which get allowed languages.
     *
     * @return String allowed languages.
     */
    public String getAllowedLanguages() {
        return allowedLanguages;
    }

    /**
     * This is setter which sets allowed languages.
     *
     * @param allowedLanguages
     */
    public void setAllowedLanguages(String allowedLanguages) {
        this.allowedLanguages = allowedLanguages;
    }
}
