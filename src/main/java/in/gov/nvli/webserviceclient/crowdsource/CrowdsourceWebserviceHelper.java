package in.gov.nvli.webserviceclient.crowdsource;

import in.gov.nvli.beans.crowdsource.ArtistImage;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Represent crowdsouce web-service helper.
 *
 * @author Ritesh Malviya
 * @author Vivek Bugale
 */
@Service
public class CrowdsourceWebserviceHelper {

    /**
     * MIT web service URL.
     */
    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    /**
     * This method is used to get configured HttpHeaders.
     *
     * @return {@link HttpHeaders}
     * @throws Exception
     */
    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "MetadataIntegratorUser:mit_user@123#";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * This method is used to get meta-data standard response.
     *
     * @param recordIdentifier
     * @return {@link MetadataStandardResponse}
     * @throws Exception
     */
    public MetadataStandardResponse getMetadataStandardResponse(String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/crowdsource/" + recordIdentifier;
        ResponseEntity<MetadataStandardResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, MetadataStandardResponse.class);
        return responseEntity.getBody();
    }

    /**
     * This method is used to save meta-data standard response.
     *
     * @param metadataStandardResponse
     * @return boolean
     * @throws Exception
     */
    public boolean saveMetadataStandardResponse(MetadataStandardResponse metadataStandardResponse) throws Exception {
        HttpEntity request = new HttpEntity(metadataStandardResponse, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/crowdsource/update";
        return restTemplate.postForObject(url, request, Boolean.class);
    }

    /**
     * This method is used to delete artist image.
     *
     * @param artistImage
     * @param recordIdentifier
     * @return boolean
     * @throws Exception
     */
    public boolean deleteArtistImage(ArtistImage artistImage, String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(artistImage, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/artist/delete/" + recordIdentifier;
        return restTemplate.postForObject(url, request, Boolean.class);
    }

    /**
     * This method is used to edit artist image.
     *
     * @param artistImage
     * @param recordIdentifier
     * @return boolean
     * @throws Exception
     */
    public boolean editArtistImage(ArtistImage artistImage, String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(artistImage, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/artist/edit/" + recordIdentifier;
        return restTemplate.postForObject(url, request, Boolean.class);
    }

    /**
     * This method is used to get tag details.
     *
     * @param recordIdentifier
     * @param tagType
     * @return {@link TagStandardResponse}
     * @throws Exception
     */
    public TagStandardResponse getTagDetails(String recordIdentifier, String tagType) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/get/tags/" + tagType + "/" + recordIdentifier;
        ResponseEntity<TagStandardResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, TagStandardResponse.class);
        return responseEntity.getBody();
    }

    /**
     * This method is used to save tag details.
     *
     * @param tagStandardResponse
     * @return boolean
     * @throws Exception
     */
    public boolean saveTagDetails(TagStandardResponse tagStandardResponse) throws Exception {
        HttpEntity request = new HttpEntity(tagStandardResponse, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/add/tags";
        return restTemplate.postForObject(url, request, Boolean.class);
    }

    /**
     * This method is used to get record title.
     *
     * @param recordIdentifiers
     * @return Map<String, String>
     * @throws Exception
     */
    public Map<String, String> getRecordsTitle(List<String> recordIdentifiers) throws Exception {
        HttpEntity request = new HttpEntity(recordIdentifiers, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/record/information";
        return restTemplate.postForObject(url, request, Map.class);
    }
    
    /**
     * This method is used to save article.
     *
     * @param article
     * @param resourceTypeCode
     * @return boolean
     * @throws Exception
     */
    public boolean saveArticle(Article article, String resourceTypeCode) throws Exception {
        HttpEntity request = new HttpEntity(article, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();       
        String url = mitWSBaseURL + "/article/process/"+resourceTypeCode+"/save";        
        return restTemplate.postForObject(url, request, Boolean.class);
    }
    
    //    
//    public boolean updateArticle(Article article,String resourceTypeCode) throws Exception {
//        HttpEntity request = new HttpEntity(article, getConfiguredHttpHeaders());
//        RestTemplate restTemplate = new RestTemplate();
//        String url = mitWSBaseURL + "/article/process/+"resourceTypeCode"+/update";
//        return restTemplate.postForObject(url, request, Boolean.class);
//    }
    /**
     * This method is used to delete article.
     *
     * @param article
     * @param resourceTypeCode
     * @return boolean
     * @throws Exception
     */
    public boolean deleteArticle(Article article,String resourceTypeCode) throws Exception {
        HttpEntity request = new HttpEntity(article, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/article/process/"+resourceTypeCode+"/delete";
        return restTemplate.postForObject(url, request, Boolean.class);
    }

    /**
     * This method is used to fetch Metadata From Isbn number.
     * @param isbn
     * @return Map
     * @throws Exception 
     */
    public Map<String,Object> fetchMetadataFromIsbn(String isbn) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();        
        String url = mitWSBaseURL + "/webdiscovery/isbn/"+ isbn;
        return restTemplate.exchange(url, HttpMethod.GET, request, Map.class).getBody();
    }
    
   /**
    * This method is used to fetch Z3950 Search results with pagination.
    * @param z3950Search
    * @param pageNo
    * @param dataLimit
    * @return
    * @throws Exception 
    */
    public Z3950Search fetchZ3950SRU(Z3950Search z3950Search,int pageNo,int dataLimit) throws Exception {
        HttpEntity request = new HttpEntity(z3950Search, getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "/webdiscovery/z3950/"+pageNo+"/"+dataLimit;
        return restTemplate.postForObject(url, request, Z3950Search.class);
    }
}
