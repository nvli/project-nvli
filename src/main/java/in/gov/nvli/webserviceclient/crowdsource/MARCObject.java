
package in.gov.nvli.webserviceclient.crowdsource;

import in.gov.nvli.domain.marc21.CollectionType;


/**
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 */
public class MARCObject {
    /**
     * record Object
     */
    public String marcString;
    /**
     * XML object of MARC
     */
    public CollectionType collectionType;

    public String getMarcString() {
        return marcString;
    }

    public void setMarcString(String marcString) {
        this.marcString = marcString;
    }

    public CollectionType getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(CollectionType collectionType) {
        this.collectionType = collectionType;
    }
    
    
}
