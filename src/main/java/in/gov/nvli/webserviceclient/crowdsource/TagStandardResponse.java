package in.gov.nvli.webserviceclient.crowdsource;

import java.util.List;

/**
 * Used to store CustomTag or UdcTag.
 *
 * @author Madhuri
 * @param <T>
 */
public class TagStandardResponse<T> {

    /**
     * This is record identifier.
     */
    private String recordIdentifier;

    /**
     * This is tag type.
     */
    private String tagType;

    /**
     * This is tag value.
     */
    private T tagValue;

    /**
     * This is path.
     */
    private List<String> path;

    /**
     * This is getter which get record identifier.
     *
     * @return String record identifier.
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is setter which sets record identifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This is getter which get tag type.
     *
     * @return String tag type.
     */
    public String getTagType() {
        return tagType;
    }

    /**
     * This is setter which sets tag type.
     *
     * @param tagType
     */
    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    /**
     * This is getter which get tag value.
     *
     * @return T tag value.
     */
    public T getTagValue() {
        return tagValue;
    }

    /**
     * This is setter which sets tag value.
     *
     * @param tagValue
     */
    public void setTagValue(T tagValue) {
        this.tagValue = tagValue;
    }

    /**
     * This is getter which get path.
     *
     * @return List of String path.
     */
    public List<String> getPath() {
        return path;
    }

    /**
     * This is setter which sets path.
     *
     * @param path
     */
    public void setPath(List<String> path) {
        this.path = path;
    }
}
