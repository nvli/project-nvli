package in.gov.nvli.webserviceclient.crowdsource;

import java.io.Serializable;
import java.util.Date;

/**
 * Form to communicate b/w nvli and mit webservice.
 *
 * @author Ritesh
 */
@SuppressWarnings("serial")
public class Article implements Serializable {

    /**
     * Title of article.
     */
    private String title;
    
    /**
     * Description of article.
     */
    private String description;
    
    /**
     * Date of publish.
     */
    private Date publishDate;
    
    /**
     * Author of article.
     */
    private String author;
    
    /**
     * UDC notation separated by our separator.
     */
    private String udcNotation;
    
    /**
     * Custom notation separated by our separator.
     */
    private String customNotation;
    
    /**
     * last modified date
     */
    private Date lastModifiedDate;
    
    /**
     * resource Code.
     */
    private String resourceCode;
    
    /**
     * label of resource.
     */
    private String resourceLabel;

    /**
     * identifier in nvli.
     */
    private String nvliIdentifier;

    /**
     * record identifier.
     */
    private String recordIdentifier;

    /**
     * This is getter which get record identifier.
     *
     * @return String record identifier.
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is setter which sets record identifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This is getter which get nvli identifier.
     *
     * @return String nvli identifier.
     */
    public String getNvliIdentifier() {
        return nvliIdentifier;
    }

    /**
     * This is setter which sets nvli identifier.
     *
     * @param nvliIdentifier
     */
    public void setNvliIdentifier(String nvliIdentifier) {
        this.nvliIdentifier = nvliIdentifier;
    }

    /**
     * This is getter which get resource label.
     *
     * @return String resource label.
     */
    public String getResourceLabel() {
        return resourceLabel;
    }

    /**
     * This is setter which sets resource label.
     *
     * @param resourceLabel
     */
    public void setResourceLabel(String resourceLabel) {
        this.resourceLabel = resourceLabel;
    }

    /**
     * This is getter which get title.
     *
     * @return String title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * This is setter which sets title.
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * This is getter which get description.
     *
     * @return String description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * This is setter which sets description.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * This is getter which get publish date.
     *
     * @return String publish date.
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     * This is setter which sets publish date.
     *
     * @param publishDate
     */
    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    /**
     * This is getter which get author.
     *
     * @return String author.
     */
    public String getAuthor() {
        return author;
    }

    /**
     * This is setter which sets author.
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * This is getter which get udc notation.
     *
     * @return String udc notation.
     */
    public String getUdcNotation() {
        return udcNotation;
    }

    /**
     * This is setter which sets udc notation.
     *
     * @param udcNotation
     */
    public void setUdcNotation(String udcNotation) {
        this.udcNotation = udcNotation;
    }

    /**
     * This is getter which get custom notation.
     *
     * @return String custom notation.
     */
    public String getCustomNotation() {
        return customNotation;
    }

    /**
     * This is setter which sets custom notation.
     *
     * @param customNotation
     */
    public void setCustomNotation(String customNotation) {
        this.customNotation = customNotation;
    }

    /**
     * This is getter which get last modified date.
     *
     * @return Date last modified date.
     */
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * This is setter which sets last modified date.
     *
     * @param lastModifiedDate
     */
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     * This is getter which get resource code.
     *
     * @return String resource code.
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * This is setter which sets resource code.
     *
     * @param resourceCode
     */
    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }    
    
}