package in.gov.nvli.webserviceclient.oar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author svootla
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class HarRepoCustomised implements Serializable {

    private static final long serialVersionUID = 1L;

     private String repoUID;

    private String repoName;

    private String repoBaseUrl;

    private String repoLink;

    private String repoEmail;

    private Date harvestEndTime;

    private Date harvestStartTime;

    private Long recordCount;

    private short repoStatusId;

    private Map<HarRecordMetadataType, Boolean> supportedMetadataTypes = new HashMap<>();


    public HarRepoCustomised() {

    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getRepoLink() {
        return repoLink;
    }

    public void setRepoLink(String repoLink) {
        this.repoLink = repoLink;
    }

    public Long getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Long recordCount) {
        this.recordCount = recordCount;
    }

    public Date getHarvestEndTime() {
        return harvestEndTime;
    }

    public void setHarvestEndTime(Date harvestEndTime) {
        this.harvestEndTime = harvestEndTime;
    }

    public Date getHarvestStartTime() {
        return harvestStartTime;
    }

    public void setHarvestStartTime(Date harvestStartTime) {
        this.harvestStartTime = harvestStartTime;
    }

    public short getRepoStatusId() {
        return repoStatusId;
    }

    public void setRepoStatusId(short repoStatusId) {
        this.repoStatusId = repoStatusId;
    }

    public String getRepoUID() {
        return repoUID;
    }

    public void setRepoUID(String repoUID) {
        this.repoUID = repoUID;
    }

    public String getRepoBaseUrl() {
        return repoBaseUrl;
    }

    public void setRepoBaseUrl(String repoBaseUrl) {
        this.repoBaseUrl = repoBaseUrl;
    }

    public String getRepoEmail() {
        return repoEmail;
    }

    public void setRepoEmail(String repoEmail) {
        this.repoEmail = repoEmail;
    }

    public Map<HarRecordMetadataType, Boolean> getSupportedMetadataTypes() {
        return supportedMetadataTypes;
    }

    public void setSupportedMetadataTypes(Map<HarRecordMetadataType, Boolean> supportedMetadataTypes) {
        this.supportedMetadataTypes = supportedMetadataTypes;
    }

}
