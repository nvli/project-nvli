package in.gov.nvli.webserviceclient.oar;

/**
 * Hold the mete data standards supports for harvesting
 *
 * @author ankit
 */
public enum HarRecordMetadataType {

    OAI_DC("oai_dc"),
    ORE("ore"),
    METS("mets"),
    MARC("marc"),
    MARCXML("marcxml");

    private final String text;

    private HarRecordMetadataType(final String text) {
        this.text = text;
    }

    public String value() {
        return this.text;
    }
}
