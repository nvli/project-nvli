package in.gov.nvli.webserviceclient.ad;

import java.io.Serializable;

/**
 * The class hold the required information of user for Advertisement
 *
 * @author Sujata Aher
 */
public class UserDetails implements Serializable {

    /**
     * Hold the interests of User
     */
    private String interestAreas;
    /**
     * Hold the Gender of user
     */
    private String gender;
    /**
     * Hold the age of user
     */
    private String ageGroup;
    /**
     * Hold the languages
     */
    private String languages;
    /**
     * Hold the state name
     */
    private String state;
    /**
     * Hold the city name
     */
    private String city;
    /**
     * Hold the country name
     */
    private String country;
    /**
     * Hold the profession of user
     */
    private String profession;

    /**
     * Getter method for parameter interestAreas
     *
     * @return {@link String} interestAreas
     */
    public String getInterestAreas() {
        return interestAreas;
    }

    /**
     * Setter method for parameter interestAreas
     *
     * @param interestAreas {@link String} interestAreas
     */
    public void setInterestAreas(String interestAreas) {
        this.interestAreas = interestAreas;
    }

    /**
     * Getter method for parameter gender
     *
     * @return {@link String} gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Setter method for parameter gender
     *
     * @param gender {@link String} gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Getter method for parameter ageGroup
     *
     * @return {@link String} ageGroup
     */
    public String getAgeGroup() {
        return ageGroup;
    }

    /**
     * Setter method for parameter ageGroup
     *
     * @param ageGroup {@link String} ageGroup
     */
    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    /**
     * Getter method for parameter languages
     *
     * @return {@link String} languages
     */
    public String getLanguages() {
        return languages;
    }

    /**
     * Setter method for parameter languages
     *
     * @param languages {@link String} languages
     */
    public void setLanguages(String languages) {
        this.languages = languages;
    }

    /**
     * Getter method for parameter state
     *
     * @return {@link String} state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter method for parameter state
     *
     * @param state {@link String} state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Getter method for parameter city
     *
     * @return {@link String} city
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter method for parameter city
     *
     * @param city {@link String} city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Getter method for parameter country
     *
     * @return {@link String} country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Setter method for parameter country
     *
     * @param country {@link String} country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Getter method for parameter profession
     *
     * @return {@link String} profession
     */
    public String getProfession() {
        return profession;
    }

    /**
     * Setter method for parameter profession
     *
     * @param profession {@link String} profession
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }
}
