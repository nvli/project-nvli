package in.gov.nvli.webserviceclient.payment;

public class PaymentWSURIConstants {

    public static final String PAYMENT_DETAIL = "/payment-detail/";
    public static final String FETCH_ITEM_DETAIL = "/item-detail/";
    public static final String PAY_FOR_ITEM = "/pay-for-item";
    public static final String CHECK_FOR_PAID_RECORD = "/is-paid-record-price-set/";
    public static final String CHECK_USER_PAID_FOR_RECORD = "/is-paid-record-price-set-user/";
    public static final String USER_ORDER_DETAIL = "/user-order-detail/";
    public static final String RECORD_FILE_LIST = "/record-file-list/";
    public static final String RECORD_FILE_PATH = "/record-file-path/";
}
