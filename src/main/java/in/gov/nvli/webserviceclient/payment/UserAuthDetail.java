package in.gov.nvli.webserviceclient.payment;

import java.util.List;

/**
 *
 * @author Ritesh
 */
public class UserAuthDetail{
    
    private Long userId;
    
    private String userName;

    private String email;

    private String firstName;

    private String middleName;
    
    private String lastName;
     
    private String contact;
     
    private String city;
     
    private String district;
        
    private String state;
    
    private String country;
    
    private String gender;
    
    private String aboutMe;
    
    private boolean authUser;

    private List<Long> assignedOrganizations;
    
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public List<Long> getAssignedOrganizations() {
        return assignedOrganizations;
    }

    public void setAssignedOrganizations(List<Long> assignedOrganizations) {
        this.assignedOrganizations = assignedOrganizations;
    }

    public boolean isAuthUser() {
        return authUser;
    }

    public void setAuthUser(boolean authUser) {
        this.authUser = authUser;
    }
}
