package in.gov.nvli.webserviceclient.payment;

import com.google.gson.JsonObject;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.sslclient.SSLClientFactory;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Ritesh Malviya
 */
@Service
public class PaymentWSHelper {

    @Value("${webservice.payment.base.url}")
    private String paymentWSBaseURL;
    
    @Value("${webservice.payment.username}")
    private String paymentWSUsername;
    
    @Value("${webservice.payment.password}")
    private String paymentWSPassword;

    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = paymentWSUsername+":"+paymentWSPassword;
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    public String getPaymentDetail(String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.PAYMENT_DETAIL + recordIdentifier;
        System.out.println("Url" +url);
        return restTemplate.postForObject(url, request, String.class);
    }

//    public String getPaymentDetail(String recordIdentifier,Long userId) throws Exception {
//        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
//        RestTemplate restTemplate = new RestTemplate();
//        String url = paymentWSBaseURL + PaymentWSURIConstants.PAYMENT_DETAIL + recordIdentifier+"/"+userId;
//        return restTemplate.postForObject(url, request, String.class);
//    }
    
    public String getItemDetail(String itemId,String itemType) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.FETCH_ITEM_DETAIL + itemId+"/"+itemType;
        return restTemplate.postForObject(url, request, String.class);
    }
    
    public boolean payForItem(String itemId,String itemType,User user) throws Exception {
        JsonObject payForItemDetail=new JsonObject();
        payForItemDetail.addProperty("usreId", user.getId());
        payForItemDetail.addProperty("email", user.getEmail());
        payForItemDetail.addProperty("userName", user.getFirstName()+" "+user.getLastName());
        payForItemDetail.addProperty("itemId", itemId);
        payForItemDetail.addProperty("itemType", itemType);
        HttpEntity request = new HttpEntity(payForItemDetail.toString(),getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.PAY_FOR_ITEM;
        return restTemplate.postForObject(url, request, Boolean.class);
    }
    
    public String checkForPaidRecord(String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.CHECK_FOR_PAID_RECORD+recordIdentifier;
        return restTemplate.postForObject(url, request, String.class);
    }
    
    public String checkUserPaidForRecord(String recordIdentifier,Long userId) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.CHECK_USER_PAID_FOR_RECORD+recordIdentifier+"/"+userId;
        return restTemplate.postForObject(url, request, String.class);
    }
    
    public List<UserTransaction> fecthUserTransaction(Long userId, int currentPage, int dataLimit) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.USER_ORDER_DETAIL+userId+"/"+currentPage+"/"+dataLimit;
        return restTemplate.postForObject(url, request, List.class);
    }

    public List getAllPaidRecordFileList(Long userId,String recordIdentifier) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.RECORD_FILE_LIST + userId +"/"+ recordIdentifier;
        System.out.println("URL := " + url);
        return restTemplate.postForObject(url, request, List.class);
    }

    public String getClickedFilePath(Long userId,String recordIdentifier, String filename) throws Exception {
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        String url = paymentWSBaseURL + PaymentWSURIConstants.RECORD_FILE_PATH + userId +"/"+ recordIdentifier + "/" + filename;
        System.out.println("URL := " + url);
        return restTemplate.postForObject(url, request, String.class);
    }
}
