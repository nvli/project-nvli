package in.gov.nvli.controller;

import in.gov.nvli.dao.crowdsource.IUDCConceptDAO;
import in.gov.nvli.dao.resource.IOpenRepositoryDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceStatusDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.resource.OpenRepository;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceStatus;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.webserviceclient.oar.HarRepoCustomised;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * RestController to handle operation through web service.
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@RestController
@RequestMapping(value = "/rest")
public class WebserviceController {

    @Autowired
    private IResourceDAO resourceDAO;
    @Autowired
    private IResourceStatusDAO resourceStatusDAO;
    @Autowired
    private IOpenRepositoryDAO openRepositoryDAO;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    private IResourceTypeDAO resourceTypeDAO;
    @Autowired
    private IUDCConceptDAO udcConceptDAO;
    private final static Logger LOGGER = Logger.getLogger(WebserviceController.class);

    /**
     * Update status of harvest repository through web service.
     *
     * @param repoUID unique id of repository. {@link Resource}
     * @param harRepoCustomised {@link HarRepoCustomised}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = "/update/resource/status/{repoUID}", method = RequestMethod.POST)
    @Consumes(value = {"application/json", "application/xml"})
    @Produces(value = {"application/json", "application/xml"})
    public ResponseEntity<HarRepoCustomised> updateStatusOfRepo(@PathVariable("repoUID") String repoUID, @RequestBody HarRepoCustomised harRepoCustomised) {
        try {
            List<Resource> resourceList = resourceDAO.findbyQuery("findByResourceCode", repoUID);
            ResourceStatus status = resourceStatusDAO.findbyQuery("findByStatusCode", harRepoCustomised.getRepoStatusId()).get(0);
            if (resourceList != null && !resourceList.isEmpty()) {
                Resource resource = resourceList.get(0);
                resource.setResourceStatus(status);
                resourceDAO.merge(resource);
                return new ResponseEntity<>(harRepoCustomised, HttpStatus.OK);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return new ResponseEntity<>(harRepoCustomised, HttpStatus.NOT_FOUND);
    }

    /**
     * Update record count of harvest repository through web service.
     *
     * @param repoUID unique id of repository. {@link Resource}
     * @param harRepoCustomised {@link HarRepoCustomised}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = "/update/record/count/{repoUID}", method = RequestMethod.POST)
    public ResponseEntity<HarRepoCustomised> updateRecordCountOfRepo(@PathVariable("repoUID") String repoUID, @RequestBody HarRepoCustomised harRepoCustomised) {
        try {
            List<Resource> resourceList = resourceDAO.findbyQuery("findByResourceCode", repoUID);
            if (resourceList != null && !resourceList.isEmpty()) {
                Resource resource = resourceList.get(0);
                resource.setRecordCount(harRepoCustomised.getRecordCount());
                resourceDAO.merge(resource);
                return new ResponseEntity<>(harRepoCustomised, HttpStatus.OK);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return new ResponseEntity<>(harRepoCustomised, HttpStatus.NOT_FOUND);
    }

    /**
     * Update start time of harvest repository through web service.
     *
     * @param repoUID unique id of repository. {@link Resource}
     * @param harRepoCustomised {@link HarRepoCustomised}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = "/update/resource/harvest/starttime/{repoUID}", method = RequestMethod.POST)
    public ResponseEntity<HarRepoCustomised> updateHarvestStartTimeOfRepo(@PathVariable("repoUID") String repoUID, @RequestBody HarRepoCustomised harRepoCustomised) {
        try {
            List<OpenRepository> resourceList = openRepositoryDAO.findbyQuery("findByResourceCode", repoUID);
            if (resourceList != null && !resourceList.isEmpty()) {
                OpenRepository resource = resourceList.get(0);
                resource.setHarvestStartTime(harRepoCustomised.getHarvestStartTime());
                resourceDAO.merge(resource);
                return new ResponseEntity<>(harRepoCustomised, HttpStatus.OK);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return new ResponseEntity<>(harRepoCustomised, HttpStatus.NOT_FOUND);
    }

    /**
     * Update end time of harvest repository through web service.
     *
     * @param repoUID unique id of repository. {@link Resource}
     * @param harRepoCustomised {@link HarRepoCustomised}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = "/update/resource/harvest/endtime/{repoUID}", method = RequestMethod.POST)
    public ResponseEntity<HarRepoCustomised> updateHarvestEndTimeOfRepo(@PathVariable("repoUID") String repoUID, @RequestBody HarRepoCustomised harRepoCustomised) {
        try {
            List<OpenRepository> resourceList = openRepositoryDAO.findbyQuery("findByResourceCode", repoUID);
            if (resourceList != null && !resourceList.isEmpty()) {
                OpenRepository resource = resourceList.get(0);
                Date endTime = harRepoCustomised.getHarvestEndTime();
                if(endTime != null) {
                    resource.setHarvestEndTime(endTime);
                    Calendar startTS = Calendar.getInstance();
                    startTS.setTime(resource.getHarvestStartTime());
                    Calendar endTS = Calendar.getInstance();
                    endTS.setTime(endTime);
                    long milis1 = startTS.getTimeInMillis();
                    long milis2 = endTS.getTimeInMillis();
                    long diff = milis2 - milis1;
                    long remainder = diff % (24 * 60 * 60 * 1000);
                    long hrs = remainder / (60 * 60 * 1000);
                    resource.setTotalHarvestTime(hrs);
                }
                resourceDAO.merge(resource);
                return new ResponseEntity<>(harRepoCustomised, HttpStatus.OK);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
        return new ResponseEntity<>(harRepoCustomised, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/update/resource/test/{repoUID}", method = RequestMethod.GET)
    public String updateHarvestEndTimeOfRepo(@PathVariable("repoUID") String repoUID) {
        return repoUID;
    }

    @RequestMapping(value = "/update/record/count", method = RequestMethod.POST, consumes = "application/json")
    public Boolean updateRecordCountofMIT(@RequestBody Map<String, Long> mapOfRecord) {
        boolean status = false;
        try {
            if (mapOfRecord != null) {
                for (Map.Entry<String, Long> entry : mapOfRecord.entrySet()) {
                    List<Resource> resourceList = resourceDAO.findbyQuery("findByResourceCode", entry.getKey());
                    if (resourceList != null && !resourceList.isEmpty()) {
                        Resource resource = resourceList.get(0);
                        resource.setRecordCount(entry.getValue());
                        resourceDAO.merge(resource);
                    }
                }
                List<ResourceType> resourceTypeList = resourceTypeDAO.list();
                for (ResourceType resourceType : resourceTypeList) {
                    resourceService.totalRecordCount(resourceType.getId());
                }
            } else {
                System.out.println("nothing to update... map is empty or null");
            }
            status = true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    /**
     * updateUDCNotationRecordCount method is used to update UDC Notation base
     * record count in UDC Concept table through web service.
     *
     * @param mapOfRecord
     * @return
     */
    @RequestMapping(value = "/update/udcnotation/recordcount", method = RequestMethod.POST)
    public Boolean updateUDCNotationRecordCount(@RequestBody Map<String, Long> mapOfRecord) {
        boolean status = false;
        try {
            System.out.println("calling ====>" + mapOfRecord.size());
            if (mapOfRecord != null) {
                for (Map.Entry<String, Long> entry : mapOfRecord.entrySet()) {
                    List<UDCConcept> uDCConceptList = udcConceptDAO.findbyQuery("findByUDCNotation", entry.getKey());
                    if (uDCConceptList != null && !uDCConceptList.isEmpty()) {
                        UDCConcept uDCConcept = uDCConceptList.get(0);
                        uDCConcept.setUdcNotationRecordCount(entry.getValue());
                        udcConceptDAO.merge(uDCConcept);
                    }
                }
            } else {
                System.out.println("nothing to update... map is empty or null");
            }
            status = true;
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    @RequestMapping(value = "/resourcelist/{resourceTypeCode}", method = {RequestMethod.GET, RequestMethod.POST})
    public List<String> getResourceCodeListByResourceTypeCode(@PathVariable String resourceTypeCode) {
        List<String> resorceCodeList = new ArrayList<>();
        List<Resource> resourceList = resourceTypeDAO.getResourceByResourceTypeCode(resourceTypeCode);
        for (Resource resource : resourceList) {
            resorceCodeList.add(resource.getResourceCode());
        }
        return resorceCodeList;
    }
    
    @RequestMapping(value = "/udcCodes", method = {RequestMethod.GET})
    public List<String> getUDCNotationListByUDCCode() {
        List<String> udcNotationCode = new ArrayList<>();
        List<UDCConcept> udcConceptList = udcConceptDAO.list();
        for (UDCConcept udcConcept : udcConceptList) {
            udcNotationCode.add(udcConcept.getUdcNotation());
        }
        return udcNotationCode;
    }
    
}
