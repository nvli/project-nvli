package in.gov.nvli.controller;

import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.dto.PrivacyDTO;
import in.gov.nvli.service.PolicyService;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Helper;
import in.gov.nvli.webserviceclient.mit.SearchComplexityEnum;
import in.gov.nvli.webserviceclient.mit.SearchResponse;
import java.text.Format;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Default controller to handle request to serve default view such as home page,
 * and error pages etc.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Controller
public class DefaultController {

    @Autowired
    public IResourceDAO resourceDAO;
    @Autowired
    private UserService userService;
    @Autowired
    public IResourceTypeDAO resourceTypeDAO;
    @Autowired
    public ResourceService resourceService;
    @Value("${nvli.resourcType.image.location}")
    private String resourceTypeIconLocation;
    @Value("${wiki.base.url}")
    private String wikiBaseURL;
    @Autowired
    private PolicyService policyService;
    /**
     * To hold the resource type icon read location
     */
    @Value("${nvli.read.resourcType.image.location}")
    private String resourceTypeIconReadLocation;

    /**
     * Shows search page with list of available resource types. If user is
     * logged in then it redirects to home page otherwise redirect to default
     * search page
     *
     * @return
     */
    @RequestMapping(value = {"/", "", "/home"})
    public ModelAndView showSearchPage() {
        ModelAndView modelAndView = new ModelAndView();
        List<ResourceType> resourceTypesAccToTotalRecordCount = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();//resourceDAO.getListOfUsedResourceType();
        modelAndView.addObject("resourceTypesAccToTotalRecordCount", resourceTypesAccToTotalRecordCount);
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setSearchComplexity(SearchComplexityEnum.BASIC);
        modelAndView.addObject("searchForm", searchResponse);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            User user = userService.getCurrentUser();
            modelAndView.addObject("user", user);
        }
        modelAndView.setViewName("default-home");
        modelAndView.addObject("rTIconLocation", resourceTypeIconReadLocation);
        modelAndView.addObject("wikiURL", wikiBaseURL);
        long totalRecordCount = resourceService.getTotalResourceTypeRecordCount();
        Format format = com.ibm.icu.text.NumberFormat.getInstance(new Locale("en", "in"));
        modelAndView.addObject("totalRecordCount", format.format(totalRecordCount));
        return modelAndView;
    }

    /**
     * Base Controller for loading Privacy Policy Page
     *
     * @since 1.0
     * @version 1.0
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/policies/privacy"})
    public ModelAndView privacyPolicy() {
        ModelAndView modelAndView = new ModelAndView();
        String policyId = "1";
        if (policyService.checkPolicy(policyId)) {
            modelAndView.addObject("IspolicyExist", 0);
        } else {
            PrivacyDTO policy = policyService.findMaximumVersion(policyId);
            modelAndView.addObject("policyTittle", policy.getTittle());
            modelAndView.addObject("policyDesc", policy.getDescription());
            modelAndView.addObject("policyLastModifiedTime", policy.getLastModifiedDate());

        }
        modelAndView.setViewName("PolicyView");
        modelAndView.addObject("user", userService.getCurrentUser());
        return modelAndView;
    }

    /**
     * Base Controller for loading Terms of Use Policy Page
     *
     * @since 1.0
     * @version 1.0
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/policies/terms-of-use"})
    public ModelAndView termsOfUsePolicy() {
        ModelAndView modelAndView = new ModelAndView();
        String policyId = "2";
        if (policyService.checkPolicy(policyId)) {
            modelAndView.addObject("IspolicyExist", 0);
        } else {
            PrivacyDTO policy = policyService.findMaximumVersion(policyId);
            modelAndView.addObject("policyTittle", policy.getTittle());
            modelAndView.addObject("policyDesc", policy.getDescription());
            modelAndView.addObject("policyLastModifiedTime", policy.getLastModifiedDate());

        }
        modelAndView.setViewName("PolicyView");
        modelAndView.addObject("user", userService.getCurrentUser());
        return modelAndView;
    }

    /**
     * Controller to handle request 404 and load corresponding view
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/error/404")
    public String error404() {
        return "error-404";
    }

    /**
     * Controller to handle request 403 Access Denied and load corresponding
     * view
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/error/403")
    public String error403() {
        return "error-403";
    }

    /**
     * Controller to handle request 500 and load corresponding view
     *
     * @return
     */
    @RequestMapping(value = "/error/500")
    public String error500() {
        return "error-500";
    }

    /**
     * To get the Icon on view page. "/get/icon/{value:.+}" in this url pattern
     * :.+ is to accept the file name with extension in path variable
     *
     * @param response
     * @param value file name
     */
    @RequestMapping(value = "/home/resource/type/get/icon/{value:.+}", method = RequestMethod.GET)
    public void getResourceTypeIcon(HttpServletResponse response, @PathVariable String value) {
        resourceService.getIcon(resourceTypeIconLocation, value, response);
    }

    /**
     * Base Controller for loading Special features.
     *
     * @since 1.0
     * @version 1.0@return ModelAndView object
     */
    @RequestMapping(value = {"/special-features"})
    public ModelAndView specialFeatures() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("features_view");
        return modelAndView;
    }

    @RequestMapping("/resourceTypeInfoByLang")
    @ResponseBody
    public JSONObject resourceTypeInfoByLang(@RequestParam String resourceTypeCodeValue, @RequestParam String languageCode, HttpServletRequest request) {
        return Helper.resourceTypeInfoByLangAndCode(resourceTypeCodeValue, languageCode, request);
    }

    @RequestMapping(value = "/getlocalisation")
    @ResponseBody
    public JSONObject getlocalisation(@RequestParam String languageCode, HttpServletRequest request) {
        JSONObject responseJSON = new JSONObject();
        JSONObject jsonLocalisation = (JSONObject) request.getSession().getServletContext().getAttribute("jsonLocalisation");
        if (!languageCode.equalsIgnoreCase("en")) {
            responseJSON.put("en", (JSONObject) jsonLocalisation.get("en"));
        }
        responseJSON.put(languageCode, (JSONObject) jsonLocalisation.get(languageCode));
        return responseJSON;
    }

    @RequestMapping(value = {"/about-us"})
    public ModelAndView aboutUs() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("about_us_view");
        return modelAndView;
    }

    @RequestMapping(value = {"/nvli-logo"})
    public ModelAndView nvliLogo() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("nvli_logo_view");
        return modelAndView;
    }

    @RequestMapping(value = {"/terms-of-service"})
    public ModelAndView tos() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("terms_of_service_view");
        return modelAndView;
    }

    @RequestMapping(value = {"/hyperlink-policy"})
    public ModelAndView hyperlinkPolicy() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("hyperlink_policy_view");
        return modelAndView;
    }

    @RequestMapping(value = {"/copyright-policy"})
    public ModelAndView copyrightPolicy() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("copyright_policy_view");
        return modelAndView;
    }

    @RequestMapping(value = {"/privacy-policies"})
    public ModelAndView latestPrivacyPolicy() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("privacy_policy_view");
        return modelAndView;
    }

    @RequestMapping(value = {"/partner-institutions"})
    public ModelAndView aboutPartners() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("about_partners_view");
        return modelAndView;
    }
}
