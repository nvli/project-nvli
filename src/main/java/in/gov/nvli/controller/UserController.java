package in.gov.nvli.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import in.gov.nvli.beans.UserDetailsBean;
import in.gov.nvli.custom.user.CustomDashBoard;
import in.gov.nvli.custom.user.CustomUsersLists;
import in.gov.nvli.custom.user.RequestAttributes;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.dao.user.SocialSiteDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.SocialSite;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserInterest;
import in.gov.nvli.domain.user.UserInvitation;
import in.gov.nvli.domain.user.UserProfession;
import in.gov.nvli.domain.user.UserTheme;
import in.gov.nvli.dto.ProfessionDTO;
import in.gov.nvli.dto.UserThemeDTO;
import in.gov.nvli.dto.socialDTO;
import in.gov.nvli.dto.socialSettingDTO;
import in.gov.nvli.editor.resource.ResourceTypeEditor;
import in.gov.nvli.mongodb.domain.Notifications;
import in.gov.nvli.mongodb.domain.UserAddedList;
import in.gov.nvli.mongodb.domain.UserEBook;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.mongodb.service.NotificationService;
import in.gov.nvli.mongodb.service.UserAddedListService;
import in.gov.nvli.mongodb.service.UserEBookService;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserProfessionService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.service.UserThemeService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.UserInterestComparatorByID;
import in.gov.nvli.webserviceclient.mit.SearchComplexityEnum;
import in.gov.nvli.webserviceclient.mit.SearchResponse;
import in.gov.nvli.webserviceclient.payment.PaymentWSHelper;
import in.gov.nvli.webserviceclient.payment.UserTransaction;
import in.gov.nvli.webserviceclient.user.Ebooks;
import in.gov.nvli.webserviceclient.user.UserSearchResultActivity;
import java.io.File;
import java.io.IOException;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Base Controller to perform user related activities.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Ruturaj Powar <ruturajp@cdac.in>
 */
@Controller
@RequestMapping(value = "user")
public class UserController {

    private static final Logger LOG = Logger.getLogger(UserController.class);
    @Autowired
    private UserActivityService activityServiceObj;
    @Autowired
    private UserService userService;
    @Autowired
    private ApplicationContext ctx;
    @Autowired
    private MessageSource messages;
    @Autowired
    private ResourceService resourceService;
    @Autowired
    UserProfessionService userProfessionService;
    @Value("${user.parent.directory}")
    private String userRelatedDirecory;
    @Value("${profile.pic.directory.name}")
    private String profilePicDirectory;
    @Value("${dashboard.entries.limit}")
    private Integer dashBoardEntriesLimit;
    @Autowired
    public IResourceTypeDAO resourceTypeDAO;
    @Autowired
    public CrowdSourcingService crowdSourcingService;
    @Autowired
    public UserThemeService themeService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private SocialSiteDAO socialSiteDAO;
    @Autowired
    private ActivityLogService activityLogService;
    @Autowired
    UserEBookService userEBookService;
    @Autowired
    MongoOperations mongoOperations;
    /**
     * To hold the resource type icon read location
     */
    @Value("${nvli.read.resourcType.image.location}")
    private String resourceTypeIconReadLocation;
//    @Autowired
//    private NotificationsService notificationsService;

    @Autowired
    private PaymentWSHelper paymentWSHelper;

    @Autowired
    UserActivityService userActivityService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    UserAddedListService userAddedListService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(ResourceType.class, new ResourceTypeEditor(resourceTypeDAO));
    }

    /**
     * Base Controller for loading User home page
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = {"", "/"})
    public RedirectView landing() {
        System.out.println("isLoggedIn");
        return new RedirectView("/user/home", true);
    }

    /**
     * Shows Home page for logged in user
     *
     * @return
     */
    @RequestMapping(value = {"/home"})
    public ModelAndView showHomePage() {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getCurrentUser();
        modelAndView.addObject("user", user);
        List<ResourceType> resourceTypesAccToTotalRecordCount = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();//resourceDAO.getListOfUsedResourceType();
        modelAndView.addObject("resourceTypesAccToTotalRecordCount", resourceTypesAccToTotalRecordCount);
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setSearchComplexity(SearchComplexityEnum.BASIC);
        modelAndView.addObject("searchForm", searchResponse);
        long totalRecordCount = resourceService.getTotalResourceTypeRecordCount();
        Format format = com.ibm.icu.text.NumberFormat.getInstance(new Locale("en", "in"));
        modelAndView.addObject("totalRecordCount", format.format(totalRecordCount));
        modelAndView.setViewName("default-home");
        modelAndView.addObject("rTIconLocation", resourceTypeIconReadLocation);
        return modelAndView;
    }

    /**
     * Base Controller for loading User Dashboard
     *
     * @param req HttpServletRequest
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/dashboard"})
    public ModelAndView index(HttpServletRequest req) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Dashboard");
        User user = userService.getCurrentUser();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("userDashboardView");
        return modelAndView;
    }

    /**
     * Base Controller for loading activity logs
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = "/activity/log")
    public ModelAndView redirectToActivityLogOfUser() {
        //returns activities done by a particular user to be shown in view
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Activity Logs");
        User userObj = userService.getCurrentUser();
        modelAndView.addObject("user", userObj);
        modelAndView.setViewName("UserActivityLogView");
        return modelAndView;
    }

    /**
     * Controller to filter activities logs accorting to type such as likes
     * activities, login activities.
     *
     * @param pageNumber variable for page number
     * @param pageWindow maximum entries to fetch at one time.
     * @param filter types of activities to be fatched.
     * @return JSONObject containing activities logs.
     */
    @RequestMapping(value = "filter/activity/log/{pageNumber}/{pageWindow}/{filter}/{userid}")
    @ResponseBody
    public JSONObject filterActivityLogOfUser(@PathVariable("userid") long userid, @PathVariable("filter") String filter, @PathVariable("pageNumber") int pageNumber, @PathVariable("pageWindow") int pageWindow) {
        JSONObject finalJSONObj = new JSONObject();
        User user = userService.getCurrentUser();
        Object key = userid + "" + filter;
        int startRange = (pageNumber - 1) * pageWindow;
        int endRange = startRange + pageWindow;
        List<String> list = redisTemplate.opsForList().range(key, startRange, endRange - 1);
        if (!list.isEmpty()) {
            finalJSONObj.put("activityLogs", list);
            finalJSONObj.put("userProfilePicPath", user.getProfilePicPath());
            finalJSONObj.put("userFirstName", user.getFirstName());
            finalJSONObj.put("userLastName", user.getLastName());
            finalJSONObj.put("pageNumber", pageNumber);
            finalJSONObj.put("pageWindow", pageWindow);
            return finalJSONObj;
        }
        if (userid != user.getId()) {
            return finalJSONObj;
        }
        List<String> jSONArray;
        try {
            if (filter.equals(Constants.FilterActivityLogs.AllActivities)) {
                jSONArray = activityServiceObj.getActivityLogOfUserToView(ctx, user.getId(), pageNumber, pageWindow, Constants.FilterActivityLogs.AllActivities);

            } else {
                jSONArray = activityServiceObj.getFilteredActivityLogOfUserToView(ctx, user.getId(), pageNumber, pageWindow, filter);

            }
            finalJSONObj.put("activityLogs", jSONArray);
            finalJSONObj.put("userProfilePicPath", user.getProfilePicPath());
            finalJSONObj.put("userFirstName", user.getFirstName());
            finalJSONObj.put("userLastName", user.getLastName());
            finalJSONObj.put("pageNumber", pageNumber);
            finalJSONObj.put("pageWindow", pageWindow);
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return finalJSONObj;
    }

    /**
     * Base Controller for loading Profile page
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = "/profile")
    public ModelAndView profile() {
        //returns activities done by a particular user to be shown in view
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Profile");
        User currenUser = userService.getCurrentUser();
        modelAndView.addObject("user", currenUser);
        modelAndView.setViewName("UserProfileView");
        return modelAndView;
    }

    /**
     * Base Controller for loading Profile Settings page
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = "/profile/settings")
    public ModelAndView profileSettings() {
        //returns activities done by a particular user to be shown in view
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Profile Settings");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.setViewName("UserProfileSettingsView");
        return modelAndView;
    }

    /**
     * Base Controller for loading Settings page
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = "/account/settings")
    public ModelAndView settings() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Account Settings");
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.setViewName("UserSettingsView");
        return modelAndView;
    }

    /**
     * Base Controller for loading User Interest page
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = "/interest/add/{thematicTypeId}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject saveInterests(
            @PathVariable(value = "thematicTypeId") long thematicTypeId) {
        User currentUser = userService.getCurrentUser();
        List<UserInterest> interests = currentUser.getInterests();
        Collections.sort(interests, new UserInterestComparatorByID());
        JSONObject object = new JSONObject();

        UserInterest userInterest = new UserInterest();
        userInterest.setUser(currentUser);
        userInterest.setThematicType(resourceService.getThematicTypeById(thematicTypeId));
        interests.add(userInterest);
        userService.createUserInterest(userInterest);

        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    @RequestMapping(value = "/interest/rm/{thematicTypeId}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject deleteInterests(
            @PathVariable(value = "thematicTypeId") long thematicTypeId) {
        JSONObject object = new JSONObject();
        UserInterest interest = userService.getUserInterest(thematicTypeId);
        userService.removeUserInterest(interest);
        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    /**
     * Base Controller for loading User Get started page.
     *
     * @param
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/get-started", "/interests"})
    public ModelAndView getStarted() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Interests");
        User user = userService.getCurrentUser();
        modelAndView.addObject("user", user);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Interests");
        modelAndView.setViewName("UserInterestView");
        return modelAndView;
    }

    /**
     * Base User Controller for managing User Role
     *
     * @return ModelAndView
     */
    @RequestMapping(value = {"/manage-roles", "/roles"})
    public ModelAndView manageRoles() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "User Roles");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("roleModel", new Role());
        modelAndView.addObject("roles", userService.listRoles());
        modelAndView.setViewName("UserRoleView");
        return modelAndView;
    }

    /**
     * R&D
     */
    @RequestMapping(value = {"/manage/invitations"})
    public ModelAndView manageUserInvitations() {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getCurrentUser();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Invited Users");
        modelAndView.addObject("user", user);
        modelAndView.addObject("userRolesMap", userService.filterInvitedUsersByRoles(user));//map of all roleIds and names
        modelAndView.setViewName("UserInvited");
        return modelAndView;
    }

    /**
     * Manage User Controller to load Manage User View with User's list
     *
     * @return ModelAndView
     */
    @RequestMapping(value = {"/manage"})
    public ModelAndView manageUsers() throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "All Users");
        User loggedInUser = userService.getCurrentUser();
        modelAndView.addObject("user", loggedInUser);
        List<User> userList = userService.getAllUsers();

        for (Iterator<User> iterator = userList.iterator(); iterator.hasNext();) {
            User custObj = iterator.next();
            User userObj = userService.getUserById(custObj.getId());
            if (userObj.equals(loggedInUser)) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
            }
        }
        modelAndView.addObject("allUsers", userList);
        modelAndView.addObject("userRolesMap", userService.listRoles());//map of all roleIds and names
        modelAndView.setViewName("UserListView");
        return modelAndView;
    }

    /**
     * controller to fetch users according to role of logged in user.
     *
     * @param request
     * @param roleId role id of logged in user.
     * @return ModelAndView object
     * @throws IOException
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "," + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = {"/manage/role/{roleId}"})
    public ModelAndView manageUserRoleWise(
            HttpServletRequest request,
            @PathVariable(value = "roleId") long roleId) throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("roleId", roleId);
        modelAndView.addObject("roleName", userService.getUsersByRoleId(roleId).getName());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        breadcrumbMap.put("Manage Users", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", userService.getUsersByRoleId(roleId).getName());
        modelAndView.setViewName("UserListView");
        return modelAndView;
    }

    /**
     * controller to fetch users according to role of logged in user with
     * pagination.
     *
     * @param roleId role id of logged in user.
     * @param pageNumber variable for page number
     * @param pageWindow maximum entries to br fecthed at one time.
     * @param sortBy column name by which entries to be solved.
     * @param sortOrder ascending or descending.
     * @return JSONObject containing entries.
     */
    @RequestMapping(value = "manage/roleAjax/{roleId}/{pageNumber}/{pageWindow}/{sortBy}/{sortOrder}")
    @ResponseBody
    public JSONObject getUserList(
            @PathVariable("roleId") long roleId,
            @PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageWindow") int pageWindow,
            @PathVariable("sortBy") String sortBy,
            @PathVariable("sortOrder") String sortOrder) {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray userArray = new JSONArray();
        try {
            List<User> allUsers = userService.getUsersByRoleId(roleId, pageNumber, pageWindow, sortBy, sortOrder);
            for (User userObj : allUsers) {
                JSONObject userJSONObj = new JSONObject();
                JSONArray roleArray = new JSONArray();
                Set<Role> roles = userObj.getRoles();
                roles.stream().map((role1) -> {
                    JSONObject roleJSONObj = new JSONObject();
                    roleJSONObj.put("roleId", role1.getRoleId());
                    roleJSONObj.put("roleName", role1.getName());
                    return roleJSONObj;
                }).forEach((roleJSONObj) -> {
                    roleArray.add(roleJSONObj);
                });
                userJSONObj.put("userId", userObj.getId());
                userJSONObj.put("userFirstName", userObj.getFirstName());
                userJSONObj.put("userLastName", userObj.getLastName());
                userJSONObj.put("userEmail", userObj.getEmail());
                userJSONObj.put("userName", userObj.getUsername());
                userJSONObj.put("userProfilePicPath", userObj.getProfilePicPath());
                userJSONObj.put("userDeleted", userObj.getDeleted());
                userJSONObj.put("userEnabled", userObj.getEnabled());
                userJSONObj.put("roleArray", roleArray);
                userArray.add(userJSONObj);
            }
            finalJSONObj.put("allUsers", userArray);
            finalJSONObj.put("title", "User Roles");
            finalJSONObj.put("roleId", roleId);
            finalJSONObj.put("pageNumber", pageNumber);
            finalJSONObj.put("pageWindow", pageWindow);
            Role role = userService.getRoleByID(roleId);
            finalJSONObj.put("selectedRole", role.getName());
            finalJSONObj.put("selectedRoleCode", role.getCode());
            JSONArray roleCodes = new JSONArray();
            userService.getCurrentUser().getRoles().parallelStream().forEach((roleObj) -> {
                roleCodes.add(roleObj.getCode());
            });
            finalJSONObj.put("roleCodes", roleCodes);
            finalJSONObj.put("totalPages", userService.getTotalPagesByRoleId(roleId, pageWindow));
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return finalJSONObj;
    }

    /**
     * Controller method to fetch invited users.
     *
     * @param pageNumber current page number.
     * @param pageWindow max entries to be shown on one page.
     * @param sortBy column name by which sorting to be done.
     * @param sortOrder ascending or descending.
     * @return JSONObject containing entries.
     */
    @RequestMapping(value = "manage/invitedUserList", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject getInvitedUserList(@RequestParam String searchString, @RequestParam int dataLimit, @RequestParam int pageNo) {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray userArray = new JSONArray();
        try {
            List<UserInvitation> allInvitedUsers = userService.getInvitedUsersByFiltersWithLimit(searchString, dataLimit, pageNo);
            for (UserInvitation InvitedUserObj : allInvitedUsers) {
                JSONObject userJSONObj = new JSONObject();
                JSONArray roleArray = new JSONArray();
                List<Role> roles = InvitedUserObj.getGrantedRoles();
                roles.parallelStream().map((Role role1) -> {
                    JSONObject roleJSONObj = new JSONObject();
                    roleJSONObj.put("roleId", role1.getRoleId());
                    roleJSONObj.put("roleName", role1.getName());
                    return roleJSONObj;
                }).forEach((roleJSONObj) -> {
                    roleArray.add(roleJSONObj);
                });
                userJSONObj.put("invitationDate", InvitedUserObj.getInvitationDate());
                if (Constants.InvitationStatus.PENDING == InvitedUserObj.getInvitationAccepted()) {
                    userJSONObj.put("userStatus", "Pending");
                }
                if (Constants.InvitationStatus.CANCELLED == InvitedUserObj.getInvitationAccepted()) {
                    userJSONObj.put("userStatus", Constants.INVITE_CANCELLED_STR);
                }
                userJSONObj.put("inviteeEmail", InvitedUserObj.getInviteeEmail());
                userJSONObj.put("invitationId", InvitedUserObj.getInvitationId());
                userJSONObj.put("roleArray", roleArray);

                userArray.add(userJSONObj);
            }
            finalJSONObj.put("allUsers", userArray);
            finalJSONObj.put("title", "User Roles");
            finalJSONObj.put("totalCount", userService.getInvitedUserCountByFilters(searchString));

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
        return finalJSONObj;
    }

    @RequestMapping(value = "/deleteInvitation", method = RequestMethod.POST)
    @ResponseBody
    public boolean deleteInvitaion(@RequestParam long invitationId) {
        return userService.deleteUserInvitaion(invitationId);
    }

    /**
     * Create New Role controller to save the submitted data into server
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param roleModel object of Role class to be created.
     * @param result for validation by Spring
     * @param redirectAttrs
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/create/role"})
    public ModelAndView createRole(
            HttpServletRequest request,
            HttpServletResponse response,
            @ModelAttribute Role roleModel,
            BindingResult result,
            RedirectAttributes redirectAttrs) {
        ModelAndView modelAndView = new ModelAndView();
        if (result.hasErrors()) {
            modelAndView.addObject("isSuccess", false);
        } else {
            // Save userRole
            boolean isSuccess = userService.createNewRole(roleModel);
            modelAndView.addObject("isSuccess", isSuccess);
        }
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("roleModel", new Role());
        modelAndView.addObject("roles", userService.listRoles());
        modelAndView.setViewName("UserRoleView");
        return modelAndView;
    }

    /**
     * Service method to edit the role.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param roleId id of role which is to be edited.
     * @return ModelAndView object.
     */
    @RequestMapping(value = {"/edit/role/{roleId}"})
    public ModelAndView editRole(
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("roleId") Long roleId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Edit User Role");
        modelAndView.addObject("user", userService.getCurrentUser());
        return modelAndView;
    }

    /**
     * Service method for deleting the role.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param roleId id of role which is to be deleted.
     * @return JSONObject containing appropriate message.
     */
    @RequestMapping(value = {"/delete/role/{roleId}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public JSONObject deleteRole(
            HttpServletRequest request,
            HttpServletResponse response,
            @PathVariable("roleId") Long roleId) {
        JSONObject object = new JSONObject();
        userService.deleteUserRole(roleId);
        object.put("success", true);
        object.put("message", "Deleted Successfully");
        return object;
    }

    /**
     * Controller method to get complete role details from role id.
     *
     * @param roleId id of role to be fetched.
     * @return ModelAndView containing role object.
     */
    @RequestMapping(value = {"/role/{roleId}"})
    public ModelAndView roleDetails(
            @PathVariable("roleId") Long roleId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "User Role");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("roleDetails", userService.getRoleByID(roleId));
        modelAndView.setViewName("UserRoleDetailView");
        return modelAndView;
    }

    /**
     * Ajax method to update username of currently logged in user.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param username username to be updated.
     * @return JSONObject
     */
    @RequestMapping(value = "/update/uname", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject updateUsername(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "username") String username) {
        JSONObject object = new JSONObject();

        if (userService.isAuthenticated()) {
            User user = userService.getCurrentUser();
            // Todo: First Check availability
            user.setUsername(username);
            userService.updateUser(user);
            object.put("status", "OK");
        } else {
            object.put("status", "FAILED");
            object.put("message", "Access Denied!");
        }
        return object;
    }

    /**
     * Ajax method to update contact no of currently logged in user.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param contactNo contact no to be updated.
     * @return JSONObject
     */
    @RequestMapping(value = "/update/contactNo", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject updateContactNo(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "contactNo") String contactNo) {
        JSONObject object = new JSONObject();

        if (userService.isAuthenticated()) {
            User user = userService.getCurrentUser();
            user.setContact(contactNo);
            userService.updateUser(user);
            object.put("status", "OK");
        } else {
            object.put("status", "FAILED");
            object.put("message", "Access Denied!");
        }
        return object;
    }

    /**
     * Method to fetch basic information of user.
     *
     * @param userId id of user whoes information to be fetched.
     * @param roleId
     * @return ModelAndView object.
     */
    @RequestMapping(value = {"/profile/view"})
    public ModelAndView viewUserProfileByRole(@RequestParam("userId") Long userId,
            @RequestParam("roleId") Long roleId) {
        ModelAndView modelAndView = new ModelAndView("ViewUserProfile");
        modelAndView.addObject("title", "View Profile");
        try {
            if (userService.isAuthenticated()) {
                User loggedUser = userService.getCurrentUser();
                User userObj = userService.getUserById(userId);
                UserDetailsBean userDetObj = new UserDetailsBean(
                        userObj.getId(),
                        userObj.getEnabled(),
                        userObj.getUsername(),
                        userObj.getEmail(),
                        userObj.getFirstName(),
                        userObj.getMiddleName(),
                        userObj.getLastName(),
                        userObj.getContact(),
                        userObj.getAddress(),
                        userObj.getCity(),
                        userObj.getDistrict(),
                        userObj.getState(),
                        userObj.getCountry(),
                        userObj.getGender(),
                        userService.mapOfRoleIdsNameOfUser(userObj.getId()),
                        userObj.getProfilePicPath());
                Map<String, String> breadcrumbMap = new LinkedHashMap<>();
                breadcrumbMap.put("Admin Activity", "#");
                breadcrumbMap.put("Manage Users", "#");
                breadcrumbMap.put(userService.getRoleByID(roleId).getName(), "user/manage/role/" + roleId + "");
                modelAndView.addObject("breadcrumbMap", breadcrumbMap);
                modelAndView.addObject("userObj", userDetObj);
                modelAndView.addObject("roleId", roleId);
                modelAndView.addObject("user", loggedUser);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * Controller method to redirect to edit-profile page
     *
     * @param userId id of user to be edited.
     * @param roleId
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/profile/update"})
    public ModelAndView redirectToEditProfile(@RequestParam("userId") Long userId,
            @RequestParam("roleId") Long roleId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Edit User Profile");
        try {
            modelAndView = new ModelAndView();
            if (userService.isAuthenticated()) {
                User loggedUser = userService.getCurrentUser();
                if (userId != null) {
                    User userObj = userService.getUserById(userId);
                    UserDetailsBean userDetObj = new UserDetailsBean(
                            userObj.getId(),
                            userObj.getEnabled(),
                            userObj.getUsername(),
                            userObj.getEmail(),
                            userObj.getFirstName(),
                            userObj.getMiddleName(),
                            userObj.getLastName(),
                            userObj.getContact(),
                            userObj.getAddress(),
                            userObj.getCity(),
                            userObj.getDistrict(),
                            userObj.getState(),
                            userObj.getCountry(),
                            userObj.getGender(),
                            userService.listRoleIdsOfUser(userObj.getId()),
                            userObj.getProfilePicPath());
                    System.out.println("path" + userObj.getProfilePicPath());
                    modelAndView.addObject("userRolesMap", userService.filterInvitedUsersByRoles(loggedUser));//map of all roleIds and names
                    modelAndView.addObject("userObj", userDetObj);
                    Map<String, String> breadcrumbMap = new LinkedHashMap<>();
                    breadcrumbMap.put("Admin Activity", "#");
                    breadcrumbMap.put("Manage Users", "#");
                    breadcrumbMap.put(userService.getRoleByID(roleId).getName(), "user/manage/role/" + roleId + "");
                    modelAndView.addObject("breadcrumbMap", breadcrumbMap);
                    modelAndView.addObject("title", "Edit User Profile");
                    modelAndView.addObject("roleId", roleId);
                    modelAndView.addObject("user", loggedUser);
                }
            }
            modelAndView.setViewName("EditProfileView");
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * Controller method for update user by admin.
     *
     * @param userObj updated profile.
     * @param errors
     * @param request
     * @param userroleids
     * @param selectedRoleId
     * @param roleId
     * @return
     */
    @RequestMapping(value = {"/profile/update"}, method = RequestMethod.POST)
    public RedirectView updateProfileByAdmin(@ModelAttribute UserDetailsBean userObj, BindingResult errors, HttpServletRequest request, @RequestParam("userroleids") String[] userroleids, @RequestParam String selectedRoleId) {
        if (errors.hasErrors()) {
            LOG.error("Error updating profile");
            return new RedirectView("/user/profile/home?error", true);
        }
        Long userId = null;
        System.out.println("roleee" + selectedRoleId);
        if (userService.isAuthenticated()) {
            User loggedUser = userService.getCurrentUser();
            User userObjToUpdate = userService.updateUserByAdmin(userObj, userroleids);
            if (userObjToUpdate != null) {
                userId = userObjToUpdate.getId();
                String upadatedUserFullName = (userObjToUpdate.getFirstName() != null ? userObjToUpdate.getFirstName() : "") + " " + (userObjToUpdate.getMiddleName() != null ? userObjToUpdate.getMiddleName() : "") + " " + (userObjToUpdate.getLastName() != null ? userObjToUpdate.getLastName() : "");
                activityServiceObj.saveOtherActivityOfUser(loggedUser.getId(), userObjToUpdate.getId(), Constants.UserActivityConstant.EDIT_USER_DETAILS, request, loggedUser.getUsername(), upadatedUserFullName);
            }
        }
        return new RedirectView("/user/profile/update?userId=" + userId + "&roleId=" + Long.parseLong("1"), true);
    }

    /**
     * Controller method for redirecting user to update profile page.
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/update"})
    public ModelAndView redirectToYourOwnProfile() {
        //@RequestParam(value = "profileImage", required = false) MultipartFile profileImage
        ModelAndView modelAndView = new ModelAndView("EditOwnProfileView");
        try {
            if (userService.isAuthenticated()) {
                User userObj = userService.getCurrentUser();
                UserDetailsBean userDetObj = new UserDetailsBean(userObj.getId(),
                        userObj.getEnabled(),
                        userObj.getUsername(),
                        userObj.getEmail(),
                        userObj.getFirstName(),
                        userObj.getMiddleName(),
                        userObj.getLastName(),
                        userObj.getContact(),
                        userObj.getAddress(),
                        userObj.getCity(),
                        userObj.getDistrict(),
                        userObj.getState(),
                        userObj.getCountry(),
                        userObj.getGender(),
                        userService.mapOfRoleIdsNameOfUser(userObj.getId()),
                        userObj.getProfilePicPath());
                System.out.println("address" + userDetObj.getAddress());
                modelAndView.addObject("user", userObj);
                Map<String, String> breadcrumbMap = new LinkedHashMap<>();
                breadcrumbMap.put("Profile", "user/profile");
                modelAndView.addObject("breadcrumbMap", breadcrumbMap);
                modelAndView.addObject("title", "Edit User Profile");
                modelAndView.addObject("userDetObj", userDetObj);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * Controller method to update self profile.
     *
     * @param userDetObj
     * @param errors object for spring validation
     * @param httpServletResponse
     * @param request
     * @return RedirectView
     */
    @RequestMapping(value = {"/update"}, method = RequestMethod.POST, headers = {"content-type=multipart/form-data"})
    public RedirectView updateYourOwnProfile(@ModelAttribute UserDetailsBean userDetObj, BindingResult errors, HttpServletResponse httpServletResponse, HttpServletRequest request) {
        try {
            if (errors.hasErrors()) {
                LOG.error("Error updating profile");
                return new RedirectView("/user/update", true);
            }
            if (userService.isAuthenticated()) {
                String relativeUserProfilePicPath = "";
                String imageExtension = "";
                LOG.info("profile image " + userDetObj.getProfileImage().getOriginalFilename() + " size" + userDetObj.getProfileImage().getSize());
                User loggedUser = userService.getCurrentUser();

                if (userDetObj.getProfileImage().getSize() > 0) {
                    userService.validateImage(userDetObj.getProfileImage());
                    imageExtension = userService.getFileExtension(userDetObj.getProfileImage().getOriginalFilename());
                    relativeUserProfilePicPath = loggedUser.getId() + File.separator + profilePicDirectory + File.separator + loggedUser.getId() + "." + imageExtension;
                    LOG.info("image uploaded" + relativeUserProfilePicPath);
                    userService.saveExistingOrNewlyUploadedImageOnDrive(userRelatedDirecory + File.separator + relativeUserProfilePicPath, userDetObj.getProfileImage());
                    userDetObj.setProfilePicPath(relativeUserProfilePicPath);
                } else {
                    LOG.info("image not uploaded");
                }
                userDetObj.setId(loggedUser.getId());
                User updatedUser = userService.updateUserProfile(userDetObj);
                if (updatedUser != null) {
                    activityServiceObj.saveOtherActivityOfUser(loggedUser.getId(), null, Constants.UserActivityConstant.UPDATE_PROFILE, request, loggedUser.getUsername(), null);
                }
            }
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return new RedirectView("/user/update", true);
    }

    /**
     * Controller method to delete the user.
     *
     * @param userId id of user to be deleted.
     * @param request HttpServletRequest
     * @return String
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public String deleteUser(@RequestParam("userId") Long userId, HttpServletRequest request) {
        String userMsg = "";
        try {
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                User user = userService.getUserById(userId);
                if (user != null) {
                    user.setDeleted(Constants.USER_DELETED);
                    userService.updateUser(user);
                    userMsg = messages.getMessage("user.delete.success", null, Locale.getDefault());
                    String upadatedUserFullName = (user.getFirstName() != null ? user.getFirstName() : "") + " " + (user.getMiddleName() != null ? user.getMiddleName() : "") + " " + (user.getLastName() != null ? user.getLastName() : "");
                    activityServiceObj.saveOtherActivityOfUser(loggedUser.getId(), user.getId(), Constants.UserActivityConstant.DELETE_USER, request, loggedUser.getUsername(), upadatedUserFullName);
                } else {
                    userMsg = messages.getMessage("user.delete.error", null, Locale.getDefault());
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return userMsg;
    }

    /**
     * Controller method to activate(set status of user as active) the user by
     * admin.
     *
     * @param userId id of user to be made active.
     * @param request
     * @return
     */
    @RequestMapping(value = "/activate")
    @ResponseBody
    public String activateUser(@RequestParam("userId") Long userId, HttpServletRequest request) {
        String userMsg = "";
        try {
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                User user = userService.getUserById(userId);
                if (user != null) {
                    user.setEnabled(Constants.USER_ACTIVATED);
                    userService.updateUser(user);
                    userMsg = messages.getMessage("user.activate.success", null, Locale.getDefault());
                    String upadatedUserFullName = (user.getFirstName() != null ? user.getFirstName() : "") + " " + (user.getMiddleName() != null ? user.getMiddleName() : "") + " " + (user.getLastName() != null ? user.getLastName() : "");
                    activityServiceObj.saveOtherActivityOfUser(loggedUser.getId(), user.getId(), Constants.UserActivityConstant.ACTIVATE_USER, request, loggedUser.getUsername(), upadatedUserFullName);
                } else {
                    userMsg = messages.getMessage("user.activate.error", null, Locale.getDefault());
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return userMsg;
    }

    /**
     * Controller method to deactivate(set status of user as deactivate) the
     * user by admin.
     *
     * @param userId id of user to be made deactivate.
     * @param request
     * @return
     */
    @RequestMapping(value = "/deactivate")
    @ResponseBody
    public String deactivateUser(@RequestParam("userId") Long userId, HttpServletRequest request) {
        String userMsg = "";
        try {
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                User user = userService.getUserById(userId);
                if (user != null) {
                    user.setEnabled(Constants.USER_DEACTIVATED);
                    userService.updateUser(user);
                    userMsg = messages.getMessage("user.deactivate.success", null, Locale.getDefault());
                    String upadatedUserFullName = (user.getFirstName() != null ? user.getFirstName() : "") + " " + (user.getMiddleName() != null ? user.getMiddleName() : "") + " " + (user.getLastName() != null ? user.getLastName() : "");
                    activityServiceObj.saveOtherActivityOfUser(loggedUser.getId(), user.getId(), Constants.UserActivityConstant.DEACTIVATE_USER, request, loggedUser.getUsername(), upadatedUserFullName);
                } else {
                    userMsg = messages.getMessage("user.deactivate.error", null, Locale.getDefault());
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return userMsg;
    }

    /**
     * Controller method to redirect to manage-professions page.
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/manage/professions", method = RequestMethod.GET)
    public ModelAndView viewManageProfessions() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Manage User Profession Data");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("profession", new ProfessionDTO());
        modelAndView.setViewName("UserProfessionsView");
        return modelAndView;
    }

    /**
     * Controller method to add new profession.
     *
     * @param profession {@link ProfessionDTO}
     * @return
     */
    @RequestMapping(value = "/manage/professions", method = RequestMethod.POST)
    public ModelAndView addNewProfession(@ModelAttribute ProfessionDTO profession) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Manage User Professions Data");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("profession", new ProfessionDTO());
        modelAndView.setViewName("UserProfessionsView");
        return modelAndView;
    }

    /**
     * Controller method to send invitations to user
     *
     * @param request HttpServletRequest
     * @param selected_roleid id of role for which user has been invited.
     * @param useremail mail of invited user
     * @param isUpdate boolean value if true means user is invited second times
     * with more roles permission.
     * @param invitationId id of invitation
     * @return int 1 means success o for unsuccessful invitation
     */
    @RequestMapping(value = "/invite", method = RequestMethod.POST)
    @ResponseBody
    public int inviteUser(
            HttpServletRequest request,
            @RequestParam("selected_roleid") List<Long> selected_roleid,
            @RequestParam("useremail") String useremail,
            @RequestParam("isUpdate") boolean isUpdate,
            @RequestParam("invitationId") Long invitationId) {
        try {
            User loggedInUser = userService.getCurrentUser();
            userService.inviteUser(Constants.UserActivities.INVITE_USER, null, loggedInUser, null, selected_roleid, useremail, ctx, request, request.getServletContext().getContextPath(), isUpdate, invitationId);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
            return 0;
        }
        return 1;
    }

    @RequestMapping(value = "/invite/friends", method = RequestMethod.POST)
    @ResponseBody
    public int inviteUser(HttpServletRequest request, @RequestParam("emailIds") String useremails) {
        try {
            String mailIds[] = useremails.split(";");
            for (String mailId : mailIds) {
                User loggedInUser = userService.getCurrentUser();
                userService.inviteUser(Constants.UserActivities.INVITE_USER, null, loggedInUser, null, Arrays.asList(1L), mailId, ctx, request, request.getServletContext().getContextPath(), false, null);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
            return 0;
        }
        return 1;
    }

    /**
     * Controller method to cancle the invitation.
     *
     * @param request HttpServletRequest
     * @param invitationId id of invitation to be canceled.
     * @return
     */
    @RequestMapping(value = "/cancelInvitaion", method = RequestMethod.GET)
    @ResponseBody
    public int cancelInvitaion(
            HttpServletRequest request,
            @RequestParam("invitationId") Long invitationId
    ) {
        try {
            User user = userService.getCurrentUser();
            userService.cancelInvitedUser(invitationId, request, user.getId(), user.getUsername());

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
            return 0;
        }

        return 1;
    }

    /**
     * Controller method to send reminder to invited user.
     *
     * @param request HttpServletRequest
     * @param useremail mail of invited user
     * @param invitationId id of invited user
     * @return
     */
    @RequestMapping(value = "/sendReminder", method = RequestMethod.POST)
    @ResponseBody
    public int sendReminder(
            HttpServletRequest request,
            @RequestParam("useremail") String useremail,
            @RequestParam("invitationId") Long invitationId) {
        try {
            User loggedInUser = userService.getCurrentUser();
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            String userAgent = request.getHeader("User-Agent");
            if (userAgent == null) {
                userAgent = "";
            }
            RequestAttributes reqAttrObj = new RequestAttributes();
            reqAttrObj.setIpAddress(ipAddress);
            reqAttrObj.setUserAgent(userAgent);
            userService.sentReminder(invitationId, loggedInUser, useremail, ctx, reqAttrObj, request.getServletContext().getContextPath());

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return 1;
    }

    /**
     * Controller method to check if email is already exists in database.
     *
     * @param request HttpServletRequest
     * @param useremail mail to be checked.
     * @return JSONObject containing appropriate message
     */
    @RequestMapping(value = "/checkEmail", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject checkEmail(
            HttpServletRequest request,
            @RequestParam("useremail") String useremail) {
        JSONObject obj = new JSONObject();
        boolean emailExists = userService.checkEmailIFAlredyExist(useremail);
        obj.put("isEmailExists", emailExists);
        if (emailExists) {
            obj.put("message", "User already exists with " + useremail + " address");
        }
        return obj;
    }

    /**
     * Controller method to redirect user to subscriptions page view
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/view/subscriptions")
    public ModelAndView viewSubscriptions() {
        ModelAndView modelAndView = new ModelAndView("UserSubscriptionsListView");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("title", "My Subscription");
        return modelAndView;
    }

    /**
     * Controller method to redirect user to UserSavedLibraries page view
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/saved/libraries")
    public ModelAndView viewSavedLibraries() {
        ModelAndView modelAndView = new ModelAndView("UserSavedLibrariesView");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("title", "My Saved Libraries");
        return modelAndView;
    }

    /**
     * Shows saved user resource interest
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/saved/resource/interest")
    public ModelAndView getResourceInterests() {
        ModelAndView modelAndView = new ModelAndView();
        User loggedIn = userService.getCurrentUser();
        modelAndView.addObject("user", loggedIn);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Favorite Resources");
        modelAndView.setViewName("resource-interest");
        return modelAndView;
    }

    /**
     * Controller method to redirect user to Language Settings page view
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/languages")
    public ModelAndView userLanguageSettings() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "User Language Settings");
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            List<UDCLanguage> languageList = resourceService.getAllUDCLanguages();
            modelAndView.addObject("languageList", languageList);
            LOG.info("Language List my list" + languageList.size());

        } catch (Exception ex) {
            LOG.error("Error while fetching UDC Languages", ex);
        }
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.setViewName("UserLanguageSettingsView");
        return modelAndView;
    }

    /**
     * Controller method to fetch tags of user with paginated.
     *
     * @param pageNumber
     * @param pageWindow
     * @return JSONObject containing tags.
     * @throws Exception
     */
    @RequestMapping(value = "/tags/results/{pageNumber}/{pageWindow}")
    @ResponseBody
    public JSONObject userTagsPaginated(@PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageWindow") int pageWindow) throws Exception {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("tags", getRecordsTitleAndLink(getCustomisedTags(getTagsPaginated(pageNumber, pageWindow))));
        return jSONObject;
    }

    /**
     * Controller method to fetch tags of user with pagination.
     *
     * @return List<CrowdSourceCustomTag> containing tags.
     * @throws Exception
     */
    private List<CrowdSourceCustomTag> getTagsPaginated(int pageNumber, int pageWindow) throws Exception {
        List<CrowdSourceCustomTag> tempList = crowdSourcingService.getuserTagsPaginated(userService.getCurrentUser().getId(), pageNumber, pageWindow);
        return tempList;
    }

    /**
     * Method to fetch tags of user.
     *
     * @return List<CrowdSourceCustomTag> containing tags.
     * @throws Exception
     */
    private List<CustomDashBoard> getCustomisedTags(List<CrowdSourceCustomTag> tempList) {
        List<CustomDashBoard> userTags = new ArrayList<>();
        tempList.parallelStream().forEach((tag) -> {
            CustomDashBoard ctag = new CustomDashBoard();
            ctag.setLink(tag.getRecordIdentifier());
            ctag.setTitle(tag.getRecordIdentifier());
            ctag.setTags(tag.getTags());
            userTags.add(ctag);
        });
        return userTags;
    }

    /**
     * Controller method to redirect user to user-list page.
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/lists")
    public ModelAndView userListsRequest() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My Saved Lists");
        modelAndView.setViewName("UserListsView");
        return modelAndView;
    }

    /**
     * Controller method to redirect user to public lists page.
     *
     */
    @RequestMapping(value = "redirect/publicLists")
    public ModelAndView publicListsRequest() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Public Lists");
        modelAndView.setViewName("PublicListsView");
        return modelAndView;
    }

    /**
     * Controller method to fetch saved list of user.
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/lists/results")
    @ResponseBody
    public List<CustomDashBoard> userListsResult() throws Exception {
        return getLists();
    }

    /**
     * Controller method to get top 3 saved list of user.
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/topLists")
    @ResponseBody
    public List<CustomDashBoard> topLists() throws Exception {
        List<CustomDashBoard> uList = getLists();
        List<CustomDashBoard> limitedList = new ArrayList<>();
        for (int i = 0; i < dashBoardEntriesLimit && i < uList.size(); i++) {
            limitedList.add(uList.get(i));
        }
        return limitedList;
    }

    /**
     * Controller method to fetch saved list of user.
     *
     * @return List<UserList>
     */
    private List<CustomDashBoard> getLists() {
        List<CustomDashBoard> userLists = new ArrayList<>();
        User loggedUser = userService.getCurrentUser();
        if (loggedUser != null) {
            List<UserAddedList> userAddedLists = userAddedListService.getSavedListsByUserPaginated(loggedUser.getId(), 1, 5);
            int i = 0;
            userAddedLists.forEach(list -> {
                CustomDashBoard cList;
                cList = new CustomDashBoard();
                PrettyTime p = new PrettyTime();
                cList.setAddedTime(p.format(list.getActivityTs()));
                cList.setLink(list.getListName());
                cList.setTitle(list.getListName());
                if (list.getPublicPrivate() == 0) {
                    cList.setPrivacy("Public");
                } else if (list.getPublicPrivate() == 1) {
                    cList.setPrivacy("private");
                }
                userLists.add(cList);
            });
        }
        return userLists;
    }

    /**
     * Controller method to direct user to watch list view
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/watch-lists")
    public ModelAndView viewWatchLists() {
        ModelAndView modelAndView = new ModelAndView("UserWatchListsView");
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", " My Watch Lists");
        return modelAndView;
    }

    /**
     * Controller method to fetch top interest shown by user.
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/topInterests")
    @ResponseBody
    public List<CustomDashBoard> topInterests() throws Exception {
        List<UserInterest> tempSet = userService.getCurrentUser().getInterests();
        Collections.sort(tempSet, new UserInterestComparatorByID());
        List<UserInterest> temptopInterests = new ArrayList<>();
        temptopInterests.addAll(tempSet);

        List<CustomDashBoard> topInterestResources = new ArrayList<>();
        CustomDashBoard custObj;

        for (int i = 0; i < dashBoardEntriesLimit && i < temptopInterests.size(); i++) {
            custObj = new CustomDashBoard();
            custObj.setLink("search/show/" + temptopInterests.get(i).getThematicType().getThematicType());
            custObj.setTitle(temptopInterests.get(i).getThematicType().getThematicType());
            topInterestResources.add(custObj);
        }
        return topInterestResources;
    }

    /**
     * Method to fetch top professions of user
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/topProfessions")
    @ResponseBody
    public List<CustomDashBoard> topProfessions() throws Exception {
        Set<UserProfession> tempSet = userService.getCurrentUser().getUserProfessions();
        List<UserProfession> temptopProfessions = new ArrayList<>();
        temptopProfessions.addAll(tempSet);

        List<CustomDashBoard> topInterestResources = new ArrayList<>();
        CustomDashBoard custObj;

        for (int i = 0; i < dashBoardEntriesLimit && i < temptopProfessions.size(); i++) {
            custObj = new CustomDashBoard();
            custObj.setLink("search/show/" + temptopProfessions.get(i).getProfessionName());
            custObj.setTitle(temptopProfessions.get(i).getProfessionName());
            topInterestResources.add(custObj);
        }
        return topInterestResources;
    }

    /**
     * Controller method to fetch top languages of user.
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/topLanguages")
    @ResponseBody
    public List<CustomDashBoard> topLanguages() throws Exception {
        List<UDCLanguage> tempSet = userService.getCurrentUser().getUserLanguages();
        List<UDCLanguage> temptopLanguages = new ArrayList<>();
        temptopLanguages.addAll(tempSet);

        List<CustomDashBoard> topInterestResources = new ArrayList<>();
        CustomDashBoard custObj;

        for (int i = 0; i < dashBoardEntriesLimit && i < temptopLanguages.size(); i++) {
            custObj = new CustomDashBoard();
            custObj.setLink("search/show/" + temptopLanguages.get(i).getLanguageName());
            custObj.setTitle(temptopLanguages.get(i).getLanguageName());
            topInterestResources.add(custObj);
        }
        return topInterestResources;
    }

    /**
     * Controller method to fetch top interest resource.
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/topInterestResources")
    @ResponseBody
    public List<CustomDashBoard> topInterestResources() throws Exception {
        List<ResourceType> tempList = userService.getCurrentUser().getResourceInterest();

        List<CustomDashBoard> topInterestResources = new ArrayList<>();
        CustomDashBoard custObj;
        for (int i = 0; i < dashBoardEntriesLimit && i < tempList.size(); i++) {
            custObj = new CustomDashBoard();
            custObj.setLink("search/show/" + tempList.get(i).getResourceTypeCode());
            custObj.setTitle(tempList.get(i).getResourceType());
            topInterestResources.add(custObj);
        }
        return topInterestResources;
    }

    /**
     * Controller method to fetch languages of user.
     *
     * @return JSONArray
     */
    @RequestMapping(value = "/saved/languages")
    @ResponseBody
    public JSONArray fetchUserLanguages() {
        JSONArray array = new JSONArray();
        List<UDCLanguage> userLanguages = userService.getCurrentUser().getUserLanguages();
        userLanguages.parallelStream().map((userLanguage) -> {
            JSONObject object = new JSONObject();
            object.put("languageCode", userLanguage.getLanguageCode());
            object.put("languageId", userLanguage.getId());
            object.put("languageName", userLanguage.getLanguageName());
            object.put("isIndianLanguage", userLanguage.getIsIndianLanguage());
            return object;
        }).forEach((object) -> {
            array.add(object);
        });
        return array;
    }

    /**
     * Controller method to update languages selected by user.
     *
     * @param action
     * @param id
     * @return JSONObject
     */
    @RequestMapping(value = "/{action}/language/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject updateLanguage(
            @PathVariable(value = "action") String action,
            @PathVariable(value = "id") long id) {
        User user = userService.getCurrentUser();
        List<UDCLanguage> userLanguages = user.getUserLanguages();
        if (action.equalsIgnoreCase("add")) {
            userLanguages.add(resourceService.getUDCLanguageById(id));
        } else if (action.equalsIgnoreCase("rm")) {
            userLanguages.remove(resourceService.getUDCLanguageById(id));
        }
        user.setUserLanguages(userLanguages);
        userService.updateUser(user);
        JSONObject object = new JSONObject();
        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    private List<CustomDashBoard> getRecordsTitleAndLink(List<CustomDashBoard> dashBoardObjects) throws Exception {
        List<CustomDashBoard> UpdatedashBoardObjects = new ArrayList<>();
        List<String> recordIdentifiers = new ArrayList<>();
        for (CustomDashBoard obj : dashBoardObjects) {
            if (obj.getTitle() != null) {
                recordIdentifiers.add(obj.getTitle());
            }
        }
        Map<String, String> recordTitles = UserSearchResultActivity.getRecordsTitle(recordIdentifiers);
        for (CustomDashBoard obj : dashBoardObjects) {
            if (obj.getLink() != null && (obj.getLink().contains("ENEW") || obj.getLink().contains("VALUE")
                    || obj.getLink().contains("WIKI") || obj.getLink().contains("GOVW"))) {
                obj.setLink(obj.getLink());
            } else {
                obj.setLink("search/preview/" + obj.getLink());
            }
            obj.setTitle(recordTitles.get(obj.getTitle()));
            UpdatedashBoardObjects.add(obj);
        }
        return UpdatedashBoardObjects;
    }

    /**
     * Controller method to fetch list content
     *
     * @param listName
     * @param userId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/mylists/{listName}/{userId}")
    @ResponseBody
    public List<CustomDashBoard> getListContents(@PathVariable(value = "listName") String listName, @PathVariable(value = "userId") Long userId) throws Exception {
        if (userId == 0) {
            userId = userService.getCurrentUser().getId();
        }
        List<UserAddedList> userAddedLists = userAddedListService.getListContentByUserIdAndListName(userId, listName);
        List<CustomDashBoard> userListContents = new ArrayList<>();
        CustomDashBoard obj;
        for (UserAddedList userAddedList : userAddedLists) {
            obj = new CustomDashBoard();
            obj.setTitle(userAddedList.getRecordTitle());
            obj.setLink(userAddedList.getRecordIdentifier());
            userListContents.add(obj);
        }
        return userListContents;
    }

    /**
     * Controller method to delete List.
     *
     * @param listName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteMylists/{listName}")
    @ResponseBody
    public int deleteList(HttpServletRequest request, @PathVariable(value = "listName") String listName) throws Exception {
        User user = userService.getCurrentUser();
        //maintain log of activity
        if (user != null) {
            //maintain activity log
            activityLogService.saveLogOfDeleteList(user, listName, request);

            //perform activity
            if (userAddedListService.deleteUserAddedListByUserIdAndListName(user.getId(), listName)) {
                return 1;
            }
        }
        return 2;
    }

    /**
     * Controller method to delete List.
     *
     * @param listName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/dMyListContent/{listName}/{recordIdentifier}/{recordTitle}")
    @ResponseBody
    public int deleteListContent(HttpServletRequest request, @PathVariable(value = "listName") String listName, @PathVariable(value = "recordIdentifier") String recordIdentifier, @PathVariable(value = "recordTitle") String recordTitle) throws Exception {
        User user = userService.getCurrentUser();
        //maintain log of activity
        if (user != null) {
            //maintain activity log
            activityLogService.saveLogOfDeleteListContent(user, listName, recordIdentifier, recordTitle, request);

            //perform activity
            if (userAddedListService.deleteUserAddedListContentByUserIdAndListNameAndRecordId(user.getId(), listName, recordIdentifier)) {
                return 1;
            } else {
                return 2;
            }
        }
        return 2;
    }

    /**
     * Controller method to save or update interest keywords.
     *
     * @param userInterestID
     * @param keywords
     * @return
     */
    @RequestMapping(value = "/interests/keywords/save/{userInterestID}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject saveOrUpdateInterestKeywords(
            @PathVariable("userInterestID") long userInterestID,
            @RequestParam("keywords") String keywords) {
        JSONObject object = new JSONObject();
        UserInterest interest = userService.getUserInterestByID(userInterestID);
        interest.setKeywords(keywords.toString());
        userService.saveUpdateUserInterest(interest);
        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    /**
     * Controller method to redirect user to suggested ebook page.
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/suggestedebooks")
    public ModelAndView viewSuggestedEBooks() {
        ModelAndView modelAndView = new ModelAndView("UserSuggestedEBooksView");
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My Free e-Books");
        return modelAndView;
    }

    /**
     * Controller method to get suggestions of books.
     *
     * @return ModelAndView
     */
    @RequestMapping(value = "/suggestedebooks/results")
    @ResponseBody
    public List<CustomDashBoard> suggestedEBooks() throws Exception {
        return getCustomisedSuggestedEBooks(getSuggestedBooks());
    }

    /**
     * Method to fetch suggested ebook for user.
     *
     * @return List<Ebooks>
     * @throws Exception
     */
    private List<Ebooks> getSuggestedBooks() throws Exception {
        List<UserInterest> userInterests = userService.getCurrentUser().getInterests();
        Collections.sort(userInterests, new UserInterestComparatorByID());
        List<String> keywords = new ArrayList<>();
        userInterests.stream().map((UserInterest interest) -> {
            keywords.add(interest.getThematicType().getThematicType());
            return interest;
        }).filter((interest) -> (interest.getKeywords() != null)).forEach((interest) -> {
            if (interest.getKeywords().contains(",")) {
                keywords.addAll(Arrays.asList(interest.getKeywords().split(",")));
            } else {
                keywords.add(interest.getKeywords());
            }
        });
        List<Ebooks> ebooks = null;
        if (keywords.size() > 0) {
            Gson gson = new Gson();
            ObjectMapper mapper = new ObjectMapper();
            ebooks = mapper.readValue(gson.toJson(UserSearchResultActivity.getSuggestedBooks(keywords)), new TypeReference<List<Ebooks>>() {
            });
        }
        return ebooks;
    }

    /**
     * Method to get suggested ebook in customised form.
     *
     * @param tempList
     * @return
     */
    private List<CustomDashBoard> getCustomisedSuggestedEBooks(List<Ebooks> tempList) {
        List<UserEBook> eBooks = userEBookService.findUserEBooksByUserId(userService.getCurrentUser().getId());
        List<Ebooks> updateTempList = new ArrayList<>();
        if (tempList != null) {
            for (Ebooks eBook : tempList) {
                if (checkEBookAlreadySavedOrNot(eBooks, eBook.getRecordIdentifier())) {
                    updateTempList.add(eBook);
                }
            }
        }

        List<CustomDashBoard> userSuggestedBooks = new ArrayList<>();
        CustomDashBoard cBook;
        if (tempList != null) {
            for (Ebooks book : updateTempList) {
                cBook = new CustomDashBoard();
                cBook.setLink("search/preview/" + book.getRecordIdentifier());
                cBook.setTitle(book.getTitleOrSubject());
                cBook.setIdentifier(book.getRecordIdentifier());
                userSuggestedBooks.add(cBook);
            }
        }
        return userSuggestedBooks;
    }

    /**
     * Controller method to add ebook to user acccount.
     *
     * @param req HttpServletRequest
     * @param identifier
     * @return String success or error
     */
    @RequestMapping(value = "ebooks/{identifier}/{title}", method = RequestMethod.POST)
    @ResponseBody
    public String addMyEBook(HttpServletRequest request, @PathVariable(value = "identifier") String identifier, @PathVariable(value = "title") String title) {
        try {
            User userObj = userService.getCurrentUser();
            if (userObj != null) {
                if (activityServiceObj.saveUserEBookActivity(Constants.UserActivityConstant.ADD_EBOOK, identifier, title, userObj, null, request)) {
                    return "success";
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "error";
    }

    /**
     * Controller method to delete user ebook.
     *
     * @param recordIdentifier
     * @param title
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/d/ebook")
    public String deleteMyEBook(@RequestParam(value = "recordIdentifier") String recordIdentifier, @RequestParam(value = "title") String title, HttpServletRequest request) {
        try {
            User userObj = userService.getCurrentUser();
            if (userObj != null) {
                if (activityServiceObj.deleteEbook(userObj, recordIdentifier, request, title)) {
                    return "success";
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return "error";
    }

//    @RequestMapping("/delete-redis-cache/{filter}")
//    @ResponseBody
//    public String deleteRedisCachesByUserIdAndFilter(@PathVariable("filter") String filter) {
//        userActivityService.deleteRedisCachesByUserIdAndFilter(userService.getCurrentUser().getId(), filter);
//        return "success";
//    }
    /**
     * Method to check if Book is already saved or not
     *
     * @param userEbooks
     * @param identifier
     * @return
     */
    private boolean checkEBookAlreadySavedOrNot(List<UserEBook> userEbooks, String identifier) {
        for (UserEBook userEbook : userEbooks) {
            if (userEbook.getRecordIdentifier().equals(identifier)) {
                return false;
            }
        }
        return true;
    }

    @RequestMapping(value = "/ebooks")
    public ModelAndView viewEbooks() throws Exception {
        ModelAndView modelAndView = new ModelAndView("UserEbooksView");
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My Saved e-Books");
        return modelAndView;
    }

    /**
     * Controller method to fetch ebooks results by pagination.
     *
     * @param pageNumber current page number
     * @param pageWindow max entries to be shown on each page.
     * @return JSONObject.
     * @throws Exception
     */
    @RequestMapping(value = "/ebooks/results/{pageNumber}/{pageWindow}")
    @ResponseBody
    public JSONObject userEBooksPaginated(@PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageWindow") int pageWindow) throws Exception {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("suggestions", getRecordsTitleAndLink(getCustomisedEBooks(getUserEBooksPaginated(pageNumber, pageWindow))));
        return jSONObject;
    }

    /**
     * Method to get user e books paginated.
     *
     * @param pageNumber
     * @param pageWindow
     * @return
     * @throws Exception
     */
    private List<UserEBook> getUserEBooksPaginated(int pageNumber, int pageWindow) throws Exception {
        return userEBookService.getEBooksOfUserByUserId(userService.getCurrentUser().getId(), pageNumber, pageWindow);
    }

    /**
     * Method to get ebooks in customised form.
     *
     * @param tempList
     * @return userEBooks
     */
    private List<CustomDashBoard> getCustomisedEBooks(List<UserEBook> tempList) {

        List<CustomDashBoard> userEBooks = new ArrayList<>();
        CustomDashBoard cEBook;
        for (UserEBook ebook : tempList) {
            cEBook = new CustomDashBoard();
            PrettyTime p = new PrettyTime();
            cEBook.setAddedTime(ebook.getAddedTime() != null ? p.format(ebook.getAddedTime()) : null);
            cEBook.setLink(ebook.getRecordIdentifier());
            cEBook.setTitle(ebook.getRecordIdentifier());
            cEBook.setIdentifier(ebook.getRecordIdentifier());
            userEBooks.add(cEBook);
        }
        return userEBooks;
    }

    /**
     * Method to get invited users.
     *
     * @return List<CustomUsersLists>
     */
    @RequestMapping(value = "/invited-users")
    @ResponseBody
    public List<CustomUsersLists> getInvitedUsers() {
        List<CustomUsersLists> list = new ArrayList<>();
        try {
            list = userService.getInvitedUsers(userService.getCurrentUser().getId());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Controller method to redirect user to theme-preference page.
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/theme-preference")
    public ModelAndView themePreference() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("user", userService.getCurrentUser());
        mav.addObject("themes", themeService.listPersonalThemes());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Themes");
        mav.setViewName("ThemePreferenceView");
        return mav;
    }

    /**
     * Controller method to save theme for user.
     *
     * @param themeId id of theme which is to be set.
     * @param request
     * @return
     */
    @RequestMapping(value = "/save/theme/{themeId}", method = RequestMethod.POST)
    @ResponseBody
    public UserThemeDTO saveUserTheme(
            @PathVariable(value = "themeId") long themeId,
            HttpServletRequest request) {
        UserTheme theme = themeService.findById(themeId);
        User currentUser = userService.getCurrentUser();
        currentUser.setTheme(theme);
        userService.updateUser(currentUser);
        request.getSession().setAttribute("user", currentUser);
        return new UserThemeDTO(theme);
    }

    /**
     * Method to get user specialization.
     *
     * @return ModelAndView object.
     */
    @RequestMapping(value = "specialization")
    public ModelAndView userSpecialization() {
        ModelAndView modelAndView = new ModelAndView("UserSpecializationView");
        try {
            Map<Long, String> udcParentMap = new LinkedHashMap<>();
            TreeSet<UDCConcept> udcConceptSet = new TreeSet<>(crowdSourcingService.getUDCConcept(Long.valueOf("2450")).getUdcConceptSet());
            udcConceptSet.parallelStream().forEach((udcConcept) -> {
                udcParentMap.put(udcConcept.getId(), udcConcept.getUdcNotation());
            });
            modelAndView.addObject("user", userService.getCurrentUser());
            udcParentMap.put(2450l, "ALL");
            modelAndView.addObject("udcParentMap", udcParentMap);
            TreeSet languageSet = new TreeSet(UDCLanguage.LanguageComparator);
            languageSet.addAll(crowdSourcingService.getIndianLanguageList());
            modelAndView.addObject("languageList", languageSet);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
        }
        return modelAndView;
    }

    /**
     * Controller method to update users specialization.
     *
     * @param action
     * @param id
     */
    @RequestMapping(value = "updateUserSpecialization/{action}/{id}")
    @ResponseBody
    public void updateUserSpecialization(@PathVariable(value = "action") String action, @PathVariable(value = "id") long id) {
        User user = userService.getCurrentUser();
        List<UDCConcept> userSpecializations = user.getUserSpecializations();
        if (action.equalsIgnoreCase("add")) {
            userSpecializations.add(crowdSourcingService.getUDCConcept(id));
        } else if (action.equalsIgnoreCase("remove")) {
            userSpecializations.remove(crowdSourcingService.getUDCConcept(id));
        }
        user.setUserSpecializations(userSpecializations);
        userService.updateUser(user);
    }

    /**
     * Controller method to fetch users specialization.
     *
     * @param languageId
     * @return List<UDCConceptDescription>
     * @throws Exception
     */
    @RequestMapping(value = "fetchUserSpecialization/{languageId}")
    @ResponseBody
    public List<UDCConceptDescription> fetchUserSpecialization(@PathVariable(value = "languageId") long languageId) throws Exception {
        User user = userService.getCurrentUser();
        List<UDCConcept> userSpecializations = user.getUserSpecializations();
        List<UDCConceptDescription> userSpecializationsWithDescription = new ArrayList<>();
        UDCConceptDescription udcConceptDescription;
        UDCConcept udcConcept;

        List<String> notations = new ArrayList<>();
        userSpecializations.parallelStream().forEach((userSpecialization) -> {
            notations.add(userSpecialization.getUdcNotation());
        });
        if (!notations.isEmpty()) {
            Map<String, String> map = crowdSourcingService.getUDCTagDescription(notations, languageId, true);
            for (UDCConcept userSpecialization : userSpecializations) {
                udcConceptDescription = new UDCConceptDescription();
                udcConcept = new UDCConcept();
                udcConcept.setId(userSpecialization.getId());
                udcConcept.setUdcNotation(userSpecialization.getUdcNotation());
                udcConceptDescription.setUdcConceptId(udcConcept);
                udcConceptDescription.setDescription("[" + userSpecialization.getUdcNotation() + "] " + map.get(userSpecialization.getUdcNotation()));
                userSpecializationsWithDescription.add(udcConceptDescription);
            }
        }
        return userSpecializationsWithDescription;
    }

    /**
     * Controller method to redirect user to e-news view.
     *
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/e-news")
    public ModelAndView showEnews() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("user", userService.getCurrentUser());
        mav.setViewName("UserENewsView");
        return mav;
    }

    /**
     * Controller method to fetch notifications with pagination.
     *
     * @param pageNo
     * @param maxPerPage
     * @return List<Notifications>
     */
    @RequestMapping(value = "/notifs/{pageNo}/{maxPerPage}")
    @ResponseBody
    public List<Notifications> fetchUserNotifs(
            @PathVariable(value = "pageNo") int pageNo,
            @PathVariable(value = "maxPerPage") int maxPerPage) {
        //return notificationService.getUserNotifsByUser(userService.getCurrentUser(), maxPerPage, pageNo);
        return notificationService.getUserNotifsByUser(userService.getCurrentUser().getId(), maxPerPage, pageNo);
    }

    /**
     *
     * @return unread notification count of current user
     */
    @RequestMapping(value = "/notifs/unread")
    @ResponseBody
    public HashMap<String, Object> fetchUserNotifs() {
        return notificationService.getUnreadNotifCount(userService.getCurrentUser().getId(), false);

    }

    /**
     * Method to set notification as read.
     *
     * @param notificationId
     * @return
     */
    @RequestMapping(value = "/notifs/read/{notifId}")
    @ResponseBody
    public boolean markReadUserNotifs(
            @PathVariable(value = "notifId") String notificationId) {
        return notificationService.markNotificationAsRead(notificationId);
    }

    /**
     * Controller method to fetch all notifications.
     *
     * @return
     */
    @RequestMapping(value = "/notifications")
    public ModelAndView showAllNotifications() {
        ModelAndView mav = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Notifications");
        mav.setViewName("UserNotificationsView");
        mav.addObject("user", userService.getCurrentUser());
        mav.addObject("notifications", notificationService.getUserNotifsByUser(userService.getCurrentUser().getId(), 10, 1));
        return mav;
    }

    /**
     * Controller method to redirect user to notification subscription page.
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/notifications-subscription")
    public ModelAndView notificationsSubscription() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("UserNotificationsSubsciptionView");
        mav.addObject("user", userService.getCurrentUser());
        return mav;
    }

    /**
     * Controller method to fetch top specialization.
     *
     * @return List<CustomDashBoard>
     * @throws Exception
     */
    @RequestMapping(value = "/topSpecializations")
    @ResponseBody
    public List<CustomDashBoard> topSpecialization() throws Exception {
        long languageId = 40;
        User user = userService.getCurrentUser();
        List<UDCConcept> userSpecializations = user.getUserSpecializations();
        List<UDCConceptDescription> userSpecializationsWithDescription = new ArrayList<>();
        UDCConceptDescription udcConceptDescription;
        UDCConcept udcConcept;

        List<String> notations = new ArrayList<>();
        userSpecializations.parallelStream().forEach((userSpecialization) -> {
            notations.add(userSpecialization.getUdcNotation());
        });
        if (!notations.isEmpty()) {

            Map<String, String> map = crowdSourcingService.getUDCTagDescription(notations, languageId, true);
            for (UDCConcept userSpecialization : userSpecializations) {
                udcConceptDescription = new UDCConceptDescription();
                udcConcept = new UDCConcept();
                udcConcept.setId(userSpecialization.getId());
                udcConcept.setUdcNotation(userSpecialization.getUdcNotation());
                udcConceptDescription.setUdcConceptId(udcConcept);
                udcConceptDescription.setDescription("[" + userSpecialization.getUdcNotation() + "] " + map.get(userSpecialization.getUdcNotation()));
                userSpecializationsWithDescription.add(udcConceptDescription);
            }
        }
        return getCustomisedSpecializations(userSpecializationsWithDescription);

    }

    /**
     * Controller method specialization in customised form.
     *
     * @param tempList
     * @return
     */
    private List<CustomDashBoard> getCustomisedSpecializations(List<UDCConceptDescription> tempList) {
        List<CustomDashBoard> userSpecializations = new ArrayList<>();
        tempList.parallelStream().forEach((specialization) -> {
            CustomDashBoard cSpecialization = new CustomDashBoard();
            //cEBook.setLink(ebook.getUniqueRecordIdentifier());
            cSpecialization.setTitle(specialization.getDescription());
            userSpecializations.add(cSpecialization);

        });
        return userSpecializations;
    }

    /**
     * Controller method for redirecting user to social setting page.
     *
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/social/settings")
    public ModelAndView socialSettings() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "Social Settings");
        HashMap<String, socialDTO> mapp = new HashMap<>();
        List<SocialSite> socialSitesList = socialSiteDAO.list();
        socialSitesList.parallelStream().forEach((socialSite) -> {
            mapp.put(socialSite.getProviderId(), new socialDTO(null, false));
        });
        List<socialSettingDTO> userconlist = userService.socialSettingService();
        userconlist.parallelStream().forEach((settingDTO) -> {
            mapp.put(settingDTO.getProviderId(), new socialDTO(settingDTO.getProviderUserId(), settingDTO.getConnectedStatus()));
        });//        for (String key : mapp.keySet()) {
//            socialDTO value = mapp.get(key);
//            System.out.println("Key = " + key + ", Value = " + value.getProviderUserId() + "'" + value.getConnectedStatus());
//        }

        modelAndView.addObject("connections", mapp);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.setViewName("UserSocialSettingsView");
        return modelAndView;
    }

    /**
     * Controller method to redirect user to library-expert view page.
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/library/experts")
    public ModelAndView libraryExperts() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("LibraryExpertsView");
        mav.addObject("user", userService.getCurrentUser());
        mav.addObject("allUsers", userService.getUsersByRoleCode("LIB_USER"));
        return mav;
    }

    /**
     * Controller method to redirect user to curators view page.
     *
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/curators")
    public ModelAndView curators() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("CuratorsView");
        mav.addObject("user", userService.getCurrentUser());
        mav.addObject("allUsers", userService.getUsersByRoleCode("CURATOR_USER"));
        return mav;
    }

    /*Author-Nitin Karale
     Date - 15 May 2017
     */
    @RequestMapping(value = "transaction")
    public ModelAndView userTransaction() {
        ModelAndView modelAndView = new ModelAndView("UserTransctionView");
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My Transaction");
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            List<UserTransaction> userTransactionList = paymentWSHelper.fecthUserTransaction(userService.getCurrentUser().getId(), 1, 10);
            modelAndView.addObject("userTransactionList", userTransactionList);
            modelAndView.addObject("user", userService.getCurrentUser());
            modelAndView.addObject("hasError", false);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            modelAndView.addObject("hasError", true);
        }
        return modelAndView;
    }

    /**
     * Controller method to get transactions by filter with limit.
     *
     * @param dataLimit
     * @param pageNo
     * @return List<UserTransaction>
     * @throws Exception
     */
    @RequestMapping(value = "getTransctionByFiltersWithLimit")
    @ResponseBody
    public List<UserTransaction> getTransctionByFiltersWithLimit(@RequestParam int dataLimit, @RequestParam int pageNo) throws Exception {
        List<UserTransaction> userTransactionList;
        try {
            userTransactionList = paymentWSHelper.fecthUserTransaction(userService.getCurrentUser().getId(), pageNo, dataLimit);
            return userTransactionList;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw new Exception("Error occurred while calling user transaction web service");
        }

    }
}
