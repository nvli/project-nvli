/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.beans.crowdsource.ArtistImage;
import in.gov.nvli.beans.crowdsource.ArtistProfileFormBean;
import in.gov.nvli.beans.crowdsource.CuisineDetail;
import in.gov.nvli.beans.crowdsource.CuisineDetailFormBean;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.MetadataTypeHelper;
import in.gov.nvli.webserviceclient.crowdsource.CrowdsourceWebserviceHelper;
import in.gov.nvli.webserviceclient.crowdsource.MetadataStandardResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 * @author richa
 */
@Controller
@RequestMapping(value = "editresource")
public class EditResourceController {

    private final static Logger LOGGER = Logger.getLogger(EditResourceController.class);

    @Value("${nvli.record.preview.tif.base.location}")
    private String path;

    @Autowired
    private CrowdsourceWebserviceHelper crowdsourceWebserviceHelper;

    private final String ARTISTPROFILEIMAGETYPE = "Profile Pic";
    private final String ARTISTPORTFOLIOIMAGETYPE = "Portfolio Image";
    private final String ARTISTIMAGENAME = "artist_";
    private final String RECIPEIMAGENAME = "recipe_";
    private final String SUFFIX = ".jpg";
    private final String PORTFOLIONAME = "portfolio_";

    /**
     * 
     * @param recordIdentifier
     * @param model
     * @return 
     */
    @RequestMapping(value = "showartist/{recordIdentifier:.*}")
    public ModelAndView artistShowProfile(@PathVariable String recordIdentifier, Model model) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ArtistEditProfileView");
        try {
            MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);
            String metadataJSONString = MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata());
            in.gov.nvli.beans.crowdsource.ArtistProfile artistProfile = MetadataTypeHelper.getArtistProfileFromMetadataJSONString(metadataJSONString);
            ArtistProfileFormBean artistProfileFormBean = new ArtistProfileFormBean(metadataStandardResponse.getRecordIdentifier(), artistProfile);
            model.addAttribute("artist", artistProfileFormBean);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * 
     * @param artistProfile
     * @param profilePic
     * @param portfolioUpload
     * @param result
     * @param request
     * @return 
     */
    @RequestMapping(value = "/editartist", method = RequestMethod.POST)
    public ModelAndView artistEditProfile(@ModelAttribute("artist") ArtistProfileFormBean artistProfile, @RequestParam(required = false) String profilePic, @RequestParam(required = false) String portfolioUpload, BindingResult result, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/editresource/showartist/" + artistProfile.getRecordIdentifier()));

        try {
            String profileImageName = ARTISTIMAGENAME + artistProfile.getRecordIdentifier() + SUFFIX;
            ArtistImage artistProfileImage = null;
            List<ArtistImage> artistImages = null;
            if (artistProfile.getArtistProfile().getArtistImages() != null) {
                artistImages = artistProfile.getArtistProfile().getArtistImages();
            } else {
                artistImages = new ArrayList<ArtistImage>();
            }
            if (!"".equals(profilePic) && profilePic != null && !profilePic.isEmpty()) {
                craeteImagefromBase64(profilePic, path + artistProfile.getArtistProfile().getImageUploadPath() + File.separator + profileImageName);
                if (artistProfile.getArtistProfile().getArtistImages() != null) {
                    for (ArtistImage artistImage : artistProfile.getArtistProfile().getArtistImages()) {
                        if ("Profile Pic".equals(artistImage.getImageType())) {
                            artistProfileImage = artistImage;
                            break;
                        }
                    }
                }

                if (artistProfileImage == null) {
                    artistProfileImage = new ArtistImage(profileImageName, "", null, "", ARTISTPROFILEIMAGETYPE);
                    artistImages.add(artistProfileImage);
                }
            }

            if (!"".equals(portfolioUpload) && portfolioUpload != null && !portfolioUpload.isEmpty()) {
                JSONObject jsnobject = new JSONObject(portfolioUpload);
                JSONArray jsonArray = jsnobject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    String portFolioImageName = PORTFOLIONAME + isPortfolioImageExists(path + artistProfile.getArtistProfile().getImageUploadPath(), i) + SUFFIX;
                    JSONObject explrObject = jsonArray.getJSONObject(i);
                    craeteImagefromBase64(explrObject.getString("img"), path + artistProfile.getArtistProfile().getImageUploadPath() + File.separator + portFolioImageName);
                    ArtistImage artistImage = new ArtistImage(portFolioImageName, explrObject.getString("name"), null, explrObject.getString("desc"), ARTISTPORTFOLIOIMAGETYPE);
                    artistImages.add(artistImage);
                }
            }

            artistProfile.getArtistProfile().setArtistImages(artistImages);
            MetadataStandardResponse metadataStandardResponse = new MetadataStandardResponse();
            metadataStandardResponse.setIsCloseForEdit("no");
            metadataStandardResponse.setMetadata(artistProfile.getArtistProfile());
            metadataStandardResponse.setPath(null);
            metadataStandardResponse.setRecordIdentifier(artistProfile.getRecordIdentifier());
            metadataStandardResponse.setMetadataStandard(Constants.MetadataStandardType.ARTIST_PROFILE);
            crowdsourceWebserviceHelper.saveMetadataStandardResponse(metadataStandardResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * 
     * @param imagePath
     * @param uniqueValue
     * @return 
     */
    private int isPortfolioImageExists(String imagePath, int uniqueValue) {
        for (int i = uniqueValue;; i++) {
            if (!new File(imagePath + File.separator + PORTFOLIONAME + i + SUFFIX).exists()) {
                return i;
            }
        }
    }

    /**
     * 
     * @param recordIdentifier
     * @param model
     * @return 
     */
    @RequestMapping(value = "showcuisine/{recordIdentifier:.*}")
    public ModelAndView cuisineShow(@PathVariable String recordIdentifier, Model model) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("EditCuisine");
        try {
            String path = null;
            MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);
            String metadataJSONString = MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata());
            CuisineDetail cuisineDetail = MetadataTypeHelper.getIndianRecipeFromMetadataJSONString(metadataJSONString);
            List<String> pathList = metadataStandardResponse.getPath();
            if (pathList != null && !pathList.isEmpty()) {
                path = pathList.get(0);
            }
            CuisineDetailFormBean cuisineDetailFormBean = new CuisineDetailFormBean(recordIdentifier, cuisineDetail, path);
            model.addAttribute("cuisines", cuisineDetailFormBean);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * 
     * @param cuisineDetailFormBean
     * @param recipeImage
     * @param result
     * @param request
     * @return 
     */
    @RequestMapping(value = "/editrecipe", method = RequestMethod.POST)
    public ModelAndView editRecipe(@ModelAttribute("cuisines") CuisineDetailFormBean cuisineDetailFormBean, @RequestParam(required = false) String recipeImage, BindingResult result, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/editresource/showcuisine/" + cuisineDetailFormBean.getRecordIdentifier()));

        try {
            String recipeImageName = null;

            if (cuisineDetailFormBean.getCuisineDetail().getImageName() != null && cuisineDetailFormBean.getCuisineDetail().getImageName() != "" && !cuisineDetailFormBean.getCuisineDetail().getImageName().isEmpty()) {
                recipeImageName = RECIPEIMAGENAME + cuisineDetailFormBean.getCuisineDetail().getCuisineName() + SUFFIX;
            } else {
                recipeImageName = cuisineDetailFormBean.getCuisineDetail().getImageName();
            }

            if (!"".equals(recipeImage) && recipeImage != null && !recipeImage.isEmpty()) {
                craeteImagefromBase64(recipeImage, path + cuisineDetailFormBean.getCuisineDetail().getImageUploadPath() + File.separator + recipeImageName);
                cuisineDetailFormBean.getCuisineDetail().setImageName(recipeImageName);
            }

            MetadataStandardResponse metadataStandardResponse = new MetadataStandardResponse();
            metadataStandardResponse.setIsCloseForEdit("No");
            metadataStandardResponse.setMetadata(cuisineDetailFormBean.getCuisineDetail());
            metadataStandardResponse.setRecordIdentifier(cuisineDetailFormBean.getRecordIdentifier());
            metadataStandardResponse.setMetadataStandard(Constants.MetadataStandardType.INDIAN_RECIPES);
            crowdsourceWebserviceHelper.saveMetadataStandardResponse(metadataStandardResponse);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * 
     * @param base64String
     * @param filePath
     * @throws IOException 
     */
    private void craeteImagefromBase64(String base64String, String filePath) throws IOException {

        String imageCode = base64String;
        String base64Image = imageCode.split(",")[1];

        // Convert the image code to bytes.
        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);

        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imageBytes));

        File imageFile = new File(filePath);

        ImageIO.write(bufferedImage, "jpg", imageFile);

    }

    /**
     * 
     * @param recordIdentifier
     * @param request
     * @return 
     */
    @RequestMapping(value = "removeImage/{recordIdentifier:.*}", method = RequestMethod.POST)
    public @ResponseBody
    String removePortfolioImage(@PathVariable String recordIdentifier, HttpServletRequest request) {
        try {
            ArtistImage artistImage = new ObjectMapper().readValue(request.getParameter("artistImage"), ArtistImage.class);
            if (artistImage != null) {
                if (crowdsourceWebserviceHelper.deleteArtistImage(artistImage, recordIdentifier)) {
                    File imageFile = new File(path + request.getParameter("imageUploadPath") + File.separator + artistImage.getImageName());
                    if (imageFile.exists()) {
                        imageFile.delete();
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.error(ex.getMessage(), ex);
        }
        return "Portfolio removed successfully";
    }

    /**
     * 
     * @param response 
     */
    @RequestMapping(value = "/getartistimage")
    public void getArtistImage(HttpServletResponse response) {

        response.setContentType("image/jpeg");
        File imagefilePath = new File(path + "/Cuisine/Kerala/1/recipe_1.jpg");
        if (imagefilePath.exists()) {
            try (FileInputStream fin = new FileInputStream(imagefilePath);
                    OutputStream outputStream = response.getOutputStream()) {
                byte[] buffer = new byte[fin.available()];
                fin.read(buffer);
                outputStream.write(buffer);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 
     * @param request
     * @return 
     */
    @RequestMapping(value = "/editporfolio", method = RequestMethod.POST)
    public ModelAndView editPortfolio(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/editresource/showartist/" + request.getParameter("recordIdentifier")));

        try {
            ArtistImage artistImage = new ArtistImage();
            artistImage.setImageName(request.getParameter("imageName"));
            artistImage.setImageTitle(request.getParameter("imageTitle"));
            artistImage.setImageDescription(request.getParameter("imageDescription"));
            artistImage.setImagePath(request.getParameter("imagePath"));
            artistImage.setImageType(request.getParameter("imageType"));
            crowdsourceWebserviceHelper.editArtistImage(artistImage, request.getParameter("recordIdentifier"));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }
}
