package in.gov.nvli.controller;

import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.user.NewsPaper;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.service.NewsArticleService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import org.springframework.security.access.prepost.PreAuthorize;
import in.gov.nvli.webserviceclient.news.NewsArticleWebserviceHelper;
import in.gov.nvli.webserviceclient.news.NewsData;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for handling all news papers and news channels related requests.
 *
 * @author Ritesh
 */
@RestController
@RequestMapping(value = {"nac", "user"})
public class NewsArticleController {

    private static final Logger LOG = Logger.getLogger(NewsArticleController.class);

    @Autowired
    private NewsArticleWebserviceHelper newsArticleWebserviceHelper;

    @Autowired
    private UserService userService;

    @Autowired
    private NewsArticleService newsArticleService;

    @Autowired
    private CrowdSourcingService crowdSourcingService;

    @RequestMapping(value = "google-news")
    @ResponseBody
    public List<NewsData> fetchGoogleNews(@RequestParam String languageCode, @RequestParam String categoryName) {
        try {
            return newsArticleWebserviceHelper.fetchGoogleNews(languageCode, categoryName);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/subscribe-news")
    public ModelAndView showSubscribeNews() {
        ModelAndView modelAndView = new ModelAndView();
        TreeSet languageSet = new TreeSet(UDCLanguage.LanguageComparator);
        try {
            languageSet.addAll(crowdSourcingService.getIndianLanguageList());
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        modelAndView.addObject("languageList", languageSet);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "News Pappers");
        modelAndView.setViewName("NewsArticleSubscribeView");
        return modelAndView;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/fetch-all-news-papers")
    @ResponseBody
    public List<NewsPaper> fetchAllNewsPapers(@RequestParam String paperLang) throws Exception {
        return newsArticleService.getNewsPapers(paperLang, Boolean.FALSE);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/update-user-news-paper")
    @ResponseBody
    public JSONObject updateUserNewsPaper(@RequestParam String action, @RequestParam String paperCode) {
        User user = userService.getCurrentUser();
        Set<NewsPaper> newsPapers = user.getNewsPapers();
        if (action.equalsIgnoreCase("add")) {
            newsPapers.add(newsArticleService.getNewsPaper(paperCode));
        } else if (action.equalsIgnoreCase("remove")) {
            newsPapers.remove(newsArticleService.getNewsPaper(paperCode));
        }
        user.setNewsPapers(newsPapers);
        userService.updateUser(user);
        JSONObject object = new JSONObject();
        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/news-feed")
    public ModelAndView showNewsFeed() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My News Pappers");
        modelAndView.setViewName("NewsArticleFeedView");
        return modelAndView;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "fetch-news-article-feeds")
    @ResponseBody
    public List<NewsData> fetchNewsArticleFeeds(@RequestParam String paperCode) {
        try {
            return newsArticleWebserviceHelper.fetchNewsArticleFeed(paperCode);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "fetch-user-subscribed-news-article")
    @ResponseBody
    public List<NewsPaper> fetchUserSubscribedNewsArticle() {
        Set<NewsPaper> newsPapers = userService.getCurrentUser().getNewsPapers();
        List<NewsPaper> newsPapersList = newsPapers.stream().filter(np -> {
            return !np.isNewsType();
        }).collect(Collectors.toList());
        return newsPapersList;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/subscribe-news-channels")
    public ModelAndView showSubscribeNewsChannels() {
        ModelAndView modelAndView = new ModelAndView();
        TreeSet languageSet = new TreeSet(UDCLanguage.LanguageComparator);
        try {
            languageSet.addAll(crowdSourcingService.getIndianLanguageList());
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        modelAndView.addObject("languageList", languageSet);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "News Channels");
        modelAndView.setViewName("NewsChannelSubscribeView");
        return modelAndView;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/fetch-all-news-channels")
    @ResponseBody
    public List<NewsPaper> fetchAllNewsChannels(@RequestParam String paperLang) throws Exception {
        return newsArticleService.getNewsPapers(paperLang, Boolean.TRUE);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/update-user-news-channel")
    @ResponseBody
    public JSONObject updateUserNewsChannel(@RequestParam String action, @RequestParam String paperCode) {
        User user = userService.getCurrentUser();
        Set<NewsPaper> newsPapers = user.getNewsPapers();
        if (action.equalsIgnoreCase("add")) {
            newsPapers.add(newsArticleService.getNewsPaper(paperCode));
        } else if (action.equalsIgnoreCase("remove")) {
            newsPapers.remove(newsArticleService.getNewsPaper(paperCode));
        }
        user.setNewsPapers(newsPapers);
        userService.updateUser(user);
        JSONObject object = new JSONObject();
        object.put("status", Constants.AjaxResponseCode.SUCCESS);
        return object;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/news-channel")
    public ModelAndView showNewsChannel() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My News Channels");
        modelAndView.setViewName("NewsChannelView");
        return modelAndView;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "fetch-news-channels")
    @ResponseBody
    public String fetchNewsChannels(@RequestParam String paperCode) {
        try {
            String newsChannelData = newsArticleWebserviceHelper.fetchNewsChannels(paperCode);
            return newsChannelData;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "fetch-user-subscribed-news-channels")
    @ResponseBody
    public List<NewsPaper> fetchUserSubscribedNewsChannels() {
        Set<NewsPaper> newsChannels = userService.getCurrentUser().getNewsPapers();
        List<NewsPaper> newsChannelsList = newsChannels.stream().filter(np -> {
            return np.isNewsType();
        }).collect(Collectors.toList());
        return newsChannelsList;
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView getNews() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("newsLayout");
        return mav;
    }

    @RequestMapping(value = "/news-categories")
    @ResponseBody
    public List<String> fetchNewsCategories(@RequestParam String langCode) {
        try {
            return newsArticleWebserviceHelper.fetchNewsCategories(langCode);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }

    @RequestMapping(value = "/questionanswer", method = RequestMethod.GET)
    public ModelAndView getQuestionAnswer() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("QuestionAnswerView");
        return mav;
    }

    @RequestMapping(value = "/fetch-answers", method = RequestMethod.POST)
    public JSONObject fetchAnswers(@RequestParam(value = "question") String question) {
        return newsArticleService.fetchAnswers(question);
    }
}
