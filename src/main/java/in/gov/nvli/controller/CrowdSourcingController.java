package in.gov.nvli.controller;

import com.google.gson.Gson;
import in.gov.nvli.beans.crowdsource.CreateArticleFormBean;
import in.gov.nvli.beans.crowdsource.CrowdSourceBean;
import in.gov.nvli.beans.crowdsource.CrowdSourceEditBean;
import in.gov.nvli.beans.crowdsource.CrowdSourceTagBean;
import in.gov.nvli.beans.crowdsource.CrowdSourceUDCTagBean;
import in.gov.nvli.beans.crowdsource.MetadataBean;
import in.gov.nvli.beans.crowdsource.UDCTranslationBean;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticleAttachedResource;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.domain.crowdsource.CrowdSourceUDCTag;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.crowdsource.UDCTagVote;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.timecodemarker.Content;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.Notifications;
import in.gov.nvli.mongodb.service.NotificationService;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.service.RewardsService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Helper;
import in.gov.nvli.util.LanguageRestrictionHelper;
import in.gov.nvli.util.MetadataTypeHelper;
import in.gov.nvli.util.SRTFileParser;
import in.gov.nvli.util.StringComparator;
import in.gov.nvli.webserviceclient.crowdsource.Article;
import in.gov.nvli.webserviceclient.crowdsource.CrowdsourceWebserviceHelper;
import in.gov.nvli.webserviceclient.crowdsource.MetadataStandardResponse;
import in.gov.nvli.webserviceclient.crowdsource.TagStandardResponse;
import in.gov.nvli.webserviceclient.crowdsource.Z3950Search;
import in.gov.nvli.webserviceclient.curationactivity.CurationActivityWebserviceHelper;
import in.gov.nvli.webserviceclient.mit.Record;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for handling all crowdsource related requests.
 *
 * @author Ritesh Malviya
 * @author Vivek Bugale
 *
 */
@Controller
@RequestMapping(value = "/cs")
public class CrowdSourcingController {

    private final static Logger LOG = Logger.getLogger(CrowdSourcingController.class);

    @Autowired
    private CrowdSourcingService crowdSourcingService;

    @Autowired
    private UserService userService;

    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    @Autowired
    private CrowdsourceWebserviceHelper crowdsourceWebserviceHelper;

    @Autowired
    private CurationActivityWebserviceHelper curationActivityWebserviceHelper;

    @Value("${srt.filepath}")
    private String srtFilePath;

    @Autowired
    public IResourceTypeDAO resourceTypeDAO;

    @Autowired
    private MessageSource messages;

    @Autowired
    private NotificationService notificationService;

    @Value("${crowdsource.edit.threshold}")
    private int crowdsourceEditThreshold;

    /**
     * To hold the resource type search icon read location
     */
    @Value("${nvli.read.resourcType.search.image.location}")
    private String resourceTypeIconSearchReadLocation;
    
    @Value("${cdn1.base.url}")
    private String cdn;
    @Autowired
    private RewardsService rewardsService;

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    /**
     * Redirect According To Resource
     *
     * @param recordIdentifier
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{recordIdentifier:.*}")
    public ModelAndView redirectAccordingToResource(@PathVariable("recordIdentifier") String recordIdentifier, HttpServletRequest request) {
        ModelAndView modelAndView;
        String identifierTokens[] = recordIdentifier.split("_");
        switch (identifierTokens[0]) {
            case "ARTP":
                modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/editresource/showartist/" + recordIdentifier));
                break;
            case "INRE":
                modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/editresource/showcuisine/" + recordIdentifier));
                break;
            default:
                modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/edit-record-metadata/" + recordIdentifier));
                break;
        }
        return modelAndView;
    }

    /**
     * This is to get all the crowdsourced/curated records by all users.
     *
     * @param pageNumber
     * @param pageWindow
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @RequestMapping(value = "metadata-curated-record-register/{pageNumber}/{pageWindow}")
    public ModelAndView metadataCuratedRecordRegister(@PathVariable("pageNumber") int pageNumber, @PathVariable("pageWindow") int pageWindow) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("MetadataCuratedRecordRegisterView");
        User user = userService.getCurrentUser();
        modelAndView.addObject("user", user);
        try {
            modelAndView.addObject("crowdSourceRecords", crowdSourcingService.getMetadataCuratedRecordListByUserId(pageNumber, pageWindow, user.getId()));
            modelAndView.addObject("totalPages", crowdSourcingService.getMetadataCuratedRecordTotalCountByUserId(pageWindow, user.getId()));
            modelAndView.addObject("pageNumber", pageNumber);
            modelAndView.addObject("pageWindow", pageWindow);

        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
        }
        return modelAndView;
    }

    /**
     * This is to get crowdsource form for a record.
     *
     * @param recordIdentifier
     * @param request
     * @param redirectAttributes
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "edit-record-metadata/{recordIdentifier:.*}")
    public ModelAndView editRecordMetadata(@PathVariable("recordIdentifier") String recordIdentifier, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        try {
            Gson gson = new Gson();
            String marc21DescriptionMap = gson.toJson(request.getServletContext().getAttribute("marc21TitleMap"));
            request.setAttribute("marc21DescriptionMap", marc21DescriptionMap);
            request.setAttribute("libraryDetailsMap", gson.toJson(Helper.z3950LibraryDetails));
            ModelAndView modelAndView;
            MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);
            CrowdSource crowdSource = crowdSourcingService.getCrowdSourceByRecordIdentifier(recordIdentifier);
            //if role is curator and record was assigned to this user then check curation status
            User user = userService.getCurrentUser();
            String curationStatus = Constants.UNCURATED;
            boolean isCurator = false;
            for (Role role : user.getRoles()) {
                if (role.getCode().equalsIgnoreCase(Constants.UserRole.CURATOR_USER)) {
                    isCurator = true;
                    Map<String, String> recordStatusMap = curationActivityWebserviceHelper.getRecordStatusFromServer(recordIdentifier);
                    if (recordStatusMap.get(Constants.ASSIGNED_ID).equalsIgnoreCase(user.getId().toString())) {
                        curationStatus = recordStatusMap.get(Constants.CURATION_STATUS).toLowerCase();
                    }
                    break;
                }
            }
            if ("yes".equalsIgnoreCase(metadataStandardResponse.getIsCloseForEdit()) && !isCurator) {
                modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/search/preview/" + recordIdentifier));
                redirectAttributes.addFlashAttribute("crowdsourceFailReason", "Sorry this record is closed for crowdsourcing");
                return modelAndView;
            } else {
                modelAndView = new ModelAndView("EditRecordMetadataView");
                modelAndView.addObject("user", userService.getCurrentUser());
                CrowdSourceUDCTag crowdSourceUDCTag;
                CrowdSourceBean crowdSourceBean = new CrowdSourceBean();
                crowdSourceBean.setRecordIdentifier(recordIdentifier);
                String allowedLanguages;
                List<CrowdSourceEditBean> crowdSourceEditBeanList = new ArrayList<>();
                if (crowdSource == null) {
                    //crowdSourceBean.setCrowdSourceId(recordIdentifier);
                    String metadataJSONString = MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata());
                    crowdSourceBean.setOriginalMetadata(MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataStandardResponse.getMetadataStandard(), metadataJSONString));
                    crowdSourceBean.setCrowdSourceEditBeanList(crowdSourceEditBeanList);
                    crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(recordIdentifier, Byte.parseByte("0"));
                    if (crowdSourceUDCTag != null) {
                        crowdSourceBean.setCrowdSourceUDCOriginalTags(crowdSourceUDCTag.getOriginalTags());
                        crowdSourceBean.setCrowdSourceUDCTagsAdded(crowdSourceUDCTag.getTagsAdded());
                    } else {
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.UDC);
                        if (tagStandardResponse.getTagValue() != null) {
                            crowdSourceBean.setCrowdSourceUDCOriginalTags(tagStandardResponse.getTagValue().toString());
                        }
                    }

                    CrowdSourceCustomTag crowdSourceCustomTag = crowdSourcingService.getCrowdSourceCustomTag(recordIdentifier, userService.getCurrentUser());
                    if (crowdSourceCustomTag != null) {
                        crowdSourceBean.setCrowdSourceCustomTags(crowdSourceCustomTag.getTags());
                    } else {
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.CUSTOM);
                        if (tagStandardResponse.getTagValue() != null) {
                            crowdSourceBean.setCrowdSourceCustomTags(tagStandardResponse.getTagValue().toString());
                        }
                    }
                    modelAndView.addObject("isEditFirstTime", true);
                    allowedLanguages = metadataStandardResponse.getAllowedLanguages();
                } else {
                    CrowdSourceEditBean crowdSourceEditBean;
                    TreeSet<CrowdSourceEdit> tempSet = new TreeSet();
                    tempSet.addAll(crowdSource.getCrowdSourceEditSet());
                    for (CrowdSourceEdit crowdSourceEdit : tempSet) {
                        crowdSourceEditBean = new CrowdSourceEditBean();
                        crowdSourceEditBean.setSubEditNumber(crowdSourceEdit.getSubEditNumber());
                        crowdSourceEditBean.setCrowdSourceEditId(crowdSourceEdit.getId().toString());
                        crowdSourceEditBean.setEditedMetadata(MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(crowdSource.getMetadataType(), crowdSourceEdit.getEditedMetadataJson()));
                        crowdSourceEditBean.setIsCuratedVersion(crowdSourceEdit.getIsCuratedVersion());
                        crowdSourceEditBeanList.add(crowdSourceEditBean);
                    }
                    crowdSourceBean.setCrowdSourceId(crowdSource.getId().toString());
                    crowdSourceBean.setOriginalMetadata(MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(crowdSource.getMetadataType(), crowdSource.getOriginalMetadataJson()));
                    crowdSourceBean.setCrowdSourceEditBeanList(crowdSourceEditBeanList);
                    crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(recordIdentifier, Byte.parseByte("0"));
                    if (crowdSourceUDCTag != null) {
                        crowdSourceBean.setCrowdSourceUDCOriginalTags(crowdSourceUDCTag.getOriginalTags());
                        crowdSourceBean.setCrowdSourceUDCTagsAdded(crowdSourceUDCTag.getTagsAdded());
                    } else {
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.UDC);
                        if (tagStandardResponse.getTagValue() != null) {
                            crowdSourceBean.setCrowdSourceUDCOriginalTags(tagStandardResponse.getTagValue().toString());
                        }
                    }
                    CrowdSourceCustomTag crowdSourceCustomTag = crowdSourcingService.getCrowdSourceCustomTag(recordIdentifier, userService.getCurrentUser());
                    if (crowdSourceCustomTag != null) {
                        crowdSourceBean.setCrowdSourceCustomTags(crowdSourceCustomTag.getTags());
                    } else {
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.CUSTOM);
                        if (tagStandardResponse.getTagValue() != null) {
                            crowdSourceBean.setCrowdSourceCustomTags(tagStandardResponse.getTagValue().toString());
                        }
                    }
                    modelAndView.addObject("isEditFirstTime", false);
                    allowedLanguages = crowdSource.getAllowedLanguages();
                    if (crowdSource.getLastSubeditNumber() == crowdsourceEditThreshold) {
                        crowdSourceBean.setIsRecordEditable(false);
                    }
                }
                TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.UDC);
                if (tagStandardResponse != null) {
                    List<String> recordPathList = tagStandardResponse.getPath();
                    if (recordPathList != null && !recordPathList.isEmpty()) {
                        String contentType = crowdSourcingService.getContentType(recordPathList.get(0));
                        modelAndView.addObject("contentType", contentType);
                        if (contentType.equalsIgnoreCase(Constants.ContentType.JPEG)) {
                            modelAndView.addObject("recordPathList", recordPathList);
                        }
                    }
                }

                //If record is curated it should close for editing of metadata
                if (curationStatus.equalsIgnoreCase(Constants.CURATED)) {
                    crowdSourceBean.setIsRecordEditable(false);
                }
                modelAndView.addObject("userType", "normal");
                for (Role role : user.getRoles()) {
                    if (role.getCode().equalsIgnoreCase(Constants.UserRole.LIB_USER)) {
                        modelAndView.addObject("userType", "expert");
                        modelAndView.addObject("crowdSourceUDCTagBean", new CrowdSourceUDCTagBean());
                        break;
                    }
                }
                System.out.println("id-------------" + crowdSourceBean.getCrowdSourceId());
                modelAndView.addObject("curationStatus", curationStatus);
                modelAndView.addObject("crowdSourceBean", crowdSourceBean);
                modelAndView.addObject("crowdSourceTagBean", new CrowdSourceTagBean());
                modelAndView.addObject("languageList", crowdSourcingService.getIndianLanguageList());
                modelAndView.addObject("languageArray", LanguageRestrictionHelper.prepareAllowedLanguages(allowedLanguages));
                return modelAndView;
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            String referrer = request.getHeader("Referer");
            if (referrer == null || referrer.isEmpty()) {
                referrer = request.getContextPath() + "/auth/login/success";
            }
            redirectAttributes.addFlashAttribute("isEditMetadataClosed", "true");
            ModelAndView modelAndView = new ModelAndView(new RedirectView(referrer));
            return modelAndView;
        }
    }

    /**
     * This is to get the meta data of the record.
     *
     * @param request
     * @param response
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getMetadata")
    @ResponseBody
    public ModelAndView getMetadata(HttpServletRequest request, HttpServletResponse response) {
        try {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("metadataEditableFormView");
            modelAndView.addObject("user", userService.getCurrentUser());
            boolean isOriginal = Boolean.valueOf(request.getParameter("isOriginal"));
            String metadataStandardType = request.getParameter("metadataStandardType");
            String crowdSourceId = request.getParameter("crowdSourceId");
            String userType = request.getParameter("userType");
            String recordIdentifier = request.getParameter("recordIdentifier");
            boolean isEditFirstTime = Boolean.valueOf(request.getParameter("isEditFirstTime"));
            MetadataBean metadataBean;
            String allowedLanguages;
            if (isEditFirstTime) {
                MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);
                String metadataJSONString = MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata());
                metadataBean = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataStandardResponse.getMetadataStandard(), metadataJSONString);
                allowedLanguages = metadataStandardResponse.getAllowedLanguages();
            } else if (isOriginal) {
                CrowdSource crowdSource = crowdSourcingService.getCrowdSource(Long.valueOf(crowdSourceId));
                metadataBean = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataStandardType, crowdSource.getOriginalMetadataJson());
                allowedLanguages = crowdSource.getAllowedLanguages();
            } else {
                CrowdSourceEdit crowdSourceEdit = crowdSourcingService.getCrowdSourceEdit(Long.valueOf(crowdSourceId));
                metadataBean = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataStandardType, crowdSourceEdit.getEditedMetadataJson());
                crowdSourceId = crowdSourceEdit.getCrowdSource().getId().toString();
                allowedLanguages = crowdSourceEdit.getCrowdSource().getAllowedLanguages();
            }
            metadataBean.setCrowdSourceId(crowdSourceId);
            metadataBean.setIsEditFirstTime(isEditFirstTime);
            metadataBean.setUserType(userType);
            metadataBean.setRecordIdentifier(recordIdentifier);
            modelAndView.addObject("metadataBeanForm", metadataBean);
            if (allowedLanguages == null || allowedLanguages.isEmpty()) {
                allowedLanguages = "en";
            }
            modelAndView.addObject("languageList", crowdSourcingService.getLanguageListByLangCodeList(allowedLanguages.split("&|&")));
            response.addHeader("status", "success");
            return modelAndView;
        } catch (Exception ex) {
            response.addHeader("status", "fail");
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to save meta data of crowdsourced record and also approved record
     * by expert.
     *
     * @param metadataBean
     * @param modifiedMetadataJson
     * @param request
     * @param redirectAttrs
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "saveMetaData", method = RequestMethod.POST)
    public ModelAndView saveMetaData(@ModelAttribute("metadataBeanForm") MetadataBean metadataBean, @RequestParam String modifiedMetadataJson, HttpServletRequest request, RedirectAttributes redirectAttrs) {
        System.out.println("modifiedMetadataJson" + modifiedMetadataJson);
        ModelAndView modelAndView;
        if (metadataBean.isApproved()) {
            if (!metadataBean.isIsEdit()) {
                modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/metadata-curated-record-register/1/15"));
            } else {
                modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/edit-record-metadata/" + metadataBean.getRecordIdentifier()));
            }
        } else {
            modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/edit-record-metadata/" + metadataBean.getRecordIdentifier()));
        }
        try {
            Map<String, Object> map = crowdSourcingService.validateDuplicateMetadata(metadataBean);
            boolean isNotDuplicate = (boolean) map.get("status");
            if (!isNotDuplicate) {
                redirectAttrs.addFlashAttribute("version", map.get("version"));
            } else if (crowdSourcingService.saveMetaData(metadataBean, userService.getCurrentUser(), modifiedMetadataJson, request)) {
                redirectAttrs.addFlashAttribute("status", "true");
            } else {
                redirectAttrs.addFlashAttribute("status", "false");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            redirectAttrs.addFlashAttribute("status", "false");
        }
        return modelAndView;
    }

    /**
     * This is to save the UDC/Custom tags to the reccord.
     *
     * @param crowdSourceTagBean
     * @param modifiedJson
     * @param request
     * @param redirectAttrs
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "saveTags", method = RequestMethod.POST)
    public ModelAndView saveTags(@ModelAttribute("crowdSourceTagBean") CrowdSourceTagBean crowdSourceTagBean, @RequestParam String modifiedJson, HttpServletRequest request, RedirectAttributes redirectAttrs) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/edit-record-metadata/" + crowdSourceTagBean.getRecordIdentifier()));
        try {
            Long crowdSourceId = null;
            if (crowdSourceTagBean.getRecordIdentifier() != null && !crowdSourceTagBean.getRecordIdentifier().isEmpty()
                    && crowdSourceTagBean.getCrowdSourceTagType() != null && !crowdSourceTagBean.getCrowdSourceTagType().isEmpty()
                    && crowdSourceTagBean.getCrowdSourceTags() != null && !crowdSourceTagBean.getCrowdSourceTags().isEmpty()) {
                if (crowdSourceTagBean.getCrowdSourceTagType().equalsIgnoreCase("udc")) {
                    CrowdSourceUDCTag crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(crowdSourceTagBean.getRecordIdentifier(), Byte.parseByte("0"));
                    if (crowdSourceUDCTag == null) {
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(crowdSourceTagBean.getRecordIdentifier(), Constants.TagType.UDC);
                        Map<String, String> recordTitleMap = crowdsourceWebserviceHelper.getRecordsTitle(Arrays.asList(crowdSourceTagBean.getRecordIdentifier()));
                        crowdSourceUDCTag = new CrowdSourceUDCTag();
                        if (tagStandardResponse.getTagValue() != null) {
                            crowdSourceUDCTag.setOriginalTags(tagStandardResponse.getTagValue().toString());
                        }
                        crowdSourceUDCTag.setRecordIdentifier(crowdSourceTagBean.getRecordIdentifier());
                        if (recordTitleMap != null && !recordTitleMap.isEmpty()) {
                            crowdSourceUDCTag.setRecordTitle(recordTitleMap.get(crowdSourceTagBean.getRecordIdentifier()));
                        }
                    }
                    crowdSourceUDCTag.setTagsAdded(crowdSourceTagBean.getCrowdSourceTags());
                    crowdSourcingService.saveOrUpdateCrowdSourceUDCTag(crowdSourceUDCTag);
                    crowdSourceId = crowdSourceUDCTag.getId();
                } else if (crowdSourceTagBean.getCrowdSourceTagType().equalsIgnoreCase("custom")) {
                    CrowdSourceCustomTag crowdSourceCustomTag = crowdSourcingService.getCrowdSourceCustomTag(crowdSourceTagBean.getRecordIdentifier(), userService.getCurrentUser());
                    if (crowdSourceCustomTag == null) {
                        crowdSourceCustomTag = new CrowdSourceCustomTag();
                        crowdSourceCustomTag.setRecordIdentifier(crowdSourceTagBean.getRecordIdentifier());
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(crowdSourceTagBean.getRecordIdentifier(), Constants.TagType.CUSTOM);
                        if (tagStandardResponse.getTagValue() != null && !tagStandardResponse.getTagValue().toString().isEmpty()) {
                            crowdSourceCustomTag.setTags(tagStandardResponse.getTagValue().toString());
                        }
                    }
                    crowdSourceCustomTag.setUser(userService.getCurrentUser());
                    crowdSourcingService.saveOrUpdateCrowdSourceCustomTag(crowdSourceCustomTag, crowdSourceTagBean.getCrowdSourceTags());
                    crowdSourceId = crowdSourceCustomTag.getId();
                }
                rewardsService.rewardsPointsForTags(modifiedJson, crowdSourceId, crowdSourceTagBean.getRecordIdentifier(), request, crowdSourceTagBean.getCrowdSourceTagType(), userService.getCurrentUser());
                redirectAttrs.addFlashAttribute("status", "true");
            } else {
                redirectAttrs.addFlashAttribute("status", "false");
            }
            return modelAndView;
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            redirectAttrs.addFlashAttribute("status", "false");
            return modelAndView;
        }
    }

    /**
     * This is to get the suggestions of the UDC description.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUDCTagsSuggestions", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getUDCTagsSuggestions(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.getUDCTagsSuggestions(request.getParameter("languageId"), request.getParameter("tagPrefix"));
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to get the UDC description of a UDC tag based on UDC notation and
     * the language.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUDCTagDescription")
    @ResponseBody
    public Map<String, String> getUDCTagDescription(HttpServletRequest request
    ) {
        try {
            String udcNotations = request.getParameter("udcNotations").trim();
            long languageId = Long.parseLong(request.getParameter("languageId").trim());
            Map<String, String> map = crowdSourcingService.getUDCTagDescription(Arrays.asList(udcNotations.split("&\\|&")), languageId, true);
            return map;
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to get the view of crowdsourced record.
     *
     * @param crowdSourceId
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @RequestMapping(value = "approve-record-metadata/{crowdSourceId}")
    public ModelAndView approveRecordMetadata(@PathVariable("crowdSourceId") String crowdSourceId
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            modelAndView.setViewName("ApproveRecordMetadataView");
            CrowdSource crowdSource = crowdSourcingService.getCrowdSource(Long.parseLong(crowdSourceId));
            CrowdSourceBean crowdSourceBean = new CrowdSourceBean();
            List<CrowdSourceEditBean> crowdSourceEditBeanList = new ArrayList<>();
            TreeSet<CrowdSourceEdit> tempSet = new TreeSet();
            tempSet.addAll(crowdSource.getCrowdSourceEditSet());
            for (CrowdSourceEdit crowdSourceEdit : tempSet) {
                CrowdSourceEditBean crowdSourceEditBean = new CrowdSourceEditBean();
                crowdSourceEditBean.setSubEditNumber(crowdSourceEdit.getSubEditNumber());
                crowdSourceEditBean.setIsCuratedVersion(crowdSourceEdit.getIsCuratedVersion());
                crowdSourceEditBean.setCrowdSourceEditId(crowdSourceEdit.getId().toString());
                crowdSourceEditBean.setEditedMetadata(MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(crowdSource.getMetadataType(), crowdSourceEdit.getEditedMetadataJson()));
                crowdSourceEditBeanList.add(crowdSourceEditBean);
            }
            crowdSourceBean.setRecordIdentifier(crowdSource.getRecordIdentifier());
            crowdSourceBean.setCrowdSourceId(crowdSourceId);
            crowdSourceBean.setOriginalMetadata(MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(crowdSource.getMetadataType(), crowdSource.getOriginalMetadataJson()));
            crowdSourceBean.setCrowdSourceEditBeanList(crowdSourceEditBeanList);
            modelAndView.addObject("crowdSourceBean", crowdSourceBean);
            modelAndView.addObject("recordIdentifier", crowdSource.getRecordIdentifier());
            TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(crowdSource.getRecordIdentifier(), Constants.TagType.UDC);
            if (tagStandardResponse != null) {
                List<String> recordPathList = tagStandardResponse.getPath();
                if (recordPathList != null && !recordPathList.isEmpty()) {
                    String contentType = crowdSourcingService.getContentType(recordPathList.get(0));
                    modelAndView.addObject("contentType", contentType);
                    if (contentType.equalsIgnoreCase(Constants.ContentType.JPEG)) {
                        modelAndView.addObject("recordPathList", recordPathList);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
        }
        return modelAndView;
    }

    /**
     * This is to get register contains all UDC tagged records by different
     * users.
     *
     * @param pageNumber
     * @param pageWindow
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @RequestMapping(value = "udc-tagged-record-register/{pageNumber}/{pageWindow}")
    public ModelAndView udcTaggedRecordRegister(@PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageWindow") int pageWindow
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UDCTaggedRecordRegisterView");
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            modelAndView.addObject("crowdSourceUDCTags", crowdSourcingService.getCrowdSourceUDCTagListByUserId(pageNumber, pageWindow));
            modelAndView.addObject("totalPages", crowdSourcingService.getCrowdSourceTotalUDCTagCountByUserId(pageWindow));
            modelAndView.addObject("pageNumber", pageNumber);
            modelAndView.addObject("pageWindow", pageWindow);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
        }
        return modelAndView;
    }

    /**
     * This is to load translated tags register .
     *
     * @return @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "udc-translated-tag")
    public ModelAndView showUDCTranslatedTags() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UDCTransalatedTagsRegisterView");
        return modelAndView;
    }

    /**
     * This is to get register contains all translated UDC tags by different
     * user.
     *
     * @param searchString
     * @param dataLimit
     * @param pageNo
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @ResponseBody
    @RequestMapping(value = "udc-translated-tag-register", method = RequestMethod.POST)
    public JSONObject udcTranslatedTagsRegister(@RequestParam String searchString,
            @RequestParam int dataLimit,
            @RequestParam int pageNo
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tagsJsonObject", crowdSourcingService.fetchTranslatedTagsWithLimit(userService.getCurrentUser().getId(), searchString, dataLimit, pageNo));
            jsonObject.put("totalCount", crowdSourcingService.fetchTranslatedTagsTotalCount(userService.getCurrentUser().getId(), searchString));
            return jsonObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
            return null;
        }
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @ResponseBody
    @RequestMapping(value = "see-udc-translated-tag", method = RequestMethod.POST)
    public JSONObject seeUDCTranslatedTags(@RequestParam long conceptId,
            @RequestParam long languageId
    ) {
        return crowdSourcingService.seeUDCTranslatedTags(conceptId, languageId);
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @ResponseBody
    @RequestMapping(value = "update-udc-translation", method = RequestMethod.POST)
    public boolean updateUDCTranslation(@RequestBody JSONObject data
    ) {
        try {
            JSONParser parser = new JSONParser();
            JSONArray selectedTranslationArr, rejectedTransalationsArr;
            selectedTranslationArr = (JSONArray) parser.parse(data.get("selectedTranslationArr").toString());
            rejectedTransalationsArr = (JSONArray) parser.parse(data.get("rejectedTransalationsArr").toString());
            crowdSourcingService.updateUDCTranslation(selectedTranslationArr, rejectedTransalationsArr);
            return true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * This is to get the approve view UDC/Custom tagged record to the expert.
     *
     * @param crowdSourceUDCTagId
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @RequestMapping(value = "approve-record-tags/{crowdSourceUDCTagId}")
    public ModelAndView approveRecordTags(@PathVariable("crowdSourceUDCTagId") String crowdSourceUDCTagId
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            modelAndView.setViewName("ApproveRecordTagsView");
            CrowdSourceUDCTag crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(Long.parseLong(crowdSourceUDCTagId));
            MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(crowdSourceUDCTag.getRecordIdentifier());
            modelAndView.addObject("metadata", MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataStandardResponse.getMetadataStandard(), MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata())));
            modelAndView.addObject("existingUDCOriginalTags", crowdSourceUDCTag.getOriginalTags());
            modelAndView.addObject("existingUDCTagsAdded", crowdSourceUDCTag.getTagsAdded());
            modelAndView.addObject("crowdSourceUDCTagId", crowdSourceUDCTag.getId());
            modelAndView.addObject("recordIdentifier", crowdSourceUDCTag.getRecordIdentifier());
            modelAndView.addObject("crowdSourceUDCTagBean", new CrowdSourceUDCTagBean());
            modelAndView.addObject("languageList", crowdSourcingService.getIndianLanguageList());
            TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(crowdSourceUDCTag.getRecordIdentifier(), Constants.TagType.UDC);
            if (tagStandardResponse != null) {
                List<String> recordPathList = tagStandardResponse.getPath();
                if (recordPathList != null && !recordPathList.isEmpty()) {
                    String contentType = crowdSourcingService.getContentType(recordPathList.get(0));
                    modelAndView.addObject("contentType", contentType);
                    if (contentType.equalsIgnoreCase(Constants.ContentType.JPEG)) {
                        modelAndView.addObject("recordPathList", recordPathList);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
        }
        return modelAndView;
    }

    /**
     * This is to approve the UDC / Custom tagged records.
     *
     * @param crowdSourceUDCTagBean
     * @param request
     * @param redirectAttributes
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.LIB_USER + "')")
    @RequestMapping(value = "approveUDCTags")
    public ModelAndView approveUDCTags(@ModelAttribute("crowdSourceUDCTagBean") CrowdSourceUDCTagBean crowdSourceUDCTagBean, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        ModelAndView modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/udc-tagged-record-register/1/15"));
        try {
            if (crowdSourceUDCTagBean.getIsEdit() == null || crowdSourceUDCTagBean.getIsEdit().isEmpty()) {
                redirectAttributes.addFlashAttribute("status", "false");
            } else {
                CrowdSourceUDCTag crowdSourceUDCTag = null;
                if (crowdSourceUDCTagBean.getIsEdit().equalsIgnoreCase("no")) {
                    crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(Long.parseLong(crowdSourceUDCTagBean.getCrowdSourceUDCTagId()));
                } else {
                    modelAndView = new ModelAndView(new RedirectView(request.getContextPath() + "/cs/edit-record-metadata/" + crowdSourceUDCTagBean.getRecordIdentifier()));
                    crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(crowdSourceUDCTagBean.getRecordIdentifier().trim(), Byte.parseByte("0"));
                    if (crowdSourceUDCTag == null) {
                        crowdSourceUDCTag = crowdSourcingService.getCrowdSourceUDCTag(crowdSourceUDCTagBean.getRecordIdentifier().trim(), Byte.parseByte("1"));
                    }
                    if (crowdSourceUDCTag == null) {
                        TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(crowdSourceUDCTagBean.getRecordIdentifier(), Constants.TagType.UDC);
                        Map<String, String> recordTitleMap = crowdsourceWebserviceHelper.getRecordsTitle(Arrays.asList(crowdSourceUDCTagBean.getRecordIdentifier()));
                        crowdSourceUDCTag = new CrowdSourceUDCTag();
                        if (tagStandardResponse.getTagValue() != null) {
                            crowdSourceUDCTag.setOriginalTags(tagStandardResponse.getTagValue().toString());
                        }
                        crowdSourceUDCTag.setRecordIdentifier(crowdSourceUDCTagBean.getRecordIdentifier());
                        if (recordTitleMap != null && !recordTitleMap.isEmpty()) {
                            crowdSourceUDCTag.setRecordTitle(recordTitleMap.get(crowdSourceUDCTagBean.getRecordIdentifier()));
                        }
                        crowdSourcingService.saveOrUpdateCrowdSourceUDCTag(crowdSourceUDCTag);
                    }
                }
                crowdSourceUDCTag.setApprovedTags(crowdSourceUDCTagBean.getCrowdSourceTags());
                crowdSourceUDCTag.setIsApproved(Byte.parseByte("1"));
                if (crowdSourcingService.saveCrowdSourceUDCTag(crowdSourceUDCTag)) {
                    //rewards points for approved udc tags
                    rewardsService.rewardsPointsForApprovedUDCTags(crowdSourceUDCTag.getOriginalTags(), crowdSourceUDCTag.getApprovedTags(), crowdSourceUDCTagBean.getCrowdSourceUDCTagId(), request);
                    TagStandardResponse tagStandardResponse = new TagStandardResponse();
                    tagStandardResponse.setRecordIdentifier(crowdSourceUDCTag.getRecordIdentifier());
                    tagStandardResponse.setTagType(Constants.TagType.UDC);
                    tagStandardResponse.setTagValue(crowdSourceUDCTagBean.getCrowdSourceTags());
                    try {
                        if (crowdsourceWebserviceHelper.saveTagDetails(tagStandardResponse)) {
                            crowdSourceUDCTag.setIsUpdatedAtSource(Byte.valueOf("1"));
                        } else {
                            crowdSourceUDCTag.setIsUpdatedAtSource(Byte.valueOf("0"));
                        }
                    } catch (Exception ex) {
                        crowdSourceUDCTag.setIsUpdatedAtSource(Byte.valueOf("0"));
                        ex.printStackTrace();
                    }
                    crowdSourcingService.saveCrowdSourceUDCTag(crowdSourceUDCTag);

                    redirectAttributes.addFlashAttribute("status", "true");

                    //save approved 
                } else {
                    redirectAttributes.addFlashAttribute("status", "false");
                }
                //APRROVED TAGS

            }
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute("status", "false");
            //LOG.error(ex.getMessage());
            ex.printStackTrace();
        }
        return modelAndView;
    }

    /**
     * This is to get the suggestion for the custom tags based on the tag prefix
     * and language.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getCustomTagsSuggestions", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getCustomTagsSuggestions(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.getCustomTagsSuggestions(request.getParameter("languageId"), request.getParameter("tagPrefix"));
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to get the tree view of the UDC tags.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUDCTreeviewData")
    @ResponseBody
    public Map<String, Object> getUDCTreeviewData(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.generateUDCTreeView(Long.parseLong(request.getParameter("languageId")), request.getParameter("tags"), Boolean.parseBoolean(request.getParameter("isDisable")));
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to MARC21 tag suggestion.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getMarc21TagsSuggestions", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getMarc21TagsSuggestions(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.getMarc21TagsSuggestions(request.getParameter("tagField"));
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to get MARC21 description suggestion.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getMarc21DescriptionSuggestions")
    @ResponseBody
    public String getMarc21DescriptionSuggestions(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.getMarc21DescriptionSuggestions(request.getParameter("librarianTitle"));
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to to load audio / video marking page.
     *
     * @param recordIdentifier
     * @param request
     * @param response
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/audioVideoMarking/{recordIdentifier:.*}")
    public ModelAndView audioVideoMarking(@PathVariable("recordIdentifier") String recordIdentifier, HttpServletRequest request, HttpServletResponse response
    ) {
        try {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("AudioVideoMarkingView");
            modelAndView.addObject("user", userService.getCurrentUser());
            TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.MARKER);
            List<String> recordPathList = tagStandardResponse.getPath();
            if (recordPathList != null && !recordPathList.isEmpty()) {
                Collections.sort(recordPathList, new StringComparator());
                String contentType = crowdSourcingService.getContentType(recordPathList.get(0));
                if (contentType.equalsIgnoreCase(Constants.ContentType.AUDIO) || contentType.equalsIgnoreCase(Constants.ContentType.VIDEO)) {
                    Map<String, Map<Long, String>> conentInMap = crowdSourcingService.convertContentToMap(MetadataTypeHelper.getContentFromObject(tagStandardResponse.getTagValue()));
                    org.json.JSONObject jSONObject = new org.json.JSONObject(conentInMap);
                    modelAndView.addObject("audioVideoMarkingMap", jSONObject.toString());
                }
                modelAndView.addObject("sourceObjectType", contentType);
                modelAndView.addObject("previewFileList", recordPathList);
                modelAndView.addObject("recordIdentifier", recordIdentifier);
            }
            return modelAndView;
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to save the audio / video marking tags.
     *
     * @param request
     * @param response
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/saveAudioVideoMarking")
    @ResponseBody
    public String saveAudioVideoMarking(HttpServletRequest request, HttpServletResponse response
    ) {
        synchronized (this) {
            try {
                String recordIdentifier = request.getParameter("recordIdentifier").trim();
                org.json.JSONObject jSONObject = new org.json.JSONObject(request.getParameter("audioVideoMarkingMap"));
                TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.MARKER);
                Content content = crowdSourcingService.jsonObjectToContent(jSONObject, recordIdentifier, MetadataTypeHelper.getContentFromObject(tagStandardResponse.getTagValue()));
                tagStandardResponse.setTagValue(content);
                if (crowdsourceWebserviceHelper.saveTagDetails(tagStandardResponse)) {
                    return "success";
                } else {
                    return "failed";
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                LOG.error(ex.getMessage());
                return "failed";
            }
        }
    }

    /**
     * This is to load the content of the record like audio/video/pdf.
     *
     * @param request
     * @param response
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/getRecordContent")
    public @ResponseBody
    ModelAndView getRecordContent(HttpServletRequest request, HttpServletResponse response
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        RestTemplate restTemplate = new RestTemplate();
        try {
            String recordIdentifier = request.getParameter("recordIdentifier");
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();
            modelAndView.addObject("record", record);
            modelAndView.addObject("totalMedia", 0);
            modelAndView.setViewName("PreviewAllTIFView");
            TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.UDC);
            if (tagStandardResponse != null) {
                List<String> recordPathList = tagStandardResponse.getPath();
                if (recordPathList != null && !recordPathList.isEmpty()) {
                    String contentType = crowdSourcingService.getContentType(recordPathList.get(0));
                    modelAndView.addObject("totalMedia", recordPathList.size());
                    if (contentType.equalsIgnoreCase(Constants.ContentType.AUDIO) || contentType.equalsIgnoreCase(Constants.ContentType.VIDEO) || contentType.equalsIgnoreCase(Constants.ContentType.DOCUMENT)) {
                        modelAndView.addObject("crowdSource", true);
                        modelAndView.setViewName("PreviewAllMP3MP4View");
                        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
                        String temp[] = recordIdentifier.split("_");
                        ResourceType resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
                        modelAndView.addObject("resourceType", resourceTypeObj);
                    } else {
                        modelAndView.addObject("contentType", contentType);
                        modelAndView.addObject("totalMedia", recordPathList.size());
                        modelAndView.setViewName("PreviewAllTIFView");
                        if (contentType.equalsIgnoreCase(Constants.ContentType.TIFF)) {
                        } else if (contentType.equalsIgnoreCase(Constants.ContentType.JPEG)) {
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error("Error occured.." + ex.getMessage());
        } finally {
            return modelAndView;
        }
    }

    /**
     * This is to open the create article form.
     *
     * @return @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "create-article")
    public ModelAndView createArticle() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        try {
            JSONArray relatedArticleJsonArray = new JSONArray();
            CrowdSourceArticle crowdSourceArticle = new CrowdSourceArticle();
            crowdSourceArticle.setId(Long.parseLong("-1"));
            crowdSourceArticle.setUser(userService.getCurrentUser());
            CreateArticleFormBean createArticleFormBean = new CreateArticleFormBean();
            createArticleFormBean.setCrowdSourceArticle(crowdSourceArticle);
            createArticleFormBean.setCrowdSourceArticleAttachedResources(new ArrayList<CrowdSourceArticleAttachedResource>());
            modelAndView.addObject("createArticleFormBean", createArticleFormBean);
            modelAndView.addObject("user", userService.getCurrentUser());
            modelAndView.addObject("languageList", crowdSourcingService.getIndianLanguageList());
            modelAndView.addObject("resourcesList", crowdSourcingService.getResourceByResourceType());
            modelAndView.addObject("relatedArticleJsonArray", relatedArticleJsonArray);
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("My Stuff", "#");
            breadcrumbMap.put("My Article", "cs/show-articles");
            modelAndView.addObject("breadcrumbMap", breadcrumbMap);
            modelAndView.addObject("title", "Create Article");
            modelAndView.setViewName("CreateArticleView");
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
        return modelAndView;
    }

    /**
     * This is to load edit article form.
     *
     * @param articleId
     * @return
     * @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "edit-article/{articleId}")
    public ModelAndView editArticle(@PathVariable("articleId") String articleId) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            CrowdSourceArticle crowdSourceArticle = crowdSourcingService.getArticle(Long.parseLong(articleId));
            User user = userService.getCurrentUser();
            if (!user.getId().equals(crowdSourceArticle.getUser().getId())) {
                throw new Exception();
            }
            CreateArticleFormBean createArticleFormBean = new CreateArticleFormBean();
            createArticleFormBean.setCrowdSourceArticle(crowdSourceArticle);
            List<CrowdSourceArticleAttachedResource> list = new ArrayList<>();
            if (crowdSourceArticle.getCrowdSourceArticleAttachedResources() != null) {
                list.addAll(crowdSourceArticle.getCrowdSourceArticleAttachedResources());
            }
            createArticleFormBean.setCrowdSourceArticleAttachedResources(list);

            JSONObject relatedArticleJsonObject;
            JSONArray relatedArticleJsonArray = new JSONArray();
            List<Long> articleIds = new ArrayList<>();
            if (!crowdSourceArticle.getRelatedArticles().isEmpty()) {
                for (String id : crowdSourceArticle.getRelatedArticles().split("&\\|&")) {
                    articleIds.add(Long.parseLong(id));
                }
                for (CrowdSourceArticle tempCrowdSourceArticle : crowdSourcingService.getArticleList(articleIds)) {
                    relatedArticleJsonObject = new JSONObject();
                    relatedArticleJsonObject.put("id", tempCrowdSourceArticle.getId());
                    relatedArticleJsonObject.put("author", tempCrowdSourceArticle.getUser().getFirstName() + " " + tempCrowdSourceArticle.getUser().getLastName());
                    relatedArticleJsonObject.put("title", tempCrowdSourceArticle.getArticleTitle());
                    relatedArticleJsonArray.add(relatedArticleJsonObject);
                }
            }
            modelAndView.addObject("createArticleFormBean", createArticleFormBean);
            modelAndView.addObject("languageList", crowdSourcingService.getIndianLanguageList());
            modelAndView.addObject("relatedArticleJsonArray", relatedArticleJsonArray);
            modelAndView.addObject("resourcesList", crowdSourcingService.getResourceByResourceType());
            modelAndView.setViewName("CreateArticleView");
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
        return modelAndView;
    }

    /**
     * This is to save article.
     *
     * @param createArticleFormBean
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "save-article")
    public ModelAndView saveArticle(@ModelAttribute("createArticleFormBean") CreateArticleFormBean createArticleFormBean, HttpServletRequest request
    ) {
        CrowdSourceArticle crowdSourceArticle = createArticleFormBean.getCrowdSourceArticle();
        try {
            if (crowdSourceArticle.getId().equals(Long.parseLong("-1"))) {
                crowdSourceArticle.setId(null);
            }
            crowdSourceArticle.setLastModifiedDate(new Timestamp(new Date().getTime()));
            crowdSourcingService.saveOrUpdateArticle(crowdSourceArticle);
            crowdSourcingService.saveOrUpdateOrDeleteAttachedResource(crowdSourceArticle, createArticleFormBean.getCrowdSourceArticleAttachedResources());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
        return new ModelAndView(new RedirectView(request.getContextPath() + "/cs/edit-article/" + crowdSourceArticle.getId()));
    }

    /**
     * This is to load a a article by id.
     *
     * @param articleId
     * @return
     * @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = {"preview-article/{articleId}", "manage/preview-article/{articleId}"})
    public ModelAndView previewArticle(@PathVariable("articleId") String articleId) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            CrowdSourceArticle crowdSourceArticle = crowdSourcingService.getArticle(Long.parseLong(articleId));
            User user = userService.getCurrentUser();
            Role role = new Role();
            role.setCode(Constants.UserRole.ADMIN_USER);
            if (user.getId().equals(crowdSourceArticle.getUser().getId()) || (user.getRoles().contains(role) && crowdSourceArticle.getIsPublished())) {
                Map<String, String> map = crowdSourcingService.getUDCTagDescription(Arrays.asList(crowdSourceArticle.getUdcTags().split("&\\|&")), 40l, false);
                List<CrowdSourceArticle> crowdSourceArticleList = new ArrayList<>();
                List<Long> articleIds = new ArrayList<>();
                if (!crowdSourceArticle.getRelatedArticles().isEmpty()) {
                    for (String id : crowdSourceArticle.getRelatedArticles().split("&\\|&")) {
                        articleIds.add(Long.parseLong(id));
                    }
                    crowdSourceArticleList = crowdSourcingService.getArticleList(articleIds);
                }
                modelAndView.addObject("crowdSourceArticle", crowdSourceArticle);
                modelAndView.addObject("crowdSourceArticleUDCTags", map);
                modelAndView.addObject("crowdSourceArticleCustomTags", Arrays.asList(crowdSourceArticle.getCustomTags().split("&\\|&")));
                modelAndView.addObject("crowdSourceAttachedArticleList", crowdSourceArticleList);
                modelAndView.addObject("currentUserId", user.getId());
                modelAndView.setViewName("PreviewArticleView");
            } else {
                throw new Exception();
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
        return modelAndView;
    }

    /**
     * This is to publish a article.
     *
     * @param request
     * @param articleId
     * @return
     * @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "publish-article")
    @ResponseBody
    public boolean publishArticle(HttpServletRequest request,
            @RequestParam Long articleId) throws Exception {
        return crowdSourcingService.setPublishFlag(articleId);
    }

    /**
     * This is to with draw a article.
     *
     * @param request
     * @param articleId
     * @return
     * @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "withdraw-article")
    @ResponseBody
    public boolean withdrawArticle(HttpServletRequest request,
            @RequestParam Long articleId) throws Exception {
        CrowdSourceArticle crowdSourceArticle = crowdSourcingService.getArticle(articleId);
        boolean withdrawStatus = true;
        Article article = new Article();
        String resourceTypeCode = crowdSourceArticle.getArticleCategory().getResourceType().getResourceTypeCode();
        article.setRecordIdentifier(resourceTypeCode + "_" + crowdSourceArticle.getArticleCategory().getResourceCode() + "_" + crowdSourceArticle.getId());
        if (crowdSourceArticle.getIsApproved()) {
            withdrawStatus = crowdsourceWebserviceHelper.deleteArticle(article, resourceTypeCode);
        }
        if (withdrawStatus) {
            crowdSourcingService.setWithdrawFlag(articleId);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This is to approve a article by admin .
     *
     * @param request
     * @param articleId
     * @return
     * @throws Exception
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "approve-article")
    @ResponseBody
    public boolean approveArticle(HttpServletRequest request,
            @RequestParam Long articleId) throws Exception {
        CrowdSourceArticle crowdSourceArticle = crowdSourcingService.getArticle(articleId);
        Article article = new Article();
        article.setTitle(crowdSourceArticle.getArticleTitle());
        article.setDescription(crowdSourceArticle.getArticleText());
        article.setUdcNotation(crowdSourceArticle.getUdcTags());
        article.setCustomNotation(crowdSourceArticle.getCustomTags());
        article.setPublishDate(crowdSourceArticle.getPublishDate());
        article.setAuthor(crowdSourceArticle.getUser().getFirstName() + " " + crowdSourceArticle.getUser().getLastName());
        article.setLastModifiedDate(crowdSourceArticle.getLastModifiedDate());
        article.setResourceCode(crowdSourceArticle.getArticleCategory().getResourceCode());
        article.setResourceLabel(crowdSourceArticle.getArticleCategory().getResourceName());
        article.setNvliIdentifier(crowdSourceArticle.getId() + "");
        String resourceTypeCode = crowdSourceArticle.getArticleCategory().getResourceType().getResourceTypeCode();
        article.setRecordIdentifier(resourceTypeCode + "_" + crowdSourceArticle.getArticleCategory().getResourceCode() + "_" + crowdSourceArticle.getId());
        boolean serviceStatus = crowdsourceWebserviceHelper.saveArticle(article, resourceTypeCode);
        if (serviceStatus) {
            crowdSourcingService.setApproveFlag(articleId);
            return true;
        } else {
            return false;
        }
    }

    /**
     * This is to validate the URL.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "is-active-url")
    @ResponseBody
    public boolean isActiveURL(HttpServletRequest request
    ) {
        try {
            URL url = new URL(request.getParameter("activeURL"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            return connection.getResponseCode() == 200;
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return false;
        }
    }

    /**
     * This is to load article register .
     *
     * @return @throws Exception
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "show-articles")
    public ModelAndView showArticles() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My Articles");
        modelAndView.setViewName("ShowArticlesView");
        return modelAndView;
    }

    /**
     * This is to filter the article.
     *
     * @param searchString
     * @param statusTypeFilter
     * @param dataLimit
     * @param pageNo
     * @param isRequiredUserRestriction
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getArticlesByFiltersWithLimit")
    @ResponseBody
    public JSONObject getArticlesByFiltersWithLimit(@RequestParam String searchString,
            @RequestParam String statusTypeFilter,
            @RequestParam int dataLimit,
            @RequestParam int pageNo,
            @RequestParam boolean isRequiredUserRestriction
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("articleList", crowdSourcingService.getArticlesByFiltersWithLimit(userService.getCurrentUser().getId(), searchString, statusTypeFilter, dataLimit, pageNo, isRequiredUserRestriction));
            jsonObject.put("totalCount", crowdSourcingService.getArticlesTotalCountByFilters(userService.getCurrentUser().getId(), searchString, statusTypeFilter, isRequiredUserRestriction));
            return jsonObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to load all articles.
     *
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "manage/articles")
    public ModelAndView manageArticles() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Manage Articles");
        modelAndView.setViewName("ManageArticlesView");
        return modelAndView;
    }

    /**
     * This is to load all UDC tags for crowdsource.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "udc-tags-crowdsource")
    public ModelAndView udcTagsCrowdsource(HttpServletRequest request
    ) {
        ModelAndView modelAndView = new ModelAndView("UDCTagsCrowdsourceView");
        modelAndView.addObject("user", userService.getCurrentUser());
        try {
            Map<Long, String> udcParentMap = new LinkedHashMap<Long, String>();
            TreeSet<UDCConcept> udcConceptSet = new TreeSet<>(crowdSourcingService.getUDCConcept(Long.valueOf("2450")).getUdcConceptSet());
            for (UDCConcept udcConcept : udcConceptSet) {
                udcParentMap.put(udcConcept.getId(), udcConcept.getUdcNotation());
            }
            udcParentMap.put(2450l, "ALL");
            modelAndView.addObject("udcParentMap", udcParentMap);
            TreeSet languageSet = new TreeSet(UDCLanguage.LanguageComparator);
            languageSet.addAll(crowdSourcingService.getIndianLanguageList());
            modelAndView.addObject("languageList", languageSet);
            modelAndView.addObject("udcConceptId", request.getParameter("udcConceptId"));
//            modelAndView.addObject("udcTagsCrowdsourceHistoryList", crowdSourcingService.getUDCTagsCrowdsourceList(userService.getCurrentUser().getId()));
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
        }
        return modelAndView;
    }

    /**
     * This is to UDC tree data .
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUDCTreeviewDataWithPagination")
    @ResponseBody
    public JSONArray getUDCTreeviewDataWithPagination(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.generateUDCTreeViewJSONArray(Long.parseLong(request.getParameter("languageId")), Long.parseLong(request.getParameter("parentId")), request.getParameter("translationUDCLanguageId"), false);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to UDC tree view data with pagination.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUDCTableDataWithPagination")
    @ResponseBody
    public List<UDCConceptDescription> getUDCTableDataWithPagination(HttpServletRequest request
    ) {
        try {
            List<UDCConceptDescription> resultList = new ArrayList<>();
            UDCConceptDescription tempUDCConceptDescription;
            List<UDCConceptDescription> udcConceptDescriptionList = crowdSourcingService.generateUDCTableDataWithPagination(request.getParameter("parentId"), Long.parseLong(request.getParameter("languageId")), Long.parseLong(request.getParameter("translationUDCLanguageId")));
            for (UDCConceptDescription udcConceptDescription : udcConceptDescriptionList) {
                tempUDCConceptDescription = new UDCConceptDescription();
                tempUDCConceptDescription.setDescription("[" + udcConceptDescription.getUdcConceptId().getUdcNotation() + "] " + udcConceptDescription.getDescription());
                tempUDCConceptDescription.setId(udcConceptDescription.getUdcConceptId().getId());
                resultList.add(tempUDCConceptDescription);
            }
            return resultList;
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to get newly translated tags of UDC tag.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getNewlyTranslatedTags")
    @ResponseBody
    public List<UDCConceptDescription> getNewlyTranslatedTags(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.getNewlyTranslatedUDCTagDescription(request.getParameter("parentId"), Long.parseLong(request.getParameter("languageId")), Long.parseLong(request.getParameter("translationUDCLanguageId")));
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to load the UDC translation form.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getUDCTranslation")
    @ResponseBody
    public UDCTranslationBean getUDCTranslation(HttpServletRequest request
    ) {
        try {
            return crowdSourcingService.getUDCTranslation(Long.parseLong(request.getParameter("udcConceptId")), Long.parseLong(request.getParameter("translationUDCLanguageId")), userService.getCurrentUser().getId());
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to save the translation of a UDC tag.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "saveUDCTranslation")
    @ResponseBody
    public boolean saveUDCTranslation(HttpServletRequest request
    ) {
        try {
            long udcConceptId = Long.parseLong(request.getParameter("udcConceptId"));
            long translationUDCLanguageId = Long.parseLong(request.getParameter("translationUDCLanguageId"));
            User user = userService.getCurrentUser();
            UDCTagsCrowdsource udcTagsCrowdsource = crowdSourcingService.checkUDCTagsCrowdsourceAlreadyExist(udcConceptId, translationUDCLanguageId, user.getId());

            if (udcTagsCrowdsource == null) {
                udcTagsCrowdsource = new UDCTagsCrowdsource();
                udcTagsCrowdsource.setUdcConcept(crowdSourcingService.getUDCConcept(udcConceptId));
                udcTagsCrowdsource.setUdcLanguage(crowdSourcingService.getUDCLanguage(translationUDCLanguageId));
                udcTagsCrowdsource.setUser(user);
            }
            udcTagsCrowdsource.setDescription(request.getParameter("translation"));
            udcTagsCrowdsource.setVoteCount(0);
            udcTagsCrowdsource.setTranslationDate(new Timestamp(new Date().getTime()));
            //distribution of translated tags           
            udcTagsCrowdsource.setuDCTranslationExpertId(crowdSourcingService.distributionOfTranslatedTag(udcTagsCrowdsource));
            //save or update udcTagsCrowdSource
            crowdSourcingService.saveOrUpdateUDCTagsCrowdsource(udcTagsCrowdsource);
            //notification
            Notifications notif = new Notifications();
            notif.setNotifactionDateTime(new Date());
            notif.setNotificationMessage(messages.getMessage("notif.udc.translation", new String[]{user.getFirstName() + " " + user.getLastName(), udcTagsCrowdsource.getUdcConcept().getUdcNotation(), udcTagsCrowdsource.getUdcLanguage().getLanguageName()}, Locale.getDefault()));
            notif.setSenderId(user.getId());
            notif.setNotificationTargetURL("/cs/udc-tags-crowdsource?udcConceptId=" + udcConceptId);
            List<Long> userIds = new ArrayList<>();
            udcTagsCrowdsource.getUdcLanguage().getUsers().stream().forEach(reciever -> userIds.add(reciever.getId()));
            notificationService.saveNotification(notif, userIds);
            return true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * This is to vote the UDC tag translation.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "voteUDCTranslation")
    @ResponseBody
    public boolean voteUDCTranslation(HttpServletRequest request
    ) {
        try {
            UDCTagsCrowdsource udcTagsCrowdsource = crowdSourcingService.getUDCTagsCrowdsource(Long.parseLong(request.getParameter("udcTagsCrowdsourceId")));
            udcTagsCrowdsource.setVoteCount(udcTagsCrowdsource.getVoteCount() + 1);
            crowdSourcingService.saveOrUpdateUDCTagsCrowdsource(udcTagsCrowdsource);
            UDCTagVote udcTagVote = new UDCTagVote();
            udcTagVote.setUdcTagsCrowdsource(udcTagsCrowdsource);
            udcTagVote.setUser(userService.getCurrentUser());
            return crowdSourcingService.saveUDCTagVote(udcTagVote);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * This is to get the sub titles of the the video record.
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "getSubtiltes")
    @ResponseBody
    public Map<Integer, String> getSubtiltes(HttpServletRequest request
    ) {
        try {
            String filterString = request.getParameter("filterString").trim();
            return SRTFileParser.parseFile(new FileInputStream(new File(srtFilePath + "subtitleFormat.srt")), filterString);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "fetchMetadata")
    @ResponseBody
    public String fetchMedataFromIsbn(HttpServletRequest request
    ) {
        try {
            return new Gson().toJson(crowdsourceWebserviceHelper.fetchMetadataFromIsbn(request.getParameter("isbn")));
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "z3950WS-sru", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject fetchZ3950SRU(HttpServletRequest request) {
        try {
            JSONObject searchResult = new JSONObject();
            Gson gson = new Gson();
            Z3950Search z3950Search = gson.fromJson(request.getParameter("parameterMap"), Z3950Search.class);
            z3950Search.setLibCode(request.getParameter("selectedLibrary"));
            Z3950Search z = crowdsourceWebserviceHelper.fetchZ3950SRU(z3950Search, Integer.parseInt(request.getParameter("pageNo")), Integer.parseInt(request.getParameter("dataLimit")));
            searchResult.put("Z3950Search", z.getListOfResult());
            searchResult.put("resultSize", z.getResultSize());
            return searchResult;
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }
    
    /**
     * 
     * @param recordName
     * @param currentPage
     * @param totalPages
     * @param request
     * @return 
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping("/full-text-crowdsource/{recordName:.*}/{currentPage}/{totalPages}")
    public ModelAndView crowdsource(@PathVariable("recordName") String recordName, @PathVariable("currentPage") String currentPage, @PathVariable("totalPages") String totalPages, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        String recordLocation= request.getParameter("recordLocation");
        String recordTitle= request.getParameter("recordTitle");
        modelAndView.addObject("recordTitle", recordTitle);
        modelAndView.addObject("recordLocation", recordLocation);
        modelAndView.addObject("recordName", recordName);
        modelAndView.addObject("currentPage", currentPage);
        modelAndView.addObject("totalPages", totalPages);
        modelAndView.addObject("cdn", cdn);
        modelAndView.setViewName("full-text-crowdsource");
        return modelAndView;
    }
    
    @PreAuthorize("isAuthenticated()")
    @RequestMapping (value = "/full-text-save/{recordName:.*}/{hocrFileName:.*}", method = RequestMethod.POST)
    @ResponseBody
    public String getData(@PathVariable("recordName") String recordName, @PathVariable("hocrFileName") String hocrFileName,@RequestParam(required = false, value = "hocr") String hocr){
        System.out.println("recordname is : "+recordName+"hocrfile "+hocrFileName);
        System.out.println("hocr=="+hocr);
        return "OK";
    }
    
}
