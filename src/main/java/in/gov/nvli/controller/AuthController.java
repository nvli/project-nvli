package in.gov.nvli.controller;

import in.gov.nvli.beans.NewUserBean;
import in.gov.nvli.custom.user.CustomUserPasswordReset;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserTheme;
import in.gov.nvli.rabbitmq.RabbitmqProducer;
import in.gov.nvli.service.AdminService;
import in.gov.nvli.service.CustomUserDetailService;
import in.gov.nvli.service.SendMailService;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.service.UserThemeService;
import in.gov.nvli.social.SpringSecuritySignInAdapter;
import in.gov.nvli.util.Captcha;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Constants.UserActivityConstant;
import in.gov.nvli.util.EncodeDecodeUtils;
import in.gov.nvli.util.Helper;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.http.HttpHeaders;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Authorization controller to facilitate various user authentication and
 * security related request handling
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Controller
@RequestMapping(value = "auth")
public class AuthController {

    private static final Logger LOG = Logger.getLogger(AuthController.class);
    @Autowired
    private UserActivityService activityServiceObj;
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    @Qualifier("authManager")
    private AuthenticationManager authManager;
    @Autowired
    private CustomUserDetailService userDetailService;
    @Autowired
    private MessageSource messages;
    @Autowired
    private SendMailService mailService;
    @Value("${sender.ipaddress}")
    private String senderIpaddress;
    @Value("${application.url}")
    private String applicationURL;
    @Autowired
    private ApplicationContext ctx;
    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;
    @Autowired
    private SpringSecuritySignInAdapter springSecuritySignInAdapter;
    @Autowired
    private UsersConnectionRepository usersConnectionRepository;
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private UserThemeService themeService;
    @Value("${user.parent.directory}")
    String userRelatedDirecory;
    @Value("${profile.pic.directory.name}")
    String profilePicDirectory;
    @Value("${themes.directory.name}")
    String themesDirectory;
    @Autowired
    private AdminService adminService;

    @Value("${secret.access.key}")
    private String secretAccessKey;

    /**
     *
     * Authentication Controller for loading Login Page
     *
     * @param request
     * @return ModelAndView object
     */
    @RequestMapping(value = "/login")
    public ModelAndView login(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UserLoginView");
        String referrer = request.getHeader(HttpHeaders.REFERER);
        //check captacha block status
        if (adminService.checkFeatureStatus(Constants.ApplicationConfig.Login_Captacha)) {
            modelAndView.addObject("captchaBlock", true);

        } else {
            modelAndView.addObject("captchaBlock", false);
        }
        if (null != userService.getCurrentUser() && null != referrer) {
            return new ModelAndView(new RedirectView(referrer));
        }
        request.setAttribute("login_attempt", false);
        request.setAttribute("url_prior_login", referrer);
        if (request.getParameter("login_attempt") != null) {
            request.setAttribute("login_attempt", true);
            modelAndView.addObject("failureMessage", messages.getMessage("login.failure.info", null, Locale.getDefault()));
        }
        return modelAndView;
    }

    /**
     * Controller to load appropriate page after successful login of user
     *
     * @param request
     * @param model
     * @return RedirectView
     */
    @RequestMapping(value = "/login/success")
    public RedirectView loginSuccess(HttpServletRequest request, ModelMap model) {
        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userObject = userService.getCurrentUser();
        //log user login activity
        if (userObject != null) {
            activityServiceObj.saveOtherActivityOfUser(userObject.getId(), null, UserActivityConstant.LOGIN, request, userObject.getUsername(), null);

        }
        if (auth.getAuthorities().contains(Constants.UserRole.GENERAL_USER)) {
            return new RedirectView("/home", true);
        }
        return new RedirectView("/user/home", true);
    }

    /**
     * Controller to process logout action
     *
     * @param request
     * @param response
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response, final RedirectAttributes redirectAttributes) {
        String referrer = request.getHeader(HttpHeaders.REFERER);
        SecurityContextHolder.clearContext();
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length != 0) {
            LOG.error("cookies.length " + cookies.length);
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }
        LOG.error("Successfully Logged Out!");
        redirectAttributes.addFlashAttribute("message", "Successfully Logged Out!");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0);
        return new ModelAndView(new RedirectView(request.getContextPath() + "/auth/login"));
//        if (referrer == null || referrer.isEmpty()) {
//            return new ModelAndView(new RedirectView(request.getContextPath() + "/home"));
//        } else if (referrer.endsWith("auth/register")){
//            System.out.println("referrer  " + referrer);
//            return new ModelAndView(new RedirectView(request.getContextPath() +"/auth/login"));
//        }
//        else {
//            return new ModelAndView(new RedirectView(referrer));
//        }
    }

    /**
     * Base Controller for loading User Registration/Sign Up page
     *
     * @param model
     * @param request
     * @param req
     * @return ModelAndView object
     */
    @RequestMapping(value = {"/registration", "/signup"})
    public ModelAndView registration(Model model, WebRequest request, HttpServletRequest req) {
        ModelAndView modelAndView = new ModelAndView();
        String idHash = request.getParameter("idh");
        String mailSendTime = request.getParameter("st");
        HttpSession session = req.getSession();
        if (adminService.checkFeatureStatus(Constants.ApplicationConfig.Registration_Captcha)) {
            modelAndView.addObject("captchaBlock", true);

        } else {
            modelAndView.addObject("captchaBlock", false);
        }
        //String captchaKey = (String) session.getAttribute("captchaKey");
        ProviderSignInUtils providerSignInUtils = new ProviderSignInUtils(connectionFactoryLocator, usersConnectionRepository);
        Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        NewUserBean newUserBean = new NewUserBean();
        if (connection != null) {
            String email = "";
            //get basic info of registrating user from connected provider.
            //for facebook.
            if (connection.getApi() instanceof Facebook) {
                Facebook facebook = (Facebook) connection.getApi();
                String[] fields = {"id", "email", "first_name", "last_name"};
                org.springframework.social.facebook.api.User userProfile = facebook.fetchObject("me", org.springframework.social.facebook.api.User.class, fields);
                email = userProfile.getEmail();
                newUserBean.setEmail(email);
                newUserBean.setFirstName(userProfile.getFirstName());
                newUserBean.setLastName(userProfile.getLastName());
                //for other than facebook providers.
            } else {
                email = connection.fetchUserProfile().getEmail();
                System.out.println("email " + email);
                newUserBean.setFirstName(connection.fetchUserProfile().getFirstName());
                newUserBean.setLastName(connection.fetchUserProfile().getLastName());
                newUserBean.setEmail(email);
                newUserBean.setUsername(connection.fetchUserProfile().getUsername());
            }
            if (userService.emailExist(email)) {
                providerSignInUtils.doPostSignUp(email, request);
                if (this.authenticate(userService.getUserByEmailID(email), req)) {
                    System.out.println(connection.getApi().getClass());
                    springSecuritySignInAdapter.providerSignInLogging(email, connection);
                    return new ModelAndView(new RedirectView("/user/profile", true, true, true));
                } else {
                    return new ModelAndView(new RedirectView("/auth/login", true, true, true));
                }
            }
            modelAndView.addObject("isRedirectedFromProvider", true);
        }
        // If not Normal Registration
        if (mailSendTime != null && idHash != null) {
            try {
                // Registration for invited users
                String decodedSendTime = EncodeDecodeUtils.decodeString(mailSendTime);

                // Setting a flag for invited User Registration
                modelAndView.addObject("isInvited", true);

                int invitaionAccepted = userService.getUserInvitaionByHash(idHash).getInvitationAccepted();

                //calculate time difference and redirect if mail expired
                if (!mailService.checkIfLinkExpired(decodedSendTime) || invitaionAccepted != Constants.InvitationStatus.CANCELLED) {
                    newUserBean.setEmail(userService.getUserInvitaionByHash(idHash).getInviteeEmail());
                } else {
                    String userMsg = messages.getMessage("mail.link.expired", null, Locale.getDefault());
                    modelAndView.addObject("userMsg", userMsg);
                    modelAndView.setViewName("UserMessageView");
                    return modelAndView;
                }
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
                LOG.error(ex.getMessage());
            }
        }
        modelAndView.addObject("newUserBean", newUserBean);
        modelAndView.addObject("inviteIdHash", idHash);
        //modelAndView.addObject("captchaKey", captchaKey);
        modelAndView.setViewName("UserRegistrationView");
        return modelAndView;
    }

    /**
     * Base Controller for loading User Registration completion page. In this
     * registration completion page User will provide more details if required
     * to complete the User Registration process.
     *
     * @return ModelAndView object
     */
    @RequestMapping(value = "/registration/complete")
    public ModelAndView completeRegistration() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UserRegsitrationCompleteView");
        return modelAndView;
    }

    /**
     * Controller for processing registration of form data
     *
     * @param req
     * @param request
     * @param response@param errors
     * @param newUserBean
     * @param result
     * @param modelMap
     * @param idHash
     * @return ModelAndView object
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView registerUserAccount(
            WebRequest req,
            HttpServletRequest request,
            HttpServletResponse response,
            @ModelAttribute("newUserBean") @Valid NewUserBean newUserBean,
            BindingResult result,
            ModelMap modelMap,
            @RequestParam("inviteIdHash") String idHash,
            Errors errors) {

        String secretKey = request.getParameter("secret-key");
        //System.out.println("Secret Key " + request.getParameter("secret-key"));
        ModelAndView modelAndView = new ModelAndView();
        if (result.hasErrors()) {
            modelMap.put(BindingResult.class.getName(), request);
            modelAndView.addObject("userModel", newUserBean);
            modelAndView.addObject("inviteIdHash", idHash);
            modelAndView.setViewName("UserRegistrationView");
            System.out.println("Some Error occured while validation");
            return modelAndView;
        }
        if (userService.checkSecretKey(secretKey) == 0) {
            modelMap.put(BindingResult.class.getName(), request);
            modelAndView.addObject("userModel", newUserBean);
            modelAndView.addObject("inviteIdHash", idHash);
            modelAndView.addObject("msgSecretKey", "Security Code Mismatch. Please check the Secret Key.");
            modelAndView.setViewName("UserRegistrationView");
            return modelAndView;
        } else if (userService.checkSecretKey(secretKey) == -1) {
            modelMap.put(BindingResult.class.getName(), request);
            modelAndView.addObject("userModel", newUserBean);
            modelAndView.addObject("inviteIdHash", idHash);
            modelAndView.addObject("msgSecretKey", "Security Code exhausted!!!");
            modelAndView.setViewName("UserRegistrationView");
            return modelAndView;
        }
        // Creating User Model
        System.out.println(newUserBean);
        User user = new User(newUserBean);
        user.setPassword(passwordEncoder.encode(newUserBean.getPassword()));
        // Grabbing Client's IP Address
        String ipAddress = request.getRemoteAddr();
        // Setting Account Creation Date
        user.setCreatedOn(Helper.rightNow());

        UserTheme defaultTheme = themeService.getThemeByThemeCode("default");
        // if user model is validated then call registerNewUser service to save new user
        user.setIpAddress(ipAddress);
        user.setTheme(defaultTheme);
        boolean success = userService.registerNewUser(user, idHash, request);
        if (success) {
            userService.insertSecretKeyDetails(secretKey, user.getId());
        }
        User registered = userService.getUserByEmailID(user.getEmail());
        System.out.println(registered.getId());
        // long userid = registered.getId();
        try {
            if (registered != null) {
                String userSalt = EncodeDecodeUtils.getSHA256EncodedData(registered.getId().toString());
                System.out.println("salt " + userSalt);
                registered.setSalt(userSalt);
                userService.updateUser(registered);
                //on registeration,folder structure is created as userid parent folder and themes,profile-pic etc as subfolders
                File profilePicDir = new File(userRelatedDirecory + File.separator + registered.getId() + File.separator + profilePicDirectory);
                File themeDir = new File(userRelatedDirecory + File.separator + registered.getId() + File.separator + themesDirectory);
                System.out.println("profilepic folder created" + profilePicDir.mkdirs());
                System.out.println("themes folder created" + themeDir.mkdirs());
                ProviderSignInUtils providerSignInUtils = new ProviderSignInUtils(connectionFactoryLocator, usersConnectionRepository);
                providerSignInUtils.doPostSignUp(registered.getEmail(), req);
            }
        } catch (UnsupportedEncodingException ex) {
            LOG.error(ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex) {
            LOG.error(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        modelAndView.addObject("userModel", newUserBean);
        modelAndView.addObject("inviteIdHash", idHash);

        /**
         * Perform login authentication after successful registration
         */
        try {
            UserDetails userDetails = userDetailService.loadUserByUsername(user.getEmail());

            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    userDetails,
                    user.getPassword(),
                    userDetails.getAuthorities());
//                authManager.authenticate(auth);
            if (auth.isAuthenticated()) {
                activityServiceObj.saveOtherActivityOfUser(registered.getId(), null, UserActivityConstant.SIGNUP, request, registered.getUsername(), null);
                try {
                    String confirmationUrl = "auth/account/activate";
                    String message = messages.getMessage("registration.activation.message", null, Locale.getDefault());
                    String subject = messages.getMessage("registration.activation.subject", null, Locale.getDefault());
                    RabbitmqProducer producer = new RabbitmqProducer();
                    JSONObject detailsJson = new JSONObject();
                    detailsJson.put("firstName", user.getFirstName());
                    detailsJson.put("salt", user.getSalt());
                    detailsJson.put("servletCtxName", request.getServletContext().getContextPath());
                    detailsJson.put("userMailId", user.getEmail());
                    detailsJson.put("message", message);
                    detailsJson.put("controllerMapping", confirmationUrl);
                    detailsJson.put("subject", subject);
                    producer.sendEmailDetailsToQueue(amqpTemplate, detailsJson, Constants.RabbitMq.EMAIL_VERIFICATION_KEY);
                } catch (Exception e) {
                    e.printStackTrace();
                    LOG.error(e.getMessage(), e);
                }
                SecurityContextHolder.getContext().setAuthentication(auth);
                modelAndView.addObject("user", userService.getCurrentUser());
            }
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Problem authenticating user " + user.getFirstName() + " " + user.getLastName() + e.getMessage());
        }
        modelAndView.setViewName("UserInterestView");
//        } else {
        modelAndView.addObject("userModel", newUserBean);
        modelAndView.addObject("inviteIdHash", idHash);
//            modelAndView.setViewName("UserRegistrationView");
//        }
        return modelAndView;
    }

    /**
     * Controller to activate user after he clicks on verification email.
     *
     * @param req HttpServletRequest object
     * @return ModelAndView
     */
    @RequestMapping(value = {"/account/activate"})
    public ModelAndView activateAccount(HttpServletRequest req) {
        ModelAndView modelAndView = new ModelAndView("UserMessageView");
        try {
            String userDigest = req.getParameter("idh");//idh is user hash of id
            User user = userService.getUserBySalt(userDigest);
            String userMsg = "";
            if (user != null) {
                user.setActive(Constants.USER_ACTIVE);
                userService.updateUser(user);
                modelAndView.addObject("user", user);
                userMsg = messages.getMessage("registration.activation.success",
                        new Object[]{senderIpaddress + req.getServletContext().getContextPath() + "/auth/login"},
                        Locale.getDefault());
            } else {
                userMsg = messages.getMessage("registration.activation.error",
                        null,
                        Locale.getDefault());
            }
            modelAndView.addObject("userMsg", userMsg);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return modelAndView;
    }

    /**
     * Controller to load view to reset forgotten password
     *
     * @param request
     * @return ModelAndView
     */
    @RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
    public ModelAndView forgotPasswordView(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UserForgotPasswordView");
        return modelAndView;
    }

    /**
     * This controller method handles the request for Forgot Password event and
     * returns appropriate message
     *
     * @param email
     * @param req
     * @return String usrMessage
     */
    @RequestMapping(value = "/forgot-password-mail")
    @ResponseBody
    public JSONObject forgotPassword(@RequestParam("emailAdd") String email, HttpServletRequest req) {
        return userService.sendResetPasswordMail(email, ctx, req.getServletContext().getContextPath());
    }

    /**
     * This controller method redirects requests to reset pass password view to
     * allow user to reset their forgotten password
     *
     * @param request
     * @return ModelAndView
     */
    @RequestMapping(value = "/redirect-to-reset-password")
    public ModelAndView openResetPassword(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            String mailSendTime = request.getParameter("st");
            String decodedSendTime = EncodeDecodeUtils.decodeString(mailSendTime);
            //calculate time difference and redirect if mail expired
            if (!mailService.checkIfLinkExpired(decodedSendTime)) {
                String userDigest = request.getParameter("idh");//idh is user hash of id
                User user = userService.getUserBySalt(userDigest);
                if (user != null) {
                    CustomUserPasswordReset customUserPasswordReset = new CustomUserPasswordReset();
                    customUserPasswordReset.setEmailId(user.getEmail());
                    modelAndView.addObject("PasswordResetBean", customUserPasswordReset);
                }
            } else {
                String userMsg = messages.getMessage("mail.link.expired", null, Locale.getDefault());
                modelAndView.addObject("userMsg", userMsg);
                modelAndView.setViewName("UserMessageView");
                return modelAndView;
            }
        } catch (UnsupportedEncodingException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        modelAndView.setViewName("UserResetPasswordView");
        return modelAndView;
    }

    /**
     * This controller method handles the request to match the generated
     * passcode to reset forgotten password to validate reset forgotten password
     * request.
     *
     * @param passcode
     * @param emailId
     * @return String matchStatus
     */
    @RequestMapping(value = "/match-passcode")
    @ResponseBody
    public String matchPasscode(@RequestParam("passcode") String passcode,
            @RequestParam("userMail") String emailId) {
        String matchStatus = "";
        if (userService.matchPasscode(passcode, emailId)) {
            matchStatus = "1";//success
        } else {
            matchStatus = "0";//error
        }
        System.out.println("Match Status === " + matchStatus);
        return matchStatus;
    }

    /**
     * This controller method handle to reset password request and loads
     * UserMessageView to show the appropriate status of resetting password
     * event.
     *
     * @param request
     * @param passwordResetBean
     * @param result
     * @return ModelAndView
     */
    @RequestMapping(value = "/reset-password")
    public ModelAndView resetPassword(HttpServletRequest request,
            @ModelAttribute("PasswordResetBean") @Valid CustomUserPasswordReset passwordResetBean, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView("UserMessageView");
        try {
            String userMsg = "";
            if (result.hasErrors()) {
                userMsg = messages.getMessage("reset.password.error",
                        new Object[]{senderIpaddress + request.getServletContext().getContextPath() + "/auth/login"},
                        Locale.getDefault());
                return modelAndView;
            }

            User user = userService.getUserByEmailID(passwordResetBean.getEmailId());
            System.out.println("--" + passwordEncoder.encode(passwordResetBean.getNewPassword()));
            user.setPassword(passwordEncoder.encode(passwordResetBean.getNewPassword()));
            userService.updateUser(user);
            activityServiceObj.saveOtherActivityOfUser(user.getId(), null, UserActivityConstant.PASSWORD_CHANGE, request, user.getUsername(), null);
            userMsg = messages.getMessage("reset.password.success",
                    new Object[]{applicationURL + "/auth/login"},
                    Locale.getDefault());
            modelAndView.addObject("userMsg", userMsg);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * This method handles the request to resend email-verification email to the
     * registered user, who hasn't yet verified their email address yet.
     *
     * @param request
     * @return String
     */
    @RequestMapping(value = "/resend-activation-mail")
    @ResponseBody
    public String resendMail(HttpServletRequest request) {
        String usrMessage = "";
        User user = userService.getCurrentUser();
        if (user != null) {
            try {
                String confirmationUrl = "auth/account/activate";
                String message = messages.getMessage("registration.activation.message", null, Locale.getDefault());
                String subject = messages.getMessage("registration.activation.subject", null, Locale.getDefault());
                //mailService.sendMail(user.getFirstName(), user.getSalt(), request.getServletContext().getContextPath(), user.getEmail(), message, confirmationUrl, subject);
                RabbitmqProducer producer = new RabbitmqProducer();
                JSONObject detailsJson = new JSONObject();
                detailsJson.put("firstName", user.getFirstName());
                detailsJson.put("salt", user.getSalt());
                detailsJson.put("servletCtxName", request.getServletContext().getContextPath());
                detailsJson.put("userMailId", user.getEmail());
                detailsJson.put("message", message);
                detailsJson.put("controllerMapping", confirmationUrl);
                detailsJson.put("subject", subject);
                producer.sendEmailDetailsToQueue(amqpTemplate, detailsJson, Constants.RabbitMq.EMAIL_VERIFICATION_KEY);
                usrMessage = messages.getMessage("registration.email.verify.sent", null, Locale.getDefault());
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                usrMessage = messages.getMessage("registration.email.send.error", null, Locale.getDefault());
            }
        }

        return usrMessage;
    }

//    @RequestMapping(value = {"/check/currentPassword"})
//    @ResponseBody
//    public boolean usernameAvailbale(
//            @RequestParam(value = "current_password") String currentPassword
//    ) {
//        return userService. checkCurrentPassword(currentPassword);
//    }
    /**
     * This controller method handles the request to update user's password
     *
     * @param request
     * @param response
     * @param currentPassword
     * @param newPassword
     * @param reNewPassword
     * @return JSONObject
     */
    @RequestMapping(value = "/update/password", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject updatePassword(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "current_password") String currentPassword,
            @RequestParam(value = "new_password") String newPassword,
            @RequestParam(value = "re_new_password") String reNewPassword) {
        JSONObject object = new JSONObject();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            String email = userDetails.getUsername();
            User user = userService.getUserByEmailID(email);
            if (passwordEncoder.matches(currentPassword, user.getPassword())
                    && newPassword.equals(reNewPassword)) {
                user.setPassword(passwordEncoder.encode(newPassword));
                userService.updateUser(user);
                userDetails = userDetailService.loadUserByUsername(email);
                auth.setDetails(userDetails);
                SecurityContextHolder.getContext().setAuthentication(auth);
                object.put("status", Constants.AjaxResponseCode.SUCCESS);
                activityServiceObj.saveOtherActivityOfUser(user.getId(), null, UserActivityConstant.PASSWORD_CHANGE, request, user.getUsername(), null);
            } else {
                object.put("status", Constants.AjaxResponseCode.ACCESS_DENIED);
                object.put("message", messages.getMessage("error.403", null, Locale.getDefault()));
            }
        }
        return object;
    }

    /**
     * This method validates the email for being updated if available to be
     * updated. The email shouldn't be registered with any other user.
     *
     *
     * @param currentEmail
     * @return JSONObject
     */
    @RequestMapping(value = "/validate/email", produces = "application/json", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject validateEmailForUpdation(
            @RequestParam(value = "currentEmail") String currentEmail) {
        JSONObject obj = new JSONObject();
        boolean isValid = userService.getCurrentUser().getEmail().equals(currentEmail);
        if (isValid) {
            obj.put("status", Constants.AjaxResponseCode.SUCCESS);
            obj.put("message", "Email is valid");
        } else {
            obj.put("status", Constants.AjaxResponseCode.FAILED);
            obj.put("message", "Invalid Email ID");
        }
        return obj;
    }

    /**
     * Handles the request to update Email Address.
     *
     * @param request
     * @param response
     * @param currentEmail
     * @param newEmail
     * @return JSONObject
     */
    @RequestMapping(value = "/update/email", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject updateEmailAddress(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "current_email") String currentEmail,
            @RequestParam(value = "new_email") String newEmail) {
        JSONObject object = new JSONObject();

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            String email = userDetails.getUsername();
            User user = userService.getUserByEmailID(email);
            //TODO : validate if new email address is a valid email address & exists & no other account is registered with it
            if (user.getEmail().equals(currentEmail)) {
                user.setEmail(newEmail);
                userService.updateUser(user);
                userDetails = userDetailService.loadUserByUsername(email);
                auth.setDetails(userDetails);
                SecurityContextHolder.getContext().setAuthentication(auth);
                object.put("status", Constants.AjaxResponseCode.SUCCESS);
            } else {
                object.put("status", Constants.AjaxResponseCode.INPUT_ERROR);
                object.put("message", "Input Error");
            }
        } else {
            object.put("status", Constants.AjaxResponseCode.ACCESS_DENIED);
            object.put("message", messages.getMessage("error.403", null, Locale.getDefault()));
        }
        return object;
    }

    /**
     * This method authenticates a user using Spring Security
     *
     *
     * @param user
     * @param request
     * @return boolean authenticated
     */
    public boolean authenticate(User user, HttpServletRequest request) {
        UserDetails userDetails = userDetailService.loadUserByUsername(user.getEmail());

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                userDetails,
                user.getPassword(),
                userDetails.getAuthorities());
//                authManager.authenticate(auth);
        boolean authenticated = auth.isAuthenticated();
        if (authenticated) {
//                    activityServiceObj.saveUserActivityLog(Constants.UserActivities.SIGNUP, null, registered, null, request);
//            try {
//                String confirmationUrl = "auth/account/activate";
//                String message = messages.getMessage("registration.activation.message", null, Locale.getDefault());
//                String subject = messages.getMessage("registration.activation.subject", null, Locale.getDefault());
//                mailService.sendMail(user.getFirstName(), user.getSalt(), request, user.getEmail(), message, confirmationUrl, subject);
//            } catch (Exception e) {
//                e.printStackTrace();
//                LOG.error(e.getMessage(), e);
//            }
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        return authenticated;
    }

    /**
     * This method use to check whether password is same or not at the time of
     * account deactivation
     *
     * @return boolean
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = {"/check/password"}, method = RequestMethod.POST)
    @ResponseBody
    public boolean checkPassword(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "password") String password) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        String email = userDetails.getUsername();
        User user = userService.getUserByEmailID(email);
        if (passwordEncoder.matches(password, user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }

    @RequestMapping(value = "/generateCaptchImage")
    public String generateCaptchImage(HttpServletRequest request, HttpServletResponse response) {
        //generate captcha image for registration
        Captcha.generateCaptchImage(request, response);
        return null;
    }

    @RequestMapping(value = {"/validateCaptcha/{captchatext}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public JSONObject validateCaptcha(HttpServletRequest request, HttpServletResponse response, @PathVariable("captchatext") String captchatext) {

        JSONObject obj = new JSONObject();
        HttpSession session = request.getSession();
        String captchaKey = (String) session.getAttribute("captchaKey");
        if (!captchaKey.equals(captchatext)) {
            obj.put("status", -1);
            obj.put("message", "Invalid Captcha");
        } else {
            obj.put("status", 1);
            obj.put("message", "OK");
        }
        return obj;
    }
}
