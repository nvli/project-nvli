/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.controller;

import in.gov.nvli.dto.PolicyTypeDTO;
import in.gov.nvli.dto.PrivacyDTO;
import in.gov.nvli.service.PolicyService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@RestController
@RequestMapping(value = "policy")
public class PolicyController {

    private static final Logger LOG = Logger.getLogger(PolicyController.class);
    @Autowired
    private PolicyService policyService;

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "create-privacy-policy/{typeId}")
    public ModelAndView createPrivacyPolicy(
            @PathVariable(value = "typeId") String id) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("id", id);
        modelAndView.setViewName("CreatePrivacyPolicyView");
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/manage/policy-types/{id}")
    @ResponseBody
    public ModelAndView privacyPolicy(
            @PathVariable(value = "id") String Id) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("typeId", Id);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        breadcrumbMap.put("Policy Management", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", policyService.findPolicyTypeById(Long.parseLong(Id)).getPolicyName());
        modelAndView.setViewName("PrivacyPolicyView");
        return modelAndView;
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "policy", produces = "application/json", method = RequestMethod.POST)
    public JSONObject PrivacyPolicy(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "text") String content,
            @RequestParam(value = "tittle") String tittle,
            @RequestParam(value = "policyId") String typeId)
            throws Exception {
        JSONObject object = new JSONObject();
        if (policyService.checkPolicy(typeId)) {
            if (policyService.createPrivacyPolicy(content, tittle, typeId)) {
                object.put("status", Constants.PrivacypPolicyStatus.CREATED);
                object.put("notificationActivityType", Constants.UserActivityConstant.POLICY_UPDATED);
                object.put("message", "Policy Created");
                return object;
            } else {
                object.put("status", Constants.PrivacypPolicyStatus.NOTCREATED);
                return object;
            }
        } else {
            PrivacyDTO Latestpolicy = policyService.findMaximumVersion(typeId);
            if (content.trim().equalsIgnoreCase(Latestpolicy.getDescription())) {
                object.put("status", Constants.PrivacypPolicyStatus.ALREADYUPDATED);
                return object;
            } else {
                policyService.updatePolicy(content, tittle, typeId);
                object.put("status", Constants.PrivacypPolicyStatus.UPDATED);
                object.put("notificationActivityType", Constants.UserActivityConstant.POLICY_UPDATED);
                object.put("message", "Policy Updated");
                return object;
            }
        }
    }

    @RequestMapping(value = "/check/policy")
    @ResponseBody
    public boolean checkPolicy(
            @RequestParam(value = "typeId") String typeId) {
        return policyService.checkPolicy(typeId);
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "getPoliciesByFiltersWithLimit")
    @ResponseBody
    public JSONObject getPoliciesByFiltersWithLimit(@RequestParam String searchString, @RequestParam int dataLimit, @RequestParam int pageNo, @RequestParam String typeId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("policyList", policyService.getPoliciesByFiltersWithLimit(searchString, dataLimit, pageNo, typeId));
            jsonObject.put("totalCount", policyService.getPoliciesTotalCountByFilters(searchString, typeId));
            return jsonObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/find/maximum-version")
    @ResponseBody
    public JSONObject findMaxVersion(
            @RequestParam(value = "typeId") String id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("policyObject", policyService.findMaximumVersion(id));
            jsonObject.put("status", 1);
            return jsonObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return null;
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/preview-policy", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject previewPolicy(
            @RequestParam(value = "privacyId") String pId) throws Exception {
        JSONObject jsonObject = new JSONObject();
        try {
            PrivacyDTO policy = policyService.getPolicy(Long.parseLong(pId));
            jsonObject.put("policyTittle", policy.getTittle());
            jsonObject.put("policyDesc", policy.getDescription());
            return jsonObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return null;
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/check/policy-type", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject checkPolicyType(
            @RequestParam(value = "typeId") String typeId) throws Exception {
        JSONObject jsonObject = new JSONObject();
        PolicyTypeDTO policyType = policyService.findPolicyTypeById(Long.parseLong(typeId));
        try {
            if (policyType.getPolicyName().equalsIgnoreCase(Constants.PolicyType.PRIVACYPOLICY)) {
                jsonObject.put("policyName", Constants.PolicyType.PRIVACYPOLICY);
                jsonObject.put("status", 1);
            } else if (policyType.getPolicyName().equalsIgnoreCase(Constants.PolicyType.TNCPOLICY)) {
                jsonObject.put("policyName", Constants.PolicyType.TNCPOLICY + " Policy");
                jsonObject.put("status", 2);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
        return jsonObject;
    }

    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = {"types"})
    public List<PolicyTypeDTO> getPolicyTypes() {
        return policyService.getPolicyTypes();
    }
}
