/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.controller;

import in.gov.nvli.domain.resource.ArtistProfile;
import in.gov.nvli.domain.resource.ArtisticResourceType;
import in.gov.nvli.service.ArtistService;
import in.gov.nvli.service.ArtistSiteService;
import in.gov.nvli.util.ArtistProfileUtils;
import in.gov.nvli.util.ProfileExtractor;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller class deals with Artist Profile section
 *
 * @author Gajraj Singh Tanwar
 * 
 */
@Controller
@RequestMapping(value = "artist")
public class ArtistController {

    private final static Logger LOGGER = Logger.getLogger(ArtistController.class);
    @Autowired
    private ArtistService artistService;

    @Autowired
    private ArtistSiteService artistSiteService;

     /**
     * This method is used to fetch all the artist presetn in the system.
     */
    @RequestMapping(value = "all")
    public ModelAndView getAllArtistInSystem(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("artistlist");
        try {
            List<ArtistProfile> artistList = artistService.listAllArtist();
            LOGGER.info("list----" + artistList.size());
            request.setAttribute("artistList", artistList);
        }
        catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return modelAndView;
    }
    /**
     * This method is used to fetch specified artist in the system based on Gallery
     * @Param galleryId Gallery from which artist should be shown
     */
    @RequestMapping(value = "ingallery/{galleryId}")
    public ModelAndView getArtistInSystem(@PathVariable int galleryId) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("artistlist");
        //System.out.println("INSIDE IN GALLERY CALL ");
        try {
            List<ArtistProfile> artistList = artistService.getAllArtistForGallary(galleryId);
            LOGGER.info("Artist in gallery --> " + galleryId + "list----" + artistList.size());
            modelAndView.addObject("artistList", artistList);
            modelAndView.addObject("artistSiteID", galleryId);
        }
        catch (Exception ex) {
           // ex.printStackTrace();
            LOGGER.error(ex.getMessage());
        }
        return modelAndView;
    }

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "gallery")
    public ModelAndView getArtistForGallery(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("artSitelist");
        try {
            List<ArtisticResourceType> artisticResourceList = artistSiteService.getAllArtResource();
            LOGGER.info(" artisticResourceList list----" + artisticResourceList.size());
            request.setAttribute("artisticResourceList", artisticResourceList);
        }
        catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return modelAndView;
    }

    /*
     update all artist details for specified gallery
     */
    @RequestMapping(value = "/update/{artistSiteId}")
    @ResponseBody
    public void updateArtistSite(@PathVariable int artistSiteId) throws IOException, InterruptedException {
        List<ArtistProfile> artistList = artistService.getAllArtistForGallary(artistSiteId);
        System.err.println("Site ID  : " + artistSiteId);
        if (artistList != null && !artistList.isEmpty()) {
            for (ArtistProfile artist : artistList) {
                Thread.sleep(20000);
                String artFurl = ArtistProfileUtils.getArtistFinalURL(artist.getArtistProfileLink(), artistSiteService.getSiteById(artist.getArtistResourceID().getArtisticResourseId()));
                String artistDescription = ArtistProfileUtils.getArtistProfileInformation(artFurl, 40);
                LOGGER.info(" Artist ID : " + artist.getId() + "\t and Description : \n" + artistDescription);
                artist.setArtistDescription(artistDescription);
                artistService.updateArtist(artist);
            }

        }

    }

    /**
     * This function extract the all the artistic profile from the site.
     *
     * @param artistSiteId
     * @throws IOException
     * @throws NullPointerException
     * @throws InterruptedException
     */
    @RequestMapping(value = "/extract/{artistSiteId}")
    @ResponseBody
    public void extractArtistForSite(@PathVariable int artistSiteId) throws IOException, NullPointerException, InterruptedException {
        String site = artistSiteService.getSiteById(artistSiteId);
        LOGGER.info("Extraction Process for Site ID : " + artistSiteId + "   begun!!!!!!!");
        ProfileExtractor pe = new ProfileExtractor(site);
        pe.getPossibleArtistFromSite(3);
        LOGGER.info("Total Number of the links in the Linkmap" + pe.getLinkMap().size());

        List<ProfileExtractor.Artist> aList = pe.getArtistList();
        LOGGER.info("Total Number of the links in the List" + aList.size());

        for (ProfileExtractor.Artist artist : aList) {
            String finalUrl = ArtistProfileUtils.getArtistFinalURL(artist.getArtistProfileLink(), site);
            String description = ArtistProfileUtils.getArtistProfileInformation(finalUrl, 40);
            ArtistProfile ap = new ArtistProfile(artist.getArtistName(), finalUrl, null, artistSiteId);
            ap.setArtistDescription(description);
            boolean created = artistService.createArtist(ap);
            if (created) {
                LOGGER.info("New Artist Profile Created");
            }
        }
    }

    /**
     * update the existing artist profile with id
     *
     * @param artistID
     * @throws IOException
     */
    @RequestMapping(value = "existing/update/{artistID}")
    @ResponseBody
    public void updateExistingArtist(@PathVariable int artistID) throws IOException {
        ArtistProfile artist = artistService.getArtist(artistID);
        String artFurl = ArtistProfileUtils.getArtistFinalURL(artist.getArtistProfileLink(), artistSiteService.getSiteById(artist.getArtistResourceID().getArtisticResourseId()));
        String artistDescription = ArtistProfileUtils.getArtistProfileInformation(artFurl, 40);
        LOGGER.info(" Artist ID : " + artist.getId() + "\t and Description : \n" + artistDescription);
        artist.setArtistDescription(artistDescription);
        artistService.updateArtist(artist);

    }

      /**
     * Add or Create new Artist
     *
     * @param artistID
     * @throws IOException
     */
    @RequestMapping(value = "/add/new")
    public ModelAndView addNewArtist() {
        ArtistProfile ap = new ArtistProfile();
        ap.setArtistDescription("test");
        ap.setArtistName("test1");
        ap.setArtistImageURL(null);
        ArtisticResourceType rs = new ArtisticResourceType();
        rs.setArtistSite("tsuieete");
        rs.setSiteName("name");
        ap.setArtistResourceID(rs);
        artistService.createArtist(ap);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("artistlist");
        return modelAndView;

    }

}
