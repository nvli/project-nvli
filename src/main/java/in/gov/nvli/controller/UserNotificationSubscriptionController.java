/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.controller;

import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.NotificationSubscriptionService;
import in.gov.nvli.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * class represent all functionality related to user subscription
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Controller
@RequestMapping("/notificationSubscription")
public class UserNotificationSubscriptionController {

    @Autowired
    private NotificationSubscriptionService notificationSubscriptionService;

    @Autowired
    private UserService userService;

    /**
     * method to find notification subscription and unsubscription
     *
     * @return
     */
    @RequestMapping(value = "/notifications-subscription")
    public ModelAndView notificationsSubscription() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("UserNotificationsSubsciptionView");
        mav.addObject("user", userService.getCurrentUser());
        return mav;
    }

    /**
     * Method to update the subscription status of Link share activity via email
     * in NVLI
     *
     * @param data
     * @return boolean status
     */
    @RequestMapping(value = "/check/nvli-share-status", method = RequestMethod.GET)
    @ResponseBody
    public boolean checkNvliShareStatus() {
        User user = userService.getCurrentUser();
        return notificationSubscriptionService.checkNvliShareLinkStatus(user.getId(),"WithEmail");
    }

    @RequestMapping(value = "/nvli/link/share/send/email/{data}")
    public boolean subscribeUnsubscribeNvliShareLinkStatusWithEmail(@PathVariable(value = "data") String data) {
        User user = userService.getCurrentUser();
        boolean NvliLinkShareStatus = false;
        if (data.equals("checked")) {
            NvliLinkShareStatus = true;
        }
        return notificationSubscriptionService.updateNvliLinkShareWithEmail(NvliLinkShareStatus, user.getId());
    }
   

}
