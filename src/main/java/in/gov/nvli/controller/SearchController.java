package in.gov.nvli.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.SeekableStream;
import de.undercouch.citeproc.CSL;
import de.undercouch.citeproc.csl.CSLDate;
import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.csl.CSLItemDataBuilder;
import de.undercouch.citeproc.csl.CSLType;
import in.gov.nvli.custom.user.CustomRecordReview;
import in.gov.nvli.dao.resource.IGroupTypeDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.marc21.CollectionType;
import in.gov.nvli.domain.resource.ENews;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.MetadataIntegrator;
import in.gov.nvli.domain.resource.OpenRepository;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.resource.WebCrawler;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.ReviewAndRating;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.mongodb.service.ReviewAndRatingService;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Constants.UserActivityConstant;
import in.gov.nvli.util.Helper;
import in.gov.nvli.webserviceclient.mit.BasicSearchWebserviceHelper;
import in.gov.nvli.webserviceclient.mit.BookViewer;
import in.gov.nvli.webserviceclient.mit.ImageDetails;
import in.gov.nvli.webserviceclient.mit.ImageTag;
import in.gov.nvli.webserviceclient.mit.Path;
import in.gov.nvli.webserviceclient.mit.Record;
import in.gov.nvli.webserviceclient.mit.SearchComplexityEnum;
import in.gov.nvli.webserviceclient.mit.SearchResponse;
import in.gov.nvli.webserviceclient.mit.SearchResult;
import in.gov.nvli.webserviceclient.mit.SearchResultResponse;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.media.jai.ImageLayout;
import javax.media.jai.NullOpImage;
import javax.media.jai.OpImage;
import javax.media.jai.PlanarImage;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.h2.util.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * This controller handles request for search in all resource and in particular
 * resources. Also record preview with View All,Citation functionality.
 *
 * @author Priya Bhalerao<mpriya@cdac.in>
 * @author Madhuri Dhone
 * @author Ruturaj Powar<ruturajp@cdac.in>
 * @author Vivek Bugale
 * @author Ritesh Malviya
 * @version 1
 * @since 1
 */
@Controller
@RequestMapping(value = "search")
public class SearchController {

    /**
     * The name of the property LOGGER used to holds {@link Logger} object
     * reference.
     */
    private final static Logger LOGGER = Logger.getLogger(SearchController.class);
    /**
     * The name of the property groupTypeDAO used to holds {@link IGroupTypeDAO}
     * object reference.
     */
    @Autowired
    public IGroupTypeDAO groupTypeDAO;
    /**
     * The name of the property restTemplate used to holds {@link RestTemplate}
     * object reference.
     */
    RestTemplate restTemplate = null;
    /**
     * The name of the property modelAndView used to holds {@link ModelAndView}
     * object reference.
     */
    ModelAndView modelAndView = null;
    /**
     * The name of the property mitWSBaseURL used to holds web-service base URL.
     */
    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;
    /**
     * The name of the property baseLocationTif used to holds record preview
     * TIFF base URL/location.
     */
    @Value("${nvli.record.preview.tif.base.location}")
    private String baseLocationTif;
    /**
     * The name of the property baseLocation used to holds record preview base
     * URL/location.
     */
    @Value("${nvli.record.preview.base.location}")
    private String baseLocation;
    /**
     * The name of the property resourceTypeDAO used to holds
     * {@link IResourceTypeDAO} object reference.
     */
    @Autowired
    public IResourceTypeDAO resourceTypeDAO;
    /**
     * The name of the property resourceDAO used to holds {@link IResourceDAO}
     * object reference.
     */
    @Autowired
    public IResourceDAO resourceDAO;
    /**
     * The name of the property imageLocation used to holds resource image
     * location.
     */
    @Value("${nvli.resource.image.location}")
    private String imageLocation;
    /**
     * The name of the property qrCodeUrl used to holds QR Code URL.
     */
    private String qrCodeUrl;
    /**
     * The name of the property userActivityService used to holds
     * {@link UserActivityService} object reference.
     */
    @Autowired
    private UserActivityService userActivityService;
    /**
     * The name of the property userService used to holds {@link UserService}
     * object reference.
     */
    @Autowired
    private UserService userService;
    /**
     * The name of the property servletContext used to holds
     * {@link ServletContext} object reference.
     */
    @Autowired
    private ServletContext servletContext;
    /**
     * The name of the property ctx used to holds {@link ApplicationContext}
     * object reference.
     */
    @Autowired
    private ApplicationContext ctx;
    /**
     * The name of the property crowdSourcingService used to holds
     * {@link CrowdSourcingService} object reference.
     */
    @Autowired
    private CrowdSourcingService crowdSourcingService;
    /**
     * The name of the property groupIconLocation used to holds group icon
     * location.
     */
    @Value("${nvli.group.image.location}")
    private String groupIconLocation;
    /**
     * The name of the property resourceTypeIconSearchLocation used to holds
     * resource type icon search location.
     */
    @Value("${nvli.resourcType.search.image.location}")
    private String resourceTypeIconSearchLocation;
    /**
     * The name of the property resourceTypeIconDefaultLocation used to holds
     * resource type icon default location.
     */
    @Value("${nvli.resourcType.default.image.location}")
    private String resourceTypeIconDefaultLocation;
    /**
     * The name of the property resourceService used to holds
     * {@link ResourceService} object reference.
     */
    @Autowired
    public ResourceService resourceService;

    @Autowired
    private BasicSearchWebserviceHelper basicSearchWebserviceHelper;

    @Autowired
    ActivityLogService activityLogService;
    /**
     * This is used to communicate with user review repository.
     */
    @Autowired
    ReviewAndRatingService reviewAndRatingService;
    /**
     * To hold the resource type icon read location
     */
    @Value("${nvli.read.resourcType.image.location}")
    private String resourceTypeIconReadLocation;

    /**
     * To hold the resource type default icon read location
     */
    @Value("${nvli.read.resourcType.default.image.location}")
    private String resourceTypeIconDefalutReadLocation;

    /**
     * To hold the resource type search icon read location
     */
    @Value("${nvli.read.resourcType.search.image.location}")
    private String resourceTypeIconSearchReadLocation;

    /**
     * To hold the group icon read location
     */
    @Value("${nvli.read.group.image.location}")
    private String groupIconReadLocation;

    /**
     * For save search functionality this method is used to check if link is
     * already saved or not.
     *
     * @param link url to be checked
     * @return JSONObject containing appropriate message about is current url is
     * saved or not.
     */
    @RequestMapping(value = "check/saved", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject checkIfLinkSaved(@RequestBody String link) throws UnsupportedEncodingException {
        link = URLDecoder.decode(link, "UTF-8");
        if (link.indexOf("#") != -1) {
            link = link.substring(0, link.indexOf("#"));
        }
        JSONObject finalJSONObj = new JSONObject();
        User loggedInUser = userService.getCurrentUser();
        if (loggedInUser != null) {
            link = getURLWithoutContextFromUrl(ctx, link);
            String isSaved = activityLogService.checkIfLinkAlreadySaved(loggedInUser.getId(), link);
            if (isSaved.equals("notSaved")) {
                finalJSONObj.put("isSaved", "notSaved");
            } else {
                finalJSONObj.put("isSaved", "saved");
                finalJSONObj.put("savedPhrase", isSaved);
            }
        } else {
            finalJSONObj.put("isSaved", "notLoggedIn");
        }
        return finalJSONObj;
    }

    /**
     * This method is used to save the url when user click on 'save this search'
     * button
     *
     * @param link current url to which user is visiting
     * @param savedPhrase name by which this search to be saved
     * @param request
     * @return JSONObject containing appropriate message.
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "save/saved/link")
    @ResponseBody
    public JSONObject saveSavedLink(@RequestParam("link") String link, @RequestParam("savedPhrase") String savedPhrase, HttpServletRequest request) {
        JSONObject finalJSONObj = new JSONObject();
        try {
            link = URLDecoder.decode(link, "UTF-8");
            if (link.indexOf("#") != -1) {
                link = link.substring(0, link.indexOf("#"));
            }
            User loggedInUser = userService.getCurrentUser();
            String isSaved = userActivityService.saveSavedPhrase(UserActivityConstant.SAVED_PHRASE, loggedInUser, request, savedPhrase, getURLWithoutContextFromUrl(ctx, link));
            finalJSONObj.put("isSaved", isSaved);
            finalJSONObj.put("savedPhrase", savedPhrase);
        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return finalJSONObj;
    }

    /**
     * Search Result Post method for across all or generic search.
     *
     * @param searchForm {@link SearchResponse} search form storing values like
     * search element,searchingIn,etc.
     * @param request {@link HttpServletRequest} servlet request.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/across/all", method = RequestMethod.GET)
    public ModelAndView searchResults(
            @ModelAttribute SearchResponse searchForm,
            HttpServletRequest request
    ) {
        ModelAndView modelAndView = prepareSearchResultView(searchForm, "all", 0, 0, request);
        modelAndView.addObject("mode", "Sorted");
        return modelAndView;
    }

    /**
     * Shows result across all resource with pagination(Get Method)
     *
     * @param searchForm {@link SearchResponse} search form storing values like
     * search element,searchingIn,etc.
     * @param pageNo
     * @param dataLimit
     * @param request {@link HttpServletRequest} servlet request.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/across/all/{pageNo}/{dataLimit}", method = RequestMethod.GET)
    public ModelAndView searchResultsWithPagination(
            @ModelAttribute SearchResponse searchForm,
            @PathVariable int pageNo,
            @PathVariable int dataLimit,
            HttpServletRequest request
    ) {
        ModelAndView modelAndView = prepareSearchResultView(searchForm, "all", pageNo, dataLimit, request);
        modelAndView.addObject("mode", "nvli_rl_5");
        return modelAndView;
    }

    /**
     * Shows result across particular resource .It searches within selected sub
     * resource or within all sub resource of resource(Post Method)
     *
     * @param viewAllResourceType
     * @param searchForm {@link SearchResponse} search form storing values like
     * search element,searchingIn,etc.
     * @param request {@link HttpServletRequest} servlet request.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/across/resource/{viewAllResourceType}", method = RequestMethod.GET)
    public ModelAndView searchResultsWithResource(
            @ModelAttribute SearchResponse searchForm,
            @PathVariable String viewAllResourceType,
            HttpServletRequest request
    ) {
        return prepareSearchResultView(searchForm, viewAllResourceType, 0, 0, request);
    }

    /**
     * Shows result across particular resource .It searches within selected sub
     * resource or within all sub resource of resource(Post Method)
     *
     * @param viewAllResourceType
     * @param searchForm {@link SearchResponse} search form storing values like
     * search element,searchingIn,etc.
     * @param pageNo
     * @param dataLimit
     * @param request {@link HttpServletRequest} servlet request.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/across/resource/{viewAllResourceType}/{pageNo}/{dataLimit}", method = RequestMethod.GET)
    public ModelAndView searchResultsWithResourceAndPagination(
            @ModelAttribute SearchResponse searchForm,
            @PathVariable String viewAllResourceType,
            @PathVariable int pageNo,
            @PathVariable int dataLimit,
            HttpServletRequest request
    ) {
        return prepareSearchResultView(searchForm, viewAllResourceType, pageNo, dataLimit, request);
    }

    @RequestMapping(value = "/fetch-resource-result/{resourceTypeCode}/{mode}/{pageNo}/{dataLimit}", method = RequestMethod.POST)
    @ResponseBody
    public SearchResponse fetchResourceResults(
            @ModelAttribute SearchResponse searchForm,
            @PathVariable String resourceTypeCode,
            @PathVariable String mode,
            @PathVariable int pageNo,
            @PathVariable int dataLimit,
            @RequestParam boolean facetRequired,
            HttpServletRequest request
    ) {
        searchForm.setSearchingIn(resourceTypeCode);
        try {
            searchForm.setFilters(Helper.removeNullAndEmptyStringFromMap(searchForm.getFilters()));
            searchForm.setSpecificFilters(Helper.removeNullAndEmptyStringFromMap(searchForm.getSpecificFilters()));
            searchForm.setRankingRule(mode);
            SearchResponse results = basicSearchWebserviceHelper.fetchResults(searchForm, pageNo, dataLimit, facetRequired, LocaleContextHolder.getLocale().getLanguage());
            return results;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/fetch-web-discovery/{resourceTypeCode}/{pageNo}/{dataLimit}", method = RequestMethod.POST)
    @ResponseBody
    public SearchResponse fetchWebDiscoveryResults(
            @ModelAttribute SearchResponse searchResponse,
            @PathVariable String resourceTypeCode,
            @PathVariable int pageNo,
            @PathVariable int dataLimit,
            @RequestParam boolean facetRequired,
            HttpServletRequest request
    ) {
        try {
            return basicSearchWebserviceHelper.fetchWebDiscoveryResults(searchResponse, resourceTypeCode, pageNo, dataLimit, facetRequired, LocaleContextHolder.getLocale().getLanguage());
        } catch (Exception ex) {
            LOGGER.error("Error in Web Discovery service calling : " + ex);
            ex.printStackTrace();
        }
        return null;
    }

    private ModelAndView prepareSearchResultView(SearchResponse searchForm, String viewAllResourceType, int pageNo, int dataLimit, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            searchForm.setSearchingIn(viewAllResourceType);
            //log user activity
            Map<String, ResourceType> mapOfResourceType = (Map<String, ResourceType>) request.getSession().getServletContext().getAttribute("resourceTypes");
            ObjectMapper mapperObj = new ObjectMapper();
            modelAndView.addObject("jsonOfResourceType", mapperObj.writeValueAsString(mapOfResourceType));
        } catch (HttpClientErrorException ex) {
            LOGGER.error(ex.getMessage(), ex);
            ex.printStackTrace();
            modelAndView.addObject("errorMsg", "Currently service is down");
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        modelAndView.addObject("isWebscaleDiscovery", request.getParameter("isWebscaleDiscovery"));
        modelAndView.addObject("isCrossLingualSearch", request.getParameter("isCrossLingualSearch"));
        modelAndView.addObject("languageKeyValueJson", request.getParameter("languageKeyValueJson"));
        modelAndView.addObject("mode", request.getParameter("mode"));
        modelAndView.addObject("viewAllResourceType", viewAllResourceType);
        modelAndView.addObject("pageNo", pageNo);
        modelAndView.addObject("dataLimit", dataLimit);
        modelAndView.addObject("searchForm", searchForm);
        modelAndView.addObject("rTIconLocation", resourceTypeIconReadLocation);
        modelAndView.addObject("rTIconDefLocation", resourceTypeIconDefalutReadLocation);
        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
        if (!checkIsItWebScaleDiscovery(viewAllResourceType)) {
            modelAndView.addObject("resourceLocalisationMap", Helper.resourceTypeInfoByLangAndCode(viewAllResourceType, request.getParameter("language"), request).toJSONString());
        } else {
            modelAndView.addObject("resourceLocalisationMap", Helper.resourceTypeInfoByLangAndCode("all", request.getParameter("language"), request).toJSONString());
        }
        modelAndView.setViewName("search-result");
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping(value = "/log-search-activity", method = RequestMethod.POST)
    public void logSearchActivity(@RequestParam String searchTerm, @RequestParam String url, HttpServletRequest request) {
        User loggedInUser = userService.getCurrentUser();
        if (loggedInUser != null) {
            userActivityService.saveSearchedPhrase(UserActivityConstant.KEYWORD_SEARCHED, loggedInUser, request, url, searchTerm.trim(), request.getSession());
        } else {
            userActivityService.saveSearchedPhrase(UserActivityConstant.KEYWORD_SEARCHED, null, request, url, searchTerm.trim(), request.getSession());
        }
    }

    /**
     * Get Cross Lingual Map through ajax call.
     *
     * @param languageId current language id.
     * @param searchElement user searched keyword.
     * @param request {@link HttpServletRequest}
     * @return languageMap map of languages.
     */
    @RequestMapping(value = "crosslingualMap")
    public @ResponseBody
    Map<String, String> getCrossLingualMap(@RequestParam("languageId") String languageId, @RequestParam("searchElement") String searchElement, HttpServletRequest request) {
        try {
            /**
             * Call language tab related web service.
             */
            ResponseEntity<Map> languageMapResponseEntity = null;
            Map<String, String> languageMap = null;
            String urlForLangaugeTab;
            urlForLangaugeTab = mitWSBaseURL + "/search/crosslingual/" + languageId + "/" + searchElement;
            LOGGER.info("URL urlForLangaugeTab::" + urlForLangaugeTab);
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            languageMapResponseEntity = restTemplate.exchange(urlForLangaugeTab, HttpMethod.GET, httpEntity, Map.class);
            if (languageMapResponseEntity != null) {
                languageMap = languageMapResponseEntity.getBody();
            }
//            LOGGER.info("languageMap size ::" + languageMap.size()); //done intentially to make sure data is available. if  you are removing logger ...  make sure you check size on languageMap
//            modelAndView.addObject("languageMap", languageMap);
            return languageMap;
        } catch (Exception exception) {
            LOGGER.error("Exception in getCrossLingualMap()::" + exception.getMessage());
            throw exception; //exception thrown intentianally ..do not remove without asking hemant or you know what you are doing.
        }

    }

    /**
     * Get Cross Lingual Map Single element through ajax call.
     *
     * @param srcLang source language.
     * @param destLang destination language.
     * @param searchElement user searched keyword.
     * @param request {@link HttpServletRequest}
     * @return languageMap map of languages.
     */
    @RequestMapping(value = "crosslingualMapSingle")
    public @ResponseBody
    Map<String, String> getCrossLingualMapSingle(@RequestParam("srcLang") String srcLang, @RequestParam("destLang") String destLang, @RequestParam("searchElement") String searchElement, HttpServletRequest request) throws Exception {
        Map<String, String> languageMapSingle = null;
        try {
            ResponseEntity<Map> languageMapResponseEntity = null;

            String urlForLangaugeTab;
            urlForLangaugeTab = mitWSBaseURL + "/search/crosslingual/" + srcLang + "/" + destLang + "/" + searchElement;
            LOGGER.info("URL urlForLangaugeTab::" + urlForLangaugeTab);
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            languageMapResponseEntity = restTemplate.exchange(urlForLangaugeTab, HttpMethod.GET, httpEntity, Map.class);
            languageMapSingle = languageMapResponseEntity.getBody();
            LOGGER.info("languageMap size ::" + languageMapSingle.size());
        } catch (Exception exception) {
            LOGGER.error("Exception in getCrossLingualMapSingle()::" + exception.getMessage());
            throw exception; //exception thrown intentianally ..do not remove without asking hemant or you know what you are doing.
        }
        return languageMapSingle;
    }

    /**
     * Lists resources by group type and resource type
     *
     * @param searchForm {@link SearchResponse} search form storing values like
     * search element,searchingIn,etc.
     * @param bindingResult {@link BindingResult} to check any error in search
     * form.
     * @param resourceType Resource type name.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/show/{resourceType}", method = RequestMethod.GET)
    public ModelAndView getGroupByResourceType(@ModelAttribute SearchResponse searchForm, BindingResult bindingResult,
            @PathVariable("resourceType") String resourceType
    ) {
        modelAndView = new ModelAndView();
        try {
            ResourceType resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", resourceType).get(0);
            List<GroupType> groupTypes = groupTypeDAO.getGroupByResourceType(resourceTypeObj);
            List listOfResource;
            Map<GroupType, List<Resource>> groupWiseResource = new HashMap<>();
            for (GroupType groupType : groupTypes) {
                listOfResource = resourceDAO.getResourceByGroupAndResourceType(groupType, resourceTypeObj);
                if (listOfResource != null && !listOfResource.isEmpty()) {
                    groupWiseResource.put(groupType, listOfResource);
                }
            }
            if (searchForm == null) {
                searchForm = new SearchResponse();
            }
            searchForm.setSearchingIn(resourceType);
            searchForm.setSearchComplexity(SearchComplexityEnum.BASIC);
            modelAndView.addObject("groupList", groupTypes);
            modelAndView.addObject("mappingResourceType", resourceTypeObj);
            modelAndView.addObject("searchForm", searchForm);
            modelAndView.addObject("groupWiseResource", groupWiseResource);
            User loggedInUser = userService.getCurrentUser();
            if (loggedInUser != null) {
                modelAndView.addObject("user", loggedInUser);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        modelAndView.setViewName("category");
        modelAndView.addObject("rTIconLocation", resourceTypeIconReadLocation);
        modelAndView.addObject("groupIconReadLocation", groupIconReadLocation);
        return modelAndView;
    }

    /**
     * Get Application URL without context.
     *
     * @param ctx {@link ApplicationContext} application context object used to
     * get context.
     * @param request {@link HttpServletRequest}
     * @return finalUrl {@link String} final URL without context.
     */
    public String getURLWithoutContext(ApplicationContext ctx, HttpServletRequest request) {
        String finalUrl = "";
        try {
            String appCtx = ctx.getApplicationName();
            StringBuffer requestURL = request.getRequestURL();
            String queryString = request.getQueryString();
            String[] splitString = requestURL.toString().split(appCtx);
            if (queryString == null) {
                finalUrl = splitString[1];
            } else {
                finalUrl = splitString[1] + "?" + queryString;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            finalUrl = "";
        }
        return finalUrl;
    }

    /**
     * Get Application URL without context from URL.
     *
     * @param ctx {@link ApplicationContext} application context object used to
     * get context.
     * @param link input link
     * @return finalUrl {@link String} final URL without context.
     */
    public String getURLWithoutContextFromUrl(ApplicationContext ctx, String link) {
        String finalUrl = "";
        try {
            String appCtx = ctx.getApplicationName();
            StringBuffer requestURL = new StringBuffer(link);
            String[] splitString = requestURL.toString().split(appCtx);
            finalUrl = splitString[1];
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            finalUrl = "";
        }

        if (null != finalUrl && finalUrl.length() > 0) {
            //to remove first "/"
            finalUrl = finalUrl.substring(1);
            //to remove string after question mark when saved from list of user-saved-searches.jsp

        }
        return finalUrl;
    }

    @RequestMapping(value = "/previewForTimeline/{recordIdentifier:.*}", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public JSONObject searchTimelinePreviewPost(@PathVariable("recordIdentifier") String recordIdentifier, HttpServletRequest request) {
        JSONObject jSONObject = new JSONObject();
        restTemplate = new RestTemplate();
        ResourceType resourceTypeObj;
        ResponseEntity<Record> record;
        String temp[] = recordIdentifier.split("_");
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            record = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            /**
             * Call Record {View} Web Service
             */
            callToRecordViewWebService(recordIdentifier, httpEntity);
            qrCodeUrl = getURL(request);
            jSONObject.put("baseLocation", baseLocation);
            jSONObject.put("baseLocationTif", baseLocationTif);
            resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
            jSONObject.put("resourceType", resourceTypeObj.getResourceType());
            if (record.getBody() != null) {
                jSONObject.put("fileName", record.getBody().getOriginalFileName());
            }
            if (record.getBody() != null) {
                jSONObject.put("recordId", record.getBody().getRecordIdentifier());
            }
            if (record.getBody().getPaths() != null) {
                jSONObject.put("path", record.getBody().getPaths().get(0));
            }

            if (record.getBody().getJsonView() != null && !record.getBody().getJsonView().isEmpty()) {
                Gson gson = new Gson();
                CollectionType collectionType = gson.fromJson(record.getBody().getJsonView(), in.gov.nvli.domain.marc21.CollectionType.class);
                jSONObject.put("marc21Record", collectionType.getRecord().get(0));
            }
        } catch (RestClientException | JsonSyntaxException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return jSONObject;
    }

    /**
     * Result preview method.
     *
     * @param recordIdentifier unique id of record.
     * @param request {@link HttpServletRequest}
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/preview/{recordIdentifier:.*}", method = {RequestMethod.POST, RequestMethod.GET})
    public synchronized ModelAndView searchResultPreviewPost(@PathVariable("recordIdentifier") String recordIdentifier, HttpServletRequest request) {
        modelAndView = new ModelAndView();
        restTemplate = new RestTemplate();
        ResourceType resourceTypeObj = null;
        int userRecRate = -1;//-1 means not yet rated
        ResponseEntity<Record> record = null;
        String temp[] = recordIdentifier.split("_");
        if (!(recordIdentifier.equals("undefined"))) {
            try {
                HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
                String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;

                record = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
                /**
                 * Call Record {View} Web Service
                 */
                callToRecordViewWebService(recordIdentifier, httpEntity);

                qrCodeUrl = getURL(request);
                modelAndView.addObject("baseLocation", baseLocation);
                modelAndView.addObject("baseLocationTif", baseLocationTif);
                resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
                modelAndView.addObject("resourceType", resourceTypeObj);
                modelAndView.addObject("record", record.getBody());
                if (record.getBody().getJsonView() != null && !record.getBody().getJsonView().isEmpty()) {
                    Gson gson = new Gson();
                    CollectionType collectionType = gson.fromJson(record.getBody().getJsonView(), in.gov.nvli.domain.marc21.CollectionType.class);
                    modelAndView.addObject("marc21Record", collectionType.getRecord().get(0));
                }
            } catch (Exception e) {
                e.printStackTrace();
                LOGGER.error(e.getMessage(), e);
            } finally {
                try {
                    User userObj = userService.getCurrentUser();
                    if (userObj != null) {
                        String userSearchedPhraseIdInSession = (String) request.getSession().getAttribute(userObj.getId() + "_" + Constants.USER_SEARCHED_PHRASE_ID);
                        userActivityService.saveUserVisitedLinks(UserActivityConstant.LINK_VISITED, recordIdentifier, record.getBody().getNameToView(), userObj, request, userSearchedPhraseIdInSession);
                        ReviewAndRating reviewAndRating = reviewAndRatingService.getUserReviewByUserIdAndRecordIdentifier(userObj.getId(), recordIdentifier);
                        if (reviewAndRating != null && reviewAndRating.getRecordRating() != null) {
                            userRecRate = reviewAndRating.getRecordRating();
                        }
                        if (reviewAndRating != null && reviewAndRating.getIsLiked() != null) {
                            modelAndView.addObject("userLikeFlag", reviewAndRating.getIsLiked());
                        } else {
                            modelAndView.addObject("userLikeFlag", false);
                        }
                        modelAndView.addObject("userRatingFlag", userRecRate);
                        modelAndView.addObject("myReview", reviewAndRating);
                    }
                    modelAndView.addObject("user", userObj);

                    //check record identifier not null / empty and resource type is BLOG.
                    if (recordIdentifier != null && !recordIdentifier.isEmpty() && resourceTypeObj.getResourceTypeCode().equalsIgnoreCase(Constants.RESOURCE_TYPE_BLOG_CODE)) {
                        CrowdSourceArticle crowdSourceArticle = crowdSourcingService.getArticle(Long.parseLong(temp[temp.length - 1]));//get article by splitting recordIndentifier.
                        List<CrowdSourceArticle> crowdSourceArticleList = new ArrayList<>();
                        List<Long> articleIds = new ArrayList<>();
                        if (!crowdSourceArticle.getRelatedArticles().isEmpty()) {
                            for (String id : crowdSourceArticle.getRelatedArticles().split("&\\|&")) {
                                articleIds.add(Long.parseLong(id));
                            }
                            crowdSourceArticleList = crowdSourcingService.getArticleList(articleIds);//related articles list
                        }
                        String resourceTypeCode, articleIdentifier;
                        Map<Long, String> articleIdentifierMap = new HashMap<>();
                        for (CrowdSourceArticle article : crowdSourceArticleList) {
                            resourceTypeCode = article.getArticleCategory().getResourceType().getResourceTypeCode();
                            articleIdentifier = resourceTypeCode + "_" + article.getArticleCategory().getResourceCode() + "_" + article.getId();//create article identifier
                            articleIdentifierMap.put(article.getId(), articleIdentifier); //Identifier Map having key-articleId,value-articleIdentifier
                        }
                        modelAndView.addObject("crowdSourceAttachedArticleIdentifierMap", articleIdentifierMap);
                        modelAndView.addObject("crowdSourceArticle", crowdSourceArticle);
                        modelAndView.addObject("crowdSourceAttachedArticleList", crowdSourceArticleList);
                    }
                    modelAndView.addObject("languageList", crowdSourcingService.getIndianLanguageList());
                } catch (Exception e) {
//                e.printStackTrace();
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
        modelAndView.setViewName("result-preview");
        return modelAndView;
    }

    /**
     * Render tiff as image wise.
     *
     * @param response {@link HttpServletResponse}
     * @param recordIdentifier identifier of record
     */
    @RequestMapping(value = "/preview/renderTiff/{recordIdentifier:.*}")
    public void render_tiff(HttpServletResponse response, @PathVariable String recordIdentifier) {
        HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
        String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
        ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
        Record record = recordResponseEntity.getBody();
        String fileName = record.getOriginalFileName();
        String fileLocation = record.getLocation().replace("\\", "/");
        File tiffFile = null;
        SeekableStream s = null;
        try {
            tiffFile = findFile(new File(baseLocationTif + fileLocation), fileName);//new File("/DLI/DLI_CMU/DLI_batch_02/book-10/00000001.tif");//"/home/priya/Downloads/tif/00000018.tif"
            if (tiffFile != null) {
                ImageLayout imageLayout = new ImageLayout(500, 500, 600, 800);
                s = new FileSeekableStream(tiffFile);
                ImageDecoder decoder = ImageCodec.createImageDecoder("TIFF", s, null);
                PlanarImage op = new NullOpImage(decoder.decodeAsRenderedImage(0), null, OpImage.OP_IO_BOUND, imageLayout);
                // Clear output buffer
                response.reset();

                // Set content.
                response.setContentType("image/png");
                response.setHeader("cache-control", "no-cache,no-store,must-revalidate");

                // Send image.
                ImageIO.write(op, "png", response.getOutputStream());
                response.flushBuffer();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * findFile() returns a {@link File} using directory name and filename. File
     * is searched recursively in all sub-directories of specified directory. If
     * the matching file is found, it is returned.
     *
     * @param dir Directory under which file is to be searched
     * @param fileName Name of the file to be searched
     * @return result file object {@link File}
     */
    public static File findFile(File dir, String fileName) {
        File result = null; // no need to store result as String, you're returning File anyway
        File[] dirlist = dir.listFiles();

        for (int i = 0; i < dirlist.length; i++) {
            if (dirlist[i].isDirectory()) {
                result = findFile(dirlist[i], fileName);
                if (result != null) {
                    break; // recursive call found the file; terminate the loop
                }
            } else if (dirlist[i].getName().matches(fileName)) {
                return dirlist[i]; // found the file; return it
            }
        }
        return result; // will return null if we didn't find anything
    }

    /**
     * Shows Sub resource Information
     *
     * @param pathVariableMap {@link Map} map of path variables.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/{resourcetype}/show/{subresource}", method = RequestMethod.GET)
    public ModelAndView showSubResourceInformation(@PathVariable Map pathVariableMap) {
        modelAndView = new ModelAndView();
        String resourcetype = (String) pathVariableMap.get("resourcetype");
        String subresource = (String) pathVariableMap.get("subresource");
        ResourceType resourceTypeObj = null;
        Resource resourceInfo = null;
        try {
            resourceTypeObj = resourcetype != null ? resourceTypeDAO.findbyQuery("findByResourceType", resourcetype).get(0) : null;
            resourceInfo = subresource != null ? resourceDAO.findbyQuery("findByResourceCode", subresource).get(0) : null;
            switch (resourceInfo.getResourceType().getResourceTypeCategory().trim()) {
                case Constants.RESOURCE_TYPE_MIT:
                    MetadataIntegrator mit = null;
                    if (resourceInfo != null) {
                        if (resourceInfo instanceof MetadataIntegrator) {
                            mit = (MetadataIntegrator) resourceInfo;
                        }
                    }
                    modelAndView.addObject("resourceInfo", mit);
                    break;
                case Constants.RESOURCE_TYPE_HARVEST:
                    OpenRepository openRepository = null;
                    if (resourceInfo != null) {
                        if (resourceInfo instanceof OpenRepository) {
                            openRepository = (OpenRepository) resourceInfo;
                        }
                    }
                    modelAndView.addObject("resourceInfo", openRepository);
                    break;
                case Constants.RESOURCE_TYPE_ENEWS:
                    ENews eNews = null;
                    if (resourceInfo != null) {
                        if (resourceInfo instanceof ENews) {
                            eNews = (ENews) resourceInfo;
                        }
                    }
                    modelAndView.addObject("resourceInfo", eNews);
                    break;
                case Constants.RESOURCE_TYPE_WEB_CRAWLER:
                    WebCrawler webCrawler = null;
                    if (resourceInfo != null) {
                        if (resourceInfo instanceof WebCrawler) {
                            webCrawler = (WebCrawler) resourceInfo;
                        }
                    }
                    modelAndView.addObject("resourceInfo", webCrawler);
                    break;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);

        }
        if (resourceInfo != null && resourceInfo.getOrganization() != null && resourceInfo.getOrganization().getAddress() != null) {
            resourceInfo.getOrganization().setAddress(resourceInfo.getOrganization().getAddress().replaceAll("\r", "").replaceAll("\n", ""));
        }
        modelAndView.addObject("resourceType", resourcetype);
        modelAndView.addObject("resourceTypeObj", resourceTypeObj);
        modelAndView.addObject("path", resourceInfo.getResourceIcon());
        User loggedInUser = userService.getCurrentUser();
        if (loggedInUser != null) {
            modelAndView.addObject("user", loggedInUser);
        }
        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
        modelAndView.setViewName("subresource-info");
        return modelAndView;
    }

    /**
     * Used to render resource icon in sub resource
     *
     * @param resourceIconName resource icon name.
     * @param response {@link HttpServletResponse}
     */
    @RequestMapping(value = "/get/{resourceIconName}/resourceicon")
    public void getSubresourceIcon(@PathVariable String resourceIconName, HttpServletResponse response) {
        resourceIconName = resourceIconName.trim();
        if (resourceIconName.indexOf("png") > 0) {
            response.setContentType("image/png");
        } else if (resourceIconName.indexOf("gif") > 0) {
            response.setContentType("image/gif");
        } else if (resourceIconName.indexOf("jpg") > 0 || resourceIconName.indexOf("jpe") > 0 || resourceIconName.indexOf("jpeg") > 0) {
            response.setContentType("image/jpeg");
        }
        File imagefilePath = new File(imageLocation + File.separator + resourceIconName);
        if (!imagefilePath.exists()) {
            imagefilePath = new File(servletContext.getRealPath(File.separator) + "WEB-INF"
                    + File.separator + "static" + File.separator + "themes"
                    + File.separator + "images" + File.separator + "thumb" + File.separator + "no_image_thumb.jpg");
        }
        try (FileInputStream fin = new FileInputStream(imagefilePath);
                OutputStream outputStream = response.getOutputStream()) {
            byte[] buffer = new byte[fin.available()];
            fin.read(buffer);
            outputStream.write(buffer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Result preview of all tiff files.
     *
     * @param recordIdentifier unique id of record.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/preview/all/tif/{recordIdentifier:.*}", method = RequestMethod.GET)
    public ModelAndView serachResultPreviewAllTif(@PathVariable String recordIdentifier) {
        modelAndView = new ModelAndView();
        ResourceType resourceTypeObj = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();
            modelAndView.addObject("record", record);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        User loggedInUser = userService.getCurrentUser();
        if (loggedInUser != null) {
            modelAndView.addObject("user", loggedInUser);
        }
        String temp[] = recordIdentifier.split("_");
        resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
        modelAndView.addObject("resourceType", resourceTypeObj);
        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
        modelAndView.setViewName("result-preview-all-tif");
        return modelAndView;
    }

    /**
     * Get tiff image count.
     *
     * @param recordIdentifier unique id of record.
     * @return tiff image count
     */
    @RequestMapping(value = "/preview/all/getTiffImagesCount/{recordIdentifier:.*}", method = RequestMethod.POST)
    public @ResponseBody
    int get_tiff_images_count(@PathVariable String recordIdentifier) {
        int numPages = 0;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();
            numPages = record.getPaths().size();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return numPages;
    }

    /**
     * Render tiff images.
     *
     * @param response {@link HttpServletResponse}
     * @param recordIdentifier unique id of record.
     * @param no number of pages.
     */
    @RequestMapping(value = "/preview/all/renderTiff/{recordIdentifier:.*}/{no}")
    public void render_tiff(HttpServletResponse response, @PathVariable String recordIdentifier, @PathVariable int no) {
        if (no >= 1) {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();
            String fileLocation = null;

            String filePath = record.getPaths().get(no - 1);
            String[] filePathArray = filePath.split("images");
            fileLocation = filePathArray[1];
            File tiffFile = new File(baseLocationTif + fileLocation);
            if (fileLocation != null && baseLocationTif != null) {
                SeekableStream s = null;
                try {
                    ImageLayout imageLayout = new ImageLayout(500, 500, 600, 800);
                    s = new FileSeekableStream(tiffFile.getAbsolutePath());
                    ImageDecoder decoder = ImageCodec.createImageDecoder("TIFF", s, null);
//                if (no <= decoder.getNumPages()) {
                    //PlanarImage op = new NullOpImage(decoder.decodeAsRenderedImage(0), null, OpImage.OP_IO_BOUND, null);
                    PlanarImage op = new NullOpImage(decoder.decodeAsRenderedImage(0), null, OpImage.OP_IO_BOUND, imageLayout);
                    // Clear output buffer
                    response.reset();

                    // Set content.
                    response.setContentType("image/png");
                    response.setHeader("cache-control", "no-cache,no-store,must-revalidate");

                    // Send image.
                    ImageIO.write(op, "png", response.getOutputStream());
                    response.flushBuffer();
//                } else {
//                    LOGGER.warn("Page number greater than available page number requested for tiffFile: " + tiffFile.getName());
//                }
                } catch (Exception ex) {
                    LOGGER.error("Error while rendering tiff file: " + tiffFile.getName() + " :" + ex);
                } finally {
                    try {
                        if (s != null) {
                            s.close();
                        }
                    } catch (Exception ex) {
                        LOGGER.error("Error while closing SeekableStream: " + ex);
                    }
                }
            } else {
                LOGGER.warn("No tiff file");
            }
        } else {
            LOGGER.warn("Page number less than 1 requested for tiffFile: ");
        }
    }

    /**
     * Result preview for all jpeg.
     *
     * @param recordIdentifier unique id of record.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/preview/all/jpeg/{recordIdentifier:.*}", method = RequestMethod.GET)
    public ModelAndView serachResultPreviewAllJpeg(@PathVariable String recordIdentifier, @RequestParam(required = false, value = "threeDimensional") boolean threeDimensional,@RequestParam(required = false, value = "fullTextFlag") boolean fullTextFlag, @RequestParam(required = false, value = "searchElement") String searchElement) {
        modelAndView = new ModelAndView();
        ResourceType resourceTypeObj = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/books/paths/" + recordIdentifier + "?threeDImages=" + threeDimensional;
            ResponseEntity<BookViewer> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, BookViewer.class);
            BookViewer bookViewer = recordResponseEntity.getBody();
            Gson gson = new Gson();
            Path path = new Path();
            path.getPath().addAll(bookViewer.getImagesList());
            String pathJson = gson.toJson(path);
            String books = gson.toJson(bookViewer);
            modelAndView.addObject("pathJson", pathJson);
            modelAndView.addObject("bookViewer", books);
            modelAndView.addObject("recordIdentifier", recordIdentifier);
            modelAndView.addObject("fullTextFlag", fullTextFlag);
            modelAndView.addObject("searchElement", searchElement);
            String temp[] = recordIdentifier.split("_");
            resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
            modelAndView.addObject("resourceType", resourceTypeObj);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        modelAndView.setViewName("result-preview-all-jpeg");
        return modelAndView;
    }

    /**
     * Result preview for mp4,mp3,pdf files.
     *
     * @param recordIdentifier unique id of record.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/preview/all/mp3mp4/{recordIdentifier:.*}", method = RequestMethod.GET)
    public ModelAndView serachResultPreviewAllMp3Mp4(@PathVariable String recordIdentifier) {
        modelAndView = new ModelAndView();
        ResourceType resourceTypeObj = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();
            modelAndView.addObject("record", record);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        String temp[] = recordIdentifier.split("_");
        resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
        modelAndView.addObject("resourceType", resourceTypeObj);
        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
        modelAndView.setViewName("result-preview-all-mp3mp4");
        return modelAndView;
    }

    /**
     * Get QR code image of result preview url.
     *
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     */
    @RequestMapping(value = "/preview/get/qrcode", method = RequestMethod.GET)
    public void getQRCode(HttpServletRequest request, HttpServletResponse response) {
        if (qrCodeUrl != null) {
            int size = 250;
            String fileType = "png";
            response.setContentType("image/png");
            try {
                Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
                hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

                hintMap.put(EncodeHintType.MARGIN, 1);
                /*
                 * default = 4
                 */
                hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

                QRCodeWriter qrCodeWriter = new QRCodeWriter();
                BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeUrl, BarcodeFormat.QR_CODE, size, size, hintMap);
                int width = byteMatrix.getWidth();
                BufferedImage image = new BufferedImage(width, width, BufferedImage.TYPE_INT_RGB);
                image.createGraphics();

                Graphics2D graphics = (Graphics2D) image.getGraphics();
                graphics.setColor(Color.WHITE);
                graphics.fillRect(0, 0, width, width);
                graphics.setColor(Color.BLACK);

                for (int i = 0; i < width; i++) {
                    for (int j = 0; j < width; j++) {
                        if (byteMatrix.get(i, j)) {
                            graphics.fillRect(i, j, 1, 1);
                        }
                    }
                }
                ImageIO.write(image, fileType, response.getOutputStream());
                response.flushBuffer();
            } catch (WriterException e) {
                LOGGER.error(e.getMessage(), e);
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Get result preview requested url.
     *
     * @param request {@link HttpServletRequest}
     * @return URL
     */
    public synchronized String getURL(HttpServletRequest request) {
        try {
            if (request.getQueryString() != null) {
                return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getRequestURI() + "?" + request.getQueryString();
            } else {
                return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getRequestURI();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Create citation for different format.
     *
     * @param recordIdentifier unique id of record.
     * @return citationMap {@link Map}
     */
    @RequestMapping(value = "/preview/citation/{recordIdentifier:.*}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> createCitation(@PathVariable("recordIdentifier") String recordIdentifier) {
        Map<String, String> citationMap = null;
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();

            citationMap = new LinkedHashMap<>();
            citationMap.put("IEEE", getCitation(record, "ieee"));//Institute of Electrical and Electronics Engineers
            citationMap.put("ACM", getCitation(record, "acm-siggraph"));//Association for Computing Machinery
            citationMap.put("APA", getCitation(record, "apa"));//American Psychological Association
            citationMap.put("Chicago", getCitation(record, "chicago-author-date"));
            citationMap.put("CSE", getCitation(record, "council-of-science-editors"));//Council of Science Editors
            citationMap.put("MLA", getCitation(record, "modern-language-association"));//Modern Language Association

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return citationMap;
    }

    /**
     * Get citation.
     *
     * @param record {@link Record}
     * @param format citation format.
     * @return citation string.
     */
    public String getCitation(Record record, String format) {
        String citation = null;
        try {
            String publisher = "";
            String creator = "";
            if (record.getPublisher() != null) {
                for (String p : record.getPublisher()) {
                    publisher += p + " ";
                }
            }
            if (record.getCreator() != null) {
                for (String c : record.getCreator()) {
                    creator += c.replace(",", "");
                }
            }
            CSLItemData item = new CSLItemDataBuilder().type(CSLType.REPORT).title(record.getTitle() == null ? null : (record.getTitle().get(0) == null ? null : record.getTitle().get(0))).author(creator, null).issued(record.getDate() == null ? null : (new CSLDate(null, null, null, null, record.getDate().get(0) == null ? null : record.getDate().get(0)))).URL(null).accessed(null).publisher(publisher).build();
            citation = CSL.makeAdhocBibliography(format, item).makeString();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return citation;
    }

    /**
     * To download record.
     *
     * @param request {@link HttpServletRequest}
     * @param what what download i.e. content, metadata, content + metadata
     * @param recordIdentifier unique id of record.
     * @param response {@link HttpServletResponse}
     */
    @RequestMapping(value = "/preview/download/{what}/{recordIdentifier:.*}", method = RequestMethod.GET)
    public void download(HttpServletRequest request, @PathVariable("what") String what, @PathVariable("recordIdentifier") String recordIdentifier, HttpServletResponse response) {
        try {
            switch (what) {
                case "content":
                    break;
                case "metadata":
                    break;
                case "content_metadata":
                    downloadContentAndMetadata(recordIdentifier, response, request);
                    break;

            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Download Content and metadata.
     *
     * @param recordIdentifier unique id of record.
     * @param response {@link HttpServletResponse}
     * @param request {@link HttpServletRequest}
     */
    public void downloadContentAndMetadata(String recordIdentifier, HttpServletResponse response, HttpServletRequest request) {
        ZipOutputStream zos = null;
        ServletOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            ResponseEntity<Record> recordResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            Record record = recordResponseEntity.getBody();
            if (record != null) {
                File file1 = new File(record.getPaths().get(0));

                URL url1 = new URL(record.getPaths().get(0));
                HttpURLConnection httpConn = (HttpURLConnection) url1.openConnection();

                URL url2 = new URL(baseLocation + record.getLocation().replace("\\", "/"));
                HttpURLConnection httpConn2 = (HttpURLConnection) url2.openConnection();
                InputStream inputStream2 = httpConn2.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream2));
                String line = null;
                LOGGER.info("--- START ---");
                while ((line = reader.readLine()) != null) {
                    LOGGER.info(line);
                }
                LOGGER.info("--- END ---");
                inputStream2.close();

                int responseCode = httpConn.getResponseCode();

                // always check HTTP response code first
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String fileName = "";
                    String disposition = httpConn.getHeaderField("Content-Disposition");
                    String contentType = httpConn.getContentType();
                    int contentLength = httpConn.getContentLength();

                    if (disposition != null) {
                        // extracts file name from header field
                        int index = disposition.indexOf("filename=");
                        if (index > 0) {
                            fileName = disposition.substring(index + 10,
                                    disposition.length() - 1);
                        }
                    } else {
                        // extracts file name from URL
                        fileName = file1.getPath().substring(file1.getPath().lastIndexOf("/") + 1,
                                file1.getPath().length());
                    }

                    LOGGER.info("Content-Type = " + contentType);
                    LOGGER.info("Content-Disposition = " + disposition);
                    LOGGER.info("Content-Length = " + contentLength);
                    LOGGER.info("fileName = " + fileName);

                    // opens input stream from the HTTP connection
                    inputStream = httpConn.getInputStream();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + record.getOriginalFileName() + dateFormat.format(new Date()) + ".zip\"");

                    // opens an output stream to get file
                    outputStream = response.getOutputStream();
                    zos = new ZipOutputStream(outputStream);
                    ZipEntry ze = new ZipEntry(record.getOriginalFileName() + dateFormat.format(new Date()));
                    zos.putNextEntry(ze);
                    ze = new ZipEntry(file1.getName());
                    zos.putNextEntry(ze);

                    int bytesRead = -1;
                    byte[] buffer = new byte[4096];
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        zos.write(buffer, 0, bytesRead);
                    }
                    response.flushBuffer();
                    zos.closeEntry();
                    //remember close it
                    zos.close();
                    outputStream.close();
                    inputStream.close();

                    LOGGER.info("File downloaded");
                } else {
                    LOGGER.info("No file to download. Server replied HTTP code: " + responseCode);
                }
                httpConn.disconnect();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            IOUtils.closeSilently(zos);
            IOUtils.closeSilently(outputStream);
            IOUtils.closeSilently(inputStream);
        }
    }

    /**
     * Call to record view web service.
     *
     * @param recordIdentifier unique id of record.
     * @param httpEntity {@link HttpEntity}
     */
    public void callToRecordViewWebService(String recordIdentifier, HttpEntity httpEntity) {
        String url = mitWSBaseURL + "/record/view/" + recordIdentifier;
        Long recordViewCount = restTemplate.postForObject(url, httpEntity, Long.class);
        LOGGER.info("recordViewCount--->" + recordViewCount);
    }

    /**
     * getUDCTagBaseSearchRecords method used to get UDC tag base Record Search
     * Result.
     *
     * @param response {@link HttpServletResponse}
     * @param request udcnotation {@link UDCConcept}
     * @return Map a SearchResultResponse
     */
    @RequestMapping(value = "/across/all/udctagRecordSearch", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8", headers = "Accept=application/json")
    @ResponseBody
    public ModelAndView getUDCTagBaseSearchRecords(HttpServletRequest request, HttpServletResponse response) {
        modelAndView = new ModelAndView();
        restTemplate = new RestTemplate();
        String udcnotation = request.getParameter("udcnotation");
        String pageNum = request.getParameter("pageNum");
        String pageWin = request.getParameter("pageWin");
        String url = null;
        SearchResponse webserviceSearchForm = null;
        try {
            SearchResponse searchForm = new SearchResponse();
            searchForm.setSearchElement(udcnotation);
            searchForm.setSearchingIn("all");
            HttpEntity<String> httpEntity = new HttpEntity(searchForm, Helper.mitWebserviceAthentication());
            if (pageNum != null && pageWin != null) {
                url = mitWSBaseURL + "/search/udc/" + pageNum + "/" + pageWin;
            } else {
                url = mitWSBaseURL + "/search/udc/1/10";
            }
            webserviceSearchForm = restTemplate.postForObject(url, httpEntity, SearchResponse.class);
            if (webserviceSearchForm.getResults() != null) {
                Long resultCount = webserviceSearchForm.getResultSize();
                Map<String, Long> resourceTypeSearchResultMap = new HashMap<>();
                ResourceType resourcetype = null;
                int nonEmptyResourceCountResults = 0;
                for (SearchResultResponse srrs : webserviceSearchForm.getResults().values()) {
                    if (srrs.getResultSize() != 0) {
                        nonEmptyResourceCountResults++;
                        List<SearchResult> srList = srrs.getListOfResult();
                        for (SearchResult sr : srList) {
                            resourcetype = resourceTypeDAO.getResourceTypeByCode(sr.getResourceType());
                            resourceTypeSearchResultMap.put(resourcetype.getResourceType(), srrs.getResultSize());
                        }
                    }
                }
                if (resultCount != 0) {
                    LOGGER.info("UDC Tag Searching record result count " + resultCount);
                    int pageperrecords = (nonEmptyResourceCountResults * Integer.parseInt(pageWin));
                    int totalpages = (resultCount.intValue() / pageperrecords);
                    if (resultCount.intValue() % pageperrecords > 0) {
                        totalpages++;
                    }
                    if (resultCount < pageperrecords) {
                        totalpages = 0;
                    }
                    modelAndView.addObject("resourceTypeSearchResultMap", resourceTypeSearchResultMap);
                    modelAndView.addObject("totalpages", totalpages);
                    modelAndView.addObject("pageNum", pageNum);
                    modelAndView.addObject("pageWin", pageWin);
                } else {
                    LOGGER.info("UDC Tag Record Searching Result is " + resultCount);
                }
            } else {
                LOGGER.info("No result found for UDC notaion Resource wise search ");
            }
        } catch (RestClientException | NumberFormatException e) {
            LOGGER.error("Error in UDC Tag Searching Result : " + e);
        }
        Map<String, ResourceType> mapOfResourceType = (Map<String, ResourceType>) servletContext.getAttribute("resourceTypes");
        modelAndView.addObject("mapOfResourceType", mapOfResourceType);
        modelAndView.addObject("webserviceSearchForm", webserviceSearchForm);
        modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
        modelAndView.setViewName("udc-search-result");
        return modelAndView;
    }

    /**
     * udcTagsBaseSearchRecord method used to display UDC tags Tree Structure
     *
     * @param defaultParentId
     * @param udcLanguageId
     * @return modelAndView Object
     */
    @RequestMapping(value = "udc-tags-searchRecord/{defaultParentId}/{udcLanguageId}", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public ModelAndView udcTags(@PathVariable String defaultParentId, @PathVariable String udcLanguageId) {
        ModelAndView modelAndView = new ModelAndView("udc-tags-result");
        try {
            List<UDCLanguage> languageList = crowdSourcingService.getIndianLanguageList();
            Map<Long, String> udcParentMap = new LinkedHashMap<>();
            String notation = "";
            TreeSet<UDCConcept> udcConceptSet = new TreeSet<>(crowdSourcingService.getUDCConcept(Long.valueOf("2450")).getUdcConceptSet());
            for (UDCConcept udcConcept : udcConceptSet) {
                udcParentMap.put(udcConcept.getId(), udcConcept.getUdcNotation());
                if (udcConcept.getId().toString().equals(defaultParentId)) {
                    notation = udcConcept.getUdcNotation();
                }
            }
            udcParentMap.put(2450L, "ALL");
            modelAndView.addObject("languageList", languageList);
            modelAndView.addObject("udcParentMap", udcParentMap);
            modelAndView.addObject("defaultParentId", defaultParentId);
            modelAndView.addObject("udcLanguageId", udcLanguageId);
            modelAndView.addObject("defaultParentValue", notation);
        } catch (Exception e) {
            LOGGER.error("Error in displaying UDC Tag tree structure ", e);
        }
        return modelAndView;
    }

    /**
     * Get UDC Classification.
     *
     * @param languageId
     * @return
     */
    @RequestMapping(value = "getUDCClassification")
    public @ResponseBody
    List<UDCConceptDescription> getUDCClassification(@RequestParam Long languageId) {
        Map<Long, List<UDCConceptDescription>> classificationMap = ((Map<Long, List<UDCConceptDescription>>) servletContext.getAttribute("udcClassificationMap"));
        List<UDCConceptDescription> list = classificationMap.get(languageId);
        return list == null ? classificationMap.get(40l) : list;
    }

    /**
     * Get UDC Tree view Data With Pagination.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "getUDCTreeviewDataWithPagination")
    public @ResponseBody
    JSONArray getUDCTreeviewDataWithPagination(HttpServletRequest request) {
        try {
            return crowdSourcingService.generateUDCTreeViewJSONArray(Long.parseLong(request.getParameter("languageId")), Long.parseLong(request.getParameter("parentId")), request.getParameter("translationUDCLanguageId"), true);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Shows suggestions for input term and returns json with query and
     * suggestions as key.
     *
     * @param searchingIn across all or specific resource
     * @param request
     * @return
     */
    @RequestMapping(value = "/suggestions/{searchingIn}", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getSuggestionForSearchelement(@PathVariable String searchingIn, HttpServletRequest request) {
        restTemplate = new RestTemplate();
        List<String> suggestions;
        String searchTerm = request.getParameter("term");
        JSONObject finalSuggestionJSON = new JSONObject();
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/search/suggestions/" + searchingIn + "/" + searchTerm;
            url = URLDecoder.decode(url, "utf-8");
            ResponseEntity<List> listOfSuggestion = restTemplate.exchange(url, HttpMethod.GET, httpEntity, List.class);
            suggestions = listOfSuggestion.getBody();
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;
            for (String suggestion : suggestions) {
                jsonObject = new JSONObject();
                jsonObject.put("data", suggestion);
                jsonObject.put("value", suggestion);
                jsonArray.add(jsonObject);
            }
            finalSuggestionJSON.put("query", searchTerm);
            finalSuggestionJSON.put("suggestions", jsonArray);

        } catch (UnsupportedEncodingException | RestClientException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return finalSuggestionJSON.toString();
    }

    /**
     * Get group icon.
     *
     * @param group_name
     * @param response
     */
    @RequestMapping(value = "/get/group/icon/{group_name}")
    public void getGroupIcon(@PathVariable String group_name, HttpServletResponse response) {
        File groupIconFilePath;
        final String groupName = group_name.trim();
        File groupIconDirLocation = new File(groupIconLocation);
        File[] fileList = groupIconDirLocation.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File file, String name) {
                String fileNameWithOutExt = FilenameUtils.removeExtension(name);
                if (fileNameWithOutExt.equalsIgnoreCase(groupName)) {
                    return true;
                } else {
                    return false;
                }
            }
        });
        if (fileList.length == 0) {
            groupIconFilePath = new File(servletContext.getRealPath(File.separator) + "WEB-INF"
                    + File.separator + "static" + File.separator + "themes"
                    + File.separator + "images" + File.separator + "head_icons" + File.separator + "unknown-category.png");
        } else {
            groupIconFilePath = fileList[0];//only one file will be there for group type
        }
        try (FileInputStream fin = new FileInputStream(groupIconFilePath);
                OutputStream outputStream = response.getOutputStream()) {
            byte[] buffer = new byte[fin.available()];
            fin.read(buffer);
            outputStream.write(buffer);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Used to populate similar terms for search term(i.e. Did you mean feature)
     *
     * @param searchingIn resource type.
     * @param searchTerm search keyword.
     * @param request
     * @return list of did you mean.
     */
    @RequestMapping(value = "/similar/{searchingIn}/{searchTerm}", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getDidYouMeanLists(@PathVariable String searchingIn, @PathVariable String searchTerm, HttpServletRequest request) {
        List<String> similarTermList = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/search/similar/" + searchingIn + "/" + searchTerm;
            ResponseEntity<List> list = restTemplate.exchange(url, HttpMethod.GET, httpEntity, List.class);
            similarTermList = list.getBody();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return similarTermList;
    }

    /**
     * View Image Tags Page.
     *
     * @param recordIdentifier unique id of record.
     * @return modelAndView {@link ModelAndView}
     */
    @RequestMapping(value = "/view/image-tags/{recordIdentifier:.*}")
    public ModelAndView viewImageTags(@PathVariable String recordIdentifier) {
        ModelAndView modelAndView = new ModelAndView("image-tags");
        restTemplate = new RestTemplate();
        ResponseEntity<Record> record = null;
        ResponseEntity<List> imageTagResponseEntity = null;
        List<ImageTag> imageTagList = null;
        ResourceType resourceTypeObj = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            String url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
            record = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
            try {
                String urlImageDetails = mitWSBaseURL + "/record/paths/" + recordIdentifier;
                ResponseEntity<List> recordResponseEntity = restTemplate.exchange(urlImageDetails, HttpMethod.GET, httpEntity, List.class);
                List<ImageDetails> listOfImageDetails = recordResponseEntity.getBody();

                modelAndView.addObject("listOfImageDetails", listOfImageDetails);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
            if (record != null) {
                try {
                    /**
                     * Replace .(dot) to $$ for image name.
                     */
                    String imageNameWith$$ = record.getBody().getOriginalFileName().replace(".", "$$");
                    String urlImageTag = mitWSBaseURL + "record/allImageTag/" + recordIdentifier + "/" + imageNameWith$$;
                    imageTagResponseEntity = restTemplate.exchange(urlImageTag, HttpMethod.GET, httpEntity, List.class);
                    imageTagList = imageTagResponseEntity.getBody();

                    modelAndView.addObject("imageTagList", imageTagList);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
            modelAndView.addObject("recordIdentifier", recordIdentifier);
            modelAndView.addObject("record", record.getBody());
            modelAndView.addObject("baseLocation", baseLocation);
            String temp[] = recordIdentifier.split("_");
            resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", temp[0]).get(0);
            modelAndView.addObject("resourceType", resourceTypeObj);
            modelAndView.addObject("rTIconLocation", resourceTypeIconReadLocation);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return modelAndView;
    }

    /**
     * Add Image Tag.
     *
     * @param imageTag image tag object.
     * @param recordIdentifier unique id of record.
     * @param imageName name of image.
     * @return
     */
    @RequestMapping(value = "/add/image-tags/{imageName}/{recordIdentifier:.*}", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public List<ImageTag> addImageTags(@RequestBody ImageTag imageTag, @PathVariable String recordIdentifier, @PathVariable String imageName) {
        List<ImageTag> imageTagList = null;
        try {
            restTemplate = new RestTemplate();
            /**
             * Replace .(dot) to $$ for image name.
             */
            String imageNameWith$$ = imageName.replace(".", "$$");
            String url = mitWSBaseURL + "record/addImageTag/" + recordIdentifier + "/" + imageNameWith$$;
            HttpEntity httpEntity = new HttpEntity(imageTag, Helper.mitWebserviceAthentication());
            imageTagList = restTemplate.postForObject(url, httpEntity, List.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return imageTagList;
    }

    /**
     * Remove Image Tag.
     *
     * @param recordIdentifier unique id of record.
     * @param imageName name of image.
     * @param tagId id of tag.
     * @return
     */
    @RequestMapping(value = "/remove/image-tags/{recordIdentifier:.*}/{imageName}/{tagId}", method = RequestMethod.POST)
    @ResponseBody
    public List<ImageTag> removeImageTags(@PathVariable String recordIdentifier, @PathVariable String imageName, @PathVariable String tagId) {
        ResponseEntity<List> imageTagResponseEntity = null;
        List<ImageTag> imageTagList = null;
        try {
            restTemplate = new RestTemplate();
            /**
             * Replace .(dot) to $$ for image name.
             */
            String imageNameWith$$ = imageName.replace(".", "$$");
            String url = mitWSBaseURL + "/record/deleteImageTag/" + recordIdentifier + "/" + imageNameWith$$ + "/" + tagId;
            HttpEntity httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            imageTagResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, List.class);
            imageTagList = imageTagResponseEntity.getBody();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return imageTagList;
    }

    /**
     * Get All Image Tag.
     *
     * @param imageName name of image.
     * @param recordIdentifier unique id of record.
     * @return
     */
    @RequestMapping(value = "/get/image-tags/{imageName}/{recordIdentifier:.*}", method = RequestMethod.POST)
    @ResponseBody
    public List<ImageTag> getAllImageTags(@PathVariable String imageName, @PathVariable String recordIdentifier) {
        ResponseEntity<List> imageTagResponseEntity = null;
        List<ImageTag> imageTagList = null;
        try {
            restTemplate = new RestTemplate();
            /**
             * Replace .(dot) to $$ for image name.
             */
            String imageNameWith$$ = imageName.replace(".", "$$");
            String url = mitWSBaseURL + "/record/allImageTag/" + recordIdentifier + "/" + imageNameWith$$;
            HttpEntity httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
            imageTagResponseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, List.class);
            imageTagList = imageTagResponseEntity.getBody();
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return imageTagList;
    }

    /**
     * Shows XML file. File is first loaded and then is streamed to
     * HttpServletResponse.
     *
     * @param response
     * @param request
     * @param path path of file.
     * @param type xml file type.
     */
    @RequestMapping(value = "/preview/showXMLFile/{path}/{type}", method = RequestMethod.GET)
    public void render_xml(HttpServletResponse response, HttpServletRequest request, @PathVariable String path, @PathVariable String type) {
        File xmlFile = null;
        if (path != null) {
            path = path.replace("$", "/");
            path = path.replace("||", ".");
            if (Constants.METS.equalsIgnoreCase(type)) {
                xmlFile = new File(baseLocationTif + path);
            } else if (Constants.ORE.equalsIgnoreCase(type)) {
                xmlFile = new File(baseLocationTif + path);
            }
            // Clear output buffer
            response.reset();
            int DEFAULT_BUFFER_SIZE = 10240;

            // Set content.
            response.setContentType("text/xml");
            response.setHeader("Content-Length", String.valueOf(xmlFile.length()));
            response.setBufferSize(DEFAULT_BUFFER_SIZE);

            // Prepare streams.
            BufferedInputStream input = null;
            BufferedOutputStream output = null;
            try {
                // Open streams.
                input = new BufferedInputStream(new FileInputStream(xmlFile), DEFAULT_BUFFER_SIZE);
                output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

                // Write file contents to response.
                byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                int length;
                while ((length = input.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage(), ex);
            } finally {
                try {
                    if (output != null) {
                        output.close();
                    }
                    if (input != null) {
                        input.close();
                    }
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
            try {
                response.flushBuffer();
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }

    /**
     * getUDCTagBaseSpecificResourceRecordSearch method used to get UDC tag base
     * specific resource Record Search Result.
     *
     * @param response
     * @param request udcnotation {@link UDCConcept}
     * @return Map a SearchResultResponse
     */
    @RequestMapping(value = "/specificresource/udctagRecordSearch", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8", headers = "Accept=application/json")
    @ResponseBody
    public ModelAndView getUDCTagBaseSpecificResourceRecordSearch(HttpServletRequest request, HttpServletResponse response) {
        modelAndView = new ModelAndView();
        restTemplate = new RestTemplate();
        String udcnotation = request.getParameter("udcnotation");
        String pageNum = request.getParameter("pageNum");
        String pageWin = request.getParameter("pageWin");
        String resource = request.getParameter("resource");
        String url = null;
        SearchResponse webserviceSearchForm = null;
        String[] resourceName = null;
        if (!resource.equalsIgnoreCase("All")) {
            resourceName = resource.split("\\(");
            ResourceType resourcetype = resourceTypeDAO.getResourceObjectFromMappingResourceType(resourceName[0]);
            try {
                SearchResponse searchForm = new SearchResponse();
                searchForm.setSearchElement(udcnotation);
                searchForm.setSearchingIn(resourcetype.getResourceTypeCode());
                HttpEntity<String> httpEntity = new HttpEntity(searchForm, Helper.mitWebserviceAthentication());
                if (pageNum != null && pageWin != null) {
                    url = mitWSBaseURL + "/search/udc/" + pageNum + "/" + pageWin;
                } else {
                    url = mitWSBaseURL + "/search/udc/1/10";
                }
                webserviceSearchForm = restTemplate.postForObject(url, httpEntity, SearchResponse.class);
                if (webserviceSearchForm.getResults() != null) {
                    Map<String, Long> resourceTypeSearchResultMap = new HashMap<>();
                    for (SearchResultResponse srs : webserviceSearchForm.getResults().values()) {
                        if (srs.getResultSize() != 0) {
                            String resourceTypeCode = srs.getListOfResult().iterator().next().getResourceType();
                            List<SearchResult> srList = srs.getListOfResult();
                            for (SearchResult sr : srList) {
                                resourcetype = resourceTypeDAO.getResourceTypeByCode(sr.getResourceType());
                                resourceTypeSearchResultMap.put(resourcetype.getResourceType(), srs.getResultSize());
                            }

                            if (resourceTypeCode != null && resourcetype.getResourceTypeCode().equalsIgnoreCase(resourceTypeCode)) {
                                int pageperrecords = Integer.parseInt(pageWin);
                                long totalpages = srs.getResultSize() / pageperrecords;
                                if (srs.getResultSize() % pageperrecords > 0) {
                                    totalpages++;
                                }
                                if (srs.getResultSize() < pageperrecords) {
                                    totalpages = 0;
                                }
                                modelAndView.addObject("resourceName", resourcetype.getResourceType());
                                modelAndView.addObject("totalpages", totalpages);
                                modelAndView.addObject("pageNum", pageNum);
                                modelAndView.addObject("pageWin", pageWin);
                                modelAndView.addObject("resourceTypeSearchResultMap", resourceTypeSearchResultMap);
                            }
                        } else {
                            LOGGER.info("UDC Tag Searching record result count " + srs.getResultSize());
                        }
                    }
                } else {
                    LOGGER.info("No result found for UDC notaion Resource wise search ");
                }
            } catch (RestClientException | NumberFormatException e) {
                LOGGER.error("Error in UDC Tag Searching Result : " + e);
            }
            Map<String, ResourceType> mapOfResourceType = new HashMap<>();
            mapOfResourceType.put(resourcetype.getResourceTypeCode(), resourcetype);
            modelAndView.addObject("mapOfResourceType", mapOfResourceType);
            modelAndView.addObject("webserviceSearchForm", webserviceSearchForm);
            modelAndView.addObject("rTIconSearchLocation", resourceTypeIconSearchReadLocation);
            modelAndView.setViewName("udc-search-result");
        }
        return modelAndView;
    }

    /**
     * Get RT Search Icon.
     *
     * @param response {@link HttpServletResponse}
     * @param value
     */
    @RequestMapping(value = "/get/result/icon/{value:.+}", method = RequestMethod.GET)
    public void getRTSearchIcon(HttpServletResponse response, @PathVariable String value) {
        resourceService.getIcon(resourceTypeIconSearchLocation, value, response);
    }

    /**
     * Get RT Default Icon.
     *
     * @param response {@link HttpServletResponse}
     * @param value
     */
    @RequestMapping(value = "/get/default/icon/{value:.+}", method = RequestMethod.GET)
    public void getRTDeafultIcon(HttpServletResponse response, @PathVariable String value) {
        resourceService.getIcon(resourceTypeIconDefaultLocation, value, response);
    }

    /**
     * This method check whether resourceType belong to web scale discovery or
     * not.
     *
     * @param resourceTpe
     * @return
     */
    private boolean checkIsItWebScaleDiscovery(String resourceTpe) {
        switch (resourceTpe) {
            case "WIKI":
                return true;
            case "Google-Books":
                return true;
            case "Open-Library":
                return true;
            case "OCLC":
                return true;
            default:
                return false;
        }
    }

    /**
     * This method is used get the rating statistics of the record.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/ratingStatistics")
    @ResponseBody
    public List<ReviewAndRating> ratingStatistics(HttpServletRequest request) {
        String uniqueObjectIdentifier = request.getParameter("recordIdentifier");
        return userActivityService.getRatingStatistics(uniqueObjectIdentifier);
    }

    /**
     * getAllReviewsOfRecord method used to get all reviews on record
     *
     * @param uniqueObjectIdentifier record identifier
     * @return received reviews
     */
    @RequestMapping(value = "/record/reviews/{uniqueObjectIdentifier}/{pageNumber}/{pageWindow}")
    @ResponseBody
    public List<CustomRecordReview> getAllReviewsOfRecord(@PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier, @PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageWindow") int pageWindow) {
        List<CustomRecordReview> recReviews = new ArrayList<>();
        CustomRecordReview obj = null;
        try {
            List<ReviewAndRating> userReviews = reviewAndRatingService.findReviewsByRecordIdentifier(uniqueObjectIdentifier, pageNumber, pageWindow);
            for (ReviewAndRating userReview : userReviews) {
                obj = new CustomRecordReview();
                obj.setAddedTime(userReview.getTimeStamp().toString());
                //set the review msg given by user
                obj.setReviews(userReview.getReviewText());
                User user = userService.getUserById(userReview.getUserId());
                obj.setUserName((user.getFirstName() != null ? user.getFirstName() + " " : "") + (user.getMiddleName() != null ? user.getMiddleName() + " " : "") + (user.getLastName() != null ? user.getLastName() : ""));
                obj.setUserProfilePicPath(user.getProfilePicPath());
                obj.setRecordRating(userReview.getRecordRating());
                recReviews.add(obj);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return recReviews;
    }

    @ResponseBody
    @RequestMapping(value = "/fetch-full-text", method = {RequestMethod.POST, RequestMethod.GET})
    public String getBookOCRresult(@RequestParam("recordIdentifier") String recordIdentifier, @RequestParam("q") String q) {
        String finalURL = mitWSBaseURL + "ocr/fulltext?recordIdentifier=" + recordIdentifier + "&q=" + q;
        @SuppressWarnings("unchecked")
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity requestpost = new HttpEntity(Helper.mitWebserviceAthentication());
        ResponseEntity<String> result = restTemplate.exchange(finalURL, HttpMethod.GET, requestpost, String.class);
        return result.getBody();
    }
}
