package in.gov.nvli.controller;

import com.google.gson.Gson;
import in.gov.nvli.domain.user.FeedbackForm;
import in.gov.nvli.service.UserService;
import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@RestController
@RequestMapping(value = "fc")
public class FeedbackController {
    
    @Autowired
    private UserService userService;
    
    @RequestMapping(value = "feedback")
    public ModelAndView feedback(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.setViewName("UserFeedbackView");
        return modelAndView;
    }
        
    @RequestMapping(value = "feedback/submit",method = RequestMethod.POST)
    public boolean feedbackSubmit(HttpServletRequest request){
        String userId = request.getParameter("userId");
        Gson fbGson=new Gson();        
        FeedbackForm feedbackForm = fbGson.fromJson(request.getParameter("feedbackJson"), FeedbackForm.class);
        if(userId !=null  && !userId.trim().isEmpty()){
            feedbackForm.setUserId(userService.getUserById(Long.parseLong(request.getParameter("userId"))));
        }        
        feedbackForm.setFeedbackDate(new Timestamp(System.currentTimeMillis()));
        return userService.saveUserFeedback(feedbackForm);  
    }
    
}
