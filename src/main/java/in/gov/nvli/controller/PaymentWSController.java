package in.gov.nvli.controller;

import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.UserService;
import in.gov.nvli.webserviceclient.payment.PaymentWSHelper;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for handling all nvli payment service related requests.
 *
 * @author Milind
 */
@Controller
@RequestMapping(value = "/paymentws")
public class PaymentWSController {
    
    private static final Logger LOG = Logger.getLogger(PaymentWSController.class);
    
    @Autowired
    private PaymentWSHelper paymentWSHelper;
    
    @Autowired
    private UserService userService;
    
    /**
     * To check whether record is paid or unpaid
     * @param recordIdentifier
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/checkForPaidRecord",method=RequestMethod.POST)
    public @ResponseBody String checkForPaidRecord(@RequestParam String recordIdentifier) throws Exception {
        try{
            User user = userService.getCurrentUser();
            if (user != null) {
                return paymentWSHelper.checkUserPaidForRecord(recordIdentifier, user.getId());
            }else{
                return paymentWSHelper.checkForPaidRecord(recordIdentifier);
            }    
        }catch (Exception ex){
            LOG.error(ex.getMessage(),ex);
            throw new Exception("Payment Servic is not responding");
        }
    }
    
    /**
     * Fetch payment mode of paid record
     * @param recordIdentifier
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/fetchPaymentDetail",method=RequestMethod.POST)
    public @ResponseBody String fetchPaymentDetail(@RequestParam String recordIdentifier) throws Exception {
        try{
            return paymentWSHelper.getPaymentDetail(recordIdentifier);
        }catch (Exception ex){
            LOG.error(ex.getMessage(),ex);
            throw new Exception("Error in fetching payment details.");
        }
    }
    
    /**
     * Fetch price details of paid record for selected payment type
     * @param itemId
     * @param itemType
     * @return
     * @throws Exception 
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/fetchItemDetail",method=RequestMethod.POST)
    public @ResponseBody String fetchItemDetail(@RequestParam String itemId,@RequestParam String itemType) throws Exception {
        try{
            return paymentWSHelper.getItemDetail(itemId,itemType);
        }catch (Exception ex){
            LOG.error(ex.getMessage(),ex);
            throw new Exception("Error in fetching item details.");
        }
    }
 
    /**
     * 
     * @param itemId
     * @param itemType
     * @return 
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/payForItem",method=RequestMethod.POST)
    public @ResponseBody boolean payForItem(@RequestParam String itemId,@RequestParam String itemType) {
        try{
            User user = userService.getCurrentUser();
            return paymentWSHelper.payForItem(itemId,itemType,user);
        }catch (Exception ex){
            LOG.error(ex.getMessage(),ex);
            return false;
        }
    }
    
    /**
     * checks user is logged in or not
     * @return true if logged in else return false
     */
    @RequestMapping(value = "/isUserLoggedIn")
    public @ResponseBody String isUserLoggedIn() {
        return userService.getCurrentUser()!=null ? "true" : "false";
    }

    /**
     * show record content list of user paid record
     * @param recordIdentifier
     * @return 
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/record-file-list/{recordIdentifier:.*}", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView getAllPaidRecordFileList(@PathVariable String recordIdentifier) {
        ModelAndView modelAndView;
        modelAndView = new ModelAndView();
        User user = userService.getCurrentUser();
        try {
            modelAndView.addObject("recordIdentifier", recordIdentifier);
            modelAndView.addObject("record_file_list", paymentWSHelper.getAllPaidRecordFileList(user.getId(),recordIdentifier));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        modelAndView.setViewName("paid-record-preview");
        return modelAndView;
    }

    /**
     * fetch 
     * @param recordIdentifier
     * @param recordFileName
     * @param request
     * @return
     * @throws Exception 
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/record-file-path/{recordIdentifier:.*}/{recordFileName:.*}")
    public RedirectView playClickedFile(@PathVariable String recordIdentifier, @PathVariable String recordFileName,HttpServletRequest request) throws Exception {
        String referrer = request.getHeader("referer");
        if(referrer!=null){
            User user = userService.getCurrentUser();
            return new RedirectView(paymentWSHelper.getClickedFilePath(user.getId(),recordIdentifier, recordFileName));
        }else{
            return new RedirectView();
        }
    }
}
