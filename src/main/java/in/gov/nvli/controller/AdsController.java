/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.ads.AdsData;
import in.gov.nvli.beans.AdvertisementForm;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.service.UserService;
import in.gov.nvli.sslclient.SSLClientFactory;
import in.gov.nvli.webserviceclient.mit.SearchResponse;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 * This is Ads Controller
 *
 * @author Nitin
 */
@RestController
@Controller
@RequestMapping(value = "ad")
public class AdsController {

    //adds ralated controller
    /* Author-Nitin Karale
     Date - 26 April 2017
     */
    @Value("${webservice.ads.base.url}")
    private String adsWSBaseURL;
    @Autowired
    private UserDAO userDAO;

    @Value("${nvli.record.preview.tif.base.location}")
    private String adsMediaBaseLocation;

    @Autowired
    private UserService userService;

    private final static Logger LOGGER = Logger.getLogger(AdsController.class);

    /**
     * To get ads data using webservice
     *
     * @param searchResponse
     * @return
     */
    @RequestMapping(value = "/getAds", method = RequestMethod.POST)
    @ResponseBody
    public AdsData[] getAnalyticsDataFromServer(@ModelAttribute SearchResponse searchResponse) {
        AdsData[] adsData = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            String url = adsWSBaseURL + "get/ads/" + searchResponse.getSearchElement();
            // System.out.println("URL := " + url);
//            Long userId = 1L;
//            UserDetails userDetails = null;
//            if (userId != null) {
//                User user = userDAO.get(userId);
//                System.out.println("user name " + user.getFirstName());
//                if (user != null) {
//                    userDetails = new UserDetails();
//                    userDetails.setGender(user.getGender());
//                    userDetails.setCity(user.getCity());
//                    userDetails.setCountry(user.getCountry());
//                    List<UserInterest> userInterestsList = user.getInterests();
//                    List<String> interstesList = new ArrayList<>();
//                    for (UserInterest userInterest : userInterestsList) {
//                        interstesList.add(userInterest.getThematicType().getThematicType());
//                    }
//                    String interstes = String.join(",", interstesList);
//                    userDetails.setInterestAreas(interstes);
//                }
//            }
//            HttpEntity<String> request = new HttpEntity(userDetails);
//            adsData = restTemplate.postForObject(new URI(url), request, AdsData[].class);//exchange(url, HttpMethod.GET, request, AdsData[].class);
            ResponseEntity<AdsData[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, AdsData[].class);
            adsData = responseEntity.getBody();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        //System.out.println("Ads Data := " + adsData.toString());
        return adsData;
    }

    /**
     * This medthod used for create new ad
     *
     * @param request
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/createNewAd")
    public ModelAndView createNewAd(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            //get conent type
            String url = adsWSBaseURL + "getContentTypeMap";
            modelAndView.addObject("contentTypes", restTemplate.exchange(url, HttpMethod.GET, httpEntity, Map.class).getBody());
            //get dimensions
            url = adsWSBaseURL + "getDimensions";
            modelAndView.addObject("dimensions", restTemplate.exchange(url, HttpMethod.GET, httpEntity, Map.class).getBody());
            modelAndView.addObject("adForm", new AdvertisementForm());
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("Ad Manager", "#");
            breadcrumbMap.put("My Ad", "ad/userad");
            modelAndView.addObject("breadcrumbMap", breadcrumbMap);
            modelAndView.addObject("title", "Create New Ad");
            modelAndView.setViewName("CreateAdView");
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return modelAndView;
    }

    /**
     * This is method used for to get MIME Type using webservice
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getMimeTypes")
    @ResponseBody
    public Map<Object, Object> getMimeTypes(HttpServletRequest request) {
        Map<Object, Object> map = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            String url = adsWSBaseURL + "getMimeTypesOfContent/" + request.getParameter("contentTypeId");
            map = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Map.class).getBody();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        return map;
    }

    /**
     * This is method used for to view the ad already created
     *
     * @param advertisementForm
     * @param request
     * @param file
     * @return
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/saveAd", headers = "content-type=multipart/*")
    @ResponseBody
    public ModelAndView saveAd(@ModelAttribute("adForm") @Valid AdvertisementForm advertisementForm, HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file) {

        String status = "";
        HttpEntity<String> httpEntity = null;
        RestTemplate restTemplate = null;
        ModelAndView modelAndView = new ModelAndView();
        String url = adsWSBaseURL + "saveAd";
        advertisementForm.setUserId(Integer.parseInt(userService.getCurrentUser().getId() + ""));
        if (file != null && !file.isEmpty()) {

            advertisementForm.setFileSize(file.getSize() / 1024);
            try {
                httpEntity = new HttpEntity(advertisementForm, getConfiguredHttpHeaders());
                restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
                ResponseEntity<AdvertisementForm> aResponseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, AdvertisementForm.class);
                advertisementForm = aResponseEntity.getBody();
                File saveImage = new File(adsMediaBaseLocation + File.separator + advertisementForm.getRelativePath() + File.separator + advertisementForm.getMediaName());
                FileUtils.writeByteArrayToFile(saveImage, file.getBytes());
                status = "success";
            } catch (Exception ex) {
                LOGGER.error("Media upload failure ", ex);
                status = "error";
                ex.printStackTrace();
            } finally {

                //get conent type
                url = adsWSBaseURL + "getContentTypeMap";
                modelAndView.addObject("contentTypes", restTemplate.exchange(url, HttpMethod.GET, httpEntity, Map.class).getBody());
                //get dimensions
                url = adsWSBaseURL + "getDimensions";
                modelAndView.addObject("dimensions", restTemplate.exchange(url, HttpMethod.GET, httpEntity, Map.class).getBody());
                modelAndView.addObject("adForm", advertisementForm);
                modelAndView.addObject("status", status);
                modelAndView.setViewName("UserAd");

            }
        }
        return modelAndView;
    }

    /**
     * This method used for view/approve/reject ad by using marketing manager
     * role
     *
     * @return
     */
    @RequestMapping(value = "/view-approved-rejected-ad")
    @ResponseBody
    @PreAuthorize("isAuthenticated()")
    public ModelAndView viewApprovedRejectedAd() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ViewApprovedRejectedAd");
        return modelAndView;
    }

    /**
     * This method used for user's specific ad role
     *
     * @return
     */
    @RequestMapping(value = "/userad")
    @ResponseBody
    @PreAuthorize("isAuthenticated()")
    public ModelAndView userAd() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Ad Manager", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "My Ad");
        modelAndView.setViewName("UserAd");
        return modelAndView;
    }

    /**
     * This method used for display the ad & having marketing manager role
     *
     * @param searchString
     * @param dataLimit
     * @param pageNo
     * @param statusId
     * @return
     */
    @RequestMapping(value = "/getAdByFiltersWithLimit")
    @ResponseBody
    public String getAdByFiltersWithLimit(@RequestParam String searchString, @RequestParam int dataLimit, @RequestParam int pageNo, @RequestParam int statusId) {
        String jsonObj;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            String url = adsWSBaseURL + "render-ad-approve-reject/" + pageNo + "/" + dataLimit + "/" + statusId + "?searchString=" + searchString;
            jsonObj = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            jsonObj = "error";
        }
        return jsonObj;
    }

    /**
     * This method used for user specific list of ad
     *
     * @param searchString
     * @param dataLimit
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "/getUserAdByFiltersWithLimit")
    @ResponseBody
    public String getUserSpecificAd(@RequestParam String searchString, @RequestParam int dataLimit, @RequestParam int pageNo) {
        String jsonObj;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            String url = adsWSBaseURL + "user-specific-ad/" + pageNo + "/" + dataLimit + "/" + userService.getCurrentUser().getId() + "?searchString=" + searchString;
            jsonObj = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            ex.printStackTrace();
            jsonObj = "error";
        }
        return jsonObj;
    }

    /**
     * To set the ad status
     *
     * @param adId
     * @param statusId
     * @return
     */
    @RequestMapping(value = "/setAdStatus")
    @ResponseBody
    public String setAdStatus(@RequestParam int adId, @RequestParam int statusId) {
        String url = adsWSBaseURL + "set-ad-status/" + adId + "/" + statusId;

        String jsonObj;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            jsonObj = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
        } catch (RestClientException ex) {
            LOGGER.error(ex.getMessage());
            jsonObj = "error";
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            jsonObj = "error";
        }
        return jsonObj;
    }

    /**
     * To delete the ad
     *
     * @param adId
     * @return
     */
    @RequestMapping(value = "/ad-delete")
    @ResponseBody
    public String deleteAds(@RequestParam int adId) {
        String url = adsWSBaseURL + "ad-delete/" + adId + "/" + userService.getCurrentUser().getId();
        String jsonObj;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            jsonObj = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
        } catch (RestClientException ex) {
            LOGGER.error(ex.getMessage());
            jsonObj = "error";
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            jsonObj = "error";
        }
        return jsonObj;
    }

    /**
     * To get the ad status
     *
     * @param adId
     * @param view
     * @return
     */
    @RequestMapping(value = "/get-ad-details/{adId}/{view}")
    @ResponseBody
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getAdDetails(@PathVariable int adId, @PathVariable String view) {
        String url = adsWSBaseURL + "get-ad-details/" + adId;
        Map<String, Object> map = null;
        String resultStatus = "success";
        Number adCreatedUser = null;
        try {
            HttpEntity<String> httpEntity = new HttpEntity(getConfiguredHttpHeaders());
            RestTemplate restTemplate = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            String jsonObjStr = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class).getBody();
            JSONObject jsonObj = new JSONObject(jsonObjStr);
            adCreatedUser = (Number) jsonObj.get("userId");
            //userService.getUserById(adCreatedUser);

            if ((view.equals("view")) && (adCreatedUser.longValue() != userService.getCurrentUser().getId())) {
                resultStatus = "error";
            }
            ObjectMapper mapper = new ObjectMapper();
            map = mapper.readValue(jsonObjStr, Map.class);
        } catch (RestClientException | JSONException | IOException ex) {
            LOGGER.error(ex.getMessage());
            resultStatus = "error";
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            resultStatus = "error";
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("view", view);
        modelAndView.addObject("adDetails", map);
        modelAndView.addObject("resultStatus", resultStatus);
        modelAndView.addObject("advertiseUser", userService.getUserById(adCreatedUser.longValue()));
        modelAndView.setViewName("ViewAd");
        return modelAndView;
    }

    /**
     * This method is used to get configured HttpHeaders.
     *
     * @return {@link HttpHeaders}
     * @throws Exception
     */
    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}
