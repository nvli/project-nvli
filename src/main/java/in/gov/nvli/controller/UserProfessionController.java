package in.gov.nvli.controller;

import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserProfession;
import in.gov.nvli.service.UserProfessionService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller handles requests related to User Professions
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Controller
@RequestMapping(value = "user/profession")
public class UserProfessionController {

    @Autowired
    private UserService userService;

    @Autowired
    UserProfessionService userProfessionService;

    /**
     * Loads UserProfession View
     *
     * @return ModelAndView
     */
    @RequestMapping(value = {"/", ""})
    public ModelAndView userProfession() {
        ModelAndView modelAndView = new ModelAndView("UserProfessionView");
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("userProfessions", userProfessionService.listBaseProfessions());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Settings", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Profession");
        return modelAndView;
    }

    /**
     * Handles request to add or remove profession
     *
     * @param professionId
     * @param action {"add"|"rm"}
     * @return JSONObject
     */
    @RequestMapping(value = "/{action}/{professionId}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject addUserProfession(
            @PathVariable(value = "professionId") long professionId,
            @PathVariable(value = "action") String action
    ) {
        JSONObject jsonObject = new JSONObject();
        User currentUser = userService.getCurrentUser();
        if (action.equalsIgnoreCase("add")) {
            currentUser.getUserProfessions().add(userProfessionService.findProfessionByID(professionId));
        } else if (action.equalsIgnoreCase("rm")) {
            currentUser.getUserProfessions().remove(userProfessionService.findProfessionByID(professionId));
        }

        userService.updateUser(currentUser);
        jsonObject.put("status", Constants.AjaxResponseCode.SUCCESS);
        return jsonObject;
    }

    /**
     * Handles request to save new Profession for the logged in user.
     *
     * @param otherProfession
     * @param parentId
     * @return JSONObject
     */
    @RequestMapping(value = "/save/other/{otherProfession}/{parentId}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject saveOtherProfession(
            @PathVariable(value = "otherProfession") String otherProfession,
            @PathVariable(value = "parentId") long parentId
    ) {
        UserProfession otherProfessionObject = new UserProfession();
        JSONObject obj = new JSONObject();
        otherProfessionObject.setProfessionName(otherProfession);
        if (parentId > 0L) {
            otherProfessionObject.setParentProfession(userProfessionService.findProfessionByID(parentId));
        }
        otherProfessionObject.setIsOther(true);
        UserProfession created = userProfessionService.create(otherProfessionObject);

        if (created != null) {
            User currentUser = userService.getCurrentUser();
            currentUser.getUserProfessions().add(created);
            userService.updateUser(currentUser);
            obj.put("status", Constants.AjaxResponseCode.SUCCESS);
        }
        obj.put("status", Constants.AjaxResponseCode.ERROR);
        return obj;
    }

    /**
     * Method to handle request to get parent profession by profession id
     *
     * @param parentProfessionId
     * @return JSONObject
     */
    @RequestMapping(value = "/{parentProfessionId}")
    @ResponseBody
    public JSONObject getUserProfessionByID(
            @PathVariable(value = "parentProfessionId") long parentProfessionId
    ) {
        JSONObject finalObject = new JSONObject();
        JSONArray array = new JSONArray();
        List<UserProfession> userProfessionsByParentID = userProfessionService.findProfessionByID(parentProfessionId).getSubProfessions();
        for (UserProfession profession : userProfessionsByParentID) {
            if (!profession.isIsOther()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("professionId", profession.getProfessionId());
                jsonObject.put("professionName", profession.getProfessionName());
                jsonObject.put("professionDescription", profession.getProfessionDescription());
                jsonObject.put("professionUdcTags", profession.getUdcTags());
                boolean hasChildren = !profession.getSubProfessions().isEmpty();
                jsonObject.put("hasChildren", hasChildren);
                array.add(jsonObject);
            }
        }
        finalObject.put("parentId", parentProfessionId);
        finalObject.put("children", array);
        return finalObject;
    }

    /**
     * This controller method handles the request to serve list of User
     * Professions
     *
     * @return JSONObject
     */
    @RequestMapping(value = "/saved/list")
    @ResponseBody
    public JSONObject listSavedProfession() {
        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        Set<UserProfession> savedProfessions = userService.getCurrentUser().getUserProfessions();
        for (UserProfession profession : savedProfessions) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("professionId", profession.getProfessionId());
            jsonObject.put("professionName", profession.getProfessionName());
            jsonObject.put("professionDescription", profession.getProfessionDescription());
            jsonObject.put("professionUdcTags", profession.getUdcTags());
            boolean hasChildren = !profession.getSubProfessions().isEmpty();
            jsonObject.put("hasChildren", hasChildren);
            if (null != profession.getParentProfession()) {
                jsonObject.put("parentId", profession.getParentProfession().getProfessionId());
            }
            array.add(jsonObject);
        }
        obj.put("data", array);
        return obj;
    }
}
