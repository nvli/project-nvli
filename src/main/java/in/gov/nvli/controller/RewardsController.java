package in.gov.nvli.controller;

import in.gov.nvli.domain.rewards.BadgeDetails;
import in.gov.nvli.domain.rewards.LevelDetails;
import in.gov.nvli.domain.rewards.RewardsPointDetails;
import in.gov.nvli.domain.rewards.UserRewardsSummary;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.ActivityLog;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.service.CertificateGenerationService;
import in.gov.nvli.service.RewardsService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.GenerateSalt;
import in.gov.nvli.util.Helper;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import sun.misc.BASE64Encoder;

/**
 * @author Ankita Dhongde <dankita@cdac.in>
 * @author Gulafsha<gulafsha@cdac.in>
 */
@RestController
@RequestMapping(value = "/rewards")
public class RewardsController {

    @Autowired
    RewardsService rewardsService;

    @Autowired
    UserService userService;

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    private CertificateGenerationService certificateGenerationService;

    @RequestMapping(value = "/getRewardsPoinDetails")
    @ResponseBody
    public List<RewardsPointDetails> getRewardsPointDetails() {
        return rewardsService.getRewardPointDetails();
    }

    @RequestMapping(value = "/getRewardsLevelList")
    @ResponseBody
    public List<LevelDetails> getRewardsLevelList() {
        return rewardsService.getRewardsLevelList();
    }

    @RequestMapping(value = "/getUserRewardsSummery")
    @ResponseBody
    public JSONObject getUserRewardSummery() {
        JSONObject jsno = new JSONObject();
        User userObj = userService.getCurrentUser();
        UserRewardsSummary urs = rewardsService.fetchUserRewardSummeryByUser(userObj);
        String levelName;
        BadgeDetails certificateAwarded;
        Long levelId = null;
        int approvedPoints, editedPoint;
        long starsAwarded;
        if (urs != null) {
            approvedPoints = urs.getApprovedPoints();
            editedPoint = urs.getEditPoints();
            levelName = urs.getCurrentLevel().getLevelName();
            levelId = urs.getCurrentLevel().getLevelId();
            starsAwarded = urs.getCurrentLevel().getStarsAwarded().getStarsId();
            certificateAwarded = urs.getCurrentLevel().getBadgeAwarded();
        } else {
            approvedPoints = 0;
            editedPoint = 0;
            levelName = "Level 1";
            starsAwarded = 0;
            certificateAwarded = null;

        }
        jsno.put("approvedPoints", approvedPoints);
        jsno.put("editPoints", editedPoint);
        jsno.put("levelName", levelName);
        jsno.put("levelId", levelId);
        jsno.put("starAwarded", starsAwarded);
        jsno.put("certificateAwarded", certificateAwarded);
        jsno.put("userId", userService.getCurrentUser().getId());

        List<LevelDetails> levelList = rewardsService.getRewardsLevelList();
        boolean isEditPointsFound = false;
        boolean isApprovedPointsFound = false;
        int maxEditPoint = 0, maxApprovedPoint = 0;

        for (LevelDetails llist : levelList) {
            int ListEditPoints = llist.getEditPoints();
            if (editedPoint < ListEditPoints && isEditPointsFound == false) {
                maxEditPoint = ListEditPoints;
                isEditPointsFound = true;
            }
            int listApprovedPoint = llist.getApprovedPoints();
            if (approvedPoints < listApprovedPoint && isApprovedPointsFound == false) {
                maxApprovedPoint = listApprovedPoint;
                isApprovedPointsFound = true;
            }
        }
        jsno.put("maxEditPoint", maxEditPoint);
        jsno.put("maxApprovedPoint", maxApprovedPoint);
        jsno.put("rewardHistory", activityLogService.getRewardActivityLogListOfUser(1, 10, userService.getCurrentUser().getId()));
        return jsno;
    }

    @RequestMapping(value = "/showRewards")
    public ModelAndView getRewardActivityHistory() {
        ModelAndView modelAndView = new ModelAndView("UserRewardsView");
        User userObj = userService.getCurrentUser();
        List<ActivityLog> rewardHistory = activityLogService.getRewardActivityLogListOfUser(1, 10, userObj.getId());
        modelAndView.addObject("rewardHistory", rewardHistory);
        modelAndView.addObject("user", userObj);
        modelAndView.addObject("user", userObj);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("My Stuff", "#");
        modelAndView.addObject("title", "Reward History");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        return modelAndView;
    }

    @RequestMapping(value = "/showRewardsPerPage")
    @ResponseBody
    public List<ActivityLog> getRewardHistoryPerPage(HttpServletRequest request, @RequestParam("pageNo") int pageNo) {

        // maxPerPage = Integer.parseInt(request.getParameter("maxPerPage"));
        List<ActivityLog> rewardHistory = activityLogService.getRewardActivityLogListOfUser(pageNo, 10, userService.getCurrentUser().getId());
        return rewardHistory;
    }

    @RequestMapping(value = "/certificate")
    public RedirectView generateCertificate() {
        try {
            System.out.println("------Inside generateCertificate--------");
            boolean response = certificateGenerationService.issueCertificate(Long.parseLong("38"), Long.parseLong("1"));
            System.out.println("response ::" + response);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new RedirectView("/home", true);
    }

//    @RequestMapping(value = "/certificate")
//    public void generateCertificate(HttpServletResponse httpServletResponse) throws IOException {
//        System.out.println("---"+httpServletResponse.encodeRedirectURL("http://localhost:81/certificates/110001100011.pdf"));;
//       httpServletResponse.sendRedirect(httpServletResponse.encodeRedirectURL("http://localhost:81/certificates/110001100011.pdf"));
//
//    }
    @RequestMapping(value = "/certi/{uid}")
    public RedirectView redirectController(
            HttpServletRequest request,
            @PathVariable(value = "uid") Long uid,
            @RequestParam(value = "type") String type,
            @RequestParam(value = "badgeCode") String badgeCode
    ) {
        String salt = GenerateSalt.getSalt();
        BadgeDetails badge = rewardsService.getBadgeByBadgeCode(badgeCode);
        Long certificateId = rewardsService.getCertificateIdByUserIdAndByBadgeId(uid, badge);
        String signature = this.encodeCidDate(salt, certificateId);
        return new RedirectView("http://localhost/volume1/application/certificates?sig=" + signature + "&cid=" + certificateId + "&sa=" + salt);
    }

    private String encodeCidDate(String salt, Long cid) {
        MessageDigest hash;
        String out = "";
        try {
            hash = MessageDigest.getInstance("SHA-256");
            byte[] digest = hash.digest((cid + salt).getBytes());
            out = new BASE64Encoder().encode(digest);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(RewardsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    @RequestMapping(value = "/manage/points-details")
    public ModelAndView managaePointsDetails() {
        ModelAndView modelAndView = new ModelAndView("PointDetailsView");
        return modelAndView;

    }

    @RequestMapping(value = "/manage/level-details")
    public ModelAndView managaeLevelDetails() {
        ModelAndView modelAndView = new ModelAndView("LevelDetailsView");
        return modelAndView;
    }

    @RequestMapping(value = "getLevels")
    @ResponseBody
    public JSONObject getLevels() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("LevelsList", rewardsService.getLevels());
            return jsonObject;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "/edit-level", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject editLevel(
            @RequestParam(value = "levelId") String levelId) throws Exception {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("level", rewardsService.getLevelById(Long.parseLong(levelId)));
            jsonObject.put("badges", rewardsService.getAllBadges());
            jsonObject.put("starsDetails", rewardsService.getAllStars());

        } catch (Exception ex) {
            throw ex;
        }
        return jsonObject;
    }

    @RequestMapping(value = "/update/level-details", produces = "application/json", method = RequestMethod.POST)
    public JSONObject updateLevel(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "levelId") String levelId,
            @RequestParam(value = "editPoints") String editPoints,
            @RequestParam(value = "approvedPoints") String approvedPoints,
            @RequestParam(value = "starsAwarded") String starsAwarded,
            @RequestParam(value = "badgeAwarded") String badgeAwarded,
            @RequestParam(value = "activity") String activity,
            @RequestParam(value = "levelName") String levelName) {
        JSONObject obj = new JSONObject();
        LevelDetails levelToCreate = new LevelDetails();
        levelToCreate.setApprovedPoints(Integer.valueOf(approvedPoints));
        levelToCreate.setEditPoints(Integer.valueOf(editPoints));
        levelToCreate.setBadgeAwarded(rewardsService.getBadgeByBadgeCode(badgeAwarded));
        levelToCreate.setStarsAwarded(rewardsService.getStarsByCode(starsAwarded));
        levelToCreate.setCreatedBy(userService.getCurrentUser().getId());
        levelToCreate.setLastUpdatedBy(userService.getCurrentUser().getId());
        levelToCreate.setIsCurrent(true);
        levelToCreate.setLastModifiedDate(Helper.rightNow());
        if (activity.equalsIgnoreCase("Create")) {
            levelToCreate.setLevelName(levelName);
            if (rewardsService.createLevel(levelToCreate)) {
                obj.put("status", "create-success");
                return obj;
            }
        } else if (activity.equalsIgnoreCase("Edit")) {
            LevelDetails levelToUpdate = rewardsService.getLevelById(Long.parseLong(levelId));
            levelToUpdate.setLastUpdatedBy(userService.getCurrentUser().getId());
            levelToUpdate.setLastModifiedDate(Helper.rightNow());
            levelToUpdate.setIsCurrent(false);
            levelToCreate.setLevelName(levelToUpdate.getLevelName());
            if (rewardsService.updateAndCreate(levelToCreate, levelToUpdate)) {
                obj.put("status", "update-success");
                return obj;
            }
        }
        obj.put("status", "error");
        return obj;
    }

    @RequestMapping(value = "/find/last-level", method = RequestMethod.POST)
    public JSONObject findLastLevel() {
        JSONObject obj = new JSONObject();
        LevelDetails levelDetails = rewardsService.getLastLevel();
        obj.put("LastLevel", levelDetails);
        String newLevel = "Level " + (Integer.parseInt(levelDetails.getLevelName().substring(6, levelDetails.getLevelName().length())) + 1);
        obj.put("newLevel", newLevel);
        obj.put("badges", rewardsService.getAllBadges());
        obj.put("starsDetails", rewardsService.getAllStars());
        System.out.println(newLevel);
        return obj;
    }

    @RequestMapping(value = "getPointsDetails")
    @ResponseBody
    public JSONObject getPointsDetails() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pointsDetailList", rewardsService.getRewardPointDetails());
            return jsonObject;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/edit-points-details", produces = "application/json", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject editPointsDetails(
            @RequestParam(value = "pointsId") String pointsId) throws Exception {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pointDetails", rewardsService.getPointsDetailsById(Long.parseLong(pointsId)));
        } catch (Exception ex) {
            throw ex;
        }
        return jsonObject;
    }

    @RequestMapping(value = "/update/points-details", produces = "application/json", method = RequestMethod.POST)
    public JSONObject updatePointsDetails(
            HttpServletRequest request,
            HttpServletResponse response,
            @RequestParam(value = "pointsId") String pointsId,
            @RequestParam(value = "earnedPoints") String earnedPoints
    ) {
        JSONObject obj = new JSONObject();
        RewardsPointDetails pointsDetailsToUpdate = rewardsService.getPointsDetailsById(Long.parseLong(pointsId));
        RewardsPointDetails pointDetailsToCreate = new RewardsPointDetails();
        pointDetailsToCreate.setEarnedPoints(Integer.valueOf(earnedPoints));
        pointDetailsToCreate.setIsCurrent(true);
        pointDetailsToCreate.setActivityCode(pointsDetailsToUpdate.getActivityCode());
        pointDetailsToCreate.setActivityDesc(pointsDetailsToUpdate.getActivityDesc());
        pointDetailsToCreate.setLowerBoundAccuracy(pointsDetailsToUpdate.getLowerBoundAccuracy());
        pointDetailsToCreate.setUpperBoundAccuracy(pointsDetailsToUpdate.getUpperBoundAccuracy());
        pointsDetailsToUpdate.setIsCurrent(false);
        if (rewardsService.updateAndCreatePoints(pointDetailsToCreate, pointsDetailsToUpdate)) {
            obj.put("status", "update-success");
            return obj;
        }
        obj.put("status", "error");
        return obj;
    }
}
