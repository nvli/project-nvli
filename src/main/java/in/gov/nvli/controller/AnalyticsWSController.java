package in.gov.nvli.controller;

import in.gov.nvli.beans.AnalyticsQueryParam;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.sslclient.SSLClientFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * This controller is to handle REST API requests.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@RestController
@RequestMapping(value = "/analytics/ws")
public class AnalyticsWSController {

    @Value("${analytics.webservice.url}")
    private String analyticsWebserviceURL;

    @Autowired
    private IResourceTypeDAO resourceTypeDAO;

    @Autowired
    RedisTemplate redisTemplate;

    final String SHARE_LAST_24H = "recordLast24Hr";

    final String SHARE_LAST_1M7D = "recordLast1M7D";

    final String SHARE_LAST_1Y = "recordLast1Year";

    /**
     * Method to fetch top trending search phrases
     *
     * @return Object
     */
    @RequestMapping(value = "/trending/search", method = RequestMethod.GET)
    public Object getTrendingSearch() {
        String url = analyticsWebserviceURL + "/activityAPI/trending/search";
        RestTemplate template = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        ResponseEntity responseEntity = template.getForEntity(url, Map.class);
        return responseEntity.getBody();
    }

    /**
     * Method to fetch top shared resources within a specified time range
     *
     * @param timeRange
     * @return Object
     */
    @RequestMapping(value = "/top/shared/records/{timeRange}", method = RequestMethod.GET)
    public Object getTrendingSearch(
            @PathVariable(value = "timeRange") String timeRange
    ) {

        String key;
        switch (timeRange) {
            case "d":
                key = SHARE_LAST_24H;
                break;
            case "w":
                key = SHARE_LAST_1M7D;
                break;
            case "m":
                key = SHARE_LAST_1M7D;
                break;
            case "y":
                key = SHARE_LAST_1Y;
                break;
            default:
                key = SHARE_LAST_1Y;
                break;
        }
        if (redisTemplate.hasKey(key)) {
            Map<String, Object> map = new HashMap<>();
            map.put("data", redisTemplate.opsForHash().get(key, "data"));
            map.put("info", redisTemplate.opsForHash().get(key, "info"));
            map.put("trendsData", redisTemplate.opsForHash().get(key, "trendsData"));
            return map;
        } else {
            String url = analyticsWebserviceURL + "/activityAPI/top/shared/record/" + timeRange;
            RestTemplate template = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
            ResponseEntity responseEntity = template.getForEntity(url, Map.class);
            return responseEntity.getBody();
        }
    }

    /**
     * Method to fetch resource Types
     *
     * @return JSONArray
     */
    @RequestMapping(value = "/get/resourceTypes", method = RequestMethod.GET)
    public JSONArray getResourceTypes() {
        JSONArray array = new JSONArray();
        List<ResourceType> resourceTypes = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();
        for (ResourceType resourceType : resourceTypes) {
            JSONObject obj = new JSONObject();
            obj.put("resourceType", resourceType.getResourceType());
            obj.put("resourceTypeCode", resourceType.getResourceTypeCode());
            obj.put("resourceTypeCategory", resourceType.getResourceTypeCategory());
            array.add(obj);
        }
        return array;
    }

    /**
     * Method to fetch named entities
     *
     * @return JSONArray
     */
    @RequestMapping(value = "/get/named/entities/{page}", method = RequestMethod.GET)
    public JSONObject getNamedEntities(
            @PathVariable(value = "page") String page
    ) {
        JSONObject jsono = new JSONObject();
        String url = analyticsWebserviceURL + "/entity/getPersonNamesEntity/" + page;
        System.out.println("url ---->>>" + url);
        RestTemplate template = new RestTemplate(SSLClientFactory.getClientHttpRequestFactory(SSLClientFactory.HttpClientType.HttpClient));
        ResponseEntity responseEntity = template.getForEntity(url, List.class);
        jsono.put("children", responseEntity.getBody());
        return jsono;
    }

    @RequestMapping(value = "/get/recordInfo")
    @ResponseBody
    public JSONObject getRecordInfo(HttpServletRequest request) {
        JSONObject jsono = new JSONObject();
        Map<String, String[]> map = request.getParameterMap();
        List<String> identifiers = new ArrayList<>();
        for (Map.Entry<String, String[]> entry : map.entrySet()) {
            String[] ids = entry.getValue();
            for (String id : ids) {
                identifiers.add(id);
            }
        }
        String plainCreds = "MetadataIntegratorUser:mit_user@123#";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", "Basic " + base64Creds);
        RestTemplate template = new RestTemplate();

        String url = "http://10.208.27.110:8080/MetadataIntegratorWebService/webservice/request/recordinfo";
        AnalyticsQueryParam q = new AnalyticsQueryParam();
        List<String> elemnts = new ArrayList<>();
        elemnts.add("title");
        q.setDcElements(elemnts);
        q.setRecordIdentifiers(identifiers);

        HttpEntity request1 = new HttpEntity(q, headers);
        ResponseEntity<List> listOfSuggestion = template.postForEntity(url, request1, List.class);
        jsono.put("recordinfo", listOfSuggestion.getBody());
        return jsono;
    }
}
