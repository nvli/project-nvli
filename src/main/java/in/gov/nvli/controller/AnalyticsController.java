package in.gov.nvli.controller;

import in.gov.nvli.dao.resource.IGroupTypeDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.widget.RecordInfo;
import in.gov.nvli.mongodb.domain.ActivityLog;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.service.WidgetService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Helper;
import in.gov.nvli.util.SearchKeyword;
import in.gov.nvli.webserviceclient.mit.SearchComplexityEnum;
import in.gov.nvli.webserviceclient.mit.SearchResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

/**
 *
 * @author Gulafsha
 * @author Ruturaj
 */
@Controller
@RequestMapping(value = "/analytics")
public class AnalyticsController {

    @Value("${widget.file.location}")
    String widgetFileLocation;

    @Autowired
    WidgetService widgetService;

    @Autowired
    UserService userService;

    @Autowired
    private ServletContext context;

    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    @Value("${analytics.webservice.url}")
    private String analyticsWebserviceURL;

    @Autowired
    private IResourceTypeDAO resourceTypeDAO;

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    public IResourceDAO resourceDAO;

    @Autowired
    public ResourceService resourceService;

    @Autowired
    public IGroupTypeDAO groupTypeDAO;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * The name of the property LOGGER used to holds {@link Logger} object
     * reference.
     */
    private final static Logger LOGGER = Logger.getLogger(AnalyticsController.class);

    /**
     * This controller method loads Default Widget and Analytics View
     *
     * @return ModelAndView
     */
    @RequestMapping(value = {"/", ""})
    public ModelAndView widgetAnalytics() {
        ModelAndView modelAndView = new ModelAndView();
        List<ResourceType> listOFResourceType = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();
        modelAndView.addObject("listOFResourceType", listOFResourceType);
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("analyticsEndpoint", analyticsWebserviceURL);
        modelAndView.setViewName("WidgetAndAnalyticsView");
        return modelAndView;
    }

    /**
     * This Controller Method loads dashboard for administrator
     *
     * @return ModelAndView
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = {"/admin-dashboard"})
    public ModelAndView admninDashboard() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("title", "Admin Dashboard");
        modelAndView.addObject("analyticsEndpoint", analyticsWebserviceURL);
        modelAndView.setViewName("adminDashboardView");
        return modelAndView;
    }

    /**
     * This controller method loads trends view page showing various trends
     *
     * @return ModelAndView
     */
    @RequestMapping(value = {"/trends"})
    public ModelAndView trendsHome() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("title", "NVLI Trends");
        modelAndView.addObject("analyticsEndpoint", analyticsWebserviceURL);
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.setViewName("TrendAnalyticsView");
        return modelAndView;
    }

    /**
     * Method to return record information from MIT Rest services by Resource
     * Type
     *
     * @param resourceTypeId
     * @return RecordInfo[]
     */
    @RequestMapping(value = "/getRecordInfos/{resourceTypeId}")
    @ResponseBody
    public RecordInfo[] getRecordInfos(@PathVariable("resourceTypeId") String resourceTypeId) {
        HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "analytics/view/" + resourceTypeId;
        RecordInfo[] analyticsData = null;
        analyticsData = restTemplate.postForObject(url, httpEntity, RecordInfo[].class);
        return analyticsData;
    }

    /**
     * Method to serve random resources as per resource type
     *
     * @param resourceTypeId
     * @return RecordInfo[]
     */
    @RequestMapping(value = "/getrandomResourcesFromServer/{resourceTypeId}")
    @ResponseBody
    public RecordInfo[] getrandomResourcesFromServer(@PathVariable("resourceTypeId") String resourceTypeId) {
        HttpEntity<String> httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
        RestTemplate restTemplate = new RestTemplate();
        String url = mitWSBaseURL + "analytics/records/" + resourceTypeId;
        RecordInfo[] analyticsData = null;
        analyticsData = restTemplate.postForObject(url, httpEntity, RecordInfo[].class);
        return analyticsData;
    }

    /**
     * To Find most searched keywords
     *
     * @return List of {@link SearchKeyword}
     */
    @RequestMapping("/get/most/searched/keywords")
    @ResponseBody
    public List<SearchKeyword> getMostSearchedKeywords() {
        if (redisTemplate.hasKey("mostSearchedKeywords")) {
            Set<TypedTuple> ttSet = redisTemplate.opsForZSet().rangeWithScores("mostSearchedKeywords", 0, 50);
            List<SearchKeyword> listOfMSK = new ArrayList<>();
            ttSet.forEach(tt -> listOfMSK.add(new SearchKeyword(tt.getScore().intValue(), (String) tt.getValue())));
            Collections.shuffle(listOfMSK);
            return listOfMSK;
        } else {
            Aggregation aggregation = newAggregation(ActivityLog.class,
                    match(Criteria.where("activity_type").is(Constants.UserActivityConstant.KEYWORD_SEARCHED)),
                    unwind("activity.tokanizedPhrase"), group("activity.tokanizedPhrase").count().as("score"),
                    sort(Sort.Direction.DESC, "score"), Aggregation.limit(50));
            AggregationResults results = mongoTemplate.aggregate(aggregation, "activityLog", SearchKeyword.class);
            List<SearchKeyword> listOfMSK = results.getMappedResults();
            List<SearchKeyword> listOfMSKtemp = new ArrayList<>();
            listOfMSKtemp.addAll(listOfMSK);
            Collections.shuffle(listOfMSKtemp);
            return listOfMSKtemp;
        }
    }

    /**
     * Method to migrate the SearchActivityLog from ActivityLog for analytics
     * purpose, and return the default analytics view with all required models.
     *
     * @return ModelAndView
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = {"/migrate-search-data"})
    public ModelAndView migrateSearchKeywords() {
        ModelAndView modelAndView = new ModelAndView();
        List<ResourceType> listOFResourceType = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();
        modelAndView.addObject("listOFResourceType", listOFResourceType);
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.addObject("analyticsEndpoint", analyticsWebserviceURL);
        modelAndView.setViewName("WidgetAndAnalyticsView");
        return modelAndView;
    }

    /**
     * Method to fetch resources and their subcatagories and content count
     *
     * @return JSONObject
     */
    @RequestMapping(value = "/resources")
    @ResponseBody
    public JSONObject visualizeResources() {
        JSONObject object = new JSONObject();
        object.put("name", "Resources");
        JSONArray typeArray = new JSONArray();

        List<ResourceType> resourceTypesAccToTotalRecordCount = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();//resourceDAO.getListOfUsedResourceType();

        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setSearchComplexity(SearchComplexityEnum.BASIC);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        for (ResourceType resourceType : resourceTypesAccToTotalRecordCount) {
            JSONObject typeObject = new JSONObject();
            typeObject.put("name", resourceType.getResourceType());
            JSONArray typeObjectArray = new JSONArray();

            List<GroupType> groupTypes = groupTypeDAO.getGroupByResourceType(resourceType);

            for (GroupType groupType : groupTypes) {
                JSONObject groupObject = new JSONObject();
                groupObject.put("name", groupType.getGroupType());
                JSONArray groupTypeArray = new JSONArray();
                List<Resource> listOfResource = resourceDAO.getResourceByGroupAndResourceType(groupType, resourceType);
                if (listOfResource != null && !listOfResource.isEmpty()) {
                    for (Resource res : listOfResource) {
                        JSONObject groupTypeObject = new JSONObject();
                        groupTypeObject.put("name", res.getResourceName());
                        groupTypeObject.put("size", res.getRecordCount());
                        groupTypeArray.add(groupTypeObject);
                    }
                }
                groupObject.put("children", groupTypeArray);
                typeObjectArray.add(groupObject);
            }
            typeObject.put("children", typeObjectArray);
            typeArray.add(typeObject);
        }
        object.put("children", typeArray);
        return object;
    }

    @RequestMapping(value = "/named/entities")
    @ResponseBody
    public ModelAndView bubbles() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.setViewName("NamedEntitiesView");
        return modelAndView;
    }
}
