package in.gov.nvli.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.dao.resource.IGroupTypeDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.util.Helper;
import in.gov.nvli.webserviceclient.mit.AdvanceSearchForm;
import in.gov.nvli.webserviceclient.mit.AdvanceSearchWebserviceHelper;
import in.gov.nvli.webserviceclient.mit.BasicSearchWebserviceHelper;
import in.gov.nvli.webserviceclient.mit.NamesIdentifiers;
import in.gov.nvli.webserviceclient.mit.SearchResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * Controller for Advanced Search
 *
 * @author Abhishek Acharya abhisheka@cdac.in
 * @author Ritesh Malviya
 */
@Controller
@RequestMapping(value = "advsearch")
public class AdvanceSearchController {

    private final static Logger logger = Logger.getLogger(AdvanceSearchController.class);

    @Autowired
    public IGroupTypeDAO groupTypeDAO;

    @Autowired
    public IResourceTypeDAO resourceTypeDAO;

    @Autowired
    public IResourceDAO resourceDAO;

    @Autowired
    private CrowdSourcingService crowdSourcingService;

    @Autowired
    private AdvanceSearchWebserviceHelper advanceSearchWebserviceHelper;

    @Autowired
    private BasicSearchWebserviceHelper basicSearchWebserviceHelper;

    /**
     * To hold the resource type icon read location
     */
    @Value("${nvli.read.resourcType.image.location}")
    private String resourceTypeIconReadLocation;

    /**
     * To hold the resource type default icon read location
     */
    @Value("${nvli.read.resourcType.default.image.location}")
    private String resourceTypeIconDefalutReadLocation;

    @RequestMapping("")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("advanced-search");
        try {
            AdvanceSearchForm advsearchResponse = new AdvanceSearchForm();
            mav.addObject("advanceSearchForm", advsearchResponse);
            mav.addObject("languageList", crowdSourcingService.getIndianLanguageList());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mav;
    }

    @RequestMapping(value = "getResourceWiseFieldSuggestions/{resourceCode}/{field}/{resourceJSONList}", produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String getResourceWiseFieldSuggestions(@PathVariable String resourceCode, @PathVariable String field, @PathVariable String resourceJSONList, HttpServletRequest request) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        List<String> resourceCodeList = mapper.readValue(resourceJSONList, List.class);
        String suggestionJSONString = advanceSearchWebserviceHelper.getResourceWiseFieldSuggestions(resourceCode, field, request.getParameter("tagPrefix"), resourceCodeList);
        return suggestionJSONString;
    }

    @RequestMapping(value = "getResourceWiseFacetsAndFilters/{resourceCode}")
    @ResponseBody
    public HashMap<String, List<NamesIdentifiers>> getResourceWiseFacetsAndFilters(@PathVariable String resourceCode) throws Exception {
        HashMap<String, List<NamesIdentifiers>> resourceWiseFacetsAndFiltersMap = new HashMap<>();

        List<NamesIdentifiers> facetList = advanceSearchWebserviceHelper.getResourceWiseFacets(resourceCode);
        if (facetList != null && !facetList.isEmpty()) {
            resourceWiseFacetsAndFiltersMap.put("facetList", facetList);
        }

        List<NamesIdentifiers> institutionList = advanceSearchWebserviceHelper.getInstitutionList(resourceCode);
        if (institutionList != null && !institutionList.isEmpty()) {
            resourceWiseFacetsAndFiltersMap.put("subResourceIdentifiers", institutionList);
        }

        if (resourceCode.equalsIgnoreCase("VIRM")) {
            List<NamesIdentifiers> museumDateList = advanceSearchWebserviceHelper.getMuseumDateList();
            if (museumDateList != null && !museumDateList.isEmpty()) {
                resourceWiseFacetsAndFiltersMap.put("museumDates", museumDateList);
            }
        }
        return resourceWiseFacetsAndFiltersMap;
    }

    /**
     * Search in advanced
     *
     * @param advanceSearchForm
     * @param request
     * @return
     */
    @RequestMapping(value = "/across/all", method = RequestMethod.GET)
    public ModelAndView advanceSearchResultGet(@ModelAttribute AdvanceSearchForm advanceSearchForm, HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        try {
            Map<String, ResourceType> mapOfResourceType = (Map<String, ResourceType>) request.getSession().getServletContext().getAttribute("resourceTypes");
            ObjectMapper mapperObj = new ObjectMapper();
            mav.addObject("jsonOfResourceType", mapperObj.writeValueAsString(mapOfResourceType));
            mav.addObject("advanceSearchForm", mapperObj.writeValueAsString(advanceSearchForm));
            mav.addObject("searchForm", new SearchResponse());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        mav.addObject("mode", "Sorted");
        mav.addObject("viewAllResourceType", "all");
        mav.addObject("pageNo", 0);
        mav.addObject("dataLimit", 0);
        mav.addObject("resourceLocalisationMap", Helper.resourceTypeInfoByLangAndCode("all", request.getParameter("language"), request).toJSONString());
        mav.addObject("rTIconLocation", resourceTypeIconReadLocation);
        mav.addObject("rTIconDefLocation", resourceTypeIconDefalutReadLocation);
        mav.setViewName("search-result");
        return mav;
    }

    @RequestMapping(value = "/fetch-resource-result/{pageNo}/{dataLimit}", method = RequestMethod.POST)
    @ResponseBody
    public SearchResponse fetchResourceResults(
            @ModelAttribute AdvanceSearchForm advanceSearchForm,
            @PathVariable int pageNo,
            @PathVariable int dataLimit
    ) {
        try {
            SearchResponse results = basicSearchWebserviceHelper.fetchAdvSearchResults(advanceSearchForm, pageNo, dataLimit);
            String advSearchQueryTerm = results.getAdvanceSearchQueryString();
            if (advSearchQueryTerm != null) {
                advSearchQueryTerm = advSearchQueryTerm.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
            }
            results.setSearchElement(advSearchQueryTerm);
            return results;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }
}
