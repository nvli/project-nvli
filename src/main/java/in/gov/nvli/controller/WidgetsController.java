package in.gov.nvli.controller;

import in.gov.nvli.domain.user.UserTheme;
import in.gov.nvli.domain.widget.CalenderEvent;
import in.gov.nvli.domain.widget.Widget;
import in.gov.nvli.service.CalenderEventService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.service.UserThemeService;
import in.gov.nvli.service.WidgetService;
import in.gov.nvli.util.Constants;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller handles various requests related to widgets only.
 *
 * @author Sanjay Rabidas <rabidassanjay@gmail.com>
 */
@Controller
@RequestMapping(value = "/widgets")
public class WidgetsController {

    @Value("${widget.file.location}")
    String widgetFileLocation;

    @Autowired
    WidgetService widgetService;

    @Autowired
    UserService userService;

    @Autowired
    UserThemeService userThemeService;

    @Autowired
    CalenderEventService calenderEventService;

    /**
     * Serves the request to load Widget Manager View to manage various widgets
     * to only {@link Constants.UserRole#ADMIN_USER}
     *
     * @return ModelAndView
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/manager")
    public ModelAndView widgetManager() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        modelAndView.setViewName("WidgetManagerView");
        return modelAndView;
    }

    /**
     * This controller method serves list of all widgets.
     *
     * @return List of {@link Widget}
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Widget> listWidget() {
        return widgetService.listWidgets();
    }

    /**
     * This controller method uploads the Widget File and saves the meta-data
     * from manifest.
     *
     * @param uploadfile
     * @param request
     * @return JSONObject
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject uploadWidget(
            @RequestParam(value = "uploadfile") MultipartFile uploadfile,
            HttpServletRequest request
    ) {
        String fileID = request.getParameter("widgetId");
        JSONObject object = widgetService.uploadWidget(widgetFileLocation, fileID, uploadfile);

        // Check if widget_name is available to be created??
        String widgetName = (String) object.get("widget_name");
        if (widgetService.isWidgetNameAvailable(widgetName)) {
            // If widgetName is available to be created
            widgetService.saveWidgetInfo(object);
        } else {
            JSONObject o = new JSONObject();
            o.put("status", "Failed");
            o.put("message", "Widget is already registered with the same name.");
            return o;
        }

        return object;
    }

    @RequestMapping(value = {"/themeCalWidget"})
    public ModelAndView themeCalWidget() {
        ModelAndView modelAndView = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        breadcrumbMap.put("Manage Themes", "admin/manage/themes");
        modelAndView.addObject("title", "Calender Widgets");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.addObject("user", userService.getCurrentUser());
        List<UserTheme> list = userThemeService.listGlobalThemes();
        modelAndView.addObject("globalThemes", list);
        modelAndView.setViewName("ThemeCalenderWidget");
        return modelAndView;
    }

    @RequestMapping(value = "/get/global-themes")
    @ResponseBody
    public JSONArray getGlobalThemes() {
        return userThemeService.getGlobalThemes();
    }

    @RequestMapping(value = {"/createCalenderEvent"})
    public ModelAndView calenderEvent() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", userService.getCurrentUser());
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Admin Activity", "#");
        breadcrumbMap.put("Manage Themes", "admin/manage/themes");
        breadcrumbMap.put("Calender Widgets", "/widgets/themeCalWidget");
        modelAndView.addObject("title", "Create Calender Events");
        modelAndView.addObject("breadcrumbMap", breadcrumbMap);
        modelAndView.setViewName("CalenderEvent");
        return modelAndView;
    }

    @RequestMapping(value = "/get/calenderEventList")
    @ResponseBody
    public JSONArray calenderEventList() {
        JSONArray array = new JSONArray();
        array.addAll(userThemeService.getGlobalThemes());
        array.addAll(calenderEventService.getCalenderEventList());
        return array;
    }

    @RequestMapping(value = "/addNewEvent", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject addNewEvent(
            @RequestParam(value = "title") String title,
            @RequestParam(value = "description") String discription,
            @RequestParam(value = "startDate") Date startDate,
            @RequestParam(value = "endDate") Date endDate,
            HttpServletRequest request
    ) {
        CalenderEvent calEvent = new CalenderEvent();
        calEvent.setTitle(title);
        calEvent.setDescription(discription);
        calEvent.setStartDate(startDate);
        calEvent.setEndDate(endDate);
        calEvent.setCreatedBy(userService.getCurrentUser().getId());
        widgetService.addCalenderEvent(calEvent);
        JSONObject o = new JSONObject();
        o.put("status", Constants.AjaxResponseCode.SUCCESS);
        return o;
    }

    @RequestMapping(value = "/getCalenderEventsByFiltersWithLimit", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject getCalenderEventsByFiltersWithLimit(@RequestParam String searchString, @RequestParam int dataLimit, @RequestParam int pageNo) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("eventList", calenderEventService.getcalenderEventByFiltersWithLimit(searchString, dataLimit, pageNo));
            jsonObject.put("totalCount", calenderEventService.getEventsTotalCountByFilters(searchString));
            return jsonObject;
        } catch (Exception ex) {
            return null;
        }
    }

    @RequestMapping(value = "/save/cal/event", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject editCalenderEvent(
            @RequestParam(value = "eventId") Long eventId,
            @RequestParam(value = "title") String title,
            @RequestParam(value = "description") String description,
            @RequestParam(value = "startDate") Date startDate,
            @RequestParam(value = "endDate") Date endDate
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            CalenderEvent calEvent = calenderEventService.getEventByEventId(eventId);
            calEvent.setTitle(title);
            calEvent.setDescription(description);
            calEvent.setStartDate(startDate);
            calEvent.setEndDate(endDate);
            calEvent.setCreatedBy(userService.getCurrentUser().getId());
            calenderEventService.updateCalenderEvent(calEvent);
            jsonObject.put("status", Constants.AjaxResponseCode.SUCCESS);
            jsonObject.put("data", calEvent);
            return jsonObject;

        } catch (Exception ex) {
            jsonObject.put("status", Constants.AjaxResponseCode.ERROR);
            return jsonObject;
        }
    }

    @RequestMapping(value = "/deleteCalenderEvent", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject deleteCalenderEvent(@RequestParam Long eventId) {
        JSONObject jsonObject = new JSONObject();
        try {
            CalenderEvent calEvent = calenderEventService.getEventByEventId(eventId);

            calenderEventService.removeCalenderEvent(calEvent);
            jsonObject.put("status", Constants.AjaxResponseCode.SUCCESS);
            return jsonObject;
        } catch (Exception ex) {
            jsonObject.put("status", Constants.AjaxResponseCode.ERROR);
            return jsonObject;
        }
    }
}
