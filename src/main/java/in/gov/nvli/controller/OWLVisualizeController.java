package in.gov.nvli.controller;

import in.gov.nvli.beans.ontology.DomainOntology;
import in.gov.nvli.beans.ontology.MultipleBookmarkForm;
import in.gov.nvli.beans.ontology.Organization;
import in.gov.nvli.beans.ontology.UserDefinedCustomLabelDto;
import in.gov.nvli.dao.crowdsource.ILanguageDAO;
import in.gov.nvli.service.OWLVisualizeService;
import in.gov.nvli.service.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller handles request for owl visualization.
 * <li>/visualize</li>
 *
 * @author Hemant Anjana
 * @see OWLVisualizeService
 * @
 */
@Controller
@RequestMapping("/visualize")
public class OWLVisualizeController {

    /**
     * instance of OWLVisualizeService.
     */
    @Autowired
    private OWLVisualizeService owlVisualizeService;

    /**
     * The name of the property languageDAO used to holds {@link ILanguageDAO}
     * object reference.
     */
    @Autowired
    public ILanguageDAO languageDAO;

    @Autowired
    UserService userService;

    /**
     * method handles request for owl.
     * <li>/visualize/owl</li>
     *
     * @return model and view of owl pages.
     */
    @RequestMapping(value = "/owl", method = RequestMethod.GET)
    public ModelAndView getOWL() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("user", userService.getCurrentUser());
        mav.setViewName("ontologyDefault");
        return mav;
    }

    /**
     * To get Ontology and UDC Default page.
     * <li>/visualize/owl-ontology/{ontId}</li>
     *
     * @param ontId
     * @return
     */
    @RequestMapping(value = "/owl-ontology/{ontId}", method = RequestMethod.GET)
    public ModelAndView getOWLOntology(@PathVariable String ontId) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("selectedOnt", ontId);
        mav.addObject("user", userService.getCurrentUser());
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>> " + userService.getCurrentUser());
        mav.addObject("domainOnts", owlVisualizeService.getDomainOntologies());
        mav.setViewName("owlDefaultLayout");
        return mav;
    }

    /**
     * To get Authors page for Ontology
     * * <li>/visualize/owl-authers</li>
     *
     * @return
     */
    @RequestMapping(value = "/owl-authors", method = RequestMethod.GET)
    public ModelAndView getOWLAuthers() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("resourceType", owlVisualizeService.getResourceType());
        //mav.addObject("publishers", owlVisualizeService.getPublisher());
        mav.setViewName("owlAutherDefault");
        return mav;
    }

    /**
     * returns list of Publishers
     *
     * @param subStr
     * @param length
     * @return
     */
    @RequestMapping(value = "/getPublishers/{length}", method = RequestMethod.GET)
    @ResponseBody
    public String getPublishers(@RequestParam String subStr, @PathVariable int length) {
        return owlVisualizeService.getPublisher(subStr, length).toString();
    }

    /**
     * method handles request for records of given ontology class.
     * <li>/visualize/getjsonforjs/{pageNum}/{pageWin}</li>.
     *
     * @param term Class IRI or class name which will be used for getting
     * records.
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @return
     */
    @RequestMapping(value = "/getjsonforjs/{pageNum}/{pageWin}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getReponseFromWS(@RequestParam("q") String term, @PathVariable Integer pageNum, @PathVariable Integer pageWin, @RequestParam("refOntId") String refOntId) {
        return owlVisualizeService.wsEndPointForRecord(term, pageNum, pageWin, refOntId);
    }

    /**
     * method handles request for records count of given ontology class.
     * <li>/visualize/getjsonforjsCount</li>.
     *
     * @param term Class IRI or class name which will be used for getting record
     * counts.
     * @param refOntId
     * @return number of record count for given class name.
     */
    @RequestMapping(value = "/getjsonforjsCount", method = RequestMethod.GET)
    @ResponseBody
    public Integer getReponseCountFromWS(@RequestParam("q") String term, @RequestParam("refOntId") String refOntId) {
        return owlVisualizeService.wsEndPointForCount(term, refOntId);
    }

    @RequestMapping(value = "/getLanguageCode/{languageCode}", method = RequestMethod.GET)
    @ResponseBody
    public String getLanguageCode(@PathVariable String languageCode) {
        return languageDAO.getObjectByCode(languageCode).getId() + "";
    }

    /**
     * Handles requests identified by <u>/getjsonforDomainOnts</u>
     * Returns the list of all Domain Ontology for listing on main page.
     * <li>/visualize/getjsonforDomainOnts</li>.
     *
     * @return
     */
    @RequestMapping(value = "/getjsonforDomainOnts", method = RequestMethod.GET)
    @ResponseBody
    public List<DomainOntology> getDomainOnts() {
        return owlVisualizeService.getDomainOntologies();
    }

    /**
     *
     * Returns count of Authors depends on filter criteria like Resource Type,
     * Organization, Publisher, Alfabetical Sorting or Search On Author name.
     *
     * <li>/visualize/getcountforallauthors</li>
     *
     * @param resType
     * @param organization
     * @param publisher
     * @param auther
     * @param strAlfa
     * @return
     */
    @RequestMapping(value = "/getcountforallauthors", method = RequestMethod.GET)
    @ResponseBody
    public int getAllAuthorCount(@RequestParam("resType") String resType, @RequestParam("organization") String organization, @RequestParam String publisher, @RequestParam String auther, @RequestParam String strAlfa) {
        return owlVisualizeService.getCountAllAuthor(resType, organization, publisher, auther, strAlfa);
    }

    /**
     *
     * Returns list of Authors depends on filter criteria like Resource Type,
     * Organization, Publisher, Alfabetical Sorting or Search On Author name.
     *
     * <li>/visualize/getjsonforallauthors/{pageNum}/{pageWin}</li>
     *
     * @param resType
     * @param organization
     * @param publisher
     * @param strAlfa
     * @param srchAuthor
     * @param pageNum
     * @param pageWin
     * @return
     */
    @RequestMapping(value = "/getjsonforallauthors/{pageNum}/{pageWin}", method = RequestMethod.GET)
    @ResponseBody
    public String getAllAuthors(@RequestParam("resType") String resType, @RequestParam("organization") String organization, @RequestParam String publisher, @RequestParam String strAlfa, @RequestParam String srchAuthor, @PathVariable("pageNum") Integer pageNum, @PathVariable("pageWin") Integer pageWin) {
        return owlVisualizeService.getAllAuthors(resType, organization, publisher, strAlfa, srchAuthor, (pageNum - 1) * pageWin, pageWin);
    }

    /**
     * Returns list of Co-Authors related to selected Author.
     *
     * <li>/visualize/getjsonforcoauthors/{pageNum}/{pageWin}</li>
     *
     * @param authorIRI
     * @param pageNum
     * @param pageWin
     * @return
     */
    @RequestMapping(value = "/getjsonforcoauthors/{pageNum}/{pageWin}", method = RequestMethod.GET)
    @ResponseBody
    public String getCoAuthors(@RequestParam("authorIRI") String authorIRI, @PathVariable("pageNum") Integer pageNum, @PathVariable("pageWin") Integer pageWin) {
        return owlVisualizeService.getCoAuthors(authorIRI, (pageNum - 1) * pageWin, pageWin);
    }

    /**
     * Returns count of Records related to selected Co-Authors.
     *
     * <li>/visualize/getrecordcountforauthor</li>
     *
     * @param authorIRI
     * @param organizationIRI
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getrecordcountforauthor", method = RequestMethod.GET)
    public int getRecordCountForAuthors(@RequestParam ArrayList<String> authorIRI, @RequestParam("organizationIRI") String organizationIRI) {
        return owlVisualizeService.getCountOfRecordsForAuthor(authorIRI, organizationIRI);
    }

    /**
     *
     * Returns list of Records related to selected Co-Authors.
     *
     * <li>/visualize/getrecordsforauthor/{pageNum}/{pageWin}</li>
     *
     * @param authorIRI
     * @param organizationIRI
     * @param pageNum
     * @param pageWin
     * @return
     */
    @RequestMapping(value = "/getrecordsforauthor/{pageNum}/{pageWin}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getRecordsForAuthors(@RequestParam ArrayList<String> authorIRI, @RequestParam String organizationIRI, @PathVariable Integer pageNum, @PathVariable Integer pageWin) {
        return owlVisualizeService.getRecordsForAuthor(authorIRI, organizationIRI, (pageNum - 1) * pageWin, pageWin);
    }

    /**
     * Returns list of Organizations related to Resource Type.
     *
     * <li>/visualize/orglist/{resType}</li>
     *
     * @param resType
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orglist/{resType}", method = {RequestMethod.GET, RequestMethod.POST})
    public List<Organization> getOrgList(@PathVariable String resType) {
        return owlVisualizeService.getOrganizationsForResource(resType);
    }

    /**
     * Returns list of Authors for Suggestions for Searching.
     *
     * <li>/visualize/getsuggestions/{length}</li>
     *
     * @param subStr
     * @param length
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getsuggestions/{length}", method = {RequestMethod.GET})
    public String getSuggestions(@RequestParam String subStr, @PathVariable int length) {
        return owlVisualizeService.getSuggestions(subStr, length).toString();
    }

    @ResponseBody
    @RequestMapping(value = "/labels/{userId}", method = {RequestMethod.GET})
    public List<UserDefinedCustomLabelDto> getCustomTagForClassByUser(@PathVariable Long userId, @RequestParam Long refOntId, @RequestParam String className) {
        return owlVisualizeService.getCustomLabelForRefClassByUser(userId, refOntId, className);
    }

    @ResponseBody
    @RequestMapping(value = "/label", method = {RequestMethod.POST})
    public List<UserDefinedCustomLabelDto> postCustomTagForClassByUser(@RequestParam Long userId, @RequestParam Long refOntClassId, @RequestParam String customLabel, @RequestParam Long ontID, @RequestParam String className, @RequestParam String lanuageCode) {
        List<UserDefinedCustomLabelDto> lstCustomLabelDtos = new ArrayList<>();
        UserDefinedCustomLabelDto userDefinedCustomLabelDto = new UserDefinedCustomLabelDto(); //userId, className, refOntClassId, customLabel, lanuageCode
        userDefinedCustomLabelDto.setUserId(userId);
        userDefinedCustomLabelDto.setClassName(className);
        userDefinedCustomLabelDto.setRefOntClassId(refOntClassId);
        userDefinedCustomLabelDto.setCustomLabel(customLabel);
        userDefinedCustomLabelDto.setOntologyFileId(ontID);
        lstCustomLabelDtos.add(userDefinedCustomLabelDto);
        userDefinedCustomLabelDto.toString();
        return owlVisualizeService.postCustomClasLbl(lstCustomLabelDtos);
    }

    /*public static void main(String[] args) {
        String test = "a+++b   c    d";
        System.out.println(test.replaceAll("\\++", "_"));
    }*/
    @ResponseBody
    @RequestMapping(value = "/bookmarks/multi", method = {RequestMethod.POST})
    public ResponseEntity saveTaggedRecordsWithLbl(@RequestParam Long userId, @RequestParam List<Long> custLblIds, @RequestParam List<String> recordIds) {
        MultipleBookmarkForm bookmarkForm = new MultipleBookmarkForm();
        bookmarkForm.setUserId(userId);
        bookmarkForm.setCustomLabelId(custLblIds);
        bookmarkForm.setRecordIdentifier(recordIds);
        return owlVisualizeService.postCustomLblWithRecord(bookmarkForm);
    }

    @RequestMapping(value = "/get_tagged_record_count", method = RequestMethod.GET)
    @ResponseBody
    public Long getTaggedRecordCountForCustomLbl(@RequestParam Long userID, @RequestParam List<Long> custIds) {
        return owlVisualizeService.getRecordCountForCustomLbl(userID, custIds);
    }

    @ResponseBody
    @RequestMapping(value = "/get_tagged_records/{pageNo}/{pageWindow}", method = {RequestMethod.GET})
    public Map<String, String> getTaggedRecordsForCustomLbl(@PathVariable int pageNo, @PathVariable int pageWindow, @RequestParam Long userID, @RequestParam List<Long> custIds) {
        return owlVisualizeService.getRecordsForCustomLbl(userID, custIds, pageNo, pageWindow);
    }

    @ResponseBody
    @RequestMapping(value = "/get_my_tagged_class_list/{userId}/{domainId}", method = {RequestMethod.GET})
    public List<String> getMyTaggedClassList(@PathVariable Long userId, @PathVariable Long domainId) {
        return owlVisualizeService.getMyTaggedClassList(userId, domainId);
    }

    @ResponseBody
    @RequestMapping(value = "/delete_my_custom_balel/{userId}/{lblId}", method = {RequestMethod.DELETE})
    public boolean deleteMyCustomLbl(@PathVariable Long userId, @PathVariable Long lblId) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>DLETE");
        return owlVisualizeService.deleteMyCustomLbl(userId, lblId);
    }

}
