package in.gov.nvli.controller;

import in.gov.nvli.custom.user.CustomUserSavedList;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.Notifications;
import in.gov.nvli.mongodb.domain.ReviewAndRating;
import in.gov.nvli.mongodb.domain.UserAddedList;
import in.gov.nvli.mongodb.domain.UserSubscribedList;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.mongodb.service.NotificationService;
import in.gov.nvli.mongodb.service.ReviewAndRatingService;
import in.gov.nvli.mongodb.service.UserAddedListService;
import in.gov.nvli.mongodb.service.UserSubscribedListService;
import in.gov.nvli.rabbitmq.RabbitmqProducer;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.service.NotificationSubscriptionService;
import in.gov.nvli.service.RewardsService;
import in.gov.nvli.service.SendMailService;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Constants.UserActivityConstant;
import in.gov.nvli.util.Constants.UserRole;
import in.gov.nvli.webserviceclient.crowdsource.CrowdsourceWebserviceHelper;
import in.gov.nvli.webserviceclient.crowdsource.TagStandardResponse;
import in.gov.nvli.webserviceclient.user.UserSearchResultActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * UserActivityController used call user activity related service methods
 * functionalities like user invitation, user login, record
 * like,share,review,rating etc.
 *
 * <li> User Invitation<li>
 * <li> User Login<li>
 * <li> like <li>
 * <li> rating<li>
 * <li>review<li>
 * <li>NVLI Share<li>
 * <li>Crowd source <li>
 *
 * @author Bhumika
 * @author Darshana
 * @version 1
 * @since 1
 * @author Ruturaj Powar<ruturajp@cdac.in>
 * @author Vivek Bugale<bvivek@cdac.in>
 */
@Controller
@RequestMapping(value = "search/user")
public class UserActivityController {

    /**
     * The name of the property LOG used to holds final Logger object of
     * UserActivityController class.
     */
    private static final Logger LOG = Logger.getLogger(UserActivityController.class);
    /**
     * The name of the property activityServiceObj used to holds
     * UserActivityService object
     */
    @Autowired
    private UserActivityService activityServiceObj;
    /**
     * The name of the property userService used to holds UserService object
     */
    @Autowired
    private UserService userService;
    /**
     * The name of the property ctx used to holds ApplicationContext object
     * reference
     */
    @Autowired
    private ApplicationContext ctx;
    /**
     * The name of the property messages used to holds MessageSource object
     */
    @Autowired
    private MessageSource messages;
    /**
     * The name of the property crowdSourcingService used to hold
     * CrowdSourcingService object
     */
    @Autowired
    private CrowdSourcingService crowdSourcingService;
    /**
     * The name of the property crowdsourceWebserviceHelper used to hold
     * CrowdsourceWebserviceHelper object
     */
    @Autowired
    private CrowdsourceWebserviceHelper crowdsourceWebserviceHelper;
    /**
     * The name of the property mailService used to hold SendMailService object
     */
    @Autowired
    private SendMailService mailService;
    /**
     * The name of the property notificationService used to hold
     * NotificationService object
     */
    @Autowired
    private NotificationService notificationService;
    /**
     * Creates an Executor that uses a single worker thread operating off an
     * unbounded queue.
     */
    final static ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NotificationSubscriptionService notificationSubscriptionService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    /**
     * The name of the property mitWSBaseURL used to holds web-service base URL.
     */
    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;
    @Autowired
    private ReviewAndRatingService userReviewAndRatingService;

    /**
     * service object to del with activities of user in mongodb
     */
    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    UserAddedListService userAddedListService;

    @Autowired
    UserSubscribedListService userSubscribedListService;

    @Autowired
    private RewardsService rewardsService;

    /**
     * @author Ruturaj Method to get receivers to whom notifications to be send.
     * This method get called from SaveNotifications() method.
     * @param message--like activity type
     * @param id--id of logged in user
     * @param uniqueObjectIdentifier-identifire of record
     * @return List of affected users ids.
     */
    public List<Long> getNotificationReceivers(String message, Long id, String uniqueObjectIdentifier) {
        List<Long> receivers = new ArrayList<>();
        List<ReviewAndRating> userLikeReviewRatings = userReviewAndRatingService.getReviewByRecordIdentifier(uniqueObjectIdentifier);
        userLikeReviewRatings.stream().filter(userLikeReviewRating -> userLikeReviewRating.getUserId() != id).
                forEach(userLikeReviewRating -> receivers.add(userLikeReviewRating.getUserId()));
        return receivers;
    }

    /**
     * @author Ruturaj Ajax method to send notifications to users who have
     * performed same activity which currently logged in user is performing.
     * @param uniqueObjectIdentifier-identifire of record
     * @param notificationActivityType--activity type
     * @param message--like type of activity, just used to get message from
     * properties file.
     * @return success meg
     */
    @RequestMapping(value = "/notification/{uniqueObjectIdentifier}/{notificationActivityType}/{message}")
    @ResponseBody
    public String SaveNotifications(
            @PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier,
            @PathVariable(value = "notificationActivityType") String notificationActivityType,
            @PathVariable(value = "message") String message) {
        User userObj = userService.getCurrentUser();
        if (userObj != null) {
            String title = uniqueObjectIdentifier;
            try {
                title = UserSearchResultActivity.
                        getRecordsTitle(Arrays.asList(uniqueObjectIdentifier)).
                        get(uniqueObjectIdentifier);
            } catch (Exception ex) {
                LOG.error(ex.getMessage(), ex);
            }
            Notifications notif = new Notifications();
            notif.setNotifactionDateTime(new Date());
            notif.setNotificationTargetURL("/search/preview/" + uniqueObjectIdentifier);
            notif.setNotificationMessage(messages.
                    getMessage("notif." + message, new String[]{userObj.getFirstName() + " "
                + userObj.getLastName(), title}, Locale.getDefault()));
            notif.setSenderId(userObj.getId());
            notif.setNotificationActivityType(notificationActivityType);
            notificationService.saveNotification(notif, getNotificationReceivers(message, userObj.getId(), uniqueObjectIdentifier));
        }
        return "success";
    }

    /**
     * like method is used to maintain likes on records by users and also for
     * notifications on liked records
     *
     * @param request {@link HttpServletRequest}
     * @param uniqueObjectIdentifier record identifier
     * @param isUserLiked
     * @return like count as long
     */
    @RequestMapping(value = "/like/{uniqueObjectIdentifier}/{isUserLiked}/{recordType}")
    @ResponseBody
    public JSONObject like(HttpServletRequest request, @PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier,
            @PathVariable(value = "isUserLiked") boolean isUserDisliked, @PathVariable(value = "recordType") String recordType) {
        JSONObject jSONObject = new JSONObject();
        try {
            User userObj = userService.getCurrentUser();
            if (userObj != null) {
                LOG.info("User " + userObj.getId() + " liked  record " + uniqueObjectIdentifier);
                //for removing like
                if (isUserDisliked) {
                    activityServiceObj.saveLikesInActivityLog(Constants.UserActivityConstant.REMOVE_LIKE, uniqueObjectIdentifier, userObj.getId(), request, userObj.getUsername(), recordType);
                    jSONObject.put("notificationActivityType", UserActivityConstant.REMOVE_LIKE);
                    jSONObject.put("message", "disliked");
                } //for first time like the record
                else {
                    activityServiceObj.saveLikesInActivityLog(Constants.UserActivityConstant.LIKE_RECORD, uniqueObjectIdentifier, userObj.getId(), request, userObj.getUsername(), recordType);
                    jSONObject.put("notificationActivityType", UserActivityConstant.LIKE_RECORD);
                    jSONObject.put("message", "liked");
                }
                jSONObject.put("uniqueObjectIdentifier", uniqueObjectIdentifier);
                activityServiceObj.saveLikesOfUser(uniqueObjectIdentifier, userObj.getId(), request, userObj.getUsername());
                UserSearchResultActivity.likeRecord(uniqueObjectIdentifier, !isUserDisliked);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage(), ex);
        }
        return jSONObject;
    }

    /**
     * socialShare method is used to maintain share on social sites
     *
     * @param request {@link HttpServletRequest}
     * @param uniqueObjectIdentifier record identifier
     * @param providerId
     * @return shareCount as long
     */
    @RequestMapping(value = "/share/{provider}/{uniqueObjectIdentifier}/{recordType}")
    @ResponseBody
    public long socialShare(HttpServletRequest request,
            @PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier,
            @PathVariable(value = "provider") String providerId, @PathVariable(value = "recordType") String recordType) {
        long shareCount = 0;
        try {
            User userObj = userService.getCurrentUser();
            if (userObj != null) {
                LOG.info("User " + userObj.getId() + " shared record " + uniqueObjectIdentifier);
                if (activityServiceObj.saveSocialShareOfUser(Constants.UserActivityConstant.SOCIAL_SHARE_RECORD, uniqueObjectIdentifier, userObj.getId(), request, providerId, userObj.getUsername(), recordType)) {
                    shareCount = UserSearchResultActivity.shareRecord(uniqueObjectIdentifier);
                    LOG.info(" share count " + shareCount);
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return shareCount;
    }

    /**
     * userRating method is used to maintain the rating of users on records
     *
     * @param request {@link HttpServletRequest}
     * @param uniqueObjectIdentifier record identifier
     * @param avgRecRating number of rating on record
     * @param rating
     * @param numberOfRatings
     * @param recordType
     * @return JSONObject
     */
    @RequestMapping(value = "/rating/{uniqueObjectIdentifier}/{avgRecRating}/{rating}/{numberOfRatings}/{recordType}")
    @ResponseBody
    public JSONObject userRating(HttpServletRequest request,
            @PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier,
            @PathVariable(value = "avgRecRating") float avgRecRating,
            @PathVariable(value = "rating") int rating,
            @PathVariable("numberOfRatings") int numberOfRatings,
            @PathVariable(value = "recordType") String recordType) {
        JSONObject jSONObject = new JSONObject();
        try {
            User userObj = userService.getCurrentUser();
            if (userObj != null) {
                LOG.info("User " + userObj.getId() + " rated " + rating + "on record " + uniqueObjectIdentifier);
                long ratingCount = activityServiceObj.saveRatingOfUser(uniqueObjectIdentifier, userObj.getId(), request, rating, userObj.getUsername(), avgRecRating, numberOfRatings, recordType);
                jSONObject.put("notificationActivityType", Constants.UserActivityConstant.RATE_RECORD);
                jSONObject.put("uniqueObjectIdentifier", uniqueObjectIdentifier);
                jSONObject.put("message", "rated");
                jSONObject.put("ratingCount", ratingCount);
            }
        } catch (Exception ex) {
            //this is to identify the error occured during web service call
            jSONObject.put("ratingCount", -2);
            LOG.error(ex.getMessage(), ex);
        }
        return jSONObject;
    }

    /**
     * userReview method is used to maintain users reviews on records and send
     * notification to respective users if reviewed users exist
     *
     * @param request {@link HttpServletRequest}
     * @return reviewCount as long
     */
    @RequestMapping(value = "/review")
    @ResponseBody
    public JSONObject userReview(HttpServletRequest request) {
        JSONObject jSONObject = new JSONObject();
        try {
            String uniqueObjectIdentifier = request.getParameter("recordIdentifier");
            String userReview = request.getParameter("userReview");
            String recordType = request.getParameter("recordType");

            final User userObj = userService.getCurrentUser();
            if (userObj != null) {
                //reviewCount 0--> error , greater than 0 --> Review added,-1--->review upadated
                long reviewCount = activityServiceObj.saveReviewOfUser(uniqueObjectIdentifier, userObj.getId(), request, userReview, userObj.getUsername(), recordType);
                if (reviewCount == 0) {
                    throw new Exception("Error updating review count for userId " + userObj.getId());
                }
                jSONObject.put("notificationActivityType", Constants.UserActivityConstant.REVIEW);
                jSONObject.put("uniqueObjectIdentifier", uniqueObjectIdentifier);
                jSONObject.put("message", "review");
                jSONObject.put("reviewCount", reviewCount);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            //for any errors
            jSONObject.put("reviewCount", -2);
            return jSONObject;
        }
        return jSONObject;
    }

    /**
     * The method getSavedListOfAUser used to get saved list of users
     *
     * @return list of users
     */
    @RequestMapping(value = "/get/saved/lists")
    @ResponseBody
    public List<CustomUserSavedList> getSavedListOfAUser() {
        List<CustomUserSavedList> customUserSavedLists = new ArrayList<>();
        try {
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {

                List<UserAddedList> userAddedLists = userAddedListService.gelAllSavedListOfUser(loggedUser.getId());
                userAddedLists.forEach(list -> {
                    CustomUserSavedList obj = new CustomUserSavedList();
                    String listVisibility = "";
                    obj.setListId((list.getListName()));
                    obj.setListName(list.getListName());
                    if (list.getPublicPrivate() == Constants.PRIVATE_LIST_ID) {
                        listVisibility = Constants.PRIVATE_LIST_LABEL;
                    } else if (list.getPublicPrivate() == Constants.PUBLIC_LIST_ID) {
                        listVisibility = Constants.PUBLIC_LIST_LABEL;
                    }
                    obj.setListVisibility(listVisibility);
                    customUserSavedLists.add(obj);
                    obj = null;
                });
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return customUserSavedLists;
    }

    /**
     * This controller method adds searched records to an existing list selected
     * by the user.
     *
     * @param request
     * @param uniqueObjectIdentifier
     * @param recordTitle
     * @param listName
     * @param recordType
     * @return JSONObject
     */
    @RequestMapping(value = "/add/to/existing/list/{uniqueObjectIdentifier}/{recordTitle}/{listName}/{recordType}")
    @ResponseBody
    public JSONObject addToExisitngUserList(HttpServletRequest request,
            @PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier,
            @PathVariable(value = "recordTitle") String recordTitle,
            @PathVariable(value = "listName") String listName,
            @PathVariable(value = "recordType") String recordType) {
        JSONObject jSONObject = new JSONObject();
        try {
            //0 means error;1 means success and 2 means already exists in list
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                if (!listName.isEmpty()) {
                    activityServiceObj.saveToExistingList(UserActivityConstant.ADD_TO_LIST, uniqueObjectIdentifier, recordTitle, loggedUser, request, listName, recordType);
                } else {
                    jSONObject.put("key", 0);
                    jSONObject.put("value", "List Name Should not be Empty");
                    return jSONObject;
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            jSONObject.put("key", 1);
            jSONObject.put("value", "Record Already in List.");
            return jSONObject;
        }
        jSONObject.put("key", 1);
        jSONObject.put("value", "Added to list " + listName + " successfully");
        return jSONObject;
    }

    /**
     * This controller method updates the the privacy, name of existing lists of
     * favorite records
     *
     * @param request
     * @param originalListName
     * @param listName
     * @return
     */
    @RequestMapping(value = "/update/list/{originalListName}/{listName}", method = RequestMethod.GET)
    @ResponseBody
    public long updateExisitngUserList(
            HttpServletRequest request,
            @PathVariable(value = "originalListName") String originalListName,
            @PathVariable(value = "listName") String listName
    ) {
        try {//0 means error;1 means success and 2 means already exists in list

            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                UserAddedList userAddedList = userAddedListService.getListByUserIdAndListName(loggedUser.getId(), originalListName);
                if (!userAddedListService.IsListPresentByUserIDListName(loggedUser.getId(), listName)) {
                    //maintain activity logs
                    userAddedList.setListName(originalListName);
                    userAddedList.setPublicPrivate(userAddedList.getPublicPrivate());
                    userAddedList.setUpdatedListName(listName);
                    activityLogService.saveLogOfUpdateList(loggedUser, userAddedList, request);
                    //perform activity
                    userAddedList.setListName(listName);
                    userAddedList.setPublicPrivate(userAddedList.getPublicPrivate());
                    userAddedListService.updateUserAddedList(originalListName, userAddedList);
                } else {
                    return 2;
                }
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return 0;
        }
        return 1;
    }

    /**
     * This controller method adds favorite record to newly created list
     *
     * @param request
     * @param uniqueObjectIdentifier
     * @param recordTitle
     * @param listName
     * @param listAccessType
     * @param recordType
     * @return long
     */
    @RequestMapping(value = "/add/to/new/list/{uniqueObjectIdentifier}/{recordTitle}/{listName}/{listAccessType}/{recordType}")
    @ResponseBody
    public long addToNewlyCreatedUserList(HttpServletRequest request,
            @PathVariable(value = "uniqueObjectIdentifier") String uniqueObjectIdentifier,
            @PathVariable(value = "recordTitle") String recordTitle,
            @PathVariable(value = "listName") String listName,
            @PathVariable(value = "listAccessType") int listAccessType,
            @PathVariable(value = "recordType") String recordType) {
        try {
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                if (listName != null && !listName.isEmpty()) {//by default access type of list will be private i.e 1 value
                    if (activityServiceObj.saveToNewlyCreatedList(UserActivityConstant.CREATE_LIST, uniqueObjectIdentifier, recordTitle, loggedUser, request, listName, listAccessType, recordType)) {
                        return 1;
                    } else {
                        return 2;
                    }
                }

            } else {
                return 0;
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return 1;
    }

    /**
     * This controller method handles the request to save tags provided by the
     * user to a particular record, as well as logs this user activity
     *
     * @param request
     * @return boolean
     */
    @RequestMapping(value = "/saveTags")
    @ResponseBody
    public boolean saveTags(HttpServletRequest request) {
        try {
            String recordIdentifier = request.getParameter("recordIdentifier").trim();
            String customTags = request.getParameter("customTags").trim();
            CrowdSourceCustomTag crowdSourceCustomTag = crowdSourcingService.getCrowdSourceCustomTag(recordIdentifier, userService.getCurrentUser());
            if (crowdSourceCustomTag == null) {
                crowdSourceCustomTag = new CrowdSourceCustomTag();
                crowdSourceCustomTag.setRecordIdentifier(recordIdentifier);
                TagStandardResponse tagStandardResponse = crowdsourceWebserviceHelper.getTagDetails(recordIdentifier, Constants.TagType.CUSTOM);
                if (tagStandardResponse.getTagValue() != null && !tagStandardResponse.getTagValue().toString().isEmpty()) {
                    crowdSourceCustomTag.setTags(tagStandardResponse.getTagValue().toString());
                }
            }
            crowdSourceCustomTag.setUser(userService.getCurrentUser());
            crowdSourceCustomTag = crowdSourcingService.saveOrUpdateCrowdSourceCustomTag(crowdSourceCustomTag, customTags);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(customTags, customTags.substring(0, customTags.lastIndexOf("_")));
            rewardsService.rewardsPointsForTags(jSONObject.toString(), crowdSourceCustomTag.getId(), crowdSourceCustomTag.getRecordIdentifier(), request, "custom",userService.getCurrentUser());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("sjkakdakds");
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    /**
     * This method is used to send invitation using email address and store the
     * user invitation details in activity log.
     *
     * @param request {@link HttpServletRequest}
     * @return success or error as a String
     */
    @RequestMapping(value = "/invite/friends")
    @ResponseBody
    public String inviteFriends(HttpServletRequest request) {
        try {
            final User loggedUser = userService.getCurrentUser();
            final String servletCtx = request.getServletContext().getContextPath();
            String emailIds = request.getParameter("emailIds").trim();
            if (loggedUser != null) {
                List<String> emailList = Arrays.asList(emailIds.split(";"));
                emailList.stream().map((userMail) -> userMail).forEachOrdered((String userEmail) -> {

                    Future<?> future = EXECUTOR_SERVICE.submit(() -> {
                        try {
                            List<Long> invited_roleid = new ArrayList<>();
                            invited_roleid.add(UserRole.GENERAL_USER_ID);
                            userService.inviteUser(Constants.UserActivities.INVITE_USER,
                                    null, loggedUser, null, invited_roleid, userEmail, ctx, request, servletCtx, false, null);
                        } catch (Exception e) {
                            LOG.error(e.getMessage(), e);
                        }
                    });
                });
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return "error";
        }
        return "success";
    }

    /**
     * This method is used to share the records within NVLI users * and store
     * the share details in activity log.
     *
     * @param request {@link HttpServletRequest}
     * @return success or failed as a String
     */
    @RequestMapping(value = "/nvli/share")
    @ResponseBody
    public String shareWithNvliFriends(HttpServletRequest request) {
        String returnString = "";
        try {
            String title = "";
            long sharCount;
            List<Long> IdList = new ArrayList<>();
            User loggedUser = userService.getCurrentUser();
            if (loggedUser != null) {
                String userIds = request.getParameter("emailId").trim();
                for (String s : userIds.split(",")) {
                    IdList.add(Long.parseLong(s));
                }
                String userMsg = request.getParameter("userMsg").trim();
                String recordIdentifier = request.getParameter("recordIdentifier");
                String imageUrl = request.getParameter("imageUrl");
                String recordType = request.getParameter("recordType");
                List<String> emailList = activityServiceObj.getEmailList(IdList);
                if (!emailList.isEmpty()) {
                    for (String emailId : emailList) {
                        try {
                            title = UserSearchResultActivity.getRecordsTitle(Arrays.asList(recordIdentifier)).get(recordIdentifier);
                        } catch (Exception ex) {
                            LOG.error(ex.getMessage(), ex);
                        }
                        String servletCtx = request.getServletContext().getContextPath();
                        User sharedWithUser = userService.getUserByEmailID(emailId);
                        if (sharedWithUser != null) {
                            if (activityServiceObj.saveNvliShareOfUser(Constants.UserActivityConstant.INTRA_SHARE_RECORD,
                                    recordIdentifier, loggedUser.getId(), request,
                                    sharedWithUser.getId(), userMsg, loggedUser.getUsername(), recordType)) {
                                sharCount = UserSearchResultActivity.shareRecord(recordIdentifier);//save NVLI share count through the MIT service.
                                String sharedUrl = "/search/preview/" + recordIdentifier;
                                String message = ctx.getMessage("link.share.message", new Object[]{userMsg}, Locale.getDefault());
                                String subject = ctx.getMessage("link.share.subject", null, Locale.getDefault());
//                                    if (notificationSubscriptionService.checkNvliShareLinkStatus(loggedUser.getId(), "WIthEmail")) {
                                // mailService.sendMail(sharedWithUser.getFirstName(), sharedWithUser.getSalt(), servletCtx, sharedWithUser.getEmail(), message, sharedUrl, subject);
                                User user = userService.getCurrentUser();
                                RabbitmqProducer producer = new RabbitmqProducer();
                                JSONObject detailsJson = new JSONObject();
                                detailsJson.put("firstName", sharedWithUser.getFirstName());
                                detailsJson.put("salt", sharedWithUser.getSalt());
                                detailsJson.put("servletCtxName", servletCtx);
                                detailsJson.put("userMailId", sharedWithUser.getEmail());
                                detailsJson.put("message", message);
                                detailsJson.put("controllerMapping", sharedUrl);
                                detailsJson.put("subject", subject);
                                detailsJson.put("senderFirstName", user.getFirstName());
                                detailsJson.put("recordTitle", title);
                                detailsJson.put("imageUrl", imageUrl);
                                producer.sendEmailDetailsToQueue(amqpTemplate, detailsJson, Constants.RabbitMq.EMAIL_NVLI_SHARE_KEY);
//                                    }
                            } else {
                                throw new Exception("Error saving activity log");
                            }
                        } else {
                            throw new Exception("No user with given emailId registered with system");
                        }
                    }
                    //notification
                    Notifications notif = new Notifications();
                    notif.setNotifactionDateTime(new Date());
                    notif.setNotificationTargetURL("/search/preview/" + recordIdentifier);
                    notif.setNotificationMessage(messages.getMessage("notif.link.shared", new String[]{loggedUser.getFirstName() + " " + loggedUser.getLastName(), title}, Locale.getDefault()));
                    notif.setSenderId(loggedUser.getId());
                    notif.setNotificationActivityType(Constants.UserActivityConstant.INTRA_SHARE_RECORD);
                    notificationService.saveNotification(notif, IdList);
                    //end of notification
                    returnString = "success";
                } else {
                    returnString = "failed";
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            returnString = "failed";
        }
        return returnString;
    }

    /**
     * This method is used to give the user suggestion on search of user name.
     *
     * @param request{@link HttpServletRequest}
     * @return
     */
    @RequestMapping(value = "usersSuggestions")
    @ResponseBody
    public String usersSuggestions(HttpServletRequest request
    ) {
        try {
            return activityServiceObj.usersSuggestions(request.getParameter("username"));
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * Controller to get user suggestions belonging to particular role.
     *
     * @param request {@link HttpServletRequest }
     * @param roleId id of particular role.
     * @return String JSONObject in the String form
     */
    @RequestMapping(value = "usersSuggestionsBySearchedPhrase/{roleId}")
    @ResponseBody
    public String usersSuggestionsBySearchedPhrase(HttpServletRequest request, @PathVariable("roleId") long roleId) {
        try {
            return activityServiceObj.usersSuggestionsBySearchedPhrase(request.getParameter("username"), roleId);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return null;
        }
    }

    @RequestMapping(value = "/get/publicLists/{pageNumber}/{pageWindow}")
    @ResponseBody
    public JSONObject getPublicListsPaginated(@PathVariable("pageNumber") int pageNumber, @PathVariable("pageWindow") int pageWindow) {
        JSONObject finalJsonObj = new JSONObject();
        JSONArray listArray = new JSONArray();
        User user = userService.getCurrentUser();
        List<UserAddedList> publicLists = userAddedListService.getPublicLists(pageNumber, pageWindow, user.getId());
        finalJsonObj.put("publicLists", userAddedListService.getListsByUserInJSonArrayFormat(publicLists));
        return finalJsonObj;
    }

    /**
     * @Author Ruturaj Controller method to fetch user list with pagination.
     *
     * @param pageNumber current page number.
     * @param pageWindow max entries to show on each page.
     * @return JSONObject containing results.
     * @throws Exception
     */
    @RequestMapping(value = "/lists/results/{pageNumber}/{pageWindow}")
    @ResponseBody
    public JSONObject getSavedListsPaginated(@PathVariable("pageNumber") int pageNumber,
            @PathVariable("pageWindow") int pageWindow) throws Exception {
        JSONObject jSONObject = new JSONObject();
        User user = userService.getCurrentUser();
        if (user != null) {
            List<UserAddedList> userAddedLists = userAddedListService.getSavedListsByUserPaginated(user.getId(), pageNumber, pageWindow);
            jSONObject.put("savedLists", userAddedListService.getListsByUserInJSonArrayFormat(userAddedLists));
        }
        return jSONObject;
    }

    /**
     * This method is used to subscribe user to particular list.
     *
     * @param listName--name of list to which user wants to subscribe.
     * @param createdById
     * @param createdByUsername
     * @return
     */
    @RequestMapping(value = "/subscribeToList/{listName}/{createdById}/{createdByUsername}", method = RequestMethod.POST)
    @ResponseBody
    public int subscribeToList(@PathVariable("listName") String listName, @PathVariable("createdById") Long createdById, @PathVariable("createdByUsername") String createdByUsername) {
        User subscribedUser = userService.getCurrentUser();
        Runnable followersRunnable = () -> {
            userAddedListService.increamentSubscriptionCountOfList(listName, createdById);
        };
        new Thread(followersRunnable).start();
        User creator = userService.getUserById(createdById);
        userSubscribedListService.saveSubscribedList(listName, creator, subscribedUser);
        return 2;
    }

    @RequestMapping(value = "/unSubscribeToList/{listName}/{createdById}/{createdByUsername}", method = RequestMethod.POST)
    @ResponseBody
    public int unSubscribeToList(@PathVariable("listName") String listName, @PathVariable("createdById") Long createdById, @PathVariable("createdByUsername") String createdByUsername) {
        User user = userService.getCurrentUser();
        Runnable followersRunnable = () -> {
            userAddedListService.decrementSubscriptionCountOfList(listName, createdById);
        };
        new Thread(followersRunnable).start();
        userSubscribedListService.unSubscribeSubscribedList(listName, createdById, createdByUsername, user);
        return 2;
    }

    /**
     * Controller to fetch watch list of user.
     *
     * @param pageNumber
     * @param pageWindow
     * @return JSONObject
     * @throws Exception
     */
    @RequestMapping(value = "/watch-lists/results/{pageNumber}/{pageWindow}")
    @ResponseBody
    public JSONObject userWatchLists(@PathVariable("pageNumber") int pageNumber, @PathVariable("pageWindow") int pageWindow) throws Exception {
        JSONObject jSONObject = new JSONObject();
        User user = userService.getCurrentUser();
        List<UserSubscribedList> userSubscribedLists = userSubscribedListService.getSubscribedListOfUser(user.getId(), pageNumber, pageWindow);
        jSONObject.put("watchLists", userAddedListService.getSubscribedListsByUserInJSonArrayFormat(userSubscribedLists));
        return jSONObject;
    }
}
