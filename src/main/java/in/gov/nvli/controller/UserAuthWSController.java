package in.gov.nvli.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.webserviceclient.payment.UserAuthDetail;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserAuthWSController to handle operation through web service.
 *
 * @author Milind Kapre
 * @version 1
 * @since 1
 */
@RestController
@RequestMapping(value = "/wsauth")
public class UserAuthWSController {

    @Autowired
    private UserDAO userDAO;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    private final static Logger LOGGER = Logger.getLogger(UserAuthWSController.class);

    /**
     * 
     * @param userAuth
     * @return 
     */
    @RequestMapping(value = "/user-authentication-provider", method = RequestMethod.POST)
    public UserAuthDetail userAuthenticationProvider(@RequestBody String userAuth) {
        JsonParser jsonParser = new JsonParser();
        JsonObject userAuthJSON=jsonParser.parse(userAuth).getAsJsonObject();
        User user=null;
        UserAuthDetail userAuthDetail=new UserAuthDetail();
        userAuthDetail.setAuthUser(false);
        Role role=new Role();
        role.setCode(userAuthJSON.get("roleCode").getAsString());
        try{
            if (userDAO.isValidEmail(userAuthJSON.get("userName").getAsString())) {
                user = userDAO.findByEmail(userAuthJSON.get("userName").getAsString());
            } else {
                user = userDAO.findUserByUsername(userAuthJSON.get("userName").getAsString());
            }
            if(user!=null && passwordEncoder.matches(userAuthJSON.get("password").getAsString(), user.getPassword()) && user.getRoles().contains(role) && !user.getAssignedOrganizations().isEmpty()){
                userAuthDetail.setAuthUser(true);
                userAuthDetail.setFirstName(user.getFirstName());
                userAuthDetail.setLastName(user.getLastName());
                userAuthDetail.setCountry(user.getCountry());
                userAuthDetail.setCity(user.getCity());
                userAuthDetail.setGender(user.getGender());
                userAuthDetail.setDistrict(user.getDistrict());
                userAuthDetail.setMiddleName(user.getMiddleName());
                userAuthDetail.setAboutMe(user.getAboutMe());
                userAuthDetail.setState(user.getState());
                userAuthDetail.setUserId(user.getId());
                userAuthDetail.setEmail(user.getEmail());
                userAuthDetail.setUserName(user.getUsername());
                userAuthDetail.setContact(user.getContact());
                List<Long> assignedOrganizations=new ArrayList<Long>();
                user.getAssignedOrganizations().forEach(org->{
                    assignedOrganizations.add(org.getId());
                });
                userAuthDetail.setAssignedOrganizations(assignedOrganizations);
            }
        }catch (Exception ex) {
            LOGGER.error(ex.getMessage(),ex);
        }
        return userAuthDetail;
    }
}
