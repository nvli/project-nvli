/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.controller;

import in.gov.nvli.service.FileUploadService;
import in.gov.nvli.service.UserService;
import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@RequestMapping(value = "/uploader")
@Controller
public class FileUploadController {

    @Autowired
    UserService userService;

    @Value("${user.parent.directory}")
    String fileUploadLocation;

    @Value("${user.uploaded.images.dirname}")
    String userImageBaseDirName;

    @Autowired
    FileUploadService fileUploadService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/image", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject uploadFile(
            @RequestParam(value = "uploadfile") MultipartFile uploadfile,
            HttpServletRequest request
    ) {
        JSONObject object = new JSONObject();
        boolean status = fileUploadService.uploadFile(userService.getCurrentUser(), fileUploadLocation, userImageBaseDirName, uploadfile);
        object.put("status", status);
        return object;
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/image/r/{id}", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject replaceFile(
            @RequestParam(value = "uploadfile") MultipartFile uploadfile,
            @PathVariable(value = "id") long fileId,
            HttpServletRequest request
    ) {
        JSONObject object = new JSONObject();
        boolean status = fileUploadService.replaceFile(userService.getCurrentUser(), fileUploadLocation, userImageBaseDirName, uploadfile, fileId);
        object.put("status", status);
        return object;
    }
}
