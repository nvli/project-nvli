package in.gov.nvli.controller;

import in.gov.nvli.beans.CurationForm;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.CrowdSourcingService;
import in.gov.nvli.service.CurationActivityService;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.CurationStatus;
import in.gov.nvli.util.StringComparator;
import in.gov.nvli.webserviceclient.curationactivity.CurationActivityWebserviceHelper;
import in.gov.nvli.webserviceclient.curationactivity.CurationCount;
import in.gov.nvli.webserviceclient.curationactivity.CurationStatistics;
import in.gov.nvli.webserviceclient.curationactivity.ResourceCurationCount;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for handling all curation related requests.
 *
 * @author Milind,Ritesh
 * @author Vivek Bugale
 */
@Controller
@RequestMapping(value = "curation-activity")
public class CurationActivityController {

    private static final Logger LOG = Logger.getLogger(CurationActivityController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private CurationActivityService curationActivityService;

    @Autowired
    public CrowdSourcingService crowdSourcingService;

    @Autowired
    private CurationActivityWebserviceHelper curationActivityWebserviceHelper;

    @Autowired
    public ResourceService resourceService;

    /**
     * This is to get all library users list.
     *
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/show-library-experts")
    public ModelAndView libraryExperts() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("LibraryExpertsView");
        mav.addObject("user", userService.getCurrentUser());
        mav.addObject("allUsers", userService.getUsersByRoleCode(Constants.UserRole.LIB_USER));
        return mav;
    }

    /**
     * This is to load all curators in register.
     *
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/show-curators")
    public ModelAndView curators() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("CuratorsView");
        mav.addObject("user", userService.getCurrentUser());
        mav.addObject("allUsers", userService.getUsersByRoleCode(Constants.UserRole.CURATOR_USER));
        return mav;
    }

    /**
     * This is to show record distribution register.
     *
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "show-record-distribution")
    public ModelAndView showRecordDistribution() {
        ModelAndView mav = new ModelAndView();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Curation Activity", "#");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Record Distribution Register");
        mav.setViewName("RecordDistributionView");
        return mav;
    }

    /**
     * This is to filter the resources.
     *
     * @param searchString
     * @param resourceTypeFilter
     * @param dataLimit
     * @param pageNo
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "getResourcesByFiltersWithLimit")
    @ResponseBody
    public JSONObject getResourcesByFiltersWithLimit(@RequestParam String searchString, @RequestParam long resourceTypeFilter, @RequestParam int dataLimit, @RequestParam int pageNo) {
        try {
            return curationActivityService.getResourcesByFiltersWithLimit(searchString, resourceTypeFilter, dataLimit, pageNo);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * This is to get record and user count by resource.
     *
     * @param resourceCode
     * @param resourceTypeCode
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "getRecordAndUserCountByResource")
    @ResponseBody
    public JSONObject getRecordAndUserCountByResource(@RequestParam String resourceCode, @RequestParam String resourceTypeCode) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("recordCount", curationActivityWebserviceHelper.getRecordCountByResource(resourceTypeCode, resourceCode));
            jSONObject.put("userCount", userService.getUserIdsByAssignedResource(resourceCode, Constants.UserRole.CURATOR_USER).size());
            jSONObject.put("libExpertCount", userService.getUserIdsByAssignedResource(resourceCode, Constants.UserRole.LIB_USER).size());
            return jSONObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * This is to distribute records among all users.
     *
     * @param resourceCode
     * @param resourceTypeCode
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "startDistribution")
    @ResponseBody
    public boolean startDistribution(@RequestParam String resourceCode, @RequestParam String resourceTypeCode) {
        try {
            return curationActivityWebserviceHelper.startDistribution(userService.getUserIdsByAssignedResource(resourceCode, Constants.UserRole.CURATOR_USER), resourceTypeCode, resourceCode);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return false;
        }
    }

    /**
     * This is to load curator register.
     *
     * @param curationForm
     * @param request
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.CURATOR_USER + "')")
    @RequestMapping("/curationRegister")
    public ModelAndView curationRegister(@ModelAttribute("curationForm") CurationForm curationForm, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("curationRegisterView");
        try {
            User currentUser = userService.getCurrentUser();
            List<String> resouceTypeList = new ArrayList(curationActivityWebserviceHelper.getUserAssignedResourceCodes(currentUser.getId()));
            Collections.sort(resouceTypeList, new StringComparator());
            if (curationForm.getStatus() == null) {
                curationForm = new CurationForm();
                curationForm.setPageNo("1");
                curationForm.setPageWin("20");
                curationForm.setStatus(CurationStatus.UNCURATED);
                curationForm.setResourceCode(!(resouceTypeList.isEmpty()) ? resouceTypeList.get(0) : null);
            }
            curationForm.setUserId(currentUser.getId());
            curationForm = curationActivityService.getCurationFrom(curationForm);
            if (curationForm.getSubResourceIdentifiers() == null || curationForm.getSubResourceIdentifiers().isEmpty()) {
                curationForm.setSubResourceIdentifiers(curationForm.getAllSubResourceIdentifiers());
            }
            List<Resource> resources = resourceService.getResourceByResourceTypeCode(curationForm.getResourceCode());
            modelAndView.addObject("totalPages", (int) Math.ceil((double) (curationForm.getResultSize() != null ? curationForm.getResultSize() : 0) / Integer.valueOf(curationForm.getPageWin())));
            modelAndView.addObject("languageList", curationActivityService.getLanguageList());
            modelAndView.addObject("resourceTypeCodeList", resouceTypeList);
            modelAndView.addObject("resources", resources);
            // modelAndView.addObject("otherResourceTypeList", resourceService.getListOfResourceTypeByResourceIdList(curationActivityWebserviceHelper.getUserAssignedResourceCodes(currentUser.getId()), false));
            modelAndView.addObject("curationForm", curationForm);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return modelAndView;
    }

    /**
     * This is to get assigned records of the user.
     *
     * @param userId
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "getAssignedResourcesByUser/{userId}")
    @ResponseBody
    public JSONArray getAssignedResourcesByUser(@PathVariable("userId") Long userId) {
        try {
            JSONObject resourceJSONObject;
            JSONArray resourceJSONArray = new JSONArray();

            for (Resource resource : userService.getUserById(userId).getAssignedResources()) {
                resourceJSONObject = new JSONObject();
                resourceJSONObject.put("id", resource.getId());
                resourceJSONObject.put("resourceType", resource.getResourceType().getResourceType());
                resourceJSONObject.put("resourceName", resource.getResourceName());
                resourceJSONArray.add(resourceJSONObject);
            }

            return resourceJSONArray;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return null;
        }
    }

    /**
     * This is to save assigned resources.
     *
     * @param userId
     * @param assignedResourceIds
     * @param request
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = {"saveAssignedResources/{userId}"})
    @ResponseBody
    public boolean saveAssignedResources(@PathVariable("userId") Long userId, @RequestParam List<Long> assignedResourceIds, HttpServletRequest request) {
        try {
            curationActivityService.saveAssignedResources(userId, assignedResourceIds);

            //check LIB_USER role from userId
//            if (userService.hasRole(userId, Constants.UserRole.LIB_USER)) {
//                return curationActivityService.assignedLibExpertAndDeleteUnassignedEntry(assignedResourceIds,userService.getUserById(userId));
//            }
            return true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * This is to get resource wise statistics.
     *
     * @param resourceCode
     * @param resourceTypeCode
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = {"showResourceStatistics"})
    @ResponseBody
    public JSONObject showResourceStatistics(@RequestParam String resourceCode, @RequestParam String resourceTypeCode) {
        try {
            JSONObject responseObject = new JSONObject();
            Map<Long, CurationCount> statistics = curationActivityWebserviceHelper.getResourceWiseStatistics(resourceTypeCode, resourceCode);
            responseObject.put("associatedUsers", curationActivityService.getUserByAssignedResourceAndUserIds(resourceCode, statistics.keySet(), Constants.UserRole.CURATOR_USER));
            responseObject.put("statistics", statistics);
            return responseObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to load the statistics of the library expert.
     *
     * @param resourceCode
     * @param resourceTypeCode
     * @return
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = {"showLibExpertStatistics"})
    @ResponseBody
    public JSONObject showLibExpertStatistics(@RequestParam String resourceCode, @RequestParam String resourceTypeCode) {
        try {
            JSONObject responseObject = new JSONObject();
            JSONObject libExpStatsJsonObj = curationActivityService.getLibraryExpertStatistics(resourceCode);
            if (libExpStatsJsonObj == null) {
                responseObject.put("statistics", "");
                responseObject.put("associatedUsers", curationActivityService.getUserByAssignedResourceAndUserIds(resourceCode, null, Constants.UserRole.LIB_USER));
            } else {
                responseObject.put("statistics", libExpStatsJsonObj.get("libraryExpertStatArr"));
                responseObject.put("associatedUsers", curationActivityService.getUserByAssignedResourceAndUserIds(resourceCode, (Set<Long>) libExpStatsJsonObj.get("keySet"), Constants.UserRole.LIB_USER));
            }
            return responseObject;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * This is to get work report of the curator.
     *
     * @param userId
     * @param request
     * @param response
     * @throws Exception
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "curator-report/{userId}")
    public void curatorWorkReport(@PathVariable("userId") Long userId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String sDate = request.getParameter("startDate");
            String eDate = request.getParameter("endDate");
            if (sDate != null && eDate != null && userId != null) {
                User userObj = userService.getUserById(userId);
                Date startDate = format.parse(sDate);
                Date endDate = format.parse(eDate);

                if (userObj != null) {
                    ByteArrayOutputStream baos = curationActivityService.generateCuratorWorkReportInPDF(userObj, startDate, endDate);
                    response.setHeader("Expires", "0");
                    response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                    response.setHeader("Pragma", "public");
                    response.setContentType("application/pdf");
                    response.setContentLength(baos.size());
                    response.setHeader("Content-disposition", "inline; filename=" + "cuartion-activity-report.pdf");
                    OutputStream os;
                    os = response.getOutputStream();
                    baos.writeTo(os);
                    os.flush();
                    os.close();
                }
            }
        } catch (Exception ex) {
            ByteArrayOutputStream baos = curationActivityService.generateErrorReportInPDF();
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("application/pdf");
            response.setContentLength(baos.size());
            response.setHeader("Content-disposition", "inline; filename=" + "cuartion-activity-report.pdf");
            OutputStream os;
            os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();
            ex.printStackTrace();
            LOG.error(ex.getMessage());
        }
    }

    /**
     * This is to get work report of library expert.
     *
     * @param userId
     * @param request
     * @param response
     * @throws Exception
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "library-expert-report/{userId}")
    public void libraryExpertWorkReport(@PathVariable("userId") Long userId, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String sDate = request.getParameter("startDate");
            String eDate = request.getParameter("endDate");
            if (sDate != null && eDate != null && userId != null) {
                User userObj = userService.getUserById(userId);
                Date startDate = format.parse(sDate);
                Date endDate = format.parse(eDate);

                if (userObj != null) {
                    ByteArrayOutputStream baos = curationActivityService.generateLibraryExpertReportInPDF(userObj, startDate, endDate);
                    response.setHeader("Expires", "0");
                    response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                    response.setHeader("Pragma", "public");
                    response.setContentType("application/pdf");
                    response.setContentLength(baos.size());
                    response.setHeader("Content-disposition", "inline; filename=" + "cuartion-activity-report.pdf");
                    OutputStream os;
                    os = response.getOutputStream();
                    baos.writeTo(os);
                    os.flush();
                    os.close();
                }
            }
        } catch (Exception ex) {
            ByteArrayOutputStream baos = curationActivityService.generateErrorReportInPDF();
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("application/pdf");
            response.setContentLength(baos.size());
            response.setHeader("Content-disposition", "inline; filename=" + "cuartion-activity-report.pdf");
            OutputStream os;
            os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();
            ex.printStackTrace();
            LOG.error(ex.getMessage());
        }
    }

    /**
     * This is to get work status of the curator.
     *
     * @param userId
     * @return
     * @throws Exception
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "show-work-statistics/{userId}")
    @ResponseBody
    public JSONObject curatorWorkStats(@PathVariable("userId") Long userId) throws Exception {
        JSONObject jSONObject = new JSONObject();
        List<String> resourceCodeList = new ArrayList<>();
        CurationStatistics curationStatistics = null;
        try {
            curationStatistics = curationActivityWebserviceHelper.getWorkStatistics(userId);
            for (ResourceCurationCount rcc : curationStatistics.getResourceWiseCount()) {
                resourceCodeList.addAll(rcc.getUserCurationCounts().keySet());
            }
            Map<String, String> resourceCodeNameMap = curationActivityService.getResourceCodeNameMap(resourceCodeList);
            jSONObject.put("curationStatistics", curationStatistics);
            jSONObject.put("resourceCodeNameMap", resourceCodeNameMap);
            return jSONObject;
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage());
            return null;
        }
    }

    /**
     * This is to get resources which are not assigned any library expert.
     *
     * @return @throws Exception
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.METADATA_ADMIN + "')")
    @RequestMapping(value = "/check-Resources-Without-LibUser-Register")
    @ResponseBody
    public JSONObject checkResourcesWithoutLibUserRegister() throws Exception {
        return curationActivityService.checkResourcesWithoutLibUserRegister();
    }

    /**
     * This is used to fetch all organizations.
     *
     * @return String Json String of organizations.
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/fetch-all-organizations")
    @ResponseBody
    public String fetchAllOrgnizations() {
        return curationActivityService.fetchAllOrganizations().toString();
    }

    /**
     * This method is used to fetch assigned organizations to user.
     *
     * @param userId
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = "/fetch-assigned-organizations")
    @ResponseBody
    public String fetchAssignedOrgnizations(@RequestParam Long userId) {
        try {
            JSONObject organizationJSONObject;
            JSONArray resourceJSONArray = new JSONArray();

            for (Organization organization : userService.getUserById(userId).getAssignedOrganizations()) {
                organizationJSONObject = new JSONObject();
                organizationJSONObject.put("oraganizationId", organization.getId());
                organizationJSONObject.put("organizationName", organization.getName());
                resourceJSONArray.add(organizationJSONObject);
            }

            return resourceJSONArray.toString();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return "";
        }
    }

    /**
     * This method is used to save assigned organizations to user.
     *
     * @param userId
     * @param assignedOrganizationsIds
     * @param request
     */
    @PreAuthorize("hasAnyRole('" + Constants.UserRole.ADMIN_USER + "')")
    @RequestMapping(value = {"save-assigned-organizations"})
    @ResponseBody
    public boolean saveAssignedOrganizations(@RequestParam Long userId, @RequestParam List<Long> assignedOrganizationIds, HttpServletRequest request) {
        try {
            curationActivityService.saveAssignedOragnizations(userId, assignedOrganizationIds);
            return true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return false;
        }
    }
}
