/**
 *
 * Copyright 2015 HCDC, Human Centered Design Computing Licensed. you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package in.gov.nvli.controller;

import in.gov.nvli.domain.resource.ArtistProfile;
import in.gov.nvli.domain.resource.ArtisticResourceType;
import in.gov.nvli.service.ArtistService;
import in.gov.nvli.service.ArtistSiteService;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.Produces;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gajraj Singh Tanwar
 */
@RestController
@RequestMapping(value = "/rest")
public class ArtistRestController {

    private static final Logger logger = Logger.getLogger(ArtistRestController.class);

    public static String SAVE_DIRECTORY = "Z:\\Aritist";
    public static String DEFAULT_BACTH_NAME = "batch01";

    @Autowired
    private ArtistService artistService;
    @Autowired
    private ArtistSiteService artistSiteService;

    /**
     * generate the DC xmls for a specific gallery id
     *
     * @param galleryId
     * @throws IOException
     */
    @RequestMapping(value = "/dc/generate/{galleryId}")
    @Produces(value = {"application/json", "application/xml"})
    public String generateDublinCoreXmlsForSite(@PathVariable("galleryId") int galleryId) throws IOException {
        ArtisticResourceType artResource = artistSiteService.getArtistResourcebyId(galleryId);

        File dir = new File(SAVE_DIRECTORY + File.separatorChar + CreateName((artResource.getSiteName())) + File.separatorChar + DEFAULT_BACTH_NAME);
        if (!dir.exists()) {
            boolean created = dir.mkdirs();
            if (created) {
                logger.info("Resource Directory Created Successfully " + dir.getAbsolutePath());
            } else {
                logger.error("Error in creating Directory " + dir.getAbsolutePath());
            }
        }
        if (dir.exists()) {
            List<ArtistProfile> artists = artistService.getAllArtistForGallary(galleryId);
            if (artists != null) {
                for (ArtistProfile artist : artists) {
                    StringBuilder sb = generateDCfields(artist);
                    File profDir = new File(dir.getAbsolutePath() + File.separatorChar + artist.getArtistName());
                    if (!profDir.exists()) {
                        boolean created = profDir.mkdirs();
                        if (created) {
                            logger.info("Profile Directory Created Successfully " + dir.getAbsolutePath());
                        } else {
                            logger.error("Error in creating Profile Directory " + dir.getAbsolutePath());
                        }
                    }
                    if (profDir.exists()) {
                        File file = new File(profDir.getAbsolutePath() + File.separatorChar + Integer.toString(artist.getId()) + ".xml");
                        boolean fileCreated = file.createNewFile();
                        if (fileCreated) {
                            FileUtils.writeStringToFile(file, sb.toString(), Charsets.UTF_8);
                        }
                    } else {
                        return "ERROR";
                    }

                }
            }
        }
        return "SUCCESS";
    }

    /**
     *Generates the DC XML for All 
     * @return @throws java.io.IOException
     */
    @RequestMapping(value = "/dc/generate/all")
    @Produces(value = {"application/json", "application/xml"})
    public String generateDublinCoreXmlsForAll() throws IOException {
        List<ArtisticResourceType> siteList = artistSiteService.getAllArtResource();
        for (ArtisticResourceType artistResource : siteList) {
            String siteDir = CreateName(artistResource.getSiteName());
            File file = new File(SAVE_DIRECTORY + File.separatorChar + siteDir);
            if (!file.exists()) {
                boolean created = file.mkdir();
                if (created) {
                    logger.info("Directory created successfuly" + siteDir);
                } else {
                    logger.error("Error in creating Directory : " + siteDir);
                }
            }
            if (file.exists()) {
                File batchFolder = new File(SAVE_DIRECTORY + File.separatorChar + siteDir + File.separatorChar + "batch01");
                if (!batchFolder.exists()) {
                    boolean created = batchFolder.mkdir();
                    if (created) {
                        logger.info("Directory created successfuly " + batchFolder.getName());
                    } else {
                        logger.error("Error in creating Directory : " + batchFolder.getName());
                    }
                }
                if (batchFolder.exists()) {
                    List<ArtistProfile> artistList = artistService.getAllArtistForGallary(artistResource.getArtisticResourseId());
                    if (artistList != null) {
                        for (ArtistProfile artist : artistList) {
                            String artistName = artist.getArtistName();
                            File profileFolder = new File(batchFolder.getAbsolutePath() + File.separatorChar + artistName);
                            if (!profileFolder.exists()) {
                                boolean created = profileFolder.mkdir();
                                if (created) {
                                    logger.info("Directory created successfuly " + profileFolder.getName());
                                } else {
                                    logger.error("Error in creating Directory : " + profileFolder.getName());
                                }
                            }
                            if (profileFolder.exists()) {
                                StringBuilder sb = generateDCfields(artist);
                                File xmlfile = new File(profileFolder.getAbsolutePath() + File.separatorChar + Integer.toString(artist.getId()) + ".xml");
                                if (!xmlfile.exists()) {
                                    xmlfile.createNewFile();
                                }
                                if (xmlfile.exists()) {
                                    logger.info(" xml File  created Successfully" + xmlfile.getName());
                                    FileUtils.writeStringToFile(xmlfile, sb.toString(), Charsets.UTF_8, false);
                                } else {
                                    logger.error("Error in creating xml File " + xmlfile.getName());
                                }
                            }

                        }

                    }
                }
            }

        }
        return "SUCCESS";
    }
    /**
     *This Method generates DC metadata Fields required for 
     * Method @generateDublinCoreXmlsForAll
     */
    private StringBuilder generateDCfields(ArtistProfile artist) {
        StringBuilder sb = new StringBuilder();
        sb.append(" <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        sb.append("<metadata xmlns=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">\n");
        String artistName = artist.getArtistName();
        if (artistName != null && !artistName.isEmpty()) {
            sb.append(addTagAndValue("dc:title", artistName));
        }
        // addTagAndValue("creator", artist.getArtistName());
        // addTagAndValue("subject", artist.getArtistName());
        String artistDescription = artist.getArtistDescription();
        if (artistDescription != null && !artistDescription.isEmpty()) {
            sb.append(addTagAndValue("dc:description", artistDescription));
        }
        // addTagAndValue("publisher", artist.getArtistName());
        String contributor = artist.getArtistResourceID().getSiteName();
        if (contributor != null && !contributor.isEmpty()) {
            sb.append(addTagAndValue("dc:contributor", contributor));
        }
        // addTagAndValue("date", artist.getArtistName());
        sb.append(addTagAndValue("dc:type", "Artistic Profile"));
        // addTagAndValue("format", artist.getArtistName());
        sb.append(addTagAndValue("dc:identifier", Integer.toString(artist.getId())));
        sb.append(addTagAndValue("dc:source", artist.getArtistProfileLink()));
        sb.append(addTagAndValue("dc:language", "English"));
        // addTagAndValue("relation", artist.getArtistName());
        //  addTagAndValue("coverage", artist.getArtistName());
        // addTagAndValue("rights", artist.getArtistName());
        return sb;
    }

    private String addTagAndValue(String tag, String value) {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(tag).append(">").append(value).append("</").append(tag).append(">").append("\n");
        return sb.toString();
    }

    /**
     *
     * @param siteName
     * @return
     */
    private String CreateName(String siteName) {
        String replaceString = "[^a-zA-z]";
        siteName = siteName.replaceAll(replaceString, " ");
        siteName = siteName.replace("www", "");
        siteName = siteName.replace("com", "");
        siteName = siteName.replace("http", "");
        return siteName.trim();
    }

}
