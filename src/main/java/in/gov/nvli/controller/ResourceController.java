package in.gov.nvli.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.dao.crowdsource.ILanguageDAO;
import in.gov.nvli.dao.resource.IContentTypeDAO;
import in.gov.nvli.dao.resource.IENewsDAO;
import in.gov.nvli.dao.resource.IGroupTypeDAO;
import in.gov.nvli.dao.resource.IMetadataIntegratorDAO;
import in.gov.nvli.dao.resource.IOpenRepositoryDAO;
import in.gov.nvli.dao.resource.IOrganizationDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceStatusDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.dao.resource.IResourceTypeInfoDAO;
import in.gov.nvli.dao.resource.IStateDAO;
import in.gov.nvli.dao.resource.IThematicTypeDAO;
import in.gov.nvli.dao.resource.IWebCrawlerDAO;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.resource.ContentType;
import in.gov.nvli.domain.resource.ENews;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.MetadataIntegrator;
import in.gov.nvli.domain.resource.NewsFeed;
import in.gov.nvli.domain.resource.NewsFeeds;
import in.gov.nvli.domain.resource.OpenRepository;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceStatus;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.resource.ResourceTypeInfo;
import in.gov.nvli.domain.resource.ThematicType;
import in.gov.nvli.domain.resource.WebCrawler;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.editor.resource.ContentTypeEditor;
import in.gov.nvli.editor.resource.GroupTypeEditor;
import in.gov.nvli.editor.resource.LanguageEditor;
import in.gov.nvli.editor.resource.OrganizationEditor;
import in.gov.nvli.editor.resource.ResourceEditor;
import in.gov.nvli.editor.resource.ResourceTypeEditor;
import in.gov.nvli.editor.resource.ThematicTypeEditor;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Helper;
import in.gov.nvli.util.ResultHolder;
import in.gov.nvli.webserviceclient.mit.ClassificationLevel;
import in.gov.nvli.webserviceclient.oar.HarRepoCustomised;
import in.gov.nvli.webserviceclient.oar.IdentifyType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Hibernate;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for handling all resource related requests.
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Controller
@SessionAttributes("resource")
@RequestMapping(value = "/resource")
public class ResourceController {

    /**
     * To hold the model and view
     *
     * @see ModelAndView
     */
    private ModelAndView mav;
    /**
     * The name of the property resourceService used to holds
     * {@link ResourceService} object reference.
     */
    @Autowired
    public ResourceService resourceService;
    /**
     * The name of the property resourceTypeDAO used to holds
     * {@link IResourceTypeDAO} object reference.
     */
    @Autowired
    public IResourceTypeDAO resourceTypeDAO;
    /**
     * The name of the property thematicTypeDAO used to holds
     * {@link IThematicTypeDAO} object reference.
     */
    @Autowired
    public IThematicTypeDAO thematicTypeDAO;
    /**
     * The name of the property contentTypeDAO used to holds
     * {@link IContentTypeDAO} object reference.
     */
    @Autowired
    public IContentTypeDAO contentTypeDAO;
    /**
     * The name of the property languageDAO used to holds {@link ILanguageDAO}
     * object reference.
     */
    @Autowired
    public ILanguageDAO languageDAO;
    /**
     * The name of the property resourceDAO used to holds {@link IResourceDAO}
     * object reference.
     */
    @Autowired
    public IResourceDAO resourceDAO;
    /**
     * The name of the property redirectView used to holds {@link RedirectView}
     * object reference.
     */
    private RedirectView redirectView;
    /**
     * The name of the property stateDAO used to holds {@link IStateDAO} object
     * reference.
     */
    @Autowired
    public IStateDAO stateDAO;
    /**
     * The name of the property groupTypeDAO used to holds {@link IGroupTypeDAO}
     * object reference.
     */
    @Autowired
    public IGroupTypeDAO groupTypeDAO;
    /**
     * The name of the property openRepositoryDAO used to holds
     * {@link IOpenRepositoryDAO} object reference.
     */
    @Autowired
    private IOpenRepositoryDAO openRepositoryDAO;
    /**
     * The name of the property eNewsDAO used to holds {@link IENewsDAO} object
     * reference.
     */
    @Autowired
    private IENewsDAO eNewsDAO;
    /**
     * The name of the property metadataIntegratorDAO used to holds
     * {@link IMetadataIntegratorDAO} object reference.
     */
    @Autowired
    private IMetadataIntegratorDAO metadataIntegratorDAO;
    /**
     * To hold the resource icon location
     */
    @Value("${nvli.resource.image.location}")
    private String resourceIconLocation;
    /**
     * To hold the group icon location
     */
    @Value("${nvli.group.image.location}")
    private String groupIconLocation;
    /**
     * The name of the property LOGGER used to holds {@link Logger} object
     * reference.
     */
    private final static Logger LOGGER = Logger.getLogger(ResourceController.class);
    /**
     * Hold default window size for all register.
     */
    @Value("${view.datatable.default.records-per-page}")
    private String defaultPageWin;
    /**
     * The name of the property webCrawlerDAO used to holds
     * {@link IWebCrawlerDAO} object reference.
     */
    @Autowired
    private IWebCrawlerDAO webCrawlerDAO;
    /**
     * The name of the property resultHolder used to holds {@link ResultHolder}
     * object reference.
     */
    private ResultHolder resultHolder;
    /**
     * The name of the property resourceTypeInfoDAO used to holds
     * {@link IResourceTypeInfoDAO} object reference.
     */
    @Autowired
    private IResourceTypeInfoDAO resourceTypeInfoDAO;
    /**
     * To hold the resource type icon location
     */
    @Value("${nvli.resourcType.image.location}")
    private String resourceTypeIconLocation;
    /**
     * To hold the resource type search icon location
     */
    @Value("${nvli.resourcType.search.image.location}")
    private String resourceTypeIconSearchLocation;
    /**
     * To hold the resource type default icon location
     */
    @Value("${nvli.resourcType.default.image.location}")
    private String resourceTypeIconDefalutLocation;
    /**
     * The name of the property organizationDAO used to holds
     * {@link IOrganizationDAO} object reference.
     */
    @Autowired
    private IOrganizationDAO organizationDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private IResourceStatusDAO resourceStatusDAO;

    /**
     * To hold the resource icon read location
     */
    @Value("${nvli.read.resource.image.location}")
    private String resourceIconReadLocation;

    /**
     * To hold the group icon read location
     */
    @Value("${nvli.read.group.image.location}")
    private String groupIconReadLocation;

    /**
     * To hold the resource type icon read location
     */
    @Value("${nvli.read.resourcType.image.location}")
    private String resourceTypeIconReadLocation;
    /**
     * To hold the resource type search icon read location
     */
    @Value("${nvli.read.resourcType.search.image.location}")
    private String resourceTypeIconSearchReadLocation;
    /**
     * To hold the resource type default icon read location
     */
    @Value("${nvli.read.resourcType.default.image.location}")
    private String resourceTypeIconDefalutReadLocation;

    /**
     * To initialize {@link WebDataBinder}
     *
     * @param webDataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(ResourceType.class, new ResourceTypeEditor(resourceTypeDAO));
        webDataBinder.registerCustomEditor(ContentType.class, new ContentTypeEditor(contentTypeDAO));
        webDataBinder.registerCustomEditor(ThematicType.class, new ThematicTypeEditor(thematicTypeDAO));
        webDataBinder.registerCustomEditor(UDCLanguage.class, new LanguageEditor(languageDAO));
        webDataBinder.registerCustomEditor(Resource.class, new ResourceEditor(resourceDAO));
        webDataBinder.registerCustomEditor(GroupType.class, new GroupTypeEditor(groupTypeDAO));
        webDataBinder.registerCustomEditor(Organization.class, new OrganizationEditor(organizationDAO));
        StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
        webDataBinder.registerCustomEditor(String.class, stringtrimmer);
    }

    /**
     * Handles url for creating new Content type
     *
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/contenttype/create")
    public ModelAndView createContentType() {
        mav = new ModelAndView();
        mav.addObject("contentType", new ContentType());
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Content Type List", "resource/contenttype/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Content Type Form");
        mav.setViewName("resource-contenttype-create");
        return mav;
    }

    /**
     * Handles URL for Save the newly created Content Type.
     *
     * @param contentType {@link ContentType}
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/contenttype/submit")
    public RedirectView submitContentType(@ModelAttribute ContentType contentType, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        boolean isSaved = resourceService.createNewContentType(contentType);
        redirectView = new RedirectView(request.getContextPath() + "/resource/contenttype/create");
        redirectAttributes.addFlashAttribute("isSaved", isSaved);
        return redirectView;
    }

    /**
     * Handles url for creating new Resource type
     *
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/type/create")
    public ModelAndView createResourceType() {
        try {
            mav = new ModelAndView();
            mav.addObject("resourceType", new ResourceType());
            List<String> categoryForResourceType = resourceService.getCategoryListForResourceType();
            List<UDCLanguage> listOfLang = resourceService.getIndianLanguageList();
            UDCLanguage engLanguage = languageDAO.getLanguageByLanguageName("English");
            listOfLang.remove(engLanguage);
            mav.addObject("languageList", listOfLang);
            mav.addObject("engLanguage", engLanguage);
            mav.addObject("categoryList", categoryForResourceType);
            mav.addObject("isEditType", false);
            mav.setViewName("resource-resourcetype-create");
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("Resource Type Register", "resource/resourceType/list");
            mav.addObject("breadcrumbMap", breadcrumbMap);
            mav.addObject("title", "Resource Type Form");
            mav.addObject("rtIconDefalutLocation", resourceTypeIconDefalutReadLocation);
            mav.addObject("rtIconLocation", resourceTypeIconReadLocation);
            mav.addObject("rtIconSearchLocation", resourceTypeIconSearchReadLocation);
            User user = userService.getCurrentUser();
            mav.addObject("user", user);
        } catch (Exception ex) {
            LOGGER.error("Error in Reosurce Type Create/Edit", ex);
        }
        return mav;
    }

    /**
     * Handles URL for Save the newly created Content Type.
     *
     * @param resourceType {@link ResourceType}
     * @param request
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/type/submit", method = RequestMethod.POST)
    public RedirectView submitResourceType(@ModelAttribute ResourceType resourceType, HttpServletRequest request) {
        try {
            String resourceTypeIcon = request.getParameter("resourceTypeIcon");
            String resourceTypedefaultIcon = request.getParameter("resourceTypedefaultIcon");
            String resourceTypeSearchIcon = request.getParameter("resourceTypeSearchIcon");
            String[] fileNameAndExtension = resourceTypeIcon.split(Constants.REG_EXP_FOR_EXTENSION);
            String[] fileNameAndExtensionRTD = resourceTypedefaultIcon.split(Constants.REG_EXP_FOR_EXTENSION);
            String[] fileNameAndExtensionRTS = resourceTypeSearchIcon.split(Constants.REG_EXP_FOR_EXTENSION);
            if (fileNameAndExtension[0].equalsIgnoreCase("temp")) {
                File resourceTypeIconDir = new File(resourceTypeIconLocation);
                String iconAbsolutePath = resourceTypeIconDir.getAbsolutePath();
                String iconName = resourceType.getResourceTypeCode() + "." + fileNameAndExtension[1];
                String resourceTypeName = resourceService.saveIcon(iconAbsolutePath, iconName);
                resourceType.setResourceTypeIcon(resourceTypeName);
            } else {
                resourceType.setResourceTypeIcon(resourceTypeIcon);
            }

            if (fileNameAndExtensionRTD[0].equalsIgnoreCase("temp")) {
                File resourceTypeIconDir = new File(resourceTypedefaultIcon);
                String iconAbsolutePath = resourceTypeIconDir.getAbsolutePath();
                String iconName = resourceType.getResourceTypeCode() + "." + fileNameAndExtensionRTD[1];
                String resourceTypeName = resourceService.saveIcon(iconAbsolutePath, iconName);
                resourceType.setResourceTypedefaultIcon(resourceTypeName);
            } else {
                resourceType.setResourceTypedefaultIcon(resourceTypedefaultIcon);
            }
            resourceType.setTotalRecordCount(new Long(0));
            if (fileNameAndExtensionRTS[0].equalsIgnoreCase("temp")) {
                File resourceTypeIconDir = new File(resourceTypeSearchIcon);
                String iconAbsolutePath = resourceTypeIconDir.getAbsolutePath();
                String iconName = resourceType.getResourceTypeCode() + "." + fileNameAndExtensionRTS[1];
                String resourceTypeName = resourceService.saveIcon(iconAbsolutePath, iconName);
                resourceType.setResourceTypeSearchIcon(resourceTypeName);
            } else {
                resourceType.setResourceTypeSearchIcon(resourceTypeSearchIcon);
            }

            for (ResourceTypeInfo resourceInfo : resourceType.getResourceTypeInfo()) {
                resourceInfo.setResourceType(resourceType);
            }
            resourceService.removeEmptyObject(resourceType.getResourceTypeInfo());
            resourceType.setPublishFlag(true);
            if (resourceService.createNewResourceType(resourceType)) {
                Map<String, ResourceType> resourceTypesMap = (Map<String, ResourceType>) request.getServletContext().getAttribute("resourceTypes");
                request.getServletContext().setAttribute("resourceTypes", resourceService.updateResourceTypeMap(resourceTypesMap, resourceType));
                redirectView = new RedirectView(request.getContextPath() + "/resource/resourceType/list");
            } else {
                redirectView = new RedirectView(request.getContextPath() + "/resource/type/create");
            }
        } catch (IOException io) {
            LOGGER.error("Error in save icon for resource type : ", io);
        } catch (Exception ex) {
            LOGGER.error("Error in save resource type : ", ex);
        }
        return redirectView;
    }

    /**
     * Handles url for creating new Resource
     *
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/create")
    public ModelAndView createResource() {
        mav = resourceService.populateAddResourceForm();
        mav.setViewName("resource-create");
        mav.addObject("isEdit", false);
        mav.addObject("isDraft", false);
        User user = userService.getCurrentUser();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Resource Type Register", "resource/resourceType/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("resourceIconLocation", resourceIconReadLocation);
        mav.addObject("title", "Resource Form");
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Save the newly created Resource.
     *
     * @param resource {@link Resource}
     * @param result
     * @param request
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    public RedirectView submitResource(@ModelAttribute Resource resource, BindingResult result, HttpServletRequest request
    ) throws Exception {
        try {
            resource.setCreatedOn(Helper.rightNow());
            ResourceType resourceType = resource.getResourceType();
            ResourceStatus resourceStatus = resourceService.createNewResource(resource);
            redirectView = new RedirectView(request.getContextPath() + "/resource/register/" + resourceType.getId());
        } catch (URISyntaxException | IOException e) {
            LOGGER.error("Error in save resource : ", e);
        }
        return redirectView;
    }

    /**
     * Handles url for creating new Language
     *
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/language/create")
    public ModelAndView createLanguage() {
        mav = new ModelAndView();
        mav.addObject("language", new UDCLanguage());
        mav.setViewName("language-create");
        User user = userService.getCurrentUser();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Language List", "resource/language/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Language Form");
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Save the newly created Language.
     *
     * @param language {@link Language}
     * @param request {@link HttpServletRequest}
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/language/save")
    public RedirectView saveLanguage(@ModelAttribute UDCLanguage language, HttpServletRequest request
    ) {
        if (resourceService.createNewLanguage(language)) {
            redirectView = new RedirectView(request.getContextPath() + "/resource/language/list");
        } else {
            redirectView = new RedirectView(request.getContextPath() + "/resource/language/create");
        }
        return redirectView;
    }

    /**
     * Handles url for creating new ThematicType
     *
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/thematictype/create")
    public ModelAndView createThematicType() {
        mav = resourceService.populateAddThematicType();
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Thematic Type List", "resource/thematic/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Thematic Form");
        mav.setViewName("thematic-type-create");
        return mav;
    }

    /**
     * Handles URL for Save the newly created Thematic Type.
     *
     * @param thematicType {@link ThematicType}
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return ModelAndView object for creation page
     */
    @RequestMapping(value = "/thematictype/save")
    public RedirectView saveThematicType(@ModelAttribute ThematicType thematicType, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        boolean isSaved = resourceService.createNewThematicType(thematicType);
        redirectView = new RedirectView(request.getContextPath() + "/resource/thematictype/create");
        redirectAttributes.addFlashAttribute("isSaved", isSaved);
        return redirectView;
    }

    /**
     * Handles URL for displays Content Type List.
     *
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/contenttype/list")
    public ModelAndView contentTypeList() {
        mav = new ModelAndView();
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("title", "Content Type List");
        mav.setViewName("content-type-list");
        return mav;
    }

    /**
     * Handles URL for displays Resource Type List.
     *
     * @param searchString
     * @param pageNum page number
     * @param pageWin window size
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/resourceType/list")
    @ResponseBody
    public JSONObject resourceTypeList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum) {
        System.out.println("resourceTypeList");
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getRTListWithPagination(searchString, pageNum, pageWin);
            jsonObject.put("resourceTypeList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of resource Type", ex);
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * Handles URL for displays Thematic Type List.
     *
     * @return ModelAndView object.
     */
    @RequestMapping(value = "/thematic/list")
    public ModelAndView thematicTypeList() {
        mav = new ModelAndView();
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("title", "Thematic Type List");
        mav.setViewName("thematic-type-list");
        return mav;
    }

    /**
     * Handles URL for displays language list.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/language/list")
    public ModelAndView languageList() {
        mav = new ModelAndView();
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("title", "Language List");
        mav.setViewName("language-list");
        return mav;
    }

    /**
     * To display the Resource form as per Resource Type selection.
     *
     * @param resource {@link Resource}
     * @param resourceTypeId {@link ResourceType}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/getResourceForm/{resourceTypeId}")
    public ModelAndView getResourceForm(@ModelAttribute Resource resource, @PathVariable Long resourceTypeId) {
        mav = new ModelAndView();
        try {

            ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
            mav = resourceService.getInitialFieldsForForm(mav, resourceType);
            switch (resourceType.getResourceTypeCategory().trim()) {
                case Constants.RESOURCE_TYPE_MIT:
                    MetadataIntegrator mit = null;
                    mit = new MetadataIntegrator();
                    List<ClassificationLevel> listOfClassificationLevels = new ArrayList<ClassificationLevel>(Arrays.asList(ClassificationLevel.values()));
                    mit.setResourceType(resourceType);
                    mav.addObject("resource", mit);
                    mav.addObject("classificationLevelList", listOfClassificationLevels);
                    mav.setViewName("resource_library_book");
                    break;
                case Constants.RESOURCE_TYPE_HARVEST:
                    mav.addObject("softwareList", resourceService.getSoftwareList());
                    OpenRepository openRepository;
                    if (resource != null) {
                        if (resource instanceof OpenRepository) {
                            openRepository = (OpenRepository) resource;
                        } else {
                            openRepository = new OpenRepository();
                        }
                    } else {
                        openRepository = new OpenRepository();
                    }
                    openRepository.setResourceType(resourceType);
                    mav.addObject("resource", openRepository);
                    mav.setViewName("resource_harvester");
                    break;
                case Constants.RESOURCE_TYPE_ENEWS:
                    ENews eNews = null;
                    NewsFeeds neewsFeeds = null;
                    if (resource != null) {
                        if (resource instanceof ENews) {
                            eNews = (ENews) resource;
                        } else {
                            eNews = new ENews();
                            neewsFeeds = new NewsFeeds();
                        }
                    } else {
                        eNews = new ENews();
                        neewsFeeds = new NewsFeeds();

                    }
                    eNews.setResourceType(resourceType);
                    mav.addObject("newsFeeds", neewsFeeds);
                    mav.addObject("resource", eNews);
                    mav.setViewName("resource_e_news");
                    break;
                case Constants.RESOURCE_TYPE_WEB_CRAWLER:
                    WebCrawler webCrawler = null;
                    if (resource != null) {
                        if (resource instanceof WebCrawler) {
                            webCrawler = (WebCrawler) resource;
                        } else {
                            webCrawler = new WebCrawler();
                        }
                    } else {
                        webCrawler = new WebCrawler();
                    }
                    webCrawler.setResourceType(resourceType);
                    mav.addObject("resource", webCrawler);
                    mav.setViewName("resource_webcrawler");
                    break;
            }

        } catch (Exception e) {
            LOGGER.error("Error in get Resource form ", e);
        }
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("isEdit", false);
        mav.addObject("resourceIconLocation", resourceIconReadLocation);
        return mav;
    }

    /**
     * Web service call to get the OAR resource information
     *
     * @param openRepository {@link OpenRepository}
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link ModelAndView} object with extracted information
     */
    @RequestMapping(value = "/harvester/get/information", method = RequestMethod.GET)
    public ModelAndView getInfoHarvester(@ModelAttribute OpenRepository openRepository, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        try {
            mav = new ModelAndView();
            openRepository = resourceService.getOpenRepoInfo(openRepository);
            mav = getResourceForm(openRepository, openRepository.getResourceType().getId());
        } catch (Exception e) {
            LOGGER.error("Error in get Repository Information ", e);
        }
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for displays Resource List based on ResourceType Id.
     *
     * @param resourceTypeId {@link ResourceType} primary id/key
     * @return {@link ModelAndView} object.
     */
    @RequestMapping(value = "/register/{resourceTypeId}")
    public ModelAndView resourceRegister(@PathVariable Long resourceTypeId) {
        mav = new ModelAndView();

        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        switch (resourceType.getResourceTypeCategory().trim()) {
            case Constants.RESOURCE_TYPE_MIT:
                mav.setViewName("library-register");
                break;
            case Constants.RESOURCE_TYPE_HARVEST:
                mav.setViewName("oar-register");
                break;
            case Constants.RESOURCE_TYPE_ENEWS:
                mav.setViewName("enews-register");
                break;
            case Constants.RESOURCE_TYPE_WEB_CRAWLER:
                mav.setViewName("webcrawler-register");
                break;
        }
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Resource Type Register", "resource/resourceType/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", resourceType.getResourceType());
        mav.addObject("resourceType", resourceType);
        mav.addObject("resourceTypeId", resourceType.getId());
        mav.addObject("resourceIconLocation", resourceIconReadLocation);
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles url for creating new GroupType
     *
     * @return {@link ModelAndView} object for creation page
     */
    @RequestMapping(value = "/group/create")
    public ModelAndView createGroupType() {
        mav = resourceService.populateAddThematicType();
        mav.setViewName("group-create");
        User user = userService.getCurrentUser();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Group Type List", "resource/group/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Group Type Form");
        mav.addObject("groupIconLocation", groupIconReadLocation);
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Save the newly created Group Type.
     *
     * @param groupType {@link GroupType}
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link ModelAndView} object for creation page
     */
    @RequestMapping(value = "/group/save")
    public RedirectView saveGroupType(@ModelAttribute GroupType groupType, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        File groupIconDir = new File(groupIconLocation);
        String groupIconName = request.getParameter("groupIcon");
        String iconAbsolutePath = groupIconDir.getAbsolutePath();
        String[] fileNameAndExtension = groupIconName.split(Constants.REG_EXP_FOR_EXTENSION);
        boolean isSaved = false;
        try {
            if (fileNameAndExtension[0].equalsIgnoreCase("temp")) {
                String iconName = groupType.getGroupType() + "." + fileNameAndExtension[1];
                String groupIcon = resourceService.saveIcon(iconAbsolutePath, iconName);
                groupType.setGroupIcon(groupIcon);
            } else {
                groupType.setGroupIcon(groupIconName);
            }
            isSaved = resourceService.createNewGroupType(groupType);
        } catch (IOException io) {
            LOGGER.error("Error in gropu Icon save ", io);
        } catch (Exception ex) {
            LOGGER.error("Error in gropu type save", ex);
        }
        redirectAttributes.addFlashAttribute("isSaved", isSaved);
        redirectView = new RedirectView(request.getContextPath() + "/resource/group/create");
        return redirectView;
    }

    /**
     * Handles URL for displays Group Type List.
     *
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/group/list")
    public ModelAndView groupTypeList() {
        mav = new ModelAndView();
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("title", "Group Type List");
        mav.addObject("groupIconLocation", groupIconReadLocation);
        mav.setViewName("group-list");
        return mav;
    }

    /**
     * Handles URL for displays Group wise Resource List.
     *
     * @param pageNum page number
     * @param pageWin window size
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/group/register/{pageNum}/{pageWin}")
    public ModelAndView groupRegister(@PathVariable String pageNum, @PathVariable String pageWin
    ) {
        mav = new ModelAndView();
        Long resultCount = null;
        List<GroupType> groupList = null;
        resultHolder = groupTypeDAO.doPaginateGroupList(pageNum, pageWin);
        resultCount = resultHolder.getResultSize();
        groupList = resultHolder.getResults();
        int pageperrecords = Integer.parseInt(pageWin);
        int totalpages = (resultCount.intValue() / pageperrecords);
        if (resultCount.intValue() % pageperrecords > 0) {
            totalpages++;
        }
        if (resultCount < pageperrecords) {
            totalpages = 0;
        }
        mav.addObject("totalpages", totalpages);
        mav.addObject("resultCount", resultCount);
        mav.addObject("groupList", groupList);
        mav.setViewName("group-register");
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Check url status.
     *
     * @param resource {@link Resource}
     * @return {@link String} status message.
     */
    @RequestMapping(value = "/check/url")
    public @ResponseBody
    String checkURL(@ModelAttribute OpenRepository resource
    ) throws NoSuchAlgorithmException, KeyManagementException {
        String baseURL = resource.getHarvestUrl().trim();
        return resourceService.checkURLStatus(baseURL, resource.getId());
    }

    /**
     * Upload Resource icon
     *
     * @param uploadfile {@link MultipartFile}
     * @param request {@link HttpServletRequest}
     * @return {@link String} uploaded icon file name
     */
    @RequestMapping(value = "/upload/icon", method = RequestMethod.POST)
    public @ResponseBody
    String uploadIcon(@RequestParam("uploadfile") MultipartFile uploadfile, HttpServletRequest request) {
        String reosourceCode = request.getParameter("resourceCode");
        String filePath = resourceService.uploadIcon(resourceIconLocation, reosourceCode, uploadfile.getOriginalFilename());
        FileOutputStream outputStream = null;
        File file = new File(filePath);
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();

        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return file.getName();
    }

    /**
     * To get the Icon on view page. "/get/icon/{value:.+}" in this url pattern
     * :.+ is to accept the file name with extension in path variable
     *
     * @param response {@link HttpServletResponse}
     * @param value file name
     */
    @RequestMapping(value = "/get/icon/{value:.+}", method = RequestMethod.GET)
    public void getIcon(HttpServletResponse response, @PathVariable String value
    ) {
        resourceService.getIcon(resourceIconReadLocation, value, response);
    }

    /**
     * To Remove Icon (work in progress)
     *
     * @param response {@link HttpServletResponse}
     * @param value file name
     */
    @RequestMapping(value = "/remove/icon/{value:.+}", method = RequestMethod.GET)
    public void removeIcon(HttpServletResponse response, @PathVariable String value
    ) {
        File resourceIconDir = new File(resourceIconLocation);
        File file = new File(resourceIconDir.getAbsolutePath() + File.separator + value);
        if (file.exists()) {
            file.deleteOnExit();
        }
    }

    /**
     * Call to perform <ul> <li>Start harvesting</li>
     * <li>Update/Incremental Harvesting</li> </ul>
     * through web-service
     *
     * @param resourceId {@link Resource} primary key/id
     * @param eventType type of event
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link RedirectView} to resource type register
     */
    @RequestMapping(value = "/oar/{eventType}/{resourceId}")
    @ResponseBody
    public String callEventForHarvestURL(@PathVariable Long resourceId, @PathVariable String eventType, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        OpenRepository openRepository = null;
        ResourceStatus status = null;
        String resourceStatus = null;
        List<ResourceStatus> statusList = null;
        try {
            openRepository = openRepositoryDAO.get(resourceId);
            String eventStatus = resourceService.submitEntityToHarvester(openRepository, eventType);
            statusList = resourceStatusDAO.findbyQuery("findByStatusName", eventStatus);
        } catch (Exception e) {
            if (eventType.equalsIgnoreCase("harvest")) {
                resourceStatus = "Unable to Process";
            } else if (eventType.equalsIgnoreCase("harvest")) {
                resourceStatus = "Increment Harvest Processing Error";
            }
            statusList = resourceStatusDAO.findbyQuery("findByStatusName", resourceStatus);
            LOGGER.error("Error in callEventForHarvestURL", e);
            LOGGER.info(" Resource " + " " + eventType + " " + "failed " + ". Try after some time.");

        }
        if (statusList != null && !statusList.isEmpty()) {
            status = statusList.get(0);
        } else {
            status = openRepository.getResourceStatus();
        }
        openRepository.setResourceStatus(status);
        LOGGER.info("Resource status is " + openRepository.getResourceStatus().getStatusName());
        Resource rs = resourceDAO.merge(openRepository);
        resourceStatus = status.getStatusDesc();
        return resourceStatus;
    }

    /**
     * Handles URL To Remove Group
     *
     * @param groupId {@link GroupType}
     * @param request {@link  HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link RedirectView} to Group Register
     */
    @RequestMapping(value = "/group/remove/{groupId}", method = RequestMethod.GET)
    public RedirectView removeGroup(@PathVariable Long groupId, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String grpMessage = null;
        GroupType groupType = groupTypeDAO.findbyQuery("findByGroupId", groupId).get(0);
        if (groupType.getResources().isEmpty()) {
            groupType.getResourceTypes().removeAll(groupType.getResourceTypes());
            resourceService.removeGroup(groupType);
            LOGGER.info(groupType.getGroupType() + " group is removed successfully.");
            grpMessage = groupType.getGroupType() + " group is removed successfully.";
        } else {
            LOGGER.info(groupType.getGroupType() + " group is already associated with resource not possible to delete.");
            grpMessage = groupType.getGroupType() + " group is already associated with resource not possible to delete.";
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/group/list");
        redirectAttributes.addFlashAttribute("grpMessage", grpMessage);
        return redirectView;
    }

    /**
     * Handles URL to Edit Group
     *
     * @param groupId {@link GroupType} primary key/Id
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/group/populate/{groupId}", method = RequestMethod.GET)
    public ModelAndView editGroupByID(@PathVariable Long groupId) {
        mav = new ModelAndView();
        GroupType groupType = groupTypeDAO.findbyQuery("findByGroupId", groupId).get(0);
        List<ResourceType> resourceTypesList = resourceService.getResouceTypeList();
        mav.addObject("resourceTypesList", resourceTypesList);
        mav.addObject("groupType", groupType);
        mav.setViewName("group-edit");
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Group Type List", "resource/group/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Group Type Form");
        mav.addObject("groupIconLocation", groupIconReadLocation);
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Update the Existing Group and Associated Resource Types.
     *
     * @param groupId
     * @param groupType {@link GroupType}
     * @param request {@link HttpServletRequest}
     * @return {@link ModelAndView} for creation page
     */
    @RequestMapping(value = "/group/edit/{groupId}", method = RequestMethod.GET)
    public RedirectView editGroup(@PathVariable Long groupId, @ModelAttribute GroupType groupType, HttpServletRequest request) {

        File groupIconDir = new File(groupIconLocation);
        List<String> extentionList = new ArrayList();
        extentionList.add("jpg");
        extentionList.add("png");
        for (String extention : extentionList) {
            String tempFileName = "temp." + extention;
            File tempFile = new File(groupIconDir.getAbsolutePath() + File.separator + tempFileName);
            try {
                if (tempFile.exists()) {
                    String groupIconName = tempFileName.replace(tempFileName, groupType.getGroupType() + "." + extention);
                    File groupIcon = new File(groupIconDir.getAbsolutePath() + File.separator + groupIconName);
                    FileUtils.copyFile(tempFile, groupIcon);
                    groupType.setGroupIcon(groupIconName);
                }
            } catch (Exception e) {
                LOGGER.error("Error in editGroup", e);
            }
        }
        if (resourceService.editGroup(groupType)) {
            redirectView = new RedirectView(request.getContextPath() + "/resource/group/list");
            LOGGER.info("group is Updated Successfully");
        } else {
            resourceService.editGroup(groupType);
            redirectView = new RedirectView(request.getContextPath() + "/group/populate/" + groupId);
        }
        return redirectView;
    }

    /**
     * Handle URLs to Remove Thematic Type
     *
     * @param thematicTypeId {@link ThematicType}
     * @param request {@link  HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link RedirectView} to Thematic Register
     */
    @RequestMapping(value = "/thematic/remove/{thematicTypeId}", method = RequestMethod.GET)
    public RedirectView removeThematic(@PathVariable Long thematicTypeId, HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String thematicMessage = null;
        ThematicType thematicType = thematicTypeDAO.findbyQuery("findByThematicId", thematicTypeId).get(0);
        System.out.println("theme hi....");
        if (thematicType.getResources().isEmpty()) {
            thematicType.getResourceTypes().removeAll(thematicType.getResourceTypes());
            System.out.println("not coming hi....");
            resourceService.removeThematic(thematicType);
            LOGGER.info("Thematic is removed successfully");
            thematicMessage = thematicType.getThematicType() + " Thematic Type is removed successfully.";
        } else {
            LOGGER.info("This Thematic is already Associated with Resource Not Possible to delete");
            thematicMessage = thematicType.getThematicType() + " Thematic Type is already associated with resource not possible to delete.";
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/thematic/list");
        redirectAttributes.addFlashAttribute("thematicMessage", thematicMessage);
        return redirectView;
    }

    /**
     * Handle URLs to Edit Thematic Type
     *
     * @param thematicTypeId {@link ThematicType}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/thematic/populate/{thematicTypeId}", method = RequestMethod.GET)
    public ModelAndView editThematicTypeById(@PathVariable Long thematicTypeId
    ) {
        mav = new ModelAndView();
        ThematicType thematicType = thematicTypeDAO.findbyQuery("findByThematicId", thematicTypeId).get(0);
        List<ResourceType> resourceTypesList = resourceService.getResouceTypeList();
        mav.addObject("resourceTypesList", resourceTypesList);
        mav.addObject("thematicType", thematicType);
        mav.setViewName("thematic-type-edit");
        User user = userService.getCurrentUser();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Thematic Type List", "resource/thematic/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Thematic Form");
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Update the Existing Group and Associated Resource Types.
     *
     * @param thematicTypeId {@link ThematicType} primary key/id
     * @param thematicType {@link ThematicType}
     * @param request {@link HttpServletRequest}
     * @return {@link ModelAndView} for creation page
     */
    @RequestMapping(value = "/thematic/edit/{thematicTypeId}", method = RequestMethod.GET)
    public RedirectView editThematic(@PathVariable Long thematicTypeId, @ModelAttribute ThematicType thematicType, HttpServletRequest request
    ) {
        if (resourceService.editThematic(thematicType)) {
            redirectView = new RedirectView(request.getContextPath() + "/resource/thematic/list");
        } else {
            redirectView = new RedirectView(request.getContextPath() + "thematic/populate/" + thematicTypeId);
        }
        return redirectView;
    }

    /**
     * Call operation for selected Repositories. <ul> <li>Remove</li>
     * <li>Harvest</li> </ul>
     *
     * @param eventType {@link  String} type of event
     * @param request {@link HttpServletRequest}
     * @return {@link RedirectView} to resource type register
     */
    @RequestMapping(value = "/{eventType}/selected")
    public RedirectView callOperationOnSelectedRepo(@PathVariable String eventType, HttpServletRequest request) {
        String[] folderNamesCommaSeparated = request.getParameterValues("harvestRepoCh");
        resourceService.operationSelectedHarvestURL(eventType, StringUtils.join(folderNamesCommaSeparated, ","));
        Long resourceTypeId = Long.parseLong(request.getParameter("resourceTypeId"));
        redirectView = new RedirectView(request.getContextPath() + "/resource/register/" + resourceTypeId);
        return redirectView;
    }

    /**
     * Edit Resource Form
     *
     * @param resourceCode {@link Resource} code
     * @param type
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/edit/{resourceCode}")
    public ModelAndView editResourceForm(@PathVariable String resourceCode, @RequestParam(required = false) String type) {
        try {
            Resource resource = resourceDAO.findbyQuery("findByResourceCode", resourceCode).get(0);
            mav.addObject("resource", resource);
            mav.addObject("resourceTypeList", resourceTypeDAO.list());
            mav.addObject("isEdit", true);
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("Resource Type Register", "resource/resourceType/list");
            ResourceType rt = resource.getResourceType();
            if (type.equals("1")) {
                breadcrumbMap.put(rt.getResourceType() + " Draft Register", "resource/draft/register/" + rt.getId());
                mav.addObject("isDraft", true);
            } else {
                breadcrumbMap.put(rt.getResourceType(), "resource/register/" + rt.getId());
                mav.addObject("isDraft", false);
            }
            mav.addObject("breadcrumbMap", breadcrumbMap);
            mav.addObject("title", "Resource Form");
            mav.setViewName("resource-create");
            mav.addObject("resourceIconLocation", resourceIconReadLocation);
            User user = userService.getCurrentUser();
            mav.addObject("user", user);
        } catch (Exception ex) {
            LOGGER.error("Exception in editResourceForm: ", ex);
        }
        return mav;
    }

    /**
     * To display the Resource form as per Resource Type selection.
     *
     * @param resourceId {@link Resource} primary key/id
     * @param resourceTypeId {@link ResourceType} primary key/id
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/getResourceForm/edit/{resourceTypeId}/{resourceId}")
    public ModelAndView getResourceFormEdit(@PathVariable Long resourceId, @PathVariable Long resourceTypeId, @RequestParam(required = false) String type) {
        mav = new ModelAndView();
        try {
            ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
            Set<UDCLanguage> languages = null;
            Set<ContentType> contentTypes = null;
            Set<ThematicType> thematicTypes = null;
            switch (resourceType.getResourceTypeCategory().trim()) {
                case Constants.RESOURCE_TYPE_MIT:
                    MetadataIntegrator editMitResource = metadataIntegratorDAO.get(resourceId);
                    System.out.println("edit");
                    languages = editMitResource.getLanguages();
                    contentTypes = editMitResource.getContentTypes();
                    thematicTypes = editMitResource.getThematicTypes();
                    mav.addObject("resource", editMitResource);
                    List<ClassificationLevel> listOfClassificationLevels = new ArrayList<ClassificationLevel>(Arrays.asList(ClassificationLevel.values()));
                    mav.addObject("classificationLevelList", listOfClassificationLevels);
                    mav.setViewName("resource_library_book");
                    break;
                case Constants.RESOURCE_TYPE_HARVEST:
                    OpenRepository editOpenRepoResource = openRepositoryDAO.get(resourceId);
                    editOpenRepoResource = resourceService.getInfoHarvesterForEdit(editOpenRepoResource);
                    languages = editOpenRepoResource.getLanguages();
                    contentTypes = editOpenRepoResource.getContentTypes();
                    thematicTypes = editOpenRepoResource.getThematicTypes();
                    mav.addObject("softwareList", resourceService.getSoftwareList());
                    mav.addObject("resource", editOpenRepoResource);
                    mav.setViewName("resource_harvester");
                    break;
                case Constants.RESOURCE_TYPE_ENEWS:
                    ENews editNewsResource = eNewsDAO.get(resourceId);
                    languages = editNewsResource.getLanguages();
                    contentTypes = editNewsResource.getContentTypes();
                    thematicTypes = editNewsResource.getThematicTypes();
                    ObjectMapper mapper = new ObjectMapper();
                    String newsFeedJson = editNewsResource.getNewsFeed();
                    List<NewsFeed> listOfNewsFeeds = null;
                    if (newsFeedJson != null && !("").equals(newsFeedJson)) {
                        listOfNewsFeeds = mapper.readValue(newsFeedJson, new TypeReference<List<NewsFeed>>() {
                        });
                    } else {
                        listOfNewsFeeds = new ArrayList<>();
                    }
                    NewsFeeds newsFeeds = new NewsFeeds();
                    newsFeeds.setNewsFeeds(listOfNewsFeeds);
                    mav.addObject("resource", editNewsResource);
                    mav.addObject("newsFeeds", newsFeeds);
                    mav.setViewName("resource_e_news");
                    break;
                case Constants.RESOURCE_TYPE_WEB_CRAWLER:
                    WebCrawler editWebCrawlerResource = webCrawlerDAO.get(resourceId);
                    contentTypes = editWebCrawlerResource.getContentTypes();
                    thematicTypes = editWebCrawlerResource.getThematicTypes();
                    languages = editWebCrawlerResource.getLanguages();
                    mav.addObject("resource", editWebCrawlerResource);
                    mav.setViewName("resource_webcrawler");
                    break;

            }
            mav.addObject("isEdit", true);
            if (type != null && type.equals("1")) {
                mav.addObject("isDraft", true);
            } else {
                mav.addObject("isDraft", false);
            }

            mav = resourceService.getInitialFieldsForForm(mav, resourceType);
            if (languages != null) {
                mav.addObject("addedLangaugeList", new ArrayList<>(languages));
            }

            if (contentTypes != null) {
                mav.addObject("alreadyAssingedCTList", new ArrayList<>(contentTypes));
            }

            if (thematicTypes != null) {
                mav.addObject("alreadyAssingedTTList", new ArrayList<>(thematicTypes));
            }
        } catch (Exception e) {
            LOGGER.error("Resource Form Edit: ", e);
        }
        mav.addObject("resourceIconLocation", resourceIconReadLocation);
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Update Resource.
     *
     * @param resourceId {@link Resource} primary key/id
     * @param resource {@link Resource}
     * @param request {@link HttpServletRequest}
     * @return {@link ModelAndView} object for creation page
     */
    @RequestMapping(value = "/update/{resourceId}", method = RequestMethod.POST)
    public RedirectView updateResource(@PathVariable Long resourceId, @ModelAttribute Resource resource, HttpServletRequest request, RedirectAttributes redirectAttributes) {

        String rescMessage = null;
        Long resourceTypeId = resource.getResourceType().getId();
        try {
            //Set null to ContentType, ThematicType and lanuages after removing all objects from list.
            if (request.getParameter("contentTypes") == null) {
                resource.setContentTypes(null);
            }
            if (request.getParameter("thematicTypes") == null) {
                resource.setThematicTypes(null);
            }
            if (request.getParameter("languages") == null) {
                resource.setLanguages(null);
            }
            short resourceStatus = resourceService.editResource(resourceId, resource);

            if (resourceStatus == (short) 1) {
                rescMessage = resource.getResourceName() + "  this Resource is updated succesfully.";

            } else {
                rescMessage = resource.getResourceName() + "  this Resource is not updated. Try after some time.";
            }
        } catch (Exception e) {
            LOGGER.error("Error in updating resource." + e);
            rescMessage = resource.getResourceName() + "  this Resource is not updated. Try after some time.";

        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/register/" + resourceTypeId);
        redirectAttributes.addFlashAttribute("rescMessage", rescMessage);

        return redirectView;
    }

    /**
     * Check resource name is duplicate or not.
     *
     * @param resourceTypeId {@link ResourceType} primary key/id
     * @param resourceName {@link Resource} name
     * @param resourceId {@link Resource} primary key/id
     * @return if resource name is available returns true otherwise false
     */
    @RequestMapping(value = "/resourcename/check/{resourceTypeId}")
    @ResponseBody
    public boolean checkResourceName(@PathVariable Long resourceTypeId, @RequestParam String resourceName, @RequestParam String resourceId
    ) {
        boolean isDuplicate = false;
        try {
            ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
            System.out.println("resourceType " + resourceType.getResourceType() + resourceId);
            Resource resource = resourceDAO.getResourceByNameAndRT(resourceName, resourceType);
            if (resource != null) {
                if (resourceId != null) {
                    if (!resource.getId().equals(Long.valueOf(resourceId))) {
                        isDuplicate = true;
                        System.out.println("its true");
                    }
                } else {
                    isDuplicate = true;
                }
                System.out.println("resource..................." + resource.getResourceName());
            }

        } catch (Exception e) {
            LOGGER.error("Error in find duplicate resource name" + e);
            isDuplicate = true;
        }
        return isDuplicate;

    }

    /**
     * Get the draft register for resources.
     *
     * @param resourceTypeId {@link ResourceType} primary key/id
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "draft/register/{resourceTypeId}")
    public ModelAndView getDraftOrRegister(@PathVariable Long resourceTypeId
    ) {
        mav = new ModelAndView();

        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        switch (resourceType.getResourceTypeCategory().trim()) {
            case Constants.RESOURCE_TYPE_MIT:
                mav.setViewName("library-draft-register");
                break;
            case Constants.RESOURCE_TYPE_HARVEST:
                List<OpenRepository> listOR = resultHolder.getResults();
                mav.addObject("resourceList", listOR);
                mav.setViewName("draft-or-register");
                break;
            case Constants.RESOURCE_TYPE_ENEWS:
                mav.setViewName("enews-draft-register");
                break;
            case Constants.RESOURCE_TYPE_WEB_CRAWLER:
                mav.setViewName("webcrawler-draft-register");
                break;

        }
        User user = userService.getCurrentUser();
        mav.addObject("title", resourceType.getResourceType() + " Draft Register");
        mav.addObject("user", user);
        mav.addObject("resourceType", resourceType);
        mav.addObject("resourceTypeId", resourceType.getId());
        return mav;
    }

    /**
     * Check draft url status.
     *
     * @param resourceId {@link Resource} primary key/id
     * @return {@link Stirng} status message.
     */
    @RequestMapping(value = "/check/draft/url/{resourceId}")
    public @ResponseBody
    String checkDraftURL(@PathVariable Long resourceId
    ) throws NoSuchAlgorithmException, KeyManagementException {
        OpenRepository openRepository = openRepositoryDAO.get(resourceId);
        return resourceService.checkDraftURL(openRepository.getHarvestUrl());
    }

    /**
     * Call for Re-submit repositories.
     *
     * @param resourceId {@link Resource} primary key/id
     * @param resourceTypeId {@link ResourceType} primary key/id
     * @param request {@link HttpServletRequest}
     * @return {@link RedirectView}
     */
    @RequestMapping(value = "/resubmit/{resourceTypeId}/{resourceId}")
    public RedirectView resubmitOrRepositories(@PathVariable Long resourceId, @PathVariable Long resourceTypeId, HttpServletRequest request
    ) {
        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        ResourceStatus resourceStatus = null;
        try {
            switch (resourceType.getResourceTypeCategory().trim()) {
                case Constants.RESOURCE_TYPE_MIT:
                    //  resourceStatus = resourceService.submitDraftMit(resourceId);
                    break;
                case Constants.RESOURCE_TYPE_HARVEST:
                    //resourceStatus = resourceService.submitDraftRepository(resourceId);
                    break;
                case Constants.RESOURCE_TYPE_ENEWS:
                    //   resourceStatus = resourceService.submitDraftEnews(resourceId);
                    break;
                case Constants.RESOURCE_TYPE_WEB_CRAWLER:
                    //resourceStatus = resourceService.submitDraftWebcrawler(resourceId);
                    break;
            }
        } catch (Exception e) {
            LOGGER.error("Error in resubmit Resource " + e);
        }
        if (resourceStatus.getStatusCode() == (short) 1) {
            redirectView = new RedirectView(request.getContextPath() + "/resource/register/" + resourceTypeId);
        } else {
            redirectView = new RedirectView(request.getContextPath() + "/resource/draft/register/" + resourceTypeId);
        }
        return redirectView;
    }

    /**
     * Web service call to get the OAR resource information
     *
     * @param resourceId {@link Resource} primary key/id
     * @param request {@link HttpServletRequest}
     * @return {@link String} status message.
     */
    @RequestMapping(value = "/harvester/get/information/{resourceId}", method = RequestMethod.GET)
    public @ResponseBody
    String getDraftInfoHarvester(@PathVariable Long resourceId, HttpServletRequest request
    ) {
        String status = null;
        try {
            OpenRepository openRepository = openRepositoryDAO.get(resourceId);
            IdentifyType identifyType = resourceService.communicateWithHarvester(openRepository.getHarvestUrl());
            if (identifyType != null) {
                status = "200";
            } else {
                status = "Exception";
            }
        } catch (Exception e) {
            LOGGER.error("Error in getDraftInfoHarvester: ", e);
        }
        return status;
    }

    @RequestMapping(value = "/syncRepoStatus", method = RequestMethod.POST)
    @ResponseBody
    public List<HarRepoCustomised> SyncOpenRepositoriesStatusFromHarvester() {
        List<HarRepoCustomised> repoList = resourceService.getRepoStatusFromHar();
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(repoList);
            repoList = mapper.readValue(json, new TypeReference<List<HarRepoCustomised>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        Resource resource = new Resource();
        if (repoList != null) {
            for (HarRepoCustomised harRepoCustomised : repoList) {
                String repoUid = harRepoCustomised.getRepoUID();
                if (repoUid != null) {
                    resource = resourceDAO.getResourceByCode(repoUid);
                    if (resource != null) {
                        int statusCode = harRepoCustomised.getRepoStatusId();
                        resource.setResourceStatus(resourceStatusDAO.get(Long.valueOf(statusCode)));
                        resourceDAO.update(resource);
                    }
                }
            }
        }
        return repoList;

    }

    /**
     * Set parameters to draft repository through web-service.
     *
     * @param resourceId {@link Resource} primary key/id
     * @param request {@link HttpServletRequest}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/set/draft/{resourceId}", method = RequestMethod.POST)
    public ModelAndView setDraftInfoHarvester(@PathVariable Long resourceId, HttpServletRequest request
    ) {
        mav = new ModelAndView();
        try {
            OpenRepository openRepository = openRepositoryDAO.get(resourceId);
            openRepository = resourceService.getOpenRepoInfo(openRepository);
            mav.addObject("resource", openRepository);
            mav.addObject("resourceTypeList", resourceService.getResouceTypeList());
            mav.addObject("isEdit", true);
            mav.addObject("isDraft", true);
            mav.setViewName("resource-create");
        } catch (Exception e) {
            LOGGER.error("Error in setDraftInfoHarvester: ", e);
        }
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Save drafted repository after url check.
     *
     * @param resource {@link Resource}
     * @param resourceId {@link Resource} primary key/id
     * @param request {@link HttpServletRequest}
     * @return {@link RedirectView}
     */
    @RequestMapping(value = "/update/draft/{resourceId}", method = RequestMethod.POST)
    public RedirectView updateDraftInfoHarvester(@ModelAttribute Resource resource, @PathVariable Long resourceId, HttpServletRequest request
    ) {
        try {
            if (request.getParameter("contentTypes") == null) {
                resource.setContentTypes(null);
            }
            if (request.getParameter("thematicTypes") == null) {
                resource.setThematicTypes(null);
            }
            if (request.getParameter("languages") == null) {
                resource.setLanguages(null);
            }
            System.out.println("in update dreaft" + resource.getContentTypes());
            //ResourceStatus resourceStatus = 
            resourceService.editResource(resourceId, resource);
            Long resourceTypeId = resource.getResourceType().getId();
//            if (resourceStatus.getStatusCode() == (short) 1) {
//                redirectView = new RedirectView(request.getContextPath() + "/resource/register/" + resourceTypeId);
//            } else {
//                redirectView = new RedirectView(request.getContextPath() + "/resource/draft/register/" + resourceTypeId);
//            }
            //redirectView = resubmitOrRepositories(resourceId, resource.getResourceType().getId(), request);
        } catch (Exception e) {
            LOGGER.error("Error in updateDraftInfoHarvester: ", e);
        }
        return redirectView;
    }

    /**
     * Save News Feeds
     *
     * @param newsFeeds {@link NewsFeeds}
     * @return json {@link String} of List of {@link NewsFeed}
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/newsfeed/save", produces = "text/plain;charset=UTF-8")
    public @ResponseBody
    String saveNewsFeeds(@ModelAttribute NewsFeeds newsFeeds) throws JsonProcessingException {
        String jsonNewsFeed = null;
        ObjectMapper mapper = new ObjectMapper();
        List<NewsFeed> list = newsFeeds.getNewsFeeds();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLabelName() == null || list.get(i).getFeedUrl() == null) {
                newsFeeds.getNewsFeeds().remove(i);
            }
        }
        newsFeeds.getNewsFeeds().removeAll(Collections.singleton(null));
        jsonNewsFeed = mapper.writeValueAsString(newsFeeds.getNewsFeeds());
        return jsonNewsFeed;
    }

    /**
     * checkDuplicateGroupName method used to check group name is duplicate or
     * not.
     *
     * @param groupName {@link GroupType}
     * @return boolean if group name is duplicate returns true otherwise false.
     */
    @RequestMapping(value = "/checkduplicate/group/{groupName}")
    @ResponseBody
    public boolean checkDuplicateGroupName(@RequestParam String groupName
    ) {
        boolean isDuplicate = false;
        try {
            GroupType groupType = groupTypeDAO.getGroupTypeByGroupName(groupName);
            if (!groupType.getGroupType().isEmpty() && groupType.getGroupType().equalsIgnoreCase(groupName)) {
                isDuplicate = true;
                LOGGER.info("duplicate Group Name found");
                return isDuplicate;
            }
        } catch (Exception e) {
            LOGGER.error("Duplicate entry for Group Name", e);
        }
        return isDuplicate;
    }

    /**
     * checkDuplicateThemename method used to check Theme name is duplicate or
     * not.
     *
     * @param themeName {@link ThematicType}
     * @return boolean if theme name is duplicate returns true otherwise false.
     */
    @RequestMapping(value = "/checkduplicate/theme/{themeName}")
    @ResponseBody
    public boolean checkDuplicateThemeName(@RequestParam String themeName
    ) {
        boolean isDuplicate = false;
        try {
            ThematicType thematicType = thematicTypeDAO.getThematicTypeByThematicName(themeName);
            if (!thematicType.getThematicType().isEmpty() && thematicType.getThematicType().equalsIgnoreCase(themeName)) {
                isDuplicate = true;
                LOGGER.info("duplicate thematic name found");
                return isDuplicate;
            }
        } catch (Exception e) {
            LOGGER.error("Duplicate entry for thematic name", e);
        }
        return isDuplicate;
    }

    /**
     * checkDuplicateContentname method used to check Content name is duplicate
     * or not.
     *
     * @param contentName {@link ContentType}
     * @return if content type name is duplicate returns true otherwise false.
     */
    @RequestMapping(value = "/checkduplicate/content/{contentName}")
    @ResponseBody
    public boolean checkDuplicateContentName(@RequestParam String contentName
    ) {
        boolean isDuplicate = false;
        try {
            ContentType contentType = contentTypeDAO.getContentTypeByContentName(contentName);
            if (!contentType.getContentType().isEmpty() && contentType.getContentType().equalsIgnoreCase(contentName)) {
                isDuplicate = true;
                LOGGER.info("duplicate content name found");
                return isDuplicate;
            }
        } catch (Exception e) {
            LOGGER.error("Duplicate entry for content Name", e);
        }
        return isDuplicate;
    }

    /**
     * checkDuplicateLanguagename method used to check Language name is
     * duplicate or not.
     *
     * @param languageName {@link String}name of language
     * @return if language name is duplicate returns true otherwise false
     */
    @RequestMapping(value = "/checkduplicate/languagename/{languageName}")
    @ResponseBody
    public boolean checkDuplicateLanguageName(@RequestParam String languageName
    ) {
        boolean isDuplicate = false;
        try {
            UDCLanguage uDCLanguage = languageDAO.getLanguageByLanguageName(languageName);
            if (!uDCLanguage.getLanguageName().isEmpty() && uDCLanguage.getLanguageName().equalsIgnoreCase(languageName)) {
                isDuplicate = true;
                LOGGER.info("duplicate language name found");
                return isDuplicate;
            }
        } catch (Exception e) {
            LOGGER.error("Duplicate entry for Language Name", e);
        }
        return isDuplicate;
    }

    /**
     * checkDuplicateLanguageCode method used to check Language name is
     * duplicate or not.
     *
     * @param languageCode {@link String}code of language
     * @return if language code is duplicate returns true otherwise false
     */
    @RequestMapping(value = "/checkduplicate/languagecode/{languageCode}")
    @ResponseBody
    public boolean checkDuplicateLanguageCode(@RequestParam String languageCode
    ) {
        boolean isDuplicate = false;
        try {
            UDCLanguage uDCLanguage = languageDAO.getObjectByCode(languageCode);
            if (!uDCLanguage.getLanguageCode().isEmpty() && uDCLanguage.getLanguageCode().equalsIgnoreCase(languageCode)) {
                isDuplicate = true;
                LOGGER.info("duplicate language code found");
                return isDuplicate;
            }
        } catch (Exception e) {
            LOGGER.error("Duplicate entry for Language code", e);
        }
        return isDuplicate;
    }

    /**
     * Populate form for edit ResourceType.
     *
     * @param resourceTypeId {@link ResourceType} primary key/Id
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/type/edit/{resourceTypeId}")
    public ModelAndView editResourceType(@PathVariable Long resourceTypeId
    ) {
        try {
            mav = new ModelAndView();
            ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
            List<String> categoryForResourceType = resourceService.getCategoryListForResourceType();
            List<UDCLanguage> listOfLang = resourceService.getIndianLanguageList();
            UDCLanguage engLanguage = languageDAO.getLanguageByLanguageName("English");
            listOfLang.remove(engLanguage);
            mav.addObject("engLanguage", engLanguage);
            mav.addObject("resourceType", resourceType);
            mav.addObject("isEditType", true);
            mav.addObject("categoryList", categoryForResourceType);
            mav.addObject("languageList", listOfLang);
            mav.setViewName("resource-resourcetype-create");
            mav.addObject("rtIconDefalutLocation", resourceTypeIconDefalutReadLocation);
            mav.addObject("rtIconSearchLocation", resourceTypeIconSearchReadLocation);
            mav.addObject("rtIconLocation", resourceTypeIconReadLocation);
            User user = userService.getCurrentUser();
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("Resource Type Register", "resource/resourceType/list");
            mav.addObject("breadcrumbMap", breadcrumbMap);
            mav.addObject("title", "Resource Type Form");
            mav.addObject("user", user);
        } catch (Exception ex) {
            LOGGER.error("Error in Reosurce Type Create/Edit", ex);
        }
        return mav;
    }

    /**
     * Save updated data of Resource Type.
     *
     * @param resourceType {@link ResourceType}
     * @param resourceTypeId {@link ResourceType} primary key/Id
     * @param request {@link HttpServletRequest}
     * @return {@link RedirectView}
     */
    @RequestMapping(value = "/type/edit/submit/{resourceTypeId}")
    public RedirectView editSubmitResourceType(@ModelAttribute ResourceType resourceType, @PathVariable Long resourceTypeId, HttpServletRequest request
    ) {
        try {
            ResourceType rType = resourceService.updateResourceType(resourceType, resourceTypeId);
            Map<String, ResourceType> resourceTypesMap = (Map<String, ResourceType>) request.getServletContext().getAttribute("resourceTypes");
            request.getServletContext().setAttribute("resourceTypes", resourceService.updateResourceTypeMap(resourceTypesMap, rType));
        } catch (Exception e) {
            LOGGER.error("Error in submit Resource Type Edit Form.", e);
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/resourceType/list");
        return redirectView;
    }

    /**
     * Remove ResourceTypeInfo.
     *
     * @param resourceTypeId {@link ResourceType} primary key/Id
     * @param index index number of {@link ResourceType}
     * @param request {@link HttpServletRequest}
     * @return {@link String} status message
     */
    @RequestMapping(value = "/type/remove/info/{resourceTypeId}/{index}")
    @ResponseBody
    public String removeResourceTypeInfo(@PathVariable Long resourceTypeId, @PathVariable Long index, HttpServletRequest request
    ) {
        String status = null;
        try {
            ResourceType preResourceType = resourceTypeDAO.get(resourceTypeId);
            if (preResourceType.getResourceTypeInfo() != null && !preResourceType.getResourceTypeInfo().isEmpty()) {
                ResourceTypeInfo resourceTypeInfo = resourceTypeInfoDAO.get(index);
                preResourceType.getResourceTypeInfo().remove(resourceTypeInfo);
                resourceTypeInfoDAO.delete(resourceTypeInfo);
                resourceTypeDAO.merge(preResourceType);
                Map<String, ResourceType> resourceTypesMap = (Map<String, ResourceType>) request.getServletContext().getAttribute("resourceTypes");
                request.getServletContext().setAttribute("resourceTypes", resourceService.updateResourceTypeMap(resourceTypesMap, preResourceType));
                status = "done";
            }
        } catch (Exception e) {
            status = "not removed";
            LOGGER.error("Error in removeResourceTypeInfo", e);
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/resourceType/list");
        return status;
    }

    /**
     * Upload Resource icon
     *
     * @param uploadfile {@link MultipartFile} to upload
     * @param request {@link HttpServletRequest}
     * @return {@link String} Icon file name
     */
    @RequestMapping(value = "/upload/group/icon", method = RequestMethod.POST)
    public @ResponseBody
    String uploadGroupIcon(@RequestParam("uploadfile") MultipartFile uploadfile, HttpServletRequest request
    ) {
        String groupType = request.getParameter("groupTypeName");
        String filePath = resourceService.uploadIcon(groupIconLocation, groupType, uploadfile.getOriginalFilename());
        FileOutputStream outputStream = null;
        File file = new File(filePath);
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();
        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return file.getName();
    }

    /**
     * To get the Icon on view page. "/get/icon/{value:.+}" in this url pattern
     * :.+ is to accept the file name with extension in path variable
     *
     * @param response {@link HttpServletResponse}
     * @param value file name
     */
    @RequestMapping(value = "/get/group/icon/{value:.+}", method = RequestMethod.GET)
    public void getGroupIcon(HttpServletResponse response, @PathVariable String value
    ) {
        resourceService.getIcon(groupIconReadLocation, value, response);
    }

    /**
     * Auto-Captures feed URLs for provided news URL.
     *
     * @param newsFeeds {@link NewsFeeds}
     * @param request {@link HttpServletRequest}
     * @return {@link List} of {@link NewsFeed}
     */
    @RequestMapping(value = "/autocapture/newsfeed")
    public @ResponseBody
    List<NewsFeed> autoCaptureFeeds(@ModelAttribute NewsFeeds newsFeeds, HttpServletRequest request
    ) {
        List<NewsFeed> allNewsFeedUrls = new ArrayList<>();
        String newMainFeedUrl = null;
        try {
            newMainFeedUrl = request.getParameter("feedUrlInForm");
            String organisationUrl = request.getParameter("organisationUrlFeed");
            allNewsFeedUrls = resourceService.autoCapureFeeds(organisationUrl, newMainFeedUrl);
        } catch (SocketTimeoutException e) {
            LOGGER.error("Time out for url " + newMainFeedUrl, e);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return allNewsFeedUrls;
    }

    /**
     * Refresh feed urls for provided news URL.
     *
     * @param resourceCode {@link Resource} code
     * @param request {@link HttpServletRequest}
     * @return {@link List} of {@link NewsFeed}
     */
    @RequestMapping(value = "/refresh/autocapture/newsfeed/{resourceCode}")
    public @ResponseBody
    List<NewsFeed> refreshCaptureFeeds(@PathVariable String resourceCode, HttpServletRequest request
    ) {
        ENews resource = eNewsDAO.findbyQuery("findByResourceCode", resourceCode).get(0);
        List<NewsFeed> allNewsFeedUrls = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        String newsFeedJson = resource.getNewsFeed();
        List<NewsFeed> oldNewsFeedUrls = null;
        try {
            if (newsFeedJson != null && !("").equals(newsFeedJson)) {
                try {
                    oldNewsFeedUrls = mapper.readValue(newsFeedJson, new TypeReference<List<NewsFeed>>() {
                    });
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            } else {
                oldNewsFeedUrls = new ArrayList<>();
            }
            NewsFeeds oldNewsFeeds = new NewsFeeds();
            oldNewsFeeds.setNewsFeeds(oldNewsFeedUrls);
            allNewsFeedUrls = resourceService.autoCapureFeeds(resource.getOrganization().getOrganisationUrl(), resource.getNewsFeedUrl());//autoCaptureFeeds(oldNewsFeeds, request);
            for (NewsFeed oldNewsFeed : oldNewsFeeds.getNewsFeeds()) {
                if (!allNewsFeedUrls.contains(oldNewsFeed)) {

                    oldNewsFeed.setActiveFeed(false);
                    allNewsFeedUrls.add(oldNewsFeed);
                }
            }
        } catch (SocketTimeoutException e) {
            LOGGER.error("Time out for url " + resource.getNewsFeedUrl(), e);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        return allNewsFeedUrls;

    }

    /**
     * Save refresh feed urls
     *
     * @param newsFeeds {@link NewsFeeds}
     * @param resourceCode {@link Resource} code
     * @return Josn {@link String} of List of {@link NewsFeed}
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/refresh/newsfeed/save/{resourceCode}", produces = "text/plain;charset=UTF-8")
    public @ResponseBody
    String saveRefreshNewsFeeds(@ModelAttribute NewsFeeds newsFeeds, @PathVariable String resourceCode) throws JsonProcessingException {
        String jsonNewsFeed;
        ObjectMapper mapper = new ObjectMapper();
        List<NewsFeed> list = newsFeeds.getNewsFeeds();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLabelName() == null || list.get(i).getFeedUrl() == null) {
                newsFeeds.getNewsFeeds().remove(i);
            }
        }
        newsFeeds.getNewsFeeds().removeAll(Collections.singleton(null));
        jsonNewsFeed = mapper.writeValueAsString(newsFeeds.getNewsFeeds());
        ENews resource = eNewsDAO.findbyQuery("findByResourceCode", resourceCode).get(0);
        resource.setNewsFeed(jsonNewsFeed);
        eNewsDAO.merge(resource);
        return jsonNewsFeed;
    }

    /**
     * Upload Resource icon
     *
     * @param uploadfile {@link MultipartFile} file to upload
     * @param request {@link HttpServletRequest}
     * @return {@link String} Icon file name
     */
    @RequestMapping(value = "/type/upload/icon", method = RequestMethod.POST)
    public @ResponseBody
    String uploadResourceTypeIcon(@RequestParam("uploadfile") MultipartFile uploadfile, HttpServletRequest request
    ) {
        String reosourceType = request.getParameter("resourceType");
        String filePath = resourceService.uploadIcon(resourceTypeIconLocation, reosourceType, uploadfile.getOriginalFilename());
        FileOutputStream outputStream = null;
        File file = new File(filePath);
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();

        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return file.getName();

    }

    /**
     * To get the Icon on view page. "/get/icon/{value:.+}" in this url pattern
     * :.+ is to accept the file name with extension in path variable
     *
     * @param response {@link HttpServletResponse}
     * @param value file name
     */
    @RequestMapping(value = "/type/get/icon/{value:.+}", method = RequestMethod.GET)
    public void getResourceTypeIcon(HttpServletResponse response, @PathVariable String value
    ) {
        resourceService.getIcon(resourceTypeIconReadLocation, value, response);
    }

    /**
     * Find duplicate resource type by code and name.
     *
     * @param event {@link String} code or name
     * @param name {@link ResourceType} code or name
     * @return true if found duplicate value otherwise false
     */
    @RequestMapping(value = "/type/checkduplicate/{event}/{name}")
    @ResponseBody
    public boolean findDuplicationInResourceType(@PathVariable String event, @PathVariable String name
    ) {
        ResourceType resourceType = null;
        boolean isExist = false;
        try {
            if (event.equalsIgnoreCase("code")) {
                resourceType = resourceTypeDAO.getResourceTypeByCode(name);
            } else if (event.equalsIgnoreCase("name")) {
                resourceType = resourceTypeDAO.getResourceTypeByName(name);
            }
            if (resourceType == null) {
                isExist = false;
            } else {
                LOGGER.info("Duplicate resource type " + event + " found.");
                isExist = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in finfind duplicate resource type " + event, ex);
        }
        return isExist;
    }

    /**
     * Upload Resource Type default icon
     *
     * @param uploadfile {@link MultipartFile} to upload
     * @param request {@link HttpServletRequest}
     * @return {@link String}Icon file name
     */
    @RequestMapping(value = "/type/default/upload/icon", method = RequestMethod.POST)
    public @ResponseBody
    String uploadResourceTypeDefaultIcon(@RequestParam("uploadfileDefault") MultipartFile uploadfile, HttpServletRequest request
    ) {
        String rTIconDefalutIcon = request.getParameter("resourceType");
        String filePath = resourceService.uploadIcon(resourceTypeIconDefalutLocation, rTIconDefalutIcon, uploadfile.getOriginalFilename());
        FileOutputStream outputStream = null;
        File file = new File(filePath);
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();

        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return file.getName();

    }

    /**
     * Upload Resource Type Search icon
     *
     * @param uploadfile {@link MultipartFile} to upload
     * @param request {@link HttpServletRequest}
     * @return {@link String}Icon file name
     */
    @RequestMapping(value = "/type/search/upload/icon", method = RequestMethod.POST)
    public @ResponseBody
    String uploadResourceTypeSearchIcon(@RequestParam("uploadFileSearch") MultipartFile uploadfile, HttpServletRequest request
    ) {
        String rTIconSearchIcon = request.getParameter("resourceType");
        String filePath = resourceService.uploadIcon(resourceTypeIconSearchLocation, rTIconSearchIcon, uploadfile.getOriginalFilename());
        FileOutputStream outputStream = null;
        File file = new File(filePath);
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();

        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return file.getName();

    }

    /**
     * To get the Resource Type Default Icon on view page.
     * "/get/icon/{value:.+}" in this url pattern :.+ is to accept the file name
     * with extension in path variable
     *
     * @param response {@link HttpServletResponse}
     * @param value file name
     */
    @RequestMapping(value = "/type/default/get/icon/{value:.+}", method = RequestMethod.GET)
    public void getRTDeafultIcon(HttpServletResponse response, @PathVariable String value
    ) {
        resourceService.getIcon(resourceTypeIconDefalutReadLocation, value, response);
    }

    /**
     * To get the Resource Type Default Icon on view page.
     * "/get/icon/{value:.+}" in this url pattern :.+ is to accept the file name
     * with extension in path variable
     *
     * @param response {@link HttpServletResponse}
     * @param value file name
     */
    @RequestMapping(value = "/type/search/get/icon/{value:.+}", method = RequestMethod.GET)
    public void getRTSearchIcon(HttpServletResponse response, @PathVariable String value
    ) {
        resourceService.getIcon(resourceTypeIconSearchReadLocation, value, response);
    }

    /**
     * Find duplicate {@link Resource} by code and name.
     *
     * @param event {@link String} code or name
     * @param name {@link Resource} code or name
     * @return true if found duplicate value otherwise false
     */
    @RequestMapping(value = "/checkduplicate/{event}/{name}")
    @ResponseBody
    public boolean findDuplicationInResource(@PathVariable String event, @PathVariable String name
    ) {
        Resource resource = null;
        boolean isExist = false;
        try {
            if (event.equalsIgnoreCase("code")) {
                resource = resourceDAO.getResourceByCode(name);
            } else if (event.equalsIgnoreCase("name")) {
                resource = resourceDAO.getResourceByName(name);
            }
            if (resource == null) {
                isExist = false;
            } else {
                LOGGER.info("Duplicate resource type " + event + " found.");
                isExist = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Error in finfind duplicate resource type " + event, ex);
        }
        return isExist;
    }

    /**
     * Upload XLS file for Record with metadata resources.
     *
     * @param uploadfile {@link MultipartFile} xls file to upload
     * @param request {@link HttpServletRequest}
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/upload/file", method = RequestMethod.POST)
    public ModelAndView uploadFile(@RequestParam("xlsFileUpload") MultipartFile uploadfile, HttpServletRequest request
    ) {
        FileOutputStream outputStream = null;
        String resourceTypeFileload = request.getParameter("resourceTypeFileload");
        Long resourceTypeId = Long.parseLong(resourceTypeFileload);
        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        Row row = null;
        //File file = new File(uploadfile.);
        MetadataIntegrator resource = new MetadataIntegrator();
        try {
            InputStream fis = uploadfile.getInputStream();
            XSSFWorkbook xSSFWorkbook = new XSSFWorkbook(fis);
            XSSFSheet xSSFSheet = xSSFWorkbook.getSheetAt(0);
            row = xSSFSheet.getRow(1);
            resource = resourceService.mapplingXLSToResource(row);
        } catch (Exception e) {
            LOGGER.error("Exception in upload file", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        resource.setResourceType(resourceType);
        List<ClassificationLevel> listOfClassificationLevels = new ArrayList<ClassificationLevel>(Arrays.asList(ClassificationLevel.values()));
        mav.addObject("resource", resource);
        mav.addObject("classificationLevelList", listOfClassificationLevels);
        mav.setViewName("resource_library_book");
        if (resource.getLanguages() != null) {
            mav.addObject("addedLangaugeList", new ArrayList<>(resource.getLanguages()));
        }
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Call to perform  <li>Publish ResourceType URL</li> <li>unPublish
     * ResourceType</li>
     * through web-service
     *
     * @param resourceTypeCode {@link ResourceType} code
     * @param eventType type of event
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes {@link RedirectAttributes}
     * @return {@link RedirectView}.
     */
    @RequestMapping(value = "/type/{eventType}/{resourceTypeCode}")
    public RedirectView callEventForResourceType(@PathVariable String resourceTypeCode, @PathVariable String eventType, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        String resTypeMessage = null;
        ResourceType resourceType = null;
        try {
            resourceType = resourceService.operationForResourceType(resourceTypeCode, eventType);
            if (resourceType != null) {
                resourceTypeDAO.merge(resourceType);
                Hibernate.initialize(resourceType.getResourceTypeInfo());
                Map<String, ResourceType> resourceTypesMap = (Map<String, ResourceType>) request.getServletContext().getAttribute("resourceTypes");
                request.getServletContext().setAttribute("resourceTypes", resourceService.updateResourceTypeMap(resourceTypesMap, resourceType));
                resTypeMessage = resourceType.getResourceType() + " resource type " + " " + eventType + " " + "successfully." + "  ";
            } else {
                LOGGER.error("Error in callEventForHarvestURL");
                resTypeMessage = resourceType.getResourceType() + " resource type" + " " + eventType + " " + "Failed " + ". Try after some time.";
            }

        } catch (Exception e) {
            LOGGER.error("Error in callEventForHarvestURL", e);
            resTypeMessage = resourceType.getResourceType() + " resource type" + " " + eventType + " " + "Failed " + ". Try after some time.";
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/resourceType/list");
        redirectAttributes.addFlashAttribute("resTypeMessage", resTypeMessage);
        return redirectView;
    }

    /**
     * To Populate {@link Organization} create view
     *
     * @return {@link ModelAndView} of create organization view
     * @see Organization
     */
    @RequestMapping(value = "/organization/create")
    public ModelAndView createOrganization() {
        mav = new ModelAndView();
        Organization organization = new Organization();
        mav.addObject("organization", organization);
        mav.addObject("isEditType", false);
        mav.setViewName("organization_create");
        User user = userService.getCurrentUser();
        Map<String, String> breadcrumbMap = new LinkedHashMap<>();
        breadcrumbMap.put("Organization List", "resource/organization/list");
        mav.addObject("breadcrumbMap", breadcrumbMap);
        mav.addObject("title", "Organization Form");
        mav.addObject("user", user);
        return mav;
    }

    /**
     * To save {@link Organization}
     *
     * @param organization {@link Organization}
     * @param request {@link HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link ModelAndView} of create organization view.
     */
    @RequestMapping(value = "/organization/submit")
    public RedirectView submitOrgnaization(@ModelAttribute Organization organization, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        redirectView = new RedirectView(request.getContextPath() + "/resource/organization/create");
        organization.setCreatedOn(Helper.rightNow());
        if (organizationDAO.createNew(organization)) {
            redirectAttributes.addFlashAttribute("organization", new Organization());

            redirectAttributes.addFlashAttribute("organizationMsg", "Oranization " + organization.getName() + " is created successfully");
            LOGGER.info("Oranization " + organization.getName() + " is created successfully.");
        } else {
            redirectAttributes.addFlashAttribute("organization", organization);
            redirectAttributes.addFlashAttribute("organizationMsg", "Oranization " + organization.getName() + " is not created successfully");
            LOGGER.info("Oranization " + organization.getName() + " is not created successfully.");
        }

        return redirectView;
    }

    /**
     * To get the list of organization.
     *
     * @return {@link ModelAndView} of organization list view.
     */
    @RequestMapping(value = "/organization/list")
    public ModelAndView getListOfOrganization() {
        mav = new ModelAndView();
        mav.setViewName("organization_list");
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("title", "Organization List");
        return mav;

    }

    /**
     * * Populate form for edit ContentType.
     *
     * @param contentTypeId
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/content/edit/populate/{contentTypeId}")
    public ModelAndView editContentType(@PathVariable Long contentTypeId
    ) {
        try {
            mav = new ModelAndView();
            ContentType contentType = contentTypeDAO.get(contentTypeId);
            mav.addObject("contentType", contentType);
            mav.addObject("isEditType", true);
            mav.setViewName("resource-contenttype-create");
        } catch (Exception ex) {
            LOGGER.error("Error in Content Type Create/Edit", ex);
        }
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Save updated data of Content Type.
     *
     * @param contentType
     * @param contentTypeId
     * @param request {@link HttpServletRequest}
     * @return {@link RedirectView}
     */
    @RequestMapping(value = "/content/edit/submit/{contentTypeId}")
    public RedirectView editSubmitContentType(@ModelAttribute ContentType contentType, @PathVariable Long contentTypeId, HttpServletRequest request
    ) {
        try {
            System.out.println("content Type id is::" + contentTypeId);
            resourceService.updateContentType(contentType, contentTypeId);
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("Content Type List", "resource/contenttype/list");
            mav.addObject("breadcrumbMap", breadcrumbMap);
            mav.addObject("title", "Content Type Form");
        } catch (Exception e) {
            LOGGER.error("Error in submit Content Type Edit Form.", e);
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/contenttype/list");
        return redirectView;
    }

    /**
     * Handles URL To Remove ContentType
     *
     * @param contenttypeId
     * @param request {@link  HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link RedirectView} to ContentType Register
     */
    @RequestMapping(value = "/contenttype/remove/{contenttypeId}", method = RequestMethod.GET)
    public RedirectView removeContentType(@PathVariable Long contenttypeId, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        String contentMessage = null;
        ContentType contentType = contentTypeDAO.findbyQuery("findByContentTypeId", contenttypeId).get(0);
        if (contentType.getResources() == null || contentType.getResources().isEmpty()) {
            resourceService.removeContentType(contentType);
            LOGGER.info(contentType.getContentType() + " contentType is removed successfully.");
            contentMessage = contentType.getContentType() + " contentType is removed successfully.";
        } else {
            LOGGER.info(contentType.getContentType() + " ContentType is already associated with resource not possible to delete.");
            contentMessage = contentType.getContentType() + " ContentType is already associated with resource not possible to delete.";

        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/contenttype/list");
        redirectAttributes.addFlashAttribute("contentMessage", contentMessage);
        return redirectView;
    }

    /**
     * Handles URL To Remove Resource
     *
     * @param resourceCode resource code
     * @param request {@link  HttpServletRequest}
     * @param redirectAttributes {@link RedirectAttributes}
     * @return {@link RedirectView} to ContentType Register
     */
    @RequestMapping(value = "/remove/{resourceCode}", method = RequestMethod.GET)
    @ResponseBody
    public boolean removeResource(@PathVariable String resourceCode, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        String rescMessage = null;
        boolean isDeleted = false;
        Resource resource = null;
        try {
            resource = resourceDAO.findbyQuery("findByResourceCode", resourceCode).get(0);
            if (resource.getResourceCode() != null) {
                if (resource.getResourceStatus().getStatusCode() == (short) 8 || resource.getResourceStatus().getStatusCode() == (short) 9) {
                    resourceDAO.delete(resource);
                    isDeleted = true;
                    rescMessage = resource.getResourceName() + " resource is removed from draft register successfully.";
                } else {
                    if (resourceService.removeResource(resource)) {
                        isDeleted = true;
                        rescMessage = resource.getResourceName() + " resource is removed successfully.";
                    } else {
                        isDeleted = false;
                        rescMessage = resource.getResourceName() + "  this Resource is not Possible to delete. Try after some time.";
                    }
                }
            }
        } catch (Exception e) {
            rescMessage = resource.getResourceName() + "  this Resource is not Possible to delete. Try after some time.";
            LOGGER.error("Error in removeResource ", e);
        }
        LOGGER.info(rescMessage);
        return isDeleted;
    }

    /**
     * Handles URL To Remove ResourceType
     *
     * @param resourceTypeCode resource type code
     * @param request {@link  HttpServletRequest}
     * @param redirectAttributes {@link RedirectAttributes}
     * @return {@link RedirectView} to ContentType Register
     */
    @RequestMapping(value = "/type/remove/{resourceTypeCode}", method = RequestMethod.GET)
    public RedirectView removeResourceType(@PathVariable String resourceTypeCode, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        String resTypeMessage = null;
        ResourceType resourceType = resourceTypeDAO.findbyQuery("findByResourceType", resourceTypeCode).get(0);
        if (resourceType.getResourceTypeCode() != null) {
            resourceType.getGroupTypes().removeAll(resourceType.getGroupTypes());
            if (resourceService.removeResourceType(resourceType)) {
                Map<String, ResourceType> resourceTypesMap = (Map<String, ResourceType>) request.getServletContext().getAttribute("resourceTypes");
                request.getServletContext().setAttribute("resourceTypes", resourceService.updateResourceTypeMap(resourceTypesMap, resourceType));
            } else {
                LOGGER.info(resourceType.getResourceType() + " this ResourceType not Possible to delete. Try after some time.");
                resTypeMessage = resourceType.getResourceType() + "  this ResourceType not Possible to delete. Try after some time.";
            }
        } else {
            LOGGER.info(resourceType.getResourceType() + " resourceType is removed successfully");
            resTypeMessage = resourceType.getResourceType() + " resourceType is removed successfully.";
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/resourceType/list");
        redirectAttributes.addFlashAttribute("resTypeMessage", resTypeMessage);
        return redirectView;
    }

    /**
     * To populate resource Type List.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/resourceType/list")
    public ModelAndView populateResourceTypeList() {
        mav = new ModelAndView();
        mav.setViewName("resource-type-list");
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        mav.addObject("rtIconLocation", resourceTypeIconReadLocation);
        mav.addObject("title", "Resource Type Register");
        return mav;
    }

    /**
     * Handles URL for displays Language List.
     *
     * @param searchString name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/language/list")
    @ResponseBody
    public JSONObject languageList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getLangListWithPagination(searchString, pageNum, pageWin);
            jsonObject.put("languageList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of language", ex);
            return null;
        }

    }

    /**
     * Handles URL for displays Organization List.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/organization/list")
    @ResponseBody
    public JSONObject organizationList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getOrgListWithPagination(searchString, pageNum, pageWin);
            jsonObject.put("organizationList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of organization", ex);
            return null;
        }

    }

    /**
     * Handles URL for displays Content Type List.
     *
     * @param searchString name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/contentType/list")
    @ResponseBody
    public JSONObject contentTypeList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getCTListWithPagination(searchString, pageNum, pageWin);
            jsonObject.put("contentTypeList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of contentType", ex);
            return null;
        }

    }

    /**
     * Handles URL for displays Group List.
     *
     * @param searchString name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/grouptype/list")
    @ResponseBody
    public JSONObject groupTypeList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getGroupListWithPagination(searchString, pageNum, pageWin);
            jsonObject.put("groupTypeList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of group", ex);
            return null;
        }

    }

    /**
     * Handles URL for displays Thematic List.
     *
     * @param searchString name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/thematicType/list")
    @ResponseBody
    public JSONObject thematicTypeList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getThematicListWithPagination(searchString, pageNum, pageWin);
            jsonObject.put("thematicTypeList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of thematicType", ex);
            return null;
        }

    }

    /**
     * Handles URL for displays register.
     *
     * @param searchString resource name to search
     * @param pageNum page number
     * @param pageWin window size
     * @param resourceTypeId {@link ResourceType} primary key
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/register/list")
    @ResponseBody
    public JSONObject getAsyncResourceList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum, @RequestParam Long resourceTypeId
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getResourceListWithPagination(searchString, pageNum, pageWin, resourceTypeId);
            System.out.println("resultHolder.getResults()" + resultHolder.getResults().size());
            jsonObject.put("resourceList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of enews", ex);
            return null;
        }

    }

    /**
     * * Populate form for edit organization.
     *
     * @param organizationId organization primary key
     * @return {@link ModelAndView}
     */
    @RequestMapping(value = "/organization/populate/edit/{organizationId}")
    public ModelAndView editOrganization(@PathVariable Long organizationId
    ) {
        try {
            mav = new ModelAndView();
            Organization organization = organizationDAO.get(organizationId);
            mav.addObject("organization", organization);
            mav.addObject("isEditType", true);
            mav.setViewName("organization_create");
            Map<String, String> breadcrumbMap = new LinkedHashMap<>();
            breadcrumbMap.put("Organization List", "resource/organization/list");
            mav.addObject("breadcrumbMap", breadcrumbMap);
            mav.addObject("title", "Organization Form");
        } catch (Exception ex) {
            LOGGER.error("Error in organization Create/Edit", ex);
        }
        User user = userService.getCurrentUser();
        mav.addObject("user", user);
        return mav;
    }

    /**
     * Handles URL for Update the Existing organization
     *
     * @param organizationId {@link Organization} primary key/id
     * @param organization {@link Organization}
     * @param request {@link HttpServletRequest}
     * @return {@link ModelAndView} for creation page
     */
    @RequestMapping(value = "/organization/edit/submit/{organizationId}")
    public RedirectView editSubmitOrganization(@PathVariable Long organizationId, @ModelAttribute Organization organization, HttpServletRequest request
    ) {
        organization.setId(organizationId);
        if (resourceService.editOrganization(organization)) {
            redirectView = new RedirectView(request.getContextPath() + "/resource/organization/list");
        } else {
            redirectView = new RedirectView(request.getContextPath() + "organization/populate/edit/" + organizationId);
        }
        return redirectView;
    }

    /**
     * Handle URLs to Remove organization
     *
     * @param organizationId {@link Organization}
     * @param request {@link  HttpServletRequest}
     * @param redirectAttributes object of{@link RedirectAttributes}
     * @return {@link RedirectView} to organization Register
     */
    @RequestMapping(value = "/organization/remove/{organizationId}", method = RequestMethod.GET)
    public RedirectView removeOrganization(@PathVariable Long organizationId, HttpServletRequest request, RedirectAttributes redirectAttributes
    ) {
        String orgMessage = null;
        Organization organization = organizationDAO.get(organizationId);
        System.out.println("theme hi...." + organization.getResources().size());
        if (organization.getResources().isEmpty()) {

            resourceService.removeOrganization(organization);
            LOGGER.info("organization is removed successfully.");
            orgMessage = organization.getName() + "organization is removed successfully.";
        } else {
            LOGGER.info("This organization is already Associated with Resource Not Possible to delete.");
            orgMessage = organization.getName() + "organization is already Associated with Resource Not Possible to delete.";
        }
        redirectView = new RedirectView(request.getContextPath() + "/resource/organization/list");
        redirectAttributes.addFlashAttribute("orgMessage", orgMessage);
        return redirectView;
    }

    /**
     * Handles URL for displays draft register.
     *
     * @param searchString resource name to search
     * @param pageNum page number
     * @param pageWin window size
     * @param resourceTypeId {@link ResourceType} primary key
     * @return {@link ModelAndView}.
     */
    @RequestMapping(value = "/async/draft/register/list")
    @ResponseBody
    public JSONObject getAsyncDraftResourceList(@RequestParam String searchString, @RequestParam int pageWin, @RequestParam int pageNum, @RequestParam Long resourceTypeId
    ) {
        JSONObject jsonObject = new JSONObject();
        try {
            resultHolder = resourceService.getDraftResourceListWithPagination(searchString, pageNum, pageWin, resourceTypeId);
            System.out.println("resultHolder.getResults()" + resultHolder.getResults().size());
            jsonObject.put("resourceList", resultHolder.getResults());
            jsonObject.put("totalCount", resultHolder.getResultSize());
            return jsonObject;
        } catch (Exception ex) {
            LOGGER.error("Error in get list of enews", ex);
            return null;
        }

    }

    /**
     * checkDuplicateOrganizationName method used to check Organization name is
     * duplicate or not.
     *
     * @param organizationName {@link String}name of organization
     * @return if Organization name is duplicate returns true otherwise false
     */
    @RequestMapping(value = "/checkduplicate/organizationname/{organizationName}")
    @ResponseBody
    public boolean checkDuplicateOrganizationName(@RequestParam String organizationName
    ) {
        boolean isDuplicate = false;
        try {
            Organization organization = organizationDAO.getOrganizationByOrganizationName(organizationName);
            if (!organization.getName().isEmpty() && organization.getName().equalsIgnoreCase(organizationName)) {
                isDuplicate = true;
                LOGGER.info("duplicate organization name found");
                return isDuplicate;
            }
        } catch (Exception e) {
            LOGGER.error("Duplicate entry for Organization Name", e);
        }
        return isDuplicate;
    }

    /**
     * Call to perform <ul> <li>Activate Web URL</li> <li>Deactivate Repository
     * URL</li> <li>Publish</li>
     * <li>unPublish</li> </ul> through web-service
     *
     * @param resourceId {@link Resource} primary key/id
     * @param eventType type of event
     * @return true if event success otherwise false
     */
    @RequestMapping(value = "/{eventType}/action/{resourceId}")
    @ResponseBody
    public boolean callEventForResource(@PathVariable Long resourceId, @PathVariable String eventType
    ) {
        boolean isSuccess = false;
        try {
            Resource resource = resourceDAO.get(resourceId);
            isSuccess = resourceService.operationForResource(resource, eventType);
        } catch (Exception e) {
            isSuccess = false;
            LOGGER.error("Error in callEventForResource: ", e);

        }
        return isSuccess;
    }

    @RequestMapping(value = "/record/processing/{resourceId}")
    @ResponseBody
    public boolean callEventForRecordProcessing(@PathVariable Long resourceId
    ) {
        boolean isSuccess = false;
        try {
            Resource resource = resourceDAO.get(resourceId);
            short status = resourceService.submitEntityToMit(resource, "save");
            ResourceStatus resourceStatus = null;
            if (resource.getResourceType().getResourceTypeCategory().equalsIgnoreCase(Constants.RESOURCE_TYPE_HARVEST)) {
                resourceStatus = resource.getResourceStatus();
            } else {
                resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", status).get(0);
            }

            resource.setResourceStatus(resourceStatus);

            if (status == (short) 13) {
                resource.setActive(false);
                isSuccess = false;
            } else {
                resource.setActive(true);
                isSuccess = true;
            }
            resourceDAO.merge(resource);
        } catch (Exception e) {
            isSuccess = false;
            LOGGER.error("Error in callEventForRecordProcessing: ", e);

        }
        return isSuccess;
    }

    @RequestMapping(value = "/getstatusdetails/{resourceCode}")
    @ResponseBody
    public String getStatusDeatils(@PathVariable String resourceCode
    ) {
        String stausDetailsJson = null;
        try {
            stausDetailsJson = resourceService.callTogetStatusDetails(resourceCode);
        } catch (Exception e) {
            LOGGER.error("Error in getStatusDeatils: ", e);

        }
        return stausDetailsJson;
    }

}
