package in.gov.nvli.controller;

import in.gov.nvli.custom.user.RequestAttributes;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.service.UserActivityService;
import in.gov.nvli.service.UserService;
import in.gov.nvli.util.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * This Controller is used to redirect rediect user to external resources like
 * news,wiki,gov websites etc and also to maintain logs.
 *
 * @author Ruturaj
 */
@Controller
@RequestMapping("/redirect")
public class RedirectControllerForExternalResources {

    /**
     * The name of the property userActivityService used to holds
     * {@link UserActivityService} object reference.
     */
    @Autowired
    private UserActivityService userActivityService;

    /**
     * The name of the property userService used to holds {@link UserService}
     * object reference.
     */
    @Autowired
    private UserService userService;

    @RequestMapping("/redirectToExtResource/{recordIdentifier:.*}")
    public ModelAndView redirectToExtResource(@PathVariable("recordIdentifier") String recordIdentifier, HttpServletRequest request, HttpServletResponse response) {
        User userObj = userService.getCurrentUser();
        if (userObj != null) {
            Long userSearchedPhraseIdInSession = (Long) request.getSession().getAttribute(userObj.getId() + "_" + Constants.USER_SEARCHED_PHRASE_ID);
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            String userAgent = request.getHeader("User-Agent");
            if (userAgent == null) {
                userAgent = "";
            }

            RequestAttributes reqAttrObj = new RequestAttributes();
            reqAttrObj.setIpAddress(ipAddress);
            reqAttrObj.setUserAgent(userAgent);
        }
        return new ModelAndView(new RedirectView(request.getParameter("url")));
    }
}
