package in.gov.nvli.listener;

import in.gov.nvli.service.DataImportService;
import in.gov.nvli.service.ResourceService;
import in.gov.nvli.util.Helper;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Implementation of ApplicationListener to run start scripts
 * @author Milind
 */
@Component
public class ApplicationListenerImpl implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DataImportService dataImportService;
    @Autowired
    private ResourceService resourceService;

    private static final Logger logger = Logger.getLogger(ApplicationListenerImpl.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent e) {
    }

    /**
     * run start scripts before initializing methods are called. 
     */
    @PostConstruct
    public void inIt() {
        try {
            dataImportService.udcDataImportInDB();
            dataImportService.marc21DataImportInDB();
            dataImportService.userProfessionDataInportInDB();
            dataImportService.themeDataInportInDB();
            resourceService.updateTotalResourceRecordCountInDB();
            
            /**
             * To create library details map with key: code and value : name
             */
            Properties libraryDetails = Z3950LibraryCodes();
            Map<String,String>map=new HashMap<>();
            libraryDetails.stringPropertyNames().stream().forEach((code) -> {
                map.put(code,libraryDetails.getProperty(code));
            });
            Helper.z3950LibraryDetails=Helper.sortByValue(map);
            
        } catch (Exception ex) {
            logger.error("ApplicationListenerImpl inIt method Error while running start scripts", ex);
        }
    }
    
    public  static Properties  Z3950LibraryCodes()throws Exception{
        Properties prop = null;
        InputStream input = null;
        String homeDir = System.getProperty("user.home");
        File config = new File(homeDir + File.separator + ".nvli" + File.separator + "conf" + File.separator + "libraty-setting.properties");
        prop = new Properties();
        input = new FileInputStream(config);
        // load a properties file
        prop.load(input);
        //close the stream
        input.close();
        return prop;
    }
}
