/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.widget;

import in.gov.nvli.domain.generic.CreatableEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author akinza
 */
@Entity
@Table(name = "widget_container_page_type")
public class WidgetContainerPageType extends CreatableEntity implements Serializable{
    
}
