package in.gov.nvli.domain.widget;

import in.gov.nvli.domain.generic.CreatableEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author akinza
 */
@Entity
@Table(name = "widget_container_page")
public class WidgetContainerPage extends CreatableEntity implements Serializable{
    
}
