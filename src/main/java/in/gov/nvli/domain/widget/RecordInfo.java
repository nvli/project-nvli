package in.gov.nvli.domain.widget;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 * This class used for to parse JSON data of analytics of most viewed object
 *
 * @author Nitin
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecordInfo implements Serializable{
    
    @JsonProperty("recordIdentifier")
    private String recordIdentifier;
    
    @JsonProperty("recordViewCount")
    private String recordViewCount;
    
    @JsonProperty("nameToView")
    private String nameToView;
    
    /**
     * Constructor
     *
     */
    public RecordInfo() {
        
    }

    /**
     * Constructor
     *
     * @param recordIdentifier
     * @param recordViewCount
     * @param nameToView
     */
    public RecordInfo(@JsonProperty("recordIdentifier") String recordIdentifier, @JsonProperty("recordViewCount") String recordViewCount,
                    @JsonProperty("nameToView") String nameToView ){
        
        this.recordIdentifier = recordIdentifier;
        this.recordViewCount = recordViewCount;
        this.nameToView = nameToView;
        
     }
     
    /**
     *
     * @return {@link RecordInfo#recordIdentifier}
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }
     
    /**
     *
     * @return {@link RecordInfo#recordViewCount}
     */
    public String getRecordViewCount() {
        return recordViewCount;
    }

    /**
     *
     * @param recordViewCount
     */
    public void setRecordViewCount(String recordViewCount) {
        this.recordViewCount = recordViewCount;
    }
    
    /**
     *
     * @return {@link RecordInfo#nameToView}
     */
    public String getNameToView() {
        return nameToView;
    }

    /**
     *
     * @param nameToView
     */
    public void setNameToView(String nameToView) {
        this.nameToView = nameToView;
    }
    
    /*
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString() {
           return "recordIdentifier=" + getRecordIdentifier() + ", nameToView=" + getNameToView() + ", recordViewCount=" + getRecordViewCount();
   }
}
