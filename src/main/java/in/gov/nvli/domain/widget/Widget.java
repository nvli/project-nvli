package in.gov.nvli.domain.widget;

import in.gov.nvli.domain.generic.CreatableEntity;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Creatable Entity for widgets
 *
 * @author Sanjay Rabidas <rabidassanjay@gmail.com>
 */
@Entity
@Table(name = Constants.PORTAL_WIDGET_TABLE_NAME)
public class Widget extends CreatableEntity implements Serializable {

    @Column(name = "name", nullable = false, unique = true)
    private String widgetName;

    @Column(name = "description", nullable = false)
    private String widgetDescription;

    @Column(name = "widget_version", nullable = false)
    private String widgetVersion;

    @Column(name = "widget_logo", nullable = false)
    private String widgetLogo;

    @Column(name = "widget_style", nullable = false)
    private String widgetStyle;

    @Column(name = "widget_js", nullable = false)
    private String widgetJs;

    @Column(name = "widget_template", nullable = false)
    private String widgetTemplate;

    @Column(name = "widget_author_name", nullable = false)
    private String widgetAuthorName;

    @Column(name = "widget_author_email", nullable = false)
    private String widgetAuthorEmail;

    /**
     *
     * @return {@link Widget#widgetName}
     */
    public String getWidgetName() {
        return widgetName;
    }

    /**
     *
     * @param widgetName
     */
    public void setWidgetName(String widgetName) {
        this.widgetName = widgetName;
    }

    /**
     *
     * @return {@link Widget#widgetDescription}
     */
    public String getWidgetDescription() {
        return widgetDescription;
    }

    /**
     *
     * @param widgetDescription
     */
    public void setWidgetDescription(String widgetDescription) {
        this.widgetDescription = widgetDescription;
    }

    /**
     *
     * @return {@link Widget#widgetVersion}
     */
    public String getWidgetVersion() {
        return widgetVersion;
    }

    /**
     *
     * @param widgetVersion
     */
    public void setWidgetVersion(String widgetVersion) {
        this.widgetVersion = widgetVersion;
    }

    /**
     *
     * @return {@link Widget#widgetLogo}
     */
    public String getWidgetLogo() {
        return widgetLogo;
    }

    /**
     *
     * @param widgetLogo
     */
    public void setWidgetLogo(String widgetLogo) {
        this.widgetLogo = widgetLogo;
    }

    /**
     *
     * @return {@link Widget#widgetStyle}
     */
    public String getWidgetStyle() {
        return widgetStyle;
    }

    /**
     *
     * @param widgetStyle
     */
    public void setWidgetStyle(String widgetStyle) {
        this.widgetStyle = widgetStyle;
    }

    /**
     *
     * @return {@link Widget#widgetJs}
     */
    public String getWidgetJs() {
        return widgetJs;
    }

    /**
     *
     * @param widgetJs
     */
    public void setWidgetJs(String widgetJs) {
        this.widgetJs = widgetJs;
    }

    /**
     *
     * @return {@link Widget#widgetTemplate}
     */
    public String getWidgetTemplate() {
        return widgetTemplate;
    }

    /**
     *
     * @param widgetTemplate
     */
    public void setWidgetTemplate(String widgetTemplate) {
        this.widgetTemplate = widgetTemplate;
    }

    /**
     *
     * @return {@link Widget#widgetAuthorName}
     */
    public String getWidgetAuthorName() {
        return widgetAuthorName;
    }

    /**
     *
     * @param widgetAuthorName
     */
    public void setWidgetAuthorName(String widgetAuthorName) {
        this.widgetAuthorName = widgetAuthorName;
    }

    /**
     *
     * @return {@link Widget#widgetAuthorEmail}
     */
    public String getWidgetAuthorEmail() {
        return widgetAuthorEmail;
    }

    /**
     *
     * @param widgetAuthorEmail
     */
    public void setWidgetAuthorEmail(String widgetAuthorEmail) {
        this.widgetAuthorEmail = widgetAuthorEmail;
    }

    
}
