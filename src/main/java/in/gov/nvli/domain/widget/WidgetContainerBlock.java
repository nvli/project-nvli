package in.gov.nvli.domain.widget;

import in.gov.nvli.domain.generic.CreatableEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author akinza
 */
@Entity
@Table(name = "widget_container_block")
public class WidgetContainerBlock extends CreatableEntity implements Serializable{
    
    @Column(name = "widget_container_name", nullable = false)
    private String widgetContainerName;
    
    @Column(name = "widget_container_desc", nullable = false)
    private String widgetContainerDesc;
    
    @OneToMany
    private List<Widget> widgets;

    /**
     *
     * @return {@link WidgetContainerBlock#widgetContainerName}
     */
    public String getWidgetContainerName() {
        return widgetContainerName;
    }

    /**
     *
     * @param widgetContainerName
     */
    public void setWidgetContainerName(String widgetContainerName) {
        this.widgetContainerName = widgetContainerName;
    }

    /**
     *
     * @return {@link WidgetContainerBlock#widgetContainerDesc}
     */
    public String getWidgetContainerDesc() {
        return widgetContainerDesc;
    }

    /**
     *
     * @param widgetContainerDesc
     */
    public void setWidgetContainerDesc(String widgetContainerDesc) {
        this.widgetContainerDesc = widgetContainerDesc;
    }

    /**
     *
     * @return {@link WidgetContainerBlock#widgets}
     */
    public List<Widget> getWidgets() {
        return widgets;
    }

    /**
     *
     * @param widgets
     */
    public void setWidgets(List<Widget> widgets) {
        this.widgets = widgets;
    }
    
    
}
