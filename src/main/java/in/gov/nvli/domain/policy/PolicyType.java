/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.policy;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Entity
@Table(name = "policy_type")
public class PolicyType implements Serializable {

    /**
     * variable use to hold type id that should be unique and can not be null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "type_id")
    private Long typeId;

    /**
     * variable use to hold policy type name and can not be null
     */
    @Column(name = "policy_name")
    private String PolicyName;

    /**
     * variable use to hold list of all policy document
     */
    @OneToMany(mappedBy = "policy", fetch = FetchType.LAZY)
    private List<PolicyDocument> PolicyList;

    /**
     * Gets the policy name
     *
     * @return {@link PolicyType#PolicyName}
     */
    public String getPolicyName() {
        return PolicyName;
    }

    /**
     * This is a setter which sets the policy name
     *
     * @param PolicyName
     */
    public void setPolicyName(String PolicyName) {
        this.PolicyName = PolicyName;
    }

    /**
     * Gets the list for policy document
     *
     * @return {@link PolicyType#PolicyList}
     */
    public List<PolicyDocument> getPolicyList() {
        return PolicyList;
    }

    /**
     * This is a setter which sets the policy list
     *
     * @param PolicyList
     */
    public void setPolicyList(List<PolicyDocument> PolicyList) {
        this.PolicyList = PolicyList;
    }

    /**
     * Gets the type id
     *
     * @return {@link PolicyType#typeId}
     */
    public Long getTypeId() {
        return typeId;
    }

    /**
     * This is a setter which sets the policy type id
     *
     * @param typeId
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

}
