/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.policy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "policy_document")
public class PolicyDocument implements Serializable {

    /**
     * variable use to hold policy Id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policy_id")
    private Long policyId;
    /**
     * variable use to hold policy tittle that can not be null.
     */
    @Column(name = "policy_tittle")
    private String policyTittle;
    /**
     * variable use to hold policy description
     */
    @Column(name = "policy_description", columnDefinition = "LONGTEXT")
    private String policyDescription;
    /**
     * variable use to hold user id of user who created policy particular policy
     * document
     */
    @Column(name = "created_by")
    private Long createdBy;
    /**
     * variable use to hold user id of user who updated policy particular policy
     * document
     */
    @Column(name = "updated_by")
    private Long updatedBy;
    /**
     * variable use to hold last modification date of policy document
     */
    @NotNull
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
    /**
     * variable use to hold publish date of policy document
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publish_date")
    private Date publishDate;
    /**
     * variable use to hold version of policy document
     */
    @Column(name = "version")
    private Long version;
    /**
     * variable use to hold policy type of policy document document
     */
    @ForeignKey(name = "FK_policy_type_id")
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "type_id")
    @JsonIgnore
    private PolicyType policy;
    /**
     * variable use to hold list of users
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "policies")
    private List<User> users;

    /**
     * Gets the Users
     *
     * @return {@link PolicyDocument#users}
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * This is a setter which sets the users
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * Gets the policy id of policy document
     *
     * @return {@link PolicyDocument#policyId}
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * Gets the policy tittle of policy document
     *
     * @return {@link PolicyDocument#policyTittle}
     */
    public String getPolicyTittle() {
        return policyTittle;
    }

    /**
     * Gets the policy description of policy document
     *
     * @return {@link PolicyDocument#policyDescription}
     */
    public String getPolicyDescription() {
        return policyDescription;
    }

    /**
     * Gets the user id that created policy of policy document
     *
     * @return {@link PolicyDocument#createdBy}
     */
    public Long getCreatedBy() {
        return createdBy;
    }

    /**
     * Gets the user id that updated policy of policy document
     *
     * @return {@link PolicyDocument#updatedBy}
     */
    public Long getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Gets the policy type of policy document
     *
     * @param policy {@link  PolicyDocument#policy}
     */
    public void setPolicy(PolicyType policy) {
        this.policy = policy;
    }

    /**
     * Gets the policy type of policy document
     *
     * @return policy {@link  PolicyDocument#policy}
     */
    public PolicyType getPolicy() {
        return policy;
    }

    /**
     * Gets the last modification date of policy document
     *
     * @return {@link PolicyDocument#lastModifiedDate}
     */
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Gets the publish date of policy document
     *
     * @return {@link PolicyDocument#publishDate}
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     * Gets the version no of policy document
     *
     * @return {@link PolicyDocument#version}
     */
    public Long getVersion() {
        return version;
    }

    /**
     * This is a setter which sets the policyId
     *
     * @param policyId
     */
    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    /**
     * This is a setter which sets the policyTittle
     *
     * @param policyTittle
     */
    public void setPolicyTittle(String policyTittle) {
        this.policyTittle = policyTittle;
    }

    /**
     * This is a setter which sets the policyDescription
     *
     * @param policyDescription
     */
    public void setPolicyDescription(String policyDescription) {
        this.policyDescription = policyDescription;
    }

    /**
     * This is a setter which sets the createdBy
     *
     * @param createdBy
     */
    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * This is a setter which sets the updatedBy
     *
     * @param updatedBy
     */
    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * This is a setter which sets the lastModifiedDate
     *
     * @param lastModifiedDate
     */
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     * This is a setter which sets the publishDate
     *
     * @param publishDate
     */
    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    /**
     * This is a setter which sets the version
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     *
     */
    public PolicyDocument() {
    }

    public PolicyDocument(String policyTittle, String policyDescription, Long createdBy, Long updatedBy, Date lastModifiedDate, Date publishDate, Long version, PolicyType policy) {
        this.policyTittle = policyTittle;
        this.policyDescription = policyDescription;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.publishDate = publishDate;
        this.version = version;
        this.policy = policy;
    }

}
