/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.app;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Entity
@Table(name = "feature_config")
public class FeatureConfig implements Serializable{

    /**
     * variable use to hold feature id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Basic(optional = false)
    private Long id;
    /**
     * variable use to hold feature name
     */
    @NotNull
    @Column(name = "featureName")
    private String featureName;
    /**
     * variable use to hold feature status whether it is enabled or disabled
     * default value is 0
     */
    @Column(name = "featureEnabledStatus", columnDefinition = "boolean default false")
    private boolean featureEnabledStatus = true;

    public void setFeatureEnabledStatus(boolean featureEnabledStatus) {
        this.featureEnabledStatus = featureEnabledStatus;
    }

    public boolean isFeatureEnabledStatus() {
        return featureEnabledStatus;
    }

    public Long getId() {
        return id;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

}
