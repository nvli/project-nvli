package in.gov.nvli.domain.generic;

import java.util.Date;

/**
 * Interface for createdON, createdBy
 *
 * @author Sujata Aher
 * @since 1
 * @version 1
 */
public interface Creatable {

    /**
     *
     * @return
     */
    public Long getCreatedBy();

    /**
     *
     * @param createdBy
     */
    public void setCreatedBy(Long createdBy);

    /**
     *
     * @return
     */
    public Date getCreatedOn();

    /**
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn);
}
