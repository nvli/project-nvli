package in.gov.nvli.domain.generic;

/**
 * Interface for updatedBy,updatedOn,createdBy and createdOn
 *
 * @author Sujata Aher 
 * @since 1
 * @version 1
 */
public interface Auditable extends Creatable, Updatable, Identifiable {
}
