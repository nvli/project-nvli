package in.gov.nvli.domain.generic;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Generic Entity for auto increment id creation
 *
 * @author Sujata Aher
 * @since 1
 * @version 1
 */
@MappedSuperclass
public class BaseEntity implements Identifiable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "nvliSeqGenerator")
    @TableGenerator(name = "nvliSeqGenerator", allocationSize = 1, initialValue = 1, table = Constants.PORTAL_TABLE_PREFIX + "nvli_sequence")
    private Long id;

    /**
     * give value of id property
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * set value of id property
     *
     * @param id
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" + "id=" + id + '}';
    }
}
