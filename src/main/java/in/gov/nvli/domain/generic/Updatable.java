package in.gov.nvli.domain.generic;

import java.util.Date;

/**
 * Interface for updatedOn, updatedBy.
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public interface Updatable {

    /**
     *
     * @return
     */
    public Long getUpdatedBy();

    /**
     *
     * @param updatedBy
     */
    public void setUpdatedBy(Long updatedBy);

    /**
     *
     * @return
     */
    public Date getUpdatedOn();

    /**
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn);
}
