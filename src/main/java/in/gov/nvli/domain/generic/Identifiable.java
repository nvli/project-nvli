package in.gov.nvli.domain.generic;

/**
 * Interface for id
 *
 * @author Sujata Aher
 * @since 1
 * @version 1
 */
public interface Identifiable {

    /**
     *
     * @return
     */
    public Long getId();

    /**
     *
     * @param id
     */
    public void setId(Long id);
}
