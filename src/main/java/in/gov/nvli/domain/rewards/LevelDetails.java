package in.gov.nvli.domain.rewards;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "level_details")
public class LevelDetails implements Serializable {

    /**
     * variable use to hold level id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "level_id")
    private Long levelId;
    /**
     * variable use to hold level name that can not be null.
     */
    @NotNull
    @Column(name = "level_name")
    private String levelName;
    /**
     * variable use to hold edit points
     */
    @NotNull
    @Column(name = "edit_points")
    private int editPoints;
    /**
     * variable use to hold approved points
     */
    @NotNull
    @Column(name = "approved_points")
    private int approvedPoints;
    /**
     * variable use to hold stars awarded
     */
    @OneToOne
    @ForeignKey(name = "FK_user_stars_awarded_id")
    @JoinColumn(name = "stars_awarded", referencedColumnName = "stars_id")
    private StarsDetails starsAwarded;

    /**
     * variable use to hold created user id
     */
    @Column(name = "created_by")
    private Long createdBy;

    /**
     * variable use to hold created user id
     */
    @Column(name = "last_updated_by")
    private Long lastUpdatedBy;
    /**
     * variable use to hold publish date of levels
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_date")
    private Date lastModifiedDate;
    /**
     * /**
     * variable use to hold certificate awarded
     */
    @OneToOne
    @ForeignKey(name = "FK_user_badge_awarded_id")
    @JoinColumn(name = "badge_awarded", referencedColumnName = "badge_id")
    private BadgeDetails badgeAwarded;
    /**
     * variable use to hold status whether particular point is currently
     * applicable or not
     */
    @NotNull
    @Column(name = "is_current")
    private boolean isCurrent;

    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public void setEditPoints(int editPoints) {
        this.editPoints = editPoints;
    }

    public void setApprovedPoints(int approvedPoints) {
        this.approvedPoints = approvedPoints;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public Long getLevelId() {
        return levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public int getEditPoints() {
        return editPoints;
    }

    public int getApprovedPoints() {
        return approvedPoints;
    }

    public void setStarsAwarded(StarsDetails starsAwarded) {
        this.starsAwarded = starsAwarded;
    }

    public StarsDetails getStarsAwarded() {
        return starsAwarded;
    }

    public boolean isIsCurrent() {
        return isCurrent;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public BadgeDetails getBadgeAwarded() {
        return badgeAwarded;
    }

    public void setBadgeAwarded(BadgeDetails badgeAwarded) {
        this.badgeAwarded = badgeAwarded;
    }

    public Long getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Long lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LevelDetails(String levelName, int editPoints, int approvedPoints, StarsDetails starsAwarded, Long createdBy, Date lastModifiedDate, BadgeDetails badgeAwarded, boolean isCurrent, Long lastUpdatedBy) {
        this.levelName = levelName;
        this.editPoints = editPoints;
        this.approvedPoints = approvedPoints;
        this.starsAwarded = starsAwarded;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.badgeAwarded = badgeAwarded;
        this.isCurrent = isCurrent;
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public LevelDetails() {
    }

    @Override
    public String toString() {
        return "LevelDetails{" + "levelId=" + levelId + ", levelName=" + levelName + ", editPoints=" + editPoints + ", approvedPoints=" + approvedPoints + ", starsAwarded=" + starsAwarded + ", createdBy=" + createdBy + ", lastModifiedDate=" + lastModifiedDate + ", badgeAwarded=" + badgeAwarded + ", isCurrent=" + isCurrent + '}';
    }

}
