package in.gov.nvli.domain.rewards;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "reward_points_details")
public class RewardsPointDetails implements Serializable {

    /**
     * variable use to hold reward points id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "points_id")
    private Long pointsId;
    /**
     * variable use to hold activity Description that can not be null.
     */
    @NotNull
    @Column(name = "activity_description", columnDefinition = "LONGTEXT")
    private String activityDesc;
    /**
     * variable use to hold earned points
     */
    @NotNull
    @Column(name = "earned_points")
    private int earnedPoints;
    /**
     * variable use to hold activity code
     */
    @NotNull
    @Column(name = "activity_code")
    private String activityCode;

    /**
     * variable use to hold lower bound accuracy
     */
    @Column(name = "lower_bound_accuracy")
    private int lowerBoundAccuracy;

    /**
     * variable use to hold upper bound accuracy
     */
    @Column(name = "upper_bound_accuracy")
    private int upperBoundAccuracy;
    /**
     * variable use to hold status whether particular point is currently
     * applicable or not
     */
    @NotNull
    @Column(name = "is_current")
    private boolean isCurrent;

    public void setPointsId(Long pointsId) {
        this.pointsId = pointsId;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public void setEarnedPoints(int earnedPoints) {
        this.earnedPoints = earnedPoints;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public Long getPointsId() {
        return pointsId;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public int getEarnedPoints() {
        return earnedPoints;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public int getLowerBoundAccuracy() {
        return lowerBoundAccuracy;
    }

    public int getUpperBoundAccuracy() {
        return upperBoundAccuracy;
    }

    public void setLowerBoundAccuracy(int LowerBoundAccuracy) {
        this.lowerBoundAccuracy = LowerBoundAccuracy;
    }

    public void setUpperBoundAccuracy(int upperBoundAccuracy) {
        this.upperBoundAccuracy = upperBoundAccuracy;
    }

    public boolean isIsCurrent() {
        return isCurrent;
    }

}
