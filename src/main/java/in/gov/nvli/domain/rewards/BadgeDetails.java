package in.gov.nvli.domain.rewards;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "badge_details")
public class BadgeDetails implements Serializable {

    /**
     * variable use to hold badge id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "badge_id")
    @NotNull
    private Long badgeId;
    /**
     * variable use to hold badge name that can not be null.
     */
    @NotNull
    @Column(name = "badge_name")
    private String badgeName;
    /**
     * variable use to hold badge code
     */
    @NotNull
    @Column(name = "badge_code")
    private String badgeCode;

    @Column(name = "badge_weightage")
    private int badgeWeight;

    public int getBadgeWeight() {
        return badgeWeight;
    }

    public void setBadgeWeight(int badgeWeight) {
        this.badgeWeight = badgeWeight;
    }

    public Long getBadgeId() {
        return badgeId;
    }

    public String getBadgeName() {
        return badgeName;
    }

    public String getBadgeCode() {
        return badgeCode;
    }

    public void setBadgeId(Long badgeId) {
        this.badgeId = badgeId;
    }

    public void setBadgeName(String badgeName) {
        this.badgeName = badgeName;
    }

    public void setBadgeCode(String badgeCode) {
        this.badgeCode = badgeCode;
    }

}
