package in.gov.nvli.domain.rewards;

import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "user_rewards_summary")
public class UserRewardsSummary implements Serializable {

    /**
     * variable use to hold user rewards summary id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usummary_id")
    private Long usummaryId;

    /**
     * variable use to hold username that can not be null.
     */
    @OneToOne(cascade = CascadeType.ALL)
    @ForeignKey(name = "FK_user_id_reward_summary")
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User userId;

    /**
     * variable use to hold edit points
     */
    @Column(name = "edit_points")
    private int editPoints;
    /**
     * variable use to hold approved points
     */

    @Column(name = "approved_points")
    private int approvedPoints;

    /**
     * variable use to hold current level
     */
    @OneToOne(cascade = CascadeType.ALL)
    @ForeignKey(name = "FK_current_level_details")
    @JoinColumn(name = "current_level_id", referencedColumnName = "level_id")
    private LevelDetails currentLevel;

    public Long getUsummaryId() {
        return usummaryId;
    }

    public User getUserId() {
        return userId;
    }

    public int getEditPoints() {
        return editPoints;
    }

    public int getApprovedPoints() {
        return approvedPoints;
    }

    public void setUsummaryId(Long usummaryId) {
        this.usummaryId = usummaryId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public void setEditPoints(int editPoints) {
        this.editPoints = editPoints;
    }

    public void setApprovedPoints(int approvedPoints) {
        this.approvedPoints = approvedPoints;
    }

    public LevelDetails getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(LevelDetails currentLevel) {
        this.currentLevel = currentLevel;
    }

}
