package in.gov.nvli.domain.rewards;

import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 * @author Vivek Bugale
 * @author Gulafsha
 */
@Entity
@Table(name = "certificate_details")
public class CertificateDetails implements Serializable {

    public CertificateDetails() {

    }

    public CertificateDetails(Date certificateDate, User user, BadgeDetails badgeDetails, String fullname) {
        this.certificateDate=certificateDate;
        this.user = user;
        this.badge = badgeDetails;
        this.userFullName = fullname;
    }

    /**
     * variable use to hold certificate id that can not be null.
     * This is also used for barcode.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "certificate_id")
    private Long certificateId;

    /**
     * variable use to hold user
     */
    @ForeignKey(name = "FK_user_id")
    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * variable use to hold user's fullname
     */
    @NotNull
    @Column(name = "user_full_name")
    private String userFullName;

    /**
     * variable use to hold certificate date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "certificate_date")
    private Date certificateDate;

    @NotNull
    @OneToOne
    @ForeignKey(name = "FK_user_badge_id")
    @JoinColumn(name = "badge", referencedColumnName = "badge_id")
    private BadgeDetails badge;

    @Column(name = "is_generated")
    private boolean generated;

    public Long getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(Long certificateId) {
        this.certificateId = certificateId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public Date getCertificateDate() {
        return certificateDate;
    }

    public void setCertificateDate(Date certificateDate) {
        this.certificateDate = certificateDate;
    }

    public BadgeDetails getBadge() {
        return badge;
    }

    public void setBadge(BadgeDetails badge) {
        this.badge = badge;
    }

    public boolean isGenerated() {
        return generated;
    }

    public void setGenerated(boolean generated) {
        this.generated = generated;
    }

    @Override
    public String toString() {
        return "CertificateDetails{" + "certificateId=" + certificateId + ", user=" + user + ", userFullName=" + userFullName + ", certificateDate=" + certificateDate + ", badge=" + badge + ", generated=" + generated + '}';
    }

}
