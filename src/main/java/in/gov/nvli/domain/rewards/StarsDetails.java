/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.rewards;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Gulafsha 
 */
@Entity
@Table(name = "stars_details")
public class StarsDetails implements Serializable {

    /**
     * variable use to hold stars id that can not be null.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stars_id")
    @NotNull
    private Long starsId;
    /**
     * variable use to hold stars name that can not be null.
     */
    @NotNull
    @Column(name = "stars_name")
    private String starsName;
    /**
     * variable use to hold badge code
     */
    @NotNull
    @Column(name = "stars_Code")
    private String starsCode;

    public Long getStarsId() {
        return starsId;
    }

    public String getStarsName() {
        return starsName;
    }

    public String getStarsCode() {
        return starsCode;
    }

    public void setStarsId(Long starsId) {
        this.starsId = starsId;
    }

    public void setStarsName(String starsName) {
        this.starsName = starsName;
    }

    public void setStarsCode(String starsCode) {
        this.starsCode = starsCode;
    }
    
}

