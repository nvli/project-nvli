/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 * The persistent class for other_user_feedback database table.
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Entity
@Table(name = "other_user_feedback")
public class UserOtherFeedback {

    /**
     * variable use to hold other feedback id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "other_fid")
    private Long otherId;
    /**
     * variable use to hold other feedback message
     */
    @Column(name = "other_feedback_msg")
    private String feedbackMessage;
    /**
     * variable use to hold user,uni-directional One To One association to user
     */
    @OneToOne(cascade = CascadeType.ALL)
    @ForeignKey(name = "fk_user_other_id")
    @JoinColumn(name = "id", referencedColumnName = "id")
    private User user;

    /**
     * Gets the other id
     *
     * @return {@link UserOtherFeedback#otherId}
     */
    public Long getOtherId() {
        return otherId;
    }

    /**
     * Gets the feedback message
     *
     * @return {@link UserOtherFeedback#feedbackMessage}
     */
    public String getFeedbackMessage() {
        return feedbackMessage;
    }

    /**
     * Gets the user object
     *
     * @return {@link UserOtherFeedback#user}
     */
    public User getUser() {
        return user;
    }

    /**
     * This is a setter which sets the other feedback id
     *
     * @param otherId
     */
    public void setOtherId(Long otherId) {
        this.otherId = otherId;
    }

    /**
     * This is a setter which sets the feedback message
     *
     * @param feedbackMessage
     */
    public void setFeedbackMessage(String feedbackMessage) {
        this.feedbackMessage = feedbackMessage;
    }

    /**
     * This is a setter which sets the user object
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
