package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the user_profession database table.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha
 */
@Entity
@Table(name = "user_profession")
public class UserProfession implements Serializable {

    /**
     * variable use to hold profession id
     */
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "profession_id")
    @NotNull
    @Id
    private long professionId;
    /**
     * variable use to hold profession name
     */
    @Column(name = "profession_name")
    private String professionName;
    /**
     * variable use to hold profession description
     */
    @Column(name = "profession_description")
    private String professionDescription;
    /**
     * variable use to hold udc tags fro professions
     */
    @Column(name = "udc_tags")
    private String udcTags;

    /**
     * variable use to check is other flag whether profession is belong to is
     * other or not
     */
    @Column(name = "is_other", columnDefinition = "boolean default false")
    private boolean isOther = false;
    /**
     * variable use to hold set of all users
     */
    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "userProfessions",
            targetEntity = User.class
    )
    private Set<User> users;
    /**
     * variable use to hold parent profession
     */
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "parent_profession")
    private UserProfession parentProfession;
    /**
     * variable use to hold sub profession
     */
    @OneToMany(mappedBy = "parentProfession")
    private List<UserProfession> subProfessions;

    /**
     * constructor use to initialize properties of
     *
     * @see UserProfession
     */
    public UserProfession() {
    }

    /**
     * constructor use to initialize properties of
     *
     * @see UserProfession with parameters
     * @param professionName
     * @param professionDescription
     * @param udcTags
     * @param parentProfession
     * @
     */
    public UserProfession(String professionName, String professionDescription, String udcTags, UserProfession parentProfession) {
        this.professionName = professionName;
        this.professionDescription = professionDescription;
        this.udcTags = udcTags;
        this.parentProfession = parentProfession;
    }

    /**
     * Gets the profession name
     *
     * @return {@link UserProfession#professionName}
     */
    public String getProfessionName() {
        return professionName;
    }

    /**
     * This is a setter which sets profession name
     *
     * @param professionName
     */
    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    /**
     * Gets the profession description
     *
     * @return {@link UserProfession#professionDescription}
     */
    public String getProfessionDescription() {
        return professionDescription;
    }

    /**
     * This is a setter which sets profession description
     *
     * @param professionDescription
     */
    public void setProfessionDescription(String professionDescription) {
        this.professionDescription = professionDescription;
    }

    /**
     * Gets the udc tags for profession
     *
     * @return {@link UserProfession#udcTags}
     */
    public String getUdcTags() {
        return udcTags;
    }

    /**
     * This is a setter which sets udc tags
     *
     * @param udcTags
     */
    public void setUdcTags(String udcTags) {
        this.udcTags = udcTags;
    }

    /**
     * Gets the set of users
     *
     * @return {@link UserProfession#users}
     */
    public Set<User> getUsers() {
        return users;
    }

    /**
     * This is a setter which sets users
     *
     * @param users
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    /**
     * Gets the parent profession
     *
     * @return {@link UserProfession#parentProfession}
     */
    public UserProfession getParentProfession() {
        return parentProfession;
    }

    /**
     * This is a setter which sets parent profession
     *
     * @param parentProfession
     */
    public void setParentProfession(UserProfession parentProfession) {
        this.parentProfession = parentProfession;
    }

    /**
     * Gets the sub profession
     *
     * @return {@link UserProfession#subProfessions}
     */
    public List<UserProfession> getSubProfessions() {
        return subProfessions;
    }

    /**
     * This is a setter which sets sub profession
     *
     * @param subProfessions
     */
    public void setSubProfessions(List<UserProfession> subProfessions) {
        this.subProfessions = subProfessions;
    }

    /**
     * Gets the profession id
     *
     * @return {@link UserProfession#professionId}
     */
    public long getProfessionId() {
        return professionId;
    }

    /**
     * This is a setter which sets profession id
     *
     * @param professionId
     */
    public void setProfessionId(long professionId) {
        this.professionId = professionId;
    }

    /**
     * This is a setter which sets is_other flag
     *
     * @param isOther
     */
    public void setIsOther(boolean isOther) {
        this.isOther = isOther;
    }

    /**
     * Gets the is other flag value
     *
     * @return {@link UserProfession#isOther}
     */
    public boolean isIsOther() {
        return isOther;
    }

}
