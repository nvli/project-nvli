package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.util.Date;
import javax.mail.internet.HeaderTokenizer;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * This Class is to implement spring security remember me functionality and to
 * store persistent login informations in database
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Entity
@Table(name = "persistent_logins")
public class PersistentLogins implements Serializable {

    /**
     * variable use to hold username
     */
    @NotNull
    @Size(max = 64)
    @Column(name = "username")
    private String username;

    /**
     * variable use to hold series.
     */
    @Id
    @NotNull
    @Size(max = 64)
    @Column(name = "series")
    private String series;

    /**
     * variable use to hold unique for a perticular user
     */
    @Size(max = 64)
    @NotNull
    @Column(name = "token")
    private String token;

    /**
     * variable use to hold last user time stamp for user
     */
    @NotNull
    @Column(name = "last_used")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUsed;

    /**
     * Gets the username
     *
     * @return {@link PersistentLogins#username}
     */
    public String getUsername() {
        return username;
    }

    /**
     * This is a setter which sets the username
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the permission id
     *
     * @return
     */
    public String getSeries() {
        return series;
    }

    /**
     * This is a setter which sets the series
     * @param series
     */
    public void setSeries(String series) {
        this.series = series;
    }

    /**
     * Gets the unique token for user
     *
     * @return {@link PersistentLogins#token}
     */
    public String getToken() {
        return token;
    }

    /**
     * This is a setter which sets the token
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets the last used time stamp
     *
     * @return {@link PersistentLogins#lastUsed}
     */
    public Date getLastUsed() {
        return lastUsed;
    }

    /**
     * This is a setter which sets the last used time
     * @param lastUsed
     */
    public void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }

}
