package in.gov.nvli.domain.user;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.generic.CreatableEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Entity
@Table(name = "user_uploaded_file")
public class UserUploadedFile extends CreatableEntity implements Serializable {

    @Column(name = "file_name", nullable = false)
    private String fileName;

    @Column(name = "file_path", nullable = false)
    private String filePath;

    
    @Column(name = "file_type", nullable = false)
    private String fileType;

    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    
}
