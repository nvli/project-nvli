package in.gov.nvli.domain.user;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the notification_settings database table.
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Entity
@Table(name = "notification_settings")
public class NotificationSubscription {

    /**
     * variable use to hold user id that should be unique and can not be
     * null
     */
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    /**
     * variable use to hold nvli link share status via email whether it is true of false 
     * null
     */
    @NotNull
    @Column(name = "nvli_share_link_with_email")
    private boolean nvliShareLinkWithEmail;

    /**
     * variable use to hold  nvli link share status via app whether it is true of false 
     * null
     */
    @NotNull
    @Column(name = "nvli_share_link_with_app")
    private boolean nvliShareLinkWithApp;

    /**
     * Gets the nvli link share via app
     * @return {@link NotificationSubscription#nvliShareLinkWithApp}
     */
    public boolean getNvliShareLinkWithApp() {
        return nvliShareLinkWithApp;
    }

    /**
     *  This is a setter which sets the nvli link share status via app
     * @param nvliShareLinkWithApp
     */
    public void setNvliShareLinkWithApp(boolean nvliShareLinkWithApp) {
        this.nvliShareLinkWithApp = nvliShareLinkWithApp;
    }

    /**
     * Gets the  user id of notification
     * @return {@link NotificationSubscription#userId}
     */
    public Long getUserId() {
        return userId;
    }

    /**
     *  This is a setter which sets the user id of notification
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Gets the nvli link share via email
     * @return {@link NotificationSubscription#nvliShareLinkWithEmail}
     */
    public boolean getNvliShareLinkWithEmail() {
        return nvliShareLinkWithEmail;
    }

    /**
     *  This is a setter which sets the nvli link share status via email
     * @param nvliShareLinkWithEmail
     */
    public void setNvliShareLinkWithEmail(boolean nvliShareLinkWithEmail) {
        this.nvliShareLinkWithEmail = nvliShareLinkWithEmail;
    }

}
