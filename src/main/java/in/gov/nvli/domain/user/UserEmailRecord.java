package in.gov.nvli.domain.user;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * The persistent class for the user_email_record database table.
 *
 * @author Bhumika
 */
//@Entity
//@Table(name = "user_email_record")
public class UserEmailRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * variable use to hold id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * variable use to hold added time
     */
    @Column(name = "added_time")
    private Date addedTime;
    /**
     * variable use to hold receiver mail id
     */
    @Size(max = 200)
    @Column(name = "recepient_mailid")
    private String recepientMailid;
    /**
     * variable use to hold unique Record Identifier
     */
    @Size(max = 200)
    @Column(name = "unique_record_identifier")
    private String uniqueRecordIdentifier;
    /**
     * variable use to hold user,bi-directional many-to-one association to User
     */

    @ManyToOne
    @ForeignKey(name = "FK_user_email_record_user_id")
    @JoinColumn(name = "user_id")
    private User user;
    /**
     * variable use to hold user activity log,bi-directional one-to-one
     * association to UserActivityLog
     */
    @OneToOne
    @ForeignKey(name = "FK_user_email_record_activity_log_id")
    @JoinColumn(name = "activity_log_id")
    private UserActivityLog userActivityLog;

    /**
     * constructor use to initialize properties of 
     * @see UserEmailRecord
     */
    public UserEmailRecord() {
    }

    /**
     * Gets the id
     * @return {@link UserEmailRecord#id}
     */
    public Long getId() {
        return this.id;
    }

    /**
     *  This is a setter which sets  id
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the receiver mail id
     * @return {@link UserEmailRecord#recepientMailid}
     */
    public String getRecepientMailid() {
        return this.recepientMailid;
    }

    /**
     * This is a setter which sets receiver mail id
     * @param recepientMailid
     */
    public void setRecepientMailid(String recepientMailid) {
        this.recepientMailid = recepientMailid;
    }

    /**
     * Gets the unique Record Identifier
     * @return {@link UserEmailRecord#uniqueRecordIdentifier}
     */
    public String getUniqueRecordIdentifier() {
        return this.uniqueRecordIdentifier;
    }

    /**
     * This is a setter which sets unique Record Identifier
     * @param uniqueRecordIdentifier
     */
    public void setUniqueRecordIdentifier(String uniqueRecordIdentifier) {
        this.uniqueRecordIdentifier = uniqueRecordIdentifier;
    }

    /**
     * Gets the added time
     * @return {@link UserEmailRecord#addedTime}
     */
    public Date getAddedTime() {
        return addedTime;
    }

    /**
     * This is a setter which sets added time
     * @param addedTime
     */
    public void setAddedTime(Date addedTime) {
        this.addedTime = addedTime;
    }

    /**
     * Gets the user
     * @return {@link UserEmailRecord#user}
     */
    public User getUser() {
        return this.user;
    }

    /**
     * This is a setter which sets user 
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets the user activity log
     * @return {@link UserEmailRecord#userActivityLog}
     */
    public UserActivityLog getUserActivityLog() {
        return this.userActivityLog;
    }

    /**
     * This is a setter which sets user activity log
     * @param userActivityLog
     */
    public void setUserActivityLog(UserActivityLog userActivityLog) {
        this.userActivityLog = userActivityLog;
    }
}
