/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Represents cities that uses for User
 *
 * @author Gulafsha Khan
 */
@Entity
@Table(name = "pr_city")
public class Cities {

    /**
     * variable use to hold city id that should be unique and can not be null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cityId")
    @NotNull
    @Basic(optional = false)

    private Long cityId;
    /**
     * variable use to hold city name that can not be null.
     */
    @NotNull
    @Column(name = "city")
    private String cityName;
    /**
     * variable use to hold district that is joined by district id of
     * {@link District}
     *
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "districtId")
    private District district;

    /**
     * This is a setter which sets the city Id
     *
     * @param cityId contains city id 
     */
    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    /**
     * This is a setter which sets the city name
     *
     * @param cityName contains city name 
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * This is a setter which sets the district of corresponding city
     *
     * @param district contains district of corresponding city
     */
    public void setDistrict(District district) {
        this.district = district;
    }

    /**
     * Gets the city id of the city
     *
     * @return  {@link Cities#cityId}
     */
    public Long getCityId() {
        return cityId;
    }

    /**
     * Gets the city name of the city
     *
     * @return {@link Cities#cityName}
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Gets the district id of the district
     *
     * @return {@link Cities#district}
     */
    public District getDistrictId() {
        return district;
    }
}
