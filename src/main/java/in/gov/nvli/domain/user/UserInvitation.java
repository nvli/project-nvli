package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 * The persistent class for the user_invitation database table.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha
 */
@Entity
@Table(name = "user_invitation")
public class UserInvitation implements Serializable {

    /**
     * variable use to hold invitation id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invitaion_id")
    private Long invitationId;
    /**
     * variable use to hold invitation code
     */
    @Column(name = "invitation_code", columnDefinition = "VARCHAR( 255 ) NULL DEFAULT NULL")
    private String invitationCode;
    /**
     * variable use to hold invitee email
     */
    @NotNull
    @Column(name = "invitee_email", unique = true)
    private String inviteeEmail;
    /**
     * variable use to hold invitation date
     */
    @Column(name = "invitation_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date invitationDate;

    /**
     * variable use hold list of all that is granted to invitee user
     */
    @ManyToMany
    @JoinTable(
            name = "user_invitation_granted_roles",
            joinColumns = {
                @JoinColumn(name = "invitation_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "role_id", nullable = false, updatable = false)
            }
    )
    private List<Role> grantedRoles;
    /**
     * variable use to check whether user is accepted invitation or not,0 for
     * not registered yet 1 for registered based on invitation
     */
    @Column(name = "invitation_accepted", columnDefinition = "INT DEFAULT 0")
    private int invitationAccepted;
    /**
     * variable use to hold user who sent invitation
     */
    @NotNull
    @ManyToOne
    @ForeignKey(name = "FK_user_invitation_invited_by")
    @JoinColumn(name = "invited_by")
    private User invitedBy;

    /**
     * constructor use to initialize properties of
     *
     * @see UserInvitation
     */
    public UserInvitation() {
    }

    /**
     * Gets the invitation id
     *
     * @return {@link UserInvitation#invitationId}
     */
    public Long getInvitationId() {
        return invitationId;
    }

    /**
     * This is a setter which sets invitation Id
     *
     * @param invitationId
     */
    public void setInvitationId(Long invitationId) {
        this.invitationId = invitationId;
    }

    /**
     * Gets the invitation code
     *
     * @return {@link UserInvitation#invitationCode}
     */
    public String getInvitationCode() {
        return invitationCode;
    }

    /**
     * This is a setter which sets invitation code
     *
     * @param invitationCode
     */
    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    /**
     * Gets the invitee email address
     *
     * @return {@link UserInvitation#inviteeEmail}
     */
    public String getInviteeEmail() {
        return inviteeEmail;
    }

    /**
     * This is a setter which sets invitee email
     *
     * @param inviteeEmail
     */
    public void setInviteeEmail(String inviteeEmail) {
        this.inviteeEmail = inviteeEmail;
    }

    /**
     * Gets the granted roles
     *
     * @return {@link UserInvitation#grantedRoles}
     */
    public List<Role> getGrantedRoles() {
        return grantedRoles;
    }

    /**
     * This is a setter which sets granted roles to invitee user
     *
     * @param grantedRoles
     */
    public void setGrantedRoles(List<Role> grantedRoles) {
        this.grantedRoles = grantedRoles;
    }

    /**
     * Gets the invitation date
     *
     * @return {@link UserInvitation#invitationDate}
     */
    public Date getInvitationDate() {
        return invitationDate;
    }

    /**
     * This is a setter which sets invitation date
     *
     * @param invitationDate
     */
    public void setInvitationDate(Date invitationDate) {
        this.invitationDate = invitationDate;
    }

    /**
     * Gets the user who sent sent invitation
     *
     * @return {@link UserInvitation#invitedBy}
     */
    public User getInvitedBy() {
        return invitedBy;
    }

    /**
     * This is a setter which sets user who sent invitation
     *
     * @param invitedBy
     */
    public void setInvitedBy(User invitedBy) {
        this.invitedBy = invitedBy;
    }

    /**
     * Gets the integer value to check whether invitation is accepted or not
     *
     * @return {@link UserInvitation#invitationAccepted}
     */
    public int getInvitationAccepted() {
        return invitationAccepted;
    }

    /**
     * This is a setter which sets integer value to check whether invitation is
     * accepted or not
     *
     * @param invitationAccepted
     */
    public void setInvitationAccepted(int invitationAccepted) {
        this.invitationAccepted = invitationAccepted;
    }

}
