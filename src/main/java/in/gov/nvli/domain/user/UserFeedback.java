/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 * The persistent class for the user_feedback database table.
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Entity
@Table(name = "user_feedback")
public class UserFeedback {

    /**
     * variable use to hold user feedback id 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_fid")
    private Long userFeedbackId;
    /**
     * variable use to hold user object ,uni-directional One To One association
     * to User
     */
    @OneToOne(cascade = CascadeType.ALL)
    @ForeignKey(name = "fk_user_id")
    @JoinColumn(name = "id", referencedColumnName = "id")
    private User user;
    /**
     * variable use to hold feedback base object, uni-directional One To One
     * association to feedback base class
     */
    @OneToOne(cascade = CascadeType.ALL)
    @ForeignKey(name = "fk_fid")
    @JoinColumn(name = "f_id", referencedColumnName = "f_id")
    private DeactivationFeedbackBase deactivationFeedbackBase;

    /**
     * Gets the feedback Id
     *
     * @return {@link UserFeedback#userFeedbackId}
     */
    public Long getUserFeedbackId() {
        return userFeedbackId;
    }

    /**
     * Gets the user
     *
     * @return {@link UserFeedback#user}
     */
    public User getUser() {
        return user;
    }

    /**
     * This is a setter which sets the feedback id
     *
     * @param userFeedbackId
     */
    public void setUserFeedbackId(Long userFeedbackId) {
        this.userFeedbackId = userFeedbackId;
    }

    /**
     * This is a setter which sets the user object
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets the deactivation feedback base object
     *
     * @return {@link UserFeedback#deactivationFeedbackBase}
     */
    public DeactivationFeedbackBase getDeactivationFeedbackBase() {
        return deactivationFeedbackBase;
    }

    /**
     * This is a setter which sets the feedback base object
     *
     * @param deactivationFeedbackBase
     */
    public void setDeactivationFeedbackBase(DeactivationFeedbackBase deactivationFeedbackBase) {
        this.deactivationFeedbackBase = deactivationFeedbackBase;
    }

    /**
     * Constructor use to initialize user feedback object
     *
     * @param userFeedbackId
     * @param user
     * @param deactivationFeedbackBase
     */
    public UserFeedback(Long userFeedbackId, User user, DeactivationFeedbackBase deactivationFeedbackBase) {
        this.userFeedbackId = userFeedbackId;
        this.user = user;
        this.deactivationFeedbackBase = deactivationFeedbackBase;
    }

    /**
     * Constructor use to initialize user feedback empty object
     */
    public UserFeedback() {

    }

}
