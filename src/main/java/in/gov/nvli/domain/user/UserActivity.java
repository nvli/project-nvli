package in.gov.nvli.domain.user;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The persistent class for the user_acivity database table.
 *
 * @author Bhumika
 * @author Gulafsha
 * @since 1
 * @version 1
 */
@Entity
@Table(name = "user_activity")
public class UserActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * variable use to hold activity id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "activity_id")
    private int activityId;

    /**
     * variable use to hold activity key
     */
    @Size(max = 30)
    @NotNull
    @Column(name = "activity_key")
    private String activityKey;

    /**
     * variable use to hold list of all activity logs, bi-directional
     * many-to-one association to UserActivityLog
     */
    @OneToMany(mappedBy = "userActivity")
    private List<UserActivityLog> userActivityLogs;

    /**
     * Constructor to initialize properties of 
     * @see UserActivity
     */
    public UserActivity() {
    }

    /**
     * Gets the activity id
     * @return {@link UserActivity#activityId}
     */
    public int getActivityId() {
        return activityId;
    }

    /**
     * This is a setter which sets activity id
     * @param activityId
     */
    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    /**
     * Gets the all activity key
     * @return {@link UserActivity#activityKey}
     */
    public String getActivityKey() {
        return activityKey;
    }

    /**
     * This is a setter which sets activity key
     * @param activityKey
     */
    public void setActivityKey(String activityKey) {
        this.activityKey = activityKey;
    }

    /**
     * Gets the list of all activity logs
     * @return {@link UserActivity#userActivityLogs}
     */
    public List<UserActivityLog> getUserActivityLogs() {
        return userActivityLogs;
    }

    /**
     * This is a setter which sets all activity logs of user
     * @param userActivityLogs
     */
    public void setUserActivityLogs(List<UserActivityLog> userActivityLogs) {
        this.userActivityLogs = userActivityLogs;
    }

}
