/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Represents states that uses for User
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "pr_state")
public class States implements Serializable {

    /**
     * variable use to hold state id that should be unique and can not be null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stateId")
    @Basic(optional = false)
    @NotNull
    private Long stateId;
    /**
     * variable use to hold state name that can not be null
     */
    @NotNull
    @Column(name = "stateName")
    private String stateName;

    /**
     * variable use to hold country of a perticular state
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "countryId")
    private Country countries;

    /**
     * variable use to hold district list of a perticular state
     */
    @OneToMany(mappedBy = "state", fetch = FetchType.LAZY)
    private List<District> disttList;

    /**
     * This is a setter which sets the state id of state
     *
     * @param stateId contains state id of state
     */
    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    /**
     * This is a setter which sets the state name of state
     * @param stateName contains state name of state
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * This is a setter which sets the country name of a perticular state
     * @param countries contains country of a district
     */
    public void setCountries(Country countries) {
        this.countries = countries;
    }

    /**
     * This is a setter which sets the state list of a perticular district
     * @param disttList contains district list
     */
    public void setDisttList(List<District> disttList) {
        this.disttList = disttList;
    }

    /**
     * Gets the state id of the state
     * @return {@link States#stateId} state id of state 
     */
    public Long getStateId() {
        return stateId;
    }

    /**
     * Gets the state  name  of the state
     * @return {@link States#stateName } state name of state
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * Gets the country of a perticular  district
     * @return {@link States#countries } country of a perticular district
     */
    public Country getCountries() {
        return countries;
    }

    /**
     * Gets the district list of corresponding state 
     * @return {@link States#disttList} list of district
     */
    public List<District> getDisttList() {
        return disttList;
    }

}
