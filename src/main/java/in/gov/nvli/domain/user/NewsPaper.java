package in.gov.nvli.domain.user;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The persistent class for the news_paper database table.
 * @author Ritesh
 * @author Gulafsha
 */
@Entity
@Table(name = "news_paper")
@NamedQueries({
    @NamedQuery(name = "NewsPaper.findAll", query = "SELECT n FROM NewsPaper n")})
public class NewsPaper implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "paper_code")
    private String paperCode;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "paper_name")
    private String paperName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "paper_display_name")
    private String paperDisplayName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "paper_lang")
    private String paperLang;

    @Basic(optional = false)
    @NotNull
    @Column(name = "news_type", columnDefinition = "BIT(1)")
    private boolean newsType;

    /**
     *
     * @return
     */
    public String getPaperName() {
        return paperName;
    }

    /**
     *
     * @param paperName
     */
    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }

    /**
     *
     * @return
     */
    public String getPaperCode() {
        return paperCode;
    }

    /**
     *
     * @param paperCode
     */
    public void setPaperCode(String paperCode) {
        this.paperCode = paperCode;
    }

    /**
     *
     * @return
     */
    public String getPaperDisplayName() {
        return paperDisplayName;
    }

    /**
     *
     * @param paperDisplayName
     */
    public void setPaperDisplayName(String paperDisplayName) {
        this.paperDisplayName = paperDisplayName;
    }

    /**
     *
     * @return
     */
    public String getPaperLang() {
        return paperLang;
    }

    /**
     *
     * @param paperLang
     */
    public void setPaperLang(String paperLang) {
        this.paperLang = paperLang;
    }

    /**
     *
     * @return
     */
    public boolean isNewsType() {
        return newsType;
    }

    /**
     *
     * @param newsType
     */
    public void setNewsType(boolean newsType) {
        this.newsType = newsType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paperCode != null ? paperCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsPaper)) {
            return false;
        }
        NewsPaper other = (NewsPaper) object;
        if ((this.paperCode == null && other.paperCode != null) || (this.paperCode != null && !this.paperCode.equals(other.paperCode))) {
            return false;
        }
        return true;
    }
}
