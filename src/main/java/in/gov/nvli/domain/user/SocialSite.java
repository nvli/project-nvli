package in.gov.nvli.domain.user;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;
import javax.validation.constraints.Size;

/**
 * The persistent class for the social_sites database table.
 *
 * @author Bhumika
 */
@Entity
@Table(name = "social_sites")
@NamedQuery(name = "findSiteByProviderId" , query = "SELECT socialSite FROM SocialSite socialSite WHERE socialSite.providerId=?")
public class SocialSite implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * variable use to hold social site id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    /**
     * variable use to hold site name
     */
    @Size(max = 100)
    @Column(name = "site_name")
    private String site_name;
    /**
     * variable use to hold provider id of social site
     */
    @Column(name = "provider_id")
    private String providerId;

    /**
     * constructor use to initialize the properties of
     *
     * @see SocialSite
     *
     */
    public SocialSite() {
    }

    /**
     * Gets the social site id
     *
     * @return {@link SocialSite#id}
     */
    public int getId() {
        return this.id;
    }

    /**
     * This is a setter which sets the social site id
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the social site name
     *
     * @return {@link SocialSite#site_name}
     */
    public String getSite_name() {
        return site_name;
    }

    /**
     * This is a setter which sets the site name
     *
     * @param site_name
     */
    public void setSite_name(String site_name) {
        this.site_name = site_name;
    }

    /**
     * Gets the social site provider id
     *
     * @return {@link SocialSite#providerId}
     */
    public String getProviderId() {
        return providerId;
    }

    /**
     * This is a setter which sets the provider id
     *
     * @param providerId
     */
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

}
