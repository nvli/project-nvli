/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the deactivation_feedback_base database table.
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Entity
@Table(name = "deactivation_feedback_base")
public class DeactivationFeedbackBase {

    /**
     * variable use to hold feedback id that should be unique and can not be
     * null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "f_id")
    @NotNull
    private Long feedbackId;

    /**
     * variable use to hold feedback label
     */
    @Column(name = "feedback_label")
    private String feedbackLabel;

    /**
     * variable use to hold feedback description
     */
    @Column(name = "feedback_description")
    private String feedbackDescription;

    /**
     * Gets the feedback id
     *
     * @return {@link DeactivationFeedbackBase#feedbackId}
     */
    public Long getFeedbackId() {
        return feedbackId;
    }

    /**
     * Gets the feedback label
     *
     * @return {@link DeactivationFeedbackBase#feedbackLabel}
     */
    public String getFeedbackLabel() {
        return feedbackLabel;
    }

    /**
     * Gets the feedback description
     *
     * @return {@link DeactivationFeedbackBase#feedbackDescription}
     */
    public String getFeedbackDescription() {
        return feedbackDescription;
    }

    /**
     * This is a setter which sets the feedback id
     *
     * @param feedbackId
     */
    public void setFeedbackId(Long feedbackId) {
        this.feedbackId = feedbackId;
    }

    /**
     * This is a setter which sets the feedback label
     *
     * @param feedbackLabel
     */
    public void setFeedbackLabel(String feedbackLabel) {
        this.feedbackLabel = feedbackLabel;
    }

    /**
     * This is a setter which sets the feedback description
     *
     * @param feedbackDescription
     */
    public void setFeedbackDescription(String feedbackDescription) {
        this.feedbackDescription = feedbackDescription;
    }

    /**
     * Constructor to create feedback base object
     *
     * @param feedbackId
     * @param feedbackLabel
     * @param feedbackDescription
     */
    public DeactivationFeedbackBase(Long feedbackId, String feedbackLabel, String feedbackDescription) {
        this.feedbackId = feedbackId;
        this.feedbackLabel = feedbackLabel;
        this.feedbackDescription = feedbackDescription;
    }

    /**
     * Constructor use to initialize feedback base object
     */
    public DeactivationFeedbackBase() {

    }

}
