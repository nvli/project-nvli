/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.util.Countries;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represents country that uses for User a country have multiple states
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "pr_country")
public class Country {

    /**
     * variable use to hold country id that should be unique and can not be null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "countryId")
    @Basic(optional = false)
    @NotNull
    private Long countryId;

    /**
     * variable use to hold country name that can not be null
     */
    @NotNull
    @Column(name = "countryName")
    private String countryName;

    /**
     * variable use to hold iso code
     */
    @Column(name = "iso")
    private String iso;

    /**
     * variable use to hold nicename
     */
    @Column(name = "nicename")
    private String nicename;

    /**
     * variable use to hold iso3
     */
    @Column(name = "iso3")
    private String iso3;

    /**
     * variable use to hold num code of a country
     */
    @Column(name = "numcode")
    private String numcode;

    /**
     * variable use to hold phone code of country
     */
    @Column(name = "phonecode")
    private String phonecode;

    /**
     * variable use to hold state list of a country
     */
    @OneToMany(mappedBy = "countries", fetch = FetchType.LAZY)
    private List<States> stateList;

    /**
     * This is a setter which sets the country Id
     *
     * @param countryId contains country id of country
     */
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    /**
     * This is a setter which sets the country name
     * @param countryName contains country name of country
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * This is a setter which sets the iso
     * @param iso contains iso of country
     */
    public void setIso(String iso) {
        this.iso = iso;
    }

    /**
     * This is a setter which sets the nice name of country
     * @param nicename contains nice name of country
     */
    public void setNicename(String nicename) {
        this.nicename = nicename;
    }

    /**
     * This is a setter which sets the iso3 of country
     * @param iso3 contains iso3 of country
     */
    public void setIso3(String iso3) {
        this.iso3 = iso3;
    }

    /**
     * This is a setter which sets the num code of country
     * @param numcode contains num code of country
     */
    public void setNumcode(String numcode) {
        this.numcode = numcode;
    }

    /**
     * This is a setter which sets the phone code of country
     * @param phonecode contains phone code of country
     */
    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode;
    }

    /**
     * This is a setter which sets the state list of a country
     *
     * @param stateList contains the state list of corresponding country
     */
    public void setStateList(List<States> stateList) {
        this.stateList = stateList;
    }

    /**
     * Gets the country id of the country
     * @return {@link Country#countryId } country id of country
     */
    public Long getCountryId() {
        return countryId;
    }

    /**
     * Gets the country name of the country
     * @return {@link  Country#countryName } country name of country
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Gets the country iso of the country
     * @return {@link  Country#iso} iso of country
     */
    public String getIso() {
        return iso;
    }

    /**
     * Gets the country nice name  of the country
     * @return {@link  Country#nicename } nice name of country
     */
    public String getNicename() {
        return nicename;
    }

    /**
     * Gets the country iso3 of the country
     * @return {@link Country#iso3 } iso3 of country
     */
    public String getIso3() {
        return iso3;
    }

    /**
     * Gets the country num code of the country
     * @return {@link  Country#numcode } num code of country
     */
    public String getNumcode() {
        return numcode;
    }

    /**
     * Gets the country phone code  of the country
     * @return {@link Country#phonecode } phone code of the country
     */
    public String getPhonecode() {
        return phonecode;
    }

    /**
     * Gets the state list of a city
     *
     * @return {@link Country#stateList } list of states of a country
     */
    public List<States> getStateList() {
        return stateList;
    }
}
