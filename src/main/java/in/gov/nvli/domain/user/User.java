package in.gov.nvli.domain.user;

import in.gov.nvli.beans.NewUserBean;
import in.gov.nvli.custom.annotation.ValidEmail;
import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.crowdsource.UDCTagVote;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import in.gov.nvli.domain.policy.PolicyDocument;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.rewards.CertificateDetails;
import in.gov.nvli.domain.rewards.UserRewardsSummary;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * User Entity used to map the user table with the User Object
 *
 * @author prabhat
 */
@Entity
@Table(name = Constants.PORTAL_USER_TABLE_NAME, uniqueConstraints
        = @UniqueConstraint(columnNames = {"username", "email"}))
public class User implements Serializable {

    /**
     * variable use to hold user id that should be unique and can not be null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Basic(optional = false)
    private Long id;
    /**
     * variable use to hold username id that should be unique and can not be
     * null
     */
    @Size(max = 50)
    @Column(name = "username")
    private String username;
    /**
     * variable use to hold password
     */
    @Basic(optional = false)
    @NotNull
    @Size(max = 255)
    @Column(name = "password")
    private String password;
    /**
     * variable use to hold salt that should be unique
     */
    @Size(max = 255)
    @Column(name = "salt")
    private String salt;
    /**
     * variable use to hold ip address
     */
    @Basic(optional = false)
    @NotNull
    @Size(max = 45)
    @Column(name = "ip_address")
    private String ipAddress;
    /**
     * variable use to hold email address of user
     */
    @ValidEmail
    @Basic(optional = false)
    @NotNull
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    /**
     * variable use to hold activation code for user
     */
    @Size(max = 40)
    @Column(name = "activation_code")
    private String activationCode;
    /**
     * variable use to hold forgotten password code that should be used when
     * user try to reset their password
     */
    @Size(max = 40)
    @Column(name = "forgotten_password_code")
    private String forgottenPasswordCode;
    /**
     * variable use to hold forgotten password time
     */
    @Column(name = "forgotten_password_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date forgottenPasswordTime;
    /**
     * variable use to hold remember code for user
     */
    @Size(max = 40)
    @Column(name = "remember_code")
    private String rememberCode;
    /**
     * variable use to hold time when user get created
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    /**
     * variable use to hold last login time
     */
    @Column(name = "last_login")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    /**
     * variable use to hold status for email activation whether user email
     * address is activated or not
     */
    @Column(name = "active")
    private byte active;
    /**
     * variable use to hold first name of user
     */
    @Size(max = 40)
    @NotNull
    @Column(name = "first_name")
    private String firstName;
    /**
     * variable use to hold middle name of user
     */
    @Size(max = 40)
    @Column(name = "middle_name")
    private String middleName;
    /**
     * variable use to hold last name of user
     */
    @Size(max = 40)
    @Column(name = "last_name")
    private String lastName;
    @Size(max = 15)
    /**
     * variable use to hold contact no of user
     */
    @Column(name = "contact")
    private String contact;
    /**
     * variable use to hold address of user ,only when user does not belong from
     * INDIA
     */
    @Column(name = "address")
    private String address;
    /**
     * variable use to hold city of user
     */
    @Column(name = "city")
    private String city;
    /**
     * variable use to hold district of user
     */
    @Column(name = "district")
    private String district;

    /**
     * variable use to hold state of user
     */
    @Column(name = "state")
    private String state;
    /**
     * variable use to hold country of user
     */
//
    @Column(name = "country")
    private String country;
    /**
     * variable use to hold gender of user
     */
    @Size(max = 15)
    @Column(name = "gender")
    private String gender;
    /**
     * variable use to hold some description about user
     */
    @Basic(optional = true)
    @Column(name = "about_me")
    private String aboutMe;
    /**
     * variable use to hold profile pic path of user
     */
    @Size(max = 200)
    @Column(name = "profile_pic_path")
    private String profilePicPath;
    /**
     * variable use to hold all role that is assigned to perticular user
     */
    @ManyToMany(
            targetEntity = Role.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH
    )
    @JoinTable(
            name = "user_role",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "role_id", nullable = false, updatable = false)
            }
    )
    private Set<Role> roles;
    /**
     * variable use to hold list of all activation log of user
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserActivityLog> activityLogs;
    /**
     * variable use to hold all interests of user
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserInterest> interests;
    /**
     * variable use to check status whether user is active or not ,by default
     * user will be enabled(value will be 1),can deactivate or disable user then
     * value of this column will be 0
     */
    @Column(name = "enabled", columnDefinition = "TINYINT DEFAULT 1", length = 1)
    private byte enabled = 1;
    /**
     * variable use to check status whether user is deleted or not, default
     * status is 0 but if deleted then the value will change to 1
     */
    @Column(name = "deleted_status", columnDefinition = "TINYINT DEFAULT 0", length = 1)
    private byte deleted = 0;
    /**
     * variable use to hold all invited users,bi-directional many-to-one
     * association to UserInvitation
     */
    @OneToMany(mappedBy = "invitedBy")
    private List<UserInvitation> userInvitations;
    /**
     * bi-directional many-to-one association to CrowdSourceArticle
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<CrowdSourceArticle> crowdSourceArticleSet;
    /**
     * bi-directional One To Many association to CrowdSource
     */
    @OneToMany(mappedBy = "expertUserId", fetch = FetchType.LAZY)
    private Set<CrowdSource> crowdSourceSet;
    /**
     * bi-directional One To Many association to CrowdSourceEdit
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<CrowdSourceEdit> crowdSourceEditSet;
    /**
     * bi-directional One To Many association to UDCTagsCrowdsource
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<UDCTagsCrowdsource> udcTagsCrowdsourcesSet;
    /**
     * bi-directional One To Many association to UDCTagVote
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<UDCTagVote> udcTagVoteSet;
    /**
     * bi-directional One To Many association to CrowdSourceCustomTag
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<CrowdSourceCustomTag> crowdSourceCustomTagSet;
    /**
     * bi-directional One To Many association to user rewards summary
     */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private Set<CertificateDetails> certificateDeatils;
    /**
     * al variable use to hold all interest resources of user
     */
    @ManyToMany(
            targetEntity = ResourceType.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JoinTable(name = "user_resource_interest", joinColumns = {
        @JoinColumn(name = "user_id", nullable = false, updatable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "resource_type_id", nullable = false, updatable = false)})
    private List<ResourceType> resourceInterest;
    /**
     * variable use to hold all profession of user
     */
    @ManyToMany(
            targetEntity = PolicyDocument.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH
    )
    @JoinTable(
            name = "user_policy_agreement",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "policy_id", nullable = false, updatable = false)
            }
    )
    private Set<PolicyDocument> policies;

    @ManyToMany(
            targetEntity = UserProfession.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_professions_relation",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "profession_id", nullable = false, updatable = false)
            }
    )
    private Set<UserProfession> userProfessions;
    /**
     * variable use to hold all udc languages of user
     */
    @ManyToMany(
            targetEntity = UDCLanguage.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "user_language_relation",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "udc_language_id", nullable = false, updatable = false)
            }
    )
    private List<UDCLanguage> userLanguages;
    /**
     * variable use to hold all user specialization
     */
    @ManyToMany(
            targetEntity = UDCConcept.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "user_specialization",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "udc_concept_id", nullable = false, updatable = false)
            }
    )
    private List<UDCConcept> userSpecializations;
    /**
     * variable use to hold all user ebooks
     */
    /**
     * variable use to hold all themes of user
     */
    @ManyToOne
    private UserTheme theme;
    /**
     * variable use to hold all assign resources of user
     */
    @ManyToMany(
            targetEntity = Resource.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH
    )
    @JoinTable(
            name = "user_assigned_resources",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "resource_id", nullable = false, updatable = false)
            }
    )
    private Set<Resource> assignedResources;
    /**
     * variable use to hold all assign organization of user
     */
    @ManyToMany(
            targetEntity = Organization.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.DETACH
    )
    @JoinTable(
            name = "user_assigned_organizations",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "organization_id", nullable = false, updatable = false)
            }
    )
    private Set<Organization> assignedOrganizations;
    /**
     * variable use to hold all news papers of user
     */
    @ManyToMany(
            targetEntity = NewsPaper.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinTable(
            name = "user_subscribed_news_paper",
            joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "paper_code", nullable = false, updatable = false)
            }
    )
    private Set<NewsPaper> newsPapers;

    /**
     * constructor use to initialize all properties of
     *
     * @see User
     */
    public User() {
        this.roles = new HashSet<Role>(0);
    }

    /**
     * Constructor to create User instance from newUserBean Object
     *
     * @param newUserBean
     */
    public User(NewUserBean newUserBean) {
        this.username = newUserBean.getUsername();
        this.password = newUserBean.getPassword();
        this.email = newUserBean.getEmail();
        this.firstName = newUserBean.getFirstName();
        this.middleName = newUserBean.getMiddleName();
        this.lastName = newUserBean.getLastName();
        this.contact = newUserBean.getContact();
        this.gender = newUserBean.getGender();
    }

    /**
     * Gets the user id of user
     *
     * @return {@link User#id}
     */
    public Long getId() {
        return id;
    }

    /**
     * This is a setter which sets id of user
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the username of user
     *
     * @return {@link User#username}
     */
    public String getUsername() {
        return username;
    }

    /**
     * This is a setter which sets username of user
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password of user
     *
     * @return {@link User#password}
     */
    public String getPassword() {
        return password;
    }

    /**
     * This is a setter which sets password of user
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the salt of user
     *
     * @return {@link User#salt}
     */
    public String getSalt() {
        return salt;
    }

    /**
     * This is a setter which sets salt of user
     *
     * @param salt
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * Gets the user ip address
     *
     * @return {@link User#ipAddress}
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * This is a setter which sets the ip address
     *
     * @param ipAddress
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Gets the user email address
     *
     * @return {@link User#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     * This is a setter which sets email of user
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the user email activation code
     *
     * @return {@link User#activationCode}
     */
    public String getActivationCode() {
        return activationCode;
    }

    /**
     * This is a setter which sets the activation code of user
     *
     * @param activationCode
     */
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    /**
     * Gets the user forgot password code
     *
     * @return {@link User#forgottenPasswordCode}
     */
    public String getForgottenPasswordCode() {
        return forgottenPasswordCode;
    }

    /**
     * This is a setter which sets forgot password code of user
     *
     * @param forgottenPasswordCode
     */
    public void setForgottenPasswordCode(String forgottenPasswordCode) {
        this.forgottenPasswordCode = forgottenPasswordCode;
    }

    /**
     * Gets the user forgot password time
     *
     * @return {@link User#forgottenPasswordTime}
     */
    public Date getForgottenPasswordTime() {
        return forgottenPasswordTime;
    }

    /**
     * This is a setter which sets forgot password time of user
     *
     * @param forgottenPasswordTime
     */
    public void setForgottenPasswordTime(Date forgottenPasswordTime) {
        this.forgottenPasswordTime = forgottenPasswordTime;
    }

    /**
     * Gets the user remember code
     *
     * @return {@link User#rememberCode}
     */
    public String getRememberCode() {
        return rememberCode;
    }

    /**
     * This is a setter which sets remember code of user
     *
     * @param rememberCode
     */
    public void setRememberCode(String rememberCode) {
        this.rememberCode = rememberCode;
    }

    /**
     * Gets the user account created time
     *
     * @return {@link User#createdOn}
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * This is a setter which sets account created time of user
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Gets the user last login time
     *
     * @return {@link User#lastLogin}
     */
    public Date getLastLogin() {
        return lastLogin;
    }

    /**
     * This is a setter which sets last login time of user
     *
     * @param lastLogin
     */
    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * Gets the whether user email is activated or not
     *
     * @return {@link User#active}
     */
    public byte getActive() {
        return active;
    }

    /**
     * This is a setter which sets status whether user is activated or not
     *
     * @param active
     */
    public void setActive(byte active) {
        this.active = active;
    }

    /**
     * Gets the user first name
     *
     * @return {@link User#firstName}
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * This is a setter which sets first name of user
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the user middle name
     *
     * @return {@link User#middleName}
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * This is a setter which sets middle name of user
     *
     * @param middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the user last name
     *
     * @return {@link User#lastName}
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * This is a setter which sets last name of user
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the user contact no
     *
     * @return {@link User#contact}
     */
    public String getContact() {
        return contact;
    }

    /**
     * Gets the user address
     *
     * @return {@link User#address}
     */
    public String getAddress() {
        return address;
    }

    /**
     * This is a setter which sets the address of user
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * This is a setter which sets the contact no of user
     *
     * @param contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * Gets the city of user
     *
     * @return {@link User#city}
     */
    public String getCity() {
        return city;
    }

    /**
     * This is a setter which set the city of user
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets the district of user
     *
     * @return {@link User#district}
     */
    public String getDistrict() {
        return district;
    }

    /**
     * This is a setter which sets district of user
     *
     * @param district
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * Gets the state of user
     *
     * @return {@link User#state}
     */
    public String getState() {
        return state;
    }

    /**
     * This is a setter which sets the state name of user
     *
     * @param state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets the country of user
     *
     * @return {@link User#country}
     */
    public String getCountry() {
        return country;
    }

    /**
     * This is a setter which sets country name of user
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets the gender of user
     *
     * @return {@link User#gender}
     */
    public String getGender() {
        return gender;
    }

    /**
     * This is a setter which sets gender of user
     *
     * @param gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Gets the list of all role that is assigned to each user
     *
     * @return {@link User#roles}
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * This is a setter which sets list of all roles assigned to user
     *
     * @param roles
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * Gets the all activity logs of user
     *
     * @return {@link User#activityLogs}
     */
    public List<UserActivityLog> getActivityLogs() {
        return activityLogs;
    }

    /**
     * This is a setter which sets all activity logs of user
     *
     * @param activityLogs
     */
    public void setActivityLogs(List<UserActivityLog> activityLogs) {
        this.activityLogs = activityLogs;
    }

    /**
     * Gets the status whether user is activated or not
     *
     * @return {@link User#enabled}
     */
    public byte getEnabled() {
        return enabled;
    }

    /**
     * This is a setter which sets status whether user is enabled or not
     *
     * @param enabled
     */
    public void setEnabled(byte enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets the status whether user is deleted or not
     *
     * @return {@link User#deleted}
     */
    public byte getDeleted() {
        return deleted;
    }

    /**
     * This is a setter which sets status whether user is deleted or not
     *
     * @param deleted
     */
    public void setDeleted(byte deleted) {
        this.deleted = deleted;
    }

    /**
     * Gets the user invitation list
     *
     * @return {@link User#userInvitations}
     */
    public List<UserInvitation> getUserInvitations() {
        return userInvitations;
    }

    /**
     *
     * @param userInvitations
     */
    public void setUserInvitations(List<UserInvitation> userInvitations) {
        this.userInvitations = userInvitations;
    }

    /**
     * Gets the information about user
     *
     * @return {@link User#aboutMe}
     */
    public String getAboutMe() {
        return aboutMe;
    }

    /**
     * This is a setter which sets information about user
     *
     * @param aboutMe
     */
    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    /**
     * Gets the user profile pic path
     *
     * @return {@link User#profilePicPath}
     */
    public String getProfilePicPath() {
        return profilePicPath;
    }

    /**
     * This is a setter which sets profile pic path or user
     *
     * @param profilePicPath
     */
    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    /**
     * Gets the set of Crowd Source Articles
     *
     * @return {@link User#crowdSourceArticleSet}
     */
    public Set<CrowdSourceArticle> getCrowdSourceArticleSet() {
        return crowdSourceArticleSet;
    }

    /**
     * This is a setter which sets set of Crowd Source Articles
     *
     * @param crowdSourceArticleSet
     */
    public void setCrowdSourceArticleSet(Set<CrowdSourceArticle> crowdSourceArticleSet) {
        this.crowdSourceArticleSet = crowdSourceArticleSet;
    }

    /**
     * Gets the set of Crowd Source
     *
     * @return {@link User#crowdSourceSet}
     */
    public Set<CrowdSource> getCrowdSourceSet() {
        return crowdSourceSet;
    }

    /**
     * This is a setter which sets set of Crowd Source
     *
     * @param crowdSourceSet
     */
    public void setCrowdSourceSet(Set<CrowdSource> crowdSourceSet) {
        this.crowdSourceSet = crowdSourceSet;
    }

    /**
     * Gets the set of Crowd Source Edit
     *
     * @return {@link User#crowdSourceEditSet}
     */
    public Set<CrowdSourceEdit> getCrowdSourceEditSet() {
        return crowdSourceEditSet;
    }

    /**
     * This is a setter which sets set of Crowd Source Edit
     *
     * @param crowdSourceEditSet
     */
    public void setCrowdSourceEditSet(Set<CrowdSourceEdit> crowdSourceEditSet) {
        this.crowdSourceEditSet = crowdSourceEditSet;
    }

    /**
     * Gets the set of Crowd Source CustomTag
     *
     * @return {@link User#crowdSourceCustomTagSet}
     */
    public Set<CrowdSourceCustomTag> getCrowdSourceCustomTagSet() {
        return crowdSourceCustomTagSet;
    }

    /**
     * This is a setter which sets set of Crowd Source CustomTag
     *
     * @param crowdSourceCustomTagSet
     */
    public void setCrowdSourceCustomTagSet(Set<CrowdSourceCustomTag> crowdSourceCustomTagSet) {
        this.crowdSourceCustomTagSet = crowdSourceCustomTagSet;
    }

    /**
     * Gets the set of UserProfession
     *
     * @return {@link User#userProfessions}
     */
    public Set<UserProfession> getUserProfessions() {
        return userProfessions;
    }

    /**
     * This is a setter which sets professions of user
     *
     * @param userProfessions
     */
    public void setUserProfessions(Set<UserProfession> userProfessions) {
        this.userProfessions = userProfessions;
    }

    /**
     * Gets the set of UDC Tags Crowd source
     *
     * @return {@link User#udcTagsCrowdsourcesSet}
     */
    public Set<UDCTagsCrowdsource> getUdcTagsCrowdsourcesSet() {
        return udcTagsCrowdsourcesSet;
    }

    /**
     * This is a setter which sets set of UDC Tags Crowd source
     *
     * @param udcTagsCrowdsourcesSet
     */
    public void setUdcTagsCrowdsourcesSet(Set<UDCTagsCrowdsource> udcTagsCrowdsourcesSet) {
        this.udcTagsCrowdsourcesSet = udcTagsCrowdsourcesSet;
    }

    /**
     * Gets the set of UDC Tag Vote
     *
     * @return {@link User#udcTagVoteSet}
     */
    public Set<UDCTagVote> getUdcTagVoteSet() {
        return udcTagVoteSet;
    }

    /**
     * This is a setter which sets set of UDC Tag Vote
     *
     * @param udcTagVoteSet
     */
    public void setUdcTagVoteSet(Set<UDCTagVote> udcTagVoteSet) {
        this.udcTagVoteSet = udcTagVoteSet;
    }

    /**
     * The hashCode method used to generate unique hashing code for user Object
     *
     * @return {@link Integer}
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 71 * hash + (this.username != null ? this.username.hashCode() : 0);
        return hash;
    }

    /**
     * The equals method used to check equality whether same user Object or not
     *
     * @param obj
     * @return boolean true or false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the list of resource Interest
     *
     * @return {@link User#resourceInterest}
     */
    public List<ResourceType> getResourceInterest() {
        return resourceInterest;
    }

    /**
     * This is a setter which sets all resource interest or user
     *
     * @param resourceInterest
     */
    public void setResourceInterest(List<ResourceType> resourceInterest) {
        this.resourceInterest = resourceInterest;
    }

    /**
     * Gets the list of user udc languages
     *
     * @return {@link User#userLanguages}
     */
    public List<UDCLanguage> getUserLanguages() {
        return userLanguages;
    }

    /**
     * This is a setter which sets user languages
     *
     * @param userLanguages
     */
    public void setUserLanguages(List<UDCLanguage> userLanguages) {
        this.userLanguages = userLanguages;
    }

    /**
     * Gets the list of user theme
     *
     * @return {@link User#theme}
     */
    public UserTheme getTheme() {
        return theme;
    }

    /**
     * This is a setter which sets user themes
     *
     * @param theme
     */
    public void setTheme(UserTheme theme) {
        this.theme = theme;
    }

    /**
     * Gets all user specialization
     *
     * @return {@link User#userSpecializations}
     */
    public List<UDCConcept> getUserSpecializations() {
        return userSpecializations;
    }

    /**
     * This is a setter which sets user specializations
     *
     * @param userSpecializations
     */
    public void setUserSpecializations(List<UDCConcept> userSpecializations) {
        this.userSpecializations = userSpecializations;
    }

    /**
     * Gets the list of user Interest
     *
     * @return {@link User#interests}
     */
    public List<UserInterest> getInterests() {
        return interests;
    }

    /**
     * This is a setter which sets user interest
     *
     * @param interests
     */
    public void setInterests(List<UserInterest> interests) {
        this.interests = interests;
    }

    /**
     * Gets the set of assigned resources
     *
     * @return {@link User#assignedResources}
     */
    public Set<Resource> getAssignedResources() {
        return assignedResources;
    }

    /**
     * This is a setter which sets user assigned resources
     *
     * @param assignedResources
     */
    public void setAssignedResources(Set<Resource> assignedResources) {
        this.assignedResources = assignedResources;
    }

    public Set<Organization> getAssignedOrganizations() {
        return assignedOrganizations;
    }

    public void setAssignedOrganizations(Set<Organization> assignedOrganizations) {
        this.assignedOrganizations = assignedOrganizations;
    }

    /**
     * Gets the set of user newspapers
     *
     * @return {@link User#newsPapers}
     */
    public Set<NewsPaper> getNewsPapers() {
        return newsPapers;
    }

    /**
     * This is a setter which sets user newspapers
     *
     * @param newsPapers
     */
    public void setNewsPapers(Set<NewsPaper> newsPapers) {
        this.newsPapers = newsPapers;
    }

    public void setPolicies(Set<PolicyDocument> policies) {
        this.policies = policies;
    }

    public Set<PolicyDocument> getPolicies() {
        return policies;
    }

    public void setCertificateDeatils(Set<CertificateDetails> certificateDeatils) {
        this.certificateDeatils = certificateDeatils;
    }

    public Set<CertificateDetails> getCertificateDeatils() {
        return certificateDeatils;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", salt=" + salt + ", firstName=" + firstName + ", lastName=" + lastName + ", contact=" + contact + '}';
    }

}
