package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the user_theme database table.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Entity
@Table(name = "user_theme")
public class UserTheme implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * variable used to store id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    /**
     * variable used to store theme code
     */
    @Column(name = "theme_code")
    private String themeCode;
    /**
     * variable used to store theme name
     */
    @NotNull
    @Column(name = "theme_name")
    private String themeName;
    /**
     * variable used to store theme description
     */
    @Column(name = "theme_description")
    private String themeDescription;
    /**
     * variable use to identify whether the theme is customized is not
     */
    @Column(name = "is_custom")
    private boolean custom;
    /**
     * variable used to store header image location
     */
    @Column(name = "header_image_location")
    private String headerImageLocation;
    /**
     * variable used to store background image location
     */
    @Column(name = "bg_image_location")
    private String bgImageLocation;
    /**
     * variable used to store theme type
     */
    @Column(name = "theme_type")
    private String themeType;
    /**
     * variable used to store theme activation date
     */
    @Column(name = "theme_activation_date")
    private Date themeActivationDate;
    /**
     * variable used to store theme directory
     */
    @Column(name = "theme_directory")
    private String themeDirectory;
    /**
     * variable used to store set of users
     */
    @OneToMany(mappedBy = "theme")
    private Set<User> user;

    @Column(name = "theme_repeatable", nullable = false)
    private boolean themeRepeatable = false;

    public boolean getThemeRepeatable() {
        return themeRepeatable;
    }

    public void setThemeRepeatable(boolean themeRepeatable) {
        this.themeRepeatable = themeRepeatable;
    }

    /**
     * Gets the theme id
     *
     * @return {@link UserTheme#id}
     */
    public long getId() {
        return id;
    }

    /**
     * This is a setter which sets theme id
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets the theme code
     *
     * @return {@link UserTheme#themeCode}
     */
    public String getThemeCode() {
        return themeCode;
    }

    /**
     * This is a setter which sets theme code
     *
     * @param themeCode
     */
    public void setThemeCode(String themeCode) {
        this.themeCode = themeCode;
    }

    /**
     * Gets the theme name
     *
     * @return {@link UserTheme#themeName}
     */
    public String getThemeName() {
        return themeName;
    }

    /**
     * This is a setter which sets theme name
     *
     * @param themeName
     */
    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    /**
     * Gets the theme description
     *
     * @return {@link UserTheme#themeDescription}
     */
    public String getThemeDescription() {
        return themeDescription;
    }

    /**
     * This is a setter which sets theme description
     *
     * @param themeDescription
     */
    public void setThemeDescription(String themeDescription) {
        this.themeDescription = themeDescription;
    }

    /**
     * Gets the custom status
     *
     * @return {@link UserTheme#custom} whether theme is custom or not
     */
    public boolean isCustom() {
        return custom;
    }

    /**
     * This is a setter which sets the whether theme is custom or not
     *
     * @param custom
     */
    public void setCustom(boolean custom) {
        this.custom = custom;
    }

    /**
     * Gets the header image location
     *
     * @return {@link UserTheme#headerImageLocation}
     */
    public String getHeaderImageLocation() {
        return headerImageLocation;
    }

    /**
     * This is a setter which sets header image location
     *
     * @param headerImageLocation
     */
    public void setHeaderImageLocation(String headerImageLocation) {
        this.headerImageLocation = headerImageLocation;
    }

    /**
     * Gets the background image location
     *
     * @return {@link UserTheme#bgImageLocation}
     */
    public String getBgImageLocation() {
        return bgImageLocation;
    }

    /**
     * This is a setter which sets background image location
     *
     * @param bgImageLocation
     */
    public void setBgImageLocation(String bgImageLocation) {
        this.bgImageLocation = bgImageLocation;
    }

    /**
     * Gets the user
     *
     * @return {@link UserTheme#user}
     */
    public Set<User> getUser() {
        return user;
    }

    /**
     * This is a setter which sets user
     *
     * @param user
     */
    public void setUser(Set<User> user) {
        this.user = user;
    }

    /**
     * Gets the theme type
     *
     * @return {@link UserTheme#themeType}
     */
    public String getThemeType() {
        return themeType;
    }

    /**
     * This is a setter which sets theme type
     *
     * @param themeType
     */
    public void setThemeType(String themeType) {
        this.themeType = themeType;
    }

    /**
     * Gets the theme Activation Date
     *
     * @return {@link UserTheme#themeActivationDate}
     */
    public Date getThemeActivationDate() {
        return themeActivationDate;
    }

    /**
     * This is a setter which sets theme Activation Date
     *
     * @param themeActivationDate
     */
    public void setThemeActivationDate(Date themeActivationDate) {
        this.themeActivationDate = themeActivationDate;
    }

    /**
     * Gets the theme directory
     *
     * @return {@link UserTheme#themeDirectory}
     */
    public String getThemeDirectory() {
        return themeDirectory;
    }

    /**
     * This is a setter which sets theme directory
     *
     * @param themeDirectory
     */
    public void setThemeDirectory(String themeDirectory) {
        this.themeDirectory = themeDirectory;
    }

}
