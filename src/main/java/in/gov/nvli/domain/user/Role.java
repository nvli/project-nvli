/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the role database table.
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Entity
@Table(name = "role")
public class Role implements Serializable {

    /**
     * variable use to hold role id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    @Basic(optional = false)
    private Long roleId;

    /**
     * variable use to hold role name
     */
    @NotNull
    @Column(name = "name")
    private String name;

    /**
     * variable use to hold role description
     */
    @NotNull
    @Column(name = "description")
    private String description;

    /**
     * variable use to hold role code
     */
    @NotNull
    @Column(name = "code")
    private String code;

    /**
     * variable use to hold list of all user for any perticular user
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private List<User> users;

    /**
     * variable use to hold all permission that is assigned for that perticular
     * role
     */
    @ManyToMany(
            targetEntity = Permission.class,
            cascade = CascadeType.DETACH,
            fetch = FetchType.LAZY
    )
    @JoinTable(
            name = "role_permission",
            joinColumns = {
                @JoinColumn(name = "role_id", nullable = false, updatable = false)
            },
            inverseJoinColumns = {
                @JoinColumn(name = "permission_id", nullable = false, updatable = false)
            }
    )
    private Set<Permission> permissions;

    /**
     * variable use to hold list all invited all for that perticular role
     */
    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "grantedRoles",
            targetEntity = UserInvitation.class
    )
    private List<UserInvitation> userInvitations;

    /**
     * Gets the role id
     *
     * @return {@link Role#roleId }
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * This is a setter which sets role id
     * @param roleId
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * Gets the role code
     *
     * @return {@link Role#code}
     */
    public String getCode() {
        return code;
    }

    /**
     * This is a setter which sets role code
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the role name
     *
     * @return {@link Role#name}
     */
    public String getName() {
        return name;
    }

    /**
     * This is a setter which sets role name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the list of all user assigned to that perticular role
     *
     * @return {@link Role#users}
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * This is a setter which sets list of users
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * Gets the all permission that is allowed for any role
     *
     * @return {@link Role#permissions}
     */
    public Set<Permission> getPermissions() {
        return permissions;
    }

    /**
     * This is a setter which sets permission for any role
     *
     * @param permissions
     */
    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public String toString() {
        return "Role{" + "id=" + roleId + '}';
    }

    /**
     * Gets the role description
     *
     * @return {@link Role#permissions}
     */
    public String getDescription() {
        return description;
    }

    /**
     * This is a setter which sets role description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the list of all invited user for any perticular role
     *
     * @return {@link Role#userInvitations}
     */
    public List<UserInvitation> getUserInvitations() {
        return userInvitations;
    }

    /**
     * This is a setter which sets user invitation list
     *
     * @param userInvitations
     */
    public void setUserInvitations(List<UserInvitation> userInvitations) {
        this.userInvitations = userInvitations;
    }
    /*
    
    
     */

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Role other = (Role) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }
}
