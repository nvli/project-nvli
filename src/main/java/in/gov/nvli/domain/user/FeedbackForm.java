package in.gov.nvli.domain.user;

import in.gov.nvli.custom.annotation.ValidEmail;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@Entity
@Table(name = "user_feedback_details")
public class FeedbackForm implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="feedback_id")
    private Long feedbackId;    
    
    @Size(max = 500)
    @NotNull
    @Column(name="feedback")
    private String feedback;
    
    @Size(max = 100)
    @NotNull
    @Column(name = "name")
    private String name;
    
    @ValidEmail
    @Basic(optional = false)
    @NotNull
    @Column(name = "email")
    private String email;
    
    @Size(max = 15)    
    @Column(name = "phone_number")
    private String phoneNumber;
    
    
    @Column(name = "designation")
    private String designation;
     
    
    @Column(name = "organization")
    private String organization;
    
    @Column(name = "feedback_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date feedbackDate;
    
    @JoinColumn(name = "user_id")
    @ForeignKey(name="FK_feedback_user_id")
    @ManyToOne
    private User userId;

    public Long getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Long feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Date getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(Date feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    
}
