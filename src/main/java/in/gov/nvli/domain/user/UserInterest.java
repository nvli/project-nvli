/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import in.gov.nvli.domain.resource.ThematicType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the user_interest database table.
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha
 */
@Table(name = "user_interest")
@Entity
public class UserInterest {

    /**
     * variable use to hold id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    /**
     * variable use to hold user
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    /**
     * variable use to hold theme type
     */
    @ManyToOne
    @JoinColumn(name = "thematic_type_id")
    private ThematicType thematicType;
    /**
     * variable use to hold keywords
     */
    @Column(name = "keywords")
    private String keywords;

    /**
     *  Gets the user
     * @return {@link UserInterest#user}
     */
    public User getUser() {
        return user;
    }

    /**
     * This is a setter which sets user
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *  Gets the theme type
     * @return {@link UserInterest#thematicType}
     */
    public ThematicType getThematicType() {
        return thematicType;
    }

    /**
     *  This is a setter which sets theme type
     * @param thematicType
     */
    public void setThematicType(ThematicType thematicType) {
        this.thematicType = thematicType;
    }

    /**
     *  Gets the id
     * @return {@link UserInterest#id}
     */
    public long getId() {
        return id;
    }

    /**
     *  This is a setter which sets id
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *  Gets the keyword 
     * @return {@link UserInterest#keywords}
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     *  This is a setter which sets keywords
     * @param keywords
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

}
