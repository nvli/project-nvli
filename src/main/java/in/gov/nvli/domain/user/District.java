/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Represents district that uses for User district can have multiple cities
 *
 * @author Gulafsha
 */
@Entity
@Table(name = "pr_district")
public class District {

    /**
     * variable use to hold district id that should be unique and can not be
     * null
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "districtId")
    @NotNull
    @Basic(optional = false)
    private Long districtId;

    /**
     * variable use to hold district name can not be null
     */
    @NotNull
    @Column(name = "district")
    private String districtName;

    /**
     * variable use to hold state of a corresponding district
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "stateId")
    private States state;

    /**
     * variable use to hold city list of corresponding district
     */
    @OneToMany(mappedBy = "district", fetch = FetchType.LAZY)
    private List<Cities> cityList;

    /**
     * This is a setter which sets the district id of district
     *
     * @param districtId contains district id of district
     */
    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    /**
     * This is a setter which sets the district name of district
     *
     * @param districtName contains district name of district
     */
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    /**
     * This is a setter which sets the state of a perticular district
     *
     * @param state contains state of a perticular district
     */
    public void setState(States state) {
        this.state = state;
    }

    /**
     * This is a setter which sets the city list of a district
     *
     * @param cityList contains city list of a perticular district
     */
    public void setCityList(List<Cities> cityList) {
        this.cityList = cityList;
    }

    /**
     * Gets the district id of the district
     *
     * @return {@link  District#districtId } district id of the district
      */
    public Long getDistrictId() {
        return districtId;
    }

    /**
     * Gets the district id of the district
     *
     * @return {@link District#districtName } contains district name
     */
    public String getDistrictName() {
        return districtName;
    }

    /**
     * Gets the state name of a perticular district
     *
     * @return {@link District#state } contains state of a district
     */
    public States getState() {
        return state;
    }

    /**
     * Gets the city list of district
     * @return {@link District#cityList } contains list of all cities that belong to a perticular district
     */
    public List<Cities> getCityList() {
        return cityList;
    }

}
