package in.gov.nvli.domain.user;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the userconnection database table.
 * @author
 */
@Entity
@Table(name = "UserConnection")
public class UserConnection implements Serializable {

    /**
     * variable use to hold user id that should be unique and can not be null
     */
    @Id
    @Column(name = "userId")
    private String userId;
    /**
     * variable use to hold provider id like whether it is
     * facebook,google+,github,linkedin etc
     */
    @Id
    @Column(name = "providerId")
    private String providerId;
    /**
     * variable use to hold provider user id that should be unique
     */
    @Id
    @Column(name = "providerUserId")
    private String providerUserId;
    /**
     * variable use to hold rank of social site
     */
    @Column(name = "rank")
    private int rank;
    /**
     * variable use to hold display name of user
     */
    @Column(name = "displayName")
    private String displayName;
    /**
     * variable use to hold profile url of user
     */
    @Column(name = "profileUrl")
    private String profileUrl;
    /**
     * variable use to hold image url of user
     */
    @Column(name = "imageUrl")
    private String imageUrl;
    /**
     * variable use to hold access token
     */
    @Column(name = "accessToken")
    private String accessToken;
     /**
     * variable use to hold secret
     */
    @Column(name = "secret")
    private String secret;
     /**
     * variable use to hold refresh token
     */
    @Column(name = "refreshToken")
    private String refreshToken;
     /**
     * variable use to hold expire token
     */
    @Column(name = "expireTime")
    private String expireTime;

    /**
     *  Gets the user id
     * @return {@link UserConnection#userId}
     */
    public String getUserId() {
        return userId;
    }

    /**
     *  This is a setter which sets  user id
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *  Gets the provider id
     * @return {@link UserConnection#providerId}
     */
    public String getProviderId() {
        return providerId;
    }

    /**
     *  This is a setter which sets provider id 
     * @param providerId
     */
    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    /**
     *  Gets the provider user id
     * @return {@link UserConnection#providerUserId}
     */
    public String getProviderUserId() {
        return providerUserId;
    }

    /**
     *  This is a setter which sets provider user id 
     * @param providerUserId
     */
    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }

    /**
     *  Gets the rank of social account
     * @return {@link UserConnection#rank}
     */
    public int getRank() {
        return rank;
    }

    /**
     *  This is a setter which sets rank
     * @param rank
     */
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     *  Gets the user display name
     * @return {@link UserConnection#displayName}
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     *  This is a setter which sets display name of  user
     * @param displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *  Gets the user profile url
     * @return {@link UserConnection#profileUrl}
     */
    public String getProfileUrl() {
        return profileUrl;
    }

    /**
     *  This is a setter which sets profile url of user
     * @param profileUrl
     */
    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    /**
     *  Gets the user image url
     * @return {@link UserConnection#imageUrl}
     * 
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *  This is a setter which sets  image url of user
     * @param imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     *  Gets the access token
     * @return {@link UserConnection#accessToken}
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * This is a setter which sets access token
     * @param accessToken
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     *  Gets the secret of social site

     * @return {@link UserConnection#secret}
     */
    public String getSecreat() {
        return secret;
    }

    /**
     * This is a setter which sets secret
     * @param secreat
     */
    public void setSecreat(String secreat) {
        this.secret = secreat;
    }

    /**
     * Gets the refresh token
     * @return {@link UserConnection#refreshToken}
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * This is a setter which sets refresh token
     * @param refreshToken
     */
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    /**
     * Gets the expire time 
     * @return {@link UserConnection#expireTime}
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * This is a setter which sets expire time
     * @param expireTime
     */
    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

}
