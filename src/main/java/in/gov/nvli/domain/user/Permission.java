/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.user;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the permission database table.
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @author Gulafsha<gulafsha@cdac.in>
 */
@Entity
@Table(name = "permission")
public class Permission implements Serializable {

    /**
     * variable use to hold permission id that should be unique.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "permission_id")
    @Basic(optional = false)
    private Long permission_id;

    /**
     * variable use to hold permission allowed for user
     */
    @NotNull
    @Column(name = "name")
    private String name;

    /**
     * variable use to hold
     */
    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "permissions",
            targetEntity = Role.class
    )
    private List<Role> roles;

    /**
     * Gets the permission id
     *
     * @return {@link Permission#permission_id}
     */
    public Long getPermission_id() {
        return permission_id;
    }

    /**
     * This is a setter which sets permission id
     *
     * @param permission_id
     */
    public void setPermission_id(Long permission_id) {
        this.permission_id = permission_id;
    }

    /**
     * Gets the permission name
     *
     * @return {@link Permission#name}
     */
    public String getName() {
        return name;
    }

    /**
     * This is a setter which sets the nvli link share status via app
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the roles assigned for that perticular permission
     *
     * @return {@link Permission#roles}
     */
    public List<Role> getRoles() {
        return roles;
    }

    /**
     * This is a setter which sets the nvli link share status via app
     *
     * @param roles
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
