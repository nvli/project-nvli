package in.gov.nvli.domain.user;

import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * The persistent class for the user_activity_log database table.
 *
 * @author Bhumika
 * @author Gulafsha
 * @since 1
 * @version 1
 */
@Entity
@Table(name = "user_activity_log")
public class UserActivityLog implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * variable use to hold log id
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_id")
    private Long logId;
    /**
     * variable use to hold activity time
     */
    @Column(name = "activity_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activityTime;
    /**
     * variable use to hold ip address
     */
    @Size(max = 255)
    @Column(name = "ip_address")
    private String ipAddress;
    /**
     * variable use to hold user agent info
     */
    @Size(max = 255)
    @Column(name = "user_agent_info")
    private String userAgentInfo;
    /**
     * variable use to hold unique Record identifier
     */
    @Size(max = 200)
    @Column(name = "unique_record_identifier")
    private String uniqueRecordIdentifier;
    /**
     * variable use to hold all activity of user,bi-directional many-to-one
     * association to UserAcivity
     */
    @ManyToOne
    @ForeignKey(name = "FK_user_activity_log_activity_id")
    @JoinColumn(name = "activity_id")
    private UserActivity userActivity;
    /**
     * variable use to hold user,bi-directional many-to-one association to User
     */
    @ManyToOne
    @ForeignKey(name = "FK_user_activity_log_user_id")
    @JoinColumn(name = "user_id")
    private User user;
    /**
     * variable use to hold user,bi-directional many-to-one association to User
     */
    @ManyToOne
    @ForeignKey(name = "FK_user_activity_log_affected_user_id")
    @JoinColumn(name = "affected_user_id")
    private User affectedUser;

    /**
     * constructor use to initialize properties of 
     * @see UserActivityLog
     */
    public UserActivityLog() {
    }

    /**
     * Gets the log id
     * @return {@link UserActivityLog#logId}
     */
    public Long getLogId() {
        return this.logId;
    }

    /**
     *  This is a setter which sets log id
     * @param logId
     */
    public void setLogId(Long logId) {
        this.logId = logId;
    }

    /**
     * Gets the activity time
     * @return {@link UserActivityLog#activityTime}
     */
    public Date getActivityTime() {
        return activityTime;
    }

    /**
     *  This is a setter which sets activity time
     * @param activityTime
     */
    public void setActivityTime(Date activityTime) {
        this.activityTime = activityTime;
    }

    /**
     * Gets the all user activities 
     * @return {@link UserActivityLog#userActivity}
     */
    public UserActivity getUserActivity() {
        return userActivity;
    }

    /**
     *  This is a setter which sets user activity of user
     * @param userActivity
     */
    public void setUserActivity(UserActivity userActivity) {
        this.userActivity = userActivity;
    }

    /**
     * Gets the ip address
     * @return {@link UserActivityLog#ipAddress}
     */
    public String getIpAddress() {
        return this.ipAddress;
    }

    /**
     * This is a setter which sets ip address
     * @param ipAddress
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Gets the Unique Record Identifier
     * @return {@link UserActivityLog#uniqueRecordIdentifier}
     */
    public String getUniqueRecordIdentifier() {
        return this.uniqueRecordIdentifier;
    }

    /**
     * This is a setter which sets Unique Record Identifier
     * @param uniqueRecordIdentifier
     */
    public void setUniqueRecordIdentifier(String uniqueRecordIdentifier) {
        this.uniqueRecordIdentifier = uniqueRecordIdentifier;
    }

    /**
     *  Gets the user 
     * @return {@link UserActivityLog#user}
     */
    public User getUser() {
        return this.user;
    }

    /**
     * This is a setter which sets user 
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *  Gets the affected user
     * @return {@link UserActivityLog#affectedUser}
     */
    public User getAffectedUser() {
        return affectedUser;
    }

    /**
     * This is a setter which sets affected user
     * @param affectedUser
     */
    public void setAffectedUser(User affectedUser) {
        this.affectedUser = affectedUser;
    }
    /**
     *  Gets the user agent info
     * @return {@link UserActivityLog#userAgentInfo}
     */
    public String getUserAgentInfo() {
        return userAgentInfo;
    }

    /**
     * This is a setter which sets user agent info of user
     * @param userAgentInfo
     */
    public void setUserAgentInfo(String userAgentInfo) {
        this.userAgentInfo = userAgentInfo;
    }
}
