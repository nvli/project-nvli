/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represent crowdsourced custom tags.
 *
 * @author Doppa Srinivas
 */
@Entity
@Table(name = "custom_tags")
@AttributeOverride(name = "id", column =
@Column(name = "custom_tag_id"))
public class CustomTags extends BaseEntity implements Serializable {

    /**
     * The property tagName used to store tag name.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "tag_name")
    private String tagName;

    /**
     * The property languageId used to store language Id.
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "language_id")
    private long languageId;

    /**
     * This getter is used to get tag name.
     *
     * @return String tag name.
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * This is setter which sets tag name.
     *
     * @param tagName
     */
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    /**
     * This getter is used to get language id.
     *
     * @return long language id.
     */
    public long getLanguageId() {
        return languageId;
    }

    /**
     * This is setter which sets language id.
     *
     * @param languageId
     */
    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }
}
