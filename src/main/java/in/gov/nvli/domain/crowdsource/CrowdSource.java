package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent a record crowdsourced by NVLI user.
 *
 * @author Doppa Srinivas
 */
@Entity
@Table(name = "crowd_source")
@AttributeOverride(name = "id", column
        = @Column(name = "crowd_source_id"))
public class CrowdSource extends BaseEntity implements Serializable {

    /**
     * The property recordIdentifier used to store record     * identifier.
     */
    @Column(name = "record_identifier", nullable = false)
    private String recordIdentifier;

    /**
     * The property subResourceCode used to store sub resource code.
     */
    @Column(name = "sub_resource_code", nullable = false)
    private String subResourceCode;

    /**
     * The property editNumber used to store edit number.
     */
    @Column(name = "edit_number", nullable = false)
    private Integer editNumber;

    /**
     * The property originalMetadataJson used to store original     * metadata json.
     */
    @Column(name = "original_metadata_json", columnDefinition = "TEXT", nullable = false)
    private String originalMetadataJson;

    /**
     * The property approvedMetadataJson used to store approved     * metadata json.
     */
    @Column(name = "approved_metadata_json", columnDefinition = "TEXT")
    private String approvedMetadataJson;

    /**
     * The property lastSubeditNumber used to store last sub edit     * number.
     */
    @Column(name = "last_subedit_number", nullable = false)
    private Integer lastSubeditNumber;

    /**
     * The the property isApproved used to store approve status for     * crowdsource record.
     */
    @Column(name = "is_approved", nullable = false, columnDefinition = "BIT(1) default false")
    private byte isApproved;

    /**
     * The property startTimestamp used to store start timestamp.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_timestamp", nullable = false)
    private Date startTimestamp;

    /**
     * The property endTimestamp used to store end timestamp.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_timestamp")
    private Date endTimestamp;

    /**
     * The property metadataType used to store metadata type.
     */
    @Column(name = "metadata_type", nullable = false)
    private String metadataType;

    /**
     * The property isUpdatedAtSource used to store updated status.
     */
    @Column(name = "is_updated_at_source", nullable = false, columnDefinition = "BIT(1) default false")
    private byte isUpdatedAtSource = 0;

    /**
     * The property recordTitle used to store record title.
     */
    @Column(name = "record_title", columnDefinition = "VARCHAR(500)")
    private String recordTitle;

    /**
     * The property allowedLanguages used to store allowed     * languages.
     */
    @Column(name = "allowed_languages", columnDefinition = "VARCHAR(10)")
    private String allowedLanguages;

    /**
     * The property expertUserId used to holds {@link User} object reference.
     * bi-directional many-to-one association to {@link User}
     */
    @JoinColumn(name = "expert_user_id")
    @ForeignKey(name="FK_crowd_source_user_id")
    @ManyToOne
    private User expertUserId;

    /**
     * The property crowdSourceEditSet used to holds {@link CrowdSourceEdit}
     * object reference and bi-directional one to many association to
     * CrowdSourceEdit
     */
    @OneToMany(mappedBy = "crowdSource", fetch = FetchType.LAZY)
    private Set<CrowdSourceEdit> crowdSourceEditSet;

    /**
     * This is default constructor.
     */
    public CrowdSource() {
    }

    /**
     * This getter is used to get approved status.
     *
     * @return approved status.
     */
    public byte getIsApproved() {
        return isApproved;
    }

    /**
     * This is setter which sets the approved status(in byte).
     *
     * @param isApproved approved status(in byte)
     */
    public void setIsApproved(byte isApproved) {
        this.isApproved = isApproved;
    }

    /**
     * This getter is used to get original metadata info.
     *
     * @return JSON string of original metadata info.
     */
    public String getOriginalMetadataJson() {
        return originalMetadataJson;
    }

    /**
     * This is setter which sets the original metadata JSON string.
     *
     * @param originalMetadataJson
     */
    public void setOriginalMetadataJson(String originalMetadataJson) {
        this.originalMetadataJson = originalMetadataJson;
    }

    /**
     * This getter is used to get approved metadata info.
     *
     * @return JSON string of approved metadata info.
     */
    public String getApprovedMetadataJson() {
        return approvedMetadataJson;
    }

    /**
     * This is setter which sets the approved metadata JSON string.
     *
     * @param approvedMetadataJson
     */
    public void setApprovedMetadataJson(String approvedMetadataJson) {
        this.approvedMetadataJson = approvedMetadataJson;
    }

    /**
     * This getter is used to get start timestamp.
     *
     * @return Date start timestamp.
     */
    public Date getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * This is setter which sets Date start timestamp.
     *
     * @param startTimestamp
     */
    public void setStartTimestamp(Date startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    /**
     * This getter is used to get end timestamp.
     *
     * @return Date end timestamp.
     */
    public Date getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * This is setter which sets Date end timestamp.
     *
     * @param endTimestamp
     */
    public void setEndTimestamp(Date endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    /**
     * This getter is used to get sub edit number.
     *
     * @return Integer last sub edit number.
     */
    public Integer getLastSubeditNumber() {
        return lastSubeditNumber;
    }

    /**
     * This is setter which sets last sub edit number..
     *
     * @param lastSubeditNumber
     */
    public void setLastSubeditNumber(Integer lastSubeditNumber) {
        this.lastSubeditNumber = lastSubeditNumber;
    }

    /**
     * This getter is used to get Set of {@link  CrowdSourceEdit}.
     *
     * @return Set of {@link  CrowdSourceEdit}.
     */
    public Set<CrowdSourceEdit> getCrowdSourceEditSet() {
        return crowdSourceEditSet;
    }

    /**
     * This is setter which sets Set of {@link CrowdSourceEdit}.
     *
     * @param CrowdSourceEditSet
     */
    public void setCrowdSourceEditSet(Set<CrowdSourceEdit> CrowdSourceEditSet) {
        this.crowdSourceEditSet = CrowdSourceEditSet;
    }

    /**
     * This getter is used to get record identifier.
     *
     * @return String record identifier.
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is setter which sets recordIdentifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This getter is used to get edit number.
     *
     * @return Integer edit number.
     */
    public Integer getEditNumber() {
        return editNumber;
    }

    /**
     * This is setter which sets edit number.
     *
     * @param editNumber
     */
    public void setEditNumber(Integer editNumber) {
        this.editNumber = editNumber;
    }

    /**
     * This getter is used to get metadata type.
     *
     * @return String metadata type.
     */
    public String getMetadataType() {
        return metadataType;
    }

    /**
     * This is setter which sets metadata type.
     *
     * @param metadataType
     */
    public void setMetadataType(String metadataType) {
        this.metadataType = metadataType;
    }

    /**
     * This getter is used to get updated at source status.
     *
     * @return byte updated at source status.
     */
    public byte getIsUpdatedAtSource() {
        return isUpdatedAtSource;
    }

    /**
     * This is setter which sets updated at source status.
     *
     * @param isUpdatedAtSource
     */
    public void setIsUpdatedAtSource(byte isUpdatedAtSource) {
        this.isUpdatedAtSource = isUpdatedAtSource;
    }

    /**
     * This getter is used to get record title.
     *
     * @return String record title.
     */
    public String getRecordTitle() {
        return recordTitle;
    }

    /**
     * This is setter which sets record title.
     *
     * @param recordTitle
     */
    public void setRecordTitle(String recordTitle) {
        this.recordTitle = recordTitle;
    }

    /**
     * This getter is used to get allowed language's.
     *
     * @return String allowed language's.
     */
    public String getAllowedLanguages() {
        return allowedLanguages;
    }

    /**
     * This is setter which sets allowed language's.
     *
     * @param allowedLanguages
     */
    public void setAllowedLanguages(String allowedLanguages) {
        this.allowedLanguages = allowedLanguages;
    }

    /**
     * This getter is used to get expert id.
     *
     * @return expert id.
     */
    public User getExpertUserId() {
        return expertUserId;
    }

    /**
     * This is setter which sets expert id.
     *
     * @param expertUserId
     */
    public void setExpertUserId(User expertUserId) {
        this.expertUserId = expertUserId;
    }

    /**
     * This getter is used to get sub resource code.
     *
     * @return String sub resource code.
     */
    public String getSubResourceCode() {
        return subResourceCode;
    }

    /**
     * This is setter which sets sub resource code.
     *
     * @param subResourceCode
     */
    public void setSubResourceCode(String subResourceCode) {
        this.subResourceCode = subResourceCode;
    }
}
