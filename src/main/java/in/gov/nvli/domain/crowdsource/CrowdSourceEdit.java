package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent edited crowdsourced record.
 *
 * @author Doppa Srinivas
 */
@Entity
@Table(name = "crowd_source_edit")
@AttributeOverride(name = "id", column =
@Column(name = "crowd_source_edit_id"))
public class CrowdSourceEdit extends BaseEntity implements Serializable,Comparable<CrowdSourceEdit> {

    /**
     * The property subEditNumber used to store sub edit number.
     */
    @Column(name = "sub_edit_number",nullable = false)
    private Integer subEditNumber;

    /**
     * The property editedMetadataJson used to store edited metadata JSON.
     */
    @Column(name = "edited_metadata_json",nullable = false, columnDefinition = "TEXT")
    private String editedMetadataJson;

    /**
     * The property crowdSourceEditTimestamp used to store date of modified
     * crowdsource article.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "crowd_source_edit_timestamp",nullable = false)
    private Date crowdSourceEditTimestamp;

    /**
     * The property crowdSource used to holds {@link CrowdSource} object
     * reference. bi-directional many-to-one association to {@link CrowdSource}
     */
    @ManyToOne
    @ForeignKey(name="crowd_source_id_crowd_source_fk1")
    @JoinColumn(name="crowd_source_id",nullable=false)
    private CrowdSource crowdSource;
    
    /**
     * The property user used to holds {@link User} object reference.
     * bi-directional many-to-one association to {@link User}
     */
    @JoinColumn(name = "user_id")
    @ForeignKey(name="FK_crowd_source_edit_user_id")
    @ManyToOne
    private User user;

    /**
     * The property isCuratedVersion used to store curated version status.
     */
    @Column(name = "is_curated_version", nullable = false, columnDefinition = "BIT(1) default false")
    private boolean isCuratedVersion;
    
    /**
     * This default constructor.
     */
    public CrowdSourceEdit() {
    }

    /**
     * This getter is used to get edited metadata JSON.
     *
     * @return String edited metadata JSON.
     */
    public String getEditedMetadataJson() {
        return editedMetadataJson;
    }

    /**
     * This is setter which sets edited metadata JSON.
     *
     * @param editedMetadataJson
     */
    public void setEditedMetadataJson(String editedMetadataJson) {
        this.editedMetadataJson = editedMetadataJson;
    }

    /**
     * This getter is used to get {@link CrowdSource} object.
     *
     * @return {@link CrowdSource} object.
     */
    public CrowdSource getCrowdSource() {
        return crowdSource;
    }

    /**
     * This is setter which sets {@link CrowdSource} object.
     *
     * @param crowdSource
     */
    public void setCrowdSource(CrowdSource crowdSource) {
        this.crowdSource = crowdSource;
    }

    /**
     * This getter is used to get sub edit number.
     *
     * @return Integer sub edit number.
     */
    public Integer getSubEditNumber() {
        return subEditNumber;
    }

    /**
     * This is setter which sets sub edit number.
     *
     * @param subEditNumber
     */
    public void setSubEditNumber(Integer subEditNumber) {
        this.subEditNumber = subEditNumber;
    }

    /**
     * This getter is used to get edited crowdsourced record timestamp.
     *
     * @return Date edited crowdsourced record timestamp.
     */
    public Date getCrowdSourceEditTimestamp() {
        return crowdSourceEditTimestamp;
    }

    /**
     * This is setter which sets edited crowdsourced record timestamp.
     *
     * @param crowdSourceEditTimestamp
     */
    public void setCrowdSourceEditTimestamp(Date crowdSourceEditTimestamp) {
        this.crowdSourceEditTimestamp = crowdSourceEditTimestamp;
    }

    /**
     * This getter is used to get {@link User} object.
     *
     * @return {@link User} object.
     */
    public User getUser() {
        return user;
    }

    /**
     * This is setter which sets {@link User} object.
     *
     * @param user {@link User} object.
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This getter is used to get curated version status(in boolean).
     *
     * @return boolean curated version status(in boolean).
     */
    public boolean getIsCuratedVersion() {
        return isCuratedVersion;
    }

    /**
     * This is setter which sets curated version status(in boolean).
     *
     * @param isCuratedVersion
     */
    public void setIsCuratedVersion(boolean isCuratedVersion) {
        this.isCuratedVersion = isCuratedVersion;
    }
    
    @Override
    public int compareTo(CrowdSourceEdit object) {
        return this.subEditNumber-object.getSubEditNumber();
    }
}