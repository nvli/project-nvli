/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent udc concept.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "udc_concept")
@NamedQueries({
    @NamedQuery(name = "findByUDCNotation", query = "From UDCConcept udcconcept where udcconcept.udcNotation=?")})
@AttributeOverride(name = "id", column
        = @Column(name = "udc_concept_id"))
public class UDCConcept extends BaseEntity implements Serializable, Comparable<UDCConcept> {
    /**
     * This represent the notation of UDC tag.
     */
    @Column(name = "udc_notation", length = 45)
    private String udcNotation;
    /**
     * The conceptUrl of a UDC tag.
     */
    @Column(name = "concept_url", length = 500)
    private String conceptUrl;
    /**
     * The set of {@link UDCConcept} objects.
     */
    @OneToMany(mappedBy = "broaderId", fetch = FetchType.LAZY)
    private Set<UDCConcept> udcConceptSet;
    /**
     * This refer to parent of the {@link UDCConcept}
     */
    @JoinColumn(name = "broader_id", referencedColumnName = "udc_concept_id")
    @ForeignKey(name = "broader_key")
    @ManyToOne(fetch = FetchType.LAZY)
    private UDCConcept broaderId;
    /**
     *
     */
    @Column(name = "udc_notation_recordcount", length = 500)
    private Long udcNotationRecordCount;
    /**
     *
     */
    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "userSpecializations",
            targetEntity = User.class
    )
    private List<User> users;
    /**
     *
     */
    @OneToMany(mappedBy = "udcConceptId", fetch = FetchType.LAZY)
    private Set<UDCConceptDescription> udcConceptDescriptionSet;
    /**
     *
     */
    @OneToMany(mappedBy = "udcConcept", fetch = FetchType.LAZY)
    private Set<UDCTagsCrowdsource> udcTagsCrowdsourcesSet;
    
    /**
     * The constructor.
     */
    public UDCConcept() {
    }

    /**
     * This is to get the udcNotation.
     *
     * @return udcNotation
     */
    public String getUdcNotation() {
        return udcNotation;
    }

    /**
     * This is to set the value of udcNotation.
     *
     * @param udcNotation
     */
    public void setUdcNotation(String udcNotation) {
        this.udcNotation = udcNotation;
    }

    /**
     * This is to get the value of conceptUrl.
     *
     * @return conceptUrl
     */
    public String getConceptUrl() {
        return conceptUrl;
    }

    /**
     * This is to set the value of conceptUrl.
     *
     * @param conceptUrl
     */
    public void setConceptUrl(String conceptUrl) {
        this.conceptUrl = conceptUrl;
    }

    /**
     * This is to get the value of udcConceptSet.
     *
     * @return udcConceptSet
     */
    public Set<UDCConcept> getUdcConceptSet() {
        return udcConceptSet;
    }

    /**
     * This is to set the value of udcConceptSet.
     *
     * @param udcConceptSet
     */
    public void setUdcConceptSet(Set<UDCConcept> udcConceptSet) {
        this.udcConceptSet = udcConceptSet;
    }

    /**
     * This is to get the value of broaderId.
     *
     * @return broaderId
     */
    public UDCConcept getBroaderId() {
        return broaderId;
    }

    /**
     * This is to set the value of broaderId.
     *
     * @param broaderId
     */
    public void setBroaderId(UDCConcept broaderId) {
        this.broaderId = broaderId;
    }

    /**
     * This is to get the value of udcNotationRecordCount.
     *
     * @return udcNotationRecordCount
     */
    public Long getUdcNotationRecordCount() {
        return udcNotationRecordCount;
    }

    /**
     * This is to set the value of udcNotationRecordCount.
     *
     * @param udcNotationRecordCount
     */
    public void setUdcNotationRecordCount(Long udcNotationRecordCount) {
        this.udcNotationRecordCount = udcNotationRecordCount;
    }

    /**
     * This is to get the value of udcConceptDescriptionSet.
     *
     * @return udcConceptDescriptionSet
     */
    public Set<UDCConceptDescription> getUdcConceptDescriptionSet() {
        return udcConceptDescriptionSet;
    }

    /**
     * This is to set the value of udcConceptDescriptionSet.
     *
     * @param udcConceptDescriptionSet
     */
    public void setUdcConceptDescriptionSet(Set<UDCConceptDescription> udcConceptDescriptionSet) {
        this.udcConceptDescriptionSet = udcConceptDescriptionSet;
    }

    /**
     * This is to set the value of udcTagsCrowdsourcesSet.
     *
     * @return udcTagsCrowdsourcesSet
     */
    public Set<UDCTagsCrowdsource> getUdcTagsCrowdsourcesSet() {
        return udcTagsCrowdsourcesSet;
    }

    /**
     * This is to get the value of udcTagsCrowdsourcesSet.
     *
     * @param udcTagsCrowdsourcesSet
     */
    public void setUdcTagsCrowdsourcesSet(Set<UDCTagsCrowdsource> udcTagsCrowdsourcesSet) {
        this.udcTagsCrowdsourcesSet = udcTagsCrowdsourcesSet;
    }

    /**
     * This is to get the value of users.
     *
     * @return users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * This is to set the value of users.
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public int compareTo(UDCConcept o) {
        return this.udcNotation.compareTo(o.getUdcNotation());
    }    
}