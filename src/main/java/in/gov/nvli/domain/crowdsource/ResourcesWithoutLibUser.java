package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.resource.Resource;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Represent resources without library user.
 *
 * @author Vivek Bugale
 * @version 1
 * @since 1 
 */

@Entity
@Table(name = "resources_without_lib_user")
public class ResourcesWithoutLibUser implements Serializable{
    /**
     * This field is to hold the value of the resource unique id.
     */
    @GenericGenerator(name = "generator", strategy = "foreign",
    parameters = @Parameter(name = "property", value = "resource"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "resource_id", unique = true, nullable = false)
    private Long resourceId;
    /**
     * @see Resource
     */
    @OneToOne
    @ForeignKey(name = "resources_without_lib_user_resource_id_fk1")
    @PrimaryKeyJoinColumn
    private Resource resource;       

    /**
     * This is to get the value of resourceId.
     *
     * @return resourceId
     */
    public Long getResourceId() {
        return resourceId;
    }

    /**
     * This is to set the value of resourceId.
     *
     * @param resourceId
     */
    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * This is to get the value of {@link Resource} object.
     *
     * @return {@link Resource}
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * This is to set the value of resource.
     *
     * @param resource
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.resourceId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourcesWithoutLibUser other = (ResourcesWithoutLibUser) obj;
        if (!Objects.equals(this.resourceId, other.resourceId)) {
            return false;
        }
        return true;
    }  
}
