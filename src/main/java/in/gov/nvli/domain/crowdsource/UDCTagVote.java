/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent udc tag vote.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "udc_tag_vote")
@AttributeOverride(name = "id", column =
@Column(name = "udc_tag_vote_id"))
public class UDCTagVote extends BaseEntity implements Serializable {
    /**
     * @see User
     */
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ForeignKey(name="FK_udc_tag_vote_user_id")
    private User user;
    /**
     * @see UDCTagsCrowdsource
     */
    @JoinColumn(name = "udc_tags_crowdsource_id", referencedColumnName = "udc_tags_crowdsource_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @ForeignKey(name="FK_udc_tag_vote_udc_tags_crowdsource_id")
    private UDCTagsCrowdsource udcTagsCrowdsource;

    /**
     * Empty constructor.
     */
    public UDCTagVote() {
    }

    /**
     * This is to get the value of user.
     *
     * @return user
     */
    public User getUser() {
        return user;
    }

    /**
     * This is to set the value of user.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This is to get the value of udcTagsCrowdsource.
     *
     * @return udcTagsCrowdsource
     */
    public UDCTagsCrowdsource getUdcTagsCrowdsource() {
        return udcTagsCrowdsource;
    }

    /**
     * This is to set the value of udcTagsCrowdsource.
     *
     * @param udcTagsCrowdsource
     */
    public void setUdcTagsCrowdsource(UDCTagsCrowdsource udcTagsCrowdsource) {
        this.udcTagsCrowdsource = udcTagsCrowdsource;
    }
}