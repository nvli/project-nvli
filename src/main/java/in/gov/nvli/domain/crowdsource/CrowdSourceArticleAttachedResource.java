package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent a attached articles to the article which is crowdsourced by NVLI
 * user.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "crowd_source_article_attached_resource")
@AttributeOverride(name = "id", column =
@Column(name = "attached_resource_id"))
public class CrowdSourceArticleAttachedResource extends BaseEntity implements Serializable {

    /**
     * The property article used to holds {@link CrowdSourceArticle} object
     * reference. bi-directional many-to-one association to
     * {@link CrowdSourceArticle}
     */
    @ManyToOne
    @ForeignKey(name="FK_crowd_source_article_attached_resource_article_id")
    @JoinColumn(name = "article_id", nullable = false)
    private CrowdSourceArticle article;

    /**
     * The property resourceLabel used to store resource label.
     */
    @Column(name = "resource_label", nullable = false ,length = 500)
    private String resourceLabel;

    /**
     * The property resourceURL used to store resource URL.
     */
    @Column(name = "resource_url")
    private String resourceURL;

    /**
     * This is the default constructor.
     */
    public CrowdSourceArticleAttachedResource() {
    }

    /**
     * This getter is used to get {@link CrowdSourceArticle} object.
     *
     * @return {@link CrowdSourceArticle} object.
     */
    public CrowdSourceArticle getArticle() {
        return article;
    }

    /**
     * This is setter which sets {@link CrowdSourceArticle} object.
     *
     * @param article
     */
    public void setArticle(CrowdSourceArticle article) {
        this.article = article;
    }

    /**
     * This getter is used to get resource URL.
     *
     * @return String resource URL.
     */
    public String getResourceURL() {
        return resourceURL;
    }

    /**
     * This is setter which sets resource URL.
     *
     * @param resourceURL
     */
    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL;
    }

    /**
     * This getter is used to get resource label.
     *
     * @return String resource label.
     */
    public String getResourceLabel() {
        return resourceLabel;
    }

    /**
     * This is setter which sets resource label.
     *
     * @param resourceLabel
     */
    public void setResourceLabel(String resourceLabel) {
        this.resourceLabel = resourceLabel;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CrowdSourceArticleAttachedResource other = (CrowdSourceArticleAttachedResource) obj;
        if (!Objects.equals(this.getId(), other.getId())) {
            return false;
        }
        return true;
    }
}