package in.gov.nvli.domain.crowdsource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Represent udc language.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "udc_language")
@AttributeOverride(name = "id", column
        = @Column(name = "language_id"))
public class UDCLanguage extends BaseEntity implements Serializable {

    /**
     * The unique code of language.
     */
    @Column(name = "language_code", length = 10)
    private String languageCode;
    /**
     * The name of the language.
     */
    @Column(name = "language_name", length = 100)
    private String languageName;
    /**
     * This is to filter the indian languages with other language.
     */
    @Column(name = "is_indian_language", nullable = false)
    private byte isIndianLanguage;
    /**
     * Set of {@link UDCConceptDescription}
     */
    @JsonIgnore
    @OneToMany(mappedBy = "languageId", fetch = FetchType.LAZY)
    private Set<UDCConceptDescription> uDCConceptDescriptionSet;
    /**
     * set of {@link UDCTagsCrowdsource}
     */
    @JsonIgnore
    @OneToMany(mappedBy = "udcLanguage", fetch = FetchType.LAZY)
    private Set<UDCTagsCrowdsource> udcTagsCrowdsourcesSet;
    /**
     * List of {@link User}
     */
    @JsonIgnore
    @ManyToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "userLanguages",
            targetEntity = User.class
    )
    private List<User> users;

    /**
     * Empty constructor.
     */
    public UDCLanguage() {
    }

    /**
     * This is to get the value of languageCode.
     *
     * @return languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * This is to set the value of languageCode.
     *
     * @param languageCode
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * This is to get the value of languageName.
     *
     * @return languageName
     */
    public String getLanguageName() {
        return languageName;
    }

    /**
     * This is to set the value of languageName.
     *
     * @param languageName
     */
    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    /**
     * This is to get the value of uDCConceptDescriptionSet.
     *
     * @return uDCConceptDescriptionSet
     */
    public Set<UDCConceptDescription> getuDCConceptDescriptionSet() {
        return uDCConceptDescriptionSet;
    }

    /**
     * This is to set the value of uDCConceptDescriptionSet.
     *
     * @param uDCConceptDescriptionSet
     */
    public void setuDCConceptDescriptionSet(Set<UDCConceptDescription> uDCConceptDescriptionSet) {
        this.uDCConceptDescriptionSet = uDCConceptDescriptionSet;
    }

    /**
     * This is to get the value of isIndianLanguage.
     *
     * @return isIndianLanguage
     */
    public byte getIsIndianLanguage() {
        return isIndianLanguage;
    }

    /**
     * This is to set the value of isIndianLanguage.
     *
     * @param isIndianLanguage
     */
    public void setIsIndianLanguage(byte isIndianLanguage) {
        this.isIndianLanguage = isIndianLanguage;
    }

    /**
     * This is to get the value of users.
     *
     * @return users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * This is to set the value of users.
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * This is to get the value of udcTagsCrowdsourcesSet.
     *
     * @return udcTagsCrowdsourcesSet
     */
    public Set<UDCTagsCrowdsource> getUdcTagsCrowdsourcesSet() {
        return udcTagsCrowdsourcesSet;
    }

    /**
     * This is to set the value of udcTagsCrowdsourcesSet.
     *
     * @param udcTagsCrowdsourcesSet
     */
    public void setUdcTagsCrowdsourcesSet(Set<UDCTagsCrowdsource> udcTagsCrowdsourcesSet) {
        this.udcTagsCrowdsourcesSet = udcTagsCrowdsourcesSet;
    }

    /**
     * This is to get the value of LanguageComparator.
     *
     * @return LanguageComparator
     */
    public static Comparator<UDCLanguage> getLanguageComparator() {
        return LanguageComparator;
    }

    /**
     * This is to set the value of LanguageComparator.
     *
     * @param LanguageComparator
     */
    public static void setLanguageComparator(Comparator<UDCLanguage> LanguageComparator) {
        UDCLanguage.LanguageComparator = LanguageComparator;
    }

    /**
     * @see Comparator
     */
    public static Comparator<UDCLanguage> LanguageComparator = new Comparator<UDCLanguage>() {
        @Override
        public int compare(UDCLanguage udclanguage1, UDCLanguage udclanguage2) {
            String language1 = udclanguage1.getLanguageName().toLowerCase();
            String language2 = udclanguage2.getLanguageName().toLowerCase();
            return language1.compareToIgnoreCase(language2);
        }
    };
}
