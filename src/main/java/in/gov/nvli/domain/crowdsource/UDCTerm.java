/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Represent udc term.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "udc_term")
@AttributeOverride(name = "id", column =
@Column(name = "udc_term_id"))
public class UDCTerm extends BaseEntity implements Serializable {
    /**
     *
     */
    @Column(name = "term",length = 45)
    private String term;
    /**
     * Set of {@link UDCConceptDescription}
     */
    @OneToMany(mappedBy = "udcTermId", fetch = FetchType.LAZY)
    private Set<UDCConceptDescription> uDCConceptDescriptionSet;

    /**
     * Empty constructor.
     */
    public UDCTerm() {
    }

    /**
     * This is to get the value of term.
     *
     * @return term
     */
    public String getTerm() {
        return term;
    }

    /**
     * This is to set the value of term.
     *
     * @param term
     */
    public void setTerm(String term) {
        this.term = term;
    }

    /**
     * This is to get the value of uDCConceptDescriptionSet.
     *
     * @return uDCConceptDescriptionSet
     */
    public Set<UDCConceptDescription> getuDCConceptDescriptionSet() {
        return uDCConceptDescriptionSet;
    }

    /**
     * This is to set the value of uDCConceptDescriptionSet.
     *
     * @param uDCConceptDescriptionSet
     */
    public void setuDCConceptDescriptionSet(Set<UDCConceptDescription> uDCConceptDescriptionSet) {
        this.uDCConceptDescriptionSet = uDCConceptDescriptionSet;
    }
}