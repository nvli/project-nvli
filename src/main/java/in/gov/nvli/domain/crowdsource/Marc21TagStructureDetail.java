package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the marc21_tag_structure_detail database table.
 * 
 */
@Entity
@Table(name="marc21_tag_structure_detail")
@AttributeOverride(name = "id", column =
@Column(name = "marc21_tag_structure_id"))
public class Marc21TagStructureDetail extends BaseEntity implements Serializable {

    /**
     * The property frameworkcode used to store framework code.
     */
	@Column(nullable=false, length=4)
	private String frameworkcode;

    /**
     * The property librarianTitle used to store librarian title.
     */
	@Column(name="librarian_title", nullable=false, length=255)
	private String librarianTitle;

    /**
     * The property mandatory used to store mandatory status.
     */
	@Column(nullable=false)
	private byte mandatory;

    /**
     * The property opacTitle used to store opac title.
     */
	@Column(name="opac_title", nullable=false, length=255)
	private String opacTitle;

    /**
     * The property repeatable used to store repeatable status.
     */
	@Column(nullable=false)
	private byte repeatable;

    /**
     * The property tagField used to store tag field.
     */
	@Column(name="tag_field", nullable=false, length=3)
	private String tagField;

    /**
     * This is default constructor.
     */
    public Marc21TagStructureDetail() {
	}

    /**
     * This getter is used to get framework code.
     *
     * @return String framework code.
     */
    public String getFrameworkcode() {
		return this.frameworkcode;
	}

    /**
     * This is setter which sets framework code.
     *
     * @param frameworkcode
     */
    public void setFrameworkcode(String frameworkcode) {
		this.frameworkcode = frameworkcode;
	}

    /**
     * This getter is used to get librarian title.
     *
     * @return String librarian title.
     */
    public String getLibrarianTitle() {
		return this.librarianTitle;
	}

    /**
     * This is setter which sets librarian title.
     *
     * @param librarianTitle
     */
    public void setLibrarianTitle(String librarianTitle) {
		this.librarianTitle = librarianTitle;
	}

    /**
     * This getter is used to get mandatory status(in byte).
     *
     * @return byte mandatory status(in byte).
     */
    public byte getMandatory() {
		return this.mandatory;
	}

    /**
     * This is setter which sets mandatory status(in byte).
     *
     * @param mandatory
     */
    public void setMandatory(byte mandatory) {
		this.mandatory = mandatory;
	}

    /**
     * This getter is used to get opac title.
     *
     * @return String opac title.
     */
    public String getOpacTitle() {
		return this.opacTitle;
	}

    /**
     * This is setter which sets opac title.
     *
     * @param opacTitle
     */
    public void setOpacTitle(String opacTitle) {
		this.opacTitle = opacTitle;
	}

    /**
     * This getter is used to get repeatable status(in byte).
     *
     * @return byte repeatable status(in byte).
     */
    public byte getRepeatable() {
		return this.repeatable;
	}

    /**
     * This is setter which sets repeatable status(in byte).
     *
     * @param repeatable
     */
    public void setRepeatable(byte repeatable) {
		this.repeatable = repeatable;
	}

    /**
     * This getter is used to get tag field.
     *
     * @return String tag field.
     */
    public String getTagField() {
		return this.tagField;
	}

    /**
     * This is setter which sets tag field.
     *
     * @param tagField
     */
    public void setTagField(String tagField) {
		this.tagField = tagField;
	}

}