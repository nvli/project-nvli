/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent udc tags crowdsource.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "udc_tags_crowdsource")
@AttributeOverride(name = "id", column =
@Column(name = "udc_tags_crowdsource_id"))
public class UDCTagsCrowdsource extends BaseEntity implements Serializable {
    /**
     * @see UDCConcept
     */
    @ManyToOne
    @ForeignKey(name="FK_udc_tags_crowdsource_udc_concept_id")
    @JoinColumn(name = "udc_concept_id", nullable = false)
    private UDCConcept udcConcept;
    /**
     * @see UDCLanguage
     */
    @ManyToOne
    @ForeignKey(name="FK_udc_tags_crowdsource_language_id")
    @JoinColumn(name = "language_id", nullable = false)
    private UDCLanguage udcLanguage;
    /**
     * @see User
     */
    @ManyToOne
    @ForeignKey(name="FK_udc_tags_crowdsource_user_id")
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    /**
     * This refer to the description of the {@link UDCTagsCrowdsource}
     */
    @Column(nullable = false, length = 8000)
    private String description;
    /**
     * This tells that whether the tag translation is reviewed by expert or not.
     */
    @Column(name = "is_reviewed", nullable = false)
    private boolean isReviewed;
    /**
     * Total count of the vote.
     */
    @Column(name = "vote_count", nullable = false)
    private int voteCount;
    /**
     * Whether is tag translation is accepted by expert or not.
     */
    @Column(name = "is_accepted", nullable = false)
    private boolean isAccepted;
    /**
     * Set of {@link UDCTagVote}
     */
    @OneToMany(mappedBy = "udcTagsCrowdsource", fetch = FetchType.LAZY)
    private Set<UDCTagVote> udcTagVoteSet;
    
    /**
     * The property publishDate used to store publish date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "translation_date")
    private Date translationDate;
    
    /**
     * The property publishDate used to store publish date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reviewed_date")
    private Date reviewedDate;
    
    /**
     * The property uDCTranslationExpertId used to holds {@link UDCTranslationExperts} object
     * reference. bi-directional many-to-one association to {@link UDCTranslationExperts}
     */
    @JoinColumn(name = "udc_translation_id")
    @ForeignKey(name = "udc_translation_expert_id")
    @ManyToOne
    private UDCTranslationExperts uDCTranslationExpertId;    
    
    /**
     * Empty constructor.
     */
    public UDCTagsCrowdsource() {
    }

    /**
     * This is to get the value of udcConcept.
     *
     * @return udcConcept
     */
    public UDCConcept getUdcConcept() {
        return udcConcept;
    }

    /**
     * This is to set the value of udcConcept.
     *
     * @param udcConcept
     */
    public void setUdcConcept(UDCConcept udcConcept) {
        this.udcConcept = udcConcept;
    }

    /**
     * This is to get the value of udcLanguage.
     *
     * @return udcLanguage
     */
    public UDCLanguage getUdcLanguage() {
        return udcLanguage;
    }

    /**
     * This is to set the value of udcLanguage.
     *
     * @param udcLanguage
     */
    public void setUdcLanguage(UDCLanguage udcLanguage) {
        this.udcLanguage = udcLanguage;
    }

    /**
     * This is to get the value of user.
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * This is to set the value of user.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This is to get the value of description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * This is to set the value of description.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * This is to get the value of isReviewed.
     *
     * @return isReviewed
     */
    public boolean getIsReviewed() {
        return isReviewed;
    }

    /**
     * This is to set the value of isReviewed.
     *
     * @param isReviewed
     */
    public void setIsReviewed(boolean isReviewed) {
        this.isReviewed = isReviewed;
    }

    /**
     * This is to get the value of voteCount.
     *
     * @return voteCount
     */
    public int getVoteCount() {
        return voteCount;
    }

    /**
     * This is to set the value of voteCount.
     *
     * @param voteCount
     */
    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    /**
     * This is to get the value of isAccepted.
     *
     * @return isAccepted
     */
    public boolean getIsAccepted() {
        return isAccepted;
    }

    /**
     * This is to set the value of isAccepted.
     *
     * @param isAccepted
     */
    public void setIsAccepted(boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    /**
     * This is to get the value of udcTagVoteSet.
     *
     * @return udcTagVoteSet
     */
    public Set<UDCTagVote> getUdcTagVoteSet() {
        return udcTagVoteSet;
    }

    /**
     * This is to set the value of udcTagVoteSet.
     *
     * @param udcTagVoteSet
     */
    public void setUdcTagVoteSet(Set<UDCTagVote> udcTagVoteSet) {
        this.udcTagVoteSet = udcTagVoteSet;
    }

    /**
     * This is to get the value of translationDate.
     * @return 
     */
    public Date getTranslationDate() {
        return translationDate;
    }

    /**
     * This is to set the value of translationDate.
     * @param translationDate 
     */
    public void setTranslationDate(Date translationDate) {
        this.translationDate = translationDate;
    }

    /**
     * This is to get the value of reviewedDate.
     * @return 
     */
    public Date getReviewedDate() {
        return reviewedDate;
    }

    /**
     * This is to set the value of reviewedDate.
     * @param reviewedDate 
     */
    public void setReviewedDate(Date reviewedDate) {
        this.reviewedDate = reviewedDate;
    }   

    public UDCTranslationExperts getuDCTranslationExpertId() {
        return uDCTranslationExpertId;
    }

    public void setuDCTranslationExpertId(UDCTranslationExperts uDCTranslationExpertId) {
        this.uDCTranslationExpertId = uDCTranslationExpertId;
    }
    

    
}