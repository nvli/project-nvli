package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent udc concept description.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "udc_concept_description")
@AttributeOverride(name = "id", column =
@Column(name = "concept_description_id"))
public class UDCConceptDescription extends BaseEntity implements Serializable {
    /**
     * The description of {@link UDCConcept}
     */
    @Column(name = "description",length = 8000)
    private String description;
    /**
     * {@link UDCTerm}
     */
    @JoinColumn(name = "udc_term_id", referencedColumnName = "udc_term_id")
    @ForeignKey(name="fk3")
    @ManyToOne(fetch = FetchType.LAZY)
    private UDCTerm udcTermId;
    /**
     * {@link UDCLanguage}
     */
    @JoinColumn(name = "language_id", referencedColumnName = "language_id")
    @ForeignKey(name="fk2")
    @ManyToOne(fetch = FetchType.LAZY)
    private UDCLanguage languageId;
    /**
     * {@link UDCConcept}
     */
    @JoinColumn(name = "udc_concept_id", referencedColumnName = "udc_concept_id")
    @ForeignKey(name="fk1")
    @ManyToOne(fetch = FetchType.LAZY)
    private UDCConcept udcConceptId;
    
    /**
     * The property descriptionHistory used to store history of description.
     */
    @Column(name = "description_history", columnDefinition = "TEXT")
    private String descriptionHistory;

    /**
     * Empty constructor.
     */
    public UDCConceptDescription() {
    }

    /**
     * This is to get the value of description.
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * This is to set the value of description.
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * This is to get the value of udcTermId.
     *
     * @return udcTermId
     */
    public UDCTerm getUdcTermId() {
        return udcTermId;
    }

    /**
     * This is to set the value of udcTermId.
     *
     * @param udcTermId
     */
    public void setUdcTermId(UDCTerm udcTermId) {
        this.udcTermId = udcTermId;
    }

    /**
     * This is to get the value of languageId.
     *
     * @return languageId
     */
    public UDCLanguage getLanguageId() {
        return languageId;
    }

    /**
     * This is to set the value of languageId.
     *
     * @param languageId
     */
    public void setLanguageId(UDCLanguage languageId) {
        this.languageId = languageId;
    }

    /**
     * This is to get the value of udcConceptId.
     *
     * @return udcConceptId
     */
    public UDCConcept getUdcConceptId() {
        return udcConceptId;
    }

    /**
     * This is to set the value of udcConceptId.
     *
     * @param udcConceptId
     */
    public void setUdcConceptId(UDCConcept udcConceptId) {
        this.udcConceptId = udcConceptId;
    }

    /**
     * This is to set the value of descriptionHistory.
     * @return 
     */
    public String getDescriptionHistory() {
        return descriptionHistory;
    }

    /**
     * This is to set the value of descriptionHistory.
     * @param descriptionHistory 
     */
    public void setDescriptionHistory(String descriptionHistory) {
        this.descriptionHistory = descriptionHistory;
    }
}