package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent custom tags of crowdsourced record.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "crowd_source_custom_tag")
@AttributeOverride(name = "id", column =
@Column(name = "tag_id"))
public class CrowdSourceCustomTag extends BaseEntity implements Serializable {

    /**
     * The property recordIdentifier used to store record identifier.
     */
    @Column(name = "record_identifier", nullable = false)
    private String recordIdentifier;

    /**
     * The property tags used to store crowdsource custom tags;
     */
    @Column(name = "tags")
    private String tags;

    /**
     * The property isUpdatedAtSource used to store updated at source status(in
     * byte).
     */
    @Column(name = "is_updated_at_source", nullable = false,columnDefinition = "BIT(1) default false")
    private byte isUpdatedAtSource=0;

    /**
     * The property user used to holds {@link User} object reference.
     * bi-directional many-to-one association to {@link User}
     */
    @JoinColumn(name = "user_id")
    @ForeignKey(name="FK_crowd_source_custom_tag_user_id")
    @ManyToOne
    private User user;

    /**
     * This getter is used to get {@link User} object.
     *
     * @return User object.
     */
    public User getUser() {
        return user;
    }
 
    /**
     * This is setter which sets {@link User} object.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }
    
    /**
     * This getter is used to get record identifier.
     *
     * @return String record identifier.
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is setter which sets record identifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This getter is used to get crowdsource custom tags.
     *
     * @return String custom tags.
     */
    public String getTags() {
        return tags;
    }

    /**
     * This is setter which sets crowdsource custom tags.
     *
     * @param tags
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     * This getter is used to get updated at source status(in byte).
     *
     * @return byte updated at source status(in byte).
     */
    public byte getIsUpdatedAtSource() {
        return isUpdatedAtSource;
    }

    /**
     * This is setter which sets updated at source status(in byte).
     *
     * @param isUpdatedAtSource
     */
    public void setIsUpdatedAtSource(byte isUpdatedAtSource) {
        this.isUpdatedAtSource = isUpdatedAtSource;
    } 
}