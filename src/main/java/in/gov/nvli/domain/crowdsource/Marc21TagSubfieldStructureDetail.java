package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the marc21_tag_subfield_structure_detail database table.
 * 
 */
@Entity
@Table(name="marc21_tag_subfield_structure_detail")
@AttributeOverride(name = "id", column =
@Column(name = "marc21_tag_subfield_structure_id"))
public class Marc21TagSubfieldStructureDetail extends BaseEntity implements Serializable {
    /**
     *
     */
    @Column(name="librarian_title", nullable=false, length=255)
    private String librarianTitle;
    /**
     *
     */
    @Column(nullable=false)
    private byte mandatory;
    /**
     *
     */
    @Column(name="opac_title", nullable=false, length=255)
    private String opacTitle;
    /**
     *
     */
    @Column(nullable=false)
    private byte repeatable;
    /**
     *
     */
    @Column(name="tag_field", nullable=false, length=3)
    private String tagField;
    /**
     *
     */
    @Column(name="tag_subfield", length=1)
    private String tagSubfield;

    /**
     * This is empty constructor.
     */
    public Marc21TagSubfieldStructureDetail() {
    }

    /**
     * This is to get the librarianTitle.
     *
     * @return librarianTitle
     */
    public String getLibrarianTitle() {
            return this.librarianTitle;
    }

    /**
     * This is to set value of librarianTitle
     *
     * @param librarianTitle
     */
    public void setLibrarianTitle(String librarianTitle) {
            this.librarianTitle = librarianTitle;
    }

    /**
     * This is to get the mandatory value
     *
     * @return mandatory
     */
    public byte getMandatory() {
            return this.mandatory;
    }

    /**
     * This is to set the mandatory value.
     *
     * @param mandatory
     */
    public void setMandatory(byte mandatory) {
            this.mandatory = mandatory;
    }

    /**
     * This is to get value of the opacTitle.
     *
     * @return  opacTitle
     */
    public String getOpacTitle() {
            return this.opacTitle;
    }

    /**
     * This is to set value of the opacTitle.
     *
     * @param opacTitle
     */
    public void setOpacTitle(String opacTitle) {
            this.opacTitle = opacTitle;
    }

    /**
     * This is to get value of the repeatable.
     *
     * @return repeatable
     */
    public byte getRepeatable() {
            return this.repeatable;
    }

    /**
     * This is to set value of the repeatable.
     *
     * @param repeatable
     */
    public void setRepeatable(byte repeatable) {
            this.repeatable = repeatable;
    }

    /**
     * This is to get value of the tagField.
     *
     * @return tagField.
     */
    public String getTagField() {
            return this.tagField;
    }

    /**
     * This is to set value of the tagField.
     *
     * @param tagField
     */
    public void setTagField(String tagField) {
            this.tagField = tagField;
    }

    /**
     * This is to get value of the tagSubfield.
     *
     * @return tagSubfield
     */
    public String getTagSubfield() {
            return this.tagSubfield;
    }

    /**
     * This is to set value of the tagSubfield.
     *
     * @param tagSubfield
     */
    public void setTagSubfield(String tagSubfield) {
            this.tagSubfield = tagSubfield;
    }
}