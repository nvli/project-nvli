package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.ForeignKey;

/**
 *
 * @author Vivek Bugale
 */
@Entity
@Table(name = "udc_translation_expert")
@AttributeOverride(name = "id", column
        = @Column(name = "udc_translation_expert_id"))
public class UDCTranslationExperts extends BaseEntity implements Serializable{

    /**
     * The property udcConceptDescription used to holds {@link UDCConceptDescription} object
     * reference. bi-directional many-to-one association to {@link UDCConceptDescription}
     */
    @JoinColumn(name = "udc_concept_description_id")
    @ForeignKey(name = "FK_udc_translation_expert_udc_concept_description_id")
    @ManyToOne
    private UDCConceptDescription udcConceptDescription;

    /**
     * The property udcLanguage used to holds {@link UDCLanguage}
     * object reference. bi-directional many-to-one association to
     * {@link UDCLanguage}
     */
    @JoinColumn(name = "translation_language_id")
    @ForeignKey(name = "FK_udc_translation_expert_udc_language_id")
    @ManyToOne
    private UDCLanguage udcLanguage;

    /**
     * The property expert used to holds {@link User} object reference.
     * bi-directional many-to-one association to {@link User}
     */
    @JoinColumn(name = "expert_user_id", nullable = true)
    @ForeignKey(name = "FK_udc_translation_expert_user_id")
    @ManyToOne
    private User expert;

    public UDCConceptDescription getUdcConceptDescription() {
        return udcConceptDescription;
    }

    public void setUdcConceptDescription(UDCConceptDescription udcConceptDescription) {
        this.udcConceptDescription = udcConceptDescription;
    }

    public UDCLanguage getUdcLanguage() {
        return udcLanguage;
    }

    public void setUdcLanguage(UDCLanguage udcLanguage) {
        this.udcLanguage = udcLanguage;
    }

    public User getExpert() {
        return expert;
    }

    public void setExpert(User expert) {
        this.expert = expert;
    }
}
