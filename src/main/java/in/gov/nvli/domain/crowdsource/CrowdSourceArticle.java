package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.user.User;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.ForeignKey;

/**
 * Represent a article crowdsourced by NVLI user.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "crowd_source_article")
@AttributeOverride(name = "id", column =
@Column(name = "article_id"))
public class CrowdSourceArticle extends BaseEntity implements Serializable {

    /**
     * The property articleTitle used to store article title.
     */
    @Column(name = "article_title", nullable = false, length = 1000)
    private String articleTitle;

    /**
     * The property articleText used to store article text.
     */
    @Column(name = "article_text", columnDefinition = "LONGTEXT")
    private String articleText;   

    /**
     *
     * The property articleCategory used to holds {@link Resource} object
     * reference. bi-directional many-to-one association to {@link Resource}.
     */
    @ManyToOne
    @ForeignKey(name = "FK_crowd_source_article_category_id")
    @JoinColumn(name = "article_category",nullable = false)
    private Resource articleCategory;

    /**
     *
     * The property user used to holds {@link User} object reference.
     * bi-directional many-to-one association to {@link User}.
     */
    @ManyToOne
    @ForeignKey(name="FK_crowd_source_article_user_id")
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    /**
     * The property isPublished used to store published status.
     */
    @Column(name = "is_published", nullable = false)
    private boolean isPublished;

    /**
     * The property isApproved used to store approved status.
     */
    @Column(name = "is_approved", nullable = false)
    private boolean isApproved;

    /**
     * The property udcTags used to store udc tags.
     */
    @Column(name = "udc_tags",columnDefinition = "TEXT")
    private String udcTags;

    /**
     * The property customTags used to store custom tags.
     */
    @Column(name = "custom_tags",columnDefinition = "TEXT")
    private String customTags;

    /**
     * The property publishDate used to store publish date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "publish_date")
    private Date publishDate;

    /**
     * The property lastModifiedDate used to store last modified date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_modified_date", nullable = false)
    private Date lastModifiedDate;

    /**
     * The property relatedArticles used to store related articles.
     */
    @Column(name = "related_articles")
    private String relatedArticles;

    /**
     * The property crowdSourceArticleAttachedResources used to store Set of
     * {@link CrowdSourceArticleAttachedResource}.
     */
    @OneToMany(mappedBy = "article",fetch = FetchType.LAZY)
    private Set<CrowdSourceArticleAttachedResource> crowdSourceArticleAttachedResources;

    /**
     * This is default constructor.
     */
    public CrowdSourceArticle() {
    }

    /**
     * This getter is used to get article title.
     *
     * @return String article title.
     */
    public String getArticleTitle() {
        return articleTitle;
    }

    /**
     * This is setter which sets article title.
     *
     * @param articleTitle
     */
    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    /**
     * This getter is used to get article text.
     *
     * @return String article text.
     */
    public String getArticleText() {
        return articleText;
    }

    /**
     * This getter is used to get article category.
     *
     * @return Resource article category.
     */
    public Resource getArticleCategory() {
        return articleCategory;
    }

    /**
     * This is setter which sets article category.
     *
     * @param articleCategory
     */
    public void setArticleCategory(Resource articleCategory) {
        this.articleCategory = articleCategory;
    }

    /**
     * This is setter which sets article text.
     *
     * @param articleText
     */
    public void setArticleText(String articleText) {
        this.articleText = articleText;
    }   

    /**
     * This getter is used to get user object.
     *
     * @return User object.
     */
    public User getUser() {
        return user;
    }

    /**
     * This is setter which sets user object.
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This getter is used to get udc tags.
     *
     * @return String udc tags.
     */
    public String getUdcTags() {
        return udcTags;
    }

    /**
     * This is setter which sets udc tags.
     *
     * @param udcTags
     */
    public void setUdcTags(String udcTags) {
        this.udcTags = udcTags;
    }

    /**
     * This getter is used to get custom tags.
     *
     * @return String custom tags.
     */
    public String getCustomTags() {
        return customTags;
    }

    /**
     * This is setter which sets custom tags.
     *
     * @param customTags
     */
    public void setCustomTags(String customTags) {
        this.customTags = customTags;
    }

    /**
     * This getter is used to get publish status.
     *
     * @return publish status.
     */
    public boolean getIsPublished() {
        return isPublished;
    }

    /**
     * This getter is used to get last modified date.
     *
     * @return Date last modified.
     */
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * This is setter which sets last modified date.
     *
     * @param lastModifiedDate
     */
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    } 
    
    /**
     * This is setter which sets publish status.
     *
     * @param isPublished
     */
    public void setIsPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    /**
     * This getter is used to get approved status.
     *
     * @return boolean approved status.
     */
    public boolean getIsApproved() {
        return isApproved;
    }

    /**
     * This is setter which sets approved status.
     *
     * @param isApproved
     */
    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
    }

    /**
     * This getter is used to get related articles.
     *
     * @return String related articles.
     */
    public String getRelatedArticles() {
        return relatedArticles;
    }

    /**
     * This is setter which sets related articles.
     *
     * @param relatedArticles
     */
    public void setRelatedArticles(String relatedArticles) {
        this.relatedArticles = relatedArticles;
    }

    /**
     * This getter is used to get Set of
     * {@link CrowdSourceArticleAttachedResource}.
     *
     * @return Set of {@link CrowdSourceArticleAttachedResource}.
     */
    public Set<CrowdSourceArticleAttachedResource> getCrowdSourceArticleAttachedResources() {
        return crowdSourceArticleAttachedResources;
    }

    /**
     * This is setter which sets Set of
     * {@link CrowdSourceArticleAttachedResource}.
     *
     * @param crowdSourceArticleAttachedResources
     */
    public void setCrowdSourceArticleAttachedResources(Set<CrowdSourceArticleAttachedResource> crowdSourceArticleAttachedResources) {
        this.crowdSourceArticleAttachedResources = crowdSourceArticleAttachedResources;
    }

    /**
     * This getter is used to get publish date.
     *
     * @return Date publish date.
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     * This is setter which sets publish date.
     *
     * @param publishDate
     */
    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }
}