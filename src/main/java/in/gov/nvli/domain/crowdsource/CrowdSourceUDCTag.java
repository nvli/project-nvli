/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.crowdsource;

import in.gov.nvli.domain.generic.BaseEntity;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Represent crowdsourced udc tag.
 *
 * @author Ritesh Malviya
 */
@Entity
@Table(name = "crowd_source_udc_tag")
@AttributeOverride(name = "id", column
        = @Column(name = "tag_id"))
public class CrowdSourceUDCTag extends BaseEntity implements Serializable {

    /**
     * The property recordIdentifier used to store record identifier.
     */
    @Column(name = "record_identifier", nullable = false)
    private String recordIdentifier;

    /**
     * The property originalTags used to store original tags.
     */
    @Column(name = "original_tags")
    private String originalTags;

    /**
     * The property tagsAdded used to store added tag.
     */
    @Column(name = "tags_added")
    private String tagsAdded;

    /**
     * The property approvedTags used to store approved tags.
     */
    @Column(name = "approved_tags")
    private String approvedTags;

    /**
     * The property isApproved used to store udc tag approved status.
     */
    @Column(name = "is_approved", nullable = false, columnDefinition = "BIT(1) default false")
    private byte isApproved;

    /**
     * The property isUpdatedAtSource used to store updated at source status.
     */
    @Column(name = "is_updated_at_source", nullable = false, columnDefinition = "BIT(1) default false")
    private byte isUpdatedAtSource = 0;

    /**
     * The property recordTitle used to store record title.
     */
    @Column(name = "record_title", columnDefinition = "VARCHAR(500)")
    private String recordTitle;

    /**
     * This getter is used to get record identifier.
     *
     * @return String record identifier
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * This is setter which sets record identifier.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * This getter is used to get original tags.
     *
     * @return String original tags.
     */
    public String getOriginalTags() {
        return originalTags;
    }

    /**
     * This is setter which sets original tags.
     *
     * @param originalTags
     */
    public void setOriginalTags(String originalTags) {
        this.originalTags = originalTags;
    }

    /**
     * This getter is used to get tags added.
     *
     * @return String tags added.
     */
    public String getTagsAdded() {
        return tagsAdded;
    }

    /**
     * This is setter which sets tags added.
     *
     * @param tagsAdded
     */
    public void setTagsAdded(String tagsAdded) {
        this.tagsAdded = tagsAdded;
    }

    /**
     * This getter is used to get approved tags.
     *
     * @return String approved tags.
     */
    public String getApprovedTags() {
        return approvedTags;
    }

    /**
     * This is setter which sets approved tags.
     *
     * @param approvedTags
     */
    public void setApprovedTags(String approvedTags) {
        this.approvedTags = approvedTags;
    }

    /**
     * This getter is used to get udc tag approved status(in byte).
     *
     * @return byte udc tag approved status(in byte).
     */
    public byte getIsApproved() {
        return isApproved;
    }

    /**
     * This is setter which sets udc tag approved status(in byte).
     *
     * @param isApproved
     */
    public void setIsApproved(byte isApproved) {
        this.isApproved = isApproved;
    }

    /**
     * This getter is used to get udc tag updated status(in byte).
     *
     * @return byte udc tag updated status(in byte).
     */
    public byte getIsUpdatedAtSource() {
        return isUpdatedAtSource;
    }

    /**
     * This is setter which sets udc tag updated status(in byte).
     *
     * @param isUpdatedAtSource
     */
    public void setIsUpdatedAtSource(byte isUpdatedAtSource) {
        this.isUpdatedAtSource = isUpdatedAtSource;
    }

    /**
     * This getter is used to get record title.
     *
     * @return String record title.
     */
    public String getRecordTitle() {
        return recordTitle;
    }

    /**
     * This is setter which sets record title.
     *
     * @param recordTitle
     */
    public void setRecordTitle(String recordTitle) {
        this.recordTitle = recordTitle;
    }
}
