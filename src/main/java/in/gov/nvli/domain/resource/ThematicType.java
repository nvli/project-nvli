package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.UserInterest;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ThematicType class used create new thematic classification like science,
 * technology, engineering etc.
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "thematictype")
@AttributeOverride(name = "id", column
        = @Column(name = "thematicId"))
@NamedQueries({
    @NamedQuery(name = "findByThematicId", query = "SELECT thematicType FROM ThematicType thematicType where thematicType.id=?")
})
public class ThematicType extends BaseEntity implements Serializable {

    /**
     * The property thematicType used to store thematicType
     */
    @Column(name = "thematicType", nullable = false, unique = true)
    private String thematicType;
    /**
     * The property resourceTypes used to store set of {@link ResourceType}.Its
     * a many to many relationship with thematic type.One ThematicType may
     * contains one or more ResourceType.
     */

    @ManyToMany(targetEntity = ResourceType.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "thematic_resourcetype_relation", joinColumns
            = @JoinColumn(name = "thematicId", nullable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "resourceTypeId", nullable = true, unique = false))
    private Set<ResourceType> resourceTypes = new HashSet<ResourceType>();
    /**
     * The property resources used to store set of {@link Resource}.Its a many
     * to many relationship with thematic type.one resource may have zero or
     * many thematic types
     */
    @JsonIgnore
    @ManyToMany(targetEntity = Resource.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "thematic_resource_relation", joinColumns
            = @JoinColumn(name = "thematicId", nullable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "resourceId", nullable = true, unique = false))
    private Set<Resource> resources = new HashSet<Resource>();
    /**
     * The property interests used to holds List of {@link UserInterest} object
     * reference.
     */
    @JsonIgnore
    @OneToMany(mappedBy = "thematicType", fetch = FetchType.LAZY)
    private List<UserInterest> interests;

    /**
     * The getThematicType method used to get thematicType
     *
     * @return thematicType
     */
    public String getThematicType() {
        return thematicType;
    }

    /**
     * The setThematicType method used to set thematicType
     *
     * @param thematicType
     */
    public void setThematicType(String thematicType) {
        this.thematicType = thematicType;
    }

    /**
     * The getResourceTypes method used to get, set of {@link ResourceType}.Its
     * a many to many relationship with thematic type.One ThematicType may
     * contains one or more ResourceType.
     *
     * @return resourceTypes
     */
    public Set<ResourceType> getResourceTypes() {
        return resourceTypes;
    }

    /**
     * The setResourceTypes method used to set, set of {@link ResourceType}.Its
     * a many to many relationship with thematic type.One ThematicType may
     * contains one or more ResourceType.
     *
     * @param resourceTypes
     */
    public void setResourceTypes(Set<ResourceType> resourceTypes) {
        this.resourceTypes = resourceTypes;
    }

    /**
     * The getResources method used to get, set of {@link Resource}.Its a many
     * to many relationship with thematic type.one resource may have zero or
     * many thematic types
     *
     * @return
     */
    public Set<Resource> getResources() {
        return resources;
    }

    /**
     * The setResources method used to set, set of {@link Resource}.Its a many
     * to many relationship with thematic type.one resource may have zero or
     * many thematic types
     *
     * @param resources
     */
    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

    /**
     * The getInterests method used to get List of {@link UserInterest} object
     * reference.
     *
     * @return interests
     */
    public List<UserInterest> getInterests() {
        return interests;
    }

    /**
     * The setInterests method used to set List of {@link UserInterest} object
     * reference.
     *
     * @param interests
     */
    public void setInterests(List<UserInterest> interests) {
        this.interests = interests;
    }
    /**
     * ThemesComparator used to sort {@link ThematicType} based on thematicType
     * property in ascending order
     */
    public static Comparator<ThematicType> ThemesComparator = new Comparator<ThematicType>() {
        @Override
        public int compare(ThematicType thematicType1, ThematicType thematicType2) {
            String theme1 = thematicType1.getThematicType().toLowerCase();
            String theme2 = thematicType2.getThematicType().toLowerCase();
            return theme1.compareToIgnoreCase(theme2);
        }
    };
}
