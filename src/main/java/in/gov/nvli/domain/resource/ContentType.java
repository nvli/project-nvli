package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * For content classification like e-thesis, papers, Records etc.
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "contenttype")
@AttributeOverride(name = "id", column
        = @Column(name = "contentTypeId"))
@NamedQueries({
    @NamedQuery(name = "findByContentTypeId", query = "SELECT contentType FROM ContentType contentType where contentType.id=?")
})
public class ContentType extends BaseEntity implements Serializable {

    /**
     * The property contentType used to store content name
     */
    @Column(name = "contentType", nullable = false, unique = true)
    private String contentType;
    
       /**
     * The property resources used to store set of {@link Resource}.Its a many
     * to many relationship with content type.one resource may have zero or
     * many content types
     */
    @JsonIgnore
    @ManyToMany(targetEntity = Resource.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "content_resource_relation", joinColumns
            = @JoinColumn(name = "contentTypeId", nullable = true, updatable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "resourceId", nullable = true, updatable = true, unique = false))
    private Set<Resource> resources = new HashSet<Resource>();

    /**
     * The getContentType method used to get content name
     *
     * @return contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * The setContentType method used to set content name
     *
     * @param contentType
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    /**
     * ContentComparator used to sort {@link ContentType} based on contentType
     * property in ascending order
     */
    public static Comparator<ContentType> ContentComparator = new Comparator<ContentType>() {
        @Override
        public int compare(ContentType contentType1, ContentType contenTypet2) {
            String content1 = contentType1.getContentType().toLowerCase();
            String content2 = contenTypet2.getContentType().toLowerCase();
            return content1.compareToIgnoreCase(content2);
        }
    };
}
