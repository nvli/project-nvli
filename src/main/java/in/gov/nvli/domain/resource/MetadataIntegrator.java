package in.gov.nvli.domain.resource;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.web.multipart.MultipartFile;

/**
 * MetadataIntegrator class is used to add new Meta-data Integrator Resource. It
 * is a Sub class of resource. MetadataIntegrator extends Resource. Common
 * fields for resource creation maintained in Resource class and Content with
 * meta data related properties are in MetadataIntegrator class
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "metadata_integrator")
@PrimaryKeyJoinColumn(name = "resourceId")
public class MetadataIntegrator extends Resource implements Serializable {

    /**
     * The property contentWithMetadata holds the boolean value for incoming
     * data with meta data or not
     */
    @Column(name = "contentWithMetadata", columnDefinition = "bit(1)")
    private boolean contentWithMetadata;
    /**
     * The property classificationLevel used to store classification Levels like
     * National , State or District level
     */
    private String classificationLevel;
    /**
     * The property establishYear used to store Resource establish Year
     */
    private String establishYear;
    /**
     * The property totalCount used to store Resource record count
     */
    private String totalCount;
    /**
     * The property xlsFileUpload used to hold uploaded XLS file which contains
     * Resource meta data
     */
    @Transient
    private MultipartFile xlsFileUpload;

    /**
     * The isContentWithMetadata method used to get the boolean value for
     * incoming data with meta data or not
     *
     * @return contentWithMetadata
     */
    public boolean isContentWithMetadata() {
        return contentWithMetadata;
    }

    /**
     * The setContentWithMetadata method used to set the boolean value for
     * incoming data with meta data or not
     *
     * @param contentWithMetadata
     */
    public void setContentWithMetadata(boolean contentWithMetadata) {
        this.contentWithMetadata = contentWithMetadata;
    }

    /**
     * The getClassificationLevel method used to get classification Levels like
     * National , State or District level
     *
     * @return
     */
    public String getClassificationLevel() {
        return classificationLevel;
    }

    /**
     * The getClassificationLevel method used to set classification Levels like
     * National , State or District level
     *
     * @param classificationLevel
     */
    public void setClassificationLevel(String classificationLevel) {
        this.classificationLevel = classificationLevel;
    }

    /**
     * The getEstablishYear method used to get Resource establish Year
     *
     * @return establishYear
     */
    public String getEstablishYear() {
        return establishYear;
    }

    /**
     * The setEstablishYear method used to set Resource establish Year
     *
     * @param establishYear
     */
    public void setEstablishYear(String establishYear) {
        this.establishYear = establishYear;
    }

    /**
     * The getTotalCount method used to get Resource record count
     *
     * @return totalCount
     */
    public String getTotalCount() {

        return totalCount;
    }

    /**
     * The setTotalCount method used to set Resource record count
     *
     * @param totalCount
     */
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * The getXlsFile method used to get uploaded XLS file which contains
     * Resource meta data
     *
     * @return xlsFileUpload
     */
    public MultipartFile getXlsFile() {
        return xlsFileUpload;
    }

    /**
     * The getXlsFile method used to set uploaded XLS file which contains
     * Resource meta data
     *
     * @param xlsFile
     */
    public void setXlsFile(MultipartFile xlsFile) {
        this.xlsFileUpload = xlsFile;
    }

}
