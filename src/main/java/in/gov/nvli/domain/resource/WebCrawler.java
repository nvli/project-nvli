package in.gov.nvli.domain.resource;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * WebCrawler class used to create the Government web site resource. It is a Sub
 * class of resource. WebCrawler extends Resource. Common fields for resource
 * creation maintained in Resource class and Government web site related
 * properties are in WebCrawler class
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "web_crawler")
@PrimaryKeyJoinColumn(name = "resourceId")
public class WebCrawler extends Resource implements Serializable {

    /**
     * The property webUrl used to store resource Web site URL of the
     */
    private String webUrl;
    /**
     * The property twitterUrl used to store resource Twitter Web site URL
     */
    private String twitterUrl;
    /**
     * The property facebookUrl used to store resource Facebook Web site URL
     */
    private String facebookUrl;

    /**
     * The getWebUrl method used to get resource Web site URL of the
     *
     * @return webUrl
     */
    public String getWebUrl() {
        return webUrl;
    }

    /**
     * The setWebUrl method used to set resource Web site URL of the
     *
     * @param webUrl
     */
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    /**
     * The getTwitterUrl method used to get resource Twitter Web site URL
     *
     * @return twitterUrl
     */
    public String getTwitterUrl() {
        return twitterUrl;
    }

    /**
     * The setTwitterUrl method used to set resource Twitter Web site URL
     *
     * @param twitterUrl
     */
    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    /**
     * The getFacebookUrl method used to get resource Facebook Web site URL
     *
     * @return facebookUrl
     */
    public String getFacebookUrl() {
        return facebookUrl;
    }

    /**
     * The setFacebookUrl method used to set resource Facebook Web site URL
     *
     * @param facebookUrl
     */
    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

}
