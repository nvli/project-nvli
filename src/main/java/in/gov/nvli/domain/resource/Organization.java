package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.CreatableEntity;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * To hold information of organization
 *
 * @author Sujata Aher
 *
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "organization")
@AttributeOverride(name = "id", column
        = @Column(name = "organization_id"))
public class Organization extends CreatableEntity implements Serializable {

    /**
     * Variable to hold name of organization. Should be unique.
     */
    @Column(name = "name", unique = true, nullable = false)
    private String name;
    /**
     * Variable to hold organization's contact person's name.
     */
    private String contactPersonName;
    /**
     * Variable to hold organization's contact person's designation.
     */
    private String designation;
    /**
     * Variable to hold organization's address.
     */
    private String address;
    /**
     * Variable to hold organization's contact person's email.
     */
    private String email;
    /**
     * Variable to hold organization's contact person's phone number.
     */
    private String phoneNumber;
    /**
     * Variable to hold organization's web url.
     */
    private String organisationUrl;
    /**
     * Variable to hold name of city.
     */
    private String city;
    /**
     * Variable to hold name of country.
     */
    private String country;
    /**
     * Variable to hold latitude of organization location.
     */
    private String latitude;
    /**
     * Variable to hold longitude of organization location..
     */
    private String longitude;
    /**
     * The property stateName used to store state name of resource Organization.
     */
    private String stateName;
    /**
     * resources variable holds the List of Resources
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organization", targetEntity = Resource.class)
    private List<Resource> resources = new ArrayList();

    /**
     * The property users used to holds list of {@link User} object reference.
     * It is used identify how many organizations are assigned for particular
     * user.
     */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "assignedOrganizations")
    private List<User> users;

    /**
     * Getter method for name parameter
     *
     * @return String name of organization.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name parameter
     *
     * @param name of organization.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for contactPersonName parameter
     *
     * @return String contact person's name
     */
    public String getContactPersonName() {
        return contactPersonName;
    }

    /**
     * Setter method for contactPersonName parameter
     *
     * @param contactPersonName
     */
    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    /**
     * Getter method for designation parameter
     *
     * @return String designation of contact person
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Setter method for designation parameter
     *
     * @param designation of contact person
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * Getter method for address parameter
     *
     * @return String address of organization
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter method for address parameter
     *
     * @param address of organization
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Getter method for email parameter
     *
     * @return String email of contact person
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method for email parameter
     *
     * @param email of contact person
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter method for phoneNumber parameter
     *
     * @return String phone number of contact person
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Setter method for phoneNumber parameter
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Getter method for organisationUrl parameter
     *
     * @return String url of organization
     */
    public String getOrganisationUrl() {
        return organisationUrl;
    }

    /**
     * Setter method for organisationUrl parameter
     *
     * @param organisationUrl
     */
    public void setOrganisationUrl(String organisationUrl) {
        this.organisationUrl = organisationUrl;
    }

    /**
     * Getter method for city parameter
     *
     * @return String name of city
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter method for city parameter
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Getter method for country parameter
     *
     * @return String name of country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Setter method for country parameter
     *
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Getter method for latitude parameter
     *
     * @return String latitude of organization location
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Setter method for latitude parameter
     *
     * @param latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * Getter method for longitude parameter
     *
     * @return longitude of organization location
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Setter method for longitude parameter
     *
     * @param longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * Getter method for resources parameter
     *
     * @return list of {@link Resource}
     */
    public List<Resource> getResources() {
        return resources;
    }

    /**
     * Setter method for resources parameter
     *
     * @param resources list of {@link Resource}
     */
    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    /**
     *
     * @return
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * To get a hash code value for the object.
     *
     * @return hash code value
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.name);
        return hash;
    }

    /**
     * The getStateName method used to get stateName of resource holding agency
     *
     * @return stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * The setStateName method used to set stateName of resource holding agency
     *
     * @param stateName
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * Compares whether some other object is "equal to" this one.
     *
     * @param obj the reference object with which to compare.
     * @return true if this object is the same as the obj argument; false
     * otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Organization other = (Organization) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    /**
     * OrganizationComparator used to sort {@link Organization} based on
     * organization name property in ascending order
     */
    public static Comparator<Organization> OrganizationComparator = new Comparator<Organization>() {
        /**
         * To compares two Organization object by its name parameter.
         *
         * @param organization1 {@link Organization} first parameter to compare
         * @param organization2 {@link Organization} second parameter to compare
         * @return a negative integer, zero, or a positive integer as the first
         * argument is less than, equal to, or greater than the second.
         */
        @Override
        public int compare(Organization organization1, Organization organization2) {
            String content1 = organization1.getName().toLowerCase();
            String content2 = organization2.getName().toLowerCase();
            return content1.compareToIgnoreCase(content2);
        }
    };

}
