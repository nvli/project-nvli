package in.gov.nvli.domain.resource;

import java.util.Objects;

/**
 * NewsFeed class holds feed URL and its section/category name for e-news
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
public class NewsFeed {

    /**
     * The property labelName used to store News feed name
     */
    private String labelName;
    /**
     * The property feedUrl used to store News feed URL
     */
    private String feedUrl;
    /**
     * The property activeFeed used to holds boolean values for News feed URL
     * active or not
     */
    private boolean activeFeed;

    /**
     * The getLabelName method used to get News feed name
     *
     * @return labelName
     */
    public String getLabelName() {
        return labelName;
    }

    /**
     * The setLabelName method used to set News feed name
     *
     * @param labelName
     */
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    /**
     * The getFeedUrl method used to get News feed URL
     *
     * @return feedUrl
     */
    public String getFeedUrl() {
        return feedUrl;
    }

    /**
     * The setFeedUrl method used to set News feed URL
     *
     * @param feedUrl
     */
    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }

    /**
     * The isActiveFeed method used to get boolean values for News feed URL
     * active or not
     *
     * @return activeFeed
     */
    public boolean isActiveFeed() {
        return activeFeed;
    }

    /**
     * The setActiveFeed method used to set boolean values for News feed URL
     * active or not
     *
     * @param activeFeed
     */
    public void setActiveFeed(boolean activeFeed) {
        this.activeFeed = activeFeed;
    }

    /**
     * The hashCode method used to generate unique hashing code for NewsFeed URL
     * Object
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.feedUrl);
        hash = hash >>> 10;
        return hash;
    }

    /**
     * The equals method used to check whether same NewsFeed URL Object or not
     *
     * @param obj
     * @return boolean value true or false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NewsFeed other = (NewsFeed) obj;
        if (!Objects.equals(this.feedUrl, other.feedUrl)) {
            return false;
        }
        return true;
    }

}
