package in.gov.nvli.domain.resource;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * OpenRepository class used create new Open repository Resource.It is a Sub
 * class of resource. OpenRepository extends Resource. Common fields for
 * resource creation maintained in Resource class and Open Repository related
 * properties are in OpenRepository class
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "open_repository")
@PrimaryKeyJoinColumn(name = "resourceId")
public class OpenRepository extends Resource implements Serializable {

    /**
     * The property harvestUrl used to store harvest URL
     */
    private String harvestUrl;
    /**
     * The property repositoryUrl used to store Repository URL
     */
    private String repositoryUrl;
    /**
     * The property earliestDatestamp used to store earliestDatestamp
     */
    private String earliestDatestamp;
    /**
     * The property harvestSoftware used to store repository using software
     * names
     */
    private String harvestSoftware;
    /**
     * The property harvestStartTime used to store harvesting Start timestamp
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date harvestStartTime;
    /**
     * The property harvestEndTime used to store harvesting End timestamp
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date harvestEndTime;
    /**
     * The property totalHarvestTime used to store total duration taken for
     * harvesting timestamp
     */
    private Long totalHarvestTime;
    /**
     * The property recordMetadataStandards used to store List of
     * {@link HarRecordMetadata}.Its a one to many relationship with
     * OpenRepository.one OpenRepository resource may have 1 or many meta data
     * standards
     */
    @OneToMany(fetch = FetchType.LAZY, targetEntity = HarRecordMetadata.class, mappedBy = "openRepository", cascade = CascadeType.ALL)
    private List<HarRecordMetadata> recordMetadataStandards=new ArrayList<>();

    /**
     * The getHarvestUrl method used to get harvest URL
     *
     * @return harvestUrl
     */
    public String getHarvestUrl() {
        return harvestUrl;
    }

    /**
     * The setHarvestUrl method used to set harvest URL
     *
     * @param harvestUrl
     */
    public void setHarvestUrl(String harvestUrl) {
        this.harvestUrl = harvestUrl;
    }

    /**
     * The getRepositoryUrl method used to get Repository URL
     *
     * @return repositoryUrl
     */
    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    /**
     * The setRepositoryUrl method used to set Repository URL
     *
     * @param repositoryUrl
     */
    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    /**
     * The getEarliestDatestamp method used to get earliestDatestamp
     *
     * @return earliestDatestamp
     */
    public String getEarliestDatestamp() {
        return earliestDatestamp;
    }

    /**
     * The setEarliestDatestamp method used to set earliestDatestamp
     *
     * @param earliestDatestamp
     */
    public void setEarliestDatestamp(String earliestDatestamp) {
        this.earliestDatestamp = earliestDatestamp;
    }

    /**
     * The harvestSoftware method used to get repository using software names
     *
     * @return harvestSoftware
     */
    public String getHarvestSoftware() {
        return harvestSoftware;
    }

    /**
     * The harvestSoftware method used to set repository using software names
     *
     * @param harvestSoftware
     */
    public void setHarvestSoftware(String harvestSoftware) {
        this.harvestSoftware = harvestSoftware;
    }

    /**
     * The getHarvestStartTime method used to get harvesting Start timestamp
     *
     * @return harvestStartTime
     */
    public Date getHarvestStartTime() {
        return harvestStartTime;
    }

    /**
     * The setHarvestStartTime method used to set harvesting Start timestamp
     *
     * @param harvestStartTime
     */
    public void setHarvestStartTime(Date harvestStartTime) {
        this.harvestStartTime = harvestStartTime;
    }

    /**
     * The getHarvestEndTime method used to get harvesting End timestamp
     *
     * @return harvestEndTime
     */
    public Date getHarvestEndTime() {
        return harvestEndTime;
    }

    /**
     * The setHarvestEndTime method used to set harvesting End timestamp
     *
     * @param harvestEndTime
     */
    public void setHarvestEndTime(Date harvestEndTime) {
        this.harvestEndTime = harvestEndTime;
    }

    /**
     * The getTotalHarvestTime method used to get total duration taken for
     * harvesting timestamp
     *
     * @return totalHarvestTime as a long
     */
    public Long getTotalHarvestTime() {
        return totalHarvestTime;
    }

    /**
     * The setTotalHarvestTime method used to set total duration taken for
     * harvesting timestamp
     *
     * @param totalHarvestTime
     */
    public void setTotalHarvestTime(Long totalHarvestTime) {
        this.totalHarvestTime = totalHarvestTime;
    }

    /**
     * The getRecordMetadataStandards method used to get List of
     * HarRecordMetadata. Its a one to many relationship with OpenRepository.one
     * OpenRepository resource may have 1 or many meta data standards
     *
     * @return List of recordMetadataStandards {@link HarRecordMetadata}
     */
    public List<HarRecordMetadata> getRecordMetadataStandards() {
        return recordMetadataStandards;
    }

    /**
     * The setRecordMetadataStandards method used to set List of
     * HarRecordMetadata. Its a one to many relationship with OpenRepository.one
     * OpenRepository resource may have 1 or many meta data standards
     *
     * @param recordMetadataStandards
     */
    public void setRecordMetadataStandards(List<HarRecordMetadata> recordMetadataStandards) {
        this.recordMetadataStandards = recordMetadataStandards;
    }
}
