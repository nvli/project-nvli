/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.domain.resource;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author Gajraj Tanwar
 */
@Entity
@Table(name = "artist_site")
@TableGenerator(name = "nvliSeqGenerator", allocationSize = 1, initialValue = 1, table = Constants.PORTAL_TABLE_PREFIX + "nvli_sequence")
public class ArtisticResourceType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int artisticResourseId;

    @Column(name = "artist_site", unique = true, nullable = false)
    private String artistSite;

    @Column(name = "site_name")
    private String siteName;

    @OneToMany(mappedBy = "artistResourceID", targetEntity = ArtistProfile.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ArtistProfile> artistList;

    /**
     *
     */
    public ArtisticResourceType() {
    }

    /**
     *
     * @return
     */
    public int getArtisticResourseId() {
        return artisticResourseId;
    }

    /**
     *
     * @param artisticResourseId
     */
    public void setArtisticResourseId(int artisticResourseId) {
        this.artisticResourseId = artisticResourseId;
    }

    /**
     *
     * @return
     */
    public String getArtistSite() {
        return artistSite;
    }

    /**
     *
     * @param artistSite
     */
    public void setArtistSite(String artistSite) {
        this.artistSite = artistSite;
    }

    /**
     *
     * @return
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     *
     * @param siteName
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     *
     * @return
     */
    public List<ArtistProfile> getArtistList() {
        return artistList;
    }

    /**
     *
     * @param artistList
     */
    public void setArtistList(List<ArtistProfile> artistList) {
        this.artistList = artistList;
    }

}
