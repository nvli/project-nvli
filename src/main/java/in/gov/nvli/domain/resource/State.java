package in.gov.nvli.domain.resource;

import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * State class used holds state names
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 */
@Entity
@AttributeOverride(name = "id", column
        = @Column(name = "stateId"))
@Table(name = Constants.PORTAL_TABLE_PREFIX + "state")
public class State extends BaseEntity implements Serializable {

    /**
     * The property stateName used to store Indian state name
     */
    private String stateName;

    /**
     * The getStateName method used to get Indian state name
     *
     * @return stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * The setStateName method used to set Indian state name
     *
     * @param stateName
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

}
