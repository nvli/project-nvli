package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ResourceStatus class used to maintain the status for Resource. example:Open
 * Repository status will be active, not_active etc.
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 * @see Resource
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "resource_status")
@AttributeOverride(name = "id", column
        = @Column(name = "resourceStatusId"))
@NamedQueries({
    @NamedQuery(name = "findByStatusCode", query = "From ResourceStatus resourceStatus where resourceStatus.statusCode=?"),
    @NamedQuery(name = "findByStatusName", query = "From ResourceStatus resourceStatus where resourceStatus.statusName=?")})
public class ResourceStatus extends BaseEntity implements Serializable {

    /**
     * The property statusCode used to store resource status code
     */
    @Column(name = "statusCode", nullable = false, unique = true)
    private short statusCode;
    /**
     * The property statusName used to store resource status name
     */
    @Column(name = "statusName", nullable = false, unique = true)
    private String statusName;
    /**
     * The property resources used to store List of {@link Resource}.Its a one
     * to many relationship with Resource.one ResourceStatus may have 1 or many
     * resources
     *
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceStatus", targetEntity = Resource.class)
    private List<Resource> resources = new ArrayList();
    /**
     * The property statusDesc used to store resource status description
     */
    private String statusDesc;

    /**
     * The getStatusCode method used to get resource status code
     *
     * @return statusCode
     */
    public short getStatusCode() {
        return statusCode;
    }

    /**
     * The setStatusCode method used to get resource status code
     *
     * @param statusCode
     */
    public void setStatusCode(short statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * The getStatusName method used to get resource status name
     *
     * @return statusName
     */
    public String getStatusName() {
        return statusName;
    }

    /**
     * The setStatusName method used to set resource status name
     *
     * @param statusName
     */
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    /**
     * The getStatusDesc method used to get resource status description
     *
     * @return statusDesc
     */
    public String getStatusDesc() {
        return statusDesc;
    }

    /**
     * The setStatusDesc method used to set resource status description
     *
     * @param statusDesc
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    /**
     * The getResources method used to get List of {@link Resource}.Its a one to
     * many relationship with Resource.one ResourceStatus may have 1 or many
     * resources
     *
     * @return resources
     */
    public List<Resource> getResources() {
        return resources;
    }

    /**
     * The setResources method used to set List of {@link Resource}.Its a one to
     * many relationship with Resource.one ResourceStatus may have 1 or many
     * resources
     *
     * @param resources
     */
    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

}
