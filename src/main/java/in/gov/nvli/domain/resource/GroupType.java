package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * GroupType class used to create new GroupType like classification like
 * Science, Technology etc.
 *
 * @author Suman Behara<sumanb@cdac.in>
 * @since 1
 * @version 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "group")
@AttributeOverride(name = "id", column
        = @Column(name = "groupId"))
@NamedQueries({
    @NamedQuery(name = "findByGroupId", query = "SELECT groupType FROM GroupType groupType where groupType.id=?")
})
public class GroupType extends BaseEntity implements Serializable {

    /**
     * The property groupType used to store group name
     */
    @Column(name = "groupType", nullable = false, unique = true)
    private String groupType;
    /**
     * The property resourceTypes used to store Set of resourceTypes
     * {@link ResourceType}.Its a many to many relationship with ResourceType.
     * One GroupType may contains 1 or more ResourceType.
     */

    @ManyToMany(targetEntity = ResourceType.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "group_resourcetype_relation", joinColumns
            = @JoinColumn(name = "groupId", nullable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "resourceTypeId", nullable = true, unique = false))
    private Set<ResourceType> resourceTypes = new HashSet<ResourceType>();
    /**
     * The property resources used to store List of resources
     * {@link Resource}.Its a one to many relationship with ResourceType. One
     * GroupType may contains 1 or more resources.
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "groupType", targetEntity = Resource.class)
    private List<Resource> resources = new ArrayList();
    /**
     * The property groupIcon used to store group Icon
     */
    private String groupIcon;

    /**
     * The getGroupType method used to get group name
     *
     * @return groupType
     */
    public String getGroupType() {
        return groupType;
    }

    /**
     * The setGroupType method used to set group name
     *
     * @param groupType
     */
    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    /**
     * The setResourceTypes method used to set, Set of resourceTypes
     * {@link ResourceType}.Its a many to many relationship with ResourceType.
     * One GroupType may contains 1 or more ResourceType.
     *
     * @param resourceTypes
     */
    public void setResourceTypes(Set<ResourceType> resourceTypes) {
        this.resourceTypes = resourceTypes;
    }

    /**
     * The setResourceTypes method used to get, Set of resourceTypes
     * {@link ResourceType}.Its a many to many relationship with ResourceType.
     * One GroupType may contains 1 or more ResourceType.
     *
     * @return resourceTypes
     */
    public Set<ResourceType> getResourceTypes() {
        return resourceTypes;
    }

    /**
     * The getResources method used to get List of resources
     * {@link Resource}.Its a one to many relationship with ResourceType. One
     * GroupType may contains 1 or more resources.
     *
     * @return
     */
    public List<Resource> getResources() {
        return resources;
    }

    /**
     * The setResources method used to set List of resources
     * {@link Resource}.Its a one to many relationship with ResourceType. One
     * GroupType may contains 1 or more resources.
     *
     * @param resources
     */
    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    /**
     * The getGroupIcon method used to get group Icon
     *
     * @return groupIcon
     */
    public String getGroupIcon() {
        return groupIcon;
    }

    /**
     * The setGroupIcon method used to set group Icon
     *
     * @param groupIcon
     */
    public void setGroupIcon(String groupIcon) {
        this.groupIcon = groupIcon;
    }

    /**
     * The hashCode method used to generate unique hashing code for groupType
     * Object
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        int hash = 0;
        if (this.groupType != null) {
            hash = this.groupType.length() >>> 20;
        } else {
            hash = 111 >> 20;
            hash = hash ^ hash;
        }
        return hash;
    }

    /**
     * The equals method used to check whether same groupType Object or not
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GroupType other = (GroupType) obj;
        if (!Objects.equals(this.groupType, other.groupType)) {
            return false;
        }
        return true;
    }

    /**
     * GroupTypeComparator used to sort {@link GroupType} based on GroupType
     * property in ascending order
     */
    public static Comparator<GroupType> GroupTypeComparator = new Comparator<GroupType>() {
        @Override
        public int compare(GroupType groupType1, GroupType groupType2) {
            String content1 = groupType1.getGroupType().toLowerCase();
            String content2 = groupType2.getGroupType().toLowerCase();
            return content1.compareToIgnoreCase(content2);
        }
    };
}
