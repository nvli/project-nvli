package in.gov.nvli.domain.resource;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "artist_profile")
@TableGenerator(name = "nvliSeqGenerator", allocationSize = 1, initialValue = 3112, table = Constants.PORTAL_TABLE_PREFIX + "nvli_sequence")

public class ArtistProfile implements Serializable {

  @Id
  @Column(name = "artist_id")
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "nvliSeqGenerator")
  private int id;

  @Column(name = "artist_name")
  private String artistName;

  @Column(name = "artist_profile_link")
  private String artistProfileLink;

  @Column(name = "artist_image_url")
  private String artistImageURL;

  @Column(name = "artist_description", columnDefinition = "TEXT")
  private String artistDescription;

  @Column(name = "address_line_1", nullable = true)
  private String addressLine1;

  @Column(name = "address_line_2", nullable = true)
  private String addressLine2;

  @Column(name = "city", nullable = true)
  private String city;

  @Column(name = "state", nullable = true)
  private String state;

  @Column(name = "pincode", nullable = true)
  private String pincode;

  @Column(name = "contact", nullable = true)
  private String contact;

  @Column(name = "social_contact", nullable = true)
  private String socialContact;

  @Column(name = "other", nullable = true)
  private String other;

  @Lob
  @Column(name = "photo", nullable = true, columnDefinition="mediumblob")
  private byte[] photo;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "artisticResourseId")
  private ArtisticResourceType artistResourceID;
  
    /**
     *
     */
    public ArtistProfile() {
  }

  /**
   *
   * @param artistName
   * @param artistProfileLink
   * @param artistImageURL
   * @param artistSiteID
   */
  public ArtistProfile(String artistName, String artistProfileLink, String artistImageURL, int artistSiteID) {
    super();
    this.artistName = artistName;
    this.artistProfileLink = artistProfileLink;
    this.artistImageURL = artistImageURL;
  }

    /**
     *
     * @return
     */
    public int getId() {
    return id;
  }

    /**
     *
     * @param id
     */
    public void setId(int id) {
    this.id = id;
  }

    /**
     *
     * @return
     */
    public String getArtistName() {
    return artistName;
  }

    /**
     *
     * @param artistName
     */
    public void setArtistName(String artistName) {
    this.artistName = artistName;
  }

    /**
     *
     * @return
     */
    public String getArtistProfileLink() {
    return artistProfileLink;
  }

    /**
     *
     * @param artistProfileLink
     */
    public void setArtistProfileLink(String artistProfileLink) {
    this.artistProfileLink = artistProfileLink;
  }

    /**
     *
     * @return
     */
    public String getArtistImageURL() {
    return artistImageURL;
  }

    /**
     *
     * @param artistImageURL
     */
    public void setArtistImageURL(String artistImageURL) {
    this.artistImageURL = artistImageURL;
  }

    /**
     *
     * @return
     */
    public String getArtistDescription() {
    return artistDescription;
  }

    /**
     *
     * @param artistDescription
     */
    public void setArtistDescription(String artistDescription) {
    this.artistDescription = artistDescription;
  }

    /**
     *
     * @return
     */
    public ArtisticResourceType getArtistResourceID() {
    return artistResourceID;
  }

    /**
     *
     * @param artistResourceID
     */
    public void setArtistResourceID(ArtisticResourceType artistResourceID) {
    this.artistResourceID = artistResourceID;
  }

    /**
     *
     * @return
     */
    public String getAddressLine1() {
    return addressLine1;
  }

    /**
     *
     * @param addressLine1
     */
    public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

    /**
     *
     * @return
     */
    public String getAddressLine2() {
    return addressLine2;
  }

    /**
     *
     * @param addressLine2
     */
    public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

    /**
     *
     * @return
     */
    public String getCity() {
    return city;
  }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
    this.city = city;
  }

    /**
     *
     * @return
     */
    public String getState() {
    return state;
  }

    /**
     *
     * @param state
     */
    public void setState(String state) {
    this.state = state;
  }

    /**
     *
     * @return
     */
    public String getPincode() {
    return pincode;
  }

    /**
     *
     * @param pincode
     */
    public void setPincode(String pincode) {
    this.pincode = pincode;
  }

    /**
     *
     * @return
     */
    public String getContact() {
    return contact;
  }

    /**
     *
     * @param contact
     */
    public void setContact(String contact) {
    this.contact = contact;
  }

    /**
     *
     * @return
     */
    public String getSocialContact() {
    return socialContact;
  }

    /**
     *
     * @param socialContact
     */
    public void setSocialContact(String socialContact) {
    this.socialContact = socialContact;
  }

    /**
     *
     * @return
     */
    public String getOther() {
    return other;
  }

    /**
     *
     * @param other
     */
    public void setOther(String other) {
    this.other = other;
  }

    /**
     *
     * @return
     */
    public byte[] getPhoto() {
    return photo;
  }

    /**
     *
     * @param photo
     */
    public void setPhoto(byte[] photo) {
    this.photo = photo;
  }

}
