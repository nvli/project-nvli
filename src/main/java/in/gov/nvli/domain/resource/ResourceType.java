package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

/**
 * ResourceType class used to create new,Edit,Delete resource types like Open
 * Repository, rare books, digital library etc.
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "resourcetype")
@AttributeOverrides({
    @AttributeOverride(name = "id", column
            = @Column(name = "resourceTypeId"))})
@NamedQueries({
    @NamedQuery(name = "findByResourceType", query = "SELECT resourceType FROM ResourceType resourceType where resourceType.resourceTypeCode=?")
})
public class ResourceType extends BaseEntity implements Serializable {

    /**
     * The property resourceType used to store resource type name
     */
    @Column(name = "resourceType", nullable = false, unique = true)
    private String resourceType;
    /**
     * The property thematicTypes used to store set of thematic types
     * {@link ThematicType}. Its a many to many relationship with ResourceType.
     * One ThematicType may contains 1 or more ResourceType.
     */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "resourceTypes", targetEntity = ThematicType.class)
    private Set<ThematicType> thematicTypes = new HashSet<ThematicType>();
    /**
     * The property resources used to store List of {@link Resource}.Its a many
     * to many relationship with Resource.one resourceType may have zero or many
     * resource
     */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "resourceType", targetEntity = Resource.class)
    private List<Resource> resources = new ArrayList();
    /**
     * The property groupTypes used to store List of {@link GroupType}.Its a
     * many to many relationship with group type.one resource type may have one
     * or many groups
     */
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "resourceTypes", targetEntity = GroupType.class)
    private List<GroupType> groupTypes = new ArrayList();
    /**
     * The property resourceTypeCode used to store resource type code
     */
    private String resourceTypeCode;
    /**
     * The property totalRecordCount used to store total record count of
     * particular resource type
     */
    private Long totalRecordCount;
    /**
     * The property resourceTypeCategory used to store resource type category
     */
    private String resourceTypeCategory;
    /**
     * The property resourceTypeIcon used to store resource type Icon name
     */
    private String resourceTypeIcon;
    /**
     * The property resourceTypeSearchIcon used to store resource type Search
     * Icon name
     */
    private String resourceTypeSearchIcon;
    /**
     * The property resourceTypedefaultIcon used to store resource type Search
     * Icon name
     */
    private String resourceTypedefaultIcon;

    /**
     * The property publishFlag used to store publishing the resource type.By
     * default resource type publishing flag is 1.
     */
    private boolean publishFlag;

    /**
     * The property users used to holds of {@link User} object reference.
     */
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "resourceInterest", targetEntity = User.class)
    private List<User> users;
    /**
     * The property resourceTypeInfo used to holds of {@link ResourceTypeInfo}
     * object reference.Its a one to many relationship with ResourceTypeInfo.
     * One ResourceType may contains 1 or more ResourceTypeInfo
     */
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ResourceTypeInfo.class, mappedBy = "resourceType")
    private List<ResourceTypeInfo> resourceTypeInfo = new ArrayList();

    /**
     * The getUsers method used to get of {@link User} object reference.
     *
     * @return users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * The setUsers method used to set of {@link User} object reference.
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * The getResourceTypeCode method used to get resource type code
     *
     * @return resourceTypeCode
     */
    public String getResourceTypeCode() {
        return resourceTypeCode;
    }

    /**
     * The setResourceTypeCode method used to set resource type code
     *
     * @param resourceTypeCode
     */
    public void setResourceTypeCode(String resourceTypeCode) {
        this.resourceTypeCode = resourceTypeCode;
    }

    /**
     * The getResourceType method used to get resource type name
     *
     * @return resourceType
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * The setResourceType method used to set resource type name
     *
     * @param resourceType
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * The getResources used to get List of {@link Resource}.Its a many to many
     * relationship with Resource.one resourceType may have zero or many
     * resource
     *
     * @return resources
     */
    public List<Resource> getResources() {
        return resources;
    }

    /**
     * The setResources used to set List of {@link Resource}.Its a many to many
     * relationship with Resource.one resourceType may have zero or many
     * resource
     *
     * @param resources
     */
    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    /**
     * The getThematicTypes method used to get set of thematic types
     * {@link ThematicType}. Its a many to many relationship with ResourceType.
     * One ThematicType may contains 1 or more ResourceType.
     *
     * @return thematicTypes
     */
    public Set<ThematicType> getThematicTypes() {
        return thematicTypes;
    }

    /**
     * The setThematicTypes method used to set, set of thematic types
     * {@link ThematicType}. Its a many to many relationship with ResourceType.
     * One ThematicType may contains 1 or more ResourceType.
     *
     * @param thematicTypes
     */
    public void setThematicTypes(Set<ThematicType> thematicTypes) {
        this.thematicTypes = thematicTypes;
    }

    /**
     * The getTotalRecordCount method used to get total record count of
     * particular resource type
     *
     * @return totalRecordCount
     */
    public Long getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * The setTotalRecordCount method used to set total record count of
     * particular resource type
     *
     * @param totalRecordCount
     */
    public void setTotalRecordCount(Long totalRecordCount) {
        this.totalRecordCount = totalRecordCount;
    }

    /**
     * The getGroupTypes method used to get List of {@link GroupType}.Its a many
     * to many relationship with group type.one resource type may have one or
     * many groups
     *
     * @return groupTypes
     */
    public List<GroupType> getGroupTypes() {
        return groupTypes;
    }

    /**
     * The setGroupTypes method used to set List of {@link GroupType}.Its a many
     * to many relationship with group type.one resource type may have one or
     * many groups
     *
     * @param groupTypes
     */
    public void setGroupTypes(List<GroupType> groupTypes) {
        this.groupTypes = groupTypes;
    }

    /**
     * The getResourceTypeInfo method used to get List of
     * {@link ResourceTypeInfo} object reference.Its a one to many relationship
     * with ResourceTypeInfo. One ResourceType may contains 1 or more
     * ResourceTypeInfo
     *
     * @return resourceTypeInfo
     */
    public List<ResourceTypeInfo> getResourceTypeInfo() {
        return resourceTypeInfo;
    }

    /**
     * The getResourceTypeInfo method used to set List of
     * {@link ResourceTypeInfo} object reference.Its a one to many relationship
     * with ResourceTypeInfo. One ResourceType may contains 1 or more
     * ResourceTypeInfo
     *
     * @param resourceTypeInfo
     */
    public void setResourceTypeInfo(List<ResourceTypeInfo> resourceTypeInfo) {
        this.resourceTypeInfo = resourceTypeInfo;
    }

    /**
     *
     * @return
     */
    public static Comparator<ResourceType> getResourceTypeComparator() {
        return ResourceTypeComparator;
    }

    /**
     *
     * @param ResourceTypeComparator
     */
    public static void setResourceTypeComparator(Comparator<ResourceType> ResourceTypeComparator) {
        ResourceType.ResourceTypeComparator = ResourceTypeComparator;
    }

    /**
     * The getResourceTypeCategory method used to get resource type category
     *
     * @return resourceTypeCategory
     */
    public String getResourceTypeCategory() {
        return resourceTypeCategory;
    }

    /**
     * The setResourceTypeCategory method used to set resource type category
     *
     * @param resourceTypeCategory
     */
    public void setResourceTypeCategory(String resourceTypeCategory) {
        this.resourceTypeCategory = resourceTypeCategory;
    }

    /**
     * The getResourceTypeIcon method used to get resource type Icon name
     *
     * @return resourceTypeIcon
     */
    public String getResourceTypeIcon() {
        return resourceTypeIcon;
    }

    /**
     * The setResourceTypeIcon method used to set resource type Icon name
     *
     * @param resourceTypeIcon
     */
    public void setResourceTypeIcon(String resourceTypeIcon) {
        this.resourceTypeIcon = resourceTypeIcon;
    }

    /**
     * The getResourceTypeSearchIcon method used to get resource type Search
     * Icon name
     *
     * @return resourceTypeSearchIcon
     */
    public String getResourceTypeSearchIcon() {
        return resourceTypeSearchIcon;
    }

    /**
     * The setResourceTypeSearchIcon method used to set resource type Search
     * Icon name
     *
     * @param resourceTypeSearchIcon
     */
    public void setResourceTypeSearchIcon(String resourceTypeSearchIcon) {
        this.resourceTypeSearchIcon = resourceTypeSearchIcon;
    }

    /**
     * The getResourceTypedefaultIcon method used to get resource type Search
     * Icon name
     *
     * @return resourceTypedefaultIcon
     */
    public String getResourceTypedefaultIcon() {
        return resourceTypedefaultIcon;
    }

    /**
     * The setResourceTypedefaultIcon method used to set resource type Search
     * Icon name
     *
     * @param resourceTypedefaultIcon
     */
    public void setResourceTypedefaultIcon(String resourceTypedefaultIcon) {
        this.resourceTypedefaultIcon = resourceTypedefaultIcon;
    }

    /**
     * The isPublishFlag method used to get publishing the resource type.By
     * default resource type publishing flag is 1.
     *
     * @return publishFlag
     */
    public boolean isPublishFlag() {
        return publishFlag;
    }

    /**
     * The setPublishFlag method used to set publishing the resource type.By
     * default resource type publishing flag is 1.
     *
     * @param publishFlag
     */
    public void setPublishFlag(boolean publishFlag) {
        this.publishFlag = publishFlag;
    }

    /**
     * The hashCode method used to generate unique hashing code for ResourceType
     * Object
     *
     * @return hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.resourceType);
        hash = hash >>> 10;
        return hash;
    }

    /**
     * The equals method used to check equality whether same ResourceType Object
     * or not
     *
     * @param obj
     * @return boolean true or false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceType other = (ResourceType) obj;
        if (!Objects.equals(this.resourceType, other.resourceType)) {
            return false;
        }
        return true;
    }

    /**
     * ResourceTypeComparator used to sort {@link ResourceType} based on
     * resourceType property in ascending order
     */
    public static Comparator<ResourceType> ResourceTypeComparator = new Comparator<ResourceType>() {
        @Override
        public int compare(ResourceType resourceType1, ResourceType resourceType2) {
            String content1 = resourceType1.getResourceType().toLowerCase();
            String content2 = resourceType2.getResourceType().toLowerCase();
            return content1.compareToIgnoreCase(content2);
        }
    };

}
