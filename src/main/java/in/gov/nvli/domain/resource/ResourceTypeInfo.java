package in.gov.nvli.domain.resource;

import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 * ResourceTypeInfo class holds the title, description of Resource Type for
 * different language
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 * @see ResourceType
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "resourcetype_info")
@AttributeOverrides({
    @AttributeOverride(name = "id", column
            = @Column(name = "resourceTypeInfoId"))})
public class ResourceTypeInfo extends BaseEntity implements Serializable {

    /**
     * The property language used to holds {@link UDCLanguage} object reference
     */
    @ManyToOne
    @JoinColumn(name = "language_id")
    private UDCLanguage language;
    /**
     * The property description used to store description of resource type
     */
    @Type(type = "org.hibernate.type.TextType")
    private String description;
    /**
     * The property title used to store title of resource type
     */
    private String title;
    /**
     * The property resourceType used to holds {@link ResourceType} object
     * reference
     */
    @ManyToOne
    @JoinColumn(name = "resourceTypeId", nullable = false)
    private ResourceType resourceType;

    /**
     * default constructor
     */
    public ResourceTypeInfo() {

    }

    /**
     *
     * @param description
     * @param title
     */
    public ResourceTypeInfo(String description, String title) {
        this.description = description;
        this.title = title;
    }

    /**
     * The getLanguage method used to get {@link UDCLanguage} object reference
     *
     * @return
     */
    public UDCLanguage getLanguage() {
        return language;
    }

    /**
     * The setLanguage method used to set {@link UDCLanguage} object reference
     *
     * @param language
     */
    public void setLanguage(UDCLanguage language) {
        this.language = language;
    }

    /**
     * The getDescription method used to get description of resource type
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * The setDescription method used to set description of resource type
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * The getTitle method used to get title of resource type . Here we are
     * allowing to add title in multiple language
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * The setTitle method used to get title of resource type.Here we are
     * allowing to add title in multiple language
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * The getResourceType method used to get {@link ResourceType} object
     * reference
     *
     * @return resourceType
     */
    public ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * The setResourceType method used to set {@link ResourceType} object
     * reference
     *
     * @param resourceType
     */
    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * The hashCode method used to generate unique hashing code for based on
     * description and title Object
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.description);
        hash = 31 * hash + Objects.hashCode(this.title);
        return hash;
    }

    /**
     * The equals method used to check equality whether same ResourceTypeInfo
     * Object (description+title) generated hashing Object or not
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceTypeInfo other = (ResourceTypeInfo) obj;

        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }

        return true;
    }

    /**
     * The toString method used to print ResourceTypeInfo object
     *
     * @return String
     */
    @Override
    public String toString() {
        return "ResourceTypeInfo{" + "description=" + description + ", title=" + title + '}';
    }

}
