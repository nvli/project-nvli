package in.gov.nvli.domain.resource;

import java.util.ArrayList;
import java.util.List;

/**
 * NewsFeeds class holds the list of NewFeed {@link NewsFeed}
 *
 * @author Sujata Aher
 * @version 1
 * @since 1
 * @see NewsFeed
 */
public class NewsFeeds {

    /**
     * The property newsFeeds holds the list of {@link NewsFeed} object
     * reference
     */
    private List<NewsFeed> newsFeeds;

    /**
     * The getNewsFeeds method used to get list of {@link NewsFeed} object
     *
     * @return newsFeeds
     */
    public List<NewsFeed> getNewsFeeds() {
        if (newsFeeds == null) {
            newsFeeds = new ArrayList<>();
        }
        return newsFeeds;
    }

    /**
     * The setNewsFeeds method used to set list of {@link NewsFeed} object
     *
     * @param newsFeeds
     */
    public void setNewsFeeds(List<NewsFeed> newsFeeds) {
        this.newsFeeds = newsFeeds;
    }

}
