package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.generic.BaseEntity;
import in.gov.nvli.util.Constants;
import in.gov.nvli.webserviceclient.oar.HarRecordMetadataType;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class holds the Open repository Metadata standard information.
 *
 * @author Sujata Aher
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "open_repo_standard")
public class HarRecordMetadata extends BaseEntity implements Serializable {

    @Enumerated(EnumType.STRING)
    private HarRecordMetadataType metadataType;

    private boolean metadataSupport;

    private boolean metadataEnabled;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "resourceId")
    private OpenRepository openRepository;

    /**
     *
     * @return
     */
    public HarRecordMetadataType getMetadataType() {
        return metadataType;
    }

    /**
     *
     * @param metadataType
     */
    public void setMetadataType(HarRecordMetadataType metadataType) {
        this.metadataType = metadataType;
    }

    /**
     *
     * @return
     */
    public boolean isMetadataSupport() {
        return metadataSupport;
    }

    /**
     *
     * @param metadataSupport
     */
    public void setMetadataSupport(boolean metadataSupport) {
        this.metadataSupport = metadataSupport;
    }

    /**
     *
     * @return
     */
    public boolean isMetadataEnabled() {
        return metadataEnabled;
    }

    /**
     *
     * @param metadataEnabled
     */
    public void setMetadataEnabled(boolean metadataEnabled) {
        this.metadataEnabled = metadataEnabled;
    }

    /**
     *
     * @return
     */
    public OpenRepository getOpenRepository() {
        return openRepository;
    }

    /**
     *
     * @param openRepository
     */
    public void setOpenRepository(OpenRepository openRepository) {
        this.openRepository = openRepository;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.metadataType);
        hash = hash >>> 10;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HarRecordMetadata other = (HarRecordMetadata) obj;
        if (!Objects.equals(this.metadataType, other.metadataType)) {
            return false;
        }
        return true;
    }

}
