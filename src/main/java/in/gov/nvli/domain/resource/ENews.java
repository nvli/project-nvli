package in.gov.nvli.domain.resource;

import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 * ENews class used create new e-News Resource.
 *
 * @author Sujata Aher
 * @author Suman Behara<sumanb@cdac.in>
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "enews")
@PrimaryKeyJoinColumn(name = "resourceId")
public class ENews extends Resource implements Serializable {

    /**
     * The property newsUrl used to store news URL
     */
    private String newsUrl;
    /**
     * The property newsFeed used to store news Feed
     */
    @Lob
    private String newsFeed;
    /**
     * The property newsPaperType used to store news Paper Type like National or
     * regional paper
     */
    private String newsPaperType;
    /**
     * The property lastCrawlingTime used to store last crawling datetime
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastCrawlingTime;
    /**
     * The property newsFeedUrl used to store news feed URL
     */
    private String newsFeedUrl;

    /**
     * The getNewsUrl method used to get news URL
     *
     * @return newsUrl
     */
    public String getNewsUrl() {
        return newsUrl;
    }

    /**
     * The setNewsUrl method used to set news URL
     *
     * @param newsUrl
     */
    public void setNewsUrl(String newsUrl) {
        this.newsUrl = newsUrl;
    }

    /**
     * The getNewsFeed method used to get news Feed
     *
     * @return newsFeed
     */
    public String getNewsFeed() {
        return newsFeed;
    }

    /**
     * The setNewsFeed method used to set news Feed
     *
     * @param newsFeed
     */
    public void setNewsFeed(String newsFeed) {
        this.newsFeed = newsFeed;
    }

    /**
     * The getNewsPaperType method used to get news Paper Type like National or
     * regional paper
     *
     * @return newsPaperType
     */
    public String getNewsPaperType() {
        return newsPaperType;
    }

    /**
     * The getNewsPaperType method used to set news Paper Type like National or
     * regional paper
     *
     * @param newsPaperType
     */
    public void setNewsPaperType(String newsPaperType) {
        this.newsPaperType = newsPaperType;
    }

    /**
     * The getLastCrawlingTime method used to get last crawling datetime
     *
     * @return lastCrawlingTime
     */
    public Date getLastCrawlingTime() {
        return lastCrawlingTime;
    }

    /**
     * The getLastCrawlingTime method used to set last crawling datetime
     *
     * @param lastCrawlingTime
     */
    public void setLastCrawlingTime(Date lastCrawlingTime) {
        this.lastCrawlingTime = lastCrawlingTime;
    }

    /**
     * The getNewsFeedUrl method used to get news feed URL
     *
     * @return newsFeedUrl
     */
    public String getNewsFeedUrl() {
        return newsFeedUrl;
    }

    /**
     * The getNewsFeedUrl method used to set news feed URL
     *
     * @param newsFeedUrl
     */
    public void setNewsFeedUrl(String newsFeedUrl) {
        this.newsFeedUrl = newsFeedUrl;
    }

}
