package in.gov.nvli.domain.resource;

import com.fasterxml.jackson.annotation.JsonIgnore;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.generic.AuditableEntity;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.util.Constants;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 * Resource class used to create new Resource,Edit exist Resource and Delete
 * exist Resource.
 *
 * @author Sujata Aher
 * @author Suman Behara <sumanb@cdac.in>
 * @version 1
 * @since 1
 */
@Entity
@Table(name = Constants.PORTAL_TABLE_PREFIX + "resource")
@NamedQueries({
    @NamedQuery(name = "findByResourceCode", query = "From Resource resource where resource.resourceCode=?"),
    @NamedQuery(name = "findResourceByHarvestURL", query = "From Resource resource where resource.harvestUrl=?"),
    @NamedQuery(name = "findRcByNameWithRType", query = "From Resource resource where resource.resourceType=?"),
    @NamedQuery(name = "findRCByGroupAndResourceType", query = "From Resource resource where resource.groupType=? and resource.resourceType=?")})
@Inheritance(strategy = InheritanceType.JOINED)
@AttributeOverride(name = "id", column
        = @Column(name = "resourceId"))
public class Resource extends AuditableEntity implements Serializable {

    /**
     * The name of the property resourceName used to store resource name
     */
    @Column(name = "resourceName", nullable = false)
    private String resourceName;
    /**
     * The name of the property description used to store resource description
     */
    @Lob
    private String description;
    /**
     * The name of the property resourceType used to holds the
     * {@link ResourceType} object reference
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "resourceTypeId")
    private ResourceType resourceType;
    /**
     * The property contentTypes used to store set of content types
     * {@link ContentType}. Its a many to many relationship with resource. One
     * Resource may contains zero or more content type.
     */
    @JsonIgnore
    @ManyToMany(targetEntity = ContentType.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "content_resource_relation", joinColumns
            = @JoinColumn(name = "resourceId", nullable = true, updatable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "contentTypeId", nullable = true, updatable = true, unique = false))
    private Set<ContentType> contentTypes;

    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;
    
    private String contactPersonName;
    /**
     * The property designation used to holds designation of resource contact
     * person
     */
    private String designation;
    /**
     * The property address used to store address of the resource
     */
   // private String address;
    /**
     * The property email used to store email of resource holding agency or
     * email of resource contact person
     */
    private String email;
    /**
     * The property phoneNumber used to store Phone number of resource holding
     * agency or phone number of resource contact person
     */
    private String phoneNumber;
    /**
     * The property crowdSourcing used to store boolean value for crowd source
     * required or not for respective resource
     */
    @Column(name = "crowdSourcing", columnDefinition = "bit(1)")
    private boolean crowdSourcing;
    /**
     * The property publish used to store boolean value for publishing required
     * or not for respective resource. By default while creating resource it
     * value is 0.
     */
    @Column(name = "publish", columnDefinition = "bit(1)")
    private boolean publish;
    /**
     * The property active used to store boolean value. Used to save record processing start and stop status.
     * By default while creating resource it's value is 0.
     */
    @Column(name = "active", columnDefinition = "bit(1)")
    private boolean active;
    /**
     * The property thematicTypes used to store set of thematic types
     * {@link ThematicType}. Its a many to many relationship with resource. One
     * Resource may contains zero or more thematic type.
     */
    @JsonIgnore
    @ManyToMany(targetEntity = ThematicType.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "thematic_resource_relation", joinColumns
            = @JoinColumn(name = "resourceId", nullable = true, updatable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "thematicId", nullable = true, updatable = true, unique = false))
    private Set<ThematicType> thematicTypes = new HashSet<>();
    /**
     * The property groupType used to holds {@link GroupType} object reference.
     * each resource is associated with one group.Its a mandatory property while
     * creating resource
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "groupTypeId")
    private GroupType groupType;
    /**
     * The property languages used to store set of {@link UDCLanguage}.Its a
     * many to many relationship with resource. One Resource may contains one or
     * more languages.By default language is English
     */
    @JsonIgnore
    @ManyToMany(targetEntity = UDCLanguage.class, cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = Constants.PORTAL_TABLE_PREFIX + "language_resource_relation", joinColumns
            = @JoinColumn(name = "resourceId", nullable = true, updatable = true, unique = false), inverseJoinColumns
            = @JoinColumn(name = "language_id", nullable = true, updatable = true, unique = false))
    private Set<UDCLanguage> languages;
   
    /**
     * The property resourceStatus used to holds {@link ResourceStatus} object
     * reference. it used for maintain resource status whether resource is
     * active or not active, while creating resource error_in_save, resource
     * published or not etc.
     */
    @ManyToOne
    @JoinColumn(name = "resourceStatus", referencedColumnName = "statusCode")
    private ResourceStatus resourceStatus;
    
    /**
     * The property resourceCode used to store resource Code of resource
     * Organization. resource code is unique and mandatory
     */
    @Column(nullable = false, unique = true)
    private String resourceCode;
    /**
     * The property resourceIcon used to store resource Icon name.
     */
    private String resourceIcon;
    /**
     * The property recordCount used to store resource total record count.
     */
    private Long recordCount;
    /**
     * The property users used to holds list of {@link User} object reference.
     * It is used identify how many resources are assigned for particular user
     */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "assignedResources")
    private List<User> users;
    /**
     * The property crowdSourceArticles used to store set of crowd source
     * articles {@link CrowdSourceArticle}. It is a bi-directional many-to-one
     * association to CrowdSourceArticle
     */
    @JsonIgnore
    @OneToMany(mappedBy = "articleCategory", fetch = FetchType.LAZY)
    private Set<CrowdSourceArticle> crowdSourceArticles;

    /**
     * The getResourceName method used to get resource name
     *
     * @return resourceName
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     * The setResourceName method used to set resource name
     *
     * @param resourceName
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     * The getDescription method used to get resource description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * The setDescription method used to get resource description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * The getResourceType method used to get resource type the
     * {@link ResourceType} object reference
     *
     * @return resourceType
     */
    public ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * The setResourceType method used to set resource type the
     * {@link ResourceType} object reference
     *
     * @param resourceType
     */
    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * The getContentTypes method used to store get set of the content types
     * {@link ContentType}. Its a many to many relationship with resource. One
     * Resource may contains zero or more content type.
     *
     * @return contentTypes
     */
    public Set<ContentType> getContentTypes() {
        return contentTypes;
    }

    /**
     * The setContentTypes method used to store set set of the content types
     * {@link ContentType}. Its a many to many relationship with resource. One
     * Resource may contains zero or more content type.
     *
     * @param contentTypes
     */
    public void setContentTypes(Set<ContentType> contentTypes) {
        this.contentTypes = contentTypes;
    }

    /**
     * The getOrganization method used to get the organization
     * {@link Organization} object reference
     *
     * @return
     */
    public Organization getOrganization() {
        return organization;
    }

    /**
     * The getOrganization method used to set the organization
     * {@link Organization} object reference
     *
     * @param organization
     */
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    /**
     * The getContactPersonName method used to get resource contact person name
     *
     * @return contactPersonName
     */
    public String getContactPersonName() {
        return contactPersonName;
    }

    /**
     * The setContactPersonName method used to set resource contact person name
     *
     * @param contactPersonName
     */
    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    /**
     * The getDesignation method used to get resource contact person designation
     *
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * The setDesignation method used to set resource contact person designation
     *
     * @param designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

     /**
     * The getEmail method used to get resource holding agency or email of
     * resource contact person
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * The setEmail method used to set resource holding agency email or email of
     * resource contact person
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * The getPhoneNumber method used to get resource holding agency phone
     * number or phone number of resource contact person
     *
     * @return phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * The setPhoneNumber method used to set resource holding agency phone
     * number or phone number of resource contact person
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * The isCrowdSourcing method used to get boolean value for crowd source
     * required or not for respective resource
     *
     * @return crowdSourcing
     */
    public boolean isCrowdSourcing() {
        return crowdSourcing;
    }

    /**
     * The setCrowdSourcing method used to set boolean value for crowd source
     * required or not for respective resource
     *
     * @param crowdSourcing
     */
    public void setCrowdSourcing(boolean crowdSourcing) {
        this.crowdSourcing = crowdSourcing;
    }

    /**
     * The isPublish method used to get boolean value for publishing required or
     * not for respective resource
     *
     * @return publish
     */
    public boolean isPublish() {
        return publish;
    }

    /**
     * The setPublish method used to set boolean value for publishing required
     * or not for respective resource
     *
     * @param publish
     */
    public void setPublish(boolean publish) {
        this.publish = publish;
    }

    /**
     * The isActive method used to get boolean value for activated or not for
     * respective resource
     *
     * @return active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * The setActive method used to set boolean value for activated or not for
     * respective resource
     *
     * @param active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * The getThematicTypes method used to get, set of thematic types
     * {@link ThematicType}. Its a many to many relationship with resource. One
     * Resource may contains zero or more thematic type.
     *
     * @return thematicTypes
     */
    public Set<ThematicType> getThematicTypes() {
        return thematicTypes;
    }

    /**
     * The setThematicTypes method used to set, set of thematic types
     * {@link ThematicType}. Its a many to many relationship with resource. One
     * Resource may contains zero or more thematic type.
     *
     * @param thematicTypes
     */
    public void setThematicTypes(Set<ThematicType> thematicTypes) {
        this.thematicTypes = thematicTypes;
    }

    /**
     * The getGroupType method used to get the groupType {@link GroupType}
     * object reference
     *
     * @return groupType
     */
    public GroupType getGroupType() {
        return groupType;
    }

    /**
     * The setGroupType method used to set the groupType {@link GroupType}
     * object reference
     *
     * @param groupType
     */
    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    /**
     * The getLanguages method used to store get of {@link UDCLanguage}.Its a
     * many to many relationship with resource. One Resource may contains one or
     * more languages.By default language is English
     *
     * @return languages
     */
    public Set<UDCLanguage> getLanguages() {
        return languages;
    }

    /**
     * The setLanguages method used to store set of {@link UDCLanguage}.Its a
     * many to many relationship with resource. One Resource may contains one or
     * more languages.By default language is English
     *
     * @param languages
     */
    public void setLanguages(Set<UDCLanguage> languages) {
        this.languages = languages;
    }

    /**
     * The getResourceStatus method used to get {@link ResourceStatus} object
     * reference. it used for maintain resource status whether resource is
     * active or not active, while creating resource resource status are like
     * error_in_save, resource published or not etc.
     *
     * @return resourceStatus
     */
    public ResourceStatus getResourceStatus() {
        return resourceStatus;
    }

    /**
     * The setResourceStatus method used to set {@link ResourceStatus} object
     * reference. it used for maintain resource status whether resource is
     * active or not active,example: while creating resource resource status are
     * like error_in_save, resource published or not etc.
     *
     * @param resourceStatus
     */
    public void setResourceStatus(ResourceStatus resourceStatus) {
        this.resourceStatus = resourceStatus;
    }

    /**
     * The getResourceCode method used to get resource Code of resource
     * Organization. While creating resource , resource code is unique and
     * mandatory
     *
     * @return resourceCode
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * The setResourceCode method used to set resource Code of resource
     * Organization. While creating resource , resource code is unique and
     * mandatory
     *
     * @param resourceCode
     */
    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    /**
     * The method getResourceIcon used to get resource Icon name.
     *
     * @return resourceIcon
     */
    public String getResourceIcon() {
        return resourceIcon;
    }

    /**
     * The method getResourceIcon used to set resource Icon name.
     *
     * @param resourceIcon
     */
    public void setResourceIcon(String resourceIcon) {
        this.resourceIcon = resourceIcon;
    }

    /**
     * The method getRecordCount used to get resource total record count.
     *
     * @return recordCount
     */
    public Long getRecordCount() {
        return recordCount;
    }

    /**
     * The method setRecordCount used to set resource total record count.
     *
     * @param recordCount
     */
    public void setRecordCount(Long recordCount) {
        this.recordCount = recordCount;
    }

    /**
     * The getUsers method used to get list of {@link User} object reference. It
     * is used identify how many resources are assigned for particular user
     *
     * @return users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * The setUsers method used to set list of {@link User} object reference. It
     * is used identify how many resources are assigned for particular user
     *
     * @param users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * The getCrowdSourceArticles method used to get set of crowd source
     * articles {@link CrowdSourceArticle}. It is a bi-directional many-to-one
     * association to CrowdSourceArticle
     *
     * @return
     */
    public Set<CrowdSourceArticle> getCrowdSourceArticles() {
        return crowdSourceArticles;
    }

    /**
     * The getCrowdSourceArticles method used to set, set of crowd source
     * articles {@link CrowdSourceArticle}. It is a bi-directional many-to-one
     * association to CrowdSourceArticle
     *
     * @param crowdSourceArticles
     */
    public void setCrowdSourceArticles(Set<CrowdSourceArticle> crowdSourceArticles) {
        this.crowdSourceArticles = crowdSourceArticles;
    }

}
