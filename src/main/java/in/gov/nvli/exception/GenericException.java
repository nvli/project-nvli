/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.exception;

/**
 * This class generates Generic Exception Message
 *
 * @author Suman Behara<sumanb@cdac.in>
 * @version 1
 * @since 1
 */
public class GenericException extends RuntimeException {

    /**
     *
     * @param exceptionMessage
     */
    public GenericException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
