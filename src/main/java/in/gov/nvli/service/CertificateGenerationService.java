package in.gov.nvli.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.itextpdf.text.pdf.BaseFont;
import in.gov.nvli.dao.rewards.BadgeDetailsDAO;
import in.gov.nvli.dao.rewards.CertificateDetailsDAO;
import in.gov.nvli.domain.rewards.BadgeDetails;
import in.gov.nvli.domain.rewards.CertificateDetails;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.util.Constants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.InputSource;

/**
 *
 * @author Vivek Bugale <bvivek@cdac.in>
 */
@Service
public class CertificateGenerationService {

    private final static Logger LOG = Logger.getLogger(CertificateGenerationService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private BadgeDetailsDAO badgeDetailsDAO;

    @Autowired
    private CertificateDetailsDAO certificateDetailsDAO;

    public boolean issueCertificate(Long userId, Long batchId) {
        System.out.println("==========inside issueCertificate ==========");
        if (userId != 0 && userId != null && batchId != 0 && batchId != null) {
            //get user obj
            User user = userService.getUserById(userId);

            //get badge obj
            BadgeDetails badge = badgeDetailsDAO.get(batchId);

            //Date
            //timestamp, to save in certificate details table 
            Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("currentTimestamp ::" + currentTimestamp);

            //Full Name            
            String userFullName = getUserFullName(user);
            System.out.println(" userFullName ::" + userFullName);

            //make an entry to to certificate details table, certificate not generated yet.
            CertificateDetails certificateDetails = new CertificateDetails(currentTimestamp, user, badge, userFullName);
            System.out.println("before certificateDetails :" + certificateDetails);
            certificateDetailsDAO.saveOrUpdate(certificateDetails);
            System.out.println("after certificateDetails :" + certificateDetails);

            //barcode = 12 digit certificate id
            Long barcode = certificateDetails.getCertificateId();
            System.out.println("barcode ::" + barcode);

            // generate barcode
            boolean generateBarcode = generateBarcode(barcode, badge);

            // generate certificate                       
            if (generateBarcode) {
                return generateCertificate(userFullName, barcode, currentTimestamp, badge) ? updateCertificateStatus(certificateDetails) : false;
            }

        }
        return false;
    }

    private String getUserFullName(User user) {
        System.out.println("==========inside getUserFullName==========");

        return Optional.ofNullable(user.getMiddleName())
                .orElse(user.getMiddleName()).trim().isEmpty()
                ? user.getFirstName().trim() + " " + user.getLastName().trim()
                : user.getFirstName().trim() + " " + user.getMiddleName().trim() + " " + user.getLastName().trim();
    }

    private boolean generateBarcode(Long barcode, BadgeDetails badge) {
        System.out.println("==========inside  generateBarcode==========");

        //create barcode directory
        String path = createDirectory("barcode", badge);
        if (path != null) {
            return zxing(barcode, path);
        }
        return false;
    }

    private String createDirectory(String directory, BadgeDetails badge) {
        System.out.println("==========inside createDirectory ==========");

        try {
            System.out.println("----directory---- ::" + directory);
            String path = Constants.RewardsPath.REWARDS_DIR_PATH + File.separator;
            if ("barcode".equals(directory)) {
                path = path.concat(Constants.RewardsPath.BARCODES_DIR_PATH);
            } else {
                path = path.concat(Constants.RewardsPath.CERTIFICATES_DIR_PATH);
            }
            path += File.separator + badge.getBadgeCode().toLowerCase();

            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
                System.out.println("Directory created at ::" + file.getPath());
            } else {
                System.out.println("Directory already present at " + file.getPath());
            }
            return path;
        } catch (Exception ex) {
            LOG.error("Error while creating rewards directory ::", ex);
            ex.printStackTrace();
        }
        return null;
    }

    private boolean zxing(Long barcode, String path) {
        System.out.println("==========inside  zxing==========");

        try {
            // this is the contents that we want to encode
            String contents = String.valueOf(barcode);

            // change the height and width as per your requirement
            int width = 500;
            int height = 100;

            // image format is png
            String imageFormat = "png";

            Code128Writer code128Writer = new Code128Writer();

            BitMatrix bitMatrix = code128Writer.encode(contents, BarcodeFormat.CODE_128, width, height);

            MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(path.concat(File.separator).concat(contents).concat(".png")));
            return true;
        } catch (WriterException ex) {
            LOG.error("Error in zxing WriterException :: ", ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            LOG.error("Error in zxing FileNotFoundException :: ", ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            LOG.error("Error in zxing IOException :: ", ex);
            ex.printStackTrace();
        }
        return false;
    }

    private boolean generateCertificate(String userFullName, Long barcode, Timestamp timestamp, BadgeDetails badge) {
        System.out.println("==========inside  generateCertificate==========");

        try {
            //date, to show on certificate
            String formatedDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date(timestamp.getTime()));
            System.out.println("formatedDate ::" + formatedDate);

            //read certificate html
            StringBuilder buf = new StringBuilder();
            String fileContent = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("reward-certificates/certificates.html").toURI())));
            System.out.println("fileContent ::" + fileContent);

            //replace the required fields
            fileContent = fileContent.replace("{{name}}", userFullName);
            fileContent = fileContent.replace("{{date}}", formatedDate);
            fileContent = fileContent.replace("{{barcode}}", String.valueOf(barcode));
            fileContent = fileContent.replace("{{certificateImg}}", getCertificateSrc(badge.getBadgeCode().toLowerCase(), String.valueOf(barcode)));
            fileContent = fileContent.replace("{{barcodeImg}}", getBarcodeSrc(badge.getBadgeCode().toLowerCase(), String.valueOf(barcode)));
            fileContent = fileContent.replace("{{signImg}}", getSignSrc());
            buf.append(fileContent);

            //Itext 
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            ITextRenderer renderer = new ITextRenderer();
            renderer.getFontResolver().addFont("http://localhost:80/certificates/Italianno-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Document doc = builder.parse(new InputSource(new StringReader(buf.toString())));
            renderer.setDocument(doc, null);
            //create barcode directory
            String path = createDirectory("certificate", badge);
            if (path != null) {
                String outputFile = path.concat(File.separator).concat(String.valueOf(barcode)).concat(".pdf");
                try (OutputStream os = new FileOutputStream(outputFile)) {
                    renderer.layout();
                    renderer.createPDF(os);
                    return true;
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return false;
    }

    private String getCertificateSrc(String certificate, String barcode) {
        System.out.println("==========inside getCertificateSrc ==========");
//        return "http://localhost:81/certificates/01-diamond.png";
        return "http://localhost:80/certificates/" + certificate + ".png";
    }

    private String getBarcodeSrc(String certificate, String barcode) {
        System.out.println("==========inside getBarcodeSrc ==========");
        return "http://localhost:80/certificates/barcode_zxing3.png";
    }

    private String getSignSrc() {
        System.out.println("==========inside getSignSrc ==========");

        return "http://localhost:80/certificates/sign.png";
    }

    private boolean updateCertificateStatus(CertificateDetails certificateDetails) {
        try {
            certificateDetails.setGenerated(true);
            certificateDetailsDAO.saveOrUpdate(certificateDetails);
            return true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return false;
    }

    public CertificateDetails getLastCertificate(User userobj) {
        return certificateDetailsDAO.getLastCertificate(userobj);
    }
}
