package in.gov.nvli.service;

import in.gov.nvli.dao.user.UserThemeDAO;
import in.gov.nvli.domain.user.UserTheme;
import in.gov.nvli.util.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import static org.terracotta.modules.ehcache.store.TerracottaClusteredInstanceFactory.LOGGER;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Service
public class UserThemeService {

    @Autowired
    private UserThemeDAO userThemeDAO;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * List List of themes available
     *
     * @return List
     */
    public List<UserTheme> listThemes() {
        return userThemeDAO.list();
    }

    public List<UserTheme> listThemesOrderByAsc() {
        return userThemeDAO.getThemeList();
    }

    public List<HashMap<String, Object>> listProjectedThemes() {
        return userThemeDAO.getThemes();
    }

    /**
     * Creates new theme
     *
     * @param userTheme
     * @return
     */
    public boolean createNewTheme(UserTheme userTheme) {
        return userThemeDAO.createNew(userTheme);
    }

    public UserTheme findById(long id) {
        return userThemeDAO.get(id);
    }

    public UserTheme getDefaultTheme() {
        return this.findById(1L);
    }

    public JSONObject uploadTheme(String themeFileLocation, MultipartFile uploadfile) {
        File themeFileDir = new File(themeFileLocation);
        if (!themeFileDir.exists()) {
            themeFileDir.mkdir();
        }

        String[] fileNameAndExtension = uploadfile.getOriginalFilename().split(Constants.REG_EXP_FOR_EXTENSION);
        String fileExtension = fileNameAndExtension[1].toLowerCase();
        String fileName;
        String extractedThemeFileDirectoryName = fileNameAndExtension[0];
        fileName = fileNameAndExtension[0] + "." + fileExtension;
        String filePath = themeFileDir.getAbsolutePath() + File.separator + fileName;
        File file = new File(filePath);

        if (file.exists()) {
            // Handle properly of file exist.. prompt user for possible loss existing file
            FileUtils.deleteQuietly(file);
        }
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();

        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return this.extractTheme(file, themeFileDir.getAbsolutePath(), extractedThemeFileDirectoryName);
    }

    public JSONObject extractTheme(File file, String destinationPath, String extractedThemeFileDirectoryName) {
        try {
            ZipFile zipFile = new ZipFile(file);
            zipFile.extractAll(destinationPath);
        } catch (ZipException ex) {
            Logger.getLogger(UserThemeService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (file.exists()) {
                // Handle properly of file exist.. prompt user for possible loss existing file
                FileUtils.deleteQuietly(file);
            }
            // Read Manifest
            return this.readManifest(destinationPath + File.separator + extractedThemeFileDirectoryName + File.separator + "manifest.json");
        }
    }

    public JSONObject readManifest(String filePath) {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            Object obj = parser.parse(new FileReader(filePath));
            jsonObject = (JSONObject) obj;
            return jsonObject;
        } catch (IOException ex) {
            Logger.getLogger(UserThemeService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(UserThemeService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return jsonObject;
    }

    public boolean isThemeCodeAvailable(String themeCode) {

        UserTheme themeName = userThemeDAO.getThemeByThemeCode(themeCode);
        if (null != themeName) {
            return false;
        } else {
            return true;
        }
    }

    public void saveThemeFileInfo(JSONObject object) throws java.text.ParseException {
        UserTheme theme = new UserTheme();
        JSONArray bgImages = (JSONArray) object.get("background_image");
        theme.setBgImageLocation((String) bgImages.get(0));
        theme.setCustom(false);
        JSONArray headerImages = (JSONArray) object.get("header_image");
        theme.setHeaderImageLocation((String) headerImages.get(0));
        theme.setThemeCode((String) object.get("theme_code"));
        theme.setThemeDescription((String) object.get("theme_description"));
        theme.setThemeName((String) object.get("theme_name"));
        theme.setThemeDirectory((String) object.get("theme_directory"));
        if (object.get("repeatable") != null) {
            theme.setThemeRepeatable((Boolean) object.get("repeatable"));
        } else {
            theme.setThemeRepeatable(Boolean.FALSE);
        }
        String dateInString = (String) object.get("theme_date");
        if (dateInString != null && !dateInString.isEmpty()) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date parsed = format.parse(dateInString);
            java.sql.Date sql = new java.sql.Date(parsed.getTime());
            theme.setThemeActivationDate(sql);
            theme.setThemeType("global");
        }
        userThemeDAO.createNew(theme);
    }

    public UserTheme getThemeById(Long themeId) {
        return userThemeDAO.get(themeId);
    }

    public void deleteTheme(UserTheme object) {
        userThemeDAO.delete(object);
    }

    public void updateThemeTypeInfo(Long themeId, String themeType, Date themeDate, String themeNmae, String themeDesc, Boolean themeRepeat) {
        UserTheme theme = userThemeDAO.get(themeId);
        theme.setThemeType(themeType);
        theme.setThemeActivationDate(themeDate);
        theme.setThemeName(themeNmae);
        theme.setThemeDescription(themeDesc);
        theme.setThemeRepeatable(themeRepeat);
        userThemeDAO.update(theme);
    }

    public UserTheme fetchThemeByDate(Date themeDate) {
        return userThemeDAO.getThemeByDate(themeDate);
    }

    public List<UserTheme> listPersonalThemes() {
        return userThemeDAO.getAllPersonalThemes();
    }

//    @Scheduled(cron = "0 0/1 * * * ?")
    public void scheduleGlobalThemeLoad() {
        this.pushSheduledTheme(Constants.DEFAULT_THEME_BROKER_URL, "Hello");
    }

    public void pushSheduledTheme(String url, String message) {
        System.out.println("Pushing Current Theme");
        this.simpMessagingTemplate.convertAndSend(url, message);
    }

    public UserTheme getThemeByThemeCode(String themeCode) {
        return userThemeDAO.getThemeByThemeCode(themeCode);
    }

    public List<UserTheme> listGlobalThemes() {
        return userThemeDAO.getAllGlobalThemes();
    }

    public JSONArray getGlobalThemes() {
        List<UserTheme> themes = this.listGlobalThemes();
        JSONArray array = new JSONArray();
        for (UserTheme theme : themes) {
            JSONObject obj = new JSONObject();
            obj.put("id", theme.getId());
            obj.put("title", theme.getThemeName());
            obj.put("start", theme.getThemeActivationDate());
            obj.put("className", "widget-theme-thumb-" + theme.getThemeCode());
            array.add(obj);
        }
        return array;
    }

    public int replaceTheme(String themeFileLocation, MultipartFile replacefile, String originalfileDir, UserTheme userTheme) throws IOException, java.text.ParseException {

        FileUtils.deleteDirectory(new File(themeFileLocation + "/" + originalfileDir));
        JSONObject object = this.uploadTheme(themeFileLocation, replacefile);
        JSONArray bgImages = (JSONArray) object.get("background_image");
        userTheme.setBgImageLocation((String) bgImages.get(0));
        userTheme.setCustom(false);
        JSONArray headerImages = (JSONArray) object.get("header_image");
        userTheme.setHeaderImageLocation((String) headerImages.get(0));
        userTheme.setThemeDescription((String) object.get("theme_description"));
        userTheme.setThemeName((String) object.get("theme_name"));
        if (object.get("repeatable") != null) {
            userTheme.setThemeRepeatable((Boolean) object.get("repeatable"));
        } else {
            userTheme.setThemeRepeatable(Boolean.FALSE);
        }
        userTheme.setThemeDirectory((String) object.get("theme_directory"));
        String dateInString = (String) object.get("theme_date");
        if (dateInString != null && !dateInString.isEmpty()) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date parsed = format.parse(dateInString);
            java.sql.Date sql = new java.sql.Date(parsed.getTime());
            userTheme.setThemeActivationDate(sql);
            userTheme.setThemeType("global");
        }
        userThemeDAO.update(userTheme);
        return 1;
    }

    public void addThemeFileInfoByDate(JSONObject object, Date themeDate) {
        UserTheme theme = new UserTheme();
        JSONArray bgImages = (JSONArray) object.get("background_image");
        theme.setBgImageLocation((String) bgImages.get(0));
        theme.setCustom(false);
        JSONArray headerImages = (JSONArray) object.get("header_image");
        theme.setHeaderImageLocation((String) headerImages.get(0));
        theme.setThemeCode((String) object.get("theme_code"));
        theme.setThemeDescription((String) object.get("theme_description"));
        theme.setThemeName((String) object.get("theme_name"));
        if (object.get("repeatable") != null) {
            theme.setThemeRepeatable((Boolean) object.get("repeatable"));
        } else {
            theme.setThemeRepeatable(Boolean.FALSE);
        }
        theme.setThemeDirectory((String) object.get("theme_directory"));
        theme.setThemeActivationDate(themeDate);
        theme.setThemeType("global");
        userThemeDAO.createNew(theme);
    }

    @Scheduled(cron = "0 0 4 * * *")
    public void themeReapeatableUpdate() {
        List<UserTheme> repatableThemeList = userThemeDAO.getAllRepeatableTheme();
        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        java.sql.Date today = new java.sql.Date(currentDate.getTime());
        if (!repatableThemeList.isEmpty()) {
            for (UserTheme theme : repatableThemeList) {
                Date themeDate = theme.getThemeActivationDate();
                if (themeDate.before(today)) {
                    calendar.setTime(themeDate);
                    calendar.add(Calendar.YEAR, 1);
                    java.util.Date newDate = calendar.getTime();
                    String dd = new SimpleDateFormat("yyyy-MM-dd").format(newDate);
                    if (dd != null && !dd.isEmpty()) {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        java.util.Date parsed;
                        try {
                            parsed = format.parse(dd);
                            java.sql.Date sql = new java.sql.Date(parsed.getTime());
                            theme.setThemeActivationDate(sql);
                            userThemeDAO.update(theme);
                        } catch (java.text.ParseException ex) {
                            Logger.getLogger(UserThemeService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    // set the date to same date of next year
                }
            }
        }
    }

    public List<UserTheme> fetchThemesByDate(Date themeDate) {
        return userThemeDAO.getThemesByDate(themeDate);
    }
}
