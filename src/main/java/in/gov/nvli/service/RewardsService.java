package in.gov.nvli.service;

import in.gov.nvli.dao.rewards.BadgeDetailsDAO;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.nvli.dao.rewards.CertificateDetailsDAO;
import in.gov.nvli.dao.rewards.LevelDetailsDAO;
import in.gov.nvli.dao.rewards.RewardsPointDetailsDAO;
import in.gov.nvli.dao.rewards.StarsDetailsDAO;
import in.gov.nvli.dao.rewards.UserRewardsSummaryDAO;
import in.gov.nvli.domain.dublincore.DublinCoreMetadata;
import in.gov.nvli.domain.marc21.CollectionType;
import in.gov.nvli.domain.rewards.BadgeDetails;
import in.gov.nvli.domain.rewards.CertificateDetails;
import in.gov.nvli.domain.rewards.LevelDetails;
import in.gov.nvli.domain.rewards.RewardsPointDetails;
import in.gov.nvli.domain.rewards.StarsDetails;
import in.gov.nvli.domain.rewards.UserRewardsSummary;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.mongodb.domain.ActivityLog;
import in.gov.nvli.mongodb.domain.CrowdSource;
import in.gov.nvli.mongodb.domain.CrowdSourceAndTagging;
import in.gov.nvli.mongodb.domain.Tags;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.util.CompareMetadataVersionsUtils;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.StatementsSimilarityUtils;
import in.gov.nvli.util.StringComparator;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 * @author Gulafsha
 * @author Ritesh Malviya
 */
@Service
public class RewardsService {

    private final static Logger LOG = Logger.getLogger(RewardsService.class);

    @Autowired
    RewardsPointDetailsDAO rewardsPointDetailsDAO;

    @Autowired
    LevelDetailsDAO levelDetailsDAO;

    @Autowired
    UserRewardsSummaryDAO userRewardsSummaryDAO;

    @Autowired
    UserService userService;

    @Autowired
    UserActivityService userActivityService;

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    BadgeDetailsDAO badgeDetailsDAO;

    @Autowired
    StarsDetailsDAO starsDetailsDAO;

    @Autowired
    CertificateGenerationService certificateGenerationService;

    @Autowired
    CertificateDetailsDAO certificateDetailsDAO;

    public List<RewardsPointDetails> getRewardPointDetails() {
        List<RewardsPointDetails> pointDetails = rewardsPointDetailsDAO.getRewardPointDetails();
        Collections.sort(pointDetails, (o1, o2) -> o1.getLowerBoundAccuracy() - o2.getLowerBoundAccuracy());
        return pointDetails;
    }

    public List<LevelDetails> getRewardsLevelList() {
        List<LevelDetails> rewardLevelList = levelDetailsDAO.fetchLevelDetails();
        return rewardLevelList;
    }

    public UserRewardsSummary fetchUserRewardSummeryByUser(User userObj) {
        return userRewardsSummaryDAO.getUserRewardSummeryByUser(userObj);
    }

    private boolean updateEditPoints(int noOfEdits, User userObj, int finalEditPoints) {
        int approvedPoints = 0;
        LevelDetails currentLevel = null;
        UserRewardsSummary urs = userRewardsSummaryDAO.getUserRewardSummeryByUser(userObj);
        if (urs != null) {
            int editPoints = urs.getEditPoints();
            finalEditPoints = (finalEditPoints * noOfEdits) + editPoints;
            currentLevel = levelDetailsDAO.findCurrentLevel(finalEditPoints, urs.getApprovedPoints());
            urs.setCurrentLevel(currentLevel);
            urs.setEditPoints(finalEditPoints);
            userRewardsSummaryDAO.saveOrUpdate(urs);

        } else {
            long currentlevelId = 1;
            finalEditPoints = finalEditPoints * noOfEdits;
            UserRewardsSummary userSummaryObj = new UserRewardsSummary();
            userSummaryObj.setUserId(userObj);
            userSummaryObj.setEditPoints(finalEditPoints);
            userSummaryObj.setApprovedPoints(approvedPoints);
            currentLevel = levelDetailsDAO.findInitialLevelBylevelId(currentlevelId);
            userSummaryObj.setCurrentLevel(currentLevel);
            userRewardsSummaryDAO.createNew(userSummaryObj);
            certificateGenerationService.issueCertificate(userObj.getId(), currentLevel.getBadgeAwarded().getBadgeId());
        }
        if (currentLevel != null && currentLevel.getBadgeAwarded().getBadgeWeight() > 0) {
            genrateCertificate(userObj, currentLevel);
            return true;
        }
        return false;
    }

    private boolean saveLogForEditActivity(CrowdSourceAndTagging crowdSourceAndTagging, String recordIdentifier, User user, HttpServletRequest request, String activityType) {
        if (user != null) {
            userActivityService.saveActivityLog(recordIdentifier, user.getId(), activityType, crowdSourceAndTagging, request, user.getUsername(), null, Constants.FilterActivityLogs.CrowdSourceActivities, null);
            return true;
        }
        return false;
    }

    @Async(value = "taskExecutor")
    public void rewardPointsForEditMetadata(String modifiedMetadataJsonString, HttpServletRequest request, String recordIdentifier, Long crowdSourceId, int editNumber, User user) {
        int noOfEdits = 0;
        try {
            RewardsPointDetails rewardsPointDetails = rewardsPointDetailsDAO.fetchPointsDetailsByActivity(Constants.PointsDetails.EDIT_METADATA);
            List<CrowdSource> crowdSourceList = new ArrayList<>();
            JSONObject modifiedMetadataJson = new JSONObject(modifiedMetadataJsonString);
            Iterator<String> modifiedMetadataJsonIterator = modifiedMetadataJson.keys();
            while (modifiedMetadataJsonIterator.hasNext()) {
                String modifiedFieldName = modifiedMetadataJsonIterator.next();
                JSONArray modifiedFieldValues = modifiedMetadataJson.getJSONArray(modifiedFieldName);
                noOfEdits = noOfEdits + modifiedFieldValues.length();
                for (int index = 0; index < modifiedFieldValues.length(); index++) {
                    JSONObject values = modifiedFieldValues.getJSONObject(index);
                    crowdSourceList.add(new CrowdSource(modifiedFieldName, values.optString("oldValue"), values.optString("newValue"), rewardsPointDetails.getEarnedPoints()));
                }
            }
            CrowdSourceAndTagging crowdSourceAndTagging = new CrowdSourceAndTagging(crowdSourceId, editNumber, crowdSourceList, new Timestamp(new Date().getTime()));
            //save log in mongoDb for edit metadata rewards points
            if (saveLogForEditActivity(crowdSourceAndTagging, recordIdentifier, user, request, Constants.CrowdSourceActivity.EDIT_METADATA)) {
                // update metadata points in mysql db for user
                if (!updateEditPoints(noOfEdits, user, rewardsPointDetails.getEarnedPoints())) {
                    //do something if fail
                }
            } else {
                //do something if fail
            }
        } catch (JSONException ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public boolean updateApprovedMetadataPoints(int approvedPoint, User userObj) {
        System.out.println("upodatsjkdfjskd");
        System.out.println("approvedPoint " + approvedPoint);
        UserRewardsSummary urs = userRewardsSummaryDAO.getUserRewardSummeryByUser(userObj);
        LevelDetails currentLevel = null;
        if (urs != null) {
            approvedPoint += urs.getApprovedPoints();
            urs.setApprovedPoints(approvedPoint);
            currentLevel = levelDetailsDAO.findCurrentLevel(urs.getEditPoints(), approvedPoint);
            urs.setCurrentLevel(currentLevel);
            userRewardsSummaryDAO.saveOrUpdate(urs);
        }
        if (currentLevel != null && currentLevel.getBadgeAwarded().getBadgeWeight() > 0) {
            genrateCertificate(userObj, currentLevel);
            return true;
        }
        return false;
    }

    public List<LevelDetails> getRewardsLevels() {
        return levelDetailsDAO.fetchAllLevels();
    }

    public void findAccuracy(Long crowdSourceId, Map<String, List<String>> map, HttpServletRequest request) {
        List<ActivityLog> activityLogserviceList = activityLogService.getEditMetadataActivityLogByCrowdSourceId(crowdSourceId);
        for (ActivityLog activityLog : activityLogserviceList) {
            Long userId = activityLog.getUserId();
            CrowdSourceAndTagging crowdSourceAndTagging = (CrowdSourceAndTagging) activityLog.getActivity();
            List<CrowdSource> crowdSourcesList = crowdSourceAndTagging.getCrowdSource();
            int point = 0;
            for (CrowdSource crowdSource : crowdSourcesList) {
                double accuracy = 0;
                for (String value : map.get(crowdSource.getFieldName())) {
                    double percentage = StatementsSimilarityUtils.calculatePercSim(value, crowdSource.getNewValue());
                    if (accuracy < percentage) {
                        accuracy = percentage;
                    }
                }
                final double acc = accuracy * 100;
                List<RewardsPointDetails> pointDetailsList = getRewardPointDetails();
                List<RewardsPointDetails> list = pointDetailsList.stream()
                        .filter(pointDetail
                                -> pointDetail.getActivityCode().equals(Constants.PointsDetails.APPROVED_METADATA)
                                && pointDetail.getLowerBoundAccuracy() < acc
                                && pointDetail.getUpperBoundAccuracy() >= acc
                                && pointDetail.isIsCurrent())
                        .collect(Collectors.toList());
                if (!list.isEmpty()) {
                    point += list.get(0).getEarnedPoints();
                }
            }
            if (point > 0) {
                CrowdSource crowdSourceobj = new CrowdSource();
                crowdSourceobj.setPoints(point);
                User user = userService.getUserById(userId);
                CrowdSourceAndTagging crowdSourceAndTagging1 = new CrowdSourceAndTagging();
                crowdSourceAndTagging1.setCrowdSourceId(crowdSourceId);
                crowdSourceAndTagging1.setCrowdSource(Arrays.asList(crowdSourceobj));
                //my sql save data
                if (saveLogForEditActivity(crowdSourceAndTagging1, activityLog.getRecordIdentifier(), user, request, Constants.CrowdSourceActivity.APPROVED_METADATA)) {
                    if (!updateApprovedMetadataPoints(point, user)) {
                    }
                }
            }
        }
    }

    public List<LevelDetails> getLevels() {
        List<LevelDetails> LevelList = levelDetailsDAO.fetchAllLevels();
        Collections.sort(LevelList, (o1, o2) -> (new StringComparator().compare(o1.getLevelName(), o2.getLevelName())));
        return LevelList;
    }

    public LevelDetails getLevelById(Long LevelId) {
        return levelDetailsDAO.get(LevelId);
    }

    public RewardsPointDetails getPointsDetailsById(Long pointsId) {
        return rewardsPointDetailsDAO.get(pointsId);
    }

    public List<BadgeDetails> getAllBadges() {
        return badgeDetailsDAO.list();
    }

    public List<StarsDetails> getAllStars() {
        return starsDetailsDAO.list();
    }

    public BadgeDetails getBadgeByBadgeCode(String badgeAwarded) {
        return badgeDetailsDAO.getBadgeByBadgeCode(badgeAwarded);
    }

    public StarsDetails getStarsByCode(String starsAwarded) {
        return starsDetailsDAO.getStarsByCode(starsAwarded);
    }

    public boolean updateAndCreate(LevelDetails levelToCreate, LevelDetails levelToUpdate) {
        if (levelDetailsDAO.createNew(levelToCreate)) {
            levelDetailsDAO.saveOrUpdate(levelToUpdate);
            return true;
        }
        return false;
    }

    public boolean updateAndCreatePoints(RewardsPointDetails pointDetailsToCreate, RewardsPointDetails pointsDetailsToUpdate) {
        if (rewardsPointDetailsDAO.createNew(pointDetailsToCreate)) {
            rewardsPointDetailsDAO.saveOrUpdate(pointsDetailsToUpdate);
            return true;
        }
        return false;
    }

    public LevelDetails getLastLevel() {
        return levelDetailsDAO.getLastLevel();
    }

    public boolean createLevel(LevelDetails levelToCreate) {
        return levelDetailsDAO.createNew(levelToCreate);
    }

//    @Async(value = "taskExecutor")
    public void rewardsPointsForTags(String modifiedJson, Long crowdSourceId, String recordIdentifier, HttpServletRequest req, String tagType, User user) {
        try {
            if (modifiedJson.length() != 0) {
                int noOfEdits = 0;
                String cTag = null;
                List<Tags> tagList = new ArrayList<>();
                RewardsPointDetails rewardsPointDetails = new RewardsPointDetails();
                if (tagType.equalsIgnoreCase("udc")) {
                    rewardsPointDetails = rewardsPointDetailsDAO.fetchPointsDetailsByActivity(Constants.PointsDetails.ADD_UDC_TAG);
                    org.json.JSONObject editJSOnObject = new org.json.JSONObject(modifiedJson);
                    Iterator<String> result = editJSOnObject.keys();
                    while (result.hasNext()) {
                        noOfEdits++;
                        String udcNotation = result.next();
                        tagList.add(new Tags(udcNotation, rewardsPointDetails.getEarnedPoints()));
                    }
                    cTag = Constants.CrowdSourceActivity.ADD_UDC_TAG;

                } else if (tagType.equalsIgnoreCase("custom")) {
                    rewardsPointDetails = rewardsPointDetailsDAO.fetchPointsDetailsByActivity(Constants.PointsDetails.ADD_TAG);
                    org.json.JSONObject editJSOnObject = new org.json.JSONObject(modifiedJson);
                    Iterator<String> result = editJSOnObject.keys();
                    while (result.hasNext()) {
                        noOfEdits++;
                        String tagCode = result.next();
                        String[] split = tagCode.split("_");
                        Long languageId = Long.parseLong(split[1]);
                        tagList.add(new Tags(languageId, (String) editJSOnObject.get(tagCode), rewardsPointDetails.getEarnedPoints()));
                    }
                    cTag = Constants.CrowdSourceActivity.ADD_CUSTOM_TAG;
                }
                CrowdSourceAndTagging crowdSourceAndTagging = new CrowdSourceAndTagging(tagType, crowdSourceId, tagList, new Timestamp(new Date().getTime()));
                //save log in mongoDb for tag adding rewards points
                if (saveLogForEditActivity(crowdSourceAndTagging, recordIdentifier, user, req, cTag)) {
                    //update edit points for adding custom tag or udc tag
                    if (!updateEditPoints(noOfEdits, user, rewardsPointDetails.getEarnedPoints())) {
                        //do something if fail
                    }
                } else {
                    //do something if fail
                }
            }
        } catch (JSONException ex) {
            java.util.logging.Logger.getLogger(RewardsService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Async(value = "taskExecutor")
    public void rewardsPointsForApprovedMetadata(String metadataType, String orginalMetadataJSON, String approvedMetadataJSON, Long crowdSourceId, HttpServletRequest request) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, List<String>> map = new HashMap<>();
            if (metadataType.equals(Constants.MetadataStandardType.DUBLIN_CORE)) {
                //for dc metadata
                DublinCoreMetadata originalMetadata = (DublinCoreMetadata) mapper.readValue(orginalMetadataJSON, DublinCoreMetadata.class);
                DublinCoreMetadata approvedMetaData = (DublinCoreMetadata) mapper.readValue(approvedMetadataJSON, DublinCoreMetadata.class);
                map = CompareMetadataVersionsUtils.findDCMetadataModifications(originalMetadata, approvedMetaData);
            } else {
                //marc 21
                CollectionType originalMetadata = (CollectionType) mapper.readValue(orginalMetadataJSON, CollectionType.class);
                CollectionType approvedMetaData = (CollectionType) mapper.readValue(approvedMetadataJSON, CollectionType.class);
                map = CompareMetadataVersionsUtils.findMarc21MetadataModifications(originalMetadata, approvedMetaData);
            }
            if (!map.isEmpty()) {
                findAccuracy(crowdSourceId, map, request);
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(RewardsService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(RewardsService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void genrateCertificate(User userObj, LevelDetails curreLevelDetails) {
        CertificateDetails certificateDetails = certificateGenerationService.getLastCertificate(userObj);
        if (certificateDetails != null) {
            if (!certificateDetails.getBadge().getBadgeId().equals(curreLevelDetails.getBadgeAwarded().getBadgeId())) {
                //generate certificate
                int oldWeightage = certificateDetails.getBadge().getBadgeWeight();
                int newWeightage = curreLevelDetails.getBadgeAwarded().getBadgeWeight();
                while (oldWeightage != newWeightage) {
                    BadgeDetails badgeDetails = badgeDetailsDAO.findBadgeByWeight(newWeightage);
                    certificateGenerationService.issueCertificate(userObj.getId(), badgeDetails.getBadgeId());
                    newWeightage = newWeightage - 1;
                }
            } else {
                //certificate already genrated
            }
        } else {
            int newWeightage = curreLevelDetails.getBadgeAwarded().getBadgeWeight();
            while (newWeightage >= 1) {
                BadgeDetails badgeDetails = badgeDetailsDAO.findBadgeByWeight(newWeightage);
                certificateGenerationService.issueCertificate(userObj.getId(), badgeDetails.getBadgeId());
                newWeightage = newWeightage - 1;
            }
        }
    }

    public Long getCertificateIdByUserIdAndByBadgeId(Long userId, BadgeDetails badge) {
        return certificateDetailsDAO.findCertificateIdByUserIdAndBadgeId(userId, badge);
    }

    @Async(value = "taskExecutor")
    public void rewardsPointsForApprovedUDCTags(String originalTags, String approvedTags, String crowdSourceId, HttpServletRequest request) {
        try {
            List<String> originalTagsList = null, approvedTagsList = null;
            List<String> finalUDCTagList = new ArrayList<>();
            if (!originalTags.isEmpty()) {
                originalTagsList = Arrays.asList(originalTags.split("&\\|&"));
                for (String string : originalTagsList) {
                }
            }
            if (!approvedTags.isEmpty()) {
                approvedTagsList = Arrays.asList(approvedTags.split("&\\|&"));
                for (String string : approvedTagsList) {
                }
            }
            if (originalTagsList == null && approvedTagsList != null) {
                finalUDCTagList.addAll(approvedTagsList);
            } else if (originalTagsList != null && approvedTagsList != null) {
                //modifiedMetadataMap.put(field.getName(), CompareMetadataVersionsUtils.findModifications(originalTagsList, approvedTagsList));
                finalUDCTagList = CompareMetadataVersionsUtils.findModifications(originalTagsList, approvedTagsList);
            }
            if (!finalUDCTagList.isEmpty()) {
                approvedUdcTagPoints(crowdSourceId, finalUDCTagList, request);
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(RewardsService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void approvedUdcTagPoints(String crowdSourceId, List<String> approvedTagList, HttpServletRequest request) {
        List<ActivityLog> activityLogserviceList = activityLogService.getUDCActivityLogByCrowdSourceId(Long.parseLong(crowdSourceId));
        for (ActivityLog activityLog : activityLogserviceList) {
            Long userId = activityLog.getUserId();
            CrowdSourceAndTagging crowdSourceAndTagging = (CrowdSourceAndTagging) activityLog.getActivity();
            List<Tags> tagList = crowdSourceAndTagging.getTags();
            RewardsPointDetails rewardsPointDetails = rewardsPointDetailsDAO.fetchPointsDetailsByActivity(Constants.PointsDetails.APPROVED_UDC_TAG);
            int points = 0;
            for (Tags tags : tagList) {
                if (approvedTagList.contains(tags.getUdcNotation())) {
                    points += rewardsPointDetails.getEarnedPoints();
                }
            }
            System.out.println("pints" + points);
            if (points > 0) {
                Tags tags = new Tags();
                tags.setPoints(points);
                User user = userService.getUserById(userId);
                CrowdSourceAndTagging crowdSourceAndTagging1 = new CrowdSourceAndTagging();
                crowdSourceAndTagging1.setCrowdSourceId(Long.parseLong(crowdSourceId));
                crowdSourceAndTagging1.setTags(Arrays.asList(tags));
                //my sql save data
                if (saveLogForEditActivity(crowdSourceAndTagging1, activityLog.getRecordIdentifier(), user, request, Constants.CrowdSourceActivity.APPROVED_UDC_TAG)) {
                    if (!updateApprovedMetadataPoints(points, user)) {
                    }
                }
            }
        }
    }
}
