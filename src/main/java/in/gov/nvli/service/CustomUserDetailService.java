/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * Custom User Detail Service
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Service("userDetailsService")
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String principal) throws UsernameNotFoundException {
        User user=null;
        if (userDAO.isValidEmail(principal)) {
         user = userDAO.findByEmail(principal);

        } else {
           user = userDAO.findUserByUsername(principal);
        }
        if (user == null) {
            throw new UsernameNotFoundException("No user found with username: " + principal);
        }
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
       
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
                accountNonLocked, getAuthorities(user.getRoles()));

    }

    private Collection<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(roles));
        return authList;
    }

    private List<String> getRoles(Set<Role> roles) {
        List<String> roleCodes = new ArrayList<String>();
        for (Role role : roles) {
            roleCodes.add(role.getCode());
        }
        return roleCodes;
    }

    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

}
