/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.artist.ArtistProfileDao;
import in.gov.nvli.domain.resource.ArtistProfile;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj Singh Tanwar
 */
@Service
public class ArtistService {

    @Autowired
    ArtistProfileDao artistDao;

    public List<ArtistProfile> listAllArtist() {
        List<ArtistProfile> artistList;
        //ArtistProfile artistDummy=new ArtistProfile("Gajraj", "www.gajraj.com", "www.gajraj.com/gajraj.jpg", "1");
        //  artistList.add(artistDummy);
        artistList = artistDao.list();
        return artistList;
    }

    public List<ArtistProfile> getAllArtistForGallary(int galleryId) {
        List<ArtistProfile> artistList;
        //ArtistProfile artistDummy=new ArtistProfile("Gajraj", "www.gajraj.com", "www.gajraj.com/gajraj.jpg", "1");
        //  artistList.add(artistDummy);
        artistList = artistDao.getAllArtistForGallary(galleryId);
        return artistList;
    }

    public void updateArtist(ArtistProfile artistProfile) {
        artistDao.saveOrUpdate(artistProfile);
    }
    /*
     get artist profile by ID
     */

    public ArtistProfile getArtist(int artistId) {
        return artistDao.get(artistId);
    }

    /**
     *
     * @param ap
     * @return
     */
    public boolean createArtist(ArtistProfile ap) {
        return artistDao.createNew(ap);
    }
}
