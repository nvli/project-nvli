/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.user.UserNotificationSubscriptionDAO;
import in.gov.nvli.domain.user.NotificationSubscription;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Service
public class NotificationSubscriptionService {

    private static final Logger LOG = Logger.getLogger(NotificationSubscription.class);

    @Autowired
    private UserNotificationSubscriptionDAO userNotificationSubscriptionDAO;

    public boolean checkNvliShareLinkStatus(long userId, String status) {
        NotificationSubscription notifObject = userNotificationSubscriptionDAO.GetNotificationSettingsByUserId(userId);
        if ((status.equals("WithEmail")) && (notifObject.getNvliShareLinkWithEmail() == true)) {
            return true;
        } else if ((status.equals("WithApp")) && (notifObject.getNvliShareLinkWithApp() == true)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean updateNvliLinkShareWithEmail(boolean NvliLinkShareWithEmailStatus, long userId) {
        return userNotificationSubscriptionDAO.updateNvliLinkShareWithEmail(NvliLinkShareWithEmailStatus, userId);
    }
}
