package in.gov.nvli.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * class for sending mail and its utilities.
 */
@Service
public class SendMailService {

    private static final Logger LOG = Logger.getLogger(SendMailService.class);    

    public static boolean checkIfLinkExpired(String mailSendTime) {
        try {
            Date currentTime = new GregorianCalendar().getTime();
            DateFormat dateFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
            Date sendTime = dateFormatter.parse(mailSendTime);
            //in milliseconds
            long diff = currentTime.getTime() - sendTime.getTime();
//            long diffSeconds = diff / 1000 % 60;
//            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
//            long diffDays = diff / (24 * 60 * 60 * 1000);
            if (diffHours < 24) {
                return false;
            }
        } catch (ParseException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return true;
    }
}
