package in.gov.nvli.service;

import in.gov.nvli.dao.widget.CalenderEventDAO;
import in.gov.nvli.dao.widget.WidgetDAO;
import in.gov.nvli.domain.user.UserTheme;
import in.gov.nvli.domain.widget.CalenderEvent;
import in.gov.nvli.domain.widget.Widget;
import in.gov.nvli.util.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import static org.terracotta.modules.ehcache.store.TerracottaClusteredInstanceFactory.LOGGER;

/**
 *
 * @author Sanjay Rabidas <rabidassanjay@gmail.com>
 */
@Service
public class WidgetService {

    @Autowired
    private WidgetDAO widgetDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private UserThemeService themeService;

    @Autowired
    private CalenderEventDAO calEventDao;

    public List<Widget> listWidgets() {
        return widgetDAO.list();
    }

    public JSONObject uploadWidget(String widgetLocation, String fileID, MultipartFile uploadfile) {
        File widgetDir = new File(widgetLocation);
        if (!widgetDir.exists()) {
            widgetDir.mkdir();
        }

        String[] fileNameAndExtension = uploadfile.getOriginalFilename().split(Constants.REG_EXP_FOR_EXTENSION);
        String fileExtension = fileNameAndExtension[1].toLowerCase();
        String fileName;
        String extractedWidgetDirectoryName = fileNameAndExtension[0];
        fileName = fileNameAndExtension[0] + "." + fileExtension;
        String filePath = widgetDir.getAbsolutePath() + File.separator + fileName;
        File file = new File(filePath);

        if (file.exists()) {
            // Handle properly of file exist.. prompt user for possible loss existing file
            FileUtils.deleteQuietly(file);
        }
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();

        } catch (Exception e) {
            LOGGER.error("Exception in uploadIcon", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return this.extractWidget(file, widgetDir.getAbsolutePath(), extractedWidgetDirectoryName);
    }

    public JSONObject extractWidget(File file, String destinationPath, String extractedWidgetDirectoryName) {
        try {
            ZipFile zipFile = new ZipFile(file);
            zipFile.extractAll(destinationPath);
        } catch (ZipException ex) {
            Logger.getLogger(WidgetService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (file.exists()) {
                // Handle properly of file exist.. prompt user for possible loss existing file
                FileUtils.deleteQuietly(file);
            }
            // Read Manifest
            return this.readManifest(destinationPath + File.separator + extractedWidgetDirectoryName + File.separator + "manifest.json");
        }
    }

    public JSONObject readManifest(String filePath) {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            Object obj = parser.parse(new FileReader(filePath));
            jsonObject = (JSONObject) obj;
            return jsonObject;
        } catch (IOException ex) {
            Logger.getLogger(WidgetService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(WidgetService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return jsonObject;
    }

    public boolean isWidgetNameAvailable(String widgetName) {

        Widget widget = widgetDAO.getWidgetByName(widgetName);
        if (null != widget) {
            return false;
        } else {
            return true;
        }
    }

    public void saveWidgetInfo(JSONObject object) {
        Widget widget = new Widget();
        widget.setCreatedBy(userService.getCurrentUser().getId());
        widget.setWidgetName((String) object.get("widget_name"));
        widget.setWidgetDescription((String) object.get("widget_description"));
        Date createdOn = new Date();
        widget.setCreatedOn(createdOn);
        widget.setWidgetVersion((String) object.get("widget_version"));
        widget.setWidgetLogo((String) object.get("widget_logo"));
        widget.setWidgetJs((String) object.get("widget_js"));
        widget.setWidgetStyle((String) object.get("widget_style"));
        widget.setWidgetTemplate((String) object.get("widget_template"));
        JSONObject author = (JSONObject) object.get("widget_author");
        widget.setWidgetAuthorName((String) author.get("name"));
        widget.setWidgetAuthorEmail((String) author.get("email"));
        widgetDAO.createNew(widget);
    }

    public void addCalenderEvent(CalenderEvent calEvent) {
        calEventDao.createNew(calEvent);
    }
}
