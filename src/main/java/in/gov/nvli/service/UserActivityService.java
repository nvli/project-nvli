package in.gov.nvli.service;

import in.gov.nvli.custom.exceptions.DuplicateEntryException;
import in.gov.nvli.custom.user.RequestAttributes;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.dao.user.SocialSiteDAO;
import in.gov.nvli.dao.user.UserActivityDAO;
import in.gov.nvli.dao.user.UserActivityLogDAO;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.dao.user.UserInvitationDAO;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.SocialSite;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserActivityLog;
import in.gov.nvli.domain.user.UserInvitation;
import in.gov.nvli.mongodb.domain.Activity;
import in.gov.nvli.mongodb.domain.ActivityLog;
import in.gov.nvli.mongodb.domain.CrowdSourceAndTagging;
import in.gov.nvli.mongodb.domain.ReviewAndRating;
import in.gov.nvli.mongodb.domain.UserActivity;
import in.gov.nvli.mongodb.domain.UserAddedList;
import in.gov.nvli.mongodb.domain.UserEBook;
import in.gov.nvli.mongodb.service.ActivityLogService;
import in.gov.nvli.mongodb.service.ReviewAndRatingService;
import in.gov.nvli.mongodb.service.UserAddedListService;
import in.gov.nvli.mongodb.service.UserEBookService;
import in.gov.nvli.rabbitmq.RabbitmqProducer;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Constants.UserActivityConstant;
import in.gov.nvli.util.Helper;
import in.gov.nvli.webserviceclient.mit.Record;
import in.gov.nvli.webserviceclient.user.UserSearchResultActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import static org.springframework.data.domain.Sort.Direction.DESC;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * This has methods related to user activity logging management.
 *
 * @author Bhumika,Ruturaj
 * @author Vivek Bugale
 * @since 1
 * @version 1
 */
@Service
public class UserActivityService {

    private static final Logger LOG = Logger.getLogger(UserActivityService.class);
    @Autowired
    private UserActivityLogDAO activityLogDao;
    @Autowired
    private UserActivityDAO activityDao;
    /**
     * the name of this property userService is used to hold UserService object
     */
    @Autowired
    private UserService userService;
    @Autowired
    private UserInvitationDAO userInvitationDAO;
    /**
     * the name of this property crowdSourcingService is used to hold
     * CrowdSourcingService object
     */
    @Autowired
    private CrowdSourcingService crowdSourcingService;

    @Autowired
    private UserDAO userDAO;

    private Set<String> activityTypes;

    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;

    @Value("${mongoDB.maxAnonymousUserId}")
    long max;

    @Value("${mongoDB.minAnonymousUserId}")
    long min;

    @Autowired
    private RewardsService rewardsService;
    /**
     * the name of this property activityLogService is used to hold
     * ActivityLogService object
     */
    @Autowired
    private ActivityLogService activityLogService;

    @Autowired
    private SocialSiteDAO socialSiteDAO;
    @Autowired
    private IResourceTypeDAO resourceTypeDAO;

    @Autowired
    MongoOperations mongoOperations;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    UserEBookService userEBookService;
    /**
     * This is used to communicate with user review repository.
     */
    @Autowired
    ReviewAndRatingService userReviewAndRatingService;

    /**
     * This is used to communicate with Redis.
     */
    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private RetryTemplate retryTemplate;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    UserAddedListService userAddedListService;

    /**
     * Service to log a user's activity
     *
     * @param uniqueIdentifier the unique identifier of nvli objects
     *
     * @param activityId the id of activity performed by user which has to be
     * logged.key is stored in db and value of message in property file
     * @param userObj the user who performed the activity
     * @param affectedUserId user affected by the action/activity
     * @param requestObj client's ipaddress and userAgent
     * @return activitylog object
     */
    public UserActivityLog saveUserActivityLog(int activityId, String uniqueIdentifier, User userObj, Long affectedUserId, RequestAttributes requestObj) {
        try {
            User affectedUserObj = userService.getUserById(affectedUserId);
            //commenting below check because have to save unlogged user's search too
            if (userObj != null) {
                in.gov.nvli.domain.user.UserActivity activityObj = activityDao.getActivityObjectById(activityId);
                UserActivityLog activityLog = new UserActivityLog();
                activityLog.setUserActivity(activityObj);
                activityLog.setUniqueRecordIdentifier(uniqueIdentifier);
                activityLog.setUser(userObj);
                activityLog.setActivityTime(new Date());
                activityLog.setAffectedUser(affectedUserObj);
                activityLog.setUserAgentInfo(requestObj.getUserAgent());
                activityLog.setIpAddress(requestObj.getIpAddress());

                return activityLogDao.saveUserActivityLog(activityLog);
            } else {
                LOG.error("User " + userObj + " not in session");
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Saves logs of activities like login,signup,change password,activate
     * user,deactivate user,delete user,update profile and edit user details
     * performed by logged in user
     *
     * @param userId
     * @param updatedOrSharedWithUserId
     * @param activityName
     * @param requestObj
     * @param username
     * @param updatedOrSharedWithUserFullName
     * @return
     */
    public boolean saveOtherActivityOfUser(Long userId, Long updatedOrSharedWithUserId, String activityName, HttpServletRequest request, String username, String updatedOrSharedWithUserFullName) {
        Activity activity = new Activity();
        try {
            activity.setActivityTs(new Date());
            if (updatedOrSharedWithUserId != null) {
                activity.setUpdatedOrSharedWithUserId(updatedOrSharedWithUserId);
            }
            if (updatedOrSharedWithUserFullName != null) {
                activity.setUpdatedOrSharedWithUserFullName(updatedOrSharedWithUserFullName);
            }
            saveActivityLog(null, userId, activityName, activity, request, username, null, null, null);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    /**
     * Service to view a user's activity log
     *
     * @param ctx to retreive messages from resource bundle
     * @param userId the userid(unique) used to retreive the user who performed
     * the activity
     * @param pageNumber
     * @param pageWindow
     * @param hasFilter
     * @return list of user logs
     */
    public List<String> getActivityLogOfUserToView(ApplicationContext ctx, long userId, int pageNumber, int pageWindow, String filter) {
        List<String> jSONArray = new ArrayList<>();
        JSONObject activityJSONObj;
        List<ActivityLog> logList = null;
        Object key = (userId + "" + filter).toString();
        try {
            if (!filter.equals(Constants.FilterActivityLogs.AllActivities)) {
                logList = activityLogService.getFilteredActivityLogListOfUser(pageNumber, pageWindow, userId, activityTypes);
            } else {
                logList = activityLogService.getAllLogsOfUser(userId, pageNumber, pageWindow);
            }
            //System.out.println("size.. " + logList.size());
            for (ActivityLog activityLogObj : logList) {
                activityJSONObj = getTimeStampAndDynamicResouceBundleProp(activityLogObj, userId);
                redisTemplate.opsForList().rightPush(key, activityJSONObj.toJSONString());
                jSONArray.add(activityJSONObj.toJSONString());
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return jSONArray;
    }

    public JSONObject getTimeStampAndDynamicResouceBundleProp(ActivityLog activityLogObj, long userId) {
        JSONObject activityJSONObj = new JSONObject();
        boolean isRecordPathRequired = false;
        String searchResultPreviewLinkBase = ctx.getApplicationName() + "/search/preview";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Record> record;
        HttpEntity<String> httpEntity;
        String url;
        String dateStr = "";
        Object[] dynamicResouceBundleProp = null;
        String recordIdentifier = activityLogObj.getRecordIdentifier();
        try {
            switch (activityLogObj.getActivityType()) {
                case UserActivityConstant.LOGIN:
                    Activity activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    break;
                case UserActivityConstant.UPDATE_PROFILE:
                    activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    break;
                case UserActivityConstant.PASSWORD_CHANGE:
                    activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    break;
                case UserActivityConstant.ACTIVATE_USER:
                    activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{activity.getUpdatedOrSharedWithUserId(), activity.getUpdatedOrSharedWithUserFullName(), ctx.getApplicationName()};
                    break;
                case UserActivityConstant.DEACTIVATE_USER:
                    activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{activity.getUpdatedOrSharedWithUserId(), activity.getUpdatedOrSharedWithUserFullName(), ctx.getApplicationName()};
                    break;
                case UserActivityConstant.DELETE_USER:
                    activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{activity.getUpdatedOrSharedWithUserId(), activity.getUpdatedOrSharedWithUserFullName(), ctx.getApplicationName()};
                    break;
                case UserActivityConstant.EDIT_USER_DETAILS:
                    activity = (Activity) activityLogObj.getActivity();
                    dateStr = activity.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{activity.getUpdatedOrSharedWithUserId(), activity.getUpdatedOrSharedWithUserFullName(), ctx.getApplicationName()};
                    break;
                case UserActivityConstant.LIKE_RECORD:
                    UserActivity userLike = (UserActivity) activityLogObj.getActivity();
                    dateStr = userLike.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.REMOVE_LIKE:
                    UserActivity removeLike = (UserActivity) activityLogObj.getActivity();
                    dateStr = removeLike.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;

                case UserActivityConstant.SOCIAL_SHARE_RECORD:
                    UserActivity userSocialShare = (UserActivity) activityLogObj.getActivity();
                    dateStr = userSocialShare.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), userSocialShare.getSiteName(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.RATE_RECORD:
                case UserActivityConstant.UPDATE_RATE_RECORD:
                    UserActivity userRating = (UserActivity) activityLogObj.getActivity();
                    dateStr = userRating.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), userRating.getRecordRating(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.SEND_INVITATION:
                    in.gov.nvli.mongodb.domain.UserInvitation userInvitation = (in.gov.nvli.mongodb.domain.UserInvitation) activityLogObj.getActivity();
                    dateStr = userInvitation.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{userInvitation.getInviteeEmail(), userInvitation.getGrantedRoles()};
                    break;
                case UserActivityConstant.CANCEL_INVITATION:
                    in.gov.nvli.mongodb.domain.UserInvitation cancelInvitation = (in.gov.nvli.mongodb.domain.UserInvitation) activityLogObj.getActivity();
                    dateStr = cancelInvitation.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{cancelInvitation.getInviteeEmail(), cancelInvitation.getGrantedRoles()};
                    break;
                case UserActivityConstant.ACCEPT_INVITATION:
                    in.gov.nvli.mongodb.domain.UserInvitation acceptInvitation = (in.gov.nvli.mongodb.domain.UserInvitation) activityLogObj.getActivity();
                    dateStr = acceptInvitation.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{acceptInvitation.getSenderUserName(), acceptInvitation.getGrantedRoles()};
                    break;

                case Constants.CrowdSourceActivity.ADD_CUSTOM_TAG:
                    CrowdSourceAndTagging crowdSourceAndTagging = (in.gov.nvli.mongodb.domain.CrowdSourceAndTagging) activityLogObj.getActivity();
                    dateStr = crowdSourceAndTagging.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), crowdSourceAndTagging.getTags().get(0).getTag(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case Constants.CrowdSourceActivity.ADD_UDC_TAG:
                    CrowdSourceAndTagging crowdSourceAndTaggingudc = (in.gov.nvli.mongodb.domain.CrowdSourceAndTagging) activityLogObj.getActivity();
                    dateStr = crowdSourceAndTaggingudc.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case Constants.CrowdSourceActivity.EDIT_METADATA:
                    CrowdSourceAndTagging crowdSourceAndTaggingudcMetadata = (in.gov.nvli.mongodb.domain.CrowdSourceAndTagging) activityLogObj.getActivity();
                    dateStr = crowdSourceAndTaggingudcMetadata.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.INTRA_SHARE_RECORD:
                    UserActivity userNvliShare = (UserActivity) activityLogObj.getActivity();
                    dateStr = userNvliShare.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(),
                        userService.getFullNameById(userNvliShare.getUpdatedOrSharedWithUserId()), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.KEYWORD_SEARCHED:
                    UserActivity userSearchPhrase = (UserActivity) activityLogObj.getActivity();
                    dateStr = userSearchPhrase.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{userSearchPhrase.getPermaLink(), userSearchPhrase.getSearchPhrase(), ctx.getApplicationName()};
                    break;
                case UserActivityConstant.ADD_EBOOK:
                    UserActivity userEBook = (UserActivity) activityLogObj.getActivity();
                    dateStr = userEBook.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, userEBook.geteBookName(), searchResultPreviewLinkBase};
                    break;
                case UserActivityConstant.DELETE_EBOOK:
                    userEBook = (UserActivity) activityLogObj.getActivity();
                    dateStr = userEBook.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, userEBook.geteBookName(), searchResultPreviewLinkBase};
                    break;
                case UserActivityConstant.ADD_REVIEW:
                    UserActivity userReview = (UserActivity) activityLogObj.getActivity();
                    dateStr = userReview.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.UPDATE_REVIEW:
                    userReview = (UserActivity) activityLogObj.getActivity();
                    dateStr = userReview.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, UserSearchResultActivity.getRecordsTitle(Arrays.asList(activityLogObj.getRecordIdentifier())).get(activityLogObj.getRecordIdentifier()), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.LINK_VISITED:
                    UserActivity linkVisited = (UserActivity) activityLogObj.getActivity();
                    dateStr = linkVisited.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), searchResultPreviewLinkBase};
                    isRecordPathRequired = true;
                    break;
                case UserActivityConstant.CREATE_LIST:
                    UserAddedList createUserList = (UserAddedList) activityLogObj.getActivity();
                    dateStr = createUserList.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{createUserList.getListName()};
                    break;
                case UserActivityConstant.DELETE_LIST:
                    UserAddedList deleteUserList = (UserAddedList) activityLogObj.getActivity();
                    dateStr = deleteUserList.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{deleteUserList.getListName()};
                    break;
                case UserActivityConstant.ADD_TO_LIST:
                    UserAddedList addToUserList = (UserAddedList) activityLogObj.getActivity();
                    dateStr = addToUserList.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{recordIdentifier, activityLogObj.getRecordTitle(), addToUserList.getListName(), searchResultPreviewLinkBase};
                    break;
                case UserActivityConstant.REMOVE_FROM_LIST:
                    UserAddedList removeFromUserList = (UserAddedList) activityLogObj.getActivity();
                    dateStr = removeFromUserList.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{removeFromUserList.getRecordIdentifier(), activityLogObj.getRecordTitle(), removeFromUserList.getListName(), searchResultPreviewLinkBase};
                    break;
                case UserActivityConstant.UPDATE_LIST:
                    UserAddedList updateUserList = (UserAddedList) activityLogObj.getActivity();
                    ;
                    dateStr = updateUserList.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{updateUserList.getListName(), updateUserList.getUpdatedListName()};
                    break;
                case UserActivityConstant.SAVED_PHRASE:
                    UserActivity userSavedPhrase = (UserActivity) activityLogObj.getActivity();
                    dateStr = userSavedPhrase.getActivityTs().toString();
                    dynamicResouceBundleProp = new Object[]{userSavedPhrase.getPermaLink(), userSavedPhrase.getSearchPhrase(), ctx.getApplicationName()};
                    break;
            }
            activityJSONObj.put("activityText", ctx.getMessage(activityLogObj.getActivityType(), dynamicResouceBundleProp, Locale.getDefault()));
            activityJSONObj = doDateManipulationAndSetCustomObj(activityJSONObj, dateStr, userId);
            if (isRecordPathRequired) {
                httpEntity = new HttpEntity(Helper.mitWebserviceAthentication());
                url = mitWSBaseURL + "/record/preview/" + recordIdentifier;
                record = restTemplate.exchange(url, HttpMethod.GET, httpEntity, Record.class);
                if (record.getBody() != null && record.getBody().getPaths() != null) {
                    activityJSONObj.put("recordId", recordIdentifier);
                    activityJSONObj.put("recordImgPath", record.getBody().getPaths().get(0));
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return activityJSONObj;
    }

    public JSONObject doDateManipulationAndSetCustomObj(JSONObject activityJSONObj, String dateStr, Long userId) {
        int endIndex = dateStr.lastIndexOf(".");
        if (endIndex != -1) {
            dateStr = dateStr.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
        }
        activityJSONObj.put("activityTime", dateStr);
        return activityJSONObj;
    }

    public List<String> getFilteredActivityLogOfUserToView(ApplicationContext ctx, long userId, int pageNumber, int pageWindow, String filter) {
        this.activityTypes = new HashSet<>();
        switch (filter) {
            case Constants.FilterActivityLogs.SearchActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.KEYWORD_SEARCHED));
                break;
            case Constants.FilterActivityLogs.VisitActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.LINK_VISITED));
                break;
            case Constants.FilterActivityLogs.SaveActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.SAVED_PHRASE));
                break;
            case Constants.FilterActivityLogs.ShareActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.SOCIAL_SHARE_RECORD, Constants.UserActivityConstant.INTRA_SHARE_RECORD));
                break;
            case Constants.FilterActivityLogs.LikesActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.LIKE_RECORD, Constants.UserActivityConstant.REMOVE_LIKE));
                break;
            case Constants.FilterActivityLogs.CrowdSourceActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.CrowdSourceActivity.ADD_CUSTOM_TAG, Constants.CrowdSourceActivity.ADD_UDC_TAG, Constants.CrowdSourceActivity.EDIT_METADATA));
                break;
            case Constants.FilterActivityLogs.RatingActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.RATE_RECORD, Constants.UserActivityConstant.UPDATE_RATE_RECORD));
                break;
            case Constants.FilterActivityLogs.ReviewActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.ADD_REVIEW, Constants.UserActivityConstant.UPDATE_REVIEW));
                break;
            case Constants.FilterActivityLogs.EbookActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.ADD_EBOOK, Constants.UserActivityConstant.DELETE_EBOOK));
                break;
            case Constants.FilterActivityLogs.ListActivities:
                activityTypes = new HashSet<>(Arrays.asList(Constants.UserActivityConstant.CREATE_LIST, Constants.UserActivityConstant.DELETE_LIST, Constants.UserActivityConstant.UPDATE_LIST, Constants.UserActivityConstant.ADD_TO_LIST, Constants.UserActivityConstant.REMOVE_FROM_LIST));
                break;
            default:
                break;
        }
        return getActivityLogOfUserToView(ctx, userId, pageNumber, pageWindow, filter);
    }

    public void saveSearchedPhrase(String activityName, User userObj,
            HttpServletRequest request, String permalink,
            String searchPhrase, HttpSession httpSess
    ) {

        /**
         * Saving Activity Log in NOSQL
         */
        if (userObj == null) {
            userObj = new User();
            //userObj.setId(Constants.AnonymousUser.LOGIN_ID);
            userObj.setId(anonymousRandomUserId(min, max));
            userObj.setUsername(Constants.AnonymousUser.USER_NAME);
        }
        in.gov.nvli.mongodb.domain.UserActivity userSearchPhrase = new in.gov.nvli.mongodb.domain.UserActivity();
        userSearchPhrase.setActivityTs(new Date());
        userSearchPhrase.setPermaLink(permalink);
        userSearchPhrase.setSearchPhrase(searchPhrase);
        String searchedPhrase = Helper.removePunctuation(searchPhrase);
        String[] searchedPhrases = searchedPhrase.split(" ");
        List<String> wordList = new ArrayList<>(Arrays.asList(searchedPhrases));
        for (String str : searchedPhrases) {
            if (Helper.removeStopWords(str)) {
                wordList.remove(str);
            }
        }
        wordList.removeAll(Collections.singleton(""));
        wordList.removeAll(Collections.singleton(null));
        userSearchPhrase.setTokanizedPhrase(wordList);
        saveActivityLog(null, userObj.getId(), activityName, userSearchPhrase, request, userObj.getUsername(), httpSess, Constants.FilterActivityLogs.SearchActivities, null);
    }

    /**
     * Service to save searched result when user want to save it
     *
     * @param activityName
     * @param userObj
     * @param requestObj
     * @param savedPhrase Name by which url to be saved
     * @param savedLink url to be saved
     * @return String
     */
    public String saveSavedPhrase(String activityName, User userObj,
            HttpServletRequest request, String savedPhrase,
            String savedLink
    ) {
        try {
            if (userObj != null) {
                UserActivity userSearchPhrase = new UserActivity();
                userSearchPhrase.setActivityTs(new Date());
                userSearchPhrase.setSearchPhrase(savedPhrase);
                userSearchPhrase.setPermaLink(savedLink);
                saveActivityLog(null, userObj.getId(), activityName, userSearchPhrase, request, userObj.getUsername(), null, Constants.FilterActivityLogs.SaveActivities, null);
                return "saved";
            } else {
                LOG.error("User " + userObj + " not in session");
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * Service to save visited links of by user
     *
     * @param uniqueIdentifier the unique identifier of visited nvli object
     * @param activityName the name of activity performed by user which has to
     * be logged.key is stored in db and value of message in property file
     * @param userObj the user who performed the activity
     * @param requestObj client's ipaddress and userAgent
     * @param userSearchedPhraseIdInSession id of what user has searched
     * for(from session)
     * @return save to new list status true/false
     */
    public boolean saveUserVisitedLinks(String activityName, String uniqueIdentifier,
            String recordTitle, User userObj,
            HttpServletRequest request, String userSearchedPhraseIdInSession
    ) {
        boolean isSaved = false;
        try {
            if (userObj != null) {
                if (userSearchedPhraseIdInSession != null) {
                    /**
                     * Saving Activity Log in NOSQL
                     */
                    UserActivity userSearchPhrase = new UserActivity();
                    userSearchPhrase.setActivityTs(new Date());
                    userSearchPhrase.setSearchPhrase(userSearchedPhraseIdInSession);
                    saveActivityLog(uniqueIdentifier, userObj.getId(), activityName, userSearchPhrase, request, userObj.getUsername(), null, Constants.FilterActivityLogs.SaveActivities, null);

                } else {
                    LOG.error("Search Phrase not found in session");
                }

            } else {
                LOG.error("User " + userObj + " in session");
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return isSaved;
    }

    public List<UserInvitation> getInvitationsSentByUser(long userId) {
        return userInvitationDAO.getInvitationsSentByUser(userId);
    }

    public boolean deleteEbook(User userObj, String recordIdentifier, HttpServletRequest request, String title) {
        try {
            UserActivity userEBook = new UserActivity();
            userEBook.setActivityTs(new Date());
            userEBook.seteBookName(title);
            if (saveActivityLog(recordIdentifier, userObj.getId(), Constants.UserActivityConstant.DELETE_EBOOK, userEBook, request, userObj.getUsername(), null, Constants.FilterActivityLogs.EbookActivities, null)) {
                userEBookService.deleteUserEBooksByUserIdAndRecordIdentifier(userObj.getId(), recordIdentifier);
            }
            return true;
        } catch (Exception e) {
            LOG.error(e);
            return false;
        }
    }

    /**
     * This method is used to get user suggestion on search of user name.
     *
     * @param username
     * @return Json String of sugesstions.
     * @throws Exception
     */
    public String usersSuggestions(String username) throws Exception {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject tagsJSONObject;
        List<User> users = userDAO.usersSuggestions(username, true);
        for (User userDetails : users) {
            tagsJSONObject = new JSONObject();
            tagsJSONObject.put("data", userDetails.getId());
            if (userDetails.getMiddleName() != null) {
                tagsJSONObject.put("value", userDetails.getFirstName() + " " + userDetails.getMiddleName() + " " + userDetails.getLastName());
            } else {
                tagsJSONObject.put("value", userDetails.getFirstName() + " " + userDetails.getLastName());
            }
            tagsJSONObject.put("path", userDetails.getProfilePicPath());
            tagsJSONObject.put("city", userDetails.getCity());
            tagsJSONObject.put("state", userDetails.getState());
            tagsJSONObject.put("country", userDetails.getCountry());
            jSONArray.add(tagsJSONObject);
        }
        finalJSONObj.put("query", username);
        finalJSONObj.put("suggestions", jSONArray);
        return finalJSONObj.toString();
    }

    public String usersSuggestionsBySearchedPhrase(String username, long roleId) throws Exception {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject userJSONObj;
        List<User> users = userDAO.usersSuggestions(username, false);
        for (User userDetails : users) {
            boolean flag = false;
            for (Role role : userDetails.getRoles()) {
                if (role.getRoleId() == roleId) {
                    flag = true;
                }
            }
            if (!flag) {
                continue;
            }
            userJSONObj = new JSONObject();
            userJSONObj.put("data", userDetails.getId());
            JSONArray roleArray = new JSONArray();
            Set<Role> roles = userDetails.getRoles();
            for (Role role1 : roles) {
                JSONObject roleJSONObj = new JSONObject();
                roleJSONObj.put("roleId", role1.getRoleId());
                roleJSONObj.put("roleName", role1.getName());
                roleArray.add(roleJSONObj);
            }
            if (userDetails.getMiddleName() != null) {
                userJSONObj.put("value", userDetails.getFirstName() + " " + userDetails.getMiddleName() + " " + userDetails.getLastName());
            } else {
                userJSONObj.put("value", userDetails.getFirstName() + " " + userDetails.getLastName());
            }
            userJSONObj.put("userId", userDetails.getId());
            userJSONObj.put("userFirstName", userDetails.getFirstName());
            userJSONObj.put("userLastName", userDetails.getLastName());
            userJSONObj.put("userEmail", userDetails.getEmail());
            userJSONObj.put("userProfilePicPath", userDetails.getProfilePicPath());
            userJSONObj.put("userDeleted", userDetails.getDeleted());
            userJSONObj.put("userEnabled", userDetails.getEnabled());
            userJSONObj.put("roleArray", roleArray);
            jSONArray.add(userJSONObj);
        }
        finalJSONObj.put("selectedRole", userService.getRoleByID(roleId).getName());
        finalJSONObj.put("query", username);
        finalJSONObj.put("suggestions", jSONArray);
        return finalJSONObj.toString();
    }

    /**
     * This method used to get email list from user Id list.
     *
     * @param userIdList List of long
     * @return List of email address
     */
    public List<String> getEmailList(List<Long> userIdList) {
        return userDAO.getEmailList(userIdList);
    }

    private RequestAttributes getRequestAttributeObject(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        String userAgent = request.getHeader("User-Agent");
        if (userAgent == null) {
            userAgent = "";
        }
        RequestAttributes reqAttrObj = new RequestAttributes();
        reqAttrObj.setIpAddress(ipAddress);
        reqAttrObj.setUserAgent(userAgent);
        return reqAttrObj;
    }

    /**
     * Saves Other activities like login,invitation etc in mongo db database
     *
     * @param recordIdentifier
     * @param userId
     * @param activityName
     * @param object type of activity
     * @param requestObj RequestAttributes (holds ip address and user agent
     * info)
     * @param username
     * @return activity saved successfully or not(true|false)
     */
    public boolean saveActivityLog(String recordIdentifier, Long userId,
            final String activityName, final Activity object, HttpServletRequest request,
            String username, HttpSession httpSess, String filter, String recordType) {
        try {

            ActivityLog<Activity> activityLog = new ActivityLog<>();
            if (recordIdentifier != null && recordType != null) {
                ExecutorService executor = Executors.newFixedThreadPool(5);
                Runnable rabitRunnable = () -> {
                    RabbitmqProducer producer = new RabbitmqProducer();
                    if (activityName.equals(Constants.UserActivityConstant.RATE_RECORD)) {
                        producer.SendActivityLogToQueue(amqpTemplate, new in.gov.nvli.dto.ActivityLog(recordIdentifier, userId, activityName, (double) ((UserActivity) object).getRecordRating(), recordType));
                    } else {
                        producer.SendActivityLogToQueue(amqpTemplate, new in.gov.nvli.dto.ActivityLog(recordIdentifier, userId, activityName, -1d, recordType));
                    }
                };
                executor.execute(rabitRunnable);
                executor.shutdown();
                while (!executor.isTerminated()) {
                }

            }
            if (recordIdentifier != null) {
                activityLog.setRecordIdentifier(recordIdentifier);
                activityLog.setRecordTitle(getObjectTitle(recordIdentifier));
            }
            if (recordType != null) {
                activityLog.setRecordType(recordType);
            }
            if (userId != null) {
                activityLog.setUserId(userId);
            }
            if (username != null) {
                activityLog.setUsername(username);
            }
            if (object != null) {
                activityLog.setActivity(object);
            }
            if (activityName != null) {
                activityLog.setActivityType(activityName);
            }
            RequestAttributes requestObj = getRequestAttributeObject(request);
            if (requestObj != null) {
                if (requestObj.getUserAgent() != null) {
                    activityLog.setUserAgentInfo(requestObj.getUserAgent());
                }
                if (requestObj.getIpAddress() != null) {
                    activityLog.setIpAddress(requestObj.getIpAddress());
                }
            }
            //for not logged in users
            if (userId <= 0) {
                retryTemplate.execute(context -> {
                    return activityLogService.saveObject(activityLog) != null;
                });
            } else if (activityName.trim().equalsIgnoreCase(Constants.UserActivityConstant.KEYWORD_SEARCHED)) {
                String searchPhrase = (String) httpSess.getAttribute(userId + "_" + Constants.USER_SEARCHED_PHRASE_ID);
                if (searchPhrase == null || !searchPhrase.trim().equals(((UserActivity) object).getSearchPhrase())) {//different search
                    retryTemplate.execute(context -> {
                        ActivityLog log = activityLogService.saveObject(activityLog);
                        if (log != null) {
                            httpSess.setAttribute(userId + "_" + Constants.USER_SEARCHED_PHRASE_ID, ((UserActivity) object).getSearchPhrase());
                        }
                        return log != null;
                    });
                }
            } else {
                retryTemplate.execute(context -> {
                    return activityLogService.saveObject(activityLog) != null;
                });
            }
            if (filter != null && !filter.isEmpty()) {
                updateRedisCachesByUserIdAndFilter(userId, filter, activityLog);
            }
            return true;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }

    }

    /**
     * @author Ruturaj This method handles push and pop operations from redis.
     * @param userid
     * @param filter
     * @param activityLogObj
     */
    public void updateRedisCachesByUserIdAndFilter(Long userid, String filter, ActivityLog activityLogObj) {
        Object key = (userid + "" + filter);
        JSONObject activityJSONObj;
        activityJSONObj = getTimeStampAndDynamicResouceBundleProp(activityLogObj, userid);
        if (redisTemplate.opsForList().size(key) % 15 != 0) {
            redisTemplate.opsForList().leftPush(key, activityJSONObj.toJSONString());
        } else {
            redisTemplate.opsForList().leftPush(key, activityJSONObj.toJSONString());
            redisTemplate.opsForList().rightPop(key);
        }
        if (!filter.equalsIgnoreCase(Constants.FilterActivityLogs.AllActivities)) {
            Object key2 = (userid + "" + Constants.FilterActivityLogs.AllActivities);
            if (redisTemplate.opsForList().size(key2) % 15 != 0) {
                redisTemplate.opsForList().leftPush(key2, activityJSONObj.toJSONString());
            } else {
                redisTemplate.opsForList().leftPush(key2, activityJSONObj.toJSONString());
                redisTemplate.opsForList().rightPop(key2);
            }
        }
    }

    /**
     * Service to save the ebook activity performed by the user
     *
     * @param activityName
     * @param uniqueIdentifier the unique identifier of nvli objects
     * @param title
     * @param userObj
     * @param request
     * @param affectedUserId
     * @return like saving status of a user true/false
     */
    public boolean saveUserEBookActivity(String activityName, String uniqueIdentifier, String title, User userObj, Long affectedUserId, HttpServletRequest request) {
        UserActivity userEBook = new UserActivity();
        try {
            userEBook.setActivityTs(new Date());
            userEBook.seteBookName(title);
            if (saveActivityLog(uniqueIdentifier, userObj.getId(), activityName, userEBook, request, userObj.getUsername(), null, Constants.FilterActivityLogs.EbookActivities, null)) {
                UserEBook eBook = new UserEBook();
                eBook.setAddedTime(new Date());
                eBook.setUserId(userObj.getId());
                eBook.setRecordIdentifier(uniqueIdentifier);
                eBook.seteBookName(title);
                userEBookService.saveObject(eBook);
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public long anonymousRandomUserId(long min, long max) {
        Random rand = new Random();
        long randomNum = rand.nextInt((int) ((max - min) + 1)) + min;
        return randomNum;
    }

    public String getObjectTitle(String uniqueIdentifier) {
        String objectTitle = "";
        try {
            List<String> recIdentifiers = new ArrayList<>();
            recIdentifiers.add(uniqueIdentifier);

            Map<String, String> mapOfIdAndName = new HashMap<>();
            mapOfIdAndName = UserSearchResultActivity.getRecordsTitle(recIdentifiers);

            objectTitle = mapOfIdAndName.get(uniqueIdentifier);
            if (objectTitle == null) {
                objectTitle = "";
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return objectTitle;
    }

    /**
     * Saves like activity on record performed by user
     *
     * @param activityName name of activity
     * @param uniqueIdentifier record identifier
     * @param userId user id
     * @param request
     * @param recordType
     * @param username
     * @return
     */
    public boolean saveLikesInActivityLog(String activityName, String uniqueIdentifier, Long userId, HttpServletRequest request, String username, String recordType) {
        UserActivity likeObj = new UserActivity();
        try {
            likeObj.setActivityTs(new Date());
            saveActivityLog(uniqueIdentifier, userId, activityName, likeObj, request, username, null, Constants.FilterActivityLogs.LikesActivities, recordType);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    /**
     * Saves social share of record performed by user
     *
     * @param activityName name of activity
     * @param uniqueIdentifier record identifier
     * @param userId user id
     * @param requestObj RequestAttributes (holds ip address and user agent
     * info)
     * @param providerId provider id like For Google+ its google
     * @param username
     * @see SocialSite
     * @return activity saved successfully or not(true|false)
     */
    public boolean saveSocialShareOfUser(String activityName, String uniqueIdentifier, Long userId, HttpServletRequest request, String providerId, String username, String recordType) {
        List<SocialSite> siteObjects = socialSiteDAO.findbyQuery("findSiteByProviderId", providerId);
        if (siteObjects != null && !siteObjects.isEmpty()) {
            UserActivity shareObj = new UserActivity();
            try {
                shareObj.setActivityTs(Helper.rightNow());
                shareObj.setSiteName(siteObjects.get(0).getSite_name());
                saveActivityLog(uniqueIdentifier, userId, activityName, shareObj, request, username, null, Constants.FilterActivityLogs.ShareActivities, recordType);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                return false;
            }
        } else {
            LOG.info("Provider " + providerId + " is not present in database");
            return false;
        }
        return true;
    }

    /**
     * Saves rating activity on record performed by user
     *
     * @param activityName name of activity
     * @param uniqueIdentifier record identifier
     * @param userId user id
     * @param requestObj RequestAttributes (holds ip address and user agent
     * info)
     * @param userRating
     * @param username
     * @param user
     * @return activity saved successfully or not(true|false)
     */
    public long saveRatingOfUser(String uniqueIdentifier, Long userId, HttpServletRequest request, int userRating, String userName, float avgRecRating, int numberOfRatings, String recordType) {
        ReviewAndRating ratings = userReviewAndRatingService.getUserRatingsByUserIdAndRecordIdentifier(userId, uniqueIdentifier);
        UserActivity rateObj = new UserActivity();
        rateObj.setActivityTs(new Date());
        rateObj.setRecordRating(userRating);
        try {
            if (ratings != null) {
                if (ratings.getRecordRating() != null) {
                    ratings.setRecordRating(userRating);
                    ratings.setTimeStamp(new Date());
                    if (userReviewAndRatingService.saveObject(ratings) != null) {
                        avgRecRating = (userRating + (avgRecRating * (numberOfRatings - 1))) / (numberOfRatings);
                        saveActivityLog(uniqueIdentifier, userId, Constants.UserActivityConstant.UPDATE_RATE_RECORD, rateObj, request, userName, null, Constants.FilterActivityLogs.RatingActivities, recordType);
                        UserSearchResultActivity.rateRecord(uniqueIdentifier, avgRecRating, false);
                        return -1;
                    }
                } else {
                    ratings.setRecordRating(userRating);
                    ratings.setTimeStamp(new Date());
                    if (userReviewAndRatingService.saveObject(ratings) != null) {
                        avgRecRating = (userRating + (avgRecRating * numberOfRatings)) / (1 + numberOfRatings);
                        saveActivityLog(uniqueIdentifier, userId, Constants.UserActivityConstant.RATE_RECORD, rateObj, request, userName, null, Constants.FilterActivityLogs.RatingActivities, recordType);
                        return UserSearchResultActivity.rateRecord(uniqueIdentifier, avgRecRating, true);
                    }
                }
            } else {
                ratings = new ReviewAndRating();
                ratings.setRecordIdentifier(uniqueIdentifier);
                ratings.setRecordRating(userRating);
                ratings.setUserId(userId);
                ratings.setTimeStamp(new Date());
                if (userReviewAndRatingService.saveObject(ratings) != null) {
                    avgRecRating = (userRating + (avgRecRating * numberOfRatings)) / (1 + numberOfRatings);
                    saveActivityLog(uniqueIdentifier, userId, Constants.UserActivityConstant.RATE_RECORD, rateObj, request, userName, null, Constants.FilterActivityLogs.RatingActivities, recordType);
                    return UserSearchResultActivity.rateRecord(uniqueIdentifier, avgRecRating, true);
                } else {
                    return 0;
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    /**
     * Service to save the review activity performed by the user
     *
     * @param activityName
     * @param uniqueIdentifier the unique identifier of nvli objects
     *
     * logged.key is stored in db and value of message in property file
     * @param userId
     * @param requestObj client's ipaddress and userAgent
     * @param userReview review posted by user
     * @param status
     * @param username
     * @return save review status true/false
     */
    public long saveReviewOfUser(String uniqueIdentifier, Long userId, HttpServletRequest request, String userReview, String userName, String recordType) {
        ReviewAndRating review = userReviewAndRatingService.getUserReviewByUserIdAndRecordIdentifier(userId, uniqueIdentifier);
        UserActivity reviewObj = new UserActivity();
        reviewObj.setActivityTs(new Date());
        reviewObj.setMessage(userReview);
        try {
            if (review != null) {

                if (review.getReviewText() != null) {
                    //to upadte the review
                    review.setReviewText(userReview);
                    review.setTimeStamp(new Date());
                    if (userReviewAndRatingService.saveObject(review) != null) {
                        saveActivityLog(uniqueIdentifier, userId, Constants.UserActivityConstant.UPDATE_REVIEW, reviewObj, request, userName, null, Constants.FilterActivityLogs.ReviewActivities, recordType);
                    } else {
                        return 0;
                    }
                    return -1;
                } else {
                    //if rating is given and to add new review
                    review.setReviewText(userReview);
                    review.setTimeStamp(new Date());
                    if (userReviewAndRatingService.saveObject(review) != null) {
                        saveActivityLog(uniqueIdentifier, userId, Constants.UserActivityConstant.ADD_REVIEW, reviewObj, request, userName, null, Constants.FilterActivityLogs.ReviewActivities, recordType);
                        return UserSearchResultActivity.reviewRecord(uniqueIdentifier);
                    } else {
                        return 0;
                    }
                }
            } else {
                //to add the new review
                review = new ReviewAndRating();
                review.setRecordIdentifier(uniqueIdentifier);
                review.setReviewText(userReview);
                review.setUserId(userId);
                review.setTimeStamp(new Date());
                if (userReviewAndRatingService.saveObject(review) != null) {
                    saveActivityLog(uniqueIdentifier, userId, Constants.UserActivityConstant.ADD_REVIEW, reviewObj, request, userName, null, Constants.FilterActivityLogs.ReviewActivities, recordType);
                    return UserSearchResultActivity.reviewRecord(uniqueIdentifier);
                } else {
                    return 0;
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
            return 0;
        }
    }

    public void saveLikesOfUser(String uniqueIdentifier, Long userId, HttpServletRequest request, String userName) {
        ReviewAndRating likes = userReviewAndRatingService.getUserReviewByUserIdAndRecordIdentifier(userId, uniqueIdentifier);
        try {
            if (likes != null) {
                if (likes.getIsLiked() != null) {
                    likes.setIsLiked(!likes.getIsLiked());
                } else {
                    likes.setIsLiked(true);
                }
            } else {
                likes = new ReviewAndRating();
                likes.setRecordIdentifier(uniqueIdentifier);
                likes.setUserId(userId);
                likes.setTimeStamp(new Date());
                likes.setIsLiked(true);
            }
            userReviewAndRatingService.saveObject(likes);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            e.printStackTrace();
        }
    }

    public List<ReviewAndRating> getRatingStatistics(String uniqueIdentifier) {
        Aggregation aggregations = Aggregation.newAggregation(
                match(Criteria.where("recordIdentifier").is(uniqueIdentifier)),
                group("recordRating").count().as("recordRatingCount"),
                project("recordRatingCount").and("recordRating").previousOperation(),
                sort(DESC, "recordRating")
        );
        AggregationResults<ReviewAndRating> groupResults
                = mongoTemplate.aggregate(aggregations, Constants.CollectionNames.USER_REVIEW_RATING, ReviewAndRating.class);
        return groupResults.getMappedResults();
    }

    /**
     * Service to save nvli share activity performed by the user
     *
     * @param activityName
     * @param uniqueIdentifier the unique identifier of nvli objects
     * @param userId {@link User}
     * @param request
     * @param updatedOrSharedWithUserId {@link Activity }
     * @param message the msg which was sent
     * @param recordType
     * @param username
     * @return share successful status true/false
     */
    public boolean saveNvliShareOfUser(String activityName, String uniqueIdentifier, Long userId, HttpServletRequest request, Long updatedOrSharedWithUserId, String message, String username, String recordType) {
        UserActivity nvliShareObj = new UserActivity();
        try {
            nvliShareObj.setActivityTs(new Date());
            nvliShareObj.setUpdatedOrSharedWithUserId(updatedOrSharedWithUserId);
            nvliShareObj.setMessage(message);
            nvliShareObj.setUpdatedOrSharedWithUserFullName(userService.getFullNameById(updatedOrSharedWithUserId));
            saveActivityLog(uniqueIdentifier, userId, activityName, nvliShareObj, request, username, null, Constants.FilterActivityLogs.ShareActivities, recordType);
        } catch (Exception e) {
            LOG.error("User " + userId + " not in session");
            LOG.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    /**
     * Service to save record to an existing list of user
     *
     * @param uniqueIdentifier the unique identifier of nvli objects
     * @param activityName the name of activity performed by user which has to
     * be logged.key is stored in db and value of message in property file
     * @param recordTitle
     * @param userObj the user who performed the activity
     * @param request
     * @param listName
     * @param recordType
     * @return save add to existing list status true/false
     * @throws in.gov.nvli.custom.exceptions.DuplicateEntryException
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean saveToExistingList(String activityName, String uniqueIdentifier,
            String recordTitle, User userObj, HttpServletRequest request,
            String listName, String recordType) throws DuplicateEntryException, Exception {
        if (userObj != null) {
            UserAddedList addedList = userAddedListService.getListByUserIDListName(userObj.getId(), listName);
            if (addedList != null) {
                if (userAddedListService.checkIfResourceAlreadyInList(userObj.getId(), uniqueIdentifier, listName)) {
                    //save activity log
                    saveActivityLog(uniqueIdentifier, userObj.getId(), activityName,
                            addedList, request, userObj.getUsername(), null, Constants.FilterActivityLogs.ListActivities, recordType);
                    //save activity
                    UserAddedList userAddedList = new UserAddedList(userObj.getId(), userObj.getFirstName() + " " + userObj.getLastName(), uniqueIdentifier, recordTitle, listName, addedList.getPublicPrivate(), 1, new Date());
                    userAddedListService.save(userAddedList);
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            LOG.error("User " + userObj + " not in session");
        }
        return false;
    }

    /**
     * Service to save record to a newly created list of user
     *
     * @param uniqueIdentifier the unique identifier of nvli objects
     * @param activityName the name of activity performed by user which has to
     * be logged.key is stored in db and value of message in property file
     * @param recordTitle
     * @param userObj the user who performed the activity
     * @param request
     * @param newListName      name of list typed by user
     * @param listAccessType 0 means public list and 1 means private list
     * @param recordType
     * @return save to new list status true/false
     * @throws in.gov.nvli.custom.exceptions.DuplicateEntryException
     * @throws java.lang.Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean saveToNewlyCreatedList(String activityName, String uniqueIdentifier, String recordTitle,
            User userObj, HttpServletRequest request,
            String newListName, int listAccessType, String recordType) throws DuplicateEntryException, Exception {
        if (!userAddedListService.IsListPresentByUserIDListName(userObj.getId(), newListName)) {
            //save activity log
            UserAddedList userAddedList = new UserAddedList(userObj.getId(), userObj.getUsername(), uniqueIdentifier, recordTitle, newListName, listAccessType, 0, new Date());
            saveActivityLog(uniqueIdentifier, userObj.getId(), activityName,
                    userAddedList, request, userObj.getUsername(), null, Constants.FilterActivityLogs.ListActivities, recordType);
            //save activity
            UserAddedList addedList = new UserAddedList(userObj.getId(), userObj.getFirstName() + " " + userObj.getLastName(), uniqueIdentifier, recordTitle, newListName, listAccessType, 0, new Date());
            addedList.setUserProfilePhotoPath(userObj.getProfilePicPath());
            addedList.setFollowerCount(0);
            userAddedListService.save(addedList);
            return true;
        } else {
            LOG.info(newListName + " already exists");
            return false;
        }
    }
}
