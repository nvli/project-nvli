package in.gov.nvli.service;

import in.gov.nvli.beans.UserDetailsBean;
import in.gov.nvli.custom.user.CustomUsersLists;
import in.gov.nvli.custom.user.RequestAttributes;
import in.gov.nvli.dao.policy.PolicyDAO;
import in.gov.nvli.dao.user.DeactivationFeedbackBaseDAO;
import in.gov.nvli.dao.user.IUserFeedbackDetailsDAO;
import in.gov.nvli.dao.user.SecretKeyDetailsDAO;
import in.gov.nvli.dao.user.UploadedFilesLogDAO;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.dao.user.UserFeedbackDAO;
import in.gov.nvli.dao.user.UserInterestDAO;
import in.gov.nvli.dao.user.UserInvitationDAO;
import in.gov.nvli.dao.user.UserOtherFeedbackDAO;
import in.gov.nvli.dao.user.UserRoleDAO;
import in.gov.nvli.domain.policy.PolicyDocument;
import in.gov.nvli.domain.user.Cities;
import in.gov.nvli.domain.user.Country;
import in.gov.nvli.domain.user.DeactivationFeedbackBase;
import in.gov.nvli.domain.user.District;
import in.gov.nvli.domain.user.FeedbackForm;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.SecretKeyDetails;
import in.gov.nvli.domain.user.States;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserActivityLog;
import in.gov.nvli.domain.user.UserConnection;
import in.gov.nvli.domain.user.UserFeedback;
import in.gov.nvli.domain.user.UserInterest;
import in.gov.nvli.domain.user.UserInvitation;
import in.gov.nvli.domain.user.UserOtherFeedback;
import in.gov.nvli.domain.user.UserUploadedFile;
import in.gov.nvli.dto.RoleDTO;
import in.gov.nvli.dto.SecretKeyDTO;
import in.gov.nvli.dto.socialSettingDTO;
import in.gov.nvli.mongodb.domain.Notifications;
import in.gov.nvli.mongodb.service.NotificationService;
import in.gov.nvli.rabbitmq.RabbitmqProducer;
import in.gov.nvli.social.UserConnectionDAO;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.EncodeDecodeUtils;
import in.gov.nvli.util.UserInterestComparatorByID;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import static org.apache.commons.lang.RandomStringUtils.randomNumeric;

/**
 * This class is to provide services for User related actions
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 * @auther Ruturaj Powar <ruturajp@cdac.in>
 * @author Vivek Bugale <bvivek@cdac.in>
 * @since 1.0
 * @version 1.0
 */
@Service
public class UserService {

    private static final Logger LOG = Logger.getLogger(UserService.class);
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserConnectionDAO useerconnectionDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Autowired
    private UserInvitationDAO userInvitationDAO;

    @Value("${recaptcha.google.validation.url}")
    private String validationURL;

    @Value("${recaptcha.google.secretKey}")
    private String secretKey;

    @Value("${recaptcha.useragent}")
    private String USER_AGENT;

    @Autowired
    private SendMailService mailService;

    @Autowired
    private UserActivityService userActivityService;

    @Autowired
    private UserInterestDAO userInterestDAO;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private MessageSource messages;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private UserFeedbackDAO feedbackDAO;

    @Autowired
    private UserOtherFeedbackDAO otherFeedbackDAO;

    @Autowired
    DeactivationFeedbackBaseDAO deactivationFeedbackBaseDAO;

    @Autowired
    UploadedFilesLogDAO uploadedFilesLogDAO;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private PolicyDAO policyDAO;

    @Autowired
    private IUserFeedbackDetailsDAO userFeedbackDetailsDAO;

    @Value("${secret.key.file.path}")
    private String secretKeyFilePath;

    @Autowired
    private SecretKeyDetailsDAO secretKeyDetailsDAO;

    /**
     * Create the new User
     *
     * @param user {@link User}
     * @param invitationIdHash
     * @param request
     * @return true if created successfully otherwise false.
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean registerNewUser(User user, String invitationIdHash, HttpServletRequest request) {
//        if (emailExist(user.getEmail())) {
//            throw new EmailExistsException("Account already registerd with an email address : " + user.getEmail());
//        }
        Set<Role> roles = new HashSet<>();
        Set<PolicyDocument> policies = new HashSet<>();
        if (!(policyDAO.checkPolicy(Long.valueOf(1)))) {
            PolicyDocument privacyPolicy = policyDAO.findMaximumVersion(Long.valueOf(1));
            policies.add(privacyPolicy);
            user.setPolicies(policies);
        }
        if (!(policyDAO.checkPolicy(Long.valueOf(2)))) {
            PolicyDocument tnCPolicy = policyDAO.findMaximumVersion(Long.valueOf(2));
            policies.add(tnCPolicy);
            user.setPolicies(policies);
        }
        if (invitationIdHash == null || invitationIdHash.isEmpty()) {
            LOG.info("non invited");
            Role defaultRole = userRoleDAO.get(Long.valueOf(1));
            roles.add(defaultRole);
            user.setRoles(roles);
            return userDAO.createNew(user);
        } else {
            LOG.info("invited");
            UserInvitation invObj = userInvitationDAO.getInvitationObjectByHash(invitationIdHash);
            String grantedRoleNames = "";
            List<Role> invitedRoles = invObj.getGrantedRoles();
            for (Role role : invitedRoles) {
                grantedRoleNames = grantedRoleNames + role.getName() + " ,";
                roles.add(role);
            }
            if (grantedRoleNames.endsWith(",")) {
                grantedRoleNames = grantedRoleNames.substring(0, grantedRoleNames.length() - 1);
            }
            user.setRoles(roles);

            if (userDAO.createNew(user)) {
                invObj.setInvitationAccepted(Constants.INVITE_ACCEPTED);
                updateInvitation(invObj);
                in.gov.nvli.mongodb.domain.UserInvitation acceptInvitation = new in.gov.nvli.mongodb.domain.UserInvitation();
                acceptInvitation.setGrantedRoles(grantedRoleNames);
                acceptInvitation.setSenderUserId(invObj.getInvitedBy().getId());
                String userFullName = (invObj.getInvitedBy().getFirstName() != null ? invObj.getInvitedBy().getFirstName() : "") + " " + (invObj.getInvitedBy().getMiddleName() != null ? invObj.getInvitedBy().getMiddleName() : "") + " " + (invObj.getInvitedBy().getLastName() != null ? invObj.getInvitedBy().getLastName() : "");
                acceptInvitation.setSenderUserName(userFullName);
                acceptInvitation.setActivityTs(new Date());
                userActivityService.saveActivityLog(null, user.getId(), Constants.UserActivityConstant.ACCEPT_INVITATION, acceptInvitation, request, user.getUsername(), null, Constants.FilterActivityLogs.AllActivities, null);
                return true;
            }
        }
        return false;
    }

    /**
     * update User details
     *
     * @param user {@link User}
     */
    public void updateUser(User user) {
        System.out.println("in update user service");
        userDAO.update(user);
    }

    /**
     * update invitation details
     *
     * @param invitationObj
     */
    public void updateInvitation(UserInvitation invitationObj) {
        userInvitationDAO.updateInvitationObject(invitationObj);
    }

    /**
     * Validates Google reCaptcha response text using Google reCaptcha Client
     * Verification URL and secret key
     *
     * @param googleReCaptchaResponse
     * @param remoteIP
     * @return boolean
     */
    public boolean validateReCaptcha(String googleReCaptchaResponse, String remoteIP) {
        try {
            URL obj = new URL(validationURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //Add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "secret=" + secretKey + "&response=" + googleReCaptchaResponse + "&remoteip=" + remoteIP;
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + validationURL);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONParser parser = new JSONParser();
            Object parsedObj = parser.parse(response.toString());
            JSONObject responseObject = (JSONObject) parsedObj;
            return (Boolean) responseObject.get("success");
        } catch (MalformedURLException e) {
            LOG.error(e.getMessage(), e);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } catch (ParseException e) {
            LOG.error(e.getMessage(), e);
        }
        return false;
    }

    /**
     * Method to get list of all users
     *
     * @TODO: make it paginated
     *
     * @return List<User>
     */
    public List<User> getAllUsers() {
//        List<CustomUserDetails> customUsers = new ArrayList<>();
        List<User> users = userDAO.getUsers();
//        for (User user : users) {
//            CustomUserDetails customUser;
//            customUser = new CustomUserDetails(user.getId(), user.getEmail(), user.getEnabled(), user.getDeleted(), user.getFirstName(), user.getMiddleName(), user.getLastName(), user.getRoles());
//            customUsers.add(customUser);
//        }
        return users;
    }

    /**
     *
     * @param roleCode
     * @return
     */
    public List<User> getUsersByRoleCode(String roleCode) {
        Role role = userRoleDAO.getRoleByCode(roleCode);
        return role.getUsers();
    }

    /**
     * Method to fetch users by roleId for pagination purpose
     *
     * @param roleId
     * @param pageNumber
     * @param pageWindow
     * @param sortOrder---ascending or descending
     * @param sortBy--column name or field of User object by which sorting to be
     * done
     * @return
     */
    public List<User> getUsersByRoleId(long roleId, int pageNumber, int pageWindow, String sortBy, String sortOrder) {
        return userDAO.getUsersByRoleId(roleId, pageNumber, pageWindow, sortBy, sortOrder);

    }

    /**
     * Method to fetch invited users who are invited by Crowd Source Manager.
     *
     * @param userInvitations---List of all invited users.
     * @return -List of invited users by crowd source manager.
     */
    public List<UserInvitation> getInvitedUsersByCrowdManager(List<UserInvitation> userInvitations) {
        List<UserInvitation> invitedByCrowdManager = new ArrayList<>();
        for (Iterator<UserInvitation> iterator = userInvitations.iterator(); iterator.hasNext();) {
            UserInvitation userInvitation = iterator.next();
            for (Iterator<Role> iterateRole = userInvitation.getGrantedRoles().iterator(); iterateRole.hasNext();) {
                Role role = iterateRole.next();
                if (!role.getCode().equals("LIB_USER") & !role.getCode().equals("CURATOR_USER")) {
                    iterator.remove();
                }
            }
        }
        return invitedByCrowdManager;
    }

    /**
     * Method to fetch invited users.
     *
     * @param pageNumber
     * @param pageWindow
     * @param sortBy -ascending or descending
     * @param sortOrder -column name or field of User object by which sorting to
     * be done
     * @return List of invited users.
     */
    public List<UserInvitation> getInvitedUsers(int pageNumber, int pageWindow, String sortBy, String sortOrder) {
        User user = getCurrentUser();
        List<UserInvitation> userInvitations = userInvitationDAO.getInvitedUser(pageNumber, pageWindow, sortBy, sortOrder);
        boolean flag = false;
        for (Role role : user.getRoles()) {
            if (role.getCode().equals(Constants.UserRole.METADATA_ADMIN)) {
                flag = true;
            }
        }
        if (flag) {
            return getInvitedUsersByCrowdManager(userInvitations);
        }
        return userInvitations;
    }

    public List<UserInvitation> getInvitedUsersByFiltersWithLimit(String searchString, int dataLimit, int pageNo) {
        User user = getCurrentUser();
        List<UserInvitation> userInvitations = userInvitationDAO.getInvitedUserByFiltersWithLimit(searchString, dataLimit, pageNo);
        boolean flag = false;
        for (Role role : user.getRoles()) {
            if (role.getCode().equals(Constants.UserRole.METADATA_ADMIN)) {
                flag = true;
            }
        }
        if (flag) {
            return getInvitedUsersByCrowdManager(userInvitations);
        }
        return userInvitations;
    }

    /**
     * Method to get total pages for pagination functionality to show users in
     * Manage User section.
     *
     * @param roleId
     * @param pageWindow
     * @return long -total page count
     */
    public long getTotalPagesByRoleId(long roleId, int pageWindow) {
        long totalUsers = userDAO.getTotalUserCountByRoleId(roleId);
        double totalPages = (double) totalUsers / pageWindow;
        return (long) Math.ceil(totalPages);
    }

    /**
     * Method to get total pages for pagination functionality to show users in
     * Invited User section.
     *
     * @param pageWindow
     * @return long -total page count
     */
    public Long getInvitedUserCount(int pageWindow) {
        long totalUsers = userInvitationDAO.getInvitedUserCount();
        double totalPages = (double) totalUsers / pageWindow;
        return (long) Math.ceil(totalPages);
    }

    public long getInvitedUserCountByFilters(String searchString) throws Exception {
        return userInvitationDAO.getInvitedUserCountByFilters(searchString);
    }

    /**
     * Method to service creation of new Role. return boolean TRUE if
     * successfully created and FALSE for failed transaction
     *
     * @param role new role to be persisted.
     * @return boolean
     */
    public boolean createNewRole(Role role) {
        return userRoleDAO.createNew(role);
    }

    /**
     * Method to serve lambda expression to filter out roles according to passed
     * code as value.
     *
     * @param roles -of all available roles
     * @param p -implementation of Predicate interface
     * @return List<Role> -filtered roles after applying test method of
     * Predicate.
     */
    public static List<Role> filter(List<Role> roles, Predicate<Role> p) {
        List<Role> result = new ArrayList<>();
        for (Role role : roles) {
            if (p.test(role)) {
                result.add(role);
            }
        }
        return result;
    }

    /**
     * Service method to fetch roles. If Logged in user is Crowd Manager then
     * list only lib_user,Curator and general user roles.For admin list all
     * available roles.
     *
     * @return
     */
    public List<RoleDTO> listRoles() {
        List<RoleDTO> roleList = new ArrayList<>();
        List<Role> roles = userRoleDAO.listRoles();
        if (getCurrentUser().getRoles().parallelStream().filter((Role r) -> r.getCode().equals(Constants.UserRole.METADATA_ADMIN)).findFirst().orElse(null) != null) {
            roles = roles.parallelStream().filter((Role role) -> role.getCode().equals("LIB_USER") || role.getCode().equals("CURATOR_USER") || role.getCode().equals("GENEREAL_USER")).collect(Collectors.toList());
        }
        if (getCurrentUser().getRoles().parallelStream().filter((Role r) -> r.getCode().equals(Constants.UserRole.ADMIN_USER)).findFirst().orElse(null) != null) {
            roles = userRoleDAO.listRoles();
        }
        roles.stream().
                forEach(role -> roleList.add(new RoleDTO(role.getRoleId(), role.getName(), role.getDescription(), role.getCode(), role.getUsers().size()))
                );
        return roleList;
    }

    public List<Long> listRoleIds() {
        List<Role> roles = userRoleDAO.listRoles();
        List<Long> customRoles = new ArrayList<>();
        roles.parallelStream().forEach((role) -> {
            customRoles.add(role.getRoleId());
        });
        LOG.info("listRoleIds" + customRoles.size()
        );
        return customRoles;
    }

    /**
     * Service method to list roles of particular user.
     *
     * @param userId id of the user whoes roles to be fetched.
     * @return List<Long>
     */
    public List<Long> listRoleIdsOfUser(Long userId) {
        User userObj = userDAO.get(userId);
        Set<Role> roles = userObj.getRoles();
        List<Long> customRoles = new ArrayList<>();
        roles.parallelStream().forEach((role) -> {
            customRoles.add(role.getRoleId());
        });
        return customRoles;
    }

    /**
     * Service method to list role id and name of role of particular user.
     *
     * @param userId id of the user whoes roles to be fetched.
     * @return List<Long,String>
     */
    public Map<Long, String> mapOfRoleIdsNameOfUser(Long userId) {
        User userObj = userDAO.get(userId);
        Set<Role> roles = userObj.getRoles();
        Map<Long, String> customRoles = new HashMap<>();
        roles.parallelStream().forEach((role) -> {
            customRoles.put(role.getRoleId(), role.getName());
        });
        return customRoles;
    }

    /**
     * To get a map of all role ids and role name based on current logged in
     * user role
     *
     * @param user
     * @return Map<Long, String>
     */
    public Map<Long, String> filterInvitedUsersByRoles(User user) {
        List<Role> roles = userRoleDAO.listRoles();
        boolean flag = false;
        boolean isAdminUser = false;
        for (Role role : user.getRoles()) {
            if (role.getCode().equals(Constants.UserRole.METADATA_ADMIN)) {
                flag = true;
            }
            if (role.getCode().equals(Constants.UserRole.ADMIN_USER)) {
                isAdminUser = true;
            }
        }
        if (flag == true && isAdminUser == false) {
            roles = filter(roles, (Role role) -> role.getCode().equals("LIB_USER") | role.getCode().equals("CURATOR_USER"));
        }
        Map<Long, String> customRoles = null;
        customRoles = new HashMap<Long, String>();
        for (Role role : roles) {
            customRoles.put(role.getRoleId(), role.getName());
        }
        return customRoles;
    }

    public List<Role> getAllRoles() {
        return userRoleDAO.list();
    }

    /**
     * Service method to delete role.
     *
     * @param roleId id of role to be deleted.
     */
    public void deleteUserRole(long roleId) {
        Role role = userRoleDAO.get(roleId);
        userRoleDAO.delete(role);
    }

    /**
     * Service method to fetch role details.
     *
     * @param roleId id of role to be fetched.
     * @return {@link Role}
     */
    public Role getRoleByID(long roleId) {
        return userRoleDAO.get(roleId);
    }

    public boolean emailExist(String email) {
        User user = userDAO.findByEmail(email);
        return user != null;
    }

    /**
     * This method is used to get user by email
     *
     * @param email
     * @return User object
     */
    public User getUserByEmailID(String email) {
        return userDAO.findByEmail(email);
    }

    public User getUserBySalt(String salt) {
        return userDAO.findBySalt(salt);
    }

    /**
     * Checks if mail id has been registered. if yes then generate a random 4
     * digit code, save that in db for that user ,send mail to that email id
     * with that code else return a message that email not found.
     *
     * @param emailId email id entered by user
     * @param ctx application context for retreiving resourcebundle message
     * @param srvltCtxName
     * @return message for user specifying result of passcode match
     */
    public JSONObject sendResetPasswordMail(String emailId, ApplicationContext ctx, String servletCtxName) {
        System.out.println("emailId ::" + emailId);
        JSONObject responseJson = new JSONObject();
        if (emailId != null && !emailId.isEmpty()) {
            User existingUser = getUserByEmailID(emailId);
            if (existingUser != null) {
                int randomPin = (int) (Math.random() * 9000) + 1000;
                String pinString = String.valueOf(randomPin);
                existingUser.setForgottenPasswordCode(pinString);
                existingUser.setForgottenPasswordTime(new Date());
                userDAO.update(existingUser);
                if (existingUser.getForgottenPasswordCode().equals(pinString)) {//update successful
                    try {
                        String resetPasswordUrl = "auth/redirect-to-reset-password";
                        String message = ctx.getMessage("reset.password.message", new Object[]{pinString}, Locale.getDefault());
                        String subject = ctx.getMessage("reset.password.subject", null, Locale.getDefault());
                        RabbitmqProducer producer = new RabbitmqProducer();
                        JSONObject detailsJson = new JSONObject();
                        detailsJson.put("firstName", existingUser.getFirstName());
                        detailsJson.put("salt", existingUser.getSalt());
                        detailsJson.put("servletCtxName", servletCtxName);
                        detailsJson.put("userMailId", existingUser.getEmail());
                        detailsJson.put("message", message);
                        detailsJson.put("controllerMapping", resetPasswordUrl);
                        detailsJson.put("subject", subject);
                        producer.sendEmailDetailsToQueue(amqpTemplate, detailsJson, Constants.RabbitMq.EMAIL_RECOVERY_KEY);
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                        e.printStackTrace();
                    }
                    responseJson.put("success", ctx.getMessage("reset.password.mail.success", null, Locale.getDefault()));
                }
            } else {
                responseJson.put("error", ctx.getMessage("reset.password.email.not.found", null, Locale.getDefault()));
            }

        } else {
            responseJson.put("error", ctx.getMessage("reset.password.mail.empty", null, Locale.getDefault()));
        }
        return responseJson;
    }

    public boolean matchPasscode(String passcode, String emailId) {
        if (passcode != null && !passcode.isEmpty()) {
            if (emailId != null && !emailId.isEmpty()) {
                User usrObj = userDAO.getUserByMailAndPasscode(emailId, passcode);
                if (usrObj != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public User findUserByUsername(String username) {
        return userDAO.findUserByUsername(username);
    }

    public boolean usernameExists(String username) {
        return userDAO.existsUsername(username);
    }

    public boolean useremailExists(String useremail) {
        return userDAO.existsUseremail(useremail);
    }

    /**
     * Service method to fetch user.
     *
     * @param userId id of user which is to be fetched.
     * @return {@link User}
     */
    public User getUserById(Long userId) {
        User user = null;
        user = userDAO.get(userId);
        return user;
    }

    public User updateUserByAdmin(UserDetailsBean userObj, String[] selectedRoleIds) {
        User userObjToUpdate = null;
        try {
            userObjToUpdate = getUserById(userObj.getId());
            userObjToUpdate.setFirstName(userObj.getFirstName());
            userObjToUpdate.setMiddleName(userObj.getMiddleName());
            userObjToUpdate.setLastName(userObj.getLastName());
            userObjToUpdate.setUsername(userObj.getUsername());
            userObjToUpdate.setGender(userObj.getGender());
            userObjToUpdate.setEmail(userObj.getEmail());
            userObjToUpdate.setDistrict(userObj.getDistrict());
            //setting user roles
            Set<Role> selectedRoles = new HashSet<Role>();
            String senderName = messages.getMessage("nvli.default.username", new String[]{}, Locale.getDefault());
            if (null != this.getCurrentUser().getFirstName() || null != this.getCurrentUser().getLastName()) {
                senderName = this.getCurrentUser().getFirstName() + " " + this.getCurrentUser().getLastName();
            }

            for (String roleId : selectedRoleIds) {
                Role role = getRoleByID(Long.parseLong(roleId));
                selectedRoles.add(role);
                if (!userObjToUpdate.getRoles().contains(role)) {
                    Notifications notif = new Notifications();
                    notif.setNotifactionDateTime(new Date());
                    notif.setNotificationMessage(messages.getMessage("notif.role.granted", new String[]{senderName, role.getName()}, Locale.getDefault()));
                    notif.setSenderId(userObj.getId());
                    notif.setNotificationActivityType(Constants.UserActivityConstant.ADMIN_ROLE);

                    notif.setNotificationTargetURL("/user/profile?ref=notif#a-user-profile");
                    List<Long> receivers = new ArrayList<Long>();
                    receivers.add(userObjToUpdate.getId());
                    notificationService.saveNotification(notif, receivers);
                }
            }
            for (Role role : userObjToUpdate.getRoles()) {
                if (!selectedRoles.contains(role)) {
                    Notifications notif = new Notifications();
                    notif.setNotifactionDateTime(new Date());
                    notif.setNotificationMessage(messages.getMessage("notif.role.revoked", new String[]{senderName, role.getName()}, Locale.getDefault()));
                    notif.setSenderId(userObj.getId());
                    notif.setNotificationTargetURL("/user/profile?ref=notif#a-user-profile");
                    List<Long> receivers = new ArrayList<Long>();
                    receivers.add(userObjToUpdate.getId());
                    notificationService.saveNotification(notif, receivers);
                }
            }
            userObjToUpdate.setRoles(selectedRoles);
            userObjToUpdate.setCity(userObj.getCity());
            userObjToUpdate.setState(userObj.getState());
            userObjToUpdate.setCountry(userObj.getCountry());
            userObjToUpdate.setContact(userObj.getContact());

            updateUser(userObjToUpdate);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return userObjToUpdate;
    }

    public User updateUserProfile(UserDetailsBean userObj) {
        User userObjToUpdate = null;
        try {
            userObjToUpdate = new User();
            userObjToUpdate = getUserById(userObj.getId());
            userObjToUpdate.setFirstName(userObj.getFirstName());
            userObjToUpdate.setMiddleName(userObj.getMiddleName());
            userObjToUpdate.setLastName(userObj.getLastName());
            userObjToUpdate.setGender(userObj.getGender());
            userObjToUpdate.setDistrict(userObj.getDistrict());
            userObjToUpdate.setAddress(userObj.getAddress());
            userObjToUpdate.setCity(userObj.getCity());
            userObjToUpdate.setState(userObj.getState());
            userObjToUpdate.setCountry(userObj.getCountry());
            userObjToUpdate.setProfilePicPath(userObj.getProfilePicPath());
//            userObjToUpdate.setUserPhoto(userObj.getProfileImage().getBytes());
            updateUser(userObjToUpdate);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage(), ex);
        }
        return userObjToUpdate;
    }

    /**
     * This method is used to send invitation using email address.
     *
     * @param activityId
     * @param uniqueIdentifier
     * @param loggedUser
     * @param affectedUserId
     * @param selected_roleid
     * @param inivtee_email
     * @param ctx
     * @param requestObj
     * @param srvltCtxtName
     * @param isUpdate
     * @param invitationId
     * @throws Exception
     */
    @Transactional
    public void inviteUser(int activityId, String uniqueIdentifier, User loggedUser, Long affectedUserId, List<Long> selected_roleid, String inivtee_email, ApplicationContext ctx, HttpServletRequest request, String srvltCtxtName, boolean isUpdate, Long invitationId) throws Exception {
        String usrRoleNames = "";
        if (selected_roleid != null && (inivtee_email != null || !inivtee_email.isEmpty())) {
            List<Role> grantedRoles = new ArrayList<>();
            for (Long roleId : selected_roleid) {
                Role role = userRoleDAO.get(roleId);
                usrRoleNames = usrRoleNames + role.getName() + " ,";
                grantedRoles.add(role);
            }
            if (usrRoleNames.endsWith(",")) {
                usrRoleNames = usrRoleNames.substring(0, usrRoleNames.length() - 1);
            }
            UserActivityLog actLogObject = null;
            //Uncomment code for saving user activity log
            //UserActivityLog actLogObject = userActivityService.saveUserActivityLog(activityId, uniqueIdentifier, loggedUser, affectedUserId, requestObj);

//            if (actLogObject == null) {
//                LOG.error("Error saving activity log");
//            }
            if (isUpdate) {
                UserInvitation oldInvitationObj = userInvitationDAO.getInvitaionObjectByInvitationId(invitationId);
                oldInvitationObj.setGrantedRoles(grantedRoles);
                updateInvitation(oldInvitationObj);
            } else if (!userInvitationDAO.checkInvitationAlreadySent(inivtee_email)) {
                UserInvitation newInvitationObj = new UserInvitation();
                newInvitationObj.setInviteeEmail(inivtee_email);
                newInvitationObj.setInvitedBy(loggedUser);
                newInvitationObj.setInvitationDate(new Date());
                newInvitationObj.setGrantedRoles(grantedRoles);
                //newInvitationObj.setUserActivityLog(actLogObject);
                boolean res = userInvitationDAO.saveInvitationObject(newInvitationObj);
                LOG.info("inivte saved " + newInvitationObj.getInviteeEmail());
                //if new invitation created then
                if (res) {
                    //update object with salt
                    String invitationIdSalt = EncodeDecodeUtils.getSHA256EncodedData(newInvitationObj.getInvitationId().toString());
                    newInvitationObj.setInvitationCode(invitationIdSalt);
                    updateInvitation(newInvitationObj);
                    String registerUserUrl = "auth/registration";
                    String message = ctx.getMessage("invitation.message", new Object[]{usrRoleNames}, Locale.getDefault());
                    String subject = ctx.getMessage("invitation.subject", null, Locale.getDefault());
                    RabbitmqProducer producer = new RabbitmqProducer();
                    JSONObject detailsJson = new JSONObject();
                    detailsJson.put("firstName", "Sir/Madam");
                    detailsJson.put("salt", newInvitationObj.getInvitationCode());
                    detailsJson.put("servletCtxName", srvltCtxtName);
                    detailsJson.put("userMailId", inivtee_email);
                    detailsJson.put("message", message);
                    detailsJson.put("controllerMapping", registerUserUrl);
                    detailsJson.put("subject", subject);
                    producer.sendEmailDetailsToQueue(amqpTemplate, detailsJson, Constants.RabbitMq.EMAIL_INVITATION_KEY);
                }
            }
            //activity log saving in mongoDB
            in.gov.nvli.mongodb.domain.UserInvitation mongoInvitation = new in.gov.nvli.mongodb.domain.UserInvitation();
            mongoInvitation.setActivityTs(new Date());
            mongoInvitation.setInviteeEmail(inivtee_email);
            mongoInvitation.setGrantedRoles(usrRoleNames);
            userActivityService.saveActivityLog(uniqueIdentifier, loggedUser.getId(),
                    Constants.UserActivityConstant.SEND_INVITATION, mongoInvitation,
                    request, loggedUser.getUsername(), null, Constants.FilterActivityLogs.AllActivities, null);
        } else {
            throw new Exception("Roles or invitee email is not available.");
        }
    }

    public void sentReminder(long invitationId, User loggedInUser, String inivtee_email, ApplicationContext ctx, RequestAttributes requestObj, String srvltCtxtName) throws UnsupportedEncodingException, NoSuchAlgorithmException, Exception {
        UserInvitation oldInvitationObj = userInvitationDAO.getInvitaionObjectByInvitationId(invitationId);
        String invitationIdSalt = EncodeDecodeUtils.getSHA256EncodedData(oldInvitationObj.getInvitationId().toString());
        oldInvitationObj.setInvitationCode(invitationIdSalt);
        oldInvitationObj.setInvitedBy(loggedInUser);
        oldInvitationObj.setInviteeEmail(inivtee_email);
        updateInvitation(oldInvitationObj);
        String usrRoleNames = "";
        List<Role> selected_role = oldInvitationObj.getGrantedRoles();

        for (Role role : selected_role) {
            usrRoleNames = usrRoleNames + role.getName() + " ,";
        }
        if (usrRoleNames.endsWith(",")) {
            usrRoleNames = usrRoleNames.substring(0, usrRoleNames.length() - 1);
        }

        String registerUserUrl = "auth/registration";
        String message = ctx.getMessage("invitation.message", new Object[]{usrRoleNames}, Locale.getDefault());
        String subject = ctx.getMessage("invitation.subject", null, Locale.getDefault());
        RabbitmqProducer producer = new RabbitmqProducer();
        JSONObject detailsJson = new JSONObject();
        detailsJson.put("firstName", "Sir/Madam");
        detailsJson.put("salt", oldInvitationObj.getInvitationCode());
        detailsJson.put("servletCtxName", srvltCtxtName);
        detailsJson.put("userMailId", inivtee_email);
        detailsJson.put("message", message);
        detailsJson.put("controllerMapping", registerUserUrl);
        detailsJson.put("subject", subject);
        producer.sendEmailDetailsToQueue(amqpTemplate, detailsJson, Constants.RabbitMq.EMAIL_INVITATION_KEY);
    }

    public boolean checkEmailIFAlredyExist(String useremail) {

        List<UserInvitation> invitaionObjectByEmailId = userInvitationDAO.getInvitaionObjectByEmailId(useremail);
        return invitaionObjectByEmailId.isEmpty();

    }

    public UserInvitation getUserInvitaionByHash(String idHash) {
        UserInvitation invObj = null;
        invObj = userInvitationDAO.getInvitationObjectByHash(idHash);
        return invObj;
    }

    /**
     * Image will be created with name same as userid and saved in the parent
     * directory
     *
     * @param absoluteFilePath
     * @param userImage image uploaded by user
     * @throws IOException
     */
    public void saveExistingOrNewlyUploadedImageOnDrive(String absoluteFilePath, MultipartFile userImage) {
        FileOutputStream fos = null;
        try {
            File file = new File(absoluteFilePath);
            LOG.info("file name is " + file.getName() + " parent is " + file.getParent());

            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            fos = new FileOutputStream(file);
            fos.write(userImage.getBytes());//getting bytes from multipartfile
            fos.close();
        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
        } finally {
            IOUtils.closeQuietly(fos);
        }
    }

    public void saveProfileImageGotFromSocialOnDrive(String absoluteFilePath, BufferedImage image) throws IOException {
        try {
            File file = new File(absoluteFilePath);
            LOG.info("file name is " + file.getName() + " parent is " + file.getParent());

            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            if (file.exists()) {
                file.delete();
            }
            ImageIO.write(image, "JPG", file);
//            file.createNewFile();
//            FileOutputStream fos = new FileOutputStream(file);
//            fos.write(userImage);//getting bytes from multipartfile
//            fos.close();
        } catch (IOException e) {
            throw e;
        }
    }

    public void validateImage(MultipartFile image) throws Exception {
        if (!(image.getContentType().equals("image/jpeg") || image.getContentType().equals("image/png"))) {
            throw new Exception("Only JPG/PNG images are accepted");
        }
    }

    public String getFileExtension(String fileName) {
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }

    public boolean isAuthenticated() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ((auth instanceof AnonymousAuthenticationToken)) {
            return false;
        }
        return true;
    }

    /**
     * Service method to fetch currently logged in user.If no user is logged in
     * return null.
     *
     * @return {@link User}
     */
    public User getCurrentUser() {
        if (isAuthenticated()) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            UserDetails userDetails = (UserDetails) auth.getPrincipal();
            return this.getUserByEmailID(userDetails.getUsername());
        }
        return null;
    }

    /**
     * The method getFullNameById is used to get the full name of user
     *
     * @param userId
     * @return fullName
     */
    public String getFullNameById(long userId) {
        User user = getUserById(userId);
        String fullName = "";
        if (user != null) {
            String firstName = user.getFirstName();
            String middlename = user.getMiddleName();
            String lastName = user.getLastName();

            if (middlename == null || middlename.equalsIgnoreCase("")) {
                if (lastName == null || lastName.equalsIgnoreCase("")) {
                    fullName = firstName;
                } else {
                    fullName = firstName + " " + lastName;
                }
            } else if (lastName == null || lastName.equalsIgnoreCase("")) {
                fullName = firstName + " " + middlename;
            } else {
                fullName = firstName + " " + middlename + " " + lastName;
            }
        }
        return fullName;
    }

    public boolean createUserInterest(UserInterest interest) {
        return userInterestDAO.createNew(interest);
    }

    public UserInterest getUserInterestByID(long id) {
        return userInterestDAO.get(id);
    }

    public UserInterest getUserInterest(long thematicTypeID) {
        List<UserInterest> interests = this.getCurrentUser().getInterests();
        Collections.sort(interests, new UserInterestComparatorByID());
        UserInterest interestToReturn = new UserInterest();
        for (UserInterest interest : interests) {
            if (interest.getThematicType().getId() == thematicTypeID) {
                interestToReturn = interest;
                break;
            }
        }
        return interestToReturn;
    }

    public void saveUpdateUserInterest(UserInterest interest) {
        userInterestDAO.saveOrUpdate(interest);
    }

    @Transactional
    public void removeUserInterest(UserInterest interest) {
        userInterestDAO.delete(interest);
    }

    public List<CustomUsersLists> getInvitedUsers(long invitingUser) {
        List<CustomUsersLists> cList = new ArrayList<>();
        List<UserInvitation> iList = userInvitationDAO.getInvitationsSentByUser(invitingUser);

        for (UserInvitation obj : iList) {
            String usrRoleNames = "";
            List<Role> invitedRoles = null;
            CustomUsersLists customObj = new CustomUsersLists();
            customObj.setInvitationId(obj.getInvitationId());
            customObj.setInvitationDate(obj.getInvitationDate().toString().substring(0, obj.getInvitationDate().toString().lastIndexOf(".")));
            customObj.setUserMail(obj.getInviteeEmail());
            if (obj.getInvitationAccepted() == Constants.INVITE_ACCEPTED) {
                customObj.setUserStatus(Constants.INVITE_ACCEPTED_STR);
            } else if (obj.getInvitationAccepted() == Constants.INVITE_NOT_ACCEPTED) {
                customObj.setUserStatus(Constants.INVITE_NOT_ACCEPTED_STR);
            }
            invitedRoles = obj.getGrantedRoles();
            List<JSONObject> roleList = new LinkedList<>();
            for (Role role : invitedRoles) {
                JSONObject object = new JSONObject();
                object.put("roleId", role.getRoleId());
                object.put("roleName", role.getName());
                object.put("roleDescription", role.getDescription());
//                usrRoleNames = usrRoleNames + role.getName() + " ,";
                roleList.add(object);
            }

            customObj.setUserRoles(roleList);
            cList.add(customObj);
        }
        return cList;
    }

    /**
     *
     * @return
     */
    public List<socialSettingDTO> socialSettingService() {

        List<socialSettingDTO> socialSettingList = null;
        socialSettingList = new ArrayList<socialSettingDTO>();
        String email = this.getCurrentUser().getEmail();
        System.out.println("Userid=" + email);
        List<UserConnection> userconlist = useerconnectionDAO.getUserConnection(email);
        if (null != userconlist) {
            for (UserConnection userCon : userconlist) {
                if ((userCon.getProviderId().equalsIgnoreCase("facebook")) && (userCon.getProviderUserId() != null)) {
                    socialSettingDTO socialSetting = new socialSettingDTO();
                    System.out.println("provider id" + userCon.getProviderId());
                    System.out.println("user provider id" + userCon.getProviderUserId());
                    socialSetting.setProviderUserId(userCon.getProviderUserId());
                    socialSetting.setProviderId(userCon.getProviderId());
                    socialSetting.setConnectedStatus(true);
                    socialSettingList.add(socialSetting);
                    System.out.println(" facebook connected");

                } else if ((userCon.getProviderId().equalsIgnoreCase("twitter")) && (userCon.getProviderUserId() != null)) {

                    socialSettingDTO socialSetting = new socialSettingDTO();
                    System.out.println("provider id" + userCon.getProviderId());
                    System.out.println("user provider id" + userCon.getProviderUserId());
                    socialSetting.setProviderUserId(userCon.getProviderUserId());
                    socialSetting.setProviderId(userCon.getProviderId());
                    socialSetting.setConnectedStatus(true);
                    socialSettingList.add(socialSetting);
                    System.out.println(" twitter connected");

                } else if ((userCon.getProviderId().equalsIgnoreCase("linkedin")) && (userCon.getProviderUserId() != null)) {
                    socialSettingDTO socialSetting = new socialSettingDTO();
                    System.out.println("provider id" + userCon.getProviderId());
                    System.out.println("user provider id" + userCon.getProviderUserId());
                    socialSetting.setProviderUserId(userCon.getProviderUserId());
                    socialSetting.setProviderId(userCon.getProviderId());
                    socialSetting.setConnectedStatus(true);
                    socialSettingList.add(socialSetting);
                    System.out.println("linkein connected");

                } else if ((userCon.getProviderId().equalsIgnoreCase("github")) && (userCon.getProviderUserId() != null)) {

                    socialSettingDTO socialSetting = new socialSettingDTO();
                    System.out.println("provider id" + userCon.getProviderId());
                    System.out.println("user provider id" + userCon.getProviderUserId());
                    socialSetting.setProviderUserId(userCon.getProviderUserId());
                    socialSetting.setProviderId(userCon.getProviderId());
                    //anArray[0]=1;
                    socialSetting.setConnectedStatus(true);
                    socialSettingList.add(socialSetting);
                    System.out.println("github connected");

                } else if ((userCon.getProviderId().equalsIgnoreCase("google")) && (userCon.getProviderUserId() != null)) {
                    socialSettingDTO socialSetting = new socialSettingDTO();
                    System.out.println("provider id" + userCon.getProviderId());
                    System.out.println("user provider id" + userCon.getProviderUserId());
                    socialSetting.setProviderUserId(userCon.getProviderUserId());
                    socialSetting.setProviderId(userCon.getProviderId());
                    socialSetting.setConnectedStatus(true);
                    socialSettingList.add(socialSetting);
                    System.out.println("google connected");

                }
            }
            return socialSettingList;
        }
        return socialSettingList;

    }

    public boolean hasLanguageExpertise(User user) {
        return this.getCurrentUser().getUserLanguages().size() > 0;
    }

    public boolean hasResourceExpertise(User user) {
        return this.getCurrentUser().getResourceInterest().size() > 0;
    }

    public Object getRoleByID(String roleId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<String> suggestedUsername(String username, String firstName, String lastName) {
        List<String> suggestedUsernameList = new ArrayList<String>();
        String usernamesuggest = "";
        for (int i = 0; i <= 1; i++) {
            String usernamealpabhetic = firstName + lastName;
            if (usernamealpabhetic.length() >= 10) {
                usernamealpabhetic = usernamealpabhetic.substring(0, 6);
            }
            String random = randomNumeric(10 - (usernamealpabhetic.length()));
            String usernameSuggested = usernamealpabhetic + random;
            if (!(this.usernameExists(usernameSuggested))) {
                suggestedUsernameList.add(usernameSuggested);
            }
        }
        for (int i = 0; i <= 1; i++) {
            String usernamealpabhetic = lastName + firstName;
            if (usernamealpabhetic.length() >= 10) {
                usernamealpabhetic = usernamealpabhetic.substring(0, 6);
            }
            String random = randomNumeric(10 - (usernamealpabhetic.length()));
            String usernameSuggested = usernamealpabhetic + random;
            if (!(this.usernameExists(usernameSuggested))) {
                suggestedUsernameList.add(usernameSuggested);
            }
        }
        return suggestedUsernameList;
    }

    public List<Long> getUserIdsByAssignedResource(String resourceCode, String userRole) throws Exception {
        return userDAO.getUserIdsByAssignedResource(resourceCode, userRole);
    }

    public List<UserInvitation> getInvitedUsersByInvitaionId(long invitaionId, int pageNumber, int pageWindow, String sortBy, String sortOrder) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getRoleByInvitaionId(long invitaionId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void cancelInvitedUser(Long invitationId, HttpServletRequest request, Long userId, String username) {
        UserInvitation oldInvitationObj = userInvitationDAO.getInvitaionObjectByInvitationId(invitationId);
        oldInvitationObj.setInvitationAccepted(Constants.INVITE_CANCELLED);
        String grantedRoleNames = "";
        String inviteeEmail = oldInvitationObj.getInviteeEmail();
        List<Role> roles = oldInvitationObj.getGrantedRoles();
        for (Role granted : roles) {
            grantedRoleNames = grantedRoleNames + granted.getName() + " ,";
        }
        if (grantedRoleNames.endsWith(",")) {
            grantedRoleNames = grantedRoleNames.substring(0, grantedRoleNames.length() - 1);
        }
        updateInvitation(oldInvitationObj);
        in.gov.nvli.mongodb.domain.UserInvitation cancelInvitation = new in.gov.nvli.mongodb.domain.UserInvitation();
        cancelInvitation.setInviteeEmail(inviteeEmail);
        cancelInvitation.setGrantedRoles(grantedRoleNames);
        cancelInvitation.setActivityTs(new Date());
        userActivityService.saveActivityLog(null, userId, Constants.UserActivityConstant.CANCEL_INVITATION,
                cancelInvitation, request, username, null, Constants.FilterActivityLogs.AllActivities, null);

    }

    /**
     * get all countries from the DAO
     *
     * @return {@link List} list of all countries
     */
    public List<Country> getAllCountries() {
        return userDAO.getAllCountries();

    }

    /**
     * get all states from the DAO
     *
     * @param countyId that uses to find all states
     * @return {@link List} list of all states
     */
    public List<States> getAllStates(long countyId) {
        return userDAO.getStateByCountry(countyId);

    }

    /**
     * get all district from the DAO
     *
     * @param stateId that uses to find all district
     * @return {@link List} List of all district
     */
    public List<District> getAllDistrict(long stateId) {
        return userDAO.getDistrictByState(stateId);
    }

    /**
     * get all cities from the DAO
     *
     * @param districtId that uses to find all district
     * @return {@link List} list of all cities
     */
    public List<Cities> getAllCities(long districtId) {
        return userDAO.getCityByDistrict(districtId);
    }

    public boolean hasRole(Long userId, String roleCode) throws Exception {
        return userDAO.get(userId)
                .getRoles()
                .stream()
                .anyMatch(r -> r.getCode().equals(roleCode));
    }

    /**
     * get feedback list from the DAO
     *
     * @return {@link List} list of all feedback
     */
    public List<DeactivationFeedbackBase> getFeedbackList() {
        return userDAO.getFeedbackList();

    }

    public void saveUserFeedback(UserFeedback userfeedback) {
        feedbackDAO.saveOrUpdate(userfeedback);
    }

    public void saveUserOtherFeedback(UserOtherFeedback userOtherfeedback) {
        otherFeedbackDAO.saveOrUpdate(userOtherfeedback);
    }

    public DeactivationFeedbackBase getFeedbackBaseByFID(Long feedbackId) {
        return deactivationFeedbackBaseDAO.get(feedbackId);

    }

    public List<UserUploadedFile> fetchUserUploadedFiles(User user) {
        return uploadedFilesLogDAO.fetchByUID(user.getId());
    }

    public Role getUsersByRoleId(long roleId) {
        Role role = userRoleDAO.get(roleId);
        return role;
    }

    public boolean saveUserFeedback(FeedbackForm feedbackForm) {
        boolean success = false;
        try {
            userFeedbackDetailsDAO.saveOrUpdate(feedbackForm);
            success = true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return success;
    }

    public int checkSecretKey(String secretKey) {
        HashMap<String, String> map = new HashMap<String, String>();
        try (BufferedReader br = new BufferedReader(new FileReader(secretKeyFilePath))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] parts = sCurrentLine.split("=", 2);
                if (parts.length == 2) {
                    String key = parts[0];
                    String value = parts[1];
                    map.put(key, value);
                }
            }
            if (map.containsKey(secretKey)) {
                Long count = userDAO.findUserCount(secretKey);
                count = count + 1;
                String maxcount = map.get(secretKey);
                if (count <= Long.parseLong(maxcount)) {
                    return 1;
                } else {
                    return -1;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void insertSecretKeyDetails(String secretKey, Long userId) {
        SecretKeyDetails details = new SecretKeyDetails();
        details.setSecretKey(secretKey);
        details.setUser(getUserById(userId));
        secretKeyDetailsDAO.createNew(details);
    }

    public long getUsersTotalCountByFilters(String searchString) throws Exception {
        return secretKeyDetailsDAO.getUsersTotalCountByFilters(searchString);

    }

    public List<SecretKeyDTO> getUsersByFiltersWithLimit(String searchString, int dataLimit, int pageNo) throws Exception {
        secretKeyDetailsDAO.getUsersByFiltersWithLimit(searchString, dataLimit, pageNo);
        List<SecretKeyDTO> detailsList = new ArrayList<>();
        for (SecretKeyDetails secretKeyDetails : secretKeyDetailsDAO.getUsersByFiltersWithLimit(searchString, dataLimit, pageNo)) {
            SecretKeyDTO secretKeyDTO = new SecretKeyDTO();
            secretKeyDTO.setSecretKeyId(secretKeyDetails.getSecretKeyId());
            secretKeyDTO.setSecretKey(secretKeyDetails.getSecretKey());
            secretKeyDTO.setName(secretKeyDetails.getUser().getFirstName() + " " + secretKeyDetails.getUser().getLastName());
            detailsList.add(secretKeyDTO);
        }
        return detailsList;
    }

    public boolean deleteUserInvitaion(long invitaionId) {
        UserInvitation ui = userInvitationDAO.get(invitaionId);
        try {
            userInvitationDAO.delete(ui);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
