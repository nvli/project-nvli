package in.gov.nvli.service;

import in.gov.nvli.controller.NewsArticleController;
import in.gov.nvli.dao.user.NewsPaperDAO;
import in.gov.nvli.domain.user.NewsPaper;
import in.gov.nvli.webserviceclient.mit.QuestionAnswerForm;
import in.gov.nvli.webserviceclient.news.NewsArticleWebserviceHelper;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ritesh
 */
@Service
public class NewsArticleService {
    
    private static final Logger LOG = Logger.getLogger(NewsArticleController.class);
    @Autowired
    private NewsPaperDAO newsPaperDAO;
    
    @Autowired
    private NewsArticleWebserviceHelper newsArticleWebserviceHelper;
    
    public List<NewsPaper> getNewsPapers(String paperLang, boolean newsType) {
        List<NewsPaper> newsPapers = newsPaperDAO.getNewsPapers(paperLang, newsType);
        return newsPapers;
    }
    
    public NewsPaper getNewsPaper(String paperCode) {
        return newsPaperDAO.get(paperCode);
    }

    public JSONObject fetchAnswers(String question) {
        try {
            QuestionAnswerForm questionAnswerForm = newsArticleWebserviceHelper.fetchAnswers(question);
            HashMap<String,List<String>> answer = questionAnswerForm.getAnswers();
            if( answer!=null && !answer.isEmpty()){
                JSONObject answerJSONObject = new JSONObject();
                answerJSONObject.put("answer", answer.get("answer"));
                answerJSONObject.put("related", answer.get("related"));
                return answerJSONObject;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex.getMessage(), ex);            
        }
        return null;
    }
}
