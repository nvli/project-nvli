package in.gov.nvli.service;

import in.gov.nvli.dao.crowdsource.ILanguageDAO;
import in.gov.nvli.dao.crowdsource.IMarc21TagStructureDetailDAO;
import in.gov.nvli.dao.crowdsource.IMarc21TagSubfieldStructureDetailDAO;
import in.gov.nvli.dao.crowdsource.IUDCConceptDAO;
import in.gov.nvli.dao.crowdsource.IUDCConceptDescriptionDAO;
import in.gov.nvli.dao.crowdsource.IUDCTermDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import in.gov.nvli.dao.dataimport.IDataImportDAO;
import in.gov.nvli.dao.user.UserProfessionDAO;
import in.gov.nvli.dao.user.UserThemeDAO;

/**
 *
 * @author Administrator
 */
@Service
public class DataImportService {

    @Autowired
    private IDataImportDAO dataImportDAO;

    @Autowired
    private IUDCConceptDAO udcConceptDAO;

    @Autowired
    private IUDCConceptDescriptionDAO udcConceptDescriptionDAO;

    @Autowired
    private IUDCTermDAO udcTermDAO;

    @Autowired
    private ILanguageDAO languageDAO;

    @Autowired
    private IMarc21TagStructureDetailDAO marc21TagStructureDetailDAO;

    @Autowired
    private UserProfessionDAO userProfessionDAO;

    @Autowired
    private IMarc21TagSubfieldStructureDetailDAO marc21TagSubfieldStructureDetailDAO;

    @Autowired
    private UserThemeDAO userThemeDAO;

    public boolean udcDataImportInDB() throws Exception {

        if (udcConceptDAO.list().isEmpty() || udcConceptDescriptionDAO.list().isEmpty() || languageDAO.list().isEmpty() || udcTermDAO.list().isEmpty()) {
            System.out.println("---------UDC import------------");
            return dataImportDAO.udcDataImportInDB();
        } else {
            System.out.println("Already imported");
            return false;
        }
    }

    public boolean marc21DataImportInDB() throws Exception {

        if (marc21TagStructureDetailDAO.list().isEmpty() || marc21TagSubfieldStructureDetailDAO.list().isEmpty()) {
            System.out.println("---------MARC21 import------------");
            return dataImportDAO.marc21DataImportInDB();
        } else {
            System.out.println("Already imported");
            return false;
        }
    }

    public boolean userProfessionDataInportInDB() throws Exception {

        if (userProfessionDAO.list().isEmpty() || userProfessionDAO.list().isEmpty()) {
            System.out.println("---------User Profession Data import------------");
            return dataImportDAO.userProfessionDataImportInDB();
        } else {
            System.out.println("Already imported");
            return false;
        }
    }

    public boolean themeDataInportInDB() throws Exception {

        if (userThemeDAO.list().isEmpty() || userThemeDAO.list().isEmpty()) {
            System.out.println("---------Themes Data import------------");
            return dataImportDAO.themeDataImportInDB();
        } else {
            System.out.println("Already imported");
            return false;
        }
    }
}
