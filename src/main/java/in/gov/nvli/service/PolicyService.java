/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.policy.PolicyDAO;
import in.gov.nvli.dao.policy.PolicyTypeDAO;
import in.gov.nvli.domain.policy.PolicyDocument;
import in.gov.nvli.domain.policy.PolicyType;
import in.gov.nvli.dto.PolicyTypeDTO;
import in.gov.nvli.dto.PrivacyDTO;
import in.gov.nvli.util.Helper;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Service
public class PolicyService {

    private static final Logger LOG = Logger.getLogger(PolicyService.class);

    @Autowired
    private UserService userService;
    @Autowired
    private PolicyTypeDAO policyTypeDAO;
    @Autowired
    private PolicyDAO policyDAO;

    /**
     *
     * @param content
     * @param tittle
     * @param typeId
     * @return
     */
    public boolean createPrivacyPolicy(String content, String tittle, String typeId) {
        long version = 1;
        PolicyDocument policy = new PolicyDocument(tittle,
                content,
                userService.getCurrentUser().getId(),
                userService.getCurrentUser().getId(),
                Helper.rightNow(),
                Helper.rightNow(),
                version,
                policyTypeDAO.get(Long.valueOf(typeId)));
        return policyDAO.createNew(policy);
    }

    /**
     *
     * @param content
     * @param tittle
     * @param typeId
     */
    public void updatePolicy(String content, String tittle, String typeId) {
        PolicyDocument privacy = policyDAO.findMaximumVersion(Long.parseLong(typeId));
        PolicyDocument policy = new PolicyDocument(
                tittle,
                content,
                privacy.getCreatedBy(),
                userService.getCurrentUser().getId(),
                Helper.rightNow(),
                privacy.getPublishDate(),
                privacy.getVersion() + 1,
                policyTypeDAO.get(Long.valueOf(typeId)));
        policyDAO.createNew(policy);
    }

    /**
     *
     * @param typeId
     * @return
     */
    public boolean checkPolicy(String typeId) {
        return policyDAO.checkPolicy(Long.parseLong(typeId));
    }

    /**
     *
     * @param searchString
     * @param typeId
     * @return
     * @throws Exception
     */
    public long getPoliciesTotalCountByFilters(String searchString, String typeId) throws Exception {
        return policyDAO.getPoliciesTotalCountByFilters(searchString, Long.parseLong(typeId));

    }

    /**
     *
     * @param searchString
     * @param dataLimit
     * @param pageNo
     * @param typeId
     * @return
     * @throws Exception
     */
    public List<PrivacyDTO> getPoliciesByFiltersWithLimit(String searchString, int dataLimit, int pageNo, String typeId) throws Exception {
        List<PrivacyDTO> privacyPoliciesList = new ArrayList<>();

        for (PolicyDocument privacyPolicy : policyDAO.getPoliciesByFiltersWithLimit(searchString, dataLimit, pageNo, Long.parseLong(typeId))) {
            String createdBy = userService.getUserById(privacyPolicy.getCreatedBy()).getFirstName() + " " + userService.getUserById(privacyPolicy.getCreatedBy()).getLastName();
            String updatedBy = userService.getUserById(privacyPolicy.getUpdatedBy()).getFirstName() + " " + userService.getUserById(privacyPolicy.getUpdatedBy()).getLastName();
            PrivacyDTO policy = new PrivacyDTO(
                    privacyPolicy.getPolicyId(),
                    privacyPolicy.getPolicyTittle(),
                    privacyPolicy.getPolicyDescription(),
                    createdBy,
                    updatedBy,
                    privacyPolicy.getLastModifiedDate(),
                    privacyPolicy.getPublishDate(),
                    privacyPolicy.getVersion()
            );
            privacyPoliciesList.add(policy);
        }
        return privacyPoliciesList;
    }

    /**
     *
     * @param typeId
     * @return
     */
    public PrivacyDTO findMaximumVersion(String typeId) {
        PolicyDocument policyDocument = policyDAO.findMaximumVersion(Long.parseLong(typeId));
        return new PrivacyDTO(
                policyDocument.getPolicyId(),
                policyDocument.getPolicyTittle(),
                policyDocument.getPolicyDescription(),
                String.valueOf(policyDocument.getCreatedBy()),
                String.valueOf(policyDocument.getUpdatedBy()),
                policyDocument.getLastModifiedDate(),
                policyDocument.getPublishDate(),
                policyDocument.getVersion()
        );
    }

    /**
     *
     * @param policyId
     * @return
     */
    public PrivacyDTO getPolicy(long policyId) {
        PolicyDocument policy = policyDAO.get(policyId);
        return new PrivacyDTO(
                policy.getPolicyId(),
                policy.getPolicyTittle(),
                policy.getPolicyDescription(),
                String.valueOf(policy.getCreatedBy()),
                String.valueOf(policy.getUpdatedBy()),
                policy.getLastModifiedDate(),
                policy.getPublishDate(),
                policy.getVersion()
        );
    }

    /**
     *
     * @return
     */
    public List<PolicyTypeDTO> getPolicyTypes() {
        List<PolicyTypeDTO> typeList = new ArrayList<>();
        policyTypeDAO.list().stream().map((policyType) -> new PolicyTypeDTO(
                policyType.getTypeId(),
                policyType.getPolicyName()
        )).forEach((dto) -> {
            typeList.add(dto);
        });
        return typeList;
    }

    /**
     *
     * @param id
     * @return
     */
    public PolicyTypeDTO findPolicyTypeById(long id) {
        PolicyType policyType = policyTypeDAO.get(id);
        PolicyTypeDTO type = new PolicyTypeDTO(
                policyType.getTypeId(),
                policyType.getPolicyName()
        );
        return type;
    }

}
