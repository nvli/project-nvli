/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.app.FeatureConfigurationDAO;
import in.gov.nvli.domain.app.FeatureConfig;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gulafsha Khan <gulafsha@cdac.in>
 */
@Service
public class AdminService {

    @Autowired
    private FeatureConfigurationDAO featureConfigurationDAO;

    public List<FeatureConfig> listFeatures() {
        List<FeatureConfig> featureList = featureConfigurationDAO.featuresList();
        return featureList;
    }

    public void updateFeature(Long featureId, boolean featureStatus) {
        featureConfigurationDAO.updateFeatureStatus(featureId, featureStatus);
    }

    public boolean featureNameExist(String featureName) {
        return featureConfigurationDAO.existsFeatureName(featureName);
    }

    public boolean createFeature(String featureName) {
        FeatureConfig featureConfig = new FeatureConfig();
        featureConfig.setFeatureName(featureName);
        featureConfig.setFeatureEnabledStatus(true);
        return featureConfigurationDAO.addFeature(featureConfig);
    }

    public boolean checkFeatureStatus(String featureName) {
        return featureConfigurationDAO.checkFeatureStatus(featureName);
    }

}
