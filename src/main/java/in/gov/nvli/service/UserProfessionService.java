/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.user.UserProfessionDAO;
import in.gov.nvli.domain.user.UserProfession;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Service
public class UserProfessionService {

    @Autowired
    UserProfessionDAO userProfessionDAO;

    public UserProfession findProfessionByID(long id) {
        return userProfessionDAO.get(id);
    }

    public List<UserProfession> listBaseProfessions() {
        return userProfessionDAO.listBaseProfessions();
    }

    public boolean addNew(UserProfession userProfession) {
        return userProfessionDAO.createNew(userProfession);
    }

    public UserProfession create(UserProfession userProfession) {
        Serializable id = userProfessionDAO.create(userProfession);
        return findProfessionByID(Long.parseLong(id.toString()));
    }

}
