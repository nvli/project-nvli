package in.gov.nvli.service;

import in.gov.nvli.beans.crowdsource.MetadataBean;
import in.gov.nvli.beans.crowdsource.UDCTranslationBean;
import in.gov.nvli.dao.crowdsource.ICrowdSourceArticleAttachedResourceDAO;
import in.gov.nvli.dao.crowdsource.ICrowdSourceArticleDAO;
import in.gov.nvli.dao.crowdsource.ICrowdSourceCustomTagDAO;
import in.gov.nvli.dao.crowdsource.ICrowdSourceDAO;
import in.gov.nvli.dao.crowdsource.ICrowdSourceEditDAO;
import in.gov.nvli.dao.crowdsource.ICrowdSourceUDCTagDAO;
import in.gov.nvli.dao.crowdsource.ICustomTagsDAO;
import in.gov.nvli.dao.crowdsource.ILanguageDAO;
import in.gov.nvli.dao.crowdsource.IMarc21TagSubfieldStructureDetailDAO;
import in.gov.nvli.dao.crowdsource.IUDCConceptDAO;
import in.gov.nvli.dao.crowdsource.IUDCConceptDescriptionDAO;
import in.gov.nvli.dao.crowdsource.IUDCTagVoteDAO;
import in.gov.nvli.dao.crowdsource.IUDCTagsCrowdsourceDAO;
import in.gov.nvli.dao.crowdsource.IUDCTranslationExpertDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticle;
import in.gov.nvli.domain.crowdsource.CrowdSourceArticleAttachedResource;
import in.gov.nvli.domain.crowdsource.CrowdSourceCustomTag;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.domain.crowdsource.CrowdSourceUDCTag;
import in.gov.nvli.domain.crowdsource.CustomTags;
import in.gov.nvli.domain.crowdsource.Marc21TagSubfieldStructureDetail;
import in.gov.nvli.domain.crowdsource.UDCConcept;
import in.gov.nvli.domain.crowdsource.UDCConceptDescription;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.crowdsource.UDCTagVote;
import in.gov.nvli.domain.crowdsource.UDCTagsCrowdsource;
import in.gov.nvli.domain.crowdsource.UDCTranslationExperts;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.timecodemarker.Content;
import in.gov.nvli.domain.timecodemarker.Marker;
import in.gov.nvli.domain.timecodemarker.Media;
import in.gov.nvli.domain.user.Role;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.MetadataTypeHelper;
import in.gov.nvli.util.StringComparator;
import in.gov.nvli.webserviceclient.crowdsource.CrowdsourceWebserviceHelper;
import in.gov.nvli.webserviceclient.crowdsource.MetadataStandardResponse;
import in.gov.nvli.webserviceclient.crowdsource.TagStandardResponse;
import in.gov.nvli.webserviceclient.curationactivity.CurationActivityWebserviceHelper;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.IteratorUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is used to serve all crowdsource related services.
 *
 * @author Ritesh Malviya
 * @author Doppa Srinivas
 * @author Milind Kapre
 * @author Vivek Bugale
 */
@Service
public class CrowdSourcingService {

    private final static Logger LOG = Logger.getLogger(CrowdSourcingService.class);

    @Autowired
    private ICrowdSourceDAO crowdSourceDAO;

    @Autowired
    private ICrowdSourceEditDAO crowdSourceEditDAO;

    @Autowired
    private IUDCConceptDescriptionDAO udcConceptDescriptionDAO;

    @Autowired
    private ICrowdSourceUDCTagDAO crowdSourceUDCTagDAO;

    @Autowired
    private ICrowdSourceCustomTagDAO crowdSourceCustomTagDAO;

    @Autowired
    private ILanguageDAO languageDAO;

    @Autowired
    private IUDCConceptDAO udcConceptDAO;

    @Autowired
    private ICustomTagsDAO customTagsDAO;

    @Autowired
    private ICrowdSourceArticleDAO crowdSourceArticleDAO;

    @Autowired
    private ICrowdSourceArticleAttachedResourceDAO crowdSourceArticleAttachedResourceDAO;

    @Autowired
    private IUDCTagsCrowdsourceDAO udcTagsCrowdsourceDAO;

    @Autowired
    private IUDCTagVoteDAO udcTagVoteDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private CrowdsourceWebserviceHelper crowdsourceWebserviceHelper;

    @Autowired
    private CurationActivityWebserviceHelper curationActivityWebserviceHelper;

    @Autowired
    private IResourceTypeDAO resourceTypeDAO;

    @Autowired
    private IResourceDAO resourceDAO;

//    @Autowired
//    private IResourcesWithoutLibUserDAO resourcesWithoutLibUserDAO;
    @Autowired
    private IMarc21TagSubfieldStructureDetailDAO marc21TagSubfieldStructureDetailDAO;

    @Autowired
    private IUDCTranslationExpertDAO udcTranslationExpertDAO;

    @Value("${crowdsource.edit.threshold}")
    private int crowdsourceEditThreshold;

    @Autowired
    private RewardsService rewardsService;

    @Autowired
    private UserService userService;

    /**
     * This is to crowdsource object.
     *
     * @param id
     * @return
     */
    public CrowdSource getCrowdSource(Long id) {
        return crowdSourceDAO.get(id);
    }

    /**
     * This is to get list of crowdsource objects.
     *
     * @return
     */
    public List<CrowdSource> getCrowdSourceList() {
        return crowdSourceDAO.list();
    }

    /**
     * This is to crowdsource edit object.
     *
     * @param id
     * @return
     */
    public CrowdSourceEdit getCrowdSourceEdit(Long id) {
        return crowdSourceEditDAO.get(id);
    }

    /**
     * This is to save edited version of record meta data.
     *
     * @param crowdSourceEdit
     * @return
     */
    public boolean saveCrowdSourceEdit(CrowdSourceEdit crowdSourceEdit) {
        return crowdSourceEditDAO.createNew(crowdSourceEdit);
    }

    /**
     * This is to save or update the crowdsource object.
     *
     * @param crowdSource
     */
    public void saveOrUpdateCrowdSource(CrowdSource crowdSource) {
        crowdSourceDAO.saveOrUpdate(crowdSource);
    }

    /**
     * This is to get crowdsource object by it's record identifier.
     *
     * @param recordIdentifier
     * @return
     * @throws Exception
     */
    public CrowdSource getCrowdSourceByRecordIdentifier(String recordIdentifier) throws Exception {
        return crowdSourceDAO.getCrowdSourceByRecordIdentifier(recordIdentifier);
    }

    /**
     * This is to get max edited version of crowdsource.
     *
     * @param crowdSourceName
     * @return
     * @throws Exception
     */
    public Integer getCrowdSourceMaxEditNumber(String crowdSourceName) throws Exception {
        return crowdSourceDAO.getCrowdSourceLastEditNumber(crowdSourceName);
    }

    /**
     * This is to get curated records of the user.
     *
     * @param pageNumber
     * @param pageWindow
     * @param userId
     * @return
     * @throws Exception
     */
    public List<CrowdSource> getMetadataCuratedRecordListByUserId(int pageNumber, int pageWindow, long userId) throws Exception {
        return crowdSourceDAO.getMetadataCuratedRecordListByUserId(pageNumber, pageWindow, userId);
    }

    /**
     * This is to get curated records count of the user.
     *
     * @param pageWindow
     * @param userId
     * @return
     * @throws Exception
     */
    public long getMetadataCuratedRecordTotalCountByUserId(int pageWindow, long userId) throws Exception {
        long totalRecordCount = crowdSourceDAO.getMetadataCuratedRecordTotalCountByUserId(userId);
        double totalPages = (double) totalRecordCount / pageWindow;
        return (long) Math.ceil(totalPages);
    }

    /**
     * This is used to get CrowdSourceUDCTag object by it's ID.
     *
     * @param id
     * @return
     * @throws Exception
     */
    public CrowdSourceUDCTag getCrowdSourceUDCTag(Long id) throws Exception {
        return crowdSourceUDCTagDAO.get(id);
    }

    /**
     * This is to save CrowdSourceUDCTag.
     *
     * @param crowdSourceUDCTag
     * @return
     * @throws Exception
     */
    public boolean saveCrowdSourceUDCTag(CrowdSourceUDCTag crowdSourceUDCTag) throws Exception {
        return crowdSourceUDCTagDAO.createNew(crowdSourceUDCTag);
    }

    /**
     * This is to get CrowdSourceUDCTag
     *
     * @param recordIdentifier
     * @param isApproved
     * @return
     * @throws Exception
     */
    public CrowdSourceUDCTag getCrowdSourceUDCTag(String recordIdentifier, byte isApproved) throws Exception {
        return crowdSourceUDCTagDAO.getCrowdSourceUDCTag(recordIdentifier, isApproved);
    }

    /**
     * This is to save or update of CrowdSourceUDCTag object.
     *
     * @param crowdSourceUDCTag
     * @throws Exception
     */
    public void saveOrUpdateCrowdSourceUDCTag(CrowdSourceUDCTag crowdSourceUDCTag) throws Exception {
        crowdSourceUDCTagDAO.saveOrUpdate(crowdSourceUDCTag);
    }

    /**
     * This is to save or update the CrowdSourceCustomTag.
     *
     * @param crowdSourceCustomTag
     * @param tags
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public CrowdSourceCustomTag saveOrUpdateCrowdSourceCustomTag(CrowdSourceCustomTag crowdSourceCustomTag, String tags) throws Exception {
        String[] tagsList = tags.split("&\\|&");
        tags = "";
        CustomTags customTags;
        if (crowdSourceCustomTag.getTags() != null) {
            tags = crowdSourceCustomTag.getTags();
        }
        for (String tag : tagsList) {
            if (!tags.toLowerCase().contains(tag.split("_")[0].toLowerCase())) {
                customTags = new CustomTags();
                customTags.setTagName(tag.split("_")[0]);
                customTags.setLanguageId(Long.parseLong(tag.split("_")[1]));
                if (getCustomTag(tag.split("_")[0].trim(), Long.parseLong(tag.split("_")[1])) == null) {
                    saveCustomTag(customTags);
                }
                if (tags.equalsIgnoreCase("")) {
                    tags = tag.split("_")[0];
                } else {
                    tags = tags + "&|&" + tag.split("_")[0];
                }
            }
        }
        crowdSourceCustomTag.setTags(tags);
        crowdSourceCustomTag.setIsUpdatedAtSource(Byte.valueOf("0"));
        crowdSourceCustomTag = crowdSourceCustomTagDAO.saveOrUpdateCrowdSourceCustomTag(crowdSourceCustomTag);
        try {
            TagStandardResponse tagStandardResponse = new TagStandardResponse();
            tagStandardResponse.setRecordIdentifier(crowdSourceCustomTag.getRecordIdentifier());
            tagStandardResponse.setTagType(Constants.TagType.CUSTOM);
            tagStandardResponse.setTagValue(crowdSourceCustomTag.getTags());
            if (crowdsourceWebserviceHelper.saveTagDetails(tagStandardResponse)) {
                crowdSourceCustomTag.setIsUpdatedAtSource(Byte.valueOf("1"));
            } else {
                crowdSourceCustomTag.setIsUpdatedAtSource(Byte.valueOf("0"));
            }
        } catch (Exception ex) {
            crowdSourceCustomTag.setIsUpdatedAtSource(Byte.valueOf("0"));
            ex.printStackTrace();
        }
        crowdSourceCustomTag = crowdSourceCustomTagDAO.saveOrUpdateCrowdSourceCustomTag(crowdSourceCustomTag);
        return crowdSourceCustomTag;
    }

    /**
     * This is to get CrowdSourceCustomTag
     *
     * @param recordIdentifier
     * @param user
     * @return
     * @throws Exception
     */
    public CrowdSourceCustomTag getCrowdSourceCustomTag(String recordIdentifier, User user) throws Exception {
        return crowdSourceCustomTagDAO.getCrowdSourceCustomTag(recordIdentifier, user);
    }

    /**
     * This is to get the suggestion of UDC tag.
     *
     * @param languageId
     * @param tagPrefix
     * @return
     * @throws Exception
     */
    public String getUDCTagsSuggestions(String languageId, String tagPrefix) throws Exception {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject tagsJSONObject;
        List<UDCConceptDescription> uDCConceptDescriptions = udcConceptDescriptionDAO.getUDCTagsSuggestions(languageId, tagPrefix);
        for (UDCConceptDescription uDCConceptDescription : uDCConceptDescriptions) {
            tagsJSONObject = new JSONObject();
            tagsJSONObject.put("data", uDCConceptDescription.getUdcConceptId().getUdcNotation());
            tagsJSONObject.put("value", uDCConceptDescription.getDescription());
            jSONArray.add(tagsJSONObject);
        }
        finalJSONObj.put("query", tagPrefix);
        finalJSONObj.put("suggestions", jSONArray);
        return finalJSONObj.toString();
    }

    /**
     * This is to get all the indian languages.
     *
     * @return @throws Exception
     */
    public List<UDCLanguage> getIndianLanguageList() throws Exception {
        return languageDAO.getIndianLanguageList();
    }

    /**
     * This is to get all the languages.
     *
     * @return @throws Exception
     */
    public List<UDCLanguage> getLanguageList() throws Exception {
        return languageDAO.list();
    }

    /**
     * This is to get UDC tag description.
     *
     * @param notations
     * @param languageId
     * @param isReplaceByEnglish
     * @return
     * @throws Exception
     */
    public Map<String, String> getUDCTagDescription(List<String> notations, long languageId, boolean isReplaceByEnglish) throws Exception {
        return udcConceptDescriptionDAO.getUDCTagDescription(notations, languageId, isReplaceByEnglish);
    }

    /**
     * This is to get UDC tag tree view data.
     *
     * @param languageId
     * @param udcTags
     * @param isDisable
     * @return
     */
    public Map<String, Object> generateUDCTreeView(long languageId, String udcTags, boolean isDisable) {
        int nodeCount = 0;
        Map<String, Object> returnData = new HashMap<>();
        JSONArray udcTreeViewData = new JSONArray();
        JSONObject nodeIds = new JSONObject();
        TreeSet<UDCConcept> udcConceptSet = new TreeSet<>(udcConceptDAO.get(Long.valueOf("2450")).getUdcConceptSet());
        Iterator<UDCConcept> udcConceptIterator = udcConceptSet.iterator();
        while (udcConceptIterator.hasNext()) {
            nodeCount = generateUDCChildTagRecFun(udcConceptIterator.next(), languageId, udcTreeViewData, udcTags, nodeIds, nodeCount, isDisable);
        }
        returnData.put("nodeIds", nodeIds);
        returnData.put("udcTreeViewData", udcTreeViewData);
        return returnData;
    }

    /**
     * This is to generate UDC ChildTag RecFun;
     *
     * @param udcConcept
     * @param languageId
     * @param parentJSONArray
     * @param udcTags
     * @param nodeIds
     * @param nodeCount
     * @param isDisable
     * @return
     */
    public int generateUDCChildTagRecFun(UDCConcept udcConcept, long languageId, JSONArray parentJSONArray, String udcTags, JSONObject nodeIds, int nodeCount, boolean isDisable) {
        nodeIds.put(udcConcept.getUdcNotation(), nodeCount++);
        String englishText = "";
        JSONObject treeViewMap = new JSONObject();
        String text = "[" + udcConcept.getUdcNotation() + "] ";
        boolean isTextAvailable = false;
        Iterator<UDCConceptDescription> udcConceptDescriptionIterator = udcConcept.getUdcConceptDescriptionSet().iterator();
        while (udcConceptDescriptionIterator.hasNext()) {
            UDCConceptDescription udcConceptDescription = udcConceptDescriptionIterator.next();
            if (udcConceptDescription.getLanguageId().getId() == languageId && udcConceptDescription.getUdcTermId().getTerm().equalsIgnoreCase("skos:prefLabel")) {
                text += udcConceptDescription.getDescription();
                isTextAvailable = true;
                break;
            } else if (udcConceptDescription.getLanguageId().getId() == 40 && udcConceptDescription.getUdcTermId().getTerm().equalsIgnoreCase("skos:prefLabel")) {
                englishText = udcConceptDescription.getDescription();
            }
        }

        if (!isTextAvailable) {
            text += englishText;
        }

        JSONObject stateJSONObject = new JSONObject();
        stateJSONObject.put("expanded", false);
        stateJSONObject.put("checked", false);
        for (String udcTag : udcTags.split("&|&")) {
            if (udcTag.trim().equalsIgnoreCase(udcConcept.getUdcNotation().trim())) {
                stateJSONObject.put("checked", true);
                stateJSONObject.put("disabled", isDisable);
                break;
            }
        }
        treeViewMap.put("state", stateJSONObject);
        treeViewMap.put("text", text);

        if (udcConcept.getUdcConceptSet().size() > 0) {
            JSONArray nodes = new JSONArray();
            TreeSet<UDCConcept> tempUDCConceptSet = new TreeSet<>(udcConcept.getUdcConceptSet());
            Iterator<UDCConcept> udcConceptIterator = tempUDCConceptSet.iterator();

            while (udcConceptIterator.hasNext()) {
                nodeCount = generateUDCChildTagRecFun(udcConceptIterator.next(), languageId, nodes, udcTags, nodeIds, nodeCount, isDisable);
            }

            treeViewMap.put("nodes", nodes);
        }
        parentJSONArray.add(treeViewMap);
        return nodeCount;
    }

    /**
     * This is to get CrowdSource UDCTag List By UserId
     *
     * @param pageNumber
     * @param pageWindow
     * @return
     * @throws Exception
     */
    public List<CrowdSourceUDCTag> getCrowdSourceUDCTagListByUserId(int pageNumber, int pageWindow) throws Exception {
        return crowdSourceUDCTagDAO.getCrowdSourceUDCTagListByUserId(pageNumber, pageWindow);
    }

    /**
     * This is to get CrowdSource Total UDCTag Count By UserId
     *
     * @param pageWindow
     * @return
     * @throws Exception
     */
    public long getCrowdSourceTotalUDCTagCountByUserId(int pageWindow) throws Exception {
        long totalUDCTagCount = crowdSourceUDCTagDAO.getCrowdSourceTotalUDCTagCountByUserId();
        double totalPages = (double) totalUDCTagCount / pageWindow;
        return (long) Math.ceil(totalPages);
    }

    /**
     * This is to save the custom tag
     *
     * @param customTags
     * @return
     */
    public boolean saveCustomTag(CustomTags customTags) {
        return customTagsDAO.createNew(customTags);
    }

    /**
     * This is to get custom tag
     *
     * @param tag
     * @param languageId
     * @return
     * @throws Exception
     */
    public CustomTags getCustomTag(String tag, long languageId) throws Exception {
        return customTagsDAO.getCustomTag(tag, languageId);
    }

    /**
     * This is to get custom tag suggestions
     *
     * @param languageId
     * @param tagPrefix
     * @return
     * @throws Exception
     */
    public String getCustomTagsSuggestions(String languageId, String tagPrefix) throws Exception {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject tagsJSONObject;
        List<CustomTags> customTags = customTagsDAO.getCustomTagsSuggestions(languageId, tagPrefix);
        for (CustomTags customTag : customTags) {
            tagsJSONObject = new JSONObject();
            tagsJSONObject.put("data", customTag.getTagName());
            tagsJSONObject.put("value", customTag.getTagName());
            jSONArray.add(tagsJSONObject);
        }
        finalJSONObj.put("query", tagPrefix);
        finalJSONObj.put("suggestions", jSONArray);
        return finalJSONObj.toString();
    }

    /**
     * This is to save crowdsourced meta data.
     *
     * @param metadataBean
     * @param user
     * @param modifiedMetadataJson
     * @param request
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean saveMetaData(MetadataBean metadataBean, User user, String modifiedMetadataJson, HttpServletRequest request) throws Exception {
        Object object;
        if (metadataBean.getMetadataType().equals(Constants.MetadataStandardType.DUBLIN_CORE)) {
            object = metadataBean.getDcMetadata();
        } else {
            object = metadataBean.getCollectionType();//Marc21 data
        }
        String crowdSourceId = metadataBean.getCrowdSourceId();
        String userType = metadataBean.getUserType();
        String editStatus = metadataBean.getEditStatus();
        String recordIdentifier = metadataBean.getRecordIdentifier();
        Boolean isEditFirstTime = metadataBean.getIsEditFirstTime();
        if (object == null || userType == null || editStatus == null || recordIdentifier == null
                || isEditFirstTime == null || userType.isEmpty() || editStatus.isEmpty() || recordIdentifier.isEmpty()) {
            return false;
        } else {
            String metadataJsonString = MetadataTypeHelper.objectToJSONString(object);
            CrowdSource crowdSource;
            //for saving metadata
            if (userType.equalsIgnoreCase("normal")) {
                if (isEditFirstTime) {
                    MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);
                    Map<String, String> recordTitleMap = crowdsourceWebserviceHelper.getRecordsTitle(Arrays.asList(recordIdentifier));
                    String metadataJSONString = MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata());
                    crowdSource = new CrowdSource();
                    crowdSource.setRecordIdentifier(recordIdentifier);
                    crowdSource.setOriginalMetadataJson(metadataJSONString);
                    crowdSource.setStartTimestamp(new Date());
                    Integer maxEditNumber = getCrowdSourceMaxEditNumber(recordIdentifier);
                    if (maxEditNumber != null) {
                        crowdSource.setEditNumber(maxEditNumber + 1);
                    } else {
                        crowdSource.setEditNumber(1);
                    }
                    crowdSource.setLastSubeditNumber(0);
                    crowdSource.setMetadataType(metadataBean.getMetadataType());
                    if (recordTitleMap != null && !recordTitleMap.isEmpty()) {
                        crowdSource.setRecordTitle(recordTitleMap.get(recordIdentifier));
                    }
                    String allowedLanguages = metadataStandardResponse.getAllowedLanguages();
                    if (allowedLanguages == null || allowedLanguages.isEmpty()) {
                        allowedLanguages = "en";
                    }
                    crowdSource.setAllowedLanguages(allowedLanguages);
                    crowdSource.setSubResourceCode(metadataStandardResponse.getSubResourceCode());

                    //assign record to the appropriate expert
                    List<Long> userIds = userDAO.getUserIdsByAssignedResource(metadataStandardResponse.getSubResourceCode(), Constants.UserRole.LIB_USER);
                    if (userIds != null && !userIds.isEmpty()) {
                        User tempUser = crowdSourceDAO.getMinimumRecordAssignedUserFromUserList(userIds);
                        if (tempUser != null) {
                            crowdSource.setExpertUserId(tempUser);
                        } else {
                            crowdSource.setExpertUserId(userDAO.get(userIds.get(0)));
                        }
                    } else {
                        //uncomment code to 
                        //make entry in resources-without-lib-user table
//                        ResourcesWithoutLibUser resourcesWithoutLibUser = new ResourcesWithoutLibUser();
//                        Resource resource = resourceDAO.getResourceByCode(crowdSource.getSubResourceCode());
//                        resourcesWithoutLibUser.setResource(resource);
//                        resourcesWithoutLibUserDAO.saveOrUpdate(resourcesWithoutLibUser);
                        crowdSource.setExpertUserId(null);
                    }
                    saveOrUpdateCrowdSource(crowdSource);
                } else {
                    crowdSource = getCrowdSource(Long.valueOf(crowdSourceId));
                }

                CrowdSourceEdit crowdSourceEdit = new CrowdSourceEdit();
                //if role is curator and record was assigned to this user then set curation status
                for (Role role : user.getRoles()) {
                    if (role.getCode().equalsIgnoreCase(Constants.UserRole.CURATOR_USER)) {
                        Map<String, String> recordStatusMap = curationActivityWebserviceHelper.getRecordStatusFromServer(recordIdentifier);
                        if (recordStatusMap.get(Constants.ASSIGNED_ID).equalsIgnoreCase(user.getId().toString())) {
                            curationActivityWebserviceHelper.setRecordStatusToServer(recordIdentifier, Constants.CURATED);
                            crowdSourceEdit.setIsCuratedVersion(true);
                        }
                        break;
                    }
                }
                crowdSourceEdit.setSubEditNumber(crowdSource.getLastSubeditNumber() + 1);
                crowdSourceEdit.setUser(user);
                crowdSourceEdit.setEditedMetadataJson(metadataJsonString);
                crowdSourceEdit.setCrowdSourceEditTimestamp(new Date());
                crowdSourceEdit.setCrowdSource(crowdSource);
                if (saveCrowdSourceEdit(crowdSourceEdit)) {
                    crowdSource.setLastSubeditNumber(crowdSource.getLastSubeditNumber() + 1);
                    saveOrUpdateCrowdSource(crowdSource);
                    if (crowdSource.getLastSubeditNumber() == crowdsourceEditThreshold) {
                        MetadataStandardResponse metadataStandardResponse = new MetadataStandardResponse();
                        metadataStandardResponse.setRecordIdentifier(crowdSource.getRecordIdentifier());
                        metadataStandardResponse.setMetadataStandard(crowdSource.getMetadataType());
                        metadataStandardResponse.setIsCloseForEdit("yes");
                        metadataStandardResponse.setMetadata(object);
                        //crowdsourceWebserviceHelper.saveMetadataStandardResponse(metadataStandardResponse);
                    }

                    //comment
                    if (modifiedMetadataJson != null && !modifiedMetadataJson.isEmpty()) {
                        rewardsService.rewardPointsForEditMetadata(modifiedMetadataJson, request, crowdSource.getRecordIdentifier(), crowdSource.getId(), crowdSource.getEditNumber(), userService.getCurrentUser());
                    }
                } else {
                    return false;
                }
            } //for approving matadata
            else {
                //remove all null and empty values in string before approve
                if (metadataBean.getMetadataType().equals(Constants.MetadataStandardType.DUBLIN_CORE)) {
                    object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), MetadataTypeHelper.objectToJSONString(object)).getDcMetadata();
                } else {
                    object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), MetadataTypeHelper.objectToJSONString(object)).getCollectionType();
                }
                //if expert is approvin from edit record  metadata page and no versions found
                if (isEditFirstTime) {
                    MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);

                    Map<String, String> recordTitleMap = crowdsourceWebserviceHelper.getRecordsTitle(Arrays.asList(recordIdentifier));
                    String metadataJSONString = MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata());
                    crowdSource = new CrowdSource();
                    crowdSource.setRecordIdentifier(recordIdentifier);
                    crowdSource.setOriginalMetadataJson(metadataJSONString);
                    crowdSource.setStartTimestamp(new Date());
                    Integer maxEditNumber = getCrowdSourceMaxEditNumber(recordIdentifier);

                    if (maxEditNumber != null) {
                        crowdSource.setEditNumber(maxEditNumber + 1);
                    } else {
                        crowdSource.setEditNumber(1);
                    }
                    crowdSource.setLastSubeditNumber(0);
                    crowdSource.setMetadataType(metadataBean.getMetadataType());
                    if (recordTitleMap != null && !recordTitleMap.isEmpty()) {
                        crowdSource.setRecordTitle(recordTitleMap.get(recordIdentifier));
                    }
                    String allowedLanguages = metadataStandardResponse.getAllowedLanguages();
                    if (allowedLanguages == null || allowedLanguages.isEmpty()) {
                        allowedLanguages = "en";
                    }
                    crowdSource.setExpertUserId(user);
                    crowdSource.setAllowedLanguages(allowedLanguages);
                    crowdSource.setSubResourceCode(metadataStandardResponse.getSubResourceCode());
                } else {
                    crowdSource = getCrowdSource(Long.valueOf(crowdSourceId));
                    //if the record is not assigned to him...then assign id of the current user id
                    if (crowdSource.getExpertUserId() == null || user.getId() != crowdSource.getExpertUserId().getId()) {
                        crowdSource.setExpertUserId(user);
                    }
                }
                MetadataStandardResponse metadataStandardResponse = new MetadataStandardResponse();
                metadataStandardResponse.setRecordIdentifier(crowdSource.getRecordIdentifier());
                metadataStandardResponse.setMetadataStandard(crowdSource.getMetadataType());
                metadataStandardResponse.setIsCloseForEdit(editStatus);

                metadataStandardResponse.setMetadata(object);
                crowdSource.setApprovedMetadataJson(metadataJsonString);
                crowdSource.setIsApproved(Byte.valueOf("1"));
                crowdSource.setEndTimestamp(new Date());
                try {
                    if (crowdsourceWebserviceHelper.saveMetadataStandardResponse(metadataStandardResponse)) {
                        crowdSource.setIsUpdatedAtSource(Byte.valueOf("1"));
                    } else {
                        crowdSource.setIsUpdatedAtSource(Byte.valueOf("0"));
                    }
                } catch (Exception ex) {
                    crowdSource.setIsUpdatedAtSource(Byte.valueOf("0"));
                    ex.printStackTrace();
                }
                saveOrUpdateCrowdSource(crowdSource);
                // approving algorithm for rewards
                rewardsService.rewardsPointsForApprovedMetadata(crowdSource.getMetadataType(),crowdSource.getOriginalMetadataJson(),crowdSource.getApprovedMetadataJson(),crowdSource.getId(),request);
                
            }
        }
        return true;
    }

    /**
     * This is to get the content type of the file.
     *
     * @param path
     * @return
     */
    public String getContentType(String path) {
        String extension = path.substring(path.lastIndexOf(".") + 1, path.length());
        //List<String> extensions=Arrays.asList("gif","jpg","jpeg","png","pjpeg","GIF","JPG","JPEG","PNG","PJPEG","tif","TIF","tiff","TIFF");
        List<String> extensions = Arrays.asList("jpg", "jpeg", "JPG", "JPEG");
        if (extensions.contains(extension)) {
            return Constants.ContentType.JPEG;
        }
        extensions = Arrays.asList("tif", "TIF", "tiff", "TIFF");
        if (extensions.contains(extension)) {
            return Constants.ContentType.TIFF;
        }
        extensions = Arrays.asList("wav", "WAV", "mp3", "MP3", "amr", "AMR", "flac", "FLAC");
        if (extensions.contains(extension)) {
            return Constants.ContentType.AUDIO;
        }
        extensions = Arrays.asList("flv", "FLV", "mp4", "MP4", "avi", "AVI", "mov", "MOV", "mxf", "MXF");
        if (extensions.contains(extension)) {
            return Constants.ContentType.VIDEO;
        }
        return Constants.ContentType.DOCUMENT;
    }

    /**
     * This is to convert Content To Map
     *
     * @param content
     * @return
     */
    public Map<String, Map<Long, String>> convertContentToMap(Content content) {
        Map<String, Map<Long, String>> audioVideoMarking = new HashMap<>();
        if (content != null) {
            Map<Long, String> marker;
            for (Media media : content.getMedia()) {
                marker = new HashMap<>();
                for (Marker markerObj : media.getMarker()) {
                    marker.put(convertTimecodeIntoSeconds(markerObj.getTimecode()), markerObj.getName() + "," + markerObj.getTimecode());
                }
                audioVideoMarking.put(media.getFilename(), marker);
            }
        }
        return audioVideoMarking;
    }

    /**
     * This is to convert json to content.
     *
     * @param jSONObject
     * @param recordIdentifier
     * @param previousContent
     * @return
     * @throws JSONException
     * @throws Exception
     */
    public Content jsonObjectToContent(org.json.JSONObject jSONObject, String recordIdentifier, Content previousContent) throws JSONException, Exception {
        org.json.JSONObject innerJSONObject;
        Media media;
        Marker marker;
        List<Media> mediaList = previousContent.getMedia();
        Map<String, Object> mediaMap = new HashMap<>();
        for (Media media1 : mediaList) {
            mediaMap.put(media1.getFilename(), media1);
        }
        List<Marker> markerList = new ArrayList<>();
        List<String> keyList1 = IteratorUtils.toList(jSONObject.keys());
        Collections.sort(keyList1, new StringComparator());
        String value;
        for (String key1 : keyList1) {
            media = (Media) mediaMap.get(key1);
            if (media != null) {
                markerList = media.getMarker();
            }
            innerJSONObject = (org.json.JSONObject) jSONObject.get(key1);
            List<String> keyList2 = IteratorUtils.toList(innerJSONObject.keys());
            Collections.sort(keyList2, new StringComparator());
            for (String key2 : keyList2) {
                marker = new Marker();
                value = innerJSONObject.get(key2).toString();
                if (match(value.split(",")[0], Constants.MARKER_NAME) && match(value.split(",")[1], Constants.TIME_CODE)) {
                    marker.setName(value.split(",")[0]);
                    marker.setTimecode(value.split(",")[1]);
                    if (!markerList.contains(marker)) {
                        markerList.add(marker);
                    }
                } else {
                    throw new Exception();
                }
            }
            media = new Media();
            media.getMarker().addAll(markerList);
            media.setFilename(key1);
            if (!mediaList.contains(media)) {
                mediaList.add(media);
            }
        }
        Content content = new Content();
        content.getMedia().addAll(mediaList);
        content.setId(recordIdentifier);
        return content;
    }

    /**
     * This is to get MARC21 tag suggestions.
     *
     * @param tagField
     * @return
     * @throws Exception
     */
    public String getMarc21TagsSuggestions(String tagField) throws Exception {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject tagsJSONObject;
        List<Marc21TagSubfieldStructureDetail> marc21TagDescriptionDetailList = marc21TagSubfieldStructureDetailDAO.getMarc21TagsSuggestions(tagField);
        for (Marc21TagSubfieldStructureDetail marc21TagDescriptionDetail : marc21TagDescriptionDetailList) {
            tagsJSONObject = new JSONObject();
            tagsJSONObject.put("value", marc21TagDescriptionDetail.getTagField());
            tagsJSONObject.put("data", marc21TagDescriptionDetail.getTagSubfield());
            tagsJSONObject.put("tagLibrarianTitle", marc21TagDescriptionDetail.getLibrarianTitle());
            jSONArray.add(tagsJSONObject);
        }
        finalJSONObj.put("query", tagField);
        finalJSONObj.put("suggestions", jSONArray);
        return finalJSONObj.toString();
    }

    /**
     * This is to get Marc21 Description Suggestions
     *
     * @param librarianTitle
     * @return
     * @throws Exception
     */
    public String getMarc21DescriptionSuggestions(String librarianTitle) throws Exception {
        JSONObject finalJSONObj = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        JSONObject tagsJSONObject;
        List<Marc21TagSubfieldStructureDetail> marc21TagDescriptionList = marc21TagSubfieldStructureDetailDAO.getMarc21DescriptionSuggestions(librarianTitle);
        for (Marc21TagSubfieldStructureDetail marc21TagDescriptionDetail : marc21TagDescriptionList) {
            tagsJSONObject = new JSONObject();
            tagsJSONObject.put("value", marc21TagDescriptionDetail.getLibrarianTitle());
            tagsJSONObject.put("data", marc21TagDescriptionDetail.getTagField());
            tagsJSONObject.put("tagSubfield", marc21TagDescriptionDetail.getTagSubfield());
            jSONArray.add(tagsJSONObject);
        }
        finalJSONObj.put("query", librarianTitle);
        finalJSONObj.put("suggestions", jSONArray);
        return finalJSONObj.toString();
    }

    /**
     * This is to convert Timecode Into Seconds
     *
     * @param timecode
     * @return
     */
    private Long convertTimecodeIntoSeconds(String timecode) {
        Long milliseconds = Long.parseLong(timecode.split("\\.")[1]);
        Long seconds = Long.parseLong(timecode.split("\\.")[0].split(":")[2]) * 1000;
        Long minutes = Long.parseLong(timecode.split("\\.")[0].split(":")[1]) * 60000;
        Long hours = Long.parseLong(timecode.split("\\.")[0].split(":")[0]) * 3600000;
        return (hours + minutes + seconds + milliseconds) / 1000;
    }

    /**
     * This is to check the the regular expressions.
     *
     * @param inputString
     * @param regularExp
     * @return
     */
    private static boolean match(String inputString, String regularExp) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(regularExp);
        matcher = pattern.matcher(inputString);
        return matcher.matches();
    }

    /**
     * This is to save Or UpdateArticle to DB
     *
     * @param crowdSourceArticle
     */
    public void saveOrUpdateArticle(CrowdSourceArticle crowdSourceArticle) {
        crowdSourceArticleDAO.saveOrUpdate(crowdSourceArticle);
    }

    /**
     * This is to get article
     *
     * @param articleId
     * @return
     * @throws Exception
     */
    public CrowdSourceArticle getArticle(long articleId) throws Exception {
        return crowdSourceArticleDAO.get(articleId);
    }

    /**
     * This is to get article list
     *
     * @param articleIds
     * @return
     * @throws Exception
     */
    public List<CrowdSourceArticle> getArticleList(List<Long> articleIds) throws Exception {
        return crowdSourceArticleDAO.getArticleList(articleIds);
    }

    /**
     * This is to to set publish flag.
     *
     * @param articleId
     * @return
     * @throws Exception
     */
    public boolean setPublishFlag(long articleId) throws Exception {
        try {
            CrowdSourceArticle crowdSourceArticle = crowdSourceArticleDAO.get(articleId);
            crowdSourceArticle.setIsPublished(true);
            crowdSourceArticle.setPublishDate(new Timestamp(new Date().getTime()));
            crowdSourceArticleDAO.saveOrUpdate(crowdSourceArticle);
            return true;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            return false;
        }

    }

    /**
     * This is to set withdraw flag.
     *
     * @param articleId
     * @throws Exception
     */
    public void setWithdrawFlag(long articleId) throws Exception {
        CrowdSourceArticle crowdSourceArticle = crowdSourceArticleDAO.get(articleId);
        crowdSourceArticle.setIsPublished(false);
        crowdSourceArticle.setPublishDate(null);
        crowdSourceArticle.setIsApproved(false);
        crowdSourceArticleDAO.saveOrUpdate(crowdSourceArticle);
    }

    /**
     * This is to set approve tag
     *
     * @param articleId
     * @throws Exception
     */
    public void setApproveFlag(long articleId) throws Exception {
        CrowdSourceArticle crowdSourceArticle = crowdSourceArticleDAO.get(articleId);
        crowdSourceArticle.setIsApproved(true);
        crowdSourceArticleDAO.saveOrUpdate(crowdSourceArticle);
    }

    /**
     * This is to save Or Update Or Delete Attached Resource
     *
     * @param crowdSourceArticle
     * @param crowdSourceArticleAttachedResourceList
     */
    public void saveOrUpdateOrDeleteAttachedResource(CrowdSourceArticle crowdSourceArticle, List<CrowdSourceArticleAttachedResource> crowdSourceArticleAttachedResourceList) {
        if (crowdSourceArticleAttachedResourceList != null) {
            for (CrowdSourceArticleAttachedResource obj : crowdSourceArticleAttachedResourceList) {
                if (obj.getResourceLabel() != null && obj.getResourceURL() != null) {
                    obj.setArticle(crowdSourceArticle);
                    crowdSourceArticleAttachedResourceDAO.saveOrUpdate(obj);
                } else {
                    crowdSourceArticleAttachedResourceDAO.delete(obj);
                }
            }
        }
    }

    /**
     * This is to get UDC concept
     *
     * @param id
     * @return
     */
    public UDCConcept getUDCConcept(Long id) {
        return udcConceptDAO.get(id);
    }

    /**
     * This is to get UDC Language
     *
     * @param id
     * @return
     */
    public UDCLanguage getUDCLanguage(Long id) {
        return languageDAO.get(id);
    }

    /**
     * This is to generate UDC Tree View to JSONArray
     *
     * @param languageId
     * @param parentId
     * @param translationUDCLanguageId
     * @param showRecordCount
     * @return
     */
    public JSONArray generateUDCTreeViewJSONArray(long languageId, long parentId, String translationUDCLanguageId, boolean showRecordCount) {
        JSONArray udcTreeViewData = new JSONArray();
        generateUDCTreeViewJSONObject(udcConceptDAO.get(parentId), languageId, udcTreeViewData, translationUDCLanguageId, showRecordCount);
        if (parentId == 2450) {
            return (JSONArray) ((JSONObject) udcTreeViewData.get(0)).get("nodes");
        }
        return udcTreeViewData;
    }

    /**
     * This is to generate UDC Tree View to JSONObject
     *
     * @param udcConcept
     * @param languageId
     * @param parentJSONArray
     * @param translationUDCLanguageId
     * @param showRecordCount
     */
    public void generateUDCTreeViewJSONObject(UDCConcept udcConcept, long languageId, JSONArray parentJSONArray, String translationUDCLanguageId, boolean showRecordCount) {
        String englishText = "";
        JSONObject treeViewMap = new JSONObject();
        String text = "[" + udcConcept.getUdcNotation() + "] ";
        boolean isTextAvailable = false;
        long matchingLanguageId;
        if (translationUDCLanguageId == null || translationUDCLanguageId.isEmpty()) {
            matchingLanguageId = languageId;
        } else {
            matchingLanguageId = Long.parseLong(translationUDCLanguageId);
        }

        Iterator<UDCConceptDescription> udcConceptDescriptionIterator = udcConcept.getUdcConceptDescriptionSet().iterator();
        while (udcConceptDescriptionIterator.hasNext()) {
            UDCConceptDescription udcConceptDescription = udcConceptDescriptionIterator.next();
            if (udcConceptDescription.getLanguageId().getId() == matchingLanguageId && udcConceptDescription.getUdcTermId().getTerm().equalsIgnoreCase("skos:prefLabel")) {
                text += udcConceptDescription.getDescription();
                isTextAvailable = true;
                break;
            } else if (udcConceptDescription.getLanguageId().getId() == 40 && udcConceptDescription.getUdcTermId().getTerm().equalsIgnoreCase("skos:prefLabel")) {
                englishText = udcConceptDescription.getDescription();
            }
        }

        if (!isTextAvailable) {
            text += englishText;
            if (translationUDCLanguageId == null || translationUDCLanguageId.isEmpty()) {
                treeViewMap.put("color", "#C03247");
            }
        }

        if (showRecordCount) {
            treeViewMap.put("text", text + " (" + udcConcept.getUdcNotationRecordCount() + ")");
        } else {
            treeViewMap.put("text", text);
        }
        treeViewMap.put("href", udcConcept.getId());

        if (udcConcept.getUdcConceptSet().size() > 0) {
            JSONArray nodes = new JSONArray();
            TreeSet<UDCConcept> tempUDCConceptSet = new TreeSet<>(udcConcept.getUdcConceptSet());
            Iterator<UDCConcept> udcConceptIterator = tempUDCConceptSet.iterator();

            while (udcConceptIterator.hasNext()) {
                generateUDCTreeViewJSONObject(udcConceptIterator.next(), languageId, nodes, translationUDCLanguageId, showRecordCount);
            }

            treeViewMap.put("nodes", nodes);
        }
        parentJSONArray.add(treeViewMap);
    }

    /**
     * This is to get user tags paginated
     *
     * @param userId
     * @param pageNumber
     * @param pageWindow
     * @return
     * @throws Exception
     */
    public List<CrowdSourceCustomTag> getuserTagsPaginated(long userId, int pageNumber, int pageWindow) throws Exception {
        return crowdSourceCustomTagDAO.getuserTagsPaginated(userId, pageNumber, pageWindow);
    }

    /**
     * This is to generate UDC Table Data With Pagination
     *
     * @param ParentId
     * @param languageId
     * @param translationUDCLanguageId
     * @return
     * @throws Exception
     */
    public List<UDCConceptDescription> generateUDCTableDataWithPagination(String ParentId, long languageId, long translationUDCLanguageId) throws Exception {
        return udcConceptDescriptionDAO.getUntranslatedUDCTagDescription(ParentId, languageId, translationUDCLanguageId);
    }

    /**
     * This is to get UDC Translation
     *
     * @param udcConceptId
     * @param translationUDCLanguageId
     * @param userId
     * @return
     * @throws Exception
     */
    public UDCTranslationBean getUDCTranslation(long udcConceptId, long translationUDCLanguageId, long userId) throws Exception {
        UDCTranslationBean udcTranslationBean = new UDCTranslationBean();
        UDCConcept udcConcept = udcConceptDAO.get(udcConceptId);
        List<String> notations = new ArrayList<>();
        notations.add(udcConcept.getUdcNotation());
        Map<String, String> map = udcConceptDescriptionDAO.getUDCTagDescription(notations, 40l, false);
        udcTranslationBean.setSelectedNotation("[" + udcConcept.getUdcNotation() + "] " + map.get(udcConcept.getUdcNotation()));
        String originalTranslation = "Not Available";
        map = udcConceptDescriptionDAO.getUDCTagDescription(notations, translationUDCLanguageId, false);
        if (!map.isEmpty()) {
            originalTranslation = "[" + udcConcept.getUdcNotation() + "] " + map.get(udcConcept.getUdcNotation());
        }
        udcTranslationBean.setOriginalTranslation(originalTranslation);
        List<UDCTagsCrowdsource> otherUsersTranslation = new ArrayList<>();
        udcTranslationBean.setOwnTranslation("");
        UDCTagsCrowdsource tempUDCTagsCrowdsource;
        User tempUser;
        Map<Long, String> votingMap = new TreeMap<>();
        for (UDCTagsCrowdsource udcTagsCrowdsource : udcTagsCrowdsourceDAO.getUDCTagsCrowdsourceList(udcConceptId, translationUDCLanguageId)) {
            votingMap.put(udcTagsCrowdsource.getId(), udcTagsCrowdsource.getVoteCount() + "");

            if (udcTagsCrowdsource.getUser().getId() == userId) {
                udcTranslationBean.setOwnTranslation(udcTagsCrowdsource.getDescription());
            }

            tempUDCTagsCrowdsource = new UDCTagsCrowdsource();
            tempUDCTagsCrowdsource.setId(udcTagsCrowdsource.getId());
            tempUDCTagsCrowdsource.setDescription(udcTagsCrowdsource.getDescription());
            tempUser = new User();
            tempUser.setFirstName(udcTagsCrowdsource.getUser().getFirstName() + " " + udcTagsCrowdsource.getUser().getLastName());
            tempUDCTagsCrowdsource.setUser(tempUser);
            otherUsersTranslation.add(tempUDCTagsCrowdsource);
        }
        calculateVotingPercentage(votingMap);
        udcTranslationBean.setVotingMap(votingMap);
        udcTranslationBean.setIsVoted(udcTagVoteDAO.isVoted(udcConceptId, translationUDCLanguageId, userId));
        udcTranslationBean.setOtherUsersTranslation(otherUsersTranslation);
        return udcTranslationBean;
    }

    /**
     * This is to save Or Update UDCTagsCrowdsource
     *
     * @param udcTagsCrowdsource
     */
    public void saveOrUpdateUDCTagsCrowdsource(UDCTagsCrowdsource udcTagsCrowdsource) {
        udcTagsCrowdsourceDAO.saveOrUpdate(udcTagsCrowdsource);
    }

    /**
     * Distribution Logic :: Assign translated tag to appropriate user
     *
     * @param udcTagsCrowdsource
     * @return
     */
    public UDCTranslationExperts distributionOfTranslatedTag(UDCTagsCrowdsource udcTagsCrowdsource) {
        UDCTranslationExperts udcTranslationExpert = null;
        try {
            //Distribution Logic :: Assign translated tag to appropriate user
            //get users who has library expert for particular language
            List<Long> userIds = userDAO.getUserIdsByAssignedUDCLanguageId(udcTagsCrowdsource.getUdcLanguage().getId(), Constants.UserRole.LIB_USER);
            UDCConceptDescription udcConceptDiscription = udcConceptDescriptionDAO.getUUDCConceptDescription(udcTagsCrowdsource.getUdcConcept(), 40l);
            udcTranslationExpert = udcTranslationExpertDAO.checkEntryPresent(udcConceptDiscription, udcTagsCrowdsource.getUdcLanguage());
            if (udcTranslationExpert == null) {
                udcTranslationExpert = new UDCTranslationExperts();
                udcTranslationExpert.setUdcConceptDescription(udcConceptDiscription);
                udcTranslationExpert.setUdcLanguage(udcTagsCrowdsource.getUdcLanguage());
            }

            if (udcTranslationExpert.getExpert() == null) {
                if (userIds != null && !userIds.isEmpty()) {
                    //get minimum transalated tags assigned user
                    User tempUser = crowdSourceDAO.getMinimumTagsAssignedUserFromUserList(userIds);
                    if (tempUser != null) {
                        //set user as expert to particular tag
                        udcTranslationExpert.setExpert(tempUser);
                    } else {
                        //not get any user set first user as library expert                  
                        udcTranslationExpert.setExpert(userDAO.get(userIds.get(0)));
                    }
                } else {
                    udcTranslationExpert.setExpert(null);
                }
            }
            udcTranslationExpertDAO.saveOrUpdate(udcTranslationExpert);
            //if library user not available by default set expert id as null
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return udcTranslationExpert;
    }

    /**
     * This is to check UDCTags Crowdsource if Already Exist
     *
     * @param udcConceptId
     * @param languageId
     * @param userId
     * @return
     * @throws Exception
     */
    public UDCTagsCrowdsource checkUDCTagsCrowdsourceAlreadyExist(long udcConceptId, long languageId, long userId) throws Exception {
        return udcTagsCrowdsourceDAO.checkUDCTagsCrowdsourceAlreadyExist(udcConceptId, languageId, userId);
    }

    /**
     * This is to get UDCTags Crowdsource List
     *
     * @param userId
     * @return
     * @throws Exception
     */
    public List<UDCTagsCrowdsource> getUDCTagsCrowdsourceList(long userId) throws Exception {
        List<UDCTagsCrowdsource> udcTagsCrowdsourceList = udcTagsCrowdsourceDAO.getUDCTagsCrowdsourceList(userId);
        for (UDCTagsCrowdsource udcTagsCrowdsource : udcTagsCrowdsourceList) {
            udcTagsCrowdsource.setDescription("[" + udcTagsCrowdsource.getUdcConcept().getUdcNotation() + "] " + udcTagsCrowdsource.getDescription());
            udcTagsCrowdsource.setUdcConcept(null);
            udcTagsCrowdsource.setUdcLanguage(null);
            udcTagsCrowdsource.setUser(null);
        }
        return udcTagsCrowdsourceList;
    }

    /**
     * This is to calculate voting percentage.
     *
     * @param votingMap
     */
    private void calculateVotingPercentage(Map<Long, String> votingMap) {
        DecimalFormat df = new DecimalFormat("##.##");
        double totalCount = 0;
        for (Long key : votingMap.keySet()) {
            totalCount += Double.parseDouble(votingMap.get(key));
        }
        double voteCount;
        for (Long key : votingMap.keySet()) {
            voteCount = Double.parseDouble(votingMap.get(key));
            if (voteCount == 0) {
                votingMap.put(key, "0");
            } else {
                votingMap.put(key, df.format((voteCount / totalCount) * 100) + "");
            }
        }
    }

    /**
     * This is to get UDCTagsCrowdsource.
     *
     * @param udcTagsCrowdsourceId
     * @return
     * @throws Exception
     */
    public UDCTagsCrowdsource getUDCTagsCrowdsource(long udcTagsCrowdsourceId) throws Exception {
        return udcTagsCrowdsourceDAO.get(udcTagsCrowdsourceId);
    }

    /**
     * This is to save UDCTagVote.
     *
     * @param udcTagVote
     * @return
     * @throws Exception
     */
    public boolean saveUDCTagVote(UDCTagVote udcTagVote) throws Exception {
        return udcTagVoteDAO.createNew(udcTagVote);
    }

    /**
     * This is to get NewlyTranslated UDCTag Description.
     *
     * @param parentId
     * @param languageId
     * @param translationId
     * @return
     * @throws Exception
     */
    public List<UDCConceptDescription> getNewlyTranslatedUDCTagDescription(String parentId, long languageId, long translationId) throws Exception {
        return udcConceptDescriptionDAO.getNewlyTranslatedUDCTagDescription(parentId, languageId, translationId);
    }

    /**
     * This is to validate Duplicate Metadata.
     *
     * @param metadataBean
     * @return
     * @throws Exception
     */
    public Map<String, Object> validateDuplicateMetadata(MetadataBean metadataBean) throws Exception {
        Object object;
        Map<String, Object> map = new HashMap<>();
        map.put("status", true);
        if (metadataBean.getMetadataType().equals(Constants.MetadataStandardType.DUBLIN_CORE)) {
            object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), MetadataTypeHelper.objectToJSONString(metadataBean.getDcMetadata())).getDcMetadata();
        } else {
            object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), MetadataTypeHelper.objectToJSONString(metadataBean.getCollectionType())).getCollectionType();//Marc21 data
        }
        String crowdSourceId = metadataBean.getCrowdSourceId();
        String userType = metadataBean.getUserType();
        String editStatus = metadataBean.getEditStatus();
        String recordIdentifier = metadataBean.getRecordIdentifier();
        Boolean isEditFirstTime = metadataBean.getIsEditFirstTime();
        if (object == null || userType == null || editStatus == null || recordIdentifier == null
                || isEditFirstTime == null || userType.isEmpty() || editStatus.isEmpty() || recordIdentifier.isEmpty()) {
            map.put("status", false);
        } else {
            String metadataJsonString = MetadataTypeHelper.objectToJSONString((object));
            CrowdSource crowdSource;
            //for saving metadata
            if (userType.equalsIgnoreCase("normal")) {
                if (isEditFirstTime) {
                    MetadataStandardResponse metadataStandardResponse = crowdsourceWebserviceHelper.getMetadataStandardResponse(recordIdentifier);
                    if (metadataBean.getMetadataType().equals(Constants.MetadataStandardType.DUBLIN_CORE)) {
                        object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata())).getDcMetadata();
                    } else {
                        object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), MetadataTypeHelper.objectToJSONString(metadataStandardResponse.getMetadata())).getCollectionType();
                    }
                    String metadataJSONString = MetadataTypeHelper.objectToJSONString(object);
                    if (metadataJsonString.toLowerCase().replaceAll("\\s", "").hashCode() == metadataJSONString.toLowerCase().replaceAll("\\s", "").hashCode()) {
                        map.put("status", false);
                        map.put("version", "original");
                    }
                } else {
                    crowdSource = getCrowdSource(Long.valueOf(crowdSourceId));
                    //to remove the null and empty strings in the metadata
                    if (metadataBean.getMetadataType().equals(Constants.MetadataStandardType.DUBLIN_CORE)) {
                        object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), crowdSource.getOriginalMetadataJson()).getDcMetadata();
                    } else {
                        object = MetadataTypeHelper.getMetadataBeanFromMetadataJSONString(metadataBean.getMetadataType(), crowdSource.getOriginalMetadataJson()).getCollectionType();//Marc21 data
                    }
                    String metadataJSONString = MetadataTypeHelper.objectToJSONString(object);
                    //
                    if (metadataJsonString.toLowerCase().replaceAll("\\s", "").hashCode() == metadataJSONString.toLowerCase().replaceAll("\\s", "").hashCode()) {
                        map.put("status", false);
                        map.put("version", "original");
                    } else {
                        for (CrowdSourceEdit crowdSourceEdit : crowdSource.getCrowdSourceEditSet()) {
                            if (metadataJsonString.toLowerCase().replaceAll("\\s", "").hashCode() == crowdSourceEdit.getEditedMetadataJson().toLowerCase().replaceAll("\\s", "").hashCode()) {
                                map.put("status", false);
                                map.put("version", "Revision " + crowdSourceEdit.getSubEditNumber());
                                break;
                            }
                        }

                    }
                }
            }
        }
        return map;
    }

    /**
     * This is to get Language List By Language Code List.
     *
     * @param langCodeList
     * @return
     * @throws Exception
     */
    public List<UDCLanguage> getLanguageListByLangCodeList(String[] langCodeList) throws Exception {
        return languageDAO.getLanguageListByLangCodeList(langCodeList);
    }

    /**
     * This is to get Articles By Filters With Limit.
     *
     * @param userId
     * @param searchString
     * @param statusTypeFilter
     * @param dataLimit
     * @param pageNo
     * @param isRequiredUserRestriction
     * @return
     * @throws Exception
     */
    public List<CrowdSourceArticle> getArticlesByFiltersWithLimit(long userId, String searchString, String statusTypeFilter, int dataLimit, int pageNo, boolean isRequiredUserRestriction) throws Exception {
        List<CrowdSourceArticle> crowdSourceArticles = new ArrayList<>();
        for (CrowdSourceArticle crowdSourceArticle : crowdSourceArticleDAO.getArticlesByFiltersWithLimit(userId, searchString, statusTypeFilter, dataLimit, pageNo, isRequiredUserRestriction)) {
            CrowdSourceArticle article = new CrowdSourceArticle();
            article.setArticleTitle(crowdSourceArticle.getArticleTitle());
            article.setId(crowdSourceArticle.getId());
            article.setIsApproved(crowdSourceArticle.getIsApproved());
            article.setIsPublished(crowdSourceArticle.getIsPublished());
            article.setLastModifiedDate(crowdSourceArticle.getLastModifiedDate());
            article.setPublishDate(crowdSourceArticle.getPublishDate());
            User user = new User();
            user.setFirstName(crowdSourceArticle.getUser().getFirstName() + " " + crowdSourceArticle.getUser().getLastName());
            article.setUser(user);
            crowdSourceArticles.add(article);
        }
        return crowdSourceArticles;
    }

    /**
     * This is to get Articles Total Count By Filters.
     *
     * @param userId
     * @param searchString
     * @param statusTypeFilter
     * @param isRequiredUserRestriction
     * @return
     * @throws Exception
     */
    public long getArticlesTotalCountByFilters(long userId, String searchString, String statusTypeFilter, boolean isRequiredUserRestriction) throws Exception {
        return crowdSourceArticleDAO.getArticlesTotalCountByFilters(userId, searchString, statusTypeFilter, isRequiredUserRestriction);
    }

    public JSONObject fetchTranslatedTagsWithLimit(long userId, String searchString, int dataLimit, int pageNo) throws Exception {
        List<UDCTranslationExperts> uDCConceptDescriptions = udcConceptDescriptionDAO.fetchTranslatedTagsWithLimit(userId, searchString, dataLimit, pageNo);
        JSONObject uDCConceptDescriptionJSONObject = new JSONObject();
        JSONArray uDCConceptDescriptionJSONArray = new JSONArray();
        uDCConceptDescriptions.stream().forEach(el -> {
            JSONObject tempJSONObject = new JSONObject();
            tempJSONObject.put("udcConceptId", el.getUdcConceptDescription().getUdcConceptId().getId());
            tempJSONObject.put("udcLanguageId", el.getUdcLanguage().getId());
            tempJSONObject.put("udcLanguageName", el.getUdcLanguage().getLanguageName());
            tempJSONObject.put("udcNotation", el.getUdcConceptDescription().getUdcConceptId().getUdcNotation());
            tempJSONObject.put("description", el.getUdcConceptDescription().getDescription());
            uDCConceptDescriptionJSONArray.add(tempJSONObject);
        });
        uDCConceptDescriptionJSONObject.put("uDCConceptDescriptionJSONArray", uDCConceptDescriptionJSONArray);
        return uDCConceptDescriptionJSONObject;
    }

    public long fetchTranslatedTagsTotalCount(long userId, String searchString) throws Exception {
        return udcConceptDescriptionDAO.fetchTranslatedTagsTotalCount(userId, searchString);
    }

    /**
     * This is to get Resource By ResourceType.
     *
     * @return @throws Exception
     */
    public List<Resource> getResourceByResourceType() throws Exception {
        ResourceType resourceTypeObj = resourceTypeDAO.findbyQuery("findByResourceType", Constants.RESOURCE_TYPE_BLOG_CODE).get(0);
        List<Resource> resources = resourceDAO.getResourceByResourceType(resourceTypeObj);
        return resources;
    }

    public JSONObject seeUDCTranslatedTags(long conceptId, long languageId) {
        JSONObject seeUDCTranslatedTagsjSONObject = new JSONObject();
        JSONArray seeUDCTranslatedTagsJSONArray = new JSONArray();
        List<UDCTagsCrowdsource> udcTagsCrowdsourceList = udcTagsCrowdsourceDAO.seeUDCTranslatedTags(conceptId, languageId);
        if (!udcTagsCrowdsourceList.isEmpty()) {
            Map<Long, String> votingMap = new TreeMap<>();
            udcTagsCrowdsourceList.stream().forEach(el -> {
                votingMap.put(el.getId(), String.valueOf(el.getVoteCount()));
            });
            calculateVotingPercentage(votingMap);
            udcTagsCrowdsourceList.stream().forEach(el -> {
                JSONObject tempJSONObject = new JSONObject();
                tempJSONObject.put("udcTagsCrowdsourceId", el.getId());
                tempJSONObject.put("udcConceptId", el.getUdcConcept().getId());
                tempJSONObject.put("translatedDescription", el.getDescription());
                tempJSONObject.put("voteCount", el.getVoteCount());
                tempJSONObject.put("languageId", el.getUdcLanguage().getId());
                tempJSONObject.put("userId", el.getUser().getId());
                seeUDCTranslatedTagsJSONArray.add(tempJSONObject);
            });
            seeUDCTranslatedTagsjSONObject.put("seeUDCTranslatedTagsJSONArray", seeUDCTranslatedTagsJSONArray);
            seeUDCTranslatedTagsjSONObject.put("votingMap", votingMap);
            return seeUDCTranslatedTagsjSONObject;
        }
        return null;
    }

    public void updateUDCTranslation(JSONArray selectedTranslationArr, JSONArray rejectedTransalationsArr) {
        if (selectedTranslationArr.isEmpty()) {
            //for reject-all
            updateTranslatedTagFlag(rejectedTransalationsArr, false);
        } else {
            //save description history
            udcConceptDescriptionDAO.saveDescriptionHistory(selectedTranslationArr);
            //for accept
            updateTranslatedTagFlag(selectedTranslationArr, true);
            updateTranslatedTagFlag(rejectedTransalationsArr, false);
        }
    }

    private void updateTranslatedTagFlag(JSONArray arr, boolean isAccepted) {
        for (Object arr1 : arr) {
            JSONObject obj = (JSONObject) arr1;
            UDCTagsCrowdsource uDCTagsCrowdsource = udcTagsCrowdsourceDAO.get((long) obj.get("udcTagsCrowdsourceId"));
            uDCTagsCrowdsource.setIsAccepted(isAccepted);
            uDCTagsCrowdsource.setIsReviewed(true);
            uDCTagsCrowdsource.setReviewedDate(new Timestamp(new Date().getTime()));
            udcTagsCrowdsourceDAO.saveOrUpdate(uDCTagsCrowdsource);
            //set expert null of udc translation expert
            UDCTranslationExperts uDCTranslationExperts = udcTranslationExpertDAO.get(uDCTagsCrowdsource.getuDCTranslationExpertId().getId());
            if (uDCTranslationExperts.getExpert() != null) {
                uDCTranslationExperts.setExpert(null);
                udcTranslationExpertDAO.saveOrUpdate(uDCTranslationExperts);
            }
        }
    }
}
