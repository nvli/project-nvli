/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.artist.ArtistSiteDao;
import in.gov.nvli.domain.resource.ArtisticResourceType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj Singh Tanwar
 */
@Service
public class ArtistSiteService {

    @Autowired
    private ArtistSiteDao artisticSiteDao;

    public List<ArtisticResourceType> getAllArtResource() {
        return artisticSiteDao.list();
    }

    public String getSiteById(int id) {
        return artisticSiteDao.get(id).getArtistSite();
    }

    public ArtisticResourceType getArtistResourcebyId(int id) {
        return artisticSiteDao.get(id);
    }
}
