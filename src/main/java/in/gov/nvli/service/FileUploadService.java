package in.gov.nvli.service;

import in.gov.nvli.dao.user.UploadedFilesLogDAO;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.domain.user.UserUploadedFile;
import in.gov.nvli.util.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Sanjay Rabidas <sanjayr@cdac.in>
 */
@Service
public class FileUploadService {

    private final static Logger LOGGER = Logger.getLogger(FileUploadService.class);

    @Autowired
    UploadedFilesLogDAO uploadedFilesLogDAO;

    private String getUserImageBaseURL(String fileUploadLocation, long uid, String userImageBaseDirName) {
        return fileUploadLocation + File.separator + uid + File.separator + userImageBaseDirName;
    }

    public boolean uploadFile(User user, String fileUploadLocation, String userImageBaseDirName, MultipartFile uploadfile) {
        String userImageBase = this.getUserImageBaseURL(fileUploadLocation, user.getId(), userImageBaseDirName);
        File uploadBaseDir = new File(userImageBase);
        if (!uploadBaseDir.exists()) {
            try {
                uploadBaseDir.mkdirs();
            } catch (SecurityException se) {
                LOGGER.error(se.getMessage() + " in " + uploadBaseDir);
                return false;
            }

        }

        String fileName = this.createFileName(user, uploadfile);
        String filePath = uploadBaseDir.getAbsolutePath() + File.separator + fileName;
        File file = new File(filePath);

        if (file.exists()) {
            FileUtils.deleteQuietly(file);
        }
        FileOutputStream outputStream = null;
        boolean isUploaded = false;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();
            isUploaded = this.logUploadedFileInfo(fileName, filePath, uploadfile.getContentType(), user);
        } catch (Exception e) {
            LOGGER.error("Exception in Uploading Images in dir [ " + userImageBase + " ]", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
            System.out.println("Finally Uploaded");
        }
        return isUploaded;
    }

    public boolean replaceFile(User user, String fileUploadLocation, String userImageBaseDirName, MultipartFile uploadfile, long fileId) {
        UserUploadedFile previousFileInfo = uploadedFilesLogDAO.get(fileId);
        File oldfile = new File(previousFileInfo.getFilePath());
        if (oldfile.exists()) {
            FileUtils.deleteQuietly(oldfile);
        }
        
        
        String userImageBase = this.getUserImageBaseURL(fileUploadLocation, user.getId(), userImageBaseDirName);
        File uploadBaseDir = new File(userImageBase);

        String fileName = previousFileInfo.getFileName();
        String filePath = uploadBaseDir.getAbsolutePath() + File.separator + fileName;
        File file = new File(filePath);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(uploadfile.getBytes());
            outputStream.close();
        } catch (Exception e) {
            LOGGER.error("Exception in Uploading Images in dir [ " + userImageBase + " ]", e);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
        }
        return true;
    }

    private String createFileName(User user, MultipartFile uploadfile) {
        String[] fileNameAndExtension = uploadfile.getOriginalFilename().split(Constants.REG_EXP_FOR_EXTENSION);
        String fileExtension = fileNameAndExtension[1].toLowerCase();
        String fileName;
        Date d = new Date();
        fileName = user.getId().toString() + "-" + Long.toString(d.getTime()) + "-" + fileNameAndExtension[0] + "." + fileExtension;
        System.out.println("FileName = " + fileName);
        return fileName;
    }

    public boolean logUploadedFileInfo(String fileName, String filePath, String fileType, User user) {
        UserUploadedFile uploadedFilesLog = new UserUploadedFile();
        uploadedFilesLog.setCreatedBy(user.getId());
        uploadedFilesLog.setFileName(fileName);
        uploadedFilesLog.setFilePath(filePath);
        uploadedFilesLog.setCreatedOn(new Date());
        uploadedFilesLog.setFileType(fileType);
        return uploadedFilesLogDAO.createNew(uploadedFilesLog);
    }
}
