package in.gov.nvli.service;

import in.gov.nvli.beans.ontology.DomainOntology;
import in.gov.nvli.beans.ontology.MultipleBookmarkForm;
import in.gov.nvli.beans.ontology.Organization;
import in.gov.nvli.beans.ontology.UserDefinedCustomLabelDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Service to communicate with NVLI-Semantic-Relation webservice.
 *
 * @author Hemant Anjana
 * @since 30 May 2017
 */
@Service("owlVisualizeService")
public class OWLVisualizeService {

    private static final Logger LOG = Logger.getLogger(OWLVisualizeService.class.getName());
    /**
     * url of NVLI-Semantic-Relation web service from property file.
     */
    @Value("${nvli.semantic-relation.url}")
    private String NVLI_SEMANTIC_RELATIONS_URL;

    /**
     * method get count of records for given class iri or class name.
     * <p>
     * Communicate with NVLI-Semantic-Relation webservice using
     * NVLI_SEMANTIC_RELATIONS_URL for count.</p>
     *
     * @param classIRI Class IRI or class name which will be used for getting.
     * @param ontId Selected reference ontology for resolving name space URI.
     * @return number of record count for given class name
     */
    @SuppressWarnings("unchecked")
    public Integer wsEndPointForCount(String classIRI, String ontId) {
        Integer count = 0;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();

        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getClassRecordCount?refOntId=" + ontId + "&classIRI=" + classIRI;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for count");
        try {
            ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Integer.class);
            count = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return count;
    }

    /**
     * method get records for given class iri or class name.
     * <p>
     * Communicate with NVLI-Semantic-Relation webservice using
     * NVLI_SEMANTIC_RELATIONS_URL for records.</p>
     *
     * @param classIRI Class IRI or class name which will be used for getting.
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @param ontId Selected reference ontology for resolving name space URI.
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> wsEndPointForRecord(String classIRI, Integer pageNum, Integer pageWin, String ontId) {
        Map<String, String> map = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getRecords/" + pageNum + "/" + pageWin + "?refOntId=" + ontId + "&classIRI=" + classIRI;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for records");
        try {
            ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Map.class);
            map = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return map;
    }

    /**
     * Give HttpHeaders object to communicate with NVLISEMATICRELATIONS.
     *
     * @return object of HttpHeaders.
     */
    private HttpHeaders getHttpHeadersforNVLISEMATICRELATIONS() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "NVLI Internal Web Service");
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    /**
     * Returns the list of all Domain Ontology for listing on main page.
     *
     * @return
     */
    public List<DomainOntology> getDomainOntologies() {
        List<DomainOntology> domainOntologys = new ArrayList<>();
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getDomainOnts";
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Domain Ontologies");
        try {
            ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
            domainOntologys = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return domainOntologys;
    }

    /**
     *
     * Returns count of Authors depends on filter criteria like Resource Type,
     * Organization, Publisher, Alfabetical Sorting or Search On Author name.
     *
     * @param resType
     * @param organization
     * @param publisher
     * @param author
     * @param strAlfa
     * @return
     */
    public Integer getCountAllAuthor(String resType, String organization, String publisher, String author, String strAlfa) {
        Integer jsonAuthorCount = 0;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getAllAuthorCount?organization=" + organization + "&resType=" + resType + "&publisher=" + publisher + "&author=" + author + "&strAlfa=" + strAlfa;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Author Count");
        try {
            ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Integer.class);
            jsonAuthorCount = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return jsonAuthorCount;
    }

    /**
     * Returns list of Authors depends on filter criteria like Resource Type,
     * Organization, Publisher, Alfabetical Sorting or Search On Author name.
     *
     * @param resType
     * @param organization
     * @param publisher
     * @param strAlfa
     * @param srchAuthor
     * @param pageNum
     * @param pageWin
     * @return
     */
    public String getAllAuthors(String resType, String organization, String publisher, String strAlfa, String srchAuthor, int pageNum, int pageWin) {
        String jsonAuthors = "";
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getAllAuthors/" + pageNum + "/" + pageWin + "?organization=" + organization + "&resType=" + resType + "&publisher=" + publisher + "&strAlfa=" + strAlfa + "&srchAuthor=" + srchAuthor;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Authors");
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            jsonAuthors = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return jsonAuthors;
    }

    /**
     * Returns list of Co-Authors related to selected Author.
     *
     * @param autherIRI
     * @param pageNum
     * @param pageWin
     * @return
     */
    public String getCoAuthors(String autherIRI, int pageNum, int pageWin) {
        String jsonAuthors = "";
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getCoAuthors/" + pageNum + "/" + pageWin + "?authorIRI=" + autherIRI;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Co Authors");
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            jsonAuthors = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return jsonAuthors;
    }

    /**
     *
     * Returns count of Records related to selected Co-Authors.
     *
     * @param authorIRI
     * @param organizationIRI
     * @return
     */
    public Integer getCountOfRecordsForAuthor(ArrayList<String> authorIRI, String organizationIRI) {
        Integer jsonAuthorCount = 0;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getCountOfRecordForAuthor?organizationIRI=" + organizationIRI + "&authorIRI=" + authorIRI;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Records with Author Count");
        try {
            ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Integer.class);
            jsonAuthorCount = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return jsonAuthorCount;
    }

    /**
     * Returns list of Records related to selected Co-Authors.
     *
     * @param authorIRI
     * @param organizationIRI
     * @param pageNum
     * @param pageWin
     * @return
     */
    public Map<String, String> getRecordsForAuthor(ArrayList<String> authorIRI, String organizationIRI, int pageNum, int pageWin) {
        Map<String, String> map = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getRecordsForAuthor/" + pageNum + "/" + pageWin + "?organizationIRI=" + organizationIRI + "&authorIRI=" + authorIRI;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for records");
        try {
            ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Map.class);
            map = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return map;
    }

    /**
     *
     * Returns list of Resources.
     *
     * @return
     */
    public Set<String> getResourceType() {
        Set<String> set = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getResourceType";
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Resource type.");
        try {
            ResponseEntity<Set> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Set.class);
            set = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return set;
    }

    /**
     *
     * Returns list of Organizations related to Resource Type.
     *
     * @param resType
     * @return
     */
    public List<Organization> getOrganizationsForResource(String resType) {
        List<Organization> orgList = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getOrgForResourceType/" + resType;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Resource type.");
        try {
            ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
            orgList = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return orgList;
    }

    /**
     * Returns list of all Publishers.
     *
     * @param subStr
     * @param length
     * @return
     */
    public JSONObject getPublisher(String subStr, int length) {
        Set<String> publisherSet = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getPublishers/" + length + "?subStr=" + subStr;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Publisher List.");
        try {
            ResponseEntity<Set> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Set.class);
            publisherSet = responseEntity.getBody();
            JSONObject authorJSONObject = new JSONObject();
            JSONArray authorJSONArray = new JSONArray();
            for (String publisher : publisherSet) {
                JSONObject tempJSONObject = new JSONObject();
                tempJSONObject.put("data", publisher);
                tempJSONObject.put("value", publisher);
                authorJSONArray.add(tempJSONObject);
            }
            authorJSONObject.put("query", subStr);
            authorJSONObject.put("suggestions", authorJSONArray);
            return authorJSONObject;
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
    }

    /**
     *
     * Returns list of Authors for Suggestions for Searching.
     *
     * @param subStr
     * @param length
     * @return
     */
    public JSONObject getSuggestions(String subStr, int length) {
        Set<String> authorsSet = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/sparql/getSuggestions/" + length + "?subStr=" + subStr;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Authors Suggetions");
        try {
            ResponseEntity<Set> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Set.class);
            authorsSet = responseEntity.getBody();
            JSONObject authorJSONObject = new JSONObject();
            JSONArray authorJSONArray = new JSONArray();
            for (String author : authorsSet) {
                JSONObject tempJSONObject = new JSONObject();
                tempJSONObject.put("data", author);
                tempJSONObject.put("value", author);
                authorJSONArray.add(tempJSONObject);
            }
            authorJSONObject.put("query", subStr);
            authorJSONObject.put("suggestions", authorJSONArray);
            return authorJSONObject;
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
    }

    /**
     *
     * @param userID
     * @param refOntId
     * @param className
     * @return
     */
    public List<UserDefinedCustomLabelDto> getCustomLabelForRefClassByUser(Long userID, Long refOntId, String className) {
        List<UserDefinedCustomLabelDto> lstLbls = new ArrayList<>();
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/labels/users/" + userID + "?domOntId=" + refOntId + "&classname=" + className; //domOntId
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for Custom Lables.");
        try {
            ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
            lstLbls = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return lstLbls;
    }

    public List<UserDefinedCustomLabelDto> postCustomClasLbl(List<UserDefinedCustomLabelDto> postLstLbls) {
        List<UserDefinedCustomLabelDto> lstLbls = new ArrayList<>();
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity(postLstLbls, headers);
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/labels";
        LOG.info("Accessing " + url + " for Custom Lables.");
        try {
            lstLbls = restTemplate.postForObject(url, httpEntity, List.class);
        } catch (Exception e) {
            LOG.error(e, e);
            throw e;
        }
        return lstLbls;
    }

    public ResponseEntity postCustomLblWithRecord(MultipleBookmarkForm bookmarkForm) {
        ResponseEntity responseEntity;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity(bookmarkForm, headers);
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/bookmarks/multi";
        LOG.info("Accessing " + url + " for Posting tagged records for custom lable.");
        try {
            responseEntity = restTemplate.postForObject(url, httpEntity, ResponseEntity.class);
        } catch (Exception e) {
            LOG.error(e, e);
            throw e;
        }
        return responseEntity;
    }

    /**
     *
     * @param userId
     * @param custLblIds
     * @return
     */
    @SuppressWarnings("unchecked")
    public Long getRecordCountForCustomLbl(Long userId, List<Long> custLblIds) {
        Long count;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();

        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/getTaggedRecordCount?userID=" + userId;
        System.out.println(">>> " + url);
        HttpEntity request = new HttpEntity(custLblIds, headers);
        LOG.info("Accessing " + url + " for count");
        try {
            count = restTemplate.postForObject(url, request, Long.class);
        } catch (Exception e) {
            LOG.error(e, e);
            throw e;
        }
        return count;
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getRecordsForCustomLbl(Long userId, List<Long> custLblIds, int pageNum, int pageWin) {
        Map<String, String> map = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/getTaggedRecords/" + pageNum + "/" + pageWin + "?userID=" + userId;
        HttpEntity request = new HttpEntity(custLblIds, headers); //custLblIds
        LOG.info("Accessing " + url + " for records");
        try {
            map = restTemplate.postForObject(url, request, Map.class);
            //map = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return map;
    }

    public List<String> getMyTaggedClassList(Long userId, Long domainId) {
        List<String> myTaggedClasses = null;
        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/myTaggedClassList/" + userId + "/" + domainId;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for records");
        try {
            ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
            myTaggedClasses = responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }
        return myTaggedClasses;
    }

    public boolean deleteMyCustomLbl(Long userId, Long lblId) {

        HttpHeaders headers = getHttpHeadersforNVLISEMATICRELATIONS();
        RestTemplate restTemplate = new RestTemplate();
        String url = NVLI_SEMANTIC_RELATIONS_URL + "/custom/deleteMyCustomTag/" + userId + "/" + lblId;
        HttpEntity request = new HttpEntity(headers);
        LOG.info("Accessing " + url + " for delete custom label");
        try {
            ResponseEntity<Boolean> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, request, Boolean.class);
            return responseEntity.getBody();
        } catch (RestClientException e) {
            LOG.error(e, e);
            throw e;
        }

    }
}
