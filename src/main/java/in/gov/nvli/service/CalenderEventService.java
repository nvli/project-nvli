/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.service;

import in.gov.nvli.dao.widget.CalenderEventDAO;
import in.gov.nvli.domain.widget.CalenderEvent;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ankita Dhongde <dankita@cdac.in>
 */
@Service
public class CalenderEventService {

    @Autowired
    CalenderEventDAO calenderEventDAO;

    private final static Logger LOG = Logger.getLogger(CalenderEventService.class);

    public List<CalenderEvent> getcalenderEventByFiltersWithLimit(String searchString, int dataLimit, int pageNo) throws Exception {
        List<CalenderEvent> calenderEvent = new ArrayList<>();
        for (CalenderEvent calenderEvents : calenderEventDAO.getEventByFiltersWithLimit(searchString, dataLimit, pageNo)) {
            CalenderEvent event = new CalenderEvent();
            event.setTitle(calenderEvents.getTitle());
            event.setId(calenderEvents.getId());
            event.setDescription(calenderEvents.getDescription());
            event.setStartDate(calenderEvents.getStartDate());
            event.setEndDate(calenderEvents.getEndDate());
            calenderEvent.add(event);
        }

        return calenderEvent;
    }

    public long getEventsTotalCountByFilters(String searchString) throws Exception {
        return calenderEventDAO.getEventsTotalCountByFilters(searchString);
    }

    public CalenderEvent getEventByEventId(Long eventId) {
        return calenderEventDAO.get(eventId);
    }

    public void updateCalenderEvent(CalenderEvent calEvent) {
        calenderEventDAO.update(calEvent);
    }

    public void removeCalenderEvent(CalenderEvent calEvent) {
        calenderEventDAO.delete(calEvent);
    }

    public JSONArray getCalenderEventList() {
        List<CalenderEvent> calEvents = calenderEventDAO.list();
        JSONArray array = new JSONArray();
        for (CalenderEvent calEvent : calEvents) {
            JSONObject obj = new JSONObject();
            obj.put("id", calEvent.getId());
            obj.put("title", calEvent.getTitle());
            obj.put("start", calEvent.getStartDate());
            obj.put("end", calEvent.getEndDate());
            array.add(obj);
        }
        return array;
    }
}
