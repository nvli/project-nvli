package in.gov.nvli.service;

import in.gov.nvli.dao.crowdsource.ILanguageDAO;
import in.gov.nvli.dao.resource.IContentTypeDAO;
import in.gov.nvli.dao.resource.IENewsDAO;
import in.gov.nvli.dao.resource.IGroupTypeDAO;
import in.gov.nvli.dao.resource.IMetadataIntegratorDAO;
import in.gov.nvli.dao.resource.IOpenRepositoryDAO;
import in.gov.nvli.dao.resource.IOrganizationDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceStatusDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.dao.resource.IStateDAO;
import in.gov.nvli.dao.resource.IThematicTypeDAO;
import in.gov.nvli.dao.resource.IWebCrawlerDAO;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.resource.ContentType;
import in.gov.nvli.domain.resource.ENews;
import in.gov.nvli.domain.resource.GroupType;
import in.gov.nvli.domain.resource.HarRecordMetadata;
import in.gov.nvli.domain.resource.MetadataIntegrator;
import in.gov.nvli.domain.resource.NewsFeed;
import in.gov.nvli.domain.resource.OpenRepository;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceStatus;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.resource.ResourceTypeInfo;
import in.gov.nvli.domain.resource.ThematicType;
import in.gov.nvli.domain.resource.WebCrawler;
import in.gov.nvli.util.Constants;
import in.gov.nvli.util.Helper;
import in.gov.nvli.util.ResultHolder;
import in.gov.nvli.webserviceclient.mit.ResourceDetailsJson;
import in.gov.nvli.webserviceclient.mit.ResourceTypeInformationDetails;
import in.gov.nvli.webserviceclient.oar.HarRecordMetadataType;
import in.gov.nvli.webserviceclient.oar.HarRepoCustomised;
import in.gov.nvli.webserviceclient.oar.IdentifyType;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

/**
 * This class is to provide services for Resource Addition, Content Type,
 * Language, Thematic Type and Resource Type creation
 *
 * @author Sujata Aher
 * @author Suman Behara
 * @version 1
 * @since 1
 */
@Service
public class ResourceService {

    /**
     * The name of the property contentTypeDAO used to holds
     * {@link IContentTypeDAO} object reference.
     */
    @Autowired
    private IContentTypeDAO contentTypeDAO;
    /**
     * The name of the property resourceTypeDAO used to holds
     * {@link IResourceTypeDAO} object reference.
     */
    @Autowired
    private IResourceTypeDAO resourceTypeDAO;
    /**
     * The name of the property resourceDAO used to holds {@link IResourceDAO}
     * object reference.
     */
    @Autowired
    private IResourceDAO resourceDAO;
    /**
     * The name of the property languageDAO used to holds {@link ILanguageDAO}
     * object reference.
     */
    @Autowired
    private ILanguageDAO languageDAO;
    /**
     * The name of the property thematicTypeDAO used to holds
     * {@link IThematicTypeDAO} object reference.
     */
    @Autowired
    private IThematicTypeDAO thematicTypeDAO;
    /**
     * The name of the property groupTypeDAO used to holds {@link IGroupTypeDAO}
     * object reference.
     */
    @Autowired
    private IGroupTypeDAO groupTypeDAO;
    /**
     * The name of the property oarWSBaseURL used to hold base url of web
     * service of harvest tool.
     */
    @Value("${webservice.oar.base.url}")
    private String oarWSBaseURL;
    /**
     * The name of the property mitWSBaseURL used to hold base url of web
     * service of record with metadata processing tool.
     */
    @Value("${webservice.mit.base.url}")
    private String mitWSBaseURL;
    /**
     * The name of the property restTemplate used to holds {@link RestTemplate}
     * object reference.
     */
    public RestTemplate restTemplate = new RestTemplate();
    /**
     * The name of the property openRepositoryDAO used to holds
     * {@link IOpenRepositoryDAO} object reference.
     */
    @Autowired
    private IOpenRepositoryDAO openRepositoryDAO;
    /**
     * The name of the property eNewsDAO used to holds {@link IENewsDAO} object
     * reference.
     */
    @Autowired
    private IENewsDAO eNewsDAO;
    /**
     * The name of the property metadataIntegratorDAO used to holds
     * {@link IMetadataIntegratorDAO} object reference.
     */
    @Autowired
    private IMetadataIntegratorDAO metadataIntegratorDAO;
    /**
     * The name of the property LOGGER used to holds {@link Logger} object
     * reference.
     */
    private final static Logger LOGGER = Logger.getLogger(ResourceService.class);
    /**
     * The name of the property resourceStatusDAO used to holds
     * {@link IResourceStatusDAO} object reference.
     */
    @Autowired
    private IResourceStatusDAO resourceStatusDAO;
    /**
     * The name of the property resourceIconLocation used to hold the resource
     * icon location
     */
    @Value("${nvli.resource.image.location}")
    private String resourceIconLocation;
    /**
     * The name of the property webCrawlerDAO used to holds
     * {@link IWebCrawlerDAO} object reference.
     */
    @Autowired
    private IWebCrawlerDAO webCrawlerDAO;
    /**
     * The name of the property stateDAO used to holds {@link IStateDAO} object
     * reference.
     */
    @Autowired
    private IStateDAO stateDAO;
    /**
     * The name of the property organizationDAO used to holds
     * {@link IOrganizationDAO} object reference.
     */
    @Autowired
    private IOrganizationDAO organizationDAO;

    /**
     * Create the new Content Type
     *
     * @param contentType {@link ContentType }
     * @return true if created successfully otherwise false.
     */
    public boolean createNewContentType(ContentType contentType) {
        try {
            if (contentTypeDAO.createNew(contentType)) {
                LOGGER.info("Content Type created successfully.");
                return true;
            }
        } catch (Exception e) {
            LOGGER.info("Content Type not created" + e.getMessage());
            return false;
        }
        return false;
    }

    /**
     * Create the new Resource Type
     *
     * @param resourceType {@link ResourceType}
     * @return true if created successfully otherwise false.
     */
    public boolean createNewResourceType(ResourceType resourceType) {
        boolean isSavedToMIT = submitResourceTypeToMIT(resourceType, "save");
        boolean isSaved = false;
        if (isSavedToMIT) {
            isSaved = resourceTypeDAO.createNew(resourceType);
        }
        LOGGER.info("Resource Type is saved in database.");
        return isSaved;
    }

    private HttpHeaders getConfiguredHttpHeaders() throws Exception {
        String credential = "admin:admin";
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * Update the new Resource Type
     *
     * @param resourceType {@link ResourceType}
     * @param resourceTypeId {@link ResourceType} primary key/id
     */
    public ResourceType updateResourceType(ResourceType resourceType, Long resourceTypeId) {
        ResourceType preResourceType = resourceTypeDAO.get(resourceTypeId);
        resourceType.setId(preResourceType.getId());
        resourceType.setTotalRecordCount(preResourceType.getTotalRecordCount());
        resourceType.setPublishFlag(preResourceType.isPublishFlag());
        boolean isUpdateMIT = submitResourceTypeToMIT(resourceType, "update");
        if (isUpdateMIT) {
            resourceTypeDAO.merge(resourceType);
            LOGGER.info("Resource Type is updated in MIT database.");
        } else {
            LOGGER.info("Resource Type is not updated in MIT database.");
        }
        LOGGER.info("Resource Type is updated in database.");
        return resourceType;
    }

    /**
     * Create the new Language
     *
     * @param language {@link Language}
     * @return true if created successfully otherwise false.
     */
    public boolean createNewLanguage(UDCLanguage language) {
        return languageDAO.createNew(language);
    }

    /**
     * Create the new Thematic Type
     *
     * @param thematicType {@link ThematicType}
     * @return true if created successfully otherwise false.
     */
    public boolean createNewThematicType(ThematicType thematicType) {
        thematicTypeDAO.merge(thematicType);
        return true;
    }

    /**
     * Create the new Resource
     *
     * @param resource {@link Resource}
     * @return {@link ResourceStatus}
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     */
    public ResourceStatus createNewResource(Resource resource) throws URISyntaxException, IOException, Exception {
        ResourceStatus resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 2).get(0);//Active status
        resource.setRecordCount(0L);
        if (resource.getResourceIcon() != null && !resource.getResourceIcon().equals("")) {
            String[] fileNameAndExtension = resource.getResourceIcon().split(Constants.REG_EXP_FOR_EXTENSION);
            if (fileNameAndExtension[0].equalsIgnoreCase("temp")) {
                String iconName = resource.getResourceCode() + "." + fileNameAndExtension[1];
                String resourceIconName = saveIcon(resourceIconLocation, iconName);
                resource.setResourceIcon(resourceIconName);
            }
        }
        if (resource.getResourceStatus() == null) {
            resource.setResourceStatus(resourceStatus);
        }
         System.out.println("Resource status is after"+resource.getResourceStatus().getStatusCode());
        resourceDAO.createNew(resource);
//        switch (resource.getResourceType().getResourceTypeCategory().trim()) {
//            case Constants.RESOURCE_TYPE_MIT:
//                if (resource instanceof MetadataIntegrator) {
//
//                   //  short saveStatus = submitEntityToMit((MetadataIntegrator) resource, "save");
//                    // resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatus).get(0);
//                   
//                    metadataIntegratorDAO.createNew((MetadataIntegrator) resource);
//                }
//                break;
//            case Constants.RESOURCE_TYPE_HARVEST:
//                if (resource instanceof OpenRepository) {
//                    //if (resource.getResourceStatus() != null && resource.getResourceStatus().getStatusCode() == (short) 1) {
//                    // short saveStatus = submitEntityToMit((OpenRepository) resource, "save");
//                    // if (saveStatus == (short) 1) {
//                    //    short saveStatusOR = submitEntityToHarvester((OpenRepository) resource);
//                    //  resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatusOR).get(0);
//                    // } else {
//                    // resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatus).get(0);
//                    //}
//                    //  } else {
//                    // resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 8).get(0);
//                    //  resource.setResourceStatus(resourceStatus);
//                    //}
//                    resource.setResourceStatus(resourceStatus);
//                    openRepositoryDAO.createNew((OpenRepository) resource);
//                }
//                break;
//            case Constants.RESOURCE_TYPE_ENEWS:
//                //short saveStatus = submitEntityToMit((ENews) resource, "save");
//                // resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatus).get(0);
//                // if (resource instanceof ENews) {
//                resource.setResourceStatus(resourceStatus);
//                eNewsDAO.createNew((ENews) resource);
//                // }
//                break;
//            case Constants.RESOURCE_TYPE_WEB_CRAWLER:
//                // short saveStatusW = submitEntityToMit((WebCrawler) resource, "save");
//                //  resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatusW).get(0);
//                //  if (resource instanceof WebCrawler) {
//                resource.setResourceStatus(resourceStatus);
//                webCrawlerDAO.createNew((WebCrawler) resource);
//                //  }
//                break;
//        }
        return resourceStatus;
    }

    /**
     * To populate the add resource form, add initial objects
     *
     * @return updated {@link ModelAndView} object
     */
    public ModelAndView populateAddResourceForm() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("resourceTypeList", getResouceTypeList());
        mav.addObject("resource", new Resource());
        return mav;
    }

    /**
     * To populate the resource list
     *
     * @return {@link ModelAndView}
     */
    public ModelAndView resourcePopulateList() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("resourceList", resourceDAO.list());
        return mav;
    }

    /**
     * Get the {@link ContentType} list
     *
     *
     * @return {@link ContentType} list
     */
    public List<ContentType> getContentTypeList() {
        List<ContentType> contentTypeList = contentTypeDAO.list();
        Collections.sort(contentTypeList, ContentType.ContentComparator);
        return contentTypeList;
    }

    /**
     * Get the {@link ResourceType} list
     *
     *
     * @return {@link ResourceType} list
     */
    public List<ResourceType> getResouceTypeList() {
        List<ResourceType> resourceTypeList = resourceTypeDAO.list();
        Collections.sort(resourceTypeList, ResourceType.ResourceTypeComparator);
        return resourceTypeList;
    }

    /**
     * Get the {@link ThematicType} list
     *
     *
     * @return {@link ThematicType} list
     */
    public List<ThematicType> getThematicTypeList() {
        List<ThematicType> thematicTypeList = thematicTypeDAO.list();
        Collections.sort(thematicTypeList, ThematicType.ThemesComparator);
        return thematicTypeList;
    }

    /**
     * Get the Indian {@link UDCLanguage} List
     *
     *
     * @return {@link UDCLanguage} List
     * @throws java.lang.Exception
     */
    public List<UDCLanguage> getIndianLanguageList() throws Exception {
        List<UDCLanguage> languageList = languageDAO.getIndianLanguageList();//languageDAO.list()
        Collections.sort(languageList, UDCLanguage.LanguageComparator);
        return languageList;
    }

    /**
     * Get all {@link UDCLanguage} List
     *
     *
     * @return {@link UDCLanguage} List
     * @throws java.lang.Exception
     */
    public List<UDCLanguage> getAllUDCLanguages() throws Exception {
        List<UDCLanguage> languageList = languageDAO.getAllUDCLanguageList();
        Collections.sort(languageList, UDCLanguage.LanguageComparator);
        return languageList;
    }

    /**
     * Get the {@link UDCLanguage} by its primary key/Id
     *
     * @param id {@link UDCLanguage}primary key/id
     * @return {@link UDCLanguage}
     */
    public UDCLanguage getUDCLanguageById(long id) {
        return languageDAO.get(id);
    }

    /**
     * To populate create thematic type form , add initial objects
     *
     * @return {@link ModelAndView} object
     */
    public ModelAndView populateAddThematicType() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("resourceTypeList", getResouceTypeList());
        mav.addObject("thematicTypeList", thematicTypeDAO.list());
        mav.addObject("thematicType", new ThematicType());
        mav.addObject("groupType", new GroupType());
        return mav;
    }

    /**
     * Map the Extracted OAR information to {@link OpenRepository}
     *
     * @param identifyType {@link IdentifyType}
     * @param openRepository {@link OpenRepository}
     * @return updated {@link OpenRepository} object
     */
    public OpenRepository mappingHarvesterToResource(IdentifyType identifyType, OpenRepository openRepository) {
        openRepository.setResourceName(identifyType.getRepositoryName());
        if (identifyType.getAdminEmail() != null && !identifyType.getAdminEmail().isEmpty()) {
            openRepository.setEmail(identifyType.getAdminEmail().get(0));
        } else {
            openRepository.setEmail("");
        }
        openRepository.setEarliestDatestamp(identifyType.getEarliestDatestamp());
        Map<HarRecordMetadataType, Boolean> supportedMetadataTypes = identifyType.getSupportedMetadataTypes();
        //List<HarRecordMetadata> listHarRecordMetadata = getListOfMetadataStandard(supportedMetadataTypes, openRepository);
        openRepository = mappingHarRepoCustomisedToResource(supportedMetadataTypes, openRepository);
        // openRepository.setRecordMetadataStandards(listHarRecordMetadata);
        ResourceStatus status = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 2).get(0);// status is not_active
        openRepository.setResourceStatus(status);
        return openRepository;
    }

    /**
     * Map the Resource data to {@link HarRepoCustomised}
     *
     * @param openRepository {@link OpenRepository}
     * @return HarRepoCustomised {@link HarRepoCustomised} updated object
     * @throws java.io.IOException
     */
    public HarRepoCustomised mappingResourceToHarRepoCustomised(OpenRepository openRepository) throws IOException {
        HarRepoCustomised harRepoCustomised = new HarRepoCustomised();
        harRepoCustomised.setRepoBaseUrl(openRepository.getHarvestUrl());
        harRepoCustomised.setRepoUID(openRepository.getResourceCode());
        harRepoCustomised.setRepoEmail(openRepository.getEmail());
        harRepoCustomised.setRepoLink(openRepository.getRepositoryUrl());
        harRepoCustomised.setRepoName(openRepository.getResourceName());
        harRepoCustomised.setRepoStatusId(openRepository.getResourceStatus().getStatusCode());
        List<HarRecordMetadata> listOfMetadata = openRepository.getRecordMetadataStandards();
        Map<HarRecordMetadataType, Boolean> mapOfMetatData = new HashMap<>();
        if (listOfMetadata != null) {
            for (HarRecordMetadata harRecordMetadata : listOfMetadata) {
                mapOfMetatData.put(harRecordMetadata.getMetadataType(), harRecordMetadata.isMetadataEnabled());
            }
        }
        harRepoCustomised.setSupportedMetadataTypes(mapOfMetatData);
        System.out.println("harcust.." + harRepoCustomised.getRepoName());
        return harRepoCustomised;
    }

    /**
     * Check the URl status
     *
     * @param url URL to check the status.
     * @param id {@link Resource} primary key/id
     * @return {@link String } status code for 200 and error message for other
     * situations.
     */
    public String checkURLStatus(String url, Long id) throws NoSuchAlgorithmException, KeyManagementException {
        String urlStatus;
        if (!url.startsWith("http")) {
            url = "http://" + url;
        }
        boolean isDuplicate = checkDuplicateURL(url, id);
        if (!isDuplicate) {
            urlStatus = checkURLConnectivity(url);
        } else {
            urlStatus = "URL is duplicate.";
        }
        LOGGER.info(urlStatus + " " + url);
        return urlStatus;
    }

    /**
     * Check url is duplicate or not.
     *
     * @param url to check the status.
     * @return true if found duplicate otherwise false.
     */
    private boolean checkDuplicateURL(String url, Long resourceId) {
        boolean isDuplicate;
        try {
            List<OpenRepository> listResource = openRepositoryDAO.findbyQuery("findResourceByHarvestURL", url);
            if (listResource == null || listResource.isEmpty()) {
                isDuplicate = false;
            } else if (listResource.get(0).getId().equals(resourceId)) {
                isDuplicate = false;
            } else {
                LOGGER.info("Open repository url " + url + " is duplicate");
                isDuplicate = true;
            }
        } catch (Exception e) {
            isDuplicate = true;
        }
        return isDuplicate;
    }

    private TrustManager[] get_trust_mgr() {
        TrustManager[] dummyTrustManager = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        return dummyTrustManager;
    }

    /**
     * Check url live or not.
     *
     * @param url to check the status.
     * @return {@link String } status code
     */
    private String checkURLConnectivity(String url) throws NoSuchAlgorithmException, KeyManagementException {
        //

        String urlStatus = null;
        int responseCode;
        try {
            if (url.startsWith("https")) {
                System.out.println("for https ");
                // Create a context that doesn't check certificates.
                SSLContext ssl_ctx = SSLContext.getInstance("SSL");
                TrustManager[] trust_mgr = get_trust_mgr();
                ssl_ctx.init(null, // key manager
                        trust_mgr, // trust manager
                        new SecureRandom()); // random number generator
                HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
                HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
                connection.setRequestMethod("HEAD");
                connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                connection.setReadTimeout(Constants.CONNECTION_TIMEOUT);
                responseCode = connection.getResponseCode();

            } else {
                System.out.println("for http ");
                HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
                connection.setRequestMethod("HEAD");
                connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                connection.setReadTimeout(Constants.CONNECTION_TIMEOUT);
                responseCode = connection.getResponseCode();

            }

            if (responseCode == 200) {
                urlStatus = "200";
            } else if (responseCode == 503) {
                urlStatus = "The server is currently unavailable. Could not get the information.";
            } else {
                urlStatus = "Please check URL.";
            }
        } catch (SocketTimeoutException st) {
            urlStatus = "Connection time out. Please check URL";
            LOGGER.error(urlStatus, st);
        } catch (UnknownHostException un) {
            urlStatus = "For this url host is unkown. Please check URL";
            LOGGER.error(urlStatus, un);
        } catch (MalformedURLException ex) {
            urlStatus = "URL is invalid. Please check URL";
            LOGGER.error(urlStatus, ex);
        } catch (IOException ex) {
            urlStatus = "URL is invalid. Please check URL";
            LOGGER.error(urlStatus, ex);
        }
        LOGGER.info(urlStatus);
        return urlStatus;
    }

    /* Create the new {@link GroupType}
     *
     * @param groupType {@link GroupType}
     * @return true if created successfully otherwise false.
     */
    /**
     *
     * @param groupType
     * @return
     */
    public boolean createNewGroupType(GroupType groupType) {
        groupTypeDAO.merge(groupType);
        return true;
    }

    /**
     * Get the {@link GroupType} List
     *
     *
     * @return list of {@link GroupType}.
     */
    public List<GroupType> getGroupTypeList() {
        List<GroupType> groupTypeList = groupTypeDAO.list();
        Collections.sort(groupTypeList, GroupType.GroupTypeComparator);
        return groupTypeList;
    }

    /**
     * Service method to get the information of Open repository through web
     * service.
     *
     * @param openRepository {@link OpenRepository}
     * @return updated {@link OpenRepository} object
     * @throws java.io.UnsupportedEncodingException
     */
    public OpenRepository getOpenRepoInfo(OpenRepository openRepository) throws UnsupportedEncodingException, Exception {
        String baseURL = openRepository.getHarvestUrl().trim();

        ResourceStatus status = openRepository.getResourceStatus();
        IdentifyType identifyType = communicateWithHarvester(baseURL);//restTemplate. (harvestURL, IdentifyType.class);
        if (identifyType != null) {
            if (status != null && status.getStatusCode() != 8 && status.getStatusCode() != 9) {
                openRepository = mappingHarRepoCustomisedToResource(identifyType.getSupportedMetadataTypes(), openRepository);
            } else {
                openRepository = mappingHarvesterToResource(identifyType, openRepository);
            }

        } else {
            if (status.getStatusCode() == 8 && status.getStatusCode() == 9) {
                ResourceStatus status1 = resourceStatusDAO.findbyQuery("findByStatusCode", 8).get(0);// status is invalid_url
                openRepository.setResourceStatus(status1);
            }
        }
        openRepository.setHarvestUrl(baseURL);
        return openRepository;
    }

    /**
     * communicate to web service to get information.
     *
     * @param baseURL Repository url
     * @return {@link IdentifyType}
     * @throws java.io.UnsupportedEncodingException
     */
    public IdentifyType communicateWithHarvester(String baseURL) throws UnsupportedEncodingException, Exception {
        String applicationURL = oarWSBaseURL + "identifies/";
        String encodeBaseURL = URLEncoder.encode(baseURL, "UTF-8");
        String harvestURL = applicationURL + "?baseURL=" + encodeBaseURL;
        System.out.println("harvestURL" + harvestURL);
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        IdentifyType identifyType = restTemplate.postForObject(harvestURL, request, IdentifyType.class);
        return identifyType;
    }

    /**
     * Save 'OR resource' at Harvester through web service.
     *
     * @param openRepository {@link OpenRepository}
     * @return status code
     */
    public String submitEntityToHarvester(OpenRepository openRepository, String eventType) throws Exception {
        String url = null;
        short statusCode;
        String status = null;
        try {
            if (eventType.equalsIgnoreCase("harvest")) {
                url = oarWSBaseURL + "SaveAndHarvestRepository";
            } else if (eventType.equalsIgnoreCase("update")) {

                url = oarWSBaseURL + "UpdateAndStartIncremantalHarvesting";

            }
            URI webServiceURL = new URI(url);
            HarRepoCustomised harRepoCustomised = mappingResourceToHarRepoCustomised(openRepository);
            HttpEntity request = new HttpEntity(harRepoCustomised, getConfiguredHttpHeaders());
            status = restTemplate.postForObject(webServiceURL, request, String.class);
//            if (responseHarRepoCustomised != null) {
//                statusCode = 1; //Not Active
            System.out.println("status in harest save" + status);
            LOGGER.info("Resource status is " + status);
//            } else {
//                statusCode = 4; //error in save
//                LOGGER.info("Repository not saved successfully at Harvester.");
//            }
        } catch (URISyntaxException | IOException | RestClientException e) {
            System.out.println("Unable to process repository");
            LOGGER.info("Unable to process repository.");
            statusCode = 9; //error in save;
            LOGGER.error("Error in webservice " + url + " communication. ", e);
        }
        return status;
    }

    /**
     * Generate the resource code.
     *
     * @param firstCode
     * @param resourceTypeId
     * @return
     * @deprecated
     */
    public String generateResourceCode(String firstCode, Long resourceTypeId) {
        AtomicLong atm = new AtomicLong(0);
        try {
            if (Helper.getIdCounter().get() == atm.get()) {
                LOGGER.info("Check previous resource code.");
                List<Resource> resourceList = resourceDAO.list();
                if (resourceList != null && !resourceList.isEmpty()) {
                    Resource resource1 = resourceDAO.getLastInsertedResource(resourceTypeId);
                    if (resource1 != null) {
                        String resourceCode = resource1.getResourceCode();
                        int maxResourceCode = Integer.valueOf(resourceCode.replaceFirst(firstCode, ""));
                        atm = new AtomicLong(maxResourceCode + 1);
                        Helper.setIdCounter(atm);
                    }
                }
            }
        } catch (NumberFormatException numEx) {
            LOGGER.error("Exception in generating resource code. ", numEx);
        }
        String resourceCode = firstCode + Helper.createID();
        LOGGER.info("code in generateResourceCode------>" + resourceCode);
        return resourceCode;
    }

    /**
     * To perform <ul><li>Start harvesting</li><li>Activate Repository
     * URL</li><li>Deactivate Repository URL</li><li>Update/Incremental
     * Harvesting</li></ul> through web-service
     *
     * @param oarResource object of {@link Resource}
     * @param eventType type of event
     * @return updated {@link Resource} object.
     * @throws java.lang.Exception
     */
    public String operationForHarvestURL(Resource oarResource, String eventType) throws Exception {
        String applicationURL = null;
        String resourceCode;
        String eventStatus = null;
        if (oarResource != null) {
            resourceCode = oarResource.getResourceCode();
            switch (eventType) {
                case "harvest":
                    applicationURL = oarWSBaseURL + "harvest/";
                    break;
                case "update":
                    applicationURL = oarWSBaseURL + "incremental_harvest/";
                    break;
                default:
                    return "none";
            }
            String harvestURL = applicationURL + resourceCode;
            HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
            eventStatus = restTemplate.postForObject(harvestURL, request, String.class);
            LOGGER.info("Resource status is " + oarResource.getResourceStatus().getStatusName());
        }
        return eventStatus;
    }

    /**
     * To Edit Group Type
     *
     * @param groupType {@link GroupType}
     * @return boolean
     */
    public boolean editGroup(GroupType groupType) {
        groupTypeDAO.merge(groupType);
        return true;
    }

    /**
     * To Remove Group {@link GroupType}
     *
     * @param groupType {@link GroupType}
     */
    public void removeGroup(GroupType groupType) {
        groupTypeDAO.delete(groupType);
    }

    /**
     * To Edit Thematic
     *
     * @param thematicType {@link ThematicType}
     * @return ThematicType Object
     */
    public boolean editThematic(ThematicType thematicType) {
        thematicTypeDAO.merge(thematicType);
        return true;
    }

    /**
     * To Remove Thematic Type
     *
     * @param thematicType {@link ThematicType}
     */
    public void removeThematic(ThematicType thematicType) {
        thematicTypeDAO.delete(thematicType);
    }

    /**
     * Method used to get Information from Resource Object and mapping into
     * ResourceDetailsJson Object
     *
     * @param resource {@link Resource}
     * @return {@link ResourceDetailsJson}
     */
    public ResourceDetailsJson mappingResourceToMitBasicDetails(MetadataIntegrator resource) {
        ResourceDetailsJson resourceDetail = new ResourceDetailsJson();
        resourceDetail.setEstablishYear(resource.getEstablishYear());
        resourceDetail.setTotalCount(resource.getTotalCount());
        resourceDetail.setClassificationLevel(resource.getClassificationLevel());
        return resourceDetail;
    }

    /**
     * Save MIT Resource through web service.
     *
     * @param resource {@link Resource}
     * @param event
     * @return HttpStatus code
     */
    public short submitEntityToMit(Resource resource, String event) {
        short statusCode;
        String url = null;

        try {
            ResourceDetailsJson resourceDetail = null;
            if (resource.getResourceType().getResourceTypeCategory().equalsIgnoreCase(Constants.RESOURCE_TYPE_MIT)) {
                resourceDetail = mappingResourceToMitBasicDetails((MetadataIntegrator) resource);
            }
            resourceDetail = mappingOtherResourceToMitBasicDetails(resource, resourceDetail);
            HttpEntity<String> request = new HttpEntity(resourceDetail, Helper.mitWebserviceAthentication());
            url = mitWSBaseURL + "resource/" + event;
            URI webServiceURL = new URI(url);
            ResponseEntity<Boolean> responseEntityMit = restTemplate.postForEntity(webServiceURL, request, Boolean.class);
            if (responseEntityMit.getBody()) {
                LOGGER.info("MIT data saved successfully at Client Side.");
                statusCode = 1; //Not Active
            } else {
                LOGGER.info("MIT data  not saved successfully at Client Side.");
                statusCode = 13; //error in save
            }
        } catch (URISyntaxException | RestClientException e) {
            LOGGER.error("Error in webservice " + url + " communication. ", e);
            statusCode = 13; //error in save;
        }
        return statusCode;
    }

    /**
     * To perform <ul> <li>Activate Digital Library URL</li> <li>Deactivate
     * Digital Library URL</li> <li>Publish</li> <li>unPublish</li> </ul>
     * through web-service
     *
     * @param libraryResource object of{@link MetadataIntegrator}
     * @param eventType type of event
     * @return updated {@link Resource} object.
     * @throws URISyntaxException {@link java.net.URISyntaxException}
     * @throws IOException {@link java.io.IOException}
     */
    public boolean operationsForMiddleware(String eventType, String resourceCode) throws Exception {
        String URL = null;
        HttpHeaders headers = Helper.mitWebserviceAthentication();
        HttpEntity<String> request = new HttpEntity(headers);

        URL = mitWSBaseURL + "resource/" + eventType + "/" + resourceCode;
        URI applicationURL = new URI(URL);
        ResponseEntity<Boolean> b = restTemplate.postForEntity(applicationURL, request, Boolean.class);
        return b.getBody();
    }

    /**
     * Harvest All Repositories.
     *
     * @return resource type primary key/id.
     * @throws java.lang.Exception
     */
    public Long harvestAllURLS() throws Exception {
        Long resourceTypeId = null;
        String applicationURL = oarWSBaseURL + "harvest";
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        restTemplate.postForObject(applicationURL, request, String.class);
        return resourceTypeId;
    }

    /**
     * Operation for selected repositories. <ul> <li>Remove</li>
     * <li>Harvest</li> </ul>
     *
     * @param eventType type of operation
     * @param selectedRepos selected {@link Resouce} codes
     */
    public void operationSelectedHarvestURL(String eventType, String selectedRepos) {
        try {
            String applicationURL = null;
            switch (eventType) {
                case "remove":
                    List<String> listOdResourceCode = Arrays.asList(selectedRepos.split("\\s*,\\s*"));
                    for (String resourceCode : listOdResourceCode) {
                        resourceDAO.deleteByResourceCode(resourceCode);
                    }
                    break;
                case "harvest":
                    applicationURL = oarWSBaseURL + "harvest/";
                    String harvestURL = applicationURL + selectedRepos;
                    HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
                    String eventStatus = restTemplate.postForObject(harvestURL, request, String.class);
                    LOGGER.info("operationSelectedHarvestURL status is " + eventStatus);
                    break;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * To perform <ul> <li>Activate E-News</li> <li>Deactivate E-News</li>
     * <li>Publish</li> <li>unPublish</li> </ul> through web-service
     *
     * @param eNewsResource object of {@link ENews}
     * @param eventType type of operation to perform
     * @return updated {@link ENews} object.
     * @throws java.lang.Exception
     */
    public boolean operationsForENewsURL(ENews eNewsResource, String eventType) throws Exception {
        boolean isSuccess = false;
        if (eNewsResource != null) {
            switch (eventType) {
                case "activate":
                    eNewsResource.setActive(true);
                    break;
                case "deactivate":
                    eNewsResource.setActive(false);
                    break;
                case "publish":
                    eNewsResource.setPublish(true);
                    break;
                case "unpublish":
                    eNewsResource.setPublish(false);
                    break;
            }
            LOGGER.info("Resource status is " + eNewsResource.getResourceStatus().getStatusName());
            ENews enews = eNewsDAO.merge(eNewsResource);

        }
        return isSuccess;
    }

    /**
     * Pre defined software list for harvester.
     *
     * @return {@link List}
     */
    public List getSoftwareList() {
        List<String> sftList = new ArrayList();
        sftList.add("CONTENTdm");
        sftList.add("Digibib");
        sftList.add("Digital Commons");
        sftList.add("DigiTool");
        sftList.add("Diva-Portal");
        sftList.add("dLibra");
        sftList.add("Drupal");
        sftList.add("DSpace");
        sftList.add("Earmas");
        sftList.add("EPrints");
        sftList.add("ETD-db");
        sftList.add("Fedora");
        sftList.add("Fez");
        sftList.add("Greenstone");
        sftList.add("HAL");
        sftList.add("invenio");
        sftList.add("MyCoRe");
        sftList.add("Open Repository");
        sftList.add("OPUS");
        sftList.add("PURE");
        sftList.add("SciELO");
        sftList.add("VITAL");
        sftList.add("WEKO");
        sftList.add("XooNIps");
        sftList.add("[Other]");
        return sftList;
    }

    /**
     * Update resource information.
     *
     * @param resourceId {@link Resource} primary key/id
     * @param currentResource {@link Resource} updated object
     * @throws URISyntaxException {@link java.net.URISyntaxException}
     * @throws IOException {@link java.io.IOException}
     */
    public short editResource(Long resourceId, Resource currentResource) throws URISyntaxException, IOException {
        Resource preResource = resourceDAO.get(resourceId);
        currentResource = setPrevResource(currentResource, preResource);
        short statusCode = submitEntityToMit(currentResource, "save");
        if (statusCode == (short) 1) {
            resourceDAO.merge(currentResource);
        }

        return statusCode;
//        switch (currentResource.getResourceType().getResourceTypeCategory().trim()) {
//            case Constants.RESOURCE_TYPE_MIT:
//                if (currentResource instanceof MetadataIntegrator) {
//                    MetadataIntegrator prevMetadataIntegrator = metadataIntegratorDAO.get(resourceId);
//                    MetadataIntegrator currentMetadataIntegrator = (MetadataIntegrator) currentResource;
//                    currentMetadataIntegrator = (MetadataIntegrator) setPrevResource(currentMetadataIntegrator, prevMetadataIntegrator);
//                    metadataIntegratorDAO.merge(currentMetadataIntegrator);
//                }
//                break;
//            case Constants.RESOURCE_TYPE_HARVEST:
//                if (currentResource instanceof OpenRepository) {
//                    OpenRepository pervOpenRepository = openRepositoryDAO.get(resourceId);
//                    OpenRepository currentOpenRepo = (OpenRepository) currentResource;
//                    currentOpenRepo = (OpenRepository) setPrevResource(currentOpenRepo, pervOpenRepository);
//                    currentOpenRepo.setEarliestDatestamp(pervOpenRepository.getEarliestDatestamp());
//                    currentOpenRepo.setHarvestEndTime(pervOpenRepository.getHarvestEndTime());
//                    currentOpenRepo.setHarvestStartTime(pervOpenRepository.getHarvestStartTime());
//                    openRepositoryDAO.merge(currentOpenRepo);
//                }
//                break;
//            case Constants.RESOURCE_TYPE_ENEWS:
//                if (currentResource instanceof ENews) {
//                    ENews pervENews = eNewsDAO.get(resourceId);
//                    ENews currentENews = (ENews) currentResource;
//                    currentENews = (ENews) setPrevResource(currentENews, pervENews);
//                    eNewsDAO.merge(currentENews);
//                }
//                break;
//            case Constants.RESOURCE_TYPE_WEB_CRAWLER:
//                if (currentResource instanceof WebCrawler) {
//                    WebCrawler pervWebCrawler = webCrawlerDAO.get(resourceId);
//                    WebCrawler currentWebCrawler = (WebCrawler) currentResource;
//                    currentWebCrawler = (WebCrawler) setPrevResource(currentWebCrawler, pervWebCrawler);
//                    webCrawlerDAO.merge(currentWebCrawler);
//                }
//                break;
//        }
//        return currentResource.getResourceStatus();

    }

    /**
     * Set values of previous resource in current resource. Used for Edit
     * resource.
     *
     * @param currentResource {@link Resource} updated object
     * @param prevResource {@link Resource} before edit
     * @return {@link Resource} updated object
     */
    private Resource setPrevResource(Resource currentResource, Resource prevResource) {
        currentResource.setId(prevResource.getId());
        currentResource.setUpdatedOn(Helper.rightNow());
        currentResource.setCreatedOn(prevResource.getCreatedOn());
        currentResource.setActive(prevResource.isActive());
        currentResource.setPublish(prevResource.isPublish());
        currentResource.setResourceStatus(prevResource.getResourceStatus());
        return currentResource;
    }

    /**
     * Total Record Count based on Resource Type
     *
     * @param resourceTypeId {@link ResourceType} primary key/Id
     * @return long as totalRecords
     */
    public long totalRecordCount(Long resourceTypeId) {
        long totalRecords = 0;
        try {
            ResourceType resourceType = resourceTypeDAO.getResourceTypeById(resourceTypeId);
            if (resourceType != null) {
                long recordCount = 0;
                for (Resource resource : resourceType.getResources()) {
                    if (resource.getRecordCount() != null) {
                        recordCount = resource.getRecordCount();
                        totalRecords = totalRecords + recordCount;
                    }
                }
                resourceType.setTotalRecordCount(totalRecords);
                resourceTypeDAO.merge(resourceType);
            }
        } catch (Exception e) {
            LOGGER.error("Error in Edit Resopurce  ", e);
        }
        return totalRecords;
    }

    /**
     * List the Draft repositories.(Invalid_url and partial submitted urls)
     *
     * @param resourceTypeId {@link ResourceType} primary key/Id
     * @return {@link Resource} list
     */
    public List<Resource> getDraftedRepositories(Long resourceTypeId) {
        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        List<Resource> list = resourceDAO.getDraftResource(resourceType);
        return list;
    }

    /**
     * Re-submit repositories.
     *
     * @param reosurceId {@link Resource} primary key/Id
     * @return {@link ResourceStatus}
     */
    public ResourceStatus submitDraftRepository(Long reosurceId) {
        OpenRepository openRepository = openRepositoryDAO.get(reosurceId);
        ResourceStatus resourceStatus = null;
        try {
            short saveStatus = submitEntityToMit(openRepository, "update");
            /**
             * If Open repository is not updated properly through web service of
             * MIT using "Update" then update it using "save" web service. This
             * case is only for Draft open repository.
             */
            if (saveStatus != (short) 1) {
                saveStatus = submitEntityToMit(openRepository, "save");
            }
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatus).get(0);
            if (saveStatus == (short) 1) {
                openRepository.setResourceStatus(resourceStatus);
                String saveStatusOR = submitEntityToHarvester(openRepository, "");
                resourceStatus = resourceStatusDAO.findbyQuery("findByStatusName", saveStatusOR).get(0);
            }
            openRepository.setResourceStatus(resourceStatus);
        } catch (Exception ex) {
            LOGGER.error("Error in resubmit repository : ", ex);
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 9).get(0);
            openRepository.setResourceStatus(resourceStatus);
        }
        openRepositoryDAO.merge(openRepository);
        return resourceStatus;
    }

    /**
     * submitDraftMit Method used re submit MIT resource through web service.
     *
     * @param resourceId {@link Resource} primary key/Id
     * @return {@link ResourceStatus}
     */
    public ResourceStatus submitDraftMit(Long resourceId) {
        MetadataIntegrator metadataIntegrator = metadataIntegratorDAO.get(resourceId);
        ResourceStatus resourceStatus;
        try {
            short saveStatus = submitEntityToMit(metadataIntegrator, "save");
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", saveStatus).get(0);
            metadataIntegrator.setResourceStatus(resourceStatus);
        } catch (Exception ex) {
            LOGGER.error("Error in resubmit repository : ", ex);
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 9).get(0);
            metadataIntegrator.setResourceStatus(resourceStatus);
        }
        metadataIntegratorDAO.merge(metadataIntegrator);
        return resourceStatus;
    }

    /**
     * submitDraftEnews Method used re submit e-news resource through .
     *
     * @param resourceId {@link Resource} primary key/Id
     * @return {@link ResourceStatus}
     */
    public ResourceStatus submitDraftEnews(Long resourceId) {
        ENews enews = eNewsDAO.get(resourceId);
        ResourceStatus resourceStatus;
        try {
            //   short saveStatus = submitEntityToMit(enews, "save");
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 1).get(0);
            enews.setResourceStatus(resourceStatus);

        } catch (Exception ex) {
            LOGGER.error("Error in : ", ex);
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 9).get(0);
            enews.setResourceStatus(resourceStatus);
        }
        eNewsDAO.merge(enews);
        return resourceStatus;
    }

    /**
     * submitDraftWebcrawler Method used re submit Web crawler resource through
     * .
     *
     * @param resourceId {@link Resource} primary key/Id
     * @return {@link ResourceStatus}
     */
    public ResourceStatus submitDraftWebcrawler(Long resourceId) {
        WebCrawler webcrawler = webCrawlerDAO.get(resourceId);
        ResourceStatus resourceStatus;
        try {
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 1).get(0);
            webcrawler.setResourceStatus(resourceStatus);
        } catch (Exception ex) {
            LOGGER.error("Error in : ", ex);
            resourceStatus = resourceStatusDAO.findbyQuery("findByStatusCode", (short) 9).get(0);
            webcrawler.setResourceStatus(resourceStatus);
        }
        webCrawlerDAO.merge(webcrawler);
        return resourceStatus;
    }

    /**
     * Check draft repository base url.
     *
     * @param url repository url
     * @return {@link String } status code
     *
     */
    public String checkDraftURL(String url) throws NoSuchAlgorithmException, KeyManagementException {
        if (!url.startsWith("http")) {
            url = "http://" + url;
        }
        return checkURLConnectivity(url);
    }

    /**
     * To perform <ul><li>Activate Web URL</li><li>Deactivate Repository
     * URL</li><li>Publish</li><li>unPublish</li></ul> through web-service
     *
     * @param webCrawler object of {@link WebCrawler}
     * @param eventType type of operation to perform
     * @return updated {@link WebCrawler} object.
     * @throws java.lang.Exception
     */
    public boolean operationForWebCrawler(WebCrawler webCrawler, String eventType) throws Exception {
        boolean isSuccess = false;
        if (webCrawler != null) {
            switch (eventType) {
                case "activate":
                    webCrawler.setActive(true);
                    break;
                case "deactivate":
                    webCrawler.setActive(false);
                    break;
                case "publish":
                    webCrawler.setPublish(true);
                    break;
                case "unpublish":
                    webCrawler.setPublish(false);
                    break;
            }
            LOGGER.info("Resource status is " + webCrawler.getResourceStatus().getStatusName());
            WebCrawler wc = webCrawlerDAO.merge(webCrawler);
            if (wc != null) {
                isSuccess = true;
            } else {
                isSuccess = false;
            }
        }
        return isSuccess;
    }

    /**
     * Set initial values in ModelAndView for resource form creation and for
     * edit resource.
     *
     * @param mav {@link ModelAndView}
     * @param resourceType {@link ResourceType}
     * @return {@link ModelAndView}
     * @throws Exception {@link Exception}
     */
    public ModelAndView getInitialFieldsForForm(ModelAndView mav, ResourceType resourceType) throws Exception {
        mav.addObject("languageList", getIndianLanguageList());
        mav.addObject("contentTypeList", getContentTypeList());
        mav.addObject("thematicTypeList", getThematicTypeList());
        mav.addObject("groupTypesList", getGroupTypeList());
        mav.addObject("organizationsList", getOrganizationList());
        mav.addObject("stateList", stateDAO.list());
        mav.addObject("resourceTypeList", getResouceTypeList());
        return mav;
    }

    /**
     * update 'OR resource ' at Harvester through web service.
     *
     * @param openRepository {@link ModelAndView}
     * @return Status code
     */
    public short updateEntityToHarvester(OpenRepository openRepository) {
        String url = null;
        short statusCode = 0;
        try {
            url = oarWSBaseURL + "repositories/" + openRepository.getResourceCode();
            HarRepoCustomised harRepoCustomised = mappingResourceToHarRepoCustomised(openRepository);
            HttpHeaders headers = new HttpHeaders();
            HttpEntity request = new HttpEntity(harRepoCustomised, getConfiguredHttpHeaders());
            HttpEntity<HarRepoCustomised> entity = new HttpEntity<>(harRepoCustomised, headers);
            ResponseEntity<HarRepoCustomised> response = restTemplate.exchange(url, HttpMethod.PUT, request, HarRepoCustomised.class);
            if (response.getBody() != null) {
                LOGGER.info("Repository updated successfully at Harvester.");
                statusCode = 1; //Not Active
            } else {
                LOGGER.info("Repository not updated successfully at Harvester.");
                statusCode = 9; //error in save
            }
        } catch (IOException | RestClientException e) {
            LOGGER.error("Error in update webservice " + url + " communication. ", e);
            statusCode = 9; //error in save;
        } catch (Exception e) {
            LOGGER.error("Error in update webservice " + url + " communication. ", e);
            statusCode = 9; //error in save;
        }
        return statusCode;
    }

    /**
     * Get {@link Thematictype}by its primary key/id
     *
     * @param id {@link Thematictype} primary key/id
     * @return {@link Thematictype}
     */
    public ThematicType getThematicTypeById(long id) {
        return thematicTypeDAO.get(id);
    }

    /**
     * Get updated HarRepoCustomised object through webservice.
     *
     * @param openRepository {@link OpenRepository}
     * @return {@link OpenRepository}
     */
    public OpenRepository getInfoHarvesterForEdit(OpenRepository openRepository) {
        String url = null;
        try {
            ResourceStatus status = openRepository.getResourceStatus();
            if ("200".equals(checkURLConnectivity(openRepository.getHarvestUrl()))) {
                openRepository = getOpenRepoInfo(openRepository);
                openRepository.setResourceStatus(status);
            }
        } catch (Exception e) {
            LOGGER.error("Error in information extraction for edit " + url, e);
        }
        return openRepository;
    }

    /**
     * Map the supported Metadata Types from {@link HarRepoCustomised} to
     * {@link OpenRepository}
     *
     * @param harRepoCustomised {@link HarRepoCustomised}
     * @param openRepository {@link OpenRepository}
     * @return {@link OpenRepository} updated object
     */
    public OpenRepository mappingHarRepoCustomisedToResource(Map<HarRecordMetadataType, Boolean> supportedMetadataTypes, OpenRepository openRepository) {
        System.out.println("in mappingHarRepoCustomisedToResourcedfd");
        List<HarRecordMetadata> notPresentlist = new ArrayList();
        //Map<HarRecordMetadataType, Boolean> supportedMetadataTypes = harRepoCustomised.getSupportedMetadataTypes();
        List<HarRecordMetadata> listHarRecordMetadata = getListOfMetadataStandard(supportedMetadataTypes, openRepository);
        if (openRepository.getRecordMetadataStandards() != null && !openRepository.getRecordMetadataStandards().isEmpty()) {
            for (HarRecordMetadata harRecordMetadata1 : listHarRecordMetadata) {
                System.out.println(openRepository.getRecordMetadataStandards().contains(harRecordMetadata1) + "open repo std");
                if (!openRepository.getRecordMetadataStandards().contains(harRecordMetadata1)) {
                    notPresentlist.add(harRecordMetadata1);
                }
            }
            openRepository.getRecordMetadataStandards().addAll(notPresentlist);
        } else {
            openRepository.setRecordMetadataStandards(listHarRecordMetadata);
        }
        return openRepository;
    }

    /**
     * Get the list of metadata standards though Map.
     *
     * @param supportedMetadataTypes {@link Map} of
     * {@link HarRecordMetadataType}
     * @param openRepository {@link OpenRepository}
     * @return {@link HarRecordMetadata} list
     */
    private List<HarRecordMetadata> getListOfMetadataStandard(Map<HarRecordMetadataType, Boolean> supportedMetadataTypes, OpenRepository openRepository) {
        List<HarRecordMetadata> listHarRecordMetadata = new ArrayList();
        HarRecordMetadata harRecordMetadata = null;
        if (!supportedMetadataTypes.isEmpty()) {
            for (HarRecordMetadataType standard : supportedMetadataTypes.keySet()) {
                harRecordMetadata = new HarRecordMetadata();
                harRecordMetadata.setMetadataType(standard);
                harRecordMetadata.setMetadataEnabled(supportedMetadataTypes.get(standard));
                harRecordMetadata.setMetadataSupport(true);
                harRecordMetadata.setOpenRepository(openRepository);
                listHarRecordMetadata.add(harRecordMetadata);
            }
        }
        return listHarRecordMetadata;
    }

    /**
     * updateTotalResourceRecordCountInDB Method used to update Total all
     * Resource Record Count based on resourceType Id on application deployment
     * time
     *
     * @return true if record count is updated otherwise false.
     * @throws Exception
     */
    public boolean updateTotalResourceRecordCountInDB() throws Exception {
        long resoucTypeId = 0;
        List<ResourceType> resourceTypesList = resourceTypeDAO.list();
        if (!resourceTypesList.isEmpty()) {
            for (ResourceType resourceType : resourceTypesList) {
                resoucTypeId = resourceType.getId();
                totalRecordCount(resoucTypeId);
            }
            LOGGER.info("All Resource total Record Count Updated..");
        } else {
            LOGGER.info("No Resources to update Record count");
            return false;
        }
        return true;
    }

    /**
     * all Resource Type Total Record Count
     *
     * @return total record count
     */
    public long getTotalResourceTypeRecordCount() {
        long totalrecords = 0;
        long recordCount;
        try {
            /**
             * Modified by hemant as resourceTypesList is containing duplicated
             * elements. So i have used
             * getListOfResourceTypeAccToTotalRecordCount method.
             * <p>
             * <u>Hemant: i was told by srinu sir to fix the bug.</u>
             * TODO: check method usage and fix accordingly.
             * </p>
             */
//            List<ResourceType> resourceTypesList = resourceTypeDAO.list();
            List<ResourceType> resourceTypesList = resourceTypeDAO.getListOfResourceTypeAccToTotalRecordCount();
            if (resourceTypesList != null && !resourceTypesList.isEmpty()) {
                for (ResourceType resourceType : resourceTypesList) {
                    recordCount = resourceType.getTotalRecordCount();
                    totalrecords = totalrecords + recordCount;
                }
                LOGGER.info("----Counting Total ResourceType Record updated...-----");
            } else {
                LOGGER.info("Counting Total ResourceType Record failed");
                LOGGER.error("Counting Total ResourceType Record failed");
            }
        } catch (Exception e) {
            LOGGER.error("Error in Counting Total ResourceType Record " + e);
        }
        return totalrecords;
    }

    /**
     * Get all {@link ResourceType}
     *
     * @return {@link List} of {@link ResourceType}
     */
    public List<ResourceType> getListOfAllResourceType() {
        return resourceDAO.getListOfAllResourceType();
    }

    /**
     * Get the {@link ResourceType} by its primary key/id
     *
     * @param id {@link ResourceType} primary key/id
     * @return {@link ResourceType}
     */
    public ResourceType getResourceTypeById(final long id) {
        return resourceTypeDAO.getResourceTypeById(id);
    }

    /**
     * Auto captures news feed url
     *
     * @param organisationUrl {@link String} organization url
     * @param newMainFeedUrl {@link String} news feed url
     * @return {@link NewsFeed} list
     * @throws IOException {@link IOException}
     * @throws SocketTimeoutException {@link SocketTimeoutException}
     */
    public List autoCapureFeeds(String organisationUrl, String newMainFeedUrl) throws IOException, SocketTimeoutException {
        List<NewsFeed> allNewsFeedUrls = new ArrayList<>();
        Connection connection = Jsoup.connect(newMainFeedUrl);
        connection.timeout(Constants.CONNECTION_TIMEOUT);
        Document document = connection.get();
        NewsFeed newsFeed;
        if (document != null) {
            Elements elements = document.select("a[href]");
            boolean isNotReuiredUrls = false;
            List<String> allUrls = new ArrayList<>();
            for (Element link : elements) {
                String linkUrl = link.attr("href");
                for (String tempUrl : Constants.FEED_URL_EXCEPTIONS) {
                    if (linkUrl.contains(tempUrl)) {
                        isNotReuiredUrls = true;
                        break;
                    } else {
                        isNotReuiredUrls = false;
                    }
                }
                if (!linkUrl.startsWith("http")) {
                    if (!linkUrl.startsWith("/")) {
                        linkUrl = organisationUrl + "/" + linkUrl;
                    } else {
                        linkUrl = organisationUrl + linkUrl;
                    }
                }
                if (!isNotReuiredUrls && !allUrls.contains(linkUrl) && !linkUrl.equalsIgnoreCase(newMainFeedUrl) && (linkUrl.toLowerCase().contains("rss") || linkUrl.contains("feed"))) {
                    newsFeed = new NewsFeed();
                    newsFeed.setFeedUrl(linkUrl);
                    newsFeed.setLabelName(link.text());
                    newsFeed.setActiveFeed(true);
                    allUrls.add(linkUrl);
                    allNewsFeedUrls.add(newsFeed);
                }
            }
        }
        return allNewsFeedUrls;
    }

    /**
     * Get {@link Resource} by {@link ResourceType} code
     *
     * @param resourceTypeCode {@link ResourceType} code
     * @return List of {@link Resource}
     */
    public List<Resource> getResourceByResourceTypeCode(String resourceTypeCode) {
        return resourceTypeDAO.getResourceByResourceTypeCode(resourceTypeCode);
    }

    /**
     * To save uploaded Icon on specific location. Upload icon like group icon,
     * resourceType and Resource icon.
     *
     * @param iconLocation absolute icon path location
     * @param iconName icon name
     * @return file name
     * @throws IOException {@link IOException}
     */
    public String saveIcon(String iconLocation, String iconName) throws IOException {
        String[] fileNameAndExtension = iconName.split(Constants.REG_EXP_FOR_EXTENSION);
        String fullIconName;
        String extention = fileNameAndExtension[1];
        String tempFileName = "temp." + extention;
        File tempFile = new File(iconLocation + File.separator + tempFileName);
        if (tempFile.exists()) {
            fullIconName = iconName;
            File finalIcon = new File(iconLocation + File.separator + fullIconName);
            FileUtils.moveFile(tempFile, finalIcon);
        } else {
            fullIconName = tempFileName;
        }

        return fullIconName;
    }

    /**
     * To get/ show uploaded icon.Upload icon like group icon, resourceType and
     * Resource icon.
     *
     * @param iconLocation image location
     * @param iconName image name
     * @param response {@link HttpServletResponse}
     */
    public void getIcon(String iconLocation, String iconName, HttpServletResponse response) {
        OutputStream outputStream = null;
        FileInputStream fin = null;
        File iconFile = null;
        String fileLocation;
        try {
            File resourceIconDir = new File(iconLocation);
            fileLocation = resourceIconDir.getAbsolutePath() + File.separator + iconName;
            iconFile = new File(fileLocation);
            if (iconFile.exists()) {
                fin = new FileInputStream(iconFile);
                byte[] buffer = new byte[fin.available()];
                fin.read(buffer);
                outputStream = response.getOutputStream();
                outputStream.write(buffer);
            }
        } catch (IOException io) {
            LOGGER.error("Error in saving icon " + iconFile.getAbsolutePath(), io);
        } finally {
            if (outputStream != null) {
                IOUtils.closeQuietly(outputStream);
            }
            if (fin != null) {
                IOUtils.closeQuietly(fin);
            }
        }
    }

    /**
     * Upload icon like group icon, resourceType and Resource icon.
     *
     * @param iconLocation image location
     * @param iconName image name
     * @param orignalFileName original file name
     * @return absolute path of image
     */
    public String uploadIcon(String iconLocation, String iconName, String orignalFileName) {
        File iconDir = new File(iconLocation);
        if (!iconDir.exists()) {
            iconDir.mkdir();
        }
        String[] fileNameAndExtension = orignalFileName.split(Constants.REG_EXP_FOR_EXTENSION);
        String fileExtension = fileNameAndExtension[1].toLowerCase();
        String fileName;
        if (iconName != null && !"".equals(iconName)) {
            fileName = iconName + "." + fileExtension;
        } else {
            fileName = "temp" + "." + fileExtension;
        }
        String filePath = iconDir.getAbsolutePath() + File.separator + fileName;
        File file = new File(filePath);
        if (file.exists()) {
            FileUtils.deleteQuietly(file);
        }
        return file.getAbsolutePath();
    }

    /**
     * Get the resource type category for resource type creation
     *
     * @return List of Categories
     */
    public List getCategoryListForResourceType() {
        List<String> categoryForResourceType = new ArrayList<>();
        categoryForResourceType.add(Constants.RESOURCE_TYPE_MIT);
        categoryForResourceType.add(Constants.RESOURCE_TYPE_HARVEST);
        categoryForResourceType.add(Constants.RESOURCE_TYPE_ENEWS);
        categoryForResourceType.add(Constants.RESOURCE_TYPE_WEB_CRAWLER);
        return categoryForResourceType;
    }

    /**
     * To remove null or empty object from the ResourceTypeInfo list
     *
     * @param resourceTypeInfoList {@link List} of {@link ResourceTypeInfo}
     */
    public void removeEmptyObject(List<ResourceTypeInfo> resourceTypeInfoList) {
        if (resourceTypeInfoList != null && !resourceTypeInfoList.isEmpty()) {
            ResourceTypeInfo resourceTypeInfoE = new ResourceTypeInfo("", "");
            ResourceTypeInfo resourceTypeInfoN = new ResourceTypeInfo(null, null);
            resourceTypeInfoList.removeAll(Collections.singleton(resourceTypeInfoE));
            resourceTypeInfoList.removeAll(Collections.singleton(resourceTypeInfoN));
        }
    }

    /**
     * Save ResourceType at middleware using webservice
     *
     * @param resourceType {@link ResourceType}
     * @param event event save or update
     * @return true if saved successfully otherwise false
     */
    public boolean submitResourceTypeToMIT(ResourceType resourceType, String event) {
        String url = null;
        ResponseEntity<Boolean> isSaved = null;
        try {
            ResourceTypeInformationDetails informationDetails = mappingResourceTypeToMITType(resourceType);
            HttpEntity<String> request = new HttpEntity(informationDetails, Helper.mitWebserviceAthentication());
            url = mitWSBaseURL + "resourceType/" + event;
            URI webServiceURL = new URI(url);

            isSaved = restTemplate.postForEntity(webServiceURL, request, Boolean.class);
            if (isSaved.getBody()) {
                LOGGER.info("Resource Type saved successfully at MIT.");
            } else {
                LOGGER.info("Resource Type not saved successfully at MIT.");
            }
        } catch (URISyntaxException | RestClientException e) {
            LOGGER.info("Resource Type not saved successfully at MIT.");
            LOGGER.error("Error in webservice " + url + " communication. ", e);
        }
        return isSaved.getBody();
    }

    /**
     * Mapping of {@link ResourceType} to {@link ResourceTypeInformationDetails}
     *
     * @param resourceType {@link ResourceType}
     * @return {@link ResourceTypeInformationDetails}
     */
    public ResourceTypeInformationDetails mappingResourceTypeToMITType(ResourceType resourceType) {
        ResourceTypeInformationDetails informationDetails = new ResourceTypeInformationDetails();
        if (resourceType != null) {
            informationDetails.setResourceType(resourceType.getResourceType());
            informationDetails.setResourceTypeCode(resourceType.getResourceTypeCode());
            informationDetails.setCrowdSource(true);
            informationDetails.setPublishFlag(true);
        }
        return informationDetails;
    }

    /**
     * Mapping of XLS file parameters to {@link Resource}
     *
     * @param row {@link Row}
     * @return {@link MetadataIntegrator}
     */
    public MetadataIntegrator mapplingXLSToResource(Row row) {
        MetadataIntegrator resource = new MetadataIntegrator();
        Iterator<Cell> it = row.iterator();
        while (it.hasNext()) {
            Cell cell = it.next();
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
        resource.setResourceName(row.getCell(0).getStringCellValue());
        resource.setResourceCode(row.getCell(1).getStringCellValue());
        resource.setClassificationLevel(row.getCell(2).getStringCellValue());
        resource.setEstablishYear(row.getCell(4).getStringCellValue());
        resource.setDescription(row.getCell(5).getStringCellValue());
        resource.setTotalCount(row.getCell(7).getStringCellValue());
        resource.setContactPersonName(row.getCell(9).getStringCellValue());
        resource.setDesignation(row.getCell(10).getStringCellValue());
        resource.setPhoneNumber(row.getCell(12).getStringCellValue());
        resource.setEmail(row.getCell(14).getStringCellValue());
        String conetnetWithMetadata = row.getCell(15).getStringCellValue();
        if ("yes".equalsIgnoreCase(conetnetWithMetadata)) {
            resource.setContentWithMetadata(true);
        } else {
            resource.setContentWithMetadata(false);
        }
        String langauges = row.getCell(3).getStringCellValue();
        List<String> langaugeItems = Arrays.asList(langauges.split("\\s*,\\s*"));
        List<UDCLanguage> udcLangList = new ArrayList<>();
        System.out.println("langaugeItems size is " + langaugeItems.size());
        if (langaugeItems != null) {
            for (String lang : langaugeItems) {
                UDCLanguage language = languageDAO.getLanguageByLanguageName(lang);
                if (language != null) {
                    udcLangList.add(language);
                    System.out.println("lang name" + language.getLanguageName());
                } else {
                    System.out.println("in else");
                }
            }
            resource.setLanguages(new HashSet<>(udcLangList));
        }
        return resource;
    }

    /**
     * Method used to get Information from Resource Object and mapping into
     * {@link ResourceDetailsJson} Object
     *
     * @param resource {@link Resource}
     * @return {@link ResourceDetailsJson}
     */
    public ResourceDetailsJson mappingOtherResourceToMitBasicDetails(Resource resource, ResourceDetailsJson resourceDetail) {
        if (resourceDetail == null) {
            resourceDetail = new ResourceDetailsJson();
        }
        resourceDetail.setResourceTypeCode(resource.getResourceType().getResourceTypeCode());
        resourceDetail.setResourceCode(resource.getResourceCode());
        resourceDetail.setCrowdSource(resource.isCrowdSourcing());
        resourceDetail.setName(resource.getResourceName());
        resourceDetail.setOrganization(resource.getOrganization().getName());
        resourceDetail.setWebsite(resource.getOrganization().getOrganisationUrl());
        resourceDetail.setDescription(resource.getDescription());
        resourceDetail.setAddress(resource.getOrganization().getAddress());
        resourceDetail.setContactPerson(resource.getContactPersonName());
        if (resource.getPhoneNumber() != null && !resource.getPhoneNumber().equals("")) {
            resourceDetail.setContactNo(resource.getPhoneNumber());
        }
        resourceDetail.setDesignation(resource.getDesignation());
        resourceDetail.setEmailId(resource.getEmail());
        Collection<String> thematicTypeList = new ArrayList<>();
        if (resource.getThematicTypes() != null && !resource.getThematicTypes().isEmpty()) {
            for (ThematicType thematicType : resource.getThematicTypes()) {
                thematicTypeList.add(thematicType.getThematicType());
            }
            resourceDetail.setThematicClassification(StringUtils.join(thematicTypeList, ','));
        }
        Collection<String> contentTypeList = new ArrayList<>();
        if (resource.getContentTypes() != null && !resource.getContentTypes().isEmpty()) {
            for (ContentType contentType : resource.getContentTypes()) {
                contentTypeList.add(contentType.getContentType());
            }
            resourceDetail.setItemsCollected(StringUtils.join(contentTypeList, ','));
        }
        Collection<String> languageList = new ArrayList<>();
        if (resource.getLanguages() != null && !resource.getLanguages().isEmpty()) {
            for (UDCLanguage language : resource.getLanguages()) {
                languageList.add(language.getLanguageName());
            }
            resourceDetail.setLanguages(StringUtils.join(languageList, ','));
        }
        resourceDetail.setActivationFlag(true);
        resourceDetail.setPublishFlag(false);
        return resourceDetail;
    }

    /**
     * To perform <li>Publish ResourceType</li><li>unPublish ResourceType</li>
     * through web-service
     *
     * @param resourceTypeCode {@link ResourceType} code
     * @param eventType type of operation to perform
     * @return {@link ResourceType}
     */
    public ResourceType operationForResourceType(String resourceTypeCode, String eventType) {
        String url = null;
        ResponseEntity<Boolean> isPulished;
        ResourceType resourceType;
        ResourceTypeInformationDetails informationDetails = new ResourceTypeInformationDetails();
        try {
            resourceType = resourceTypeDAO.getResourceTypeByCode(resourceTypeCode);
            if (resourceType != null) {
                switch (eventType) {
                    case "activate":
                        informationDetails.setPublishFlag(true);
                        break;
                    case "deactivate":
                        resourceType.setPublishFlag(false);
                        informationDetails.setPublishFlag(false);
                        break;
                }
                HttpEntity<String> request = new HttpEntity(informationDetails, Helper.mitWebserviceAthentication());
                url = mitWSBaseURL + "resourceType/" + eventType + "/" + resourceTypeCode;
                URI webServiceURL = new URI(url);
                isPulished = restTemplate.postForEntity(webServiceURL, request, Boolean.class);
                if (eventType.equalsIgnoreCase("activate")) {
                    if (isPulished.getBody()) {
                        resourceType.setPublishFlag(true);
                        LOGGER.info("Resource Type Published successfully");
                    }
                } else if (eventType.equalsIgnoreCase("deactivate")) {
                    if (isPulished.getBody()) {
                        resourceType.setPublishFlag(false);
                        LOGGER.info("Resource Type Un-Published successfully");
                    }
                }
            }
            return resourceType;
        } catch (URISyntaxException | RestClientException e) {
            LOGGER.error("Error in performing " + eventType + ".", e);
            LOGGER.error("Error in webservice " + url + " communication. ", e);
        }
        return null;
    }

    /**
     * Get the Organization List
     *
     *
     * @return true if gets list successfully otherwise false.
     */
    public List<Organization> getOrganizationList() {
        List<Organization> organizationList = organizationDAO.list();
        Collections.sort(organizationList, Organization.OrganizationComparator);
        return organizationList;
    }

    /**
     * Update the Content type
     *
     * @param contentType
     * @param contentTypeId
     */
    public void updateContentType(ContentType contentType, Long contentTypeId) {
        ContentType preContentType = contentTypeDAO.get(contentTypeId);
        contentType.setId(preContentType.getId());
        contentTypeDAO.merge(contentType);
        LOGGER.info("Content Type is updated in database.");
    }

    /**
     * To Remove Content Type {@link ContentType}
     *
     * @param contentType
     */
    public void removeContentType(ContentType contentType) {
        contentTypeDAO.delete(contentType);
    }

    /**
     * To Remove Resource {@link Resource}
     * 16th dec 2016, 13 april 2017, 9 feb 2017
     * 
     * 
     * @param resource
     */
    public boolean removeResource(Resource resource) throws Exception {
        boolean isDeleted = false;
        HttpEntity<String> request = new HttpEntity(Helper.mitWebserviceAthentication());
        String resourceCode = resource.getResourceCode();
        String resourceName = resource.getResourceName();
        String applicationURL = mitWSBaseURL + "resource/delete/" + resourceCode;
        ResponseEntity<Boolean> responseEntity = restTemplate.exchange(applicationURL, HttpMethod.GET, request, Boolean.class);
        if (responseEntity.getBody()) {
            LOGGER.info(resourceName + " resource is deleted at Middelware side");
            if (resource.getResourceType().getResourceTypeCategory().trim().equalsIgnoreCase(Constants.RESOURCE_TYPE_HARVEST)) {
                String applicationURLHarvester = oarWSBaseURL + "repositories/delete/" + resource.getResourceCode();
                HttpEntity harvesterRequest = new HttpEntity(getConfiguredHttpHeaders());
                ResponseEntity<Boolean> deleteStatus = restTemplate.exchange(applicationURLHarvester, HttpMethod.POST, harvesterRequest, Boolean.class);
                if (deleteStatus.getBody() == true || responseEntity.getBody() == true) {
                    LOGGER.info(resourceName + " resource is deleted at Harvester side");
                    resourceDAO.delete(resource);
                    isDeleted = true;
                } else {
                    LOGGER.info(resourceName + " resource is not deleted at Harvester side");
                    isDeleted = false;
                }
            } else {
                LOGGER.info(resourceName + " resource is deleted at NVLI portal side");
                resourceDAO.delete(resource);
                isDeleted = true;
            }

        } else {
            LOGGER.info(resourceName + " resource is not deleted at Middelware side");
            isDeleted = false;
        }

        return isDeleted;
    }

    /**
     * To Remove ResourceType {@link ResourceType}
     *
     * @param resourceType
     */
    public boolean removeResourceType(ResourceType resourceType) {

        HttpEntity<String> request = new HttpEntity(Helper.mitWebserviceAthentication());
        String applicationURL = mitWSBaseURL + "resourceType/delete/" + resourceType.getResourceTypeCode();
        ResponseEntity<Boolean> responseEntity = restTemplate.exchange(applicationURL, HttpMethod.GET, request, Boolean.class);
        if (responseEntity.getBody() == true) {
            resourceTypeDAO.delete(resourceType);
            return true;
        }
        return false;
    }

    /**
     * getRTListWithPagination method used to do Resource type Register
     * pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return
     */
    public ResultHolder getRTListWithPagination(String searchString, int pageNum, int pageWin) {
        ResultHolder resultHolder = resourceTypeDAO.doPaginateResourceType(searchString, pageNum, pageWin);
        return resultHolder;
    }

    /**
     * getLangListWithPagination method used to do language Register pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return
     */
    public ResultHolder getLangListWithPagination(String searchString, int pageNum, int pageWin) {
        ResultHolder resultHolder = languageDAO.doPaginateLanguageList(searchString, pageNum, pageWin);
        return resultHolder;
    }

    /**
     * getOrgListWithPagination method used to do organization Register
     * pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return
     */
    public ResultHolder getOrgListWithPagination(String searchString, int pageNum, int pageWin) {
        ResultHolder resultHolder = organizationDAO.doPaginateOrganization(searchString, pageNum, pageWin);
        return resultHolder;
    }

    /**
     * getCTListWithPagination method used to do Content type Register
     * pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return
     */
    public ResultHolder getCTListWithPagination(String searchString, int pageNum, int pageWin) {
        ResultHolder resultHolder = contentTypeDAO.doPaginateContentType(searchString, pageNum, pageWin);
        return resultHolder;
    }

    /**
     * getGroupListWithPagination method used to do Content type Register
     * pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return
     */
    public ResultHolder getGroupListWithPagination(String searchString, int pageNum, int pageWin) {
        ResultHolder resultHolder = groupTypeDAO.doPaginateGroupType(searchString, pageNum, pageWin);
        return resultHolder;
    }

    /**
     * getThematicListWithPagination method used to do Thematic type Register
     * pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @return
     */
    public ResultHolder getThematicListWithPagination(String searchString, int pageNum, int pageWin) {
        ResultHolder resultHolder = thematicTypeDAO.doPaginateThematicType(searchString, pageNum, pageWin);
        return resultHolder;
    }

    /**
     * To Edit Organization
     *
     * @param organization {@link Organization}
     * @return
     */
    public boolean editOrganization(Organization organization) {
        organizationDAO.merge(organization);
        return true;
    }

    /**
     * To Remove Organization Type
     *
     * @param organization {@link Organization}
     */
    public void removeOrganization(Organization organization) {
        organizationDAO.delete(organization);
    }

    /**
     * geteNewsWithPagination method used to do eNews Register pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @param resourceTypeId {@link ResourceTy}
     * @return
     */
    public ResultHolder getResourceListWithPagination(String searchString, int pageNum, int pageWin, Long resourceTypeId) throws Exception {
        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        ResultHolder resultHolder = resourceDAO.doPaginateResource(searchString, pageNum, pageWin, resourceType);
        return resultHolder;
    }

    /**
     * getDraftResourceListWithPagination method used to do draft resource
     * Register pagination.
     *
     * @param searchString resource type name to search
     * @param pageNum page number
     * @param pageWin window size
     * @param resourceTypeId {@link ResourceTy}
     * @return
     */
    public ResultHolder getDraftResourceListWithPagination(String searchString, int pageNum, int pageWin, Long resourceTypeId) throws Exception {
        ResourceType resourceType = resourceTypeDAO.get(resourceTypeId);
        ResultHolder resultHolder = resourceDAO.doPaginateDraftResource(searchString, pageNum, pageWin, resourceType);
        return resultHolder;
    }

    public Map<String, ResourceType> updateResourceTypeMap(Map<String, ResourceType> resourceTypesMap, ResourceType rType) {
        if (rType.isPublishFlag()) {
            if (resourceTypesMap.containsKey(rType.getResourceTypeCode())) {
                resourceTypesMap.replace(rType.getResourceTypeCode(), rType);
            } else {
                resourceTypesMap.put(rType.getResourceTypeCode(), rType);
            }
        } else {
            if (resourceTypesMap.containsKey(rType.getResourceTypeCode())) {
                resourceTypesMap.remove(rType.getResourceTypeCode(), rType);
            }
        }
        return resourceTypesMap;
    }

    public List<HarRepoCustomised> getRepoStatusFromHar() {
        try {
            String url = oarWSBaseURL + "repositories/list/update";
            HttpEntity requestEntity = new HttpEntity(getConfiguredHttpHeaders());

            ResponseEntity repositoryList = restTemplate.exchange(url, HttpMethod.POST, requestEntity, List.class);
            System.out.println("url--------------" + url);

            return (List<HarRepoCustomised>) repositoryList.getBody();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ResourceService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * To perform <ul><li>Activate Web URL</li><li>Deactivate Repository
     * URL</li><li>Publish</li><li>unPublish</li></ul>
     *
     * @param webCrawler object of {@link WebCrawler}
     * @param eventType type of operation to perform
     * @return updated {@link WebCrawler} object.
     * @throws java.lang.Exception
     */
    public boolean operationForResource(Resource resource, String eventType) throws Exception {
        boolean isSuccess = false;
        String eventStatus = null;
        String resourceType = null;
        if (resource != null) {
            isSuccess = operationsForMiddleware(eventType, resource.getResourceCode());
            System.out.println("isSucess " + isSuccess);
            if (isSuccess) {
                resourceType = resource.getResourceType().getResourceTypeCategory();
                switch (eventType) {
                    case "deactivate":
                        eventStatus = "not_active";
                        resource.setActive(false);
                        break;
                    case "publish":
                        resource.setPublish(true);
                        LOGGER.info(resource.getResourceName() + " resource is published");
                        break;
                    case "unpublish":
                        resource.setPublish(false);
                        LOGGER.info(resource.getResourceName() + " resource is unpublished");
                        break;
                }
                if (resourceType.equalsIgnoreCase(Constants.RESOURCE_TYPE_HARVEST)) {
                    eventStatus = operationForHarvestURL(resource, eventType);
                    if (eventStatus.equalsIgnoreCase("active")) {
                        resource.setActive(true);
                    } else if (eventStatus.equalsIgnoreCase("not_active")) {
                        resource.setActive(false);
                    }
                }

                if (eventStatus != null) {
                    List<ResourceStatus> statusList = resourceStatusDAO.findbyQuery("findByStatusName", eventStatus);
                    ResourceStatus status = null;
                    if (statusList != null && !statusList.isEmpty()) {
                        status = statusList.get(0);
                    } else {
                        status = resource.getResourceStatus();
                    }
                    resource.setResourceStatus(status);
                    LOGGER.info("Resource status is " + resource.getResourceStatus().getStatusName());
                }
                Resource rs = resourceDAO.merge(resource);

            }
        }
        return isSuccess;
    }

    public String callTogetStatusDetails(String resourceCode) throws Exception {
        String applicationURL = oarWSBaseURL + "repositories/getMetadataStatus/" + resourceCode;
        HttpEntity request = new HttpEntity(getConfiguredHttpHeaders());
        String statusDeatils = restTemplate.postForObject(applicationURL, request, String.class);
        return statusDeatils;
    }
}
