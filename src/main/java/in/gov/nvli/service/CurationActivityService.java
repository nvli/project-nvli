package in.gov.nvli.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import in.gov.nvli.beans.CurationForm;
import in.gov.nvli.dao.crowdsource.ICrowdSourceDAO;
import in.gov.nvli.dao.crowdsource.IResourcesWithoutLibUserDAO;
import in.gov.nvli.dao.curationactivity.ICurationActivityDAO;
import in.gov.nvli.dao.resource.IOrganizationDAO;
import in.gov.nvli.dao.resource.IResourceDAO;
import in.gov.nvli.dao.resource.IResourceTypeDAO;
import in.gov.nvli.dao.user.UserDAO;
import in.gov.nvli.domain.crowdsource.CrowdSource;
import in.gov.nvli.domain.crowdsource.CrowdSourceEdit;
import in.gov.nvli.domain.crowdsource.ResourcesWithoutLibUser;
import in.gov.nvli.domain.crowdsource.UDCLanguage;
import in.gov.nvli.domain.resource.Organization;
import in.gov.nvli.domain.resource.Resource;
import in.gov.nvli.domain.resource.ResourceType;
import in.gov.nvli.domain.user.User;
import in.gov.nvli.webserviceclient.curationactivity.CurationActivityWebserviceHelper;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Milind
 * @author Vivek Bugale
 */
@Service
public class CurationActivityService {

    @Autowired
    private ICurationActivityDAO curationActivityDAO;

    /**
     *
     */
    @Autowired
    public CrowdSourcingService crowdSourcingService;

    /**
     *
     */
    @Autowired
    public ResourceService resourceService;

    /**
     *
     */
    @Autowired
    public IResourceTypeDAO resourceTypeDAO;

    /**
     *
     */
    @Autowired
    public IResourceDAO resourceDAO;

    /**
     *
     */
    @Autowired
    public UserDAO userDAO;
    
    @Autowired
    private ICrowdSourceDAO crowdSourceDAO;
    
//    @Autowired
//    private IResourcesWithoutLibUserDAO resourcesWithoutLibUserDAO;

    @Autowired
    private CurationActivityWebserviceHelper curationActivityWebserviceHelper;

    @Autowired
    private IOrganizationDAO organizationDAO;

    /**
     * This is to generate Curator Work Report in PDF.
     *
     * @param user
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public ByteArrayOutputStream generateCuratorWorkReportInPDF(User user, Date startDate, Date endDate) throws Exception {
        Calendar c = Calendar.getInstance(); 
        c.setTime(endDate); 
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();
        List<CrowdSourceEdit> crowdSourceEditRecordList = curationActivityDAO.getCrowdSourceEditRecordListByUserId(user.getId(), startDate, endDate);        
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, baos);
        try {
            PdfPCell cell;
            PdfPTable imageTable = new PdfPTable(2);
            float[] columnWidths = new float[]{50f, 50f};
            imageTable.setWidths(columnWidths);
            imageTable.setWidthPercentage(100);

            Image nvliLogo = Image.getInstance(getClass().getClassLoader().getResource("NVLI-LOGO-header.png"));
            cell = new PdfPCell(nvliLogo);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            cell.setBorder(Rectangle.BOTTOM);
            cell.setPadding(10f);
            imageTable.addCell(cell);

            PdfPTable dataTable = new PdfPTable(3);
            columnWidths = new float[]{10f, 55f, 35f};
            dataTable.setWidths(columnWidths);
            dataTable.setWidthPercentage(100);

            if (crowdSourceEditRecordList != null && !crowdSourceEditRecordList.isEmpty()) {
                PdfPCell headerCell = new PdfPCell(new Paragraph("Curator statistics"));
                headerCell.setColspan(3);
                headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                headerCell.setPadding(10.0f);
                headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                headerCell.setBackgroundColor(WebColors.getRGBColor("#ccddee"));
                dataTable.addCell(headerCell);

                cell = new PdfPCell(new Phrase("Sr No"));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setPadding(10.0f);
                dataTable.addCell(cell);
                cell = new PdfPCell(new Phrase("Record Indentifier"));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setPadding(10.0f);
                dataTable.addCell(cell);
                cell = new PdfPCell(new Phrase("Record Curated Date"));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setPadding(10.0f);
                dataTable.addCell(cell);

                Integer count = 1;
                for (CrowdSourceEdit crowdSourceEditRecord : crowdSourceEditRecordList) {
                    cell = new PdfPCell(new Phrase(count.toString()));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setPaddingBottom(5.0f);
                    cell.setPaddingTop(5.0f);
                    dataTable.addCell(cell);
                    cell = new PdfPCell(new Phrase(crowdSourceEditRecord.getCrowdSource().getRecordIdentifier()));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setPaddingBottom(5.0f);
                    cell.setPaddingTop(5.0f);
                    dataTable.addCell(cell);
                    cell = new PdfPCell(new Phrase(crowdSourceEditRecord.getCrowdSourceEditTimestamp() + ""));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setPaddingBottom(5.0f);
                    cell.setPaddingTop(5.0f);
                    dataTable.addCell(cell);
                    count++;
                }

                dataTable.setSpacingBefore(30.0f);
                dataTable.setSpacingAfter(30.0f);
            }
            FooterTable event = new FooterTable();
            writer.setPageEvent(event);
            document.open();
            document.addTitle("Curator Work Done Logs");
            document.addCreator("NVLI");
            //document.addSubject("This example shows how to add metadata");
            //document.addKeywords("Metadata, iText, PDF");
            document.addCreationDate();
            document.add(imageTable);
            document.add(new Paragraph("Curator Name : " + user.getFirstName() + " " + user.getLastName()));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            document.add(new Paragraph("Report Period : From " + format.format(startDate) + " to " + format.format(endDate)));
            document.add(new Paragraph("Total Curated Records : " + crowdSourceEditRecordList.size()));
            document.add(new Paragraph("Date : " + new SimpleDateFormat("dd MMM yyyy HH:mm:ss").format(new Date())));
            document.add(dataTable);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
        }
        return baos;
    }

    /**
     * This is to generate library expert Work Report in PDF.
     *
     * @param user
     * @param startDate
     * @param endDate
     * @return
     * @throws Exception
     */
    public ByteArrayOutputStream generateLibraryExpertReportInPDF(User user, Date startDate, Date endDate) throws Exception {
        Calendar c = Calendar.getInstance(); 
        c.setTime(endDate); 
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();
        List<CrowdSource> crowdSourceRecordList = curationActivityDAO.getCrowdSourceRecordListByExpertId(user.getId(), startDate, endDate);    
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, baos);
        try {
            PdfPCell cell;
            PdfPTable imageTable = new PdfPTable(2);
            float[] columnWidths = new float[]{50f, 50f};
            imageTable.setWidths(columnWidths);
            imageTable.setWidthPercentage(100);
            Image nvliLogo = Image.getInstance(getClass().getClassLoader().getResource("NVLI-LOGO-header.png"));
            cell = new PdfPCell(nvliLogo);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            cell.setBorder(Rectangle.BOTTOM);
            cell.setPadding(10f);
            imageTable.addCell(cell);

            PdfPTable dataTable = new PdfPTable(3);
            columnWidths = new float[]{10f, 55f, 35f};
            dataTable.setWidths(columnWidths);
            dataTable.setWidthPercentage(100);

            if (crowdSourceRecordList != null && !crowdSourceRecordList.isEmpty()) {
                PdfPCell headerCell = new PdfPCell(new Paragraph("Library expert statistics"));
                headerCell.setColspan(3);
                headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                headerCell.setPadding(10.0f);
                headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                headerCell.setBackgroundColor(WebColors.getRGBColor("#ccddee"));
                dataTable.addCell(headerCell);

                cell = new PdfPCell(new Phrase("Sr No"));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setPadding(10.0f);
                dataTable.addCell(cell);
                cell = new PdfPCell(new Phrase("Record Indentifier"));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setPadding(10.0f);
                dataTable.addCell(cell);
                cell = new PdfPCell(new Phrase("Record Curated Date"));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setPadding(10.0f);
                dataTable.addCell(cell);

                Integer count = 1;
                for (CrowdSource crowdSourceRecord : crowdSourceRecordList) {
                    cell = new PdfPCell(new Phrase(count.toString()));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setPaddingBottom(5.0f);
                    cell.setPaddingTop(5.0f);
                    dataTable.addCell(cell);
                    cell = new PdfPCell(new Phrase(crowdSourceRecord.getRecordIdentifier()));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setPaddingBottom(5.0f);
                    cell.setPaddingTop(5.0f);
                    dataTable.addCell(cell);
                    cell = new PdfPCell(new Phrase(crowdSourceRecord.getEndTimestamp() + ""));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setPaddingBottom(5.0f);
                    cell.setPaddingTop(5.0f);
                    dataTable.addCell(cell);
                    count++;
                }
            }
            FooterTable event = new FooterTable();
            writer.setPageEvent(event);
            document.open();
            document.addTitle("Library Expert Work Done Logs");
            document.addCreator("NVLI");
            //document.addSubject("This example shows how to add metadata");
            //document.addKeywords("Metadata, iText, PDF");
            document.addCreationDate();
            document.add(imageTable);
            document.add(new Paragraph("Library Expert Name : " + user.getFirstName() + " " + user.getLastName()));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            document.add(new Paragraph("Report Period : From " + format.format(startDate) + " to " + format.format(endDate)));
            document.add(new Paragraph("Total Approved Records : " + crowdSourceRecordList.size()));
            document.add(new Paragraph("Date : " + new SimpleDateFormat("dd MMM yyyy HH:mm:ss").format(new Date())));
            document.add(dataTable);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
        }
        return baos;
    }

    /**
     * This is to get CurationFrom.
     *
     * @param curationForm
     * @return
     * @throws Exception
     */
    public CurationForm getCurationFrom(CurationForm curationForm) throws Exception {
        return curationActivityWebserviceHelper.getCurationFrom(curationForm);
    }

    /**
     * This is to generate error Report in PDF.
     *
     * @return @throws Exception
     */
    public ByteArrayOutputStream generateErrorReportInPDF() throws Exception {
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, baos);
            PdfPCell cell;
            PdfPTable imageTable = new PdfPTable(2);
            float[] columnWidths = new float[]{50f, 50f};
            imageTable.setWidths(columnWidths);
            imageTable.setWidthPercentage(100);

            Image nvliLogo = Image.getInstance(getClass().getClassLoader().getResource("NVLI-LOGO-header.png"));
            cell = new PdfPCell(nvliLogo);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            cell.setBorder(Rectangle.NO_BORDER);
            imageTable.addCell(cell);

            document.open();
            document.addTitle("Work Done Logs");
            document.addCreator("NVLI");
            document.addCreationDate();
            document.add(imageTable);
            document.add(new Paragraph("Error in report generation."));

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
        }
        return baos;
    }

    /**
     * This is to check Resources Without LibUser Register.
     *
     * @return @throws Exception
     */
    public JSONObject checkResourcesWithoutLibUserRegister() throws Exception {
        List<ResourcesWithoutLibUser> resourcesWithoutLibUsersList= curationActivityDAO.checkResourcesWithoutLibUserRegister(); 
        JSONObject finalResourcesWithoutLibUsersJSONObject = new JSONObject();       
        JSONArray resourcesWithoutLibUsersJSONArray = new JSONArray();        
        resourcesWithoutLibUsersList.forEach(resource -> {
              JSONObject resourcesWithoutLibUsersJSONObject=new JSONObject();
              resourcesWithoutLibUsersJSONObject.put("resourceName", resource.getResource().getResourceName());
              resourcesWithoutLibUsersJSONObject.put("resourceTypeName", resource.getResource().getResourceType().getResourceType());              
              resourcesWithoutLibUsersJSONArray.add(resourcesWithoutLibUsersJSONObject);
        });        
        finalResourcesWithoutLibUsersJSONObject.put("status", resourcesWithoutLibUsersList.isEmpty());
        finalResourcesWithoutLibUsersJSONObject.put("resourcesWithoutLibUsersJSONArray", resourcesWithoutLibUsersJSONArray);        
        return finalResourcesWithoutLibUsersJSONObject;        
    }

    /**
     * This is to get Library Expert Statistics.
     *
     * @param resourceCode
     * @return
     */
    public JSONObject getLibraryExpertStatistics(String resourceCode) {
        List<CrowdSource> crowdSourceList = curationActivityDAO.getLibraryExpertStatistics(resourceCode);
        //check crowdSourceList isEmpty() ?
        if (!crowdSourceList.isEmpty()) {
            JSONObject libExpStatJSONObj = new JSONObject();
            JSONArray libExpStatJSONArray = new JSONArray();

            //for status :: approved=1 ,unapproved=0
            byte a = 1, u = 0;

            // key : id , value : Crowdsource
            Map<Long, List<CrowdSource>> groupByExpertId;

            //set of ids
            Set<Long> keySet;

            //group by expert id
            groupByExpertId = crowdSourceList
                    .stream()
                    .collect(Collectors.groupingBy(
                            el -> el.getExpertUserId().getId())
                    );

            groupByExpertId
                    .forEach((id, cs) -> {
                JSONObject tempLibExpStatJSONObj;

                long approved, unApproved;
                approved = crowdSourceList
                        .stream()
                        .filter(el -> Objects.equals(
                        el.getExpertUserId().getId(), id))
                        .filter(el -> Objects.equals(
                        el.getIsApproved(), a))
                        .collect(Collectors.toList())
                        .size();
                unApproved = crowdSourceList
                        .stream()
                        .filter(el -> Objects.equals(
                        el.getExpertUserId().getId(), id))
                        .filter(el -> Objects.equals(
                        el.getIsApproved(), u))
                        .collect(Collectors.toList())
                        .size();

                tempLibExpStatJSONObj = new JSONObject();
                tempLibExpStatJSONObj.put("id", id);
                tempLibExpStatJSONObj.put("assignedRecords", cs.size());
                tempLibExpStatJSONObj.put("approvedRecords", approved);
                tempLibExpStatJSONObj.put("unApprovedRecords", unApproved);

                        libExpStatJSONArray.add(tempLibExpStatJSONObj);
                    });

            keySet = new HashSet<>();

            for (int i = 0; i < libExpStatJSONArray.size(); i++) {
                JSONObject tempObj = (JSONObject) libExpStatJSONArray.get(i);
                keySet.add((Long) tempObj.get("id"));
            }
            libExpStatJSONObj.put("keySet", keySet);
            libExpStatJSONObj.put("libraryExpertStatArr", libExpStatJSONArray);
            return libExpStatJSONObj;
        }
        return null;
    }

    /**
     * This is to assigned LibExpert And Delete UnassignedEntry.
     *
     * @param assignedResourceIds
     * @param user
     * @return
     * @throws Exception
     */
    public boolean assignedLibExpertAndDeleteUnassignedEntry(List<Long> assignedResourceIds, User user) throws Exception {
        List<ResourcesWithoutLibUser> resourcesWithoutLibUserList = curationActivityDAO.checkResourcesWithoutLibUserRegister();
        if (!resourcesWithoutLibUserList.isEmpty()) {
            List<ResourcesWithoutLibUser> filteredList = resourcesWithoutLibUserList.stream()
                    .filter(resourcesWithoutLibUser-> assignedResourceIds.contains(resourcesWithoutLibUser.getResourceId()))
                    .collect(Collectors.toList());
            List<String> resourceCodes = filteredList.stream()
                    .map(ResourcesWithoutLibUser::getResource)
                    .collect(Collectors.toList())
                    .stream()
                    .map(Resource::getResourceCode)
                    .collect(Collectors.toList());
            
//            if(!resourceCodes.isEmpty() && crowdSourceDAO.assignedLibExpertToCrowdsourceList(resourceCodes,user)) {
//                filteredList.stream().forEach(obj->resourcesWithoutLibUserDAO.delete(obj));
//            }
        }
        return true;
    }

    /**
     * Fetch all available organizations.
     */
    public JSONObject fetchAllOrganizations() {
        List<Organization> organizationsList = curationActivityDAO.fetchAllOrganizations();
        JSONObject organizationJSONObject = new JSONObject();
        JSONArray organizationJSONArray = new JSONArray();
        organizationsList.forEach(org -> {
            JSONObject tempOrganizationJSONObject = new JSONObject();
            tempOrganizationJSONObject.put("organizationId", org.getId());
            tempOrganizationJSONObject.put("organizationName", org.getName());
            organizationJSONArray.add(tempOrganizationJSONObject);
        });
        organizationJSONObject.put("organizationJSONArray", organizationJSONArray);
        return organizationJSONObject;
    }

    /**
     * To save assigned organizations.
     *
     * @param userId
     * @param organizationsIdsList
     */
    public void saveAssignedOragnizations(Long userId, List<Long> organizationsIdsList) {
        User user = userDAO.getUserById(userId);
        Set<Organization> organizations;
        if (organizationsIdsList.isEmpty()) {
            organizations = new HashSet<>();
        } else {
            organizations = new HashSet<>(organizationDAO.getListOfOrganizationsByOrganizationIds(organizationsIdsList));
        }
        user.setAssignedOrganizations(organizations);
        userDAO.update(user);
    }

    /**
     * This is to Footer Table of PDF.
     */
    public class FooterTable extends PdfPageEventHelper {

        /**
         *
         * @param writer
         * @param document
         */
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            Font ffont = new Font(Font.FontFamily.UNDEFINED, 8, Font.ITALIC);
            Phrase footer = new Phrase("This report is generated through NVLI software, Developed by Human-Centered Design and Computing (HCDC) Group, C-DAC, Pune.", ffont);
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                    footer,
                    (document.right() - document.left()) / 2 + document.leftMargin(),
                    document.bottom() - 10, 0);
        }
    }

    /**
     * This is to get LanguageList.
     *
     * @return @throws Exception
     */
    public List<UDCLanguage> getLanguageList() throws Exception {
        return crowdSourcingService.getIndianLanguageList();
    }
    
    /**
     * This is to get Resources By Filters With Limit.
     *
     * @param searchString
     * @param resourceTypeFilter
     * @param dataLimit
     * @param pageNo
     * @return
     * @throws Exception
     */
    public JSONObject getResourcesByFiltersWithLimit(String searchString, long resourceTypeFilter, int dataLimit, int pageNo) throws Exception {
        JSONObject finalJSONObject = new JSONObject();
        JSONArray resourceTypeJSONArray = new JSONArray();
        JSONObject resourceTypeJSONObject;
        JSONArray resourceJSONArray;
        JSONObject resourceJSONObject;
        Map<Long, JSONArray> resourceMap = new HashMap<>();
        
        for (ResourceType resourceType : resourceTypeDAO.getResourceTypes()) {
            resourceTypeJSONObject = new JSONObject();
            resourceTypeJSONObject.put("id", resourceType.getId());
            resourceTypeJSONObject.put("resourceTypeCode", resourceType.getResourceTypeCode());
            resourceTypeJSONObject.put("resourceTypeName", resourceType.getResourceType());
            resourceTypeJSONArray.add(resourceTypeJSONObject);
            
            resourceJSONArray = new JSONArray();
            resourceMap.put(resourceType.getId(), resourceJSONArray);
        }
        
        for (Resource resource : resourceDAO.getResourcesByFiltersWithLimit(searchString, resourceTypeFilter, dataLimit, pageNo)) {
            resourceJSONObject = new JSONObject();
            resourceJSONObject.put("id", resource.getId());
            resourceJSONObject.put("resourceCode", resource.getResourceCode());
            resourceJSONObject.put("resourceName", resource.getResourceName());
            
            resourceJSONArray=resourceMap.get(resource.getResourceType().getId());
            
            resourceJSONArray.add(resourceJSONObject);
        }
        finalJSONObject.put("resourceTypeArray", resourceTypeJSONArray);
        finalJSONObject.put("resourceMap", resourceMap);
        return finalJSONObject;
    }

    /**
     * This is to save Assigned Resources.
     *
     * @param userId
     * @param resourceIdList
     * @throws Exception
     */
    public void saveAssignedResources(Long userId, List<Long> resourceIdList) throws Exception {
        User user = userDAO.getUserById(userId);
        Set<Resource> resources;
        if(resourceIdList.isEmpty()) {
            resources = new HashSet<>();
        } else {
            resources = new HashSet<>(resourceDAO.getListOfResourceByResourceIds(resourceIdList));
        }
        user.setAssignedResources(resources);
        userDAO.update(user);
    }
    
    /**
     * This is to get User By Assigned Resource And User Ids.
     *
     * @param resourceCode
     * @param userIds
     * @param userRole
     * @return
     * @throws Exception
     */
    public JSONArray getUserByAssignedResourceAndUserIds(String resourceCode, Set<Long> userIds, String userRole) throws Exception {
        return userDAO.getUserByAssignedResourceAndUserIds(resourceCode, userIds, userRole);
    }
    
    /**
     * This is to get Resource Code Name Map.
     *
     * @param resourceCodeList
     * @return
     * @throws Exception
     */
    public Map<String, String> getResourceCodeNameMap(List<String> resourceCodeList) throws Exception {
        return curationActivityDAO.getResourceCodeNameMap(resourceCodeList);
    }
}
